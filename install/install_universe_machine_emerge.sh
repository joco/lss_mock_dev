# install software
# EMERGE
# https://ui.adsabs.harvard.edu/abs/2018MNRAS.477.1822M/abstract
# http://ascl.net/1910.006
# https://github.com/bmoster/emerge

# UNIVERSE MACHINE
# https://ui.adsabs.harvard.edu/abs/2019MNRAS.488.3143B/abstract
# https://www.peterbehroozi.com/data.html
# https://bitbucket.org/pbehroozi/universemachine/wiki/Home

# MIRA titan
# https://github.com/SebastianBocquet/MiraTitanHMFemulator/
# https://ui.adsabs.harvard.edu/abs/2020arXiv200312116B
