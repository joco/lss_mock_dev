#!/bin/bash

# download source if not yet available
[ -f heasoft-6.26.1src.tar.gz ] && echo "Source alredy exists." || wget http://heasarc.gsfc.nasa.gov/FTP/software/lheasoft/lheasoft6.26/heasoft-6.26.1src.tar.gz

SOURCEDIR=$PWD
# build in /dev/shm which is much faster
BUILDDIR=/dev/shm/build_heasoft

mkdir -p $BUILDDIR
pushd $BUILDDIR

echo "Potentially removing old source"
rm -rf heasoft-6.26.1
echo "Extracting source"
tar -xf $SOURCEDIR/heasoft-6.26.1src.tar.gz

module load anaconda/2
pushd heasoft-6.26.1/BUILD_DIR/
echo "Running configure"
./configure --prefix=/u/$USER/heasoft || exit 1
# on SLES, a symlink is missing, so specify available libtermcap explicitly
sed -e "s/termcap/:libtermcap.so.2/" -i ../maxi/lib/maxitool/perl/Makefile.PL || exit 1

echo "Running make"
make || exit 1
echo "Running make install"
make install || exit 1

popd
popd

echo "Deleting build directory"
rm -rf $BUILDDIR

echo
echo
echo "Add the following lines to your .bashrc: (you might need to adapt the HEADAS path)"
echo 'export HEADAS=/u/$USER/heasoft/x86_64-pc-linux-gnu-libc2.22'
echo 'alias heainit=". $HEADAS/headas-init.sh"'
echo "With this you can call heainit on the command line to make the HEASOFT tools available."
