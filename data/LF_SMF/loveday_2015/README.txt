Jon Loveday <j.loveday@sussex.ac.uk>
2016-02-15

Data files for the luminosity and stellar mass functions presented in 
Loveday et al., 2015, MNRAS, 451, 1540.

lfs.txt corresponds to Fig. 8, and gives r-band LFs for the whole
sample (_c), blue (_b) and red (_r) galaxies separately using both
Petrosian and Sersic magnitudes.

smf.txt corresponds to Fig. 11, and includes the incompleteness-correction
weighted and unweighted stellar mass functions.

As in the paper, h=1 and K-corrections are done to z=0.1.
