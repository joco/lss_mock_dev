"""
Creates a fits catalog containing the 4FS input columns.

Create the ID array
remove unwanted column
check rulesets and templates names are correct

import os, sys, glob
import numpy as n
import astropy.io.fits as fits

root_dir = '/data42s/comparat/firefly/mocks/2020-03/QMOST/'

file_list_a = n.array(glob.glob(os.path.join(root_dir,"*.fit")))
file_list_b = n.array(glob.glob(os.path.join(root_dir,"*.fits")))
file_list_c = n.array(glob.glob(os.path.join(os.environ['MD10'],"*.fits")))
file_list_d = n.array(glob.glob(os.path.join(os.environ['MD10'],"*.fit")))
file_list = n.hstack((file_list_a, file_list_b, file_list_c, file_list_d))
file_list.sort()
for el in file_list:
	hd = fits.open(el)
	print( el, hd[1].header['NAXIS2'])


print(n.round(10326528/41273, 2))
print(n.round(16500480/41273, 2))
print(n.round(49128564/41273, 2))
print(n.round(4804985/34089 , 2))
print(n.round(2114525/34089 , 2))

print(n.round(1886551 / 7500 , 2))
print(n.round(3015543 / 7500 , 2))
print(n.round(1336103 / 1100 , 2))
print(n.round(1139832 / 7500 , 2))
print(n.round( 513828 / 7500 , 2))

 
"""
import glob, sys, time, os
t0 = time.time()
from astropy.table import Table, Column
import numpy as n
from sklearn.neighbors import BallTree

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm 
from matplotlib import rc
import astropy.io.fits as fits

from astropy.coordinates import SkyCoord
import astropy.units as u
import healpy as hp

print('Calculates shared targets', t0)
print('------------------------------------------------')
print('------------------------------------------------')

choice_area = "full"
choice_area = "small"
choice_area = sys.argv[1]

root_dir = working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'shared_target_table')
# '/data42s/comparat/firefly/mocks/2020-03/QMOST/'
radius = 0.1 / 3600. * n.pi / 180.
deg_to_rad = n.pi/180.
# get survey footprint bitlist
def fourmost_get_survbitlist():
	mydict                = {}
	mydict['des']         = 0
	mydict['kidss']       = 1
	mydict['kidsn']       = 2
	mydict['atlassgcnotdes'] = 3
	mydict['atlasngc']    = 4
	mydict['kabs']        = 5
	mydict['vhsb10']      = 6
	mydict['vhsb15']      = 7
	mydict['vhsb20']      = 8
	mydict['vhsb20clean'] = 9
	mydict['desi']        = 10
	mydict['erosita']     = 11
	mydict['waveswide']   = 12
	mydict['euclid']      = 13
	mydict['s8elg']       = 14
	mydict['s8']          = 15
	return mydict


def mk_radec_plot(ra,dec,nside=32,filename=' ', title=''):
    #
    # ra,dec in degrees
    # nside is the healpix parameter setting the size of pixels, must be a power of 2, less than 2**30
    #
	npix = hp.nside2npix(nside)
	pixarea = hp.nside2pixarea(nside, degrees = True)
	print("Pixel area [deg^2]: ",pixarea)
	# convert ra,dec to HEALPix indices and count
	indices = hp.ang2pix(nside, ra, dec, lonlat=True)
	idx, counts = n.unique(indices, return_counts=True)
	# fill the fullsky map
	hpx_map = n.zeros(npix, dtype=int)
	hpx_map[idx]      = counts/pixarea
	# set some fonts
	rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
	## for Palatino and other serif fonts use:
	#rc('font',**{'family':'serif','serif':['Palatino']})
	#rc('text', usetex=True)
	#rc('mathtext.fontset','stixsans')
	# define colour map
	cmap = plt.cm.inferno
	cmap.set_bad('w')
	cmap.set_under('w')
	# make the map with meridians overplotted
	hp.mollview(hpx_map, 
				coord='C', rot=(180,0),
				unit=r' Object counts per degree$^2$', xsize = 1000, cmap=cmap, min=50, norm='log', cbar=None)
	hp.graticule(dpar=15, dmer=20, verbose= True)
	# HA labels
	params = {'mathtext.default': 'regular' }          
	plt.rcParams.update(params)
	#hp.projtext(20,  32,  '$\mathrm{\mathsf{0^h}}$',     color = 'black', fontsize = 16, lonlat=True)
	#hp.projtext(88,  32,  '$\mathrm{\mathsf{6^h}}$',     color = 'black', fontsize = 16, lonlat=True)
	#hp.projtext(177, 32,  '$\mathrm{\mathsf{12^h}}$',    color = 'black', fontsize = 16, lonlat=True) #rotation
	#hp.projtext(264, 32,  '$\mathrm{\mathsf{18^h}}$',    color = 'black', fontsize = 16, lonlat=True)
	#hp.projtext(351, 32,  '$\mathrm{\mathsf{24^h}}$',    color = 'black', fontsize = 16, lonlat=True)
	hp.projtext(20,  32,  '$\mathrm{\mathsf{0^\circ}}$',     color = 'black', fontsize = 16, lonlat=True)
	hp.projtext(88,  32,  '$\mathrm{\mathsf{90^\circ}}$',     color = 'black', fontsize = 16, lonlat=True)
	hp.projtext(177, 32,  '$\mathrm{\mathsf{180^\circ}}$',    color = 'black', fontsize = 16, lonlat=True) #rotation
	hp.projtext(264, 32,  '$\mathrm{\mathsf{270^\circ}}$',    color = 'black', fontsize = 16, lonlat=True)
	hp.projtext(351, 32,  '$\mathrm{\mathsf{360^\circ}}$',    color = 'black', fontsize = 16, lonlat=True)
	plt.title(' ', size=14)
	ax = plt.gca()
	im = ax.get_images()[0]
	# DEC labels
	fig= plt.gcf()
	for ax in fig.get_axes():
		if type(ax) is hp.projaxes.HpxMollweideAxes:
			ax.set_ylim(-1, 0.51)
			ax.set_position([0.02, 0.03, 0.94, 0.95])
			ax.annotate(r'$\, \mathrm{\mathsf{ 0^\circ}}$', xy=(2.04,-0.02),    size=16, annotation_clip=False) 
			ax.annotate(r'$\, \mathrm{\mathsf{ 30^\circ}}$', xy=(1.90,0.38),     size=16) 
			ax.annotate(r'$\, \mathrm{\mathsf{-30^\circ}}$', xy=(1.86,-0.42),    size=16) 
			ax.annotate(r'$\, \mathrm{\mathsf{-60^\circ}}$', xy=(1.38, -0.78),   size=16) 
			ax.annotate(r'%s targets' %len(ra), xy=(-2.0, -0.92),  size=14) # optional, can be removed

	# create colour bar
	cbaxes = fig.add_axes([0.1, 0.15, 0.8, 0.04]) # [left, bottom, width, height]
	tks=[50, 100, 200, 300, 500, 1000, 10000, 100000, 1000000, 10000000]
	cb = plt.colorbar(im,  orientation='horizontal', cax = cbaxes, ticks=tks)             
	cb.set_label('Object counts per degree$\mathrm{\mathsf{^2}}$', fontsize=14)
	cb.ax.tick_params(labelsize=16)
	#plt.xlim([0,1])
	# save plot to PDF file
	plt.savefig(filename + '.pdf')
	plt.savefig(filename + '.png')

svbdict = fourmost_get_survbitlist()

#nl = lambda selection : len(selection.nonzero()[0])
catalogue_dir = '/data42s/comparat/firefly/mocks/2021-04/QMOST'

dir_2_CLU  = os.path.join(catalogue_dir, 'S5_QMOST_Apr2021_FIBERMAGcut_225_215_190_correct_output_ETC2021-04-20_16_04_35.fits.gz' )
dir_2_COS  = os.path.join(catalogue_dir, 'S8_4MOST_03May21.fits.gz'     )
dir_2_AGN  = os.path.join(catalogue_dir, 'S6_4MOST_ALL_SNR3_IR215_14Apr21_FIBERMAGcut_225_correct_output_ETC2021-04-23_09_23_35.fits.gz')

print('open catalogs',time.time()-t0)

t_CLU  = fits.open(  dir_2_CLU  )[1].data
t_COS  = fits.open(  dir_2_COS  )[1].data
t_AGN  = fits.open(  dir_2_AGN  )[1].data

print('select areas and sub surveys',time.time()-t0)

# full catalogs
if choice_area == "full":
	common_area = lambda ra, dec : (dec<40)
	area_c = 1. # is not know each catalog has a different value

if choice_area == "small":
	# small part of catalogs catalogs
	common_area = lambda ra, dec : (ra>160)&(ra<200)&(dec>-20)&(dec<-10)
	area_c = 385.87

ca_s5 = common_area(t_CLU['RA'], t_CLU['DEC'])
s3 = ( t_CLU['SUBSURVEY'] == "cluster_BCG"  )     &(ca_s5)
s4 = ( t_CLU['SUBSURVEY'] == "cluster_redGAL"  )  &(ca_s5)
s5 = ( t_CLU['SUBSURVEY'] == "filament_GAL" )     &(ca_s5)

ca_s6 = common_area(t_AGN['RA'], t_AGN['DEC'])
s0 = ( t_AGN['SUBSURVEY'] == 'AGN_WIDE' )&(ca_s6)
s1 = ( t_AGN['SUBSURVEY'] == 'AGN_DEEP' )&(ca_s6)
s2 = ( t_AGN['SUBSURVEY'] == 'AGN_IR'   )&(ca_s6)

s_QSO = ( t_COS['SUBSURVEY'] == "QSO") & common_area(t_COS['RA'], t_COS['DEC'])
s_LyA = ( t_COS['SUBSURVEY'] == "LyA") & common_area(t_COS['RA'], t_COS['DEC'])
s__BG = ( t_COS['SUBSURVEY'] == "BG")  & common_area(t_COS['RA'], t_COS['DEC'])
s_LRG = ( t_COS['SUBSURVEY'] == "LRG") & common_area(t_COS['RA'], t_COS['DEC'])

print('creates coordinates arrays',time.time()-t0)

coord_BCG = deg_to_rad * n.transpose([t_CLU['DEC'][s3], t_CLU['RA'][s3] ])
coord_GAL = deg_to_rad * n.transpose([t_CLU['DEC'][s4], t_CLU['RA'][s4] ])
coord_FIL = deg_to_rad * n.transpose([t_CLU['DEC'][s5], t_CLU['RA'][s5] ])
if choice_area == "full":
	mk_radec_plot(coord_BCG.T[1]/deg_to_rad, coord_BCG.T[0]/deg_to_rad ,nside=32,filename=os.path.join(root_dir,'figures','S5_BCG'))
	mk_radec_plot(coord_GAL.T[1]/deg_to_rad, coord_GAL.T[0]/deg_to_rad ,nside=32,filename=os.path.join(root_dir,'figures','S5_GAL'))
	mk_radec_plot(coord_FIL.T[1]/deg_to_rad, coord_FIL.T[0]/deg_to_rad ,nside=32,filename=os.path.join(root_dir,'figures','S5_FIL'))

coord_Awi = deg_to_rad * n.transpose([t_AGN['DEC'][s0], t_AGN['RA'][s0] ])
coord_Ade = deg_to_rad * n.transpose([t_AGN['DEC'][s1], t_AGN['RA'][s1] ])
coord_Air = deg_to_rad * n.transpose([t_AGN['DEC'][s2], t_AGN['RA'][s2] ])
if choice_area == "full":
	mk_radec_plot(coord_Awi.T[1]/deg_to_rad, coord_Awi.T[0]/deg_to_rad ,nside=32,filename=os.path.join(root_dir,'figures','S6_Awi'))
	mk_radec_plot(coord_Ade.T[1]/deg_to_rad, coord_Ade.T[0]/deg_to_rad ,nside=32,filename=os.path.join(root_dir,'figures','S6_Ade'))
	mk_radec_plot(coord_Air.T[1]/deg_to_rad, coord_Air.T[0]/deg_to_rad ,nside=32,filename=os.path.join(root_dir,'figures','S6_Air'))

coord_QSO = deg_to_rad * n.transpose([t_COS['DEC'][s_QSO], t_COS['RA'][s_QSO] ])
coord_LyA = deg_to_rad * n.transpose([t_COS['DEC'][s_LyA], t_COS['RA'][s_LyA] ])
coord__BG = deg_to_rad * n.transpose([t_COS['DEC'][s__BG], t_COS['RA'][s__BG] ])
coord_LRG = deg_to_rad * n.transpose([t_COS['DEC'][s_LRG], t_COS['RA'][s_LRG] ])
if choice_area == "full":
	mk_radec_plot(coord_QSO.T[1]/deg_to_rad, coord_QSO.T[0]/deg_to_rad ,nside=32,filename=os.path.join(root_dir,'figures','S8_QSO'))
	mk_radec_plot(coord_LyA.T[1]/deg_to_rad, coord_LyA.T[0]/deg_to_rad ,nside=32,filename=os.path.join(root_dir,'figures','S8_LyA'))
	mk_radec_plot(coord__BG.T[1]/deg_to_rad, coord__BG.T[0]/deg_to_rad ,nside=32,filename=os.path.join(root_dir,'figures','S8__BG'))
	mk_radec_plot(coord_LRG.T[1]/deg_to_rad, coord_LRG.T[0]/deg_to_rad ,nside=32,filename=os.path.join(root_dir,'figures', 'S8_LRG'))

N_BCG = len(coord_BCG)
N_GAL = len(coord_GAL)
N_FIL = len(coord_FIL)
N_Awi = len(coord_Awi)
N_Ade = len(coord_Ade)
N_Air = len(coord_Air)
N_QSO = len(coord_QSO)
N_LyA = len(coord_LyA)
N__BG = len(coord__BG)
N_LRG = len(coord_LRG)


print( 
'BCG', '&',
'GAL', '&',
'FIL', '&',
'Awi', '&',
'Ade', '&',
'Air', '&',
'QSO', '&',
'LyA', '&',
'_BG', '&',
'LRG', '\\\\')
print(
N_BCG, '&',
N_GAL, '&',
N_FIL, '&',
N_Awi, '&',
N_Ade, '&',
N_Air, '&',
N_QSO, '&',
N_LyA, '&',
N__BG, '&',
N_LRG, '\\\\')

def count_pairs(Tree_, coord_, radius = radius):
	test_c = Tree_.query_radius(coord_, r = radius, count_only = True) 
	sum_t = n.sum(test_c)
	print(sum_t, time.time()-t0)
	return sum_t


if choice_area == "full":
	print('builds trees',time.time()-t0)
	Tree_BCG = BallTree(coord_BCG, metric='haversine')
	Tree_GAL = BallTree(coord_GAL, metric='haversine')
	Tree_FIL = BallTree(coord_FIL, metric='haversine')

	Tree_Awi = BallTree(coord_Awi, metric='haversine')
	Tree_Ade = BallTree(coord_Ade, metric='haversine')
	Tree_Air = BallTree(coord_Air, metric='haversine')

	Tree_QSO = BallTree(coord_QSO, metric='haversine')
	Tree_LyA = BallTree(coord_LyA, metric='haversine')
	Tree__BG = BallTree(coord__BG, metric='haversine')
	Tree_LRG = BallTree(coord_LRG, metric='haversine')

	print('counts pairs trees',time.time()-t0)
	count_BCG_BCG = count_pairs(Tree_BCG, coord_BCG, radius = radius)
	count_BCG_GAL = count_pairs(Tree_BCG, coord_GAL, radius = radius)
	count_BCG_FIL = count_pairs(Tree_BCG, coord_FIL, radius = radius)
	count_BCG_Awi = count_pairs(Tree_BCG, coord_Awi, radius = radius)
	count_BCG_Ade = count_pairs(Tree_BCG, coord_Ade, radius = radius)
	count_BCG_Air = count_pairs(Tree_BCG, coord_Air, radius = radius)
	count_BCG_QSO = count_pairs(Tree_BCG, coord_QSO, radius = radius)
	count_BCG_LyA = count_pairs(Tree_BCG, coord_LyA, radius = radius)
	count_BCG__BG = count_pairs(Tree_BCG, coord__BG, radius = radius)
	count_BCG_LRG = count_pairs(Tree_BCG, coord_LRG, radius = radius)
	#
	count_GAL_GAL = count_pairs(Tree_GAL, coord_GAL, radius = radius)
	count_GAL_FIL = count_pairs(Tree_GAL, coord_FIL, radius = radius)
	count_GAL_Awi = count_pairs(Tree_GAL, coord_Awi, radius = radius)
	count_GAL_Ade = count_pairs(Tree_GAL, coord_Ade, radius = radius)
	count_GAL_Air = count_pairs(Tree_GAL, coord_Air, radius = radius)
	count_GAL_QSO = count_pairs(Tree_GAL, coord_QSO, radius = radius)
	count_GAL_LyA = count_pairs(Tree_GAL, coord_LyA, radius = radius)
	count_GAL__BG = count_pairs(Tree_GAL, coord__BG, radius = radius)
	count_GAL_LRG = count_pairs(Tree_GAL, coord_LRG, radius = radius)
	#
	count_FIL_FIL = count_pairs(Tree_FIL, coord_FIL, radius = radius)
	count_FIL_Awi = count_pairs(Tree_FIL, coord_Awi, radius = radius)
	count_FIL_Ade = count_pairs(Tree_FIL, coord_Ade, radius = radius)
	count_FIL_Air = count_pairs(Tree_FIL, coord_Air, radius = radius)
	count_FIL_QSO = count_pairs(Tree_FIL, coord_QSO, radius = radius)
	count_FIL_LyA = count_pairs(Tree_FIL, coord_LyA, radius = radius)
	count_FIL__BG = count_pairs(Tree_FIL, coord__BG, radius = radius)
	count_FIL_LRG = count_pairs(Tree_FIL, coord_LRG, radius = radius)
	#
	count_Awi_Awi = count_pairs(Tree_Awi, coord_Awi, radius = radius)
	count_Awi_Ade = count_pairs(Tree_Awi, coord_Ade, radius = radius)
	count_Awi_Air = count_pairs(Tree_Awi, coord_Air, radius = radius)
	count_Awi_QSO = count_pairs(Tree_Awi, coord_QSO, radius = radius)
	count_Awi_LyA = count_pairs(Tree_Awi, coord_LyA, radius = radius)
	count_Awi__BG = count_pairs(Tree_Awi, coord__BG, radius = radius)
	count_Awi_LRG = count_pairs(Tree_Awi, coord_LRG, radius = radius)
	#
	count_Ade_Ade = count_pairs(Tree_Ade, coord_Ade, radius = radius)
	count_Ade_Air = count_pairs(Tree_Ade, coord_Air, radius = radius)
	count_Ade_QSO = count_pairs(Tree_Ade, coord_QSO, radius = radius)
	count_Ade_LyA = count_pairs(Tree_Ade, coord_LyA, radius = radius)
	count_Ade__BG = count_pairs(Tree_Ade, coord__BG, radius = radius)
	count_Ade_LRG = count_pairs(Tree_Ade, coord_LRG, radius = radius)
	#
	count_Air_Air = count_pairs(Tree_Air, coord_Air, radius = radius)
	count_Air_QSO = count_pairs(Tree_Air, coord_QSO, radius = radius)
	count_Air_LyA = count_pairs(Tree_Air, coord_LyA, radius = radius)
	count_Air__BG = count_pairs(Tree_Air, coord__BG, radius = radius)
	count_Air_LRG = count_pairs(Tree_Air, coord_LRG, radius = radius)
	#
	count_QSO_QSO = count_pairs(Tree_QSO, coord_QSO, radius = radius)
	count_QSO_LyA = count_pairs(Tree_QSO, coord_LyA, radius = radius)
	count_QSO__BG = count_pairs(Tree_QSO, coord__BG, radius = radius)
	count_QSO_LRG = count_pairs(Tree_QSO, coord_LRG, radius = radius)
	#
	count_LyA_LyA = count_pairs(Tree_LyA, coord_LyA, radius = radius)
	count_LyA__BG = count_pairs(Tree_LyA, coord__BG, radius = radius)
	count_LyA_LRG = count_pairs(Tree_LyA, coord_LRG, radius = radius)
	#
	count__BG__BG = count_pairs(Tree__BG, coord__BG, radius = radius)
	count__BG_LRG = count_pairs(Tree__BG, coord_LRG, radius = radius)
	#
	count_LRG_LRG = count_pairs(Tree_LRG, coord_LRG, radius = radius)

	print( 
	'BCG', '&',
	'GAL', '&',
	'FIL', '&',
	'Awi', '&',
	'Ade', '&',
	'Air', '&',
	'QSO', '&',
	'LyA', '&',
	'_BG', '&',
	'LRG', '\\\\')
	print(
	N_BCG, '&',
	N_GAL, '&',
	N_FIL, '&',
	N_Awi, '&',
	N_Ade, '&',
	N_Air, '&',
	N_QSO, '&',
	N_LyA, '&',
	N__BG, '&',
	N_LRG, '\\\\')
	print( 'BCG & ', count_BCG_BCG,'&', count_BCG_GAL,'&', count_BCG_FIL,'&', count_BCG_Awi,'&', count_BCG_Ade,'&', count_BCG_Air,'&', count_BCG_QSO,'&', count_BCG_LyA,'&', count_BCG__BG,'&', count_BCG_LRG, " \\\\" )
	print( 'GAL & ', count_BCG_GAL,'&', count_GAL_GAL,'&', count_GAL_FIL,'&', count_GAL_Awi,'&', count_GAL_Ade,'&', count_GAL_Air,'&', count_GAL_QSO,'&', count_GAL_LyA,'&', count_GAL__BG,'&', count_GAL_LRG, " \\\\" )
	print( 'FIL & ', count_BCG_FIL,'&', count_GAL_FIL,'&', count_FIL_FIL,'&', count_FIL_Awi,'&', count_FIL_Ade,'&', count_FIL_Air,'&', count_FIL_QSO,'&', count_FIL_LyA,'&', count_FIL__BG,'&', count_FIL_LRG, " \\\\" )
	print( 'Awi & ', count_BCG_Awi,'&', count_GAL_Awi,'&', count_FIL_Awi,'&', count_Awi_Awi,'&', count_Awi_Ade,'&', count_Awi_Air,'&', count_Awi_QSO,'&', count_Awi_LyA,'&', count_Awi__BG,'&', count_Awi_LRG, " \\\\" )
	print( 'Ade & ', count_BCG_Ade,'&', count_GAL_Ade,'&', count_FIL_Ade,'&', count_Awi_Ade,'&', count_Ade_Ade,'&', count_Ade_Air,'&', count_Ade_QSO,'&', count_Ade_LyA,'&', count_Ade__BG,'&', count_Ade_LRG, " \\\\" )
	print( 'Air & ', count_BCG_Air,'&', count_GAL_Air,'&', count_FIL_Air,'&', count_Awi_Air,'&', count_Ade_Air,'&', count_Air_Air,'&', count_Air_QSO,'&', count_Air_LyA,'&', count_Air__BG,'&', count_Air_LRG, " \\\\" )
	print( 'QSO & ', count_BCG_QSO,'&', count_GAL_QSO,'&', count_FIL_QSO,'&', count_Awi_QSO,'&', count_Ade_QSO,'&', count_Air_QSO,'&', count_QSO_QSO,'&', count_QSO_LyA,'&', count_QSO__BG,'&', count_QSO_LRG, " \\\\" )
	print( 'LyA & ', count_BCG_LyA,'&', count_GAL_LyA,'&', count_FIL_LyA,'&', count_Awi_LyA,'&', count_Ade_LyA,'&', count_Air_LyA,'&', count_QSO_LyA,'&', count_LyA_LyA,'&', count_LyA__BG,'&', count_LyA_LRG, " \\\\" )
	print( ' BG & ', count_BCG__BG,'&', count_GAL__BG,'&', count_FIL__BG,'&', count_Awi__BG,'&', count_Ade__BG,'&', count_Air__BG,'&', count_QSO__BG,'&', count_LyA__BG,'&', count__BG__BG,'&', count__BG_LRG, " \\\\" )
	print( 'LRG & ', count_BCG_LRG,'&', count_GAL_LRG,'&', count_FIL_LRG,'&', count_Awi_LRG,'&', count_Ade_LRG,'&', count_Air_LRG,'&', count_QSO_LRG,'&', count_LyA_LRG,'&', count__BG_LRG,'&', count_LRG_LRG, " \\\\" )    


	print( 'BCG & ', n.round(100*count_BCG_BCG/N_BCG,1),'&', n.round(100*count_BCG_GAL/N_GAL,1),'&', n.round(100*count_BCG_FIL/N_FIL,1),'&', n.round(100*count_BCG_Awi/N_Awi,1),'&', n.round(100*count_BCG_Ade/N_Ade,1),'&', n.round(100*count_BCG_Air/N_Air,1),'&', n.round(100*count_BCG_QSO/N_QSO,1),'&', n.round(100*count_BCG_LyA/N_LyA,1),'&', n.round(100*count_BCG__BG/N__BG,1),'&', n.round(100*count_BCG_LRG/N_LRG,1), " \\\\" )
	print( 'GAL & ', n.round(100*count_BCG_GAL/N_BCG,1),'&', n.round(100*count_GAL_GAL/N_GAL,1),'&', n.round(100*count_GAL_FIL/N_FIL,1),'&', n.round(100*count_GAL_Awi/N_Awi,1),'&', n.round(100*count_GAL_Ade/N_Ade,1),'&', n.round(100*count_GAL_Air/N_Air,1),'&', n.round(100*count_GAL_QSO/N_QSO,1),'&', n.round(100*count_GAL_LyA/N_LyA,1),'&', n.round(100*count_GAL__BG/N__BG,1),'&', n.round(100*count_GAL_LRG/N_LRG,1), " \\\\" )
	print( 'FIL & ', n.round(100*count_BCG_FIL/N_BCG,1),'&', n.round(100*count_GAL_FIL/N_GAL,1),'&', n.round(100*count_FIL_FIL/N_FIL,1),'&', n.round(100*count_FIL_Awi/N_Awi,1),'&', n.round(100*count_FIL_Ade/N_Ade,1),'&', n.round(100*count_FIL_Air/N_Air,1),'&', n.round(100*count_FIL_QSO/N_QSO,1),'&', n.round(100*count_FIL_LyA/N_LyA,1),'&', n.round(100*count_FIL__BG/N__BG,1),'&', n.round(100*count_FIL_LRG/N_LRG,1), " \\\\" )
	print( 'Awi & ', n.round(100*count_BCG_Awi/N_BCG,1),'&', n.round(100*count_GAL_Awi/N_GAL,1),'&', n.round(100*count_FIL_Awi/N_FIL,1),'&', n.round(100*count_Awi_Awi/N_Awi,1),'&', n.round(100*count_Awi_Ade/N_Ade,1),'&', n.round(100*count_Awi_Air/N_Air,1),'&', n.round(100*count_Awi_QSO/N_QSO,1),'&', n.round(100*count_Awi_LyA/N_LyA,1),'&', n.round(100*count_Awi__BG/N__BG,1),'&', n.round(100*count_Awi_LRG/N_LRG,1), " \\\\" )
	print( 'Ade & ', n.round(100*count_BCG_Ade/N_BCG,1),'&', n.round(100*count_GAL_Ade/N_GAL,1),'&', n.round(100*count_FIL_Ade/N_FIL,1),'&', n.round(100*count_Awi_Ade/N_Awi,1),'&', n.round(100*count_Ade_Ade/N_Ade,1),'&', n.round(100*count_Ade_Air/N_Air,1),'&', n.round(100*count_Ade_QSO/N_QSO,1),'&', n.round(100*count_Ade_LyA/N_LyA,1),'&', n.round(100*count_Ade__BG/N__BG,1),'&', n.round(100*count_Ade_LRG/N_LRG,1), " \\\\" )
	print( 'Air & ', n.round(100*count_BCG_Air/N_BCG,1),'&', n.round(100*count_GAL_Air/N_GAL,1),'&', n.round(100*count_FIL_Air/N_FIL,1),'&', n.round(100*count_Awi_Air/N_Awi,1),'&', n.round(100*count_Ade_Air/N_Ade,1),'&', n.round(100*count_Air_Air/N_Air,1),'&', n.round(100*count_Air_QSO/N_QSO,1),'&', n.round(100*count_Air_LyA/N_LyA,1),'&',  n.round(100*count_Air__BG/N__BG,1),'&', n.round(100*count_Air_LRG/N_LRG,1), " \\\\" )
	print( 'QSO & ', n.round(100*count_BCG_QSO/N_BCG,1),'&', n.round(100*count_GAL_QSO/N_GAL,1),'&', n.round(100*count_FIL_QSO/N_FIL,1),'&', n.round(100*count_Awi_QSO/N_Awi,1),'&', n.round(100*count_Ade_QSO/N_Ade,1),'&', n.round(100*count_Air_QSO/N_Air,1),'&', n.round(100*count_QSO_QSO/N_QSO,1),'&', n.round(100*count_QSO_LyA/N_LyA,1),'&',  n.round(100*count_QSO__BG/N__BG,1),'&', n.round(100*count_QSO_LRG/N_LRG,1), " \\\\" )
	print( 'LyA & ', n.round(100*count_BCG_LyA/N_BCG,1),'&', n.round(100*count_GAL_LyA/N_GAL,1),'&', n.round(100*count_FIL_LyA/N_FIL,1),'&', n.round(100*count_Awi_LyA/N_Awi,1),'&', n.round(100*count_Ade_LyA/N_Ade,1),'&', n.round(100*count_Air_LyA/N_Air,1),'&', n.round(100*count_QSO_LyA/N_QSO,1),'&', n.round(100*count_LyA_LyA/N_LyA,1),'&',  n.round(100*count_LyA__BG/N__BG,1),'&', n.round(100*count_LyA_LRG/N_LRG,1), " \\\\" )
	print( ' BG & ', n.round(100*count_BCG__BG/N_BCG,1),'&', n.round(100*count_GAL__BG/N_GAL,1),'&', n.round(100*count_FIL__BG/N_FIL,1),'&', n.round(100*count_Awi__BG/N_Awi,1),'&', n.round(100*count_Ade__BG/N_Ade,1),'&', n.round(100*count_Air__BG/N_Air,1),'&', n.round(100*count_QSO__BG/N_QSO,1),'&', n.round(100*count_LyA__BG/N_LyA,1),'&',  n.round(100*count__BG__BG/N__BG,1),'&', n.round(100*count__BG_LRG/N_LRG,1), " \\\\" )
	print( 'LRG & ', n.round(100*count_BCG_LRG/N_BCG,1),'&', n.round(100*count_GAL_LRG/N_GAL,1),'&', n.round(100*count_FIL_LRG/N_FIL,1),'&', n.round(100*count_Awi_LRG/N_Awi,1),'&', n.round(100*count_Ade_LRG/N_Ade,1),'&', n.round(100*count_Air_LRG/N_Air,1),'&', n.round(100*count_QSO_LRG/N_QSO,1),'&', n.round(100*count_LyA_LRG/N_LyA,1),'&',  n.round(100*count__BG_LRG/N__BG,1),'&', n.round(100*count_LRG_LRG/N_LRG,1), " \\\\" )    


	print( 'BCG & ', n.round(100*count_BCG_BCG/N_BCG,1)                                                                                                                                               , " \\\\"        )
	print( 'GAL & ', n.round(100*count_BCG_GAL/N_BCG,1),'&', n.round(100*count_GAL_GAL/N_GAL,1)                                                                                                                                , " \\\\"        )
	print( 'FIL & ', n.round(100*count_BCG_FIL/N_BCG,1),'&', n.round(100*count_GAL_FIL/N_GAL,1),'&', n.round(100*count_FIL_FIL/N_FIL,1)                                                                                                                 , " \\\\"       )
	print( 'Awi & ', n.round(100*count_BCG_Awi/N_BCG,1),'&', n.round(100*count_GAL_Awi/N_GAL,1),'&', n.round(100*count_FIL_Awi/N_FIL,1),'&', n.round(100*count_Awi_Awi/N_Awi,1)                                                                                                  , " \\\\"        )
	print( 'Ade & ', n.round(100*count_BCG_Ade/N_BCG,1),'&', n.round(100*count_GAL_Ade/N_GAL,1),'&', n.round(100*count_FIL_Ade/N_FIL,1),'&', n.round(100*count_Awi_Ade/N_Awi,1),'&', n.round(100*count_Ade_Ade/N_Ade,1)                                                                                   , " \\\\"        )
	print( 'Air & ', n.round(100*count_BCG_Air/N_BCG,1),'&', n.round(100*count_GAL_Air/N_GAL,1),'&', n.round(100*count_FIL_Air/N_FIL,1),'&', n.round(100*count_Awi_Air/N_Awi,1),'&', n.round(100*count_Ade_Air/N_Ade,1),'&',n.round(100*count_Air_Air/N_Air,1)                                                                      , " \\\\"      )
	print( 'QSO & ', n.round(100*count_BCG_QSO/N_BCG,1),'&', n.round(100*count_GAL_QSO/N_GAL,1),'&', n.round(100*count_FIL_QSO/N_FIL,1),'&', n.round(100*count_Awi_QSO/N_Awi,1),'&', n.round(100*count_Ade_QSO/N_Ade,1),'&',n.round(100*count_Air_QSO/N_Air,1), '&',n.round(100*count_QSO_QSO/N_QSO,1)                                                        , " \\\\"     )
	print( 'LyA & ', n.round(100*count_BCG_LyA/N_BCG,1),'&', n.round(100*count_GAL_LyA/N_GAL,1),'&', n.round(100*count_FIL_LyA/N_FIL,1),'&', n.round(100*count_Awi_LyA/N_Awi,1),'&', n.round(100*count_Ade_LyA/N_Ade,1),'&',n.round(100*count_Air_LyA/N_Air,1), '&',n.round(100*count_QSO_LyA/N_QSO,1), '&',n.round(100*count_LyA_LyA/N_LyA,1)                                           , " \\\\"   )
	print( ' BG & ', n.round(100*count_BCG__BG/N_BCG,1),'&', n.round(100*count_GAL__BG/N_GAL,1),'&', n.round(100*count_FIL__BG/N_FIL,1),'&', n.round(100*count_Awi__BG/N_Awi,1),'&', n.round(100*count_Ade__BG/N_Ade,1),'&',n.round(100*count_Air__BG/N_Air,1), '&',n.round(100*count_QSO__BG/N_QSO,1), '&',n.round(100*count_LyA__BG/N_LyA,1), '&',n.round(100*count__BG__BG/N__BG,1)              , " \\\\"  )
	print( 'LRG & ', n.round(100*count_BCG_LRG/N_BCG,1),'&', n.round(100*count_GAL_LRG/N_GAL,1),'&', n.round(100*count_FIL_LRG/N_FIL,1),'&', n.round(100*count_Awi_LRG/N_Awi,1),'&', n.round(100*count_Ade_LRG/N_Ade,1),'&',n.round(100*count_Air_LRG/N_Air,1), '&',n.round(100*count_QSO_LRG/N_QSO,1), '&',n.round(100*count_LyA_LRG/N_LyA,1),  '&',n.round(100*count__BG_LRG/N__BG,1),'&', n.round(100*count_LRG_LRG/N_LRG,1), " \\\\" )    

if choice_area == "small":
	print('builds trees',time.time()-t0)
	Tree_BCG = BallTree(coord_BCG, metric='haversine')
	Tree_GAL = BallTree(coord_GAL, metric='haversine')
	Tree_FIL = BallTree(coord_FIL, metric='haversine')
	Tree_Awi = BallTree(coord_Awi, metric='haversine')
	Tree_Air = BallTree(coord_Air, metric='haversine')
	Tree_QSO = BallTree(coord_QSO, metric='haversine')
	Tree_LyA = BallTree(coord_LyA, metric='haversine')
	Tree__BG = BallTree(coord__BG, metric='haversine')
	Tree_LRG = BallTree(coord_LRG, metric='haversine')

	print('counts pairs trees',time.time()-t0)
	count_BCG_BCG = count_pairs(Tree_BCG, coord_BCG, radius = radius)
	count_BCG_GAL = count_pairs(Tree_BCG, coord_GAL, radius = radius)
	count_BCG_FIL = count_pairs(Tree_BCG, coord_FIL, radius = radius)
	count_BCG_Awi = count_pairs(Tree_BCG, coord_Awi, radius = radius)
	count_BCG_Air = count_pairs(Tree_BCG, coord_Air, radius = radius)
	count_BCG_QSO = count_pairs(Tree_BCG, coord_QSO, radius = radius)
	count_BCG_LyA = count_pairs(Tree_BCG, coord_LyA, radius = radius)
	count_BCG__BG = count_pairs(Tree_BCG, coord__BG, radius = radius)
	count_BCG_LRG = count_pairs(Tree_BCG, coord_LRG, radius = radius)
	#
	count_GAL_GAL = count_pairs(Tree_GAL, coord_GAL, radius = radius)
	count_GAL_FIL = count_pairs(Tree_GAL, coord_FIL, radius = radius)
	count_GAL_Awi = count_pairs(Tree_GAL, coord_Awi, radius = radius)
	count_GAL_Air = count_pairs(Tree_GAL, coord_Air, radius = radius)
	count_GAL_QSO = count_pairs(Tree_GAL, coord_QSO, radius = radius)
	count_GAL_LyA = count_pairs(Tree_GAL, coord_LyA, radius = radius)
	count_GAL__BG = count_pairs(Tree_GAL, coord__BG, radius = radius)
	count_GAL_LRG = count_pairs(Tree_GAL, coord_LRG, radius = radius)
	#
	count_FIL_FIL = count_pairs(Tree_FIL, coord_FIL, radius = radius)
	count_FIL_Awi = count_pairs(Tree_FIL, coord_Awi, radius = radius)
	count_FIL_Air = count_pairs(Tree_FIL, coord_Air, radius = radius)
	count_FIL_QSO = count_pairs(Tree_FIL, coord_QSO, radius = radius)
	count_FIL_LyA = count_pairs(Tree_FIL, coord_LyA, radius = radius)
	count_FIL__BG = count_pairs(Tree_FIL, coord__BG, radius = radius)
	count_FIL_LRG = count_pairs(Tree_FIL, coord_LRG, radius = radius)
	#
	count_Awi_Awi = count_pairs(Tree_Awi, coord_Awi, radius = radius)
	count_Awi_Air = count_pairs(Tree_Awi, coord_Air, radius = radius)
	count_Awi_QSO = count_pairs(Tree_Awi, coord_QSO, radius = radius)
	count_Awi_LyA = count_pairs(Tree_Awi, coord_LyA, radius = radius)
	count_Awi__BG = count_pairs(Tree_Awi, coord__BG, radius = radius)
	count_Awi_LRG = count_pairs(Tree_Awi, coord_LRG, radius = radius)
	#
	count_Air_Air = count_pairs(Tree_Air, coord_Air, radius = radius)
	count_Air_QSO = count_pairs(Tree_Air, coord_QSO, radius = radius)
	count_Air_LyA = count_pairs(Tree_Air, coord_LyA, radius = radius)
	count_Air__BG = count_pairs(Tree_Air, coord__BG, radius = radius)
	count_Air_LRG = count_pairs(Tree_Air, coord_LRG, radius = radius)
	#
	count_QSO_QSO = count_pairs(Tree_QSO, coord_QSO, radius = radius)
	count_QSO_LyA = count_pairs(Tree_QSO, coord_LyA, radius = radius)
	count_QSO__BG = count_pairs(Tree_QSO, coord__BG, radius = radius)
	count_QSO_LRG = count_pairs(Tree_QSO, coord_LRG, radius = radius)
	#
	count_LyA_LyA = count_pairs(Tree_LyA, coord_LyA, radius = radius)
	count_LyA__BG = count_pairs(Tree_LyA, coord__BG, radius = radius)
	count_LyA_LRG = count_pairs(Tree_LyA, coord_LRG, radius = radius)
	#
	count__BG__BG = count_pairs(Tree__BG, coord__BG, radius = radius)
	count__BG_LRG = count_pairs(Tree__BG, coord_LRG, radius = radius)
	#
	count_LRG_LRG = count_pairs(Tree_LRG, coord_LRG, radius = radius)

	print( 
	'BCG', '&',
	'GAL', '&',
	'FIL', '&',
	'Awi', '&',
	'Air', '&',
	'QSO', '&',
	'LyA', '&',
	'_BG', '&',
	'LRG', '\\\\')
	print(
	N_BCG, '&',
	N_GAL, '&',
	N_FIL, '&',
	N_Awi, '&',
	N_Air, '&',
	N_QSO, '&',
	N_LyA, '&',
	N__BG, '&',
	N_LRG, '\\\\')
	print( 'BCG & ', count_BCG_BCG,'&', count_BCG_GAL,'&', count_BCG_FIL,'&', count_BCG_Awi,'&', count_BCG_Air,'&', count_BCG_QSO,'&', count_BCG_LyA,'&', count_BCG__BG,'&', count_BCG_LRG, " \\\\" )
	print( 'GAL & ', count_BCG_GAL,'&', count_GAL_GAL,'&', count_GAL_FIL,'&', count_GAL_Awi,'&', count_GAL_Air,'&', count_GAL_QSO,'&', count_GAL_LyA,'&', count_GAL__BG,'&', count_GAL_LRG, " \\\\" )
	print( 'FIL & ', count_BCG_FIL,'&', count_GAL_FIL,'&', count_FIL_FIL,'&', count_FIL_Awi,'&', count_FIL_Air,'&', count_FIL_QSO,'&', count_FIL_LyA,'&', count_FIL__BG,'&', count_FIL_LRG, " \\\\" )
	print( 'Awi & ', count_BCG_Awi,'&', count_GAL_Awi,'&', count_FIL_Awi,'&', count_Awi_Awi,'&', count_Awi_Air,'&', count_Awi_QSO,'&', count_Awi_LyA,'&', count_Awi__BG,'&', count_Awi_LRG, " \\\\" )
	print( 'Air & ', count_BCG_Air,'&', count_GAL_Air,'&', count_FIL_Air,'&', count_Awi_Air,'&', count_Air_Air,'&', count_Air_QSO,'&', count_Air_LyA,'&', count_Air__BG,'&', count_Air_LRG, " \\\\" )
	print( 'QSO & ', count_BCG_QSO,'&', count_GAL_QSO,'&', count_FIL_QSO,'&', count_Awi_QSO,'&', count_Air_QSO,'&', count_QSO_QSO,'&', count_QSO_LyA,'&', count_QSO__BG,'&', count_QSO_LRG, " \\\\" )
	print( 'LyA & ', count_BCG_LyA,'&', count_GAL_LyA,'&', count_FIL_LyA,'&', count_Awi_LyA,'&', count_Air_LyA,'&', count_QSO_LyA,'&', count_LyA_LyA,'&', count_LyA__BG,'&', count_LyA_LRG, " \\\\" )
	print( ' BG & ', count_BCG__BG,'&', count_GAL__BG,'&', count_FIL__BG,'&', count_Awi__BG,'&', count_Air__BG,'&', count_QSO__BG,'&', count_LyA__BG,'&', count__BG__BG,'&', count__BG_LRG, " \\\\" )
	print( 'LRG & ', count_BCG_LRG,'&', count_GAL_LRG,'&', count_FIL_LRG,'&', count_Awi_LRG,'&', count_Air_LRG,'&', count_QSO_LRG,'&', count_LyA_LRG,'&', count__BG_LRG,'&', count_LRG_LRG, " \\\\" )    


	print( 'BCG & ', n.round(100*count_BCG_BCG/N_BCG,1),'&', n.round(100*count_BCG_GAL/N_GAL,1),'&', n.round(100*count_BCG_FIL/N_FIL,1),'&', n.round(100*count_BCG_Awi/N_Awi,1),'&', n.round(100*count_BCG_Air/N_Air,1),'&', n.round(100*count_BCG_QSO/N_QSO,1),'&', n.round(100*count_BCG_LyA/N_LyA,1),'&',  n.round(100*count_BCG__BG/N__BG,1),'&', n.round(100*count_BCG_LRG/N_LRG,1), " \\\\" )
	print( 'GAL & ', n.round(100*count_BCG_GAL/N_BCG,1),'&', n.round(100*count_GAL_GAL/N_GAL,1),'&', n.round(100*count_GAL_FIL/N_FIL,1),'&', n.round(100*count_GAL_Awi/N_Awi,1),'&', n.round(100*count_GAL_Air/N_Air,1),'&', n.round(100*count_GAL_QSO/N_QSO,1),'&', n.round(100*count_GAL_LyA/N_LyA,1),'&',  n.round(100*count_GAL__BG/N__BG,1),'&', n.round(100*count_GAL_LRG/N_LRG,1), " \\\\" )
	print( 'FIL & ', n.round(100*count_BCG_FIL/N_BCG,1),'&', n.round(100*count_GAL_FIL/N_GAL,1),'&', n.round(100*count_FIL_FIL/N_FIL,1),'&', n.round(100*count_FIL_Awi/N_Awi,1),'&', n.round(100*count_FIL_Air/N_Air,1),'&', n.round(100*count_FIL_QSO/N_QSO,1),'&', n.round(100*count_FIL_LyA/N_LyA,1),'&',  n.round(100*count_FIL__BG/N__BG,1),'&', n.round(100*count_FIL_LRG/N_LRG,1), " \\\\" )
	print( 'Awi & ', n.round(100*count_BCG_Awi/N_BCG,1),'&', n.round(100*count_GAL_Awi/N_GAL,1),'&', n.round(100*count_FIL_Awi/N_FIL,1),'&', n.round(100*count_Awi_Awi/N_Awi,1),'&', n.round(100*count_Awi_Air/N_Air,1),'&', n.round(100*count_Awi_QSO/N_QSO,1),'&', n.round(100*count_Awi_LyA/N_LyA,1),'&',  n.round(100*count_Awi__BG/N__BG,1),'&', n.round(100*count_Awi_LRG/N_LRG,1), " \\\\" )
	print( 'Air & ', n.round(100*count_BCG_Air/N_BCG,1),'&', n.round(100*count_GAL_Air/N_GAL,1),'&', n.round(100*count_FIL_Air/N_FIL,1),'&', n.round(100*count_Awi_Air/N_Awi,1),'&', n.round(100*count_Air_Air/N_Air,1),'&', n.round(100*count_Air_QSO/N_QSO,1),'&', n.round(100*count_Air_LyA/N_LyA,1),'&',  n.round(100*count_Air__BG/N__BG,1),'&', n.round(100*count_Air_LRG/N_LRG,1), " \\\\" )
	print( 'QSO & ', n.round(100*count_BCG_QSO/N_BCG,1),'&', n.round(100*count_GAL_QSO/N_GAL,1),'&', n.round(100*count_FIL_QSO/N_FIL,1),'&', n.round(100*count_Awi_QSO/N_Awi,1),'&', n.round(100*count_Air_QSO/N_Air,1),'&', n.round(100*count_QSO_QSO/N_QSO,1),'&', n.round(100*count_QSO_LyA/N_LyA,1),'&',  n.round(100*count_QSO__BG/N__BG,1),'&', n.round(100*count_QSO_LRG/N_LRG,1), " \\\\" )
	print( 'LyA & ', n.round(100*count_BCG_LyA/N_BCG,1),'&', n.round(100*count_GAL_LyA/N_GAL,1),'&', n.round(100*count_FIL_LyA/N_FIL,1),'&', n.round(100*count_Awi_LyA/N_Awi,1),'&', n.round(100*count_Air_LyA/N_Air,1),'&', n.round(100*count_QSO_LyA/N_QSO,1),'&', n.round(100*count_LyA_LyA/N_LyA,1),'&',  n.round(100*count_LyA__BG/N__BG,1),'&', n.round(100*count_LyA_LRG/N_LRG,1), " \\\\" )
	print( ' BG & ', n.round(100*count_BCG__BG/N_BCG,1),'&', n.round(100*count_GAL__BG/N_GAL,1),'&', n.round(100*count_FIL__BG/N_FIL,1),'&', n.round(100*count_Awi__BG/N_Awi,1),'&', n.round(100*count_Air__BG/N_Air,1),'&', n.round(100*count_QSO__BG/N_QSO,1),'&', n.round(100*count_LyA__BG/N_LyA,1),'&',  n.round(100*count__BG__BG/N__BG,1),'&', n.round(100*count__BG_LRG/N_LRG,1), " \\\\" )
	print( 'LRG & ', n.round(100*count_BCG_LRG/N_BCG,1),'&', n.round(100*count_GAL_LRG/N_GAL,1),'&', n.round(100*count_FIL_LRG/N_FIL,1),'&', n.round(100*count_Awi_LRG/N_Awi,1),'&', n.round(100*count_Air_LRG/N_Air,1),'&', n.round(100*count_QSO_LRG/N_QSO,1),'&', n.round(100*count_LyA_LRG/N_LyA,1),'&',  n.round(100*count__BG_LRG/N__BG,1),'&', n.round(100*count_LRG_LRG/N_LRG,1), " \\\\" )    

	print( 'BCG & ', n.round(count_BCG_BCG/area_c,1),'&', n.round(count_BCG_GAL/area_c,1),'&', n.round(count_BCG_FIL/area_c,1),'&', n.round(count_BCG_Awi/area_c,1),'&', n.round(count_BCG_Air/area_c,1),'&', n.round(count_BCG_QSO/area_c,1),'&', n.round(count_BCG_LyA/area_c,1),'&',  n.round(count_BCG__BG/area_c,1),'&', n.round(count_BCG_LRG/area_c,1), " \\\\" )
	print( 'GAL & ', n.round(count_BCG_GAL/area_c,1),'&', n.round(count_GAL_GAL/area_c,1),'&', n.round(count_GAL_FIL/area_c,1),'&', n.round(count_GAL_Awi/area_c,1),'&', n.round(count_GAL_Air/area_c,1),'&', n.round(count_GAL_QSO/area_c,1),'&', n.round(count_GAL_LyA/area_c,1),'&',  n.round(count_GAL__BG/area_c,1),'&', n.round(count_GAL_LRG/area_c,1), " \\\\" )
	print( 'FIL & ', n.round(count_BCG_FIL/area_c,1),'&', n.round(count_GAL_FIL/area_c,1),'&', n.round(count_FIL_FIL/area_c,1),'&', n.round(count_FIL_Awi/area_c,1),'&', n.round(count_FIL_Air/area_c,1),'&', n.round(count_FIL_QSO/area_c,1),'&', n.round(count_FIL_LyA/area_c,1),'&',  n.round(count_FIL__BG/area_c,1),'&', n.round(count_FIL_LRG/area_c,1), " \\\\" )
	print( 'Awi & ', n.round(count_BCG_Awi/area_c,1),'&', n.round(count_GAL_Awi/area_c,1),'&', n.round(count_FIL_Awi/area_c,1),'&', n.round(count_Awi_Awi/area_c,1),'&', n.round(count_Awi_Air/area_c,1),'&', n.round(count_Awi_QSO/area_c,1),'&', n.round(count_Awi_LyA/area_c,1),'&',  n.round(count_Awi__BG/area_c,1),'&', n.round(count_Awi_LRG/area_c,1), " \\\\" )
	print( 'Air & ', n.round(count_BCG_Air/area_c,1),'&', n.round(count_GAL_Air/area_c,1),'&', n.round(count_FIL_Air/area_c,1),'&', n.round(count_Awi_Air/area_c,1),'&', n.round(count_Air_Air/area_c,1),'&', n.round(count_Air_QSO/area_c,1),'&', n.round(count_Air_LyA/area_c,1),'&',  n.round(count_Air__BG/area_c,1),'&', n.round(count_Air_LRG/area_c,1), " \\\\" )
	print( 'QSO & ', n.round(count_BCG_QSO/area_c,1),'&', n.round(count_GAL_QSO/area_c,1),'&', n.round(count_FIL_QSO/area_c,1),'&', n.round(count_Awi_QSO/area_c,1),'&', n.round(count_Air_QSO/area_c,1),'&', n.round(count_QSO_QSO/area_c,1),'&', n.round(count_QSO_LyA/area_c,1),'&',  n.round(count_QSO__BG/area_c,1),'&', n.round(count_QSO_LRG/area_c,1), " \\\\" )
	print( 'LyA & ', n.round(count_BCG_LyA/area_c,1),'&', n.round(count_GAL_LyA/area_c,1),'&', n.round(count_FIL_LyA/area_c,1),'&', n.round(count_Awi_LyA/area_c,1),'&', n.round(count_Air_LyA/area_c,1),'&', n.round(count_QSO_LyA/area_c,1),'&', n.round(count_LyA_LyA/area_c,1),'&',  n.round(count_LyA__BG/area_c,1),'&', n.round(count_LyA_LRG/area_c,1), " \\\\" )
	print( ' BG & ', n.round(count_BCG__BG/area_c,1),'&', n.round(count_GAL__BG/area_c,1),'&', n.round(count_FIL__BG/area_c,1),'&', n.round(count_Awi__BG/area_c,1),'&', n.round(count_Air__BG/area_c,1),'&', n.round(count_QSO__BG/area_c,1),'&', n.round(count_LyA__BG/area_c,1),'&',  n.round(count__BG__BG/area_c,1),'&', n.round(count__BG_LRG/area_c,1), " \\\\" )
	print( 'LRG & ', n.round(count_BCG_LRG/area_c,1),'&', n.round(count_GAL_LRG/area_c,1),'&', n.round(count_FIL_LRG/area_c,1),'&', n.round(count_Awi_LRG/area_c,1),'&', n.round(count_Air_LRG/area_c,1),'&', n.round(count_QSO_LRG/area_c,1),'&', n.round(count_LyA_LRG/area_c,1),'&',  n.round(count__BG_LRG/area_c,1),'&', n.round(count_LRG_LRG/area_c,1), " \\\\" )    


	print( 'BCG & ', n.round(100*count_BCG_BCG/N_BCG,1)                                                                                                                                               , " \\\\"        )
	print( 'GAL & ', n.round(100*count_BCG_GAL/N_BCG,1),'&', n.round(100*count_GAL_GAL/N_GAL,1)                                                                                                                                , " \\\\"        )
	print( 'FIL & ', n.round(100*count_BCG_FIL/N_BCG,1),'&', n.round(100*count_GAL_FIL/N_GAL,1),'&', n.round(100*count_FIL_FIL/N_FIL,1)                                                                                                                 , " \\\\"       )
	print( 'Awi & ', n.round(100*count_BCG_Awi/N_BCG,1),'&', n.round(100*count_GAL_Awi/N_GAL,1),'&', n.round(100*count_FIL_Awi/N_FIL,1),'&', n.round(100*count_Awi_Awi/N_Awi,1)                                                                                                  , " \\\\"        )
	print( 'Air & ', n.round(100*count_BCG_Air/N_BCG,1),'&', n.round(100*count_GAL_Air/N_GAL,1),'&', n.round(100*count_FIL_Air/N_FIL,1),'&', n.round(100*count_Awi_Air/N_Awi,1), '&',n.round(100*count_Air_Air/N_Air,1)                                                                      , " \\\\"      )
	print( 'QSO & ', n.round(100*count_BCG_QSO/N_BCG,1),'&', n.round(100*count_GAL_QSO/N_GAL,1),'&', n.round(100*count_FIL_QSO/N_FIL,1),'&', n.round(100*count_Awi_QSO/N_Awi,1), '&',n.round(100*count_Air_QSO/N_Air,1), '&',n.round(100*count_QSO_QSO/N_QSO,1)                                                        , " \\\\"     )
	print( 'LyA & ', n.round(100*count_BCG_LyA/N_BCG,1),'&', n.round(100*count_GAL_LyA/N_GAL,1),'&', n.round(100*count_FIL_LyA/N_FIL,1),'&', n.round(100*count_Awi_LyA/N_Awi,1), '&',n.round(100*count_Air_LyA/N_Air,1), '&',n.round(100*count_QSO_LyA/N_QSO,1), '&',n.round(100*count_LyA_LyA/N_LyA,1)                                           , " \\\\"   )
	print( ' BG & ', n.round(100*count_BCG__BG/N_BCG,1),'&', n.round(100*count_GAL__BG/N_GAL,1),'&', n.round(100*count_FIL__BG/N_FIL,1),'&', n.round(100*count_Awi__BG/N_Awi,1), '&',n.round(100*count_Air__BG/N_Air,1), '&',n.round(100*count_QSO__BG/N_QSO,1), '&',n.round(100*count_LyA__BG/N_LyA,1), '&',n.round(100*count__BG__BG/N__BG,1)              , " \\\\"  )
	print( 'LRG & ', n.round(100*count_BCG_LRG/N_BCG,1),'&', n.round(100*count_GAL_LRG/N_GAL,1),'&', n.round(100*count_FIL_LRG/N_FIL,1),'&', n.round(100*count_Awi_LRG/N_Awi,1), '&',n.round(100*count_Air_LRG/N_Air,1), '&',n.round(100*count_QSO_LRG/N_QSO,1), '&',n.round(100*count_LyA_LRG/N_LyA,1), '&',n.round(100*count__BG_LRG/N__BG,1),'&', n.round(100*count_LRG_LRG/N_LRG,1), " \\\\" )    


sys.exit()
import numpy as n 
N_BCG=165886 
N_GAL=652895 
N_FIL=346858 
N_Awi=1852429 
N_Ade=100727 
N_Air=333003 
N_QSO=2100853
N_LyA=908132
N__BG=1428835
N_LRG=2841616

count_BCG_BCG=165896 #840.4583306312561
count_BCG_GAL=158548 #862.6819236278534
count_BCG_FIL=14938 #873.3475720882416
count_BCG_Awi=1693 #933.7762260437012
count_BCG_Ade=164 #938.2043287754059
count_BCG_Air=72 #948.1866393089294
count_BCG_QSO=534 #997.4294576644897
count_BCG_LyA=0 #1017.8830695152283
count_BCG__BG=33131 #1074.1191294193268
count_BCG_LRG=19266 #1138.264091014862
count_GAL_GAL=672839 #1174.1490421295166
count_GAL_FIL=21901 #1191.033920764923
count_GAL_Awi=3178 #1286.2952873706818
count_GAL_Ade=278 #1292.9131889343262
count_GAL_Air=118 #1308.7259030342102
count_GAL_QSO=1235 #1384.568327665329
count_GAL_LyA=0 #1417.3258140087128
count_GAL__BG=48661 #1506.5008680820465
count_GAL_LRG=28876 #1609.5619473457336
count_FIL_FIL=346858 #1621.290492773056
count_FIL_Awi=4772 #1685.4730234146118
count_FIL_Ade=177 #1689.77587723732
count_FIL_Air=186 #1700.4847383499146
count_FIL_QSO=752 #1752.94411444664
count_FIL_LyA=0 #1775.7214682102203
count_FIL__BG=7986 #1838.2476873397827
count_FIL_LRG=0 #1910.2667145729065
count_Awi_Awi=1852429 #1993.85107421875
count_Awi_Ade=0 #1998.2157275676727
count_Awi_Air=95754 #2011.678432226181
count_Awi_QSO=511132 #2077.13295173645
count_Awi_LyA=95741 #2105.39630484581
count_Awi__BG=6100 #2181.1477751731873
count_Awi_LRG=18937 #2270.4367201328278
count_Ade_Ade=100727 #2274.4391927719116
count_Ade_Air=3711 #2274.7879276275635
count_Ade_QSO=12926 #2276.1570749282837
count_Ade_LyA=4165 #2276.7334587574005
count_Ade__BG=160 #2278.06334233284
count_Ade_LRG=425 #2279.9554271698
count_Air_Air=333003 #2290.6301844120026
count_Air_QSO=0 #2340.993796348572
count_Air_LyA=1 #2362.71906709671
count_Air__BG=137 #2421.9351971149445
count_Air_LRG=960 #2489.956080198288
count_QSO_QSO=2100853 #2570.3367187976837
count_QSO_LyA=0 #2604.5982933044434
count_QSO__BG=6651 #2706.2634105682373
count_QSO_LRG=25337 #2814.3820745944977
count_LyA_LyA=908132 #2845.584137439728
count_LyA__BG=0 #2933.7269032001495
count_LyA_LRG=0 #3030.0754132270813
count__BG__BG=1428835 #3213.4827976226807
count__BG_LRG=1 #3302.1417138576508
count_LRG_LRG=2841630 #3395.808817386627



print( 
'BCG', '&',
'GAL', '&',
'FIL', '&',
'Awi', '&',
'Ade', '&',
'Air', '&',
'QSO', '&',
'LyA', '&',
'_BG', '&',
'LRG', '\\\\')
print(
N_BCG, '&',
N_GAL, '&',
N_FIL, '&',
N_Awi, '&',
N_Ade, '&',
N_Air, '&',
N_QSO, '&',
N_LyA, '&',
N__BG, '&',
N_LRG, '\\\\')
print( 'BCG & ', count_BCG_BCG,'&', count_BCG_GAL,'&', count_BCG_FIL,'&', count_BCG_Awi,'&', count_BCG_Ade,'&', count_BCG_Air,'&', count_BCG_QSO,'&', count_BCG_LyA,'&', count_BCG__BG,'&', count_BCG_LRG, " \\\\" )
print( 'GAL & ', count_BCG_GAL,'&', count_GAL_GAL,'&', count_GAL_FIL,'&', count_GAL_Awi,'&', count_GAL_Ade,'&', count_GAL_Air,'&', count_GAL_QSO,'&', count_GAL_LyA,'&', count_GAL__BG,'&', count_GAL_LRG, " \\\\" )
print( 'FIL & ', count_BCG_FIL,'&', count_GAL_FIL,'&', count_FIL_FIL,'&', count_FIL_Awi,'&', count_FIL_Ade,'&', count_FIL_Air,'&', count_FIL_QSO,'&', count_FIL_LyA,'&', count_FIL__BG,'&', count_FIL_LRG, " \\\\" )
print( 'Awi & ', count_BCG_Awi,'&', count_GAL_Awi,'&', count_FIL_Awi,'&', count_Awi_Awi,'&', count_Awi_Ade,'&', count_Awi_Air,'&', count_Awi_QSO,'&', count_Awi_LyA,'&', count_Awi__BG,'&', count_Awi_LRG, " \\\\" )
print( 'Ade & ', count_BCG_Ade,'&', count_GAL_Ade,'&', count_FIL_Ade,'&', count_Awi_Ade,'&', count_Ade_Ade,'&', count_Ade_Air,'&', count_Ade_QSO,'&', count_Ade_LyA,'&', count_Ade__BG,'&', count_Ade_LRG, " \\\\" )
print( 'Air & ', count_BCG_Air,'&', count_GAL_Air,'&', count_FIL_Air,'&', count_Awi_Air,'&', count_Ade_Air,'&', count_Air_Air,'&', count_Air_QSO,'&', count_Air_LyA,'&', count_Air__BG,'&', count_Air_LRG, " \\\\" )
print( 'QSO & ', count_BCG_QSO,'&', count_GAL_QSO,'&', count_FIL_QSO,'&', count_Awi_QSO,'&', count_Ade_QSO,'&', count_Air_QSO,'&', count_QSO_QSO,'&', count_QSO_LyA,'&', count_QSO__BG,'&', count_QSO_LRG, " \\\\" )
print( 'LyA & ', count_BCG_LyA,'&', count_GAL_LyA,'&', count_FIL_LyA,'&', count_Awi_LyA,'&', count_Ade_LyA,'&', count_Air_LyA,'&', count_QSO_LyA,'&', count_LyA_LyA,'&', count_LyA__BG,'&', count_LyA_LRG, " \\\\" )
print( ' BG & ', count_BCG__BG,'&', count_GAL__BG,'&', count_FIL__BG,'&', count_Awi__BG,'&', count_Ade__BG,'&', count_Air__BG,'&', count_QSO__BG,'&', count_LyA__BG,'&', count__BG__BG,'&', count__BG_LRG, " \\\\" )
print( 'LRG & ', count_BCG_LRG,'&', count_GAL_LRG,'&', count_FIL_LRG,'&', count_Awi_LRG,'&', count_Ade_LRG,'&', count_Air_LRG,'&', count_QSO_LRG,'&', count_LyA_LRG,'&', count__BG_LRG,'&', count_LRG_LRG, " \\\\" )    


print( 'BCG & ', n.round(100*count_BCG_BCG/N_BCG,1),'&', n.round(100*count_BCG_GAL/N_GAL,1),'&', n.round(100*count_BCG_FIL/N_FIL,1),'&', n.round(100*count_BCG_Awi/N_Awi,1),'&', n.round(100*count_BCG_Ade/N_Ade,1),'&', n.round(100*count_BCG_Air/N_Air,1),'&', n.round(100*count_BCG_QSO/N_QSO,1),'&', n.round(100*count_BCG_LyA/N_LyA,1),'&',  n.round(100*count_BCG__BG/N__BG,1),'&', n.round(100*count_BCG_LRG/N_LRG,1), " \\\\" )
print( 'GAL & ', n.round(100*count_BCG_GAL/N_BCG,1),'&', n.round(100*count_GAL_GAL/N_GAL,1),'&', n.round(100*count_GAL_FIL/N_FIL,1),'&', n.round(100*count_GAL_Awi/N_Awi,1),'&', n.round(100*count_GAL_Ade/N_Ade,1),'&', n.round(100*count_GAL_Air/N_Air,1),'&', n.round(100*count_GAL_QSO/N_QSO,1),'&', n.round(100*count_GAL_LyA/N_LyA,1),'&',  n.round(100*count_GAL__BG/N__BG,1),'&', n.round(100*count_GAL_LRG/N_LRG,1), " \\\\" )
print( 'FIL & ', n.round(100*count_BCG_FIL/N_BCG,1),'&', n.round(100*count_GAL_FIL/N_GAL,1),'&', n.round(100*count_FIL_FIL/N_FIL,1),'&', n.round(100*count_FIL_Awi/N_Awi,1),'&', n.round(100*count_FIL_Ade/N_Ade,1),'&', n.round(100*count_FIL_Air/N_Air,1),'&', n.round(100*count_FIL_QSO/N_QSO,1),'&', n.round(100*count_FIL_LyA/N_LyA,1),'&',  n.round(100*count_FIL__BG/N__BG,1),'&', n.round(100*count_FIL_LRG/N_LRG,1), " \\\\" )
print( 'Awi & ', n.round(100*count_BCG_Awi/N_BCG,1),'&', n.round(100*count_GAL_Awi/N_GAL,1),'&', n.round(100*count_FIL_Awi/N_FIL,1),'&', n.round(100*count_Awi_Awi/N_Awi,1),'&', n.round(100*count_Awi_Ade/N_Ade,1),'&', n.round(100*count_Awi_Air/N_Air,1),'&', n.round(100*count_Awi_QSO/N_QSO,1),'&', n.round(100*count_Awi_LyA/N_LyA,1),'&',  n.round(100*count_Awi__BG/N__BG,1),'&', n.round(100*count_Awi_LRG/N_LRG,1), " \\\\" )
print( 'Ade & ', n.round(100*count_BCG_Ade/N_BCG,1),'&', n.round(100*count_GAL_Ade/N_GAL,1),'&', n.round(100*count_FIL_Ade/N_FIL,1),'&', n.round(100*count_Awi_Ade/N_Awi,1),'&', n.round(100*count_Ade_Ade/N_Ade,1),'&', n.round(100*count_Ade_Air/N_Air,1),'&', n.round(100*count_Ade_QSO/N_QSO,1),'&', n.round(100*count_Ade_LyA/N_LyA,1),'&',  n.round(100*count_Ade__BG/N__BG,1),'&', n.round(100*count_Ade_LRG/N_LRG,1), " \\\\" )
print( 'Air & ', n.round(100*count_BCG_Air/N_BCG,1),'&', n.round(100*count_GAL_Air/N_GAL,1),'&', n.round(100*count_FIL_Air/N_FIL,1),'&', n.round(100*count_Awi_Air/N_Awi,1),'&', n.round(100*count_Ade_Air/N_Ade,1),'&', n.round(100*count_Air_Air/N_Air,1),'&', n.round(100*count_Air_QSO/N_QSO,1),'&', n.round(100*count_Air_LyA/N_LyA,1),'&',  n.round(100*count_Air__BG/N__BG,1),'&', n.round(100*count_Air_LRG/N_LRG,1), " \\\\" )
print( 'QSO & ', n.round(100*count_BCG_QSO/N_BCG,1),'&', n.round(100*count_GAL_QSO/N_GAL,1),'&', n.round(100*count_FIL_QSO/N_FIL,1),'&', n.round(100*count_Awi_QSO/N_Awi,1),'&', n.round(100*count_Ade_QSO/N_Ade,1),'&', n.round(100*count_Air_QSO/N_Air,1),'&', n.round(100*count_QSO_QSO/N_QSO,1),'&', n.round(100*count_QSO_LyA/N_LyA,1),'&',  n.round(100*count_QSO__BG/N__BG,1),'&', n.round(100*count_QSO_LRG/N_LRG,1), " \\\\" )
print( 'LyA & ', n.round(100*count_BCG_LyA/N_BCG,1),'&', n.round(100*count_GAL_LyA/N_GAL,1),'&', n.round(100*count_FIL_LyA/N_FIL,1),'&', n.round(100*count_Awi_LyA/N_Awi,1),'&', n.round(100*count_Ade_LyA/N_Ade,1),'&', n.round(100*count_Air_LyA/N_Air,1),'&', n.round(100*count_QSO_LyA/N_QSO,1),'&', n.round(100*count_LyA_LyA/N_LyA,1),'&',  n.round(100*count_LyA__BG/N__BG,1),'&', n.round(100*count_LyA_LRG/N_LRG,1), " \\\\" )
print( ' BG & ', n.round(100*count_BCG__BG/N_BCG,1),'&', n.round(100*count_GAL__BG/N_GAL,1),'&', n.round(100*count_FIL__BG/N_FIL,1),'&', n.round(100*count_Awi__BG/N_Awi,1),'&', n.round(100*count_Ade__BG/N_Ade,1),'&', n.round(100*count_Air__BG/N_Air,1),'&', n.round(100*count_QSO__BG/N_QSO,1),'&', n.round(100*count_LyA__BG/N_LyA,1),'&',  n.round(100*count__BG__BG/N__BG,1),'&', n.round(100*count__BG_LRG/N_LRG,1), " \\\\" )
print( 'LRG & ', n.round(100*count_BCG_LRG/N_BCG,1),'&', n.round(100*count_GAL_LRG/N_GAL,1),'&', n.round(100*count_FIL_LRG/N_FIL,1),'&', n.round(100*count_Awi_LRG/N_Awi,1),'&', n.round(100*count_Ade_LRG/N_Ade,1),'&', n.round(100*count_Air_LRG/N_Air,1),'&', n.round(100*count_QSO_LRG/N_QSO,1),'&', n.round(100*count_LyA_LRG/N_LyA,1),'&',  n.round(100*count__BG_LRG/N__BG,1),'&', n.round(100*count_LRG_LRG/N_LRG,1), " \\\\" )    


# estimate area of the eROSITA survey :
# about 1e6 full sky randoms :
area_FS = 129600/n.pi
from astropy.coordinates import SkyCoord
N_data = 1e7 
size = int(N_data) 
uu = n.random.uniform(size=size)
dec = n.arccos(1 - 2 * uu) * 180 / n.pi - 90.
ra = n.random.uniform(size=size) * 2 * n.pi * 180 / n.pi
#conversion to other coordinates
coords = SkyCoord(ra, dec, unit='deg', frame='icrs')
g_lat = coords.galactic.b.value
g_lon = coords.galactic.l.value
b_ecl = coords.barycentrictrueecliptic.lat.value
l_ecl = coords.barycentrictrueecliptic.lon.value

b10 = (abs(g_lat)>10)
erosita = (b10) & (g_lon>180) 
erosita_dec20 = (b10) & (g_lon>180) & (dec<20)
erosita_dec5 = (b10) & (g_lon>180) & (dec<5)
erosita_deep = (erosita_dec20) & (abs(b_ecl)>80)
erosita_wide = (erosita_dec20) & (abs(b_ecl)<80)
common_area = lambda ra, dec : (ra>160)&(ra<200)&(dec>-20)&(dec<-10)
cca = common_area(ra,dec)
nl = lambda sel : len(sel.nonzero()[0])
print(area_FS * nl(b10) / N_data           )
print(area_FS * nl(erosita) / N_data       )
print(area_FS * nl(erosita_dec20) / N_data )
print(area_FS * nl(erosita_dec5) / N_data  )
print(area_FS * nl(erosita_wide) / N_data  )
print(area_FS * nl(erosita_deep) / N_data  )
print(area_FS * nl(cca) / N_data  )

area_b10 = area_FS * nl(b10) / N_data           
area_erosita = area_FS * nl(erosita) / N_data       
area_erosita_dec20 = area_FS * nl(erosita_dec20) / N_data 
area_erosita_dec05 = area_FS * nl(erosita_dec5) / N_data  
area_erosita_dec20_wide = area_FS * nl(erosita_wide) / N_data  
area_erosita_dec20_deep = area_FS * nl(erosita_deep) / N_data  

print("BCG", N_BCG, n.round(area_erosita_dec20,1), "=", n.round(N_BCG / area_erosita_dec20,1) , "/deg2")
print("GAL", N_GAL, n.round(area_erosita_dec20,1), "=", n.round(N_GAL / area_erosita_dec20,1) , "/deg2")
print("FIL", N_FIL, n.round(area_erosita_dec20,1), "=", n.round(N_FIL / area_erosita_dec20,1) , "/deg2")
print("WIDE", N_Awi, n.round(area_erosita_dec20_wide,1), "=", n.round(N_Awi / area_erosita_dec20_wide,1) , "/deg2")
print("DEEP", N_Ade, n.round(area_erosita_dec20_deep,1), "=", n.round(N_Ade / area_erosita_dec20_deep,1) , "/deg2")
print("IR", N_Air, n.round(area_erosita_dec20,1), "=", n.round(N_Air / area_erosita_dec20,1) , "/deg2")

