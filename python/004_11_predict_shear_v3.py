"""
Each files has the structure:
— ‘Lproj' # line of sight projection length in kph/h
— ‘redshift’ # redshift of the snapshot 
— 'resolution [kpc/h]’ # width-length of one pixel in kpc/h 
— ‘halos’
    |— ‘a number for each halo’
        |— 'M200c', 'M200m', 'M500c', ‘M500m’ # mass in the grav-only run in 10^10 Msol/h
        |— 'hydro_M200c', 'hydro_M200m', 'hydro_M500c', ‘hydro_M500m’ #  mass in the hydro run in 10^10 Msol/h
        |— ‘has_profile’ # bool flagging if this cluster has a profile
        |— ‘dm_Sigma’ # projected mass in Gravity-only, 10^10 Msol/h (so to get a surface density, divide this by the bin area). Also note, the z~0.78 snapshot has not grav-only maps
        |— ‘hydro_Sigma’ # projected mass in hydro, 10^10 Msol/h


step 1 : attach (resscale) to a DM halo in the light cone
step 2 : predict reduced shear profile (DES, HSC, KIDS, ...)
         https://arxiv.org/pdf/2103.16212.pdf offers a complete list of what is known to date

python 004_11_predict_shear_v3.py 0
python 004_11_predict_shear_v3.py 1
python 004_11_predict_shear_v3.py 2
python 004_11_predict_shear_v3.py 3


"""

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt


import h5py

from scipy.special import erf
from astropy.table import Table, Column, vstack, hstack
import sys, os, time
import astropy.units as u
import astropy.io.fits as fits
import numpy as n
import numpy as np
from scipy.interpolate import interp1d
from scipy.interpolate import interp2d

from lenspack.utils import sigma_critical

print('CREATES WL profiles for eROSITA CLUSTER')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

i_slice = int(sys.argv[1])

fig_dir = os.path.join( os.environ['GIT_AGN_MOCK'], 'figures/WL' )

env = "UNIT_fA1i_DIR"
root_dir = os.path.join(os.environ[env])
path_2_catalog = os.path.join(root_dir, "UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS_Sept2021.fits")
path_2_out = os.path.join(root_dir, "UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS_Sept2021_withShear_v3_zSlice_"+str(i_slice)+".fits")

t_1 = Table.read(path_2_catalog)
print(path_2_catalog, 'opened')

s_area = ( abs( t_1['DEC'] ) < 80 ) & ( abs(t_1['g_lat']) > 10 ) & ( t_1['redshift_R'] > 0.05 ) & ( t_1['redshift_R'] < 1.0 )
s_1 = ( s_area ) & ( t_1['HALO_pid']==-1 ) & ( t_1['galaxy_SMHMR_mass']>10. ) & ( n.log10(t_1['HALO_Mvir']) > 13 )  & ( n.log10(t_1['HALO_M500c']) > 14.0 ) # & ( t_1['detectable'] )
t_2 = t_1[s_1]
print('N in catalogue = ', len(t_1))
print('Select: distinct haloes with stellar mass>1e10 & Mvir>1e13 & M500c>1e14 & detectable by eROSITA (eRASS8). Equatorial area selected : |Glat|>10 & |dec<10|')
print('N clusters of interest = ', len(t_2))

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
# UNIT cosmology setup
cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)

# Magneticum cosmology setup
cosmoBox1 = FlatLambdaCDM(H0=70.4 * u.km / u.s / u.Mpc, Om0=0.272)
h = 0.704
hz = lambda redshift : cosmoBox1.H( redshift ).value/100

#
map_unit = 1e10 # Msun/h
#
X_res = 20. # kpc/h

path_2_WL_3_0 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_052_Rotation0.fits"
path_2_WL_2_0 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_072_Rotation0.fits"
path_2_WL_1_0 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_096_Rotation0.fits"
path_2_WL_0_0 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_116_Rotation0.fits"

path_2_WL_3_1 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_052_Rotation1.fits"
path_2_WL_2_1 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_072_Rotation1.fits"
path_2_WL_1_1 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_096_Rotation1.fits"
path_2_WL_0_1 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_116_Rotation1.fits"

path_2_WL_3_2 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_052_Rotation2.fits"
path_2_WL_2_2 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_072_Rotation2.fits"
path_2_WL_1_2 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_096_Rotation2.fits"
path_2_WL_0_2 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_116_Rotation2.fits"

# at redshifts 0.25, 0.48, 0.78, 1.18

z0 = 0.25208907
z1 = 0.47019408
z2 = 0.78263327
z3 = 1.17889506

# reads all rotations
if i_slice==0:
    ts0 = vstack(( Table.read(path_2_WL_0_0), Table.read(path_2_WL_0_1), Table.read(path_2_WL_0_2) ))
if i_slice==1:
    ts0 = vstack(( Table.read(path_2_WL_1_0), Table.read(path_2_WL_1_1), Table.read(path_2_WL_1_2) ))
if i_slice==2:
    ts0 = vstack(( Table.read(path_2_WL_2_0), Table.read(path_2_WL_2_1), Table.read(path_2_WL_2_2) ))
if i_slice==3:
    ts0 = vstack(( Table.read(path_2_WL_3_0), Table.read(path_2_WL_3_1), Table.read(path_2_WL_3_2) ))

ts0 = ts0[n.argsort(ts0['M500c'])]
print('mass maps loaded:', path_2_WL_0_0, path_2_WL_0_1, path_2_WL_0_2)
# MATCHING

# slice the lightcone to assign to the closest redshift snapshot
print('retrieve clusters within z<', (z0 + z1)/2)
if i_slice==0:
    sel0 = ( t_2['redshift_R'] <= (z0 + z1)/2 )
if i_slice==1:
    sel0 = ( t_2['redshift_R'] > (z0 + z1)/2 ) & ( t_2['redshift_R'] <= (z1 + z2)/2 )
if i_slice==2:
    sel0 = ( t_2['redshift_R'] > (z1 + z2)/2 ) & ( t_2['redshift_R'] <= (z2 + z3)/2 )
if i_slice==3:
    sel0 = ( t_2['redshift_R'] > (z2 + z3)/2 )

if i_slice==0:
    z_snap = z0
if i_slice==1:
    z_snap = z1
if i_slice==2:
    z_snap = z2
if i_slice==3:
    z_snap = z3



t = t_2[sel0]
print('N clusters in that range', len(t))

print('matches with sigma -- M relation')
# use M-sigma relation to find the right halo at a different redshift
from colossus.cosmology import cosmology
my_CM = {'flat': True, 'H0':70.4, 'Om0': 0.272, 'Ob0': 0.0457, 'sigma8': 0.809, 'ns': 0.963}
CM = cosmology.setCosmology('my_cosmo', my_CM)
print(CM)
from colossus.lss import mass_function
from colossus.lss import peaks
nu = n.arange(0.4, 5.0, 0.01)
M = peaks.massFromPeakHeight(nu, z_snap)
deltac = peaks.collapseOverdensity(corrections=True, z=z_snap)
sigma_m = deltac/nu
itp_z_snap = interp1d(sigma_m, M)

peak_heights_z = peaks.peakHeight(t['HALO_M500c']*0.6777, t['redshift_R'])
deltac_z = peaks.collapseOverdensity(corrections=True, z=t['redshift_R'])
sigma_m_mock = deltac_z/peak_heights_z
m_z_snap = itp_z_snap(sigma_m_mock)

# id match in M500c
iX_res_sort = n.searchsorted( n.log10( ts0['M500c']*1e10 / hz(z_snap) ), n.log10(m_z_snap) )
iX_res_sort[iX_res_sort==len(ts0['M500c'])] = len(ts0['M500c']) - 1

source_density = 10 # arcmin-2
sigma_ne = 0.3 / source_density
#
# make sure all profiles are used not the sorted ones 
# 0,1,2
# 

##
#
# quantities for the eROSITA mock halo catalogue
#
##

##
#
# RICHNESS
#
##
print('Computes richness')

from scipy.stats import norm
ln_A_lambda = n.log( 76.9 ) # N(78.5, 8.2^2)
B_lambda = 1.020            # N(1.02, 0.08^2)
C_lambda = 0.29             # N(0.29, 0.27^2)
sigma_lambda = 0.23         # lnSigma_lambda : N(ln(0.23, 0.16^2)  
# Should we draw the parameters ? No, but we could do different flavours of it.

avg_lambda = lambda M200c, zz : ln_A_lambda + B_lambda * n.log( M200c / 3e14 ) + C_lambda * n.log ( cosmoBox1.efunc(zz) / cosmoBox1.efunc(0.6) )
sig2_tot = lambda M200c, zz : n.e**( 2*n.log(sigma_lambda) ) + (n.e**(avg_lambda(M200c, zz)) - 1 ) / (n.e**( 2*avg_lambda(M200c, zz) ) )
rvs_lam = lambda M200c, zz : n.e**(norm.rvs(loc=avg_lambda(M200c, zz), scale=sig2_tot(M200c, zz), size=1 ))[0]

R_lambda = lambda richness : (richness/100)**0.2 # Mpc/h

richness = n.array([ rvs_lam(m2, z2) for m2, z2 in zip(t['HALO_M200c']*0.6777, t['redshift_R']) ])
t['lambda'] = richness
t['R_lambda'] = R_lambda(richness)

##
#
# MIS-CENTERING
#
##
print('computes mis-centering')
from scipy.stats import rayleigh
from scipy.stats import rv_histogram
# Bleem 2020 with a Gama distribution
# B20_rho = 0.87
# B20_sig = 0.12
# B20_tau = 0.69
# G20 parameters (mean)
rho  = 0.63 # N(0.63, 0.06^2)
sig0 = 0.07 # N(0.07, 0.02^2)
sig1 = 0.25 # N(0.25, 0.07^2)
# Look at I-non's paper to decide the functional form and parameters

rvs0 = rayleigh.rvs(scale=sig0, size = 1000000 )
rvs1 = rayleigh.rvs(scale=sig1, size = 1000000 )
out0, bins0 = n.histogram(rvs0, bins = 100000)
out1 = n.histogram(rvs1, bins=bins0)[0]
pds_hist = (out0).astype('float')*rho+(1-rho)*(out1).astype('float')
hist_dist = rv_histogram((pds_hist, bins0))

Rmis = hist_dist.rvs(size=len(t)) * t['R_lambda']
t['Rmis'] = Rmis

t['Rmis_phi'] = 2*np.pi*np.random.random(len(t))

# draw a uniform RVS
# if smaller than rho => component 0 draw R0, else component 1 draw R0
 

##
#
# Source redshift distribution
#
##
print('computes Source redshift distribution')
z_s0 = 0.2
delta_beta = 0.025
sigma_beta = lambda z_cl : 0.02 * (1+z_cl**2)**2 / 1.49**2
P_zs = lambda z_s : 0.5 * z_s0 ** -3 * z_s**2 * n.e**( - z_s / z_s0 )
#
# to be updated with HSC S19A, DES Yr3, KIDS DR4.1 N(z) in due time
# in ~ 1 month
# 
z_array = n.arange(0.001, 2, 0.001)
z_bins = n.hstack(( z_array - 0.001/2., z_array.max() + 0.001/2. ))
PDZ = P_zs(z_array)
hist_dist_z = rv_histogram((PDZ, z_bins))
rd_zs = hist_dist_z.rvs(size=1000000)

mean_zs = n.array([  n.mean(rd_zs[rd_zs>zCL+0.1]) for zCL in t['redshift_R'] ])
t['mean_zs'] = mean_zs


##
#
# Kappa and Gamma maps
#
##

r_bins = np.insert(np.logspace(-2, 0.6, 27), 0, 0)
phi_bins = np.linspace(-np.pi, np.pi+0.01, num=25)

def KaiserSquires(Sigma):
    kappa_tilde = np.fft.fft2(Sigma)
    k = np.fft.fftfreq(kappa_tilde.shape[0])
    oper_1  = - 1./(k[:, None]**2 + k[None, :]**2) * (k[:, None]**2 - k[None, :]**2)
    oper_2  = - 2./(k[:, None]**2 + k[None, :]**2) * k[:, None]*k[None, :]
    oper_1[0, 0] = -1
    oper_2[0, 0] = -1
    e1_tilde = oper_1*kappa_tilde
    e2_tilde = oper_2*kappa_tilde
    e1 = np.fft.ifft2(e1_tilde).real
    e2 = np.fft.ifft2(e2_tilde).real
    return e1, e2

def getTangetial(e1, e2, center, X_res=20./1000.):
    n = e1.shape[0]
    xx = np.arange(-n/2, n/2)*X_res
    XX, YY = np.meshgrid(xx, xx)
    center_1, center_2 = center
    from_cent_1 = XX - center_1
    from_cent_2 = YY - center_2
    angle = -np.sign(from_cent_2)*np.arccos(from_cent_1/np.sqrt(from_cent_1**2+from_cent_2**2))
    radius = np.sqrt(from_cent_1**2+from_cent_2**2)
    angle[np.isnan(angle)] = 0
    et = - e1*np.cos(2*angle) - e2*np.sin(2*angle)
    ex = + e1*np.sin(2*angle) - e2*np.cos(2*angle)
    return et, ex, radius, angle

# mean projected density at that redshift for this setup
l_proj = 20 * u.Mpc # Mpc/h
rho_M_z = cosmoBox1.Om(z_snap)*(1+z_snap)**3 * cosmoBox1.critical_density(z_snap).to(u.Msun * u.Mpc**-3)
floor = 2*l_proj*rho_M_z

# converts HYDRO map into a kappa map
kappa_HD_map = n.divide(ts0['hydro_Sigma'], (X_res*0.001)**2 / 1e10) - floor.value
#kappa_DM_map = n.divide(ts0['dm_Sigma'], (X_res*0.001)**2 / 1e10) - floor.value

t['sigma_critical_raw'] = n.zeros(len(t))
t['sigma_critical'] = n.zeros(len(t))
t['kappa_profile']= n.zeros( (len(t), len(r_bins) -1 ) )
t['gammat_profile']= n.zeros( (len(t), len(r_bins) -1 ) )
t['g_t'] = n.zeros( (len(t), len(r_bins) -1 ) )
t['g_t_measured'] = n.zeros( (len(t), len(r_bins) -1 ) )
t['g_t_measured_diluted'] = n.zeros( (len(t), len(r_bins) -1 ) )


t0 = time.time()
for input_id, clu_id in zip(n.arange(len(t)), iX_res_sort):
    print(input_id, clu_id, (time.time()-t0)/input_id)
    ##
    #
    # Critical surface density
    #
    ##
    print('Critical surface density')
    z_s_samp = n.arange(t['redshift_R'][input_id]+0.1, 1.5, 0.01)
    pdf_values = hist_dist_z.pdf( z_s_samp )
    sig_crit = n.sum( n.array([ sigma_critical( t['redshift_R'][input_id], z_s_samp_i, cosmoBox1).to( u.solMass / (u.Mpc*u.Mpc) ).value for z_s_samp_i in z_s_samp ]) * pdf_values ) / pdf_values.sum()
    t['sigma_critical_raw'][input_id] = sig_crit
    t['sigma_critical'][input_id] = sig_crit * ( 1 + delta_beta + sigma_beta(t['redshift_R'][input_id]))

    ##
    #
    #
    #
    # reduced shear profile
    #
    #
    ##
    e1_map, e2_map = KaiserSquires(kappa_HD_map[clu_id])
    center_1 = t['Rmis'] [input_id] * np.cos(t['Rmis_phi'] [input_id] )
    center_2 = t['Rmis'] [input_id] * np.sin(t['Rmis_phi'] [input_id] )
    # get the shear map in R, Phi space
    et_map, ex_map, radius_map, angle_map = getTangetial(e1_map, e2_map, [center_1, center_2], X_res=X_res*0.001)
    ii_r = np.digitize(radius_map, r_bins)-1
    ii_phi = np.digitize(angle_map, phi_bins)-1
    _N  = np.zeros(( len(r_bins)-1, len(phi_bins)-1))
    _K  = np.zeros(( len(r_bins)-1, len(phi_bins)-1))
    _GT = np.zeros(( len(r_bins)-1, len(phi_bins)-1))
    for i_r, i_phi, kk, gg in zip(ii_r.flatten(), ii_phi.flatten(), kappa_HD_map[clu_id].flatten(), et_map.flatten()):
        #print(i_r, i_phi, kk, gg )
        if i_r<len(r_bins)-1:
            _N[i_r, i_phi] += 1
            _K[i_r, i_phi] += kk
            _GT[i_r, i_phi] += gg
    # final m,aps
    kappa_map_radial = _K / _N /  t['sigma_critical'][input_id]
    gammat_map_radial = _GT / _N /  t['sigma_critical'][input_id]
    kappa_map_radial[n.isnan(kappa_map_radial)]=0
    gammat_map_radial[n.isnan(gammat_map_radial)]=0
    # integrate phi
    delta_phi = phi_bins[1:] - phi_bins[:-1]
    reduced_shear_map = n.divide( gammat_map_radial, 1 - kappa_map_radial )
    reduced_shear_profile = n.sum(reduced_shear_map * delta_phi, axis = 1) / ( 2. * n.pi )
    t['kappa_profile'][input_id]  = n.sum(kappa_map_radial * delta_phi, axis = 1) / ( 2. * n.pi )
    t['gammat_profile'][input_id] = n.sum(gammat_map_radial * delta_phi, axis = 1)/ ( 2. * n.pi )
    t['g_t'][input_id] = reduced_shear_profile
    m_shear_bias = 0.01
    sigma_m = 0.01
    t['g_t_measured'][input_id]       = ( 1+ m_shear_bias + sigma_m ) * t['g_t'] [input_id]
    A_fcl = 0.3
    B_fcl = 0.15
    c_fcl = 2.
    f_bar = lambda X : X
    f_cl = lambda radius, richness, R_lambda : A_fcl * ( richness / 67 ) ** B_fcl
    f_cl = 1.
    t['g_t_measured_diluted'][input_id] = ( f_cl ) *t['g_t_measured'][input_id]



# Add uncertainties and scatter the prediction !

# Average to emulate stacks

t.write(path_2_out, overwrite = True)

