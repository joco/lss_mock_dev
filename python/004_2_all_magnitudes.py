"""
What it does
------------

Add realistic magnitudes to eROSITA cluster galaxies

Command to run
--------------

python3 005_2_all_magnitudes.py environmentVAR 

"""

#import matplotlib
#matplotlib.use('Agg')
#matplotlib.rcParams.update({'font.size': 14})
#import matplotlib.pyplot as p
from sklearn.neighbors import BallTree

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import extinction

import sys
import os
import time
from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column
from scipy.optimize import curve_fit
import astropy.io.fits as fits
import numpy as n

import glob
from scipy.stats import scoreatpercentile
import pandas as pd  
from scipy.special import erf
from astropy.coordinates import SkyCoord
import astropy.constants as cc
from matplotlib.path import Path 
from astropy import units
import pymangle


print('CREATES 4MOST FILES')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()
nl = lambda selection : len(selection.nonzero()[0])


MASKDIR = os.path.join(os.environ['GIT_AGN_MOCK'],'data', 'masks')

env = sys.argv[1] #'MD10'
path_2_cat = os.path.join(os.environ[env], env + '_eRO_CLU_b8_CM_0_pixS_20.0_CGAL.fits')
path_2_out_GAL = os.path.join(os.environ[env], env + '_eRO_CLU_b8_CM_0_pixS_20.0_4MOST_GAL.fits')
path_2_out_BCG = os.path.join(os.environ[env], env + '_eRO_CLU_b8_CM_0_pixS_20.0_4MOST_BCG.fits')
path_2_out_ALL = os.path.join(os.environ[env], env + '_eRO_CLU_b8_CM_0_pixS_20.0_4MOST_ALL.fits')
print('opens', path_2_cat)
t_survey = Table.read(path_2_cat)

maximum_Z = 1.0 # n.max(t_kids['zred'])+0.05
area_erosita = ( t_survey['DEC'] > -80. ) & ( t_survey['DEC'] < 5. ) & ( abs( t_survey['g_lon'] ) > 180. ) & ( abs( t_survey['g_lat'] ) > 10. ) 
area_all_erosita = ( abs( t_survey['g_lon'] ) > 180. ) & ( abs( t_survey['g_lat'] ) > 10. ) 
erosita_depth = ( n.log10(t_survey['HOST_CLUSTER_FX_soft'])> t_survey['HOST_flux_limit_eRASS8_pt'] ) 
erosita_deeper = ( n.log10(t_survey['HOST_CLUSTER_FX_soft'])> t_survey['HOST_flux_limit_eRASS8_pt']-1 ) 
keep = ( t_survey['redshift_R'] < maximum_Z ) & ( t_survey['galaxy_SMHMR_mass'] > 7 ) & ( area_erosita ) & ( erosita_depth )
#t_survey_all = t_survey[( t_survey['galaxy_SMHMR_mass'] > 7 ) & ( area_all_erosita ) & ( erosita_deeper ) ]
t_survey = t_survey[keep]

if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT


zs = n.arange(0.0000001, 7.1, 0.001)
dm_itp = interp1d(zs, cosmo.distmod(zs).value)

# Match to AGN catalogues 
print('load and match to AGN catalogues')
agn = Table.read('/data42s/comparat/firefly/mocks/2021-04/QMOST/S6_4MOST_ALL_SNR3_IR215.fit.gz')
qso = Table.read('/data42s/comparat/firefly/mocks/2021-04/QMOST/QSO_4MOST.fits.gz')

max_distance = 0.1 / 3600. * n.pi / 180.
deg_to_rad = n.pi/180.
coord_CGAL = deg_to_rad * n.transpose([t_survey['DEC'], t_survey['RA'] ])
coord_AGN = deg_to_rad * n.transpose([agn['DEC'], agn['RA'] ])
coord_QSO = deg_to_rad * n.transpose([qso['DEC'], qso['RA'] ])

Tree_GAL = BallTree(coord_CGAL, metric='haversine')
Tree_AGN = BallTree(coord_AGN, metric='haversine')
Tree_QSO = BallTree(coord_QSO, metric='haversine')

dist_agn, idx_agn = Tree_AGN.query(coord_CGAL, return_distance=True)
dist_qso, idx_qso = Tree_QSO.query(coord_CGAL, return_distance=True)
match_agn = idx_agn[ ( dist_agn < max_distance ) ]
match_qso = idx_qso[ ( dist_qso < max_distance ) ]
match_agn_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_agn )) < max_distance ) ]
match_qso_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_qso )) < max_distance ) ]
print(len(match_agn_d1_position), 'AGN matches')
print(len(match_qso_d1_position), 'QSO matches')
# If type 21, then leave them in
# else removes the lines
# QSO can be directly removes (only type 1)
matched_types = agn['agn_type'][match_agn]
# to remove
blu_agn = (matched_types ==11)| (matched_types==12)
red_agn = (blu_agn == False)
idx_remove = n.hstack(( match_agn_d1_position[blu_agn], match_qso_d1_position ))
# the ones to keep
idx_red_agn = match_agn_d1_position[red_agn]
# get the ones that are exactly referring to the same halo :
same_halo = (t_survey[idx_red_agn]['HALO_Mvir']/agn[match_agn[red_agn]]['HALO_Mvir']==1)
print(nl(same_halo), 'red AGN are kept')
# these will see their values changed to the AGN value
# t_survey[idx_red_agn[same_halo]] <<== agn[match_agn[red_agn][same_halo]]


#t_survey[idx_red_agn[same_halo]]['HALO_Mvir']/agn[match_agn[red_agn][same_halo]]['HALO_Mvir']


nl = lambda sel : len(sel.nonzero()[0])

print('load kids eromapper catalogue')
path_2_kids = os.path.join(os.environ['MD10'], '4most_s8_kidsdr4.lsdr9.eromapper.fits') 
t_kids = Table.read(path_2_kids)

print('filtering maximum redshift')

dup_type = (t_kids['lsdr9_TYPE']=="DUP" )
dash_type = (t_kids['lsdr9_TYPE']=="-" ) | (t_kids['lsdr9_TYPE']=='-  ')
keep_kids = (dup_type==False) & (dash_type==False) & (t_kids['zred']<1.1)
t_kids = t_kids[keep_kids]
print(n.unique(t_kids['lsdr9_TYPE']))

z0,z1,b0 = n.loadtxt(os.path.join( os.environ['GIT_AGN_MOCK'], 'data', 'logMs-MK-fits', 'look-up-table.txt'), unpack=True)
fun = lambda x, b : -2.15*x+b

sub_survey_names = n.array([ 'cluster_BCG', 'cluster_redGAL', 'filament_GAL'])
N_subsurvey = {'cluster_BCG':1, 'cluster_redGAL':2, 'filament_GAL':3}
priority_values = {'cluster_BCG':100, 'cluster_redGAL':99, 'filament_GAL':98}
ruleset_values = {'cluster_BCG':"ClusBCG", 'cluster_redGAL':"RedGAL", 'filament_GAL':"RedGAL"}

#def compute_kmag(t_survey):
	#if 'K_mag_abs' not in t_survey.columns:
		#t_survey.add_column(Column(name='K_mag_abs', data=n.zeros_like(t_survey['SMHMR_mass']), unit='mag'))

	#for zmin, zmax, p_b in zip(z0, z1, b0):
		##print(zmin, zmax, p_b)
		#s_gal  = (t_survey['redshift_R']>=zmin)  & (t_survey['redshift_R']<=zmax) 
		#mag = lambda x : fun( x, p_b)
		#t_survey ['K_mag_abs'][s_gal]  = mag(t_survey ['SMHMR_mass'][s_gal])

	#t_survey ['K_mag_abs']+=norm.rvs(loc=0, scale=0.15, size=len(t_survey['redshift_R']))
	#return t_survey


def add_magnitudes_direct_match(t_survey, flag = '', t_kids=t_kids):
	# simulated quantities
	sim_redshift = t_survey['redshift_R']
	sim_k_mag = t_survey['K_mag_abs']+dm_itp(t_survey['redshift_R'])
	# quantities in KIDS
	if flag =="":
		s_kids = (t_kids['zred']<=n.max(sim_redshift)) & (t_kids['zred']>=n.min(sim_redshift)) 
	else:
		s_kids = (t_kids[flag]) & (t_kids['zred']<=n.max(sim_redshift)) & (t_kids['zred']>=n.min(sim_redshift)) 
	t_kids2 = t_kids[s_kids]
	kids_k_mag = t_kids2['rtot']-(t_kids2['ri']+t_kids2['iz']+t_kids2['zy']+t_kids2['yj']+t_kids2['jh']+t_kids2['hks'])
	kids_photoz = t_kids2['zred']
	# input kids into a tree
	Tree_kids = BallTree(n.transpose([kids_photoz, kids_k_mag]))
	DATA = n.transpose([sim_redshift, sim_k_mag])
	kids_ID = n.arange(len(t_kids2['rtot']))
	ids_out = Tree_kids.query(DATA, k=1, return_distance = False)
	ids = n.hstack((ids_out))
	id_to_map = kids_ID[ids]
	columns_to_add = n.array(['rtot'          
								, 'rfib'          
								, 'ug'            
								, 'gr'            
								, 'ri'            
								, 'iz'            
								, 'zy'            
								, 'yj'            
								, 'jh'            
								, 'hks'           
								, 'lsdr9_TYPE'    
								, 'lsdr9_RA'      
								, 'lsdr9_DEC'     
								, 'lsdr9_SHAPE_R' 
								, 'lsdr9_SHAPE_E1'
								, 'lsdr9_SHAPE_E2'
								, 'lsdr9_SERSIC'  
								, 'lsdr9_MASKBITS'])
	print('outputing results')
	for el in columns_to_add :
		if el in t_survey.columns :
			t_survey[el] = t_kids2[el][id_to_map]
		else:
			t_survey.add_column(Column(name=el, data=t_kids2[el][id_to_map]))
	return t_survey 



def add_4most_columns(subsurvey , t_survey  ):
	# templates redshift bins
	z_all = n.hstack(( 0., n.arange(0.3, 3., 0.2), 3.5, 4.5, 6. ))
	zmins = z_all[:-1]
	zmaxs = z_all[1:]
	N_obj = len(t_survey)
	#  limit size of the string columns to the size of the longer string in the corresponding columns. 
	# 'NAME':str, max 256 char
	N1 = n.arange(len(t_survey['ebv']))
	id_list = N_subsurvey[subsurvey]*1e10 +  N1
	NAME = n.array([ str(int(el)).zfill(11) for el in id_list ])
	t_survey.add_column(Column(name='NAME', data=NAME, unit=''))
	# 'RA':n.float64, 1D
	# 'DEC':n.float64, 1D
	# 'PMRA':n.float32, 1E
	# 'PMDEC':n.float32, 1E
	# 'EPOCH':n.float32, 1E
	PMRA = n.zeros(N_obj)
	t_survey.add_column(Column(name='PMRA', data=PMRA, unit='mas/yr'))
	PMDEC = n.zeros(N_obj)
	t_survey.add_column(Column(name='PMDEC', data=PMDEC, unit='mas/yr'))
	EPOCH = n.ones(N_obj)*2000.
	t_survey.add_column(Column(name='EPOCH', data=EPOCH, unit='yr'))
	# 'RESOLUTION':n.int16, 1I
	RESOLUTION = n.ones(N_obj).astype('int')
	t_survey.add_column(Column(name='RESOLUTION', data=RESOLUTION, unit=''))
	# 'SUBSURVEY':str, max 256 char
	SUBSURVEY = n.ones(N_obj).astype('str')
	SUBSURVEY[:] = subsurvey
	t_survey.add_column(Column(name='SUBSURVEY', data=SUBSURVEY, unit=''))
	# 'PRIORITY':n.int16, 1I
	PRIORITY = n.zeros(N_obj).astype('int') + priority_values[subsurvey]
	t_survey.add_column(Column(name='PRIORITY', data=PRIORITY, unit=''))
	# EBV for templates
	ebv_1000 = (t_survey['ebv']*1000).astype('int')
	#print('EBV', n.min(ebv_1000), n.max(ebv_1000))
	ebv_1_0 = ( ebv_1000 > 1000 ) 
	ebv_0_5 = ( ebv_1000 > 500 ) & ( ebv_1000 <= 1000 ) 
	ebv_0_4 = ( ebv_1000 > 400 ) & ( ebv_1000 <= 500 ) 
	ebv_0_3 = ( ebv_1000 > 300 ) & ( ebv_1000 <= 400 ) 
	ebv_0_2 = ( ebv_1000 > 200 ) & ( ebv_1000 <= 300 ) 
	ebv_0_1 = ( ebv_1000 > 100 ) & ( ebv_1000 <= 200 ) 
	ebv_0_0 = ( ebv_1000 <= 100 ) 
	z_name = lambda z0, z1 : "_zmin_"+str(int(10*z0)).zfill(2)+"_zmax_"+str(int(10*z1)).zfill(2)
	# templates
	template_names = n.zeros(N_obj).astype('U100')
	ruleset_array = n.zeros(N_obj).astype('str')

	ruleset_array[:] = ruleset_values[subsurvey]
	for z0,z1 in zip(zmins,zmaxs):
		zsel = (t_survey['redshift_R']>=z0) & (t_survey['redshift_R']<z1)
		if len(zsel.nonzero()[0])>0:
			template_names[(zsel)]               = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
			template_names[(zsel)&(ebv_0_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
			template_names[(zsel)&(ebv_0_1)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_1.fits'   
			template_names[(zsel)&(ebv_0_2)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_2.fits'   
			template_names[(zsel)&(ebv_0_3)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_3.fits'   
			template_names[(zsel)&(ebv_0_4)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_4.fits'   
			template_names[(zsel)&(ebv_0_5)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_5.fits'   
			template_names[(zsel)&(ebv_1_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_1_0.fits'   

	# 'TEMPLATE':str, max 256 char
	t_survey.add_column(Column(name='TEMPLATE', data=template_names, unit=''))
	# 'RULESET':str, max 256 char
	t_survey.add_column(Column(name='RULESET', data=ruleset_array, unit=''))
	# 'REDSHIFT_ESTIMATE':n.float32, 1E
	# 'REDSHIFT_ERROR':n.float32, 1E
	t_survey.add_column(Column(name='REDSHIFT_ESTIMATE', data=t_survey['redshift_R'], unit=''))
	t_survey.add_column(Column(name='REDSHIFT_ERROR', data=n.ones(N_obj), unit=''))
	# 'MAG':n.float32,
	# 'MAG_ERR':n.float32
	# 'MAG_TYPE': str max 256 char
	r_v=3.1
	a_v = t_survey['ebv'] * r_v
	delta_mag = n.hstack(( n.array([ extinction.fitzpatrick99(n.array([9000.]), el, r_v=3.1, unit='aa') for el in a_v ]) ))
	print(n.median(t_survey['ebv']), n.median(delta_mag))
	#rv = av/ebv
	#av = rv x ebv
	extincted_mag = t_survey['rtot']-t_survey['ri']-t_survey['iz'] + delta_mag
	t_survey.add_column(Column(name='MAG', data=extincted_mag, unit='mag'))
	t_survey.add_column(Column(name='MAG_ERR', data=0.01 * n.ones(N_obj), unit='mag'))
	MAG_TYPE = n.ones(N_obj).astype('str')
	MAG_TYPE[:] = 'DECam_z_AB'
	t_survey.add_column(Column(name='MAG_TYPE', data=MAG_TYPE, unit=''))
	# 'REDDENING':n.float32, 1E
	t_survey.add_column(Column(name='REDDENING',data=t_survey['ebv'], unit='mag'))
	# 'DATE_EARLIEST':n.float64, JulianDate decimal days # 01-Nov-2022
	# 'DATE_LATEST':n.float64, JulianDate decimal days # 02-Feb-2033
	t_survey.add_column(Column(name='DATE_EARLIEST',data=2459945.5 * n.ones(N_obj), unit='d'))
	t_survey.add_column(Column(name='DATE_LATEST'  ,data=2462501.5 * n.ones(N_obj), unit='d'))
	CADENCE = n.zeros(N_obj).astype('int')
	t_survey.add_column(Column(name='CADENCE', data=CADENCE, unit=''))
	# extent flags and parameters
	# 'EXTENT_FLAG': 1I
	# =1
	# 'EXTENT_PARAMETER': 1E
	# =0
	# 'EXTENT_INDEX': 1E
	# =0
	t_survey.add_column(Column(name='EXTENT_FLAG'     , data=n.ones(N_obj).astype('int') , unit=''))
	t_survey.add_column(Column(name='EXTENT_PARAMETER', data=n.zeros(N_obj), unit=''))
	t_survey.add_column(Column(name='EXTENT_INDEX'    , data=n.zeros(N_obj), unit=''))
	print('assigns magnitudes and sersic types')
	phot_types = n.unique(t_survey['lsdr9_TYPE'])  
	print('LS types', phot_types)
	sel_DEV = (t_survey['lsdr9_TYPE'] == "DEV")
	sel_EXP = (t_survey['lsdr9_TYPE'] == "EXP")
	sel_PSF = (t_survey['lsdr9_TYPE'] == "PSF")
	sel_REX = (t_survey['lsdr9_TYPE'] == "REX")
	sel_SER = (t_survey['lsdr9_TYPE'] == "SER")
	t_survey['EXTENT_FLAG'][sel_PSF] = 0
	t_survey['EXTENT_FLAG'][sel_DEV] = 2
	t_survey['EXTENT_FLAG'][sel_EXP] = 2
	t_survey['EXTENT_FLAG'][sel_REX] = 2
	t_survey['EXTENT_FLAG'][sel_SER] = 2
	#
	t_survey['EXTENT_PARAMETER'][sel_PSF] = 0
	t_survey['EXTENT_PARAMETER'][sel_DEV] = t_survey['lsdr9_SHAPE_R'][sel_DEV]
	t_survey['EXTENT_PARAMETER'][sel_EXP] = t_survey['lsdr9_SHAPE_R'][sel_EXP]
	t_survey['EXTENT_PARAMETER'][sel_REX] = t_survey['lsdr9_SHAPE_R'][sel_REX]
	t_survey['EXTENT_PARAMETER'][sel_SER] = t_survey['lsdr9_SHAPE_R'][sel_SER]
	#
	print('min extent param', t_survey['EXTENT_PARAMETER'][sel_PSF == False].min() )
	t_survey['EXTENT_PARAMETER'] [ ( t_survey['EXTENT_PARAMETER']<0.1 ) & (sel_PSF == False ) ] = 0.101
	#
	t_survey['EXTENT_INDEX'][sel_PSF] = 0
	t_survey['EXTENT_INDEX'][sel_DEV] = 4
	t_survey['EXTENT_INDEX'][sel_EXP] = 1
	t_survey['EXTENT_INDEX'][sel_REX] = 1
	t_survey['EXTENT_INDEX'][sel_SER] = t_survey['lsdr9_SERSIC'][sel_SER]
	#
	print( t_survey['EXTENT_FLAG'][sel_PSF].min(), t_survey['EXTENT_FLAG'][sel_PSF].max() ) 
	print( t_survey['EXTENT_FLAG'][sel_DEV].min(), t_survey['EXTENT_FLAG'][sel_DEV].max() ) 
	print( t_survey['EXTENT_FLAG'][sel_EXP].min(), t_survey['EXTENT_FLAG'][sel_EXP].max() ) 
	print( t_survey['EXTENT_FLAG'][sel_REX].min(), t_survey['EXTENT_FLAG'][sel_REX].max() ) 
	print( t_survey['EXTENT_FLAG'][sel_SER].min(), t_survey['EXTENT_FLAG'][sel_SER].max() ) 
	#
	print( t_survey['EXTENT_PARAMETER'][sel_PSF].min(), t_survey['EXTENT_PARAMETER'][sel_PSF].max() ) 
	print( t_survey['EXTENT_PARAMETER'][sel_DEV].min(), t_survey['EXTENT_PARAMETER'][sel_DEV].max() ) 
	print( t_survey['EXTENT_PARAMETER'][sel_EXP].min(), t_survey['EXTENT_PARAMETER'][sel_EXP].max() ) 
	print( t_survey['EXTENT_PARAMETER'][sel_REX].min(), t_survey['EXTENT_PARAMETER'][sel_REX].max() ) 
	print( t_survey['EXTENT_PARAMETER'][sel_SER].min(), t_survey['EXTENT_PARAMETER'][sel_SER].max() ) 
	#
	print( t_survey['EXTENT_INDEX'][sel_PSF].min(), t_survey['EXTENT_INDEX'][sel_PSF].max() ) 
	print( t_survey['EXTENT_INDEX'][sel_DEV].min(), t_survey['EXTENT_INDEX'][sel_DEV].max() ) 
	print( t_survey['EXTENT_INDEX'][sel_EXP].min(), t_survey['EXTENT_INDEX'][sel_EXP].max() ) 
	print( t_survey['EXTENT_INDEX'][sel_REX].min(), t_survey['EXTENT_INDEX'][sel_REX].max() ) 
	print( t_survey['EXTENT_INDEX'][sel_SER].min(), t_survey['EXTENT_INDEX'][sel_SER].max() ) 
	#
	print('min extent param', t_survey['EXTENT_PARAMETER'][sel_PSF == False].min() )
	return t_survey

def update_4most_columns(subsurvey , t_survey  ):
	# templates redshift bins
	z_all = n.hstack(( 0., n.arange(0.3, 3., 0.2), 3.5, 4.5, 6. ))
	zmins = z_all[:-1]
	zmaxs = z_all[1:]
	N_obj = len(t_survey)
	#  limit size of the string columns to the size of the longer string in the corresponding columns. 
	# 'NAME':str, max 256 char
	N1 = n.arange(len(t_survey['ebv']))
	id_list = N_subsurvey[subsurvey]*1e10 +  N1
	NAME = n.array([ str(int(el)).zfill(11) for el in id_list ])
	t_survey['NAME'] = NAME
	PMRA = n.zeros(N_obj)
	t_survey['PMRA'] = PMRA
	PMDEC = n.zeros(N_obj)
	t_survey['PMDEC'] = PMDEC
	EPOCH = n.ones(N_obj)*2000.
	t_survey['EPOCH'] = EPOCH
	# 'RESOLUTION':n.int16, 1I
	RESOLUTION = n.ones(N_obj).astype('int')
	t_survey['RESOLUTION'] = RESOLUTION
	# 'SUBSURVEY':str, max 256 char
	SUBSURVEY = n.ones(N_obj).astype('str')
	SUBSURVEY[:] = subsurvey
	t_survey['SUBSURVEY'] = SUBSURVEY
	# 'PRIORITY':n.int16, 1I
	PRIORITY = n.zeros(N_obj).astype('int') + priority_values[subsurvey]
	t_survey['PRIORITY'] = PRIORITY
	# EBV for templates
	ebv_1000 = (t_survey['ebv']*1000).astype('int')
	#print('EBV', n.min(ebv_1000), n.max(ebv_1000))
	ebv_1_0 = ( ebv_1000 > 1000 ) 
	ebv_0_5 = ( ebv_1000 > 500 ) & ( ebv_1000 <= 1000 ) 
	ebv_0_4 = ( ebv_1000 > 400 ) & ( ebv_1000 <= 500 ) 
	ebv_0_3 = ( ebv_1000 > 300 ) & ( ebv_1000 <= 400 ) 
	ebv_0_2 = ( ebv_1000 > 200 ) & ( ebv_1000 <= 300 ) 
	ebv_0_1 = ( ebv_1000 > 100 ) & ( ebv_1000 <= 200 ) 
	ebv_0_0 = ( ebv_1000 <= 100 ) 
	z_name = lambda z0, z1 : "_zmin_"+str(int(10*z0)).zfill(2)+"_zmax_"+str(int(10*z1)).zfill(2)
	# templates
	template_names = n.zeros(N_obj).astype('U100')
	ruleset_array = n.zeros(N_obj).astype('str')
	# S5 
	ruleset_array[:] = ruleset_values[subsurvey]
	for z0,z1 in zip(zmins,zmaxs):
		zsel = (t_survey['redshift_R']>=z0) & (t_survey['redshift_R']<z1)
		if len(zsel.nonzero()[0])>0:
			template_names[(zsel)]               = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
			template_names[(zsel)&(ebv_0_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
			template_names[(zsel)&(ebv_0_1)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_1.fits'   
			template_names[(zsel)&(ebv_0_2)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_2.fits'   
			template_names[(zsel)&(ebv_0_3)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_3.fits'   
			template_names[(zsel)&(ebv_0_4)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_4.fits'   
			template_names[(zsel)&(ebv_0_5)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_5.fits'   
			template_names[(zsel)&(ebv_1_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_1_0.fits'   

	# 'TEMPLATE':str, max 256 char
	t_survey['TEMPLATE'] = template_names
	# 'RULESET':str, max 256 char
	t_survey['RULESET'] = ruleset_array
	# 'REDSHIFT_ESTIMATE':n.float32, 1E
	# 'REDSHIFT_ERROR':n.float32, 1E
	t_survey['REDSHIFT_ESTIMATE'] = t_survey['redshift_R']
	t_survey['REDSHIFT_ERROR'] = n.ones(N_obj)
	# 'MAG':n.float32,
	# 'MAG_ERR':n.float32
	# 'MAG_TYPE': str max 256 char
	r_v=3.1
	a_v = t_survey['ebv'] * r_v
	delta_mag = n.hstack(( n.array([ extinction.fitzpatrick99(n.array([9000.]), el, r_v=3.1, unit='aa') for el in a_v ]) ))
	print(n.median(t_survey['ebv']), n.median(delta_mag))
	#rv = av/ebv
	#av = rv x ebv
	extincted_mag = t_survey['rtot']-t_survey['ri']-t_survey['iz'] + delta_mag
	t_survey['MAG'] = extincted_mag
	t_survey['MAG_ERR'] = 0.01 * n.ones(N_obj)
	MAG_TYPE = n.ones(N_obj).astype('str')
	MAG_TYPE[:] = 'DECam_z_AB'
	t_survey['MAG_TYPE'] = MAG_TYPE
	# 'REDDENING':n.float32, 1E
	t_survey['REDDENING'] = t_survey['ebv']
	# 'DATE_EARLIEST':n.float64, JulianDate decimal days # 01-Nov-2022
	# 'DATE_LATEST':n.float64, JulianDate decimal days # 02-Feb-2033
	t_survey['DATE_EARLIEST'] = 2459945.5 * n.ones(N_obj)
	t_survey['DATE_LATEST'  ] = 2462501.5 * n.ones(N_obj)
	t_survey['CADENCE'  ] = n.zeros(N_obj).astype('int')
	# 'EXTENT_FLAG': 1I
	# =1
	# 'EXTENT_PARAMETER': 1E
	# =0
	# 'EXTENT_INDEX': 1E
	# =0
	t_survey['EXTENT_FLAG'     ] = n.zeros(N_obj).astype('int') 
	t_survey['EXTENT_PARAMETER'] = n.zeros(N_obj)
	t_survey['EXTENT_INDEX'    ] = n.zeros(N_obj)
	print('assigns magnitudes and sersic types')
	phot_types = n.unique(t_survey['lsdr9_TYPE'])  
	print('LS types', phot_types)
	sel_DEV = (t_survey['lsdr9_TYPE'] == "DEV")
	sel_EXP = (t_survey['lsdr9_TYPE'] == "EXP")
	sel_PSF = (t_survey['lsdr9_TYPE'] == "PSF")
	sel_REX = (t_survey['lsdr9_TYPE'] == "REX")
	sel_SER = (t_survey['lsdr9_TYPE'] == "SER")
	# extent flags and parameters
	t_survey['EXTENT_FLAG'][sel_PSF] = 0
	t_survey['EXTENT_FLAG'][sel_DEV] = 2
	t_survey['EXTENT_FLAG'][sel_EXP] = 2
	t_survey['EXTENT_FLAG'][sel_REX] = 2
	t_survey['EXTENT_FLAG'][sel_SER] = 2
	#
	t_survey['EXTENT_PARAMETER'][sel_PSF] = 0
	t_survey['EXTENT_PARAMETER'][sel_DEV] = t_survey['lsdr9_SHAPE_R'][sel_DEV]
	t_survey['EXTENT_PARAMETER'][sel_EXP] = t_survey['lsdr9_SHAPE_R'][sel_EXP]
	t_survey['EXTENT_PARAMETER'][sel_REX] = t_survey['lsdr9_SHAPE_R'][sel_REX]
	t_survey['EXTENT_PARAMETER'][sel_SER] = t_survey['lsdr9_SHAPE_R'][sel_SER]
	#
	print('min extent param', t_survey['EXTENT_PARAMETER'][sel_PSF == False].min() )
	t_survey['EXTENT_PARAMETER'] [ ( t_survey['EXTENT_PARAMETER']<0.1 ) & (sel_PSF == False ) ] = 0.101
	#
	t_survey['EXTENT_INDEX'][sel_PSF] = 0
	t_survey['EXTENT_INDEX'][sel_DEV] = 4
	t_survey['EXTENT_INDEX'][sel_EXP] = 1
	t_survey['EXTENT_INDEX'][sel_REX] = 1
	t_survey['EXTENT_INDEX'][sel_SER] = t_survey['lsdr9_SERSIC'][sel_SER]
	#
	print( t_survey['EXTENT_FLAG'][sel_PSF].min(), t_survey['EXTENT_FLAG'][sel_PSF].max() ) 
	print( t_survey['EXTENT_FLAG'][sel_DEV].min(), t_survey['EXTENT_FLAG'][sel_DEV].max() ) 
	print( t_survey['EXTENT_FLAG'][sel_EXP].min(), t_survey['EXTENT_FLAG'][sel_EXP].max() ) 
	print( t_survey['EXTENT_FLAG'][sel_REX].min(), t_survey['EXTENT_FLAG'][sel_REX].max() ) 
	print( t_survey['EXTENT_FLAG'][sel_SER].min(), t_survey['EXTENT_FLAG'][sel_SER].max() ) 
	#
	print( t_survey['EXTENT_PARAMETER'][sel_PSF].min(), t_survey['EXTENT_PARAMETER'][sel_PSF].max() ) 
	print( t_survey['EXTENT_PARAMETER'][sel_DEV].min(), t_survey['EXTENT_PARAMETER'][sel_DEV].max() ) 
	print( t_survey['EXTENT_PARAMETER'][sel_EXP].min(), t_survey['EXTENT_PARAMETER'][sel_EXP].max() ) 
	print( t_survey['EXTENT_PARAMETER'][sel_REX].min(), t_survey['EXTENT_PARAMETER'][sel_REX].max() ) 
	print( t_survey['EXTENT_PARAMETER'][sel_SER].min(), t_survey['EXTENT_PARAMETER'][sel_SER].max() ) 
	#
	print( t_survey['EXTENT_INDEX'][sel_PSF].min(), t_survey['EXTENT_INDEX'][sel_PSF].max() ) 
	print( t_survey['EXTENT_INDEX'][sel_DEV].min(), t_survey['EXTENT_INDEX'][sel_DEV].max() ) 
	print( t_survey['EXTENT_INDEX'][sel_EXP].min(), t_survey['EXTENT_INDEX'][sel_EXP].max() ) 
	print( t_survey['EXTENT_INDEX'][sel_REX].min(), t_survey['EXTENT_INDEX'][sel_REX].max() ) 
	print( t_survey['EXTENT_INDEX'][sel_SER].min(), t_survey['EXTENT_INDEX'][sel_SER].max() ) 
	#
	print('min index', t_survey['EXTENT_INDEX'][sel_PSF == False].min() )
	return t_survey



# get survey footprint bitlist
def fourmost_get_survbitlist():
	mydict                = {}
	mydict['des']         = 0
	mydict['kidss']       = 1
	mydict['kidsn']       = 2
	mydict['atlassgcnotdes'] = 3
	mydict['atlasngc']    = 4
	mydict['kabs']        = 5
	mydict['vhsb10']      = 6
	mydict['vhsb15']      = 7
	mydict['vhsb20']      = 8
	mydict['vhsb20clean'] = 9
	mydict['desi']        = 10
	mydict['erosita']     = 11
	mydict['waveswide']   = 12
	mydict['euclid']      = 13
	mydict['s8elg']       = 14
	mydict['s8']          = 15
	return mydict


# 4most s8 footprint
def fourmost_get_s8foot(ra_deg, dec_deg):
	ra = ra_deg.value
	dec = dec_deg.value
	svbdict = fourmost_get_survbitlist()
	survbit = n.zeros(len(ra),dtype=int)
	for bname in ['des','desi','kidss','kidsn','atlassgcnotdes','atlasngc']:
		isb           = fourmost_get_survbit_indiv(ra_deg,dec_deg,bname)
		survbit[isb] += 2**svbdict[bname]
	# bg/lrgi/qso/lya
	iss8  = ((((survbit & 2**svbdict['des'])>0) | ((survbit & 2**svbdict['atlassgcnotdes'])>0)) & ((survbit & 2**svbdict['desi'])==0)) | ((((survbit & 2**svbdict['kidsn'])>0) & (ra>154)) | ((survbit & 2**svbdict['atlasngc'])>0)) 
	# elg
	iss8elg = (((ra>330) | (ra<90)) & (dec>-35.5) & (dec<-26)) | ((ra>50) & (ra<90) & (dec>-40) & (dec<-26))
	return iss8,iss8elg



# get survey footprint bit
def fourmost_get_survbit_indiv(ra_deg,dec_deg,bname):
	#
	# Galactic l,b
	c       = SkyCoord(ra=ra_deg, dec=dec_deg, frame='fk5')
	l,b     = c.galactic.l.value,c.galactic.b.value
	lon,lat = c.barycentrictrueecliptic.lon.degree,c.barycentrictrueecliptic.lat.degree
	ra = ra_deg.value
	dec = dec_deg.value
	# 4most/s8 , s8elg
	if (bname in ['s8elg','s8']):
		iss8,iss8elg = fourmost_get_s8foot(ra_deg, dec_deg)
		if (bname=='s8elg'):
			keep = iss8elg
		else: # s8
			keep = iss8
	# desi: no ply file for the moment...
	elif (bname=='desi'):
		# desi sgc
		polyra = n.array([0 ,-25,-35,-50,-54,-45,10,10,  60, 70, 70,53,42,42,38,0])
		polydec= n.array([33,33, 25,  8,  -8, -15,-15,-20,-20,-15,0, 0, 10,20,33,33])
		sgcpoly = Path(n.concatenate(
							(polyra. reshape((len(polyra),1)),
							 polydec.reshape((len(polyra),1))),
							axis=1))
		# desi ngc
		polyra = n.array([275,107,115,130,230,230,230,255,265,275])
		polydec= n.array([33, 33, 12, -10,-10, -2, -2, -2,  13, 33])
		ngcpoly = Path(n.concatenate(
							(polyra. reshape((len(polyra),1)),
							 polydec.reshape((len(polyra),1))),
							axis=1))
		#
		tmpradec         = n.transpose(n.array([ra,dec]))
		tmp              = (ra>300)
		tmpradec[tmp,0] -= 360.
		keep = n.zeros(len(ra),dtype=bool)
		for poly in [sgcpoly,ngcpoly]:
			keep[poly.contains_points(tmpradec)] = True
	elif (bname=='erosita'):
		# johan email 30/07/2018 14:12
		keep = (abs(b)>15) & (l>180)
	elif (bname=='waveswide'):
		# https://wavesurvey.org/project/survey-design/
		keep = (((ra>155) & (ra<240) & (dec>-5) & (dec<5))
				|
				(((ra>330) | (ra<50)) & (dec>-36) & (dec<-26)))
	elif (bname=='euclid'):
		keep = (n.abs(b)>=30) & (n.abs(lat)>5.)
	else:
		if (bname[:3]=='vhs'):
			## -70<dec<0
			mng    = pymangle.Mangle(MASKDIR+'/vhsdec.ply')
			polyid = mng.polyid(ra,dec)
			keepdec= (polyid!=-1)
			# |b|>bmin
			mng    = pymangle.Mangle(MASKDIR+'/'+bname[3:6]+'.ply')
			polyid = mng.polyid(l,b)
			keepb  = (polyid!=-1)
			##
			keep   = (keepdec) & (keepb)
		else:
			mng    = pymangle.Mangle(MASKDIR+'/'+bname+'.ply')
			polyid = mng.polyid(ra,dec)
			keep   = (polyid!=-1)
		if (bname=='vhsb20clean'):
			## Jext<0.1 and low nstar selection [both VHS and DES]
			ra60    = (ra>55)  & (ra<65)  & (dec>-5)  & (dec<0)
			ra70    = (ra>67)  & (ra<72)  & (dec>-16) & (dec<-13)
			ra100   = (ra>50)  & (ra<180) & (dec>-20) & (dec<0)   & (b>-23) & (b<0)
			ra120   = (ra>100) & (ra<180)                         & (b>-15) & (b<0)
			ra230   = (ra>228) & (ra<270) & (dec>-40) & (dec<-20) & (b>0) 
			ra250   = (ra>235) & (ra<270) & (dec>-20) & (dec<0)   & (b>0)
			ra300   = (ra>180) & (ra<360) & (dec>-70) & (dec<0)   & (b>-25) & (b<0)
			LMC     = (ra>70)  & (ra<90)  & (dec>-70) & (dec<-65)
			keep    = ((keep) & 
						(~ra60)  & (~ra70)  & 
						(~ra100) & (~ra120) & 
						(~ra230) & (~ra250) & (~ra300) &
						(~LMC))
	#print( bname, len(ra[keep]))
	return keep


def fourmost_get_survbit(ra,dec):
	bdict   = fourmost_get_survbitlist()
	survbit = n.zeros(len(ra),dtype='int')
	for bname in bdict.keys():
		#print(bname)
		isb           = fourmost_get_survbit_indiv(ra,dec,bname)
		survbit[isb] += 2**bdict[bname]
	return survbit



def write_sbits(t_survey):
	if t_survey['RA'].unit==u.deg:
		mask_bit = fourmost_get_survbit(t_survey['RA'].data*u.deg, t_survey['DEC'].data*u.deg)
	else:
		mask_bit = fourmost_get_survbit(t_survey['RA']*u.deg, t_survey['DEC']*u.deg)
	if 'MASK_BIT' in t_survey.columns.keys():
		t_survey['MASK_BIT'] = mask_bit
	else:
		t_survey.add_column(Column(name='MASK_BIT'  ,data=mask_bit, unit=''))
		
	return t_survey

# previous method to compute magnitudes
# t_survey = compute_kmag(t_survey)

# compute magnitudes
t_survey = add_magnitudes_direct_match(t_survey = t_survey, t_kids = t_kids)
# t_survey_all = add_magnitudes_direct_match(t_survey = t_survey_all, t_kids = t_kids)

# add 4MOST columns
t_survey = add_4most_columns(subsurvey = 'cluster_redGAL', t_survey = t_survey)


# keeping red AGN, but with AGN magnitudes
# t_survey[idx_red_agn[same_halo]] <<== agn[match_agn[red_agn][same_halo]]
print(t_survey[idx_red_agn[same_halo]]['MAG'])
t_survey['MAG'][idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG']
print(t_survey[idx_red_agn[same_halo]]['MAG'])
t_survey['MAG_ERR']          [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG_ERR']
t_survey['EXTENT_FLAG']      [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_FLAG']
t_survey['EXTENT_PARAMETER'] [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_PARAMETER']
t_survey['EXTENT_INDEX']     [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_INDEX']
t_survey['MAG_TYPE']         [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG_TYPE']
t_survey['rtot']             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']
t_survey['rfib']             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']          
t_survey['ug'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['GALEX-NUV']-agn[match_agn[red_agn][same_halo]]['HSC-g']          
t_survey['gr'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-g']-agn[match_agn[red_agn][same_halo]]['HSC-r']          
t_survey['ri']               [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']-agn[match_agn[red_agn][same_halo]]['HSC-i']
t_survey['iz']               [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-i']-agn[match_agn[red_agn][same_halo]]['HSC-z']
t_survey['zy'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-z']-agn[match_agn[red_agn][same_halo]]['HSC-y']          
t_survey['yj'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-y']-agn[match_agn[red_agn][same_halo]]['VISTA-J']                   
t_survey['jh'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['VISTA-J']-agn[match_agn[red_agn][same_halo]]['VISTA-H']                             
t_survey['hks' ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['VISTA-H']-agn[match_agn[red_agn][same_halo]]['VISTA-K']                   

# removing QSOs
t_survey[idx_remove] = -999
t_survey = t_survey[t_survey['RA']>-990]

# defines the two sub surveys

# z-band limit : z<22
galaxies = (t_survey['rtot']-t_survey['ri']-t_survey['iz']<22.0)#&(t_survey['is_quiescent'])
t_gal = t_survey[galaxies]
print('cluster GAL', path_2_out_GAL)
print(len(t_gal))

# z-band limit : z<23
#x = ((t_survey['x']-t_survey['HOST_HALO_x'])**2. + (t_survey['y']-t_survey['HOST_HALO_y'])**2. + (t_survey['z']-t_survey['HOST_HALO_z'])**2.)**0.5
bcg_selection = (t_survey['HALO_id'] == t_survey['HOST_HALO_id']) & (t_survey['rtot']-t_survey['ri']-t_survey['iz']<23.0) # & (t_survey['is_quiescent'])
t_bcg = t_survey[bcg_selection]
print('cluster BCG', path_2_out_BCG)
print(len(t_bcg))


if 'PMRA' in t_bcg.columns :
	t_bcg = update_4most_columns(subsurvey = 'cluster_BCG', t_survey = t_bcg)
else:
	t_bcg = add_4most_columns(subsurvey = 'cluster_BCG', t_survey = t_bcg)

if 'PMRA' in t_gal.columns :
	t_gal = update_4most_columns(subsurvey = 'cluster_redGAL', t_survey = t_gal)
else:
	t_gal = add_4most_columns(subsurvey = 'cluster_redGAL', t_survey = t_gal)

#if 'PMRA' in t_survey_all.columns :
	#t_survey_all = update_4most_columns(subsurvey = 'cluster_redGAL', t_survey = t_survey_all)
#else:
	#t_survey_all = add_4most_columns(subsurvey = 'cluster_redGAL', t_survey = t_survey_all)

#t_survey = write_sbits(t_survey)
print('writes cluster GAL', path_2_out_GAL)
t_gal.write( path_2_out_GAL, overwrite = True )

print('writes BCG',path_2_out_BCG)
t_bcg.write( path_2_out_BCG , overwrite = True)

#print('writes ALL',path_2_out_ALL)
#t_survey_all.write( path_2_out_ALL , overwrite = True)
