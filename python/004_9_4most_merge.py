"""
Merges into a single fits catalog containing the 4FS input columns.


"""
import os
import sys
import astropy.io.fits as fits
import numpy as n
from astropy.coordinates import SkyCoord
from astropy.table import Table, Column
import pymangle
import astropy.units as u
from matplotlib.path import Path

env = "UNIT_fA1i_DIR"
working_dir = os.path.join(os.environ[env] )
name_in = sys.argv[1]
name_out = sys.argv[2]

catalog_input = os.path.join(working_dir, name_in) #'S05_all.fits')
t_survey = Table.read(catalog_input)
path_2_out = os.path.join(working_dir, name_out )# 'S05_all_formatApril22.fits')

#t_survey = t_survey[ ( t_survey['RA'] < 359.99 ) ]

N_obj = len(t_survey)
t_out = Table()
#  limit size of the string columns to the size of the longer string in the corresponding columns.
# 'NAME':str, max 256 char
t_out.add_column(Column(name='NAME', data=t_survey['NAME'], unit=''))
# 'RA':n.float64, 1D
# 'DEC':n.float64, 1D
t_out.add_column(Column(name='RA', data=t_survey['RA'].astype('float64'), unit='deg'))
t_out.add_column(Column(name='DEC', data=t_survey['DEC'].astype('float64'), unit='deg'))
# 'PMRA':n.float32, 1E
# 'PMDEC':n.float32, 1E
# 'EPOCH':n.float32, 1E
PMRA = n.zeros(N_obj)
t_out.add_column(Column(name='PMRA', data=PMRA.astype('float32'), unit='mas/yr'))
PMDEC = n.zeros(N_obj)
t_out.add_column(Column(name='PMDEC', data=PMDEC.astype('float32'), unit='mas/yr'))
EPOCH = n.ones(N_obj)*2015.5
t_out.add_column(Column(name='EPOCH', data=EPOCH.astype('float32'), unit='yr'))
# 'RESOLUTION':n.int16, 1I
RESOLUTION = n.ones(N_obj).astype('int16')
t_out.add_column(Column(name='RESOLUTION', data=RESOLUTION, unit=''))
# 'SUBSURVEY':str, max 256 char
t_out.add_column(Column(name='SUBSURVEY', data=t_survey['SUBSURVEY'], unit=''))

NEWtemplates = n.array([ el.strip() for el in t_survey['TEMPLATE'] ])

# 'TEMPLATE':str, max 256 char
t_out.add_column(Column(name='TEMPLATE', data=NEWtemplates, unit=''))
# 'RULESET':str, max 256 char

NEWrulesets = n.array([ el.strip() for el in t_survey['RULESET'] ])

t_out.add_column(Column(name='RULESET', data=NEWrulesets, unit=''))
# 'REDSHIFT_ESTIMATE':n.float32, 1E
# 'REDSHIFT_ERROR':n.float32, 1E
t_out.add_column(Column(name='REDSHIFT_ESTIMATE', data=t_survey['REDSHIFT_ESTIMATE'].astype('float32'), unit=''))
t_out.add_column(Column(name='REDSHIFT_ERROR', data=(0.5*n.ones(N_obj)).astype('float32'), unit=''))
# 'MAG':n.float32,
# 'MAG_ERR':n.float32
# 'MAG_TYPE': str max 256 char
t_out.add_column(Column(name='MAG', data=t_survey['MAG'].astype('float32'), unit='mag'))
t_out.add_column(Column(name='MAG_ERR', data = (0.01 * n.ones(N_obj)).astype('float32'), unit='mag'))
mag_types = n.array([ el.strip() for el in t_survey['MAG_TYPE'] ])
t_out.add_column(Column(name='MAG_TYPE', data=mag_types, unit=''))
# 'REDDENING':n.float32, 1E
t_out.add_column(Column(name='REDDENING',data=t_survey['REDDENING'].astype('float32'), unit=''))
# 'DATE_EARLIEST':n.float64, JulianDate decimal days # 01-Nov-2022
# 'DATE_LATEST':n.float64, JulianDate decimal days # 02-Feb-2033
t_out.add_column(Column(name='DATE_EARLIEST',data = n.zeros(N_obj).astype('float64'), unit='d'))
t_out.add_column(Column(name='DATE_LATEST'  ,data = n.zeros(N_obj).astype('float64'), unit='d'))
CADENCE = n.zeros(N_obj).astype('int64')
t_out.add_column(Column(name='CADENCE', data=CADENCE, unit=''))
# extent flags and parameters
# 'EXTENT_FLAG': 1I
# =1
# 'EXTENT_PARAMETER': 1E
# =0
# 'EXTENT_INDEX': 1E
# =0
t_out.add_column(Column(name='EXTENT_FLAG'     , data=t_survey['EXTENT_FLAG'].astype('int16') , unit=''))
t_out.add_column(Column(name='EXTENT_PARAMETER', data=t_survey['EXTENT_PARAMETER'].astype('float32') , unit='arcsec'))
t_out.add_column(Column(name='EXTENT_INDEX'    , data=t_survey['EXTENT_INDEX'].astype('float32') , unit=''))


# get survey footprint bitlist
MASKDIR = os.path.join(os.environ['GIT_AGN_MOCK'],'data', 'masks')

def fourmost_get_survbitlist():
    mydict                = {}
    mydict['des']         = 0
    mydict['kidss']       = 1
    mydict['kidsn']       = 2
    mydict['atlassgcnotdes'] = 3
    mydict['atlasngc']    = 4
    mydict['kabs']        = 5
    mydict['vhsb10']      = 6
    mydict['vhsb15']      = 7
    mydict['vhsb20']      = 8
    mydict['vhsb20clean'] = 9
    mydict['desi']        = 10
    mydict['erosita']     = 11
    mydict['waveswide']   = 12
    mydict['euclid']      = 13
    mydict['Qhs']         = 14
    mydict['s8']          = 15
    return mydict



# 4most s8 footprint
def fourmost_get_s8foot(ra_deg, dec_deg):
    ra = ra_deg.value
    dec = dec_deg.value
    svbdict = fourmost_get_survbitlist()
    survbit = n.zeros(len(ra),dtype=int)
    for bname in ['des','desi','kidss','kidsn','atlassgcnotdes','atlasngc']:
        isb           = fourmost_get_survbit_indiv(ra_deg,dec_deg,bname)
        survbit[isb] += 2**svbdict[bname]
    # bg/lrgi/qso/lya
    iss8  = ((((survbit & 2**svbdict['des'])>0) | ((survbit & 2**svbdict['atlassgcnotdes'])>0)) & ((survbit & 2**svbdict['desi'])==0)) | ((((survbit & 2**svbdict['kidsn'])>0) & (ra>154)) | ((survbit & 2**svbdict['atlasngc'])>0))
    # elg
    #iss8elg = (((ra>330) | (ra<90)) & (dec>-35.5) & (dec<-26)) | ((ra>50) & (ra<90) & (dec>-40) & (dec<-26))
    return iss8#,iss8elg



# get survey footprint bit
def fourmost_get_survbit_indiv(ra_deg,dec_deg,bname):
    #
    # Galactic l,b
    c       = SkyCoord(ra=ra_deg, dec=dec_deg, frame='fk5')
    l,b     = c.galactic.l.value,c.galactic.b.value
    lon,lat = c.barycentrictrueecliptic.lon.degree,c.barycentrictrueecliptic.lat.degree
    ra = ra_deg.value
    dec = dec_deg.value
    # 4most/s8 , s8elg
    if (bname=='s8'):
        keep = fourmost_get_s8foot(ra_deg, dec_deg)
    # desi: no ply file for the moment...
    elif (bname=='desi'):
        # desi sgc
        polyra = n.array([0 ,-25,-35,-50,-54,-45,10,10,  60, 70, 70,53,42,42,38,0])
        polydec= n.array([33,33, 25,  8,  -8, -15,-15,-20,-20,-15,0, 0, 10,20,33,33])
        sgcpoly = Path(n.concatenate(
                            (polyra. reshape((len(polyra),1)),
                                polydec.reshape((len(polyra),1))),
                            axis=1))
        # desi ngc
        polyra = n.array([275,107,115,130,230,230,230,255,265,275])
        polydec= n.array([33, 33, 12, -10,-10, -2, -2, -2,  13, 33])
        ngcpoly = Path(n.concatenate(
                            (polyra. reshape((len(polyra),1)),
                                polydec.reshape((len(polyra),1))),
                            axis=1))
        #
        tmpradec         = n.transpose(n.array([ra,dec]))
        tmp              = (ra>300)
        tmpradec[tmp,0] -= 360.
        keep = n.zeros(len(ra),dtype=bool)
        for poly in [sgcpoly,ngcpoly]:
            keep[poly.contains_points(tmpradec)] = True
    elif (bname=='erosita'):
        keep = (n.abs(b)>10) & (l>180)
    elif (bname=='waveswide'):
        # https://wavesurvey.org/project/survey-design/
        keep = (((ra>155) & (ra<240) & (dec>-5) & (dec<5))
                |
                (((ra>330) | (ra<50)) & (dec>-36) & (dec<-26)))
    elif (bname=='euclid'):
        keep = (n.abs(b)>=30) & (n.abs(lat)>5.)
    elif (bname=='Qhs'):
        keep = (dec<0) & (n.abs(b)>=5)
    else:
        if (bname[:3]=='vhs'):
            ## -70<dec<0
            mng    = pymangle.Mangle(MASKDIR+'/vhsdec.ply')
            polyid = mng.polyid(ra,dec)
            keepdec= (polyid!=-1)
            # |b|>bmin
            mng    = pymangle.Mangle(MASKDIR+'/'+bname[3:6]+'.ply')
            polyid = mng.polyid(l,b)
            keepb  = (polyid!=-1)
            ##
            keep   = (keepdec) & (keepb)
        else:
            mng    = pymangle.Mangle(MASKDIR+'/'+bname+'.ply')
            polyid = mng.polyid(ra,dec)
            keep   = (polyid!=-1)
        if (bname=='vhsb20clean'):
            ## Jext<0.1 and low nstar selection [both VHS and DES]
            ra60    = (ra>55)  & (ra<65)  & (dec>-5)  & (dec<0)
            ra70    = (ra>67)  & (ra<72)  & (dec>-16) & (dec<-13)
            ra100   = (ra>50)  & (ra<180) & (dec>-20) & (dec<0)   & (b>-23) & (b<0)
            ra120   = (ra>100) & (ra<180)                         & (b>-15) & (b<0)
            ra230   = (ra>228) & (ra<270) & (dec>-40) & (dec<-20) & (b>0)
            ra250   = (ra>235) & (ra<270) & (dec>-20) & (dec<0)   & (b>0)
            ra300   = (ra>180) & (ra<360) & (dec>-70) & (dec<0)   & (b>-25) & (b<0)
            LMC     = (ra>70)  & (ra<90)  & (dec>-70) & (dec<-65)
            keep    = ((keep) &
                        (~ra60)  & (~ra70)  &
                        (~ra100) & (~ra120) &
                        (~ra230) & (~ra250) & (~ra300) &
                        (~LMC))
    print( bname, len(ra[keep]))
    return keep


def fourmost_get_survbit(ra,dec):
    bdict   = fourmost_get_survbitlist()
    survbit = n.zeros(len(ra),dtype='int')
    for bname in bdict.keys():
        print(bname)
        isb           = fourmost_get_survbit_indiv(ra,dec,bname)
        survbit[isb] += 2**bdict[bname]
    return survbit



if t_out['RA'].unit==u.deg:
    mask_bit = fourmost_get_survbit(t_out['RA'].data*u.deg, t_out['DEC'].data*u.deg)
else:
    mask_bit = fourmost_get_survbit(t_out['RA']*u.deg, t_out['DEC']*u.deg)

t_out.add_column(Column(name='MASK_BIT'  ,data=mask_bit, unit=''))

t_out.write(path_2_out, overwrite = True)


