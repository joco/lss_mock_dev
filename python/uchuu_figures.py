"""
What it does
------------

Computes the AGN model from Comparat et al. 2019.

cd $GIT_AGN_MOCK/python

"""
import sys
import os
import time
import extinction
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import astropy.constants as cc
import astropy.io.fits as fits
from astropy.table import Table, Column
from scipy.special import erf
from scipy.stats import norm
from scipy.interpolate import interp2d
from scipy.interpolate import interp1d
import healpy
#import h5py
import numpy as n

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p

print('A few simple figures with UCHUU pixel 0')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

env = "UCHUU" 

if env[:2] == "UC" :
    cosmoUCHUU = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.3089)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUCHUU

PIX_ID = 0

path_2_GAL_file = os.path.join(os.environ[env], 'cat_GALAXY_all',  str(PIX_ID).zfill(4)+'.fits')

fig_dir = os.path.join(    os.environ["GIT_AGN_MOCK"], 'figures', env, 'galaxy' )

print('opens coordinate file ', path_2_GAL_file, time.time() - t0)
f1 = fits.open(path_2_GAL_file)

zz = f1[1].data['redshift_R']
cen = (f1[1].data['pid']==-1)
sat = (cen==False)
N_galaxies = len(zz)
N_galaxies_cen = len(zz[cen])
N_galaxies_sat = len(zz[sat])
print('native N, cen, sat, f_sat', N_galaxies, N_galaxies_cen, N_galaxies_sat, N_galaxies_sat*1./N_galaxies_cen)

rd = n.random.random(N_galaxies)
for_plot = (rd<0.01)

data = f1[1].data[for_plot]
print(len(data))


fig_out = os.path.join(fig_dir, 'SMHMR_mass_vs_zr.png')

X = data['redshift_R']
Y = data['SMHMR_mass']
p.figure(1, (6., 5.5))
p.tight_layout()
p.plot(X, Y, 'k,', rasterized=True)
p.xlabel('redshift R')
p.ylabel('SMHMR_mass')
p.grid()
p.savefig(fig_out)
p.clf()


fig_out = os.path.join(fig_dir, 'SFR_vs_zr.png')

X = data['redshift_R']
Y = data['star_formation_rate']
p.figure(1, (6., 5.5))
p.tight_layout()
p.plot(X, Y, 'k,', rasterized=True)
p.xlabel('redshift R')
p.ylabel('star_formation_rate')
p.grid()
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'LXhard_vs_zr.png')

X = data['redshift_R']
Y = data['LX_hard']
p.figure(1, (6., 5.5))
p.tight_layout()
p.plot(X, Y, 'k,', rasterized=True)
p.xlabel('redshift R')
p.ylabel('LX_hard')
p.grid()
p.savefig(fig_out)
p.clf()



fig_out = os.path.join(fig_dir, 'DEC_vs_RA.png')

X = data['RA']
Y = data['DEC']
p.figure(1, (6., 5.5))
p.tight_layout()
p.plot(X, Y, 'k,', rasterized=True)
p.xlabel('RA')
p.ylabel('DEC')
p.grid()
p.savefig(fig_out)
p.clf()


fig_out = os.path.join(fig_dir, 'DEC_vs_zr.png')

X = data['redshift_R']
Y = data['DEC']
p.figure(1, (6., 5.5))
p.tight_layout()
p.plot(X, Y, 'k,', rasterized=True)
p.xlabel('redshift_R')
p.ylabel('DEC')
p.grid()
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'g_lat_vs_g_lon.png')

X = data['g_lon']
Y = data['g_lat']
p.figure(1, (6., 5.5))
p.tight_layout()
p.plot(X, Y, 'k,', rasterized=True)
p.xlabel('g_lon')
p.ylabel('g_lat')
p.grid()
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'ecl_lat_vs_ecl_lon.png')

X = data['ecl_lon']
Y = data['ecl_lat']
p.figure(1, (6., 5.5))
p.tight_layout()
p.plot(X, Y, 'k,', rasterized=True)
p.xlabel('ecl_lon')
p.ylabel('ecl_lat')
p.grid()
p.savefig(fig_out)
p.clf()
