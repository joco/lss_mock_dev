import numpy as n
import os
import sys
from astropy.table import Table, Column
from tqdm import tqdm

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from scipy.interpolate import interp1d
nl = lambda selection : len(selection.nonzero()[0])

l_box = 2000.
env = "UCHUU"
#print('runs geometry.py with arguments ')
#print(sys.argv)

cosmo = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
h=0.6774

N_snap, Z_snap, A_snap, DC_max, DC_min, N_replicas = n.loadtxt(os.path.join( os.environ[env], 'snap_list_with_border.txt'), unpack = True)


z_names = n.array([  "z0p00" #  0
					,"z0p02" #  1
					,"z0p05" #  2
					,"z0p09" #  3
					,"z0p14" #  4
					,"z0p19" #  5
					,"z0p25" #  6
					,"z0p30" #  7
					,"z0p36" #  8
					,"z0p43" #  9
					,"z0p49" # 10
					,"z0p56" # 11
					,"z0p63" # 12
					,"z0p70" # 13
					,"z0p78" # 14
					,"z0p86" # 15
					,"z0p94" # 16
					,"z1p03" # 17
					,"z1p12" # 18
					,"z1p22" # 19
					,"z1p32" # 20
					,"z1p43" # 21
					,"z1p54" # 22
					,"z1p65" # 23
					,"z1p77" # 24
					,"z1p90" # 25
					,"z2p03" # 26
					,"z2p17" # 27
					,"z2p31" # 28
					,"z2p46" # 29
					,"z2p62" # 30
					,"z2p78" # 31
					,"z2p95" # 32
					,"z3p13" # 33
					,"z3p32" # 34
					,"z3p61" # 35
					,"z3p93" # 36
					,"z4p27" # 37
					,"z4p63" # 38
					,"z5p15" # 39
					,"z5p73" # 40
					,"z6p35" # 41
					,"z7p03" #
					,"z7p76" #
					,"z8p59" #
					,"z9p47" #
					,"z10p44"
					,"z11p51"
					,"z12p69"
					,"z13p96" ])[::-1]


# deduce the L_box without h
L_box = l_box / h

def is_in_box(x, y, z, x0=0, y0=0, z0=0):
    return (x>=x0)&(y>=y0)&(z>=z0)& (x<x0+L_box)&(y<y0+L_box)&(z<z0+L_box)

for jj_snap in n.arange(len(N_snap))[::-1]:
    out_name = z_names[jj_snap]
    print(N_snap [jj_snap], Z_snap [jj_snap], A_snap [jj_snap], DC_max [jj_snap], DC_min [jj_snap], N_replicas [jj_snap], out_name)

    NN = N_replicas[jj_snap]
    if NN==4.0:
        N_samp = 1000
    elif NN==3.0:
        N_samp = 500
    elif NN<3.0:
        N_samp = 2500
    phi_0 = n.linspace(0, n.pi, N_samp)[:-1]
    theta_0 = n.linspace(0, 2 * n.pi, N_samp*2)[:-1]
    phis, thetas = n.meshgrid(phi_0, theta_0)
    phi = n.ravel(phis)
    theta = n.ravel(thetas)


    # Creates the regular pavement for the replication
    pts_i = n.arange(-1 * NN, NN, 1)
    ix_i, iy_i, iz_i = n.meshgrid(pts_i, pts_i, pts_i)
    ix, iy, iz = n.ravel(ix_i), n.ravel(iy_i), n.ravel(iz_i)
    # print(ix,iy,iz)
    pts = pts_i * L_box
    x0_i, y0_i, z0_i = n.meshgrid(pts, pts, pts)
    x0, y0, z0 = n.ravel(x0_i), n.ravel(y0_i), n.ravel(z0_i)
    XT = n.transpose([x0, y0, z0])
    iXT = n.transpose([ix, iy, iz])
    t = Table()
    t['jx'] = ix
    t['jy'] = iy
    t['jz'] = iz
    t['area_DC_max'] = 0.
    t['area_DC_min'] = 0.

    r=DC_max[jj_snap]
    x = r * n.cos(theta) * n.sin(phi)
    y = r * n.sin(theta) * n.sin(phi)
    z = r * n.cos(phi)
    for jj in tqdm(range(len(iXT))):
        jx,jy,jz=iXT[jj]
        inbox_000 = n.array([ is_in_box(x, y, z, jx*L_box,jy*L_box,jz*L_box) for x,y,z in zip(x, y, z) ])
        #print(jx,jy,jz,n.round(nl(inbox_000)/len(x)*129600./n.pi,1))
        t['area_DC_max'][jj] = nl(inbox_000)/len(x)*129600./n.pi

    r=DC_min[jj_snap]
    x = r * n.cos(theta) * n.sin(phi)
    y = r * n.sin(theta) * n.sin(phi)
    z = r * n.cos(phi)

    for jj in tqdm(range(len(iXT))):
        jx,jy,jz=iXT[jj]
        inbox_000 = n.array([ is_in_box(x, y, z, jx*L_box,jy*L_box,jz*L_box) for x,y,z in zip(x, y, z) ])
        #print(jx,jy,jz,n.round(nl(inbox_000)/len(x)*129600./n.pi,1))
        t['area_DC_min'][jj] = nl(inbox_000)/len(x)*129600./n.pi
    p_2_out = os.path.join( os.environ[env], 'area_per_replica_'+out_name+'.fits')
    t.write(p_2_out, overwrite = True)
    print(p_2_out, 'written')

for jj_snap in n.arange(len(N_snap))[::-1]:
    out_name = z_names[jj_snap]
    p_2_out = os.path.join( os.environ[env], 'area_per_replica_'+out_name+'.fits')
    tt = Table.read(p_2_out)
    print(out_name, len(tt['area_DC_min'][tt['area_DC_min']>0]), len(tt['area_DC_max'][tt['area_DC_max']>0]))
