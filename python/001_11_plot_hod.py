"""

"""
import os, sys, glob
import numpy as n
from astropy.table import Table, Column

# plotting 
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p
import matplotlib.pylab as pl

env = 'UNIT_fA1i_DIR'
str_f_sat = sys.argv[1] # '10'
str_sigma = sys.argv[2] # '1.0'
print(sys.argv)
gal_dir = os.path.join( os.environ['GIT_AGN_MOCK'], 'data', 'UNIT_fA1i_DIR', 'cat_GALAXY_all', 'HIST' )

path_2_HistOut_files = n.array( glob.glob( os.path.join( gal_dir, 'HistSat_Mvir_zz_000???.ascii' )))
path_2_HistCEN_files = n.array( glob.glob( os.path.join( gal_dir, 'Hist_Mvir_zz_000???.ascii' )))
H2ds, H2Cs = [], []
for p1, p2 in zip(path_2_HistOut_files,path_2_HistCEN_files):
	H2ds.append(n.loadtxt(p1))
	H2Cs.append(n.loadtxt(p2))

H2ds = n.array( H2ds ) 
H2Cs = n.array( H2Cs ) 
H2S_sum = H2ds.sum( axis = 0 )
H2C_sum = H2Cs.sum( axis = 0 )
#H2C_sum = H2A_sum - H2S_sum

#p2CAT = os.path.join( os.environ[env],'CAT_WTH_fsat_' + str_f_sat + '_sigma_' + str_sigma + '.fits' )
p2CAT = os.path.join( os.environ[env],'CAT_WTH_fsat_' + str_f_sat + '_sigma_' + str_sigma + '_zmax_2.0_FXmin_-15.0.fits' )
print('opens', p2CAT)
f0 = Table.read(p2CAT)

# define samples of interest : 
# redshift
# LX cuts
all_monopoles = n.array( glob.glob( os.path.join( os.environ['GIT_2PCF'], 'data/2pcf_efeds', 'FZbs_ERO_LX_soft_gt_*monopole.2pcf.fits' ) ) )
L_mins = n.array([ float(os.path.basename(el).split('_')[5])/100. for el in all_monopoles ])
Z_mins = n.array([ float(os.path.basename(el).split('_')[7]) for el in all_monopoles ])
Z_maxs = n.array([ float(os.path.basename(el).split('_')[9]) for el in all_monopoles ])

z_bins = n.arange(0,6.2, 0.1)
dz = 0.1/2.
M_bins = n.arange(11.4, 15.5, 0.1)
dm = 0.05 
xM = M_bins[:-1] + dm

for z0, z1, l0, p2mono in zip(Z_mins, Z_maxs, L_mins, all_monopoles):
	print(z0, z1, l0, p2mono)
	s1 = ( f0['redshift_R'] > z0 ) & ( f0['redshift_R'] < z1 ) & ( f0['LX_soft'] > l0 ) 
	f1 = f0[s1]
	MockAll = n.histogram2d( f1['CENTRAL_Mvir'], f1['redshift_R'], bins=[n.arange(11.4, 15.5, 0.1), n.arange(0,6.2, 0.1)])[0]
	cen_agn=(f1['HALO_pid']==-1)
	MockCen = n.histogram2d( f1['CENTRAL_Mvir'][cen_agn], f1['redshift_R'][cen_agn], bins=[n.arange(11.4, 15.5, 0.1), n.arange(0,6.2, 0.1)])[0]
	MockSat = MockAll-MockCen
	# HOD
	Ntot_z = n.sum(MockCen.T, axis=1)
	selection = (Ntot_z>1)
	c_val = z_bins[:-1][selection] + dz
	c_val_norm = (c_val - n.min(c_val) ) / n.max((c_val - n.min(c_val) ))
	colors = pl.cm.RdYlGn_r(c_val_norm)
	outname = os.path.join( os.environ['GIT_AGN_MOCK'], 'data', 'SHAM_HOD', 'HOD_fsat_' + str_f_sat + '_sigma_' + str_sigma + os.path.basename(p2mono)[:-19])
	allz = z_bins[:-1][selection]+dz
	t = Table () 
	t['M_low'] = n.round(xM - dm, 2)
	t['M_mid'] = n.round(xM     , 2)
	t['M_hig'] = n.round(xM + dm, 2)
	N_bin = MockCen.T[selection].sum()*0.001
	allx, ally, allc, allNS, allNC, allHH = [], [], [], [], [], []
	for mC, mS, Hc, z_i, c_i in zip ( MockCen.T[selection], MockSat.T[selection], H2C_sum.T[selection], allz, colors ):
		print(mC, mS, Hc, z_i, c_i)
		if n.sum(mC)>N_bin:
			NC = mC/Hc
			NS = mS/Hc
			HOD = NC + NS
			#HOD.T 
			#t['mC' + '_' + str(z_i)] = mC
			#t['mS' + '_' + str(z_i)] = mS
			#t['Hc' + '_' + str(z_i)] = Hc
			#t['z'  + '_' + str(z_i)] = z_i*n.ones_like(xM)
			p.plot( xM, HOD, color=c_i, lw=2 )
			p.plot( xM, NS, color=c_i, lw=1, ls='dotted' )
			p.plot( xM, NC, color=c_i, lw=1, ls='dashed' )
			allx.append(xM)
			ally.append(HOD)
			allNS.append(mS)
			allNC.append(mC)
			allHH.append(Hc)
			allc.append( z_i*n.ones_like(xM) )
	
	NS_all = n.sum(n.array(allNS), axis=0)
	NC_all = n.sum(n.array(allNC), axis=0)
	HH_all = n.sum(n.array(allHH), axis=0)
	t['NS_all'] = NS_all.astype( 'int' )
	t['NC_all'] = NC_all.astype( 'int' )
	t['HH_all'] = HH_all.astype( 'int' )
	t['z_min'] = n.round( allz.min() - dz , 2 )
	t['z_max'] = n.round( allz.max() + dz , 2 )
	p.plot(xM, ( NS_all + NC_all ) / HH_all, color='k', lw=4, label='mean' )
	p.plot(xM, NS_all / HH_all, color='k' , lw=2, ls='dotted')
	p.plot(xM, NC_all / HH_all, color='k' , lw=2, ls='dashed')
	p.scatter(n.hstack((allx))-1000, n.hstack((ally)), s=30, c=n.vstack((allc)), cmap=pl.cm.RdYlGn_r, vmin=allz.min(), vmax = allz.max() )
	p.colorbar(label='redshift')
	p.xlabel(r"$M_{vir}$ [M$_\odot$]")
	p.ylabel(r"$\langle N(M) \rangle$")
	p.legend(fontsize=14, loc=2, ncol=1,title=r'$f_{sat}=$' + str_f_sat + r', $\sigma=$' + str_sigma )
	p.tight_layout()
	p.yscale('log')
	p.xlim(( 11.4, 15 ))
	t.write( outname + '.fits', overwrite = True)
	p.savefig( outname + '.png' )
	p.clf()
	print(outname, 'done')
	print( t )


