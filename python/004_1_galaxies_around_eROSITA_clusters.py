"""
"""

import time
t0 = time.time()

import healpy
import sys, os, json

from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column, vstack, hstack
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import glob
from sklearn.neighbors import BallTree
from scipy.interpolate import interp1d
import astropy.io.fits as fits
import numpy as n
	

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
cosmoMD = FlatLambdaCDM(
	H0=67.77 * u.km / u.s / u.Mpc,
	Om0=0.307115)  # , Ob0=0.048206)
h = 0.6777
L_box = 1000.0 / h
cosmo = cosmoMD

nl = lambda selection: len(selection.nonzero()[0])

env =  sys.argv[1] 
# "MD10" 
delta_crit = '200c'
CLU_catalog_name = sys.argv[2] 
# env + "_eRO_CLU_b8_CM_0_pixS_20.0_wCount_June2020.fit" 
print(env, delta_crit, CLU_catalog_name)

path_2_CLU_catalog = os.path.join(os.environ[env], CLU_catalog_name )
hdu_clu_i = Table.read(path_2_CLU_catalog)

area_erosita = ( hdu_clu_i['DEC'] > -80. ) & ( hdu_clu_i['DEC'] < 5. ) & ( abs( hdu_clu_i['g_lon'] ) > 180. ) & ( abs( hdu_clu_i['g_lat'] ) > 10. ) 
if env=="MD10":
    erosita_depth = ( hdu_clu_i['COUNTS_05_20_1p0_r500c_CLU_self'] > 50 )
elif env=="UNIT_fA1i_DIR":
    erosita_depth = (abs(hdu_clu_i['g_lat'])>15 ) & (  hdu_clu_i['g_lon']>180 ) & (  hdu_clu_i['HALO_pid']==-1 ) & (  hdu_clu_i['galaxy_SMHMR_mass']>1 ) & (  n.log10(hdu_clu_i['CLUSTER_FX_soft'])>hdu_clu_i['flux_limit_eRASS8_pt']+0.25 )
else:
    erosita_depth = ( hdu_clu_i['CLUSTER_FX_soft']>1e-15)

keep = ( hdu_clu_i['redshift_R'] < 1.5 ) & ( hdu_clu_i['galaxy_SMHMR_mass'] > 7 ) & ( area_erosita ) & ( erosita_depth ) & (hdu_clu_i['HALO_pid'] ==-1)

hdu_clu = hdu_clu_i[keep]

# determine matching radius by converting Rvir 
rvir_CLU = hdu_clu['HALO_Rvir']
x_coord_CLU = hdu_clu['HALO_x']
y_coord_CLU = hdu_clu['HALO_y']
z_coord_CLU = hdu_clu['HALO_z']

omega = lambda zz: cosmo.Om0*(1+zz)**3. / cosmo.efunc(zz)**2
DeltaVir_bn98 = lambda zz : (18.*n.pi**2. + 82.*(omega(zz)-1)- 39.*(omega(zz)-1)**2.) /omega(zz)

HOST_HALO_Mvir = hdu_clu['HALO_Mvir'] / h
HOST_HALO_Rvir = hdu_clu['HALO_Rvir']
HOST_HALO_M200c = hdu_clu['HALO_M200c'] / h
HOST_HALO_R200c = (DeltaVir_bn98(hdu_clu['redshift_R'])/200. * HOST_HALO_M200c / HOST_HALO_Mvir)**(1./3.)*HOST_HALO_Rvir

frac_rvir = HOST_HALO_R200c/HOST_HALO_Rvir
RADIUS = HOST_HALO_R200c

HEALPIX_8_all = n.unique(healpy.ang2pix(8, n.pi/2. - hdu_clu['DEC']*n.pi/180. , hdu_clu['RA']*n.pi/180. , nest=True))

for col_name in list( hdu_clu.columns.keys() ):
	hdu_clu[col_name].name = "HOST_"+col_name 


# loop over galaxy healpix pixels

def get_CGAL(HEALPIX_id = 0):
    print(HEALPIX_id)
    dir_2_gal_all = os.path.join(os.environ[env], "cat_GALAXY_all")
    path_2_gal_all_catalog = os.path.join(dir_2_gal_all, str(HEALPIX_id).zfill(6) + '.fit')
    path_2_out_file = os.path.join(os.environ[env], "cat_eROCGAL", str(HEALPIX_id).zfill(6) + '.fit' )
    if os.path.isfile(path_2_out_file)==False:
        hd_all = Table.read(path_2_gal_all_catalog)
        N_GAL_all = len(hd_all['RA'])

        x = hd_all['HALO_x']
        y = hd_all['HALO_y']
        z = hd_all['HALO_z']

        Tree_obj = BallTree(n.transpose([x, y, z]))
        idx_arrays, distances = Tree_obj.query_radius(n.transpose([x_coord_CLU, y_coord_CLU, z_coord_CLU]), r = RADIUS/1000. , return_distance=True)
        N_in_test = n.array([len(t_i) for t_i in idx_arrays])

        N_in_test = n.array([len(t_i) for t_i in idx_arrays])
        # cluster ID: line in the cluster file for each satellite
        # for hdu_clu
        CLU_ids = n.hstack((n.array([n.ones(el) * ii for ii, el in enumerate(N_in_test)]))).astype('int')
        # IDs of the satellite in the galaxy and coordinate file
        # for f1, f2, f3
        sf = n.hstack((idx_arrays))
        print(HEALPIX_id, nl(sf))
        if nl(sf)>0:
            t = hstack(( hd_all[sf], hdu_clu[CLU_ids] ))
            initial_ids = n.arange(N_GAL_all)[sf]
            t['line_id_cat_GALAXY_all'] = initial_ids
            print(t.columns.keys())
            t.write(path_2_out_file, overwrite=True)
            print('written to ',path_2_out_file, 'after ', time.time()-t0, ' seconds')

for HEALPIX_id in HEALPIX_8_all[::-1]:
    get_CGAL(HEALPIX_id)
