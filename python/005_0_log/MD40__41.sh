#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 574 
python3 005_0_sham_cosmology_catalogs.py MD40 575 
python3 005_0_sham_cosmology_catalogs.py MD40 576 
python3 005_0_sham_cosmology_catalogs.py MD40 577 
python3 005_0_sham_cosmology_catalogs.py MD40 578 
python3 005_0_sham_cosmology_catalogs.py MD40 579 
python3 005_0_sham_cosmology_catalogs.py MD40 580 
python3 005_0_sham_cosmology_catalogs.py MD40 581 
python3 005_0_sham_cosmology_catalogs.py MD40 582 
python3 005_0_sham_cosmology_catalogs.py MD40 583 
python3 005_0_sham_cosmology_catalogs.py MD40 584 
python3 005_0_sham_cosmology_catalogs.py MD40 585 
python3 005_0_sham_cosmology_catalogs.py MD40 586 
python3 005_0_sham_cosmology_catalogs.py MD40 587 
 
