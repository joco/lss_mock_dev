#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 588 
python3 005_0_sham_cosmology_catalogs.py MD40 589 
python3 005_0_sham_cosmology_catalogs.py MD40 590 
python3 005_0_sham_cosmology_catalogs.py MD40 591 
python3 005_0_sham_cosmology_catalogs.py MD40 592 
python3 005_0_sham_cosmology_catalogs.py MD40 593 
python3 005_0_sham_cosmology_catalogs.py MD40 594 
python3 005_0_sham_cosmology_catalogs.py MD40 595 
python3 005_0_sham_cosmology_catalogs.py MD40 596 
python3 005_0_sham_cosmology_catalogs.py MD40 597 
python3 005_0_sham_cosmology_catalogs.py MD40 598 
python3 005_0_sham_cosmology_catalogs.py MD40 599 
python3 005_0_sham_cosmology_catalogs.py MD40 600 
python3 005_0_sham_cosmology_catalogs.py MD40 601 
 
