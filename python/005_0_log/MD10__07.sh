#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD10 98 
python3 005_0_sham_cosmology_catalogs.py MD10 99 
python3 005_0_sham_cosmology_catalogs.py MD10 100 
python3 005_0_sham_cosmology_catalogs.py MD10 101 
python3 005_0_sham_cosmology_catalogs.py MD10 102 
python3 005_0_sham_cosmology_catalogs.py MD10 103 
python3 005_0_sham_cosmology_catalogs.py MD10 104 
python3 005_0_sham_cosmology_catalogs.py MD10 105 
python3 005_0_sham_cosmology_catalogs.py MD10 106 
python3 005_0_sham_cosmology_catalogs.py MD10 107 
python3 005_0_sham_cosmology_catalogs.py MD10 108 
python3 005_0_sham_cosmology_catalogs.py MD10 109 
python3 005_0_sham_cosmology_catalogs.py MD10 110 
python3 005_0_sham_cosmology_catalogs.py MD10 111 
 
