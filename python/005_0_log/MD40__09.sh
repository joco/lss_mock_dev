#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 126 
python3 005_0_sham_cosmology_catalogs.py MD40 127 
python3 005_0_sham_cosmology_catalogs.py MD40 128 
python3 005_0_sham_cosmology_catalogs.py MD40 129 
python3 005_0_sham_cosmology_catalogs.py MD40 130 
python3 005_0_sham_cosmology_catalogs.py MD40 131 
python3 005_0_sham_cosmology_catalogs.py MD40 132 
python3 005_0_sham_cosmology_catalogs.py MD40 133 
python3 005_0_sham_cosmology_catalogs.py MD40 134 
python3 005_0_sham_cosmology_catalogs.py MD40 135 
python3 005_0_sham_cosmology_catalogs.py MD40 136 
python3 005_0_sham_cosmology_catalogs.py MD40 137 
python3 005_0_sham_cosmology_catalogs.py MD40 138 
python3 005_0_sham_cosmology_catalogs.py MD40 139 
 
