#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD10 728 
python3 005_0_sham_cosmology_catalogs.py MD10 729 
python3 005_0_sham_cosmology_catalogs.py MD10 730 
python3 005_0_sham_cosmology_catalogs.py MD10 731 
python3 005_0_sham_cosmology_catalogs.py MD10 732 
python3 005_0_sham_cosmology_catalogs.py MD10 733 
python3 005_0_sham_cosmology_catalogs.py MD10 734 
python3 005_0_sham_cosmology_catalogs.py MD10 735 
python3 005_0_sham_cosmology_catalogs.py MD10 736 
python3 005_0_sham_cosmology_catalogs.py MD10 737 
python3 005_0_sham_cosmology_catalogs.py MD10 738 
python3 005_0_sham_cosmology_catalogs.py MD10 739 
python3 005_0_sham_cosmology_catalogs.py MD10 740 
python3 005_0_sham_cosmology_catalogs.py MD10 741 
 
