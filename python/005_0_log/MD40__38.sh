#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 532 
python3 005_0_sham_cosmology_catalogs.py MD40 533 
python3 005_0_sham_cosmology_catalogs.py MD40 534 
python3 005_0_sham_cosmology_catalogs.py MD40 535 
python3 005_0_sham_cosmology_catalogs.py MD40 536 
python3 005_0_sham_cosmology_catalogs.py MD40 537 
python3 005_0_sham_cosmology_catalogs.py MD40 538 
python3 005_0_sham_cosmology_catalogs.py MD40 539 
python3 005_0_sham_cosmology_catalogs.py MD40 540 
python3 005_0_sham_cosmology_catalogs.py MD40 541 
python3 005_0_sham_cosmology_catalogs.py MD40 542 
python3 005_0_sham_cosmology_catalogs.py MD40 543 
python3 005_0_sham_cosmology_catalogs.py MD40 544 
python3 005_0_sham_cosmology_catalogs.py MD40 545 
 
