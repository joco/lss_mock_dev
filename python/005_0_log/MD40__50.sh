#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 700 
python3 005_0_sham_cosmology_catalogs.py MD40 701 
python3 005_0_sham_cosmology_catalogs.py MD40 702 
python3 005_0_sham_cosmology_catalogs.py MD40 703 
python3 005_0_sham_cosmology_catalogs.py MD40 704 
python3 005_0_sham_cosmology_catalogs.py MD40 705 
python3 005_0_sham_cosmology_catalogs.py MD40 706 
python3 005_0_sham_cosmology_catalogs.py MD40 707 
python3 005_0_sham_cosmology_catalogs.py MD40 708 
python3 005_0_sham_cosmology_catalogs.py MD40 709 
python3 005_0_sham_cosmology_catalogs.py MD40 710 
python3 005_0_sham_cosmology_catalogs.py MD40 711 
python3 005_0_sham_cosmology_catalogs.py MD40 712 
python3 005_0_sham_cosmology_catalogs.py MD40 713 
 
