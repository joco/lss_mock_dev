#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 378 
python3 005_0_sham_cosmology_catalogs.py MD40 379 
python3 005_0_sham_cosmology_catalogs.py MD40 380 
python3 005_0_sham_cosmology_catalogs.py MD40 381 
python3 005_0_sham_cosmology_catalogs.py MD40 382 
python3 005_0_sham_cosmology_catalogs.py MD40 383 
python3 005_0_sham_cosmology_catalogs.py MD40 384 
python3 005_0_sham_cosmology_catalogs.py MD40 385 
python3 005_0_sham_cosmology_catalogs.py MD40 386 
python3 005_0_sham_cosmology_catalogs.py MD40 387 
python3 005_0_sham_cosmology_catalogs.py MD40 388 
python3 005_0_sham_cosmology_catalogs.py MD40 389 
python3 005_0_sham_cosmology_catalogs.py MD40 390 
python3 005_0_sham_cosmology_catalogs.py MD40 391 
 
