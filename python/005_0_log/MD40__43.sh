#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 602 
python3 005_0_sham_cosmology_catalogs.py MD40 603 
python3 005_0_sham_cosmology_catalogs.py MD40 604 
python3 005_0_sham_cosmology_catalogs.py MD40 605 
python3 005_0_sham_cosmology_catalogs.py MD40 606 
python3 005_0_sham_cosmology_catalogs.py MD40 607 
python3 005_0_sham_cosmology_catalogs.py MD40 608 
python3 005_0_sham_cosmology_catalogs.py MD40 609 
python3 005_0_sham_cosmology_catalogs.py MD40 610 
python3 005_0_sham_cosmology_catalogs.py MD40 611 
python3 005_0_sham_cosmology_catalogs.py MD40 612 
python3 005_0_sham_cosmology_catalogs.py MD40 613 
python3 005_0_sham_cosmology_catalogs.py MD40 614 
python3 005_0_sham_cosmology_catalogs.py MD40 615 
 
