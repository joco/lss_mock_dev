#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 70 
python3 005_0_sham_cosmology_catalogs.py MD40 71 
python3 005_0_sham_cosmology_catalogs.py MD40 72 
python3 005_0_sham_cosmology_catalogs.py MD40 73 
python3 005_0_sham_cosmology_catalogs.py MD40 74 
python3 005_0_sham_cosmology_catalogs.py MD40 75 
python3 005_0_sham_cosmology_catalogs.py MD40 76 
python3 005_0_sham_cosmology_catalogs.py MD40 77 
python3 005_0_sham_cosmology_catalogs.py MD40 78 
python3 005_0_sham_cosmology_catalogs.py MD40 79 
python3 005_0_sham_cosmology_catalogs.py MD40 80 
python3 005_0_sham_cosmology_catalogs.py MD40 81 
python3 005_0_sham_cosmology_catalogs.py MD40 82 
python3 005_0_sham_cosmology_catalogs.py MD40 83 
 
