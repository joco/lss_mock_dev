#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 406 
python3 005_0_sham_cosmology_catalogs.py MD40 407 
python3 005_0_sham_cosmology_catalogs.py MD40 408 
python3 005_0_sham_cosmology_catalogs.py MD40 409 
python3 005_0_sham_cosmology_catalogs.py MD40 410 
python3 005_0_sham_cosmology_catalogs.py MD40 411 
python3 005_0_sham_cosmology_catalogs.py MD40 412 
python3 005_0_sham_cosmology_catalogs.py MD40 413 
python3 005_0_sham_cosmology_catalogs.py MD40 414 
python3 005_0_sham_cosmology_catalogs.py MD40 415 
python3 005_0_sham_cosmology_catalogs.py MD40 416 
python3 005_0_sham_cosmology_catalogs.py MD40 417 
python3 005_0_sham_cosmology_catalogs.py MD40 418 
python3 005_0_sham_cosmology_catalogs.py MD40 419 
 
