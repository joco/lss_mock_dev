#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 560 
python3 005_0_sham_cosmology_catalogs.py MD40 561 
python3 005_0_sham_cosmology_catalogs.py MD40 562 
python3 005_0_sham_cosmology_catalogs.py MD40 563 
python3 005_0_sham_cosmology_catalogs.py MD40 564 
python3 005_0_sham_cosmology_catalogs.py MD40 565 
python3 005_0_sham_cosmology_catalogs.py MD40 566 
python3 005_0_sham_cosmology_catalogs.py MD40 567 
python3 005_0_sham_cosmology_catalogs.py MD40 568 
python3 005_0_sham_cosmology_catalogs.py MD40 569 
python3 005_0_sham_cosmology_catalogs.py MD40 570 
python3 005_0_sham_cosmology_catalogs.py MD40 571 
python3 005_0_sham_cosmology_catalogs.py MD40 572 
python3 005_0_sham_cosmology_catalogs.py MD40 573 
 
