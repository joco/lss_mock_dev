#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 224 
python3 005_0_sham_cosmology_catalogs.py MD40 225 
python3 005_0_sham_cosmology_catalogs.py MD40 226 
python3 005_0_sham_cosmology_catalogs.py MD40 227 
python3 005_0_sham_cosmology_catalogs.py MD40 228 
python3 005_0_sham_cosmology_catalogs.py MD40 229 
python3 005_0_sham_cosmology_catalogs.py MD40 230 
python3 005_0_sham_cosmology_catalogs.py MD40 231 
python3 005_0_sham_cosmology_catalogs.py MD40 232 
python3 005_0_sham_cosmology_catalogs.py MD40 233 
python3 005_0_sham_cosmology_catalogs.py MD40 234 
python3 005_0_sham_cosmology_catalogs.py MD40 235 
python3 005_0_sham_cosmology_catalogs.py MD40 236 
python3 005_0_sham_cosmology_catalogs.py MD40 237 
 
