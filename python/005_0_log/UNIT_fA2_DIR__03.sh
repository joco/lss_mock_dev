#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 42 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 43 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 44 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 45 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 46 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 47 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 48 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 49 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 50 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 51 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 52 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 53 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 54 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 55 
 
