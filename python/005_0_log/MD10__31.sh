#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD10 434 
python3 005_0_sham_cosmology_catalogs.py MD10 435 
python3 005_0_sham_cosmology_catalogs.py MD10 436 
python3 005_0_sham_cosmology_catalogs.py MD10 437 
python3 005_0_sham_cosmology_catalogs.py MD10 438 
python3 005_0_sham_cosmology_catalogs.py MD10 439 
python3 005_0_sham_cosmology_catalogs.py MD10 440 
python3 005_0_sham_cosmology_catalogs.py MD10 441 
python3 005_0_sham_cosmology_catalogs.py MD10 442 
python3 005_0_sham_cosmology_catalogs.py MD10 443 
python3 005_0_sham_cosmology_catalogs.py MD10 444 
python3 005_0_sham_cosmology_catalogs.py MD10 445 
python3 005_0_sham_cosmology_catalogs.py MD10 446 
python3 005_0_sham_cosmology_catalogs.py MD10 447 
 
