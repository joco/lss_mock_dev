#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD10 14 
python3 005_0_sham_cosmology_catalogs.py MD10 15 
python3 005_0_sham_cosmology_catalogs.py MD10 16 
python3 005_0_sham_cosmology_catalogs.py MD10 17 
python3 005_0_sham_cosmology_catalogs.py MD10 18 
python3 005_0_sham_cosmology_catalogs.py MD10 19 
python3 005_0_sham_cosmology_catalogs.py MD10 20 
python3 005_0_sham_cosmology_catalogs.py MD10 21 
python3 005_0_sham_cosmology_catalogs.py MD10 22 
python3 005_0_sham_cosmology_catalogs.py MD10 23 
python3 005_0_sham_cosmology_catalogs.py MD10 24 
python3 005_0_sham_cosmology_catalogs.py MD10 25 
python3 005_0_sham_cosmology_catalogs.py MD10 26 
python3 005_0_sham_cosmology_catalogs.py MD10 27 
 
