#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD10 462 
python3 005_0_sham_cosmology_catalogs.py MD10 463 
python3 005_0_sham_cosmology_catalogs.py MD10 464 
python3 005_0_sham_cosmology_catalogs.py MD10 465 
python3 005_0_sham_cosmology_catalogs.py MD10 466 
python3 005_0_sham_cosmology_catalogs.py MD10 467 
python3 005_0_sham_cosmology_catalogs.py MD10 468 
python3 005_0_sham_cosmology_catalogs.py MD10 469 
python3 005_0_sham_cosmology_catalogs.py MD10 470 
python3 005_0_sham_cosmology_catalogs.py MD10 471 
python3 005_0_sham_cosmology_catalogs.py MD10 472 
python3 005_0_sham_cosmology_catalogs.py MD10 473 
python3 005_0_sham_cosmology_catalogs.py MD10 474 
python3 005_0_sham_cosmology_catalogs.py MD10 475 
 
