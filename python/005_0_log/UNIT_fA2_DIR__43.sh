#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 602 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 603 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 604 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 605 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 606 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 607 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 608 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 609 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 610 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 611 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 612 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 613 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 614 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 615 
 
