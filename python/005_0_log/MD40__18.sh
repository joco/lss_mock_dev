#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 252 
python3 005_0_sham_cosmology_catalogs.py MD40 253 
python3 005_0_sham_cosmology_catalogs.py MD40 254 
python3 005_0_sham_cosmology_catalogs.py MD40 255 
python3 005_0_sham_cosmology_catalogs.py MD40 256 
python3 005_0_sham_cosmology_catalogs.py MD40 257 
python3 005_0_sham_cosmology_catalogs.py MD40 258 
python3 005_0_sham_cosmology_catalogs.py MD40 259 
python3 005_0_sham_cosmology_catalogs.py MD40 260 
python3 005_0_sham_cosmology_catalogs.py MD40 261 
python3 005_0_sham_cosmology_catalogs.py MD40 262 
python3 005_0_sham_cosmology_catalogs.py MD40 263 
python3 005_0_sham_cosmology_catalogs.py MD40 264 
python3 005_0_sham_cosmology_catalogs.py MD40 265 
 
