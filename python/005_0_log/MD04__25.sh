#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD04 350 
python3 005_0_sham_cosmology_catalogs.py MD04 351 
python3 005_0_sham_cosmology_catalogs.py MD04 352 
python3 005_0_sham_cosmology_catalogs.py MD04 353 
python3 005_0_sham_cosmology_catalogs.py MD04 354 
python3 005_0_sham_cosmology_catalogs.py MD04 355 
python3 005_0_sham_cosmology_catalogs.py MD04 356 
python3 005_0_sham_cosmology_catalogs.py MD04 357 
python3 005_0_sham_cosmology_catalogs.py MD04 358 
python3 005_0_sham_cosmology_catalogs.py MD04 359 
python3 005_0_sham_cosmology_catalogs.py MD04 360 
python3 005_0_sham_cosmology_catalogs.py MD04 361 
python3 005_0_sham_cosmology_catalogs.py MD04 362 
python3 005_0_sham_cosmology_catalogs.py MD04 363 
 
