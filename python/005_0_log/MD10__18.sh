#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD10 252 
python3 005_0_sham_cosmology_catalogs.py MD10 253 
python3 005_0_sham_cosmology_catalogs.py MD10 254 
python3 005_0_sham_cosmology_catalogs.py MD10 255 
python3 005_0_sham_cosmology_catalogs.py MD10 256 
python3 005_0_sham_cosmology_catalogs.py MD10 257 
python3 005_0_sham_cosmology_catalogs.py MD10 258 
python3 005_0_sham_cosmology_catalogs.py MD10 259 
python3 005_0_sham_cosmology_catalogs.py MD10 260 
python3 005_0_sham_cosmology_catalogs.py MD10 261 
python3 005_0_sham_cosmology_catalogs.py MD10 262 
python3 005_0_sham_cosmology_catalogs.py MD10 263 
python3 005_0_sham_cosmology_catalogs.py MD10 264 
python3 005_0_sham_cosmology_catalogs.py MD10 265 
 
