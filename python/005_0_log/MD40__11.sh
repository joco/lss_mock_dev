#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 154 
python3 005_0_sham_cosmology_catalogs.py MD40 155 
python3 005_0_sham_cosmology_catalogs.py MD40 156 
python3 005_0_sham_cosmology_catalogs.py MD40 157 
python3 005_0_sham_cosmology_catalogs.py MD40 158 
python3 005_0_sham_cosmology_catalogs.py MD40 159 
python3 005_0_sham_cosmology_catalogs.py MD40 160 
python3 005_0_sham_cosmology_catalogs.py MD40 161 
python3 005_0_sham_cosmology_catalogs.py MD40 162 
python3 005_0_sham_cosmology_catalogs.py MD40 163 
python3 005_0_sham_cosmology_catalogs.py MD40 164 
python3 005_0_sham_cosmology_catalogs.py MD40 165 
python3 005_0_sham_cosmology_catalogs.py MD40 166 
python3 005_0_sham_cosmology_catalogs.py MD40 167 
 
