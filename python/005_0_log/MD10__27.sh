#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD10 378 
python3 005_0_sham_cosmology_catalogs.py MD10 379 
python3 005_0_sham_cosmology_catalogs.py MD10 380 
python3 005_0_sham_cosmology_catalogs.py MD10 381 
python3 005_0_sham_cosmology_catalogs.py MD10 382 
python3 005_0_sham_cosmology_catalogs.py MD10 383 
python3 005_0_sham_cosmology_catalogs.py MD10 384 
python3 005_0_sham_cosmology_catalogs.py MD10 385 
python3 005_0_sham_cosmology_catalogs.py MD10 386 
python3 005_0_sham_cosmology_catalogs.py MD10 387 
python3 005_0_sham_cosmology_catalogs.py MD10 388 
python3 005_0_sham_cosmology_catalogs.py MD10 389 
python3 005_0_sham_cosmology_catalogs.py MD10 390 
python3 005_0_sham_cosmology_catalogs.py MD10 391 
 
