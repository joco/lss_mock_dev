#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 56 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 57 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 58 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 59 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 60 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 61 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 62 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 63 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 64 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 65 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 66 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 67 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 68 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 69 
 
