#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 56 
python3 005_0_sham_cosmology_catalogs.py MD40 57 
python3 005_0_sham_cosmology_catalogs.py MD40 58 
python3 005_0_sham_cosmology_catalogs.py MD40 59 
python3 005_0_sham_cosmology_catalogs.py MD40 60 
python3 005_0_sham_cosmology_catalogs.py MD40 61 
python3 005_0_sham_cosmology_catalogs.py MD40 62 
python3 005_0_sham_cosmology_catalogs.py MD40 63 
python3 005_0_sham_cosmology_catalogs.py MD40 64 
python3 005_0_sham_cosmology_catalogs.py MD40 65 
python3 005_0_sham_cosmology_catalogs.py MD40 66 
python3 005_0_sham_cosmology_catalogs.py MD40 67 
python3 005_0_sham_cosmology_catalogs.py MD40 68 
python3 005_0_sham_cosmology_catalogs.py MD40 69 
 
