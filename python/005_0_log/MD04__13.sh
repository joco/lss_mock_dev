#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD04 182 
python3 005_0_sham_cosmology_catalogs.py MD04 183 
python3 005_0_sham_cosmology_catalogs.py MD04 184 
python3 005_0_sham_cosmology_catalogs.py MD04 185 
python3 005_0_sham_cosmology_catalogs.py MD04 186 
python3 005_0_sham_cosmology_catalogs.py MD04 187 
python3 005_0_sham_cosmology_catalogs.py MD04 188 
python3 005_0_sham_cosmology_catalogs.py MD04 189 
python3 005_0_sham_cosmology_catalogs.py MD04 190 
python3 005_0_sham_cosmology_catalogs.py MD04 191 
python3 005_0_sham_cosmology_catalogs.py MD04 192 
python3 005_0_sham_cosmology_catalogs.py MD04 193 
python3 005_0_sham_cosmology_catalogs.py MD04 194 
python3 005_0_sham_cosmology_catalogs.py MD04 195 
 
