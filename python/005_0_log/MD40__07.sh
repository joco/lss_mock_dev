#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 98 
python3 005_0_sham_cosmology_catalogs.py MD40 99 
python3 005_0_sham_cosmology_catalogs.py MD40 100 
python3 005_0_sham_cosmology_catalogs.py MD40 101 
python3 005_0_sham_cosmology_catalogs.py MD40 102 
python3 005_0_sham_cosmology_catalogs.py MD40 103 
python3 005_0_sham_cosmology_catalogs.py MD40 104 
python3 005_0_sham_cosmology_catalogs.py MD40 105 
python3 005_0_sham_cosmology_catalogs.py MD40 106 
python3 005_0_sham_cosmology_catalogs.py MD40 107 
python3 005_0_sham_cosmology_catalogs.py MD40 108 
python3 005_0_sham_cosmology_catalogs.py MD40 109 
python3 005_0_sham_cosmology_catalogs.py MD40 110 
python3 005_0_sham_cosmology_catalogs.py MD40 111 
 
