#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD04 322 
python3 005_0_sham_cosmology_catalogs.py MD04 323 
python3 005_0_sham_cosmology_catalogs.py MD04 324 
python3 005_0_sham_cosmology_catalogs.py MD04 325 
python3 005_0_sham_cosmology_catalogs.py MD04 326 
python3 005_0_sham_cosmology_catalogs.py MD04 327 
python3 005_0_sham_cosmology_catalogs.py MD04 328 
python3 005_0_sham_cosmology_catalogs.py MD04 329 
python3 005_0_sham_cosmology_catalogs.py MD04 330 
python3 005_0_sham_cosmology_catalogs.py MD04 331 
python3 005_0_sham_cosmology_catalogs.py MD04 332 
python3 005_0_sham_cosmology_catalogs.py MD04 333 
python3 005_0_sham_cosmology_catalogs.py MD04 334 
python3 005_0_sham_cosmology_catalogs.py MD04 335 
 
