#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 392 
python3 005_0_sham_cosmology_catalogs.py MD40 393 
python3 005_0_sham_cosmology_catalogs.py MD40 394 
python3 005_0_sham_cosmology_catalogs.py MD40 395 
python3 005_0_sham_cosmology_catalogs.py MD40 396 
python3 005_0_sham_cosmology_catalogs.py MD40 397 
python3 005_0_sham_cosmology_catalogs.py MD40 398 
python3 005_0_sham_cosmology_catalogs.py MD40 399 
python3 005_0_sham_cosmology_catalogs.py MD40 400 
python3 005_0_sham_cosmology_catalogs.py MD40 401 
python3 005_0_sham_cosmology_catalogs.py MD40 402 
python3 005_0_sham_cosmology_catalogs.py MD40 403 
python3 005_0_sham_cosmology_catalogs.py MD40 404 
python3 005_0_sham_cosmology_catalogs.py MD40 405 
 
