#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD10 756 
python3 005_0_sham_cosmology_catalogs.py MD10 757 
python3 005_0_sham_cosmology_catalogs.py MD10 758 
python3 005_0_sham_cosmology_catalogs.py MD10 759 
python3 005_0_sham_cosmology_catalogs.py MD10 760 
python3 005_0_sham_cosmology_catalogs.py MD10 761 
python3 005_0_sham_cosmology_catalogs.py MD10 762 
python3 005_0_sham_cosmology_catalogs.py MD10 763 
python3 005_0_sham_cosmology_catalogs.py MD10 764 
python3 005_0_sham_cosmology_catalogs.py MD10 765 
python3 005_0_sham_cosmology_catalogs.py MD10 766 
python3 005_0_sham_cosmology_catalogs.py MD10 767 
 
