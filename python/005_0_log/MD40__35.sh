#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 490 
python3 005_0_sham_cosmology_catalogs.py MD40 491 
python3 005_0_sham_cosmology_catalogs.py MD40 492 
python3 005_0_sham_cosmology_catalogs.py MD40 493 
python3 005_0_sham_cosmology_catalogs.py MD40 494 
python3 005_0_sham_cosmology_catalogs.py MD40 495 
python3 005_0_sham_cosmology_catalogs.py MD40 496 
python3 005_0_sham_cosmology_catalogs.py MD40 497 
python3 005_0_sham_cosmology_catalogs.py MD40 498 
python3 005_0_sham_cosmology_catalogs.py MD40 499 
python3 005_0_sham_cosmology_catalogs.py MD40 500 
python3 005_0_sham_cosmology_catalogs.py MD40 501 
python3 005_0_sham_cosmology_catalogs.py MD40 502 
python3 005_0_sham_cosmology_catalogs.py MD40 503 
 
