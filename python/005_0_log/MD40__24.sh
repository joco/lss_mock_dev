#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 336 
python3 005_0_sham_cosmology_catalogs.py MD40 337 
python3 005_0_sham_cosmology_catalogs.py MD40 338 
python3 005_0_sham_cosmology_catalogs.py MD40 339 
python3 005_0_sham_cosmology_catalogs.py MD40 340 
python3 005_0_sham_cosmology_catalogs.py MD40 341 
python3 005_0_sham_cosmology_catalogs.py MD40 342 
python3 005_0_sham_cosmology_catalogs.py MD40 343 
python3 005_0_sham_cosmology_catalogs.py MD40 344 
python3 005_0_sham_cosmology_catalogs.py MD40 345 
python3 005_0_sham_cosmology_catalogs.py MD40 346 
python3 005_0_sham_cosmology_catalogs.py MD40 347 
python3 005_0_sham_cosmology_catalogs.py MD40 348 
python3 005_0_sham_cosmology_catalogs.py MD40 349 
 
