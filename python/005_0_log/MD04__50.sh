#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD04 700 
python3 005_0_sham_cosmology_catalogs.py MD04 701 
python3 005_0_sham_cosmology_catalogs.py MD04 702 
python3 005_0_sham_cosmology_catalogs.py MD04 703 
python3 005_0_sham_cosmology_catalogs.py MD04 704 
python3 005_0_sham_cosmology_catalogs.py MD04 705 
python3 005_0_sham_cosmology_catalogs.py MD04 706 
python3 005_0_sham_cosmology_catalogs.py MD04 707 
python3 005_0_sham_cosmology_catalogs.py MD04 708 
python3 005_0_sham_cosmology_catalogs.py MD04 709 
python3 005_0_sham_cosmology_catalogs.py MD04 710 
python3 005_0_sham_cosmology_catalogs.py MD04 711 
python3 005_0_sham_cosmology_catalogs.py MD04 712 
python3 005_0_sham_cosmology_catalogs.py MD04 713 
 
