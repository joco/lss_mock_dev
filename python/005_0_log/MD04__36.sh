#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD04 504 
python3 005_0_sham_cosmology_catalogs.py MD04 505 
python3 005_0_sham_cosmology_catalogs.py MD04 506 
python3 005_0_sham_cosmology_catalogs.py MD04 507 
python3 005_0_sham_cosmology_catalogs.py MD04 508 
python3 005_0_sham_cosmology_catalogs.py MD04 509 
python3 005_0_sham_cosmology_catalogs.py MD04 510 
python3 005_0_sham_cosmology_catalogs.py MD04 511 
python3 005_0_sham_cosmology_catalogs.py MD04 512 
python3 005_0_sham_cosmology_catalogs.py MD04 513 
python3 005_0_sham_cosmology_catalogs.py MD04 514 
python3 005_0_sham_cosmology_catalogs.py MD04 515 
python3 005_0_sham_cosmology_catalogs.py MD04 516 
python3 005_0_sham_cosmology_catalogs.py MD04 517 
 
