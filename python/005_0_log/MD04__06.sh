#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD04 84 
python3 005_0_sham_cosmology_catalogs.py MD04 85 
python3 005_0_sham_cosmology_catalogs.py MD04 86 
python3 005_0_sham_cosmology_catalogs.py MD04 87 
python3 005_0_sham_cosmology_catalogs.py MD04 88 
python3 005_0_sham_cosmology_catalogs.py MD04 89 
python3 005_0_sham_cosmology_catalogs.py MD04 90 
python3 005_0_sham_cosmology_catalogs.py MD04 91 
python3 005_0_sham_cosmology_catalogs.py MD04 92 
python3 005_0_sham_cosmology_catalogs.py MD04 93 
python3 005_0_sham_cosmology_catalogs.py MD04 94 
python3 005_0_sham_cosmology_catalogs.py MD04 95 
python3 005_0_sham_cosmology_catalogs.py MD04 96 
python3 005_0_sham_cosmology_catalogs.py MD04 97 
 
