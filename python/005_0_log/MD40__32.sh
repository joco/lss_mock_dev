#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 448 
python3 005_0_sham_cosmology_catalogs.py MD40 449 
python3 005_0_sham_cosmology_catalogs.py MD40 450 
python3 005_0_sham_cosmology_catalogs.py MD40 451 
python3 005_0_sham_cosmology_catalogs.py MD40 452 
python3 005_0_sham_cosmology_catalogs.py MD40 453 
python3 005_0_sham_cosmology_catalogs.py MD40 454 
python3 005_0_sham_cosmology_catalogs.py MD40 455 
python3 005_0_sham_cosmology_catalogs.py MD40 456 
python3 005_0_sham_cosmology_catalogs.py MD40 457 
python3 005_0_sham_cosmology_catalogs.py MD40 458 
python3 005_0_sham_cosmology_catalogs.py MD40 459 
python3 005_0_sham_cosmology_catalogs.py MD40 460 
python3 005_0_sham_cosmology_catalogs.py MD40 461 
 
