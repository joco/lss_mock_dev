#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD04 70 
python3 005_0_sham_cosmology_catalogs.py MD04 71 
python3 005_0_sham_cosmology_catalogs.py MD04 72 
python3 005_0_sham_cosmology_catalogs.py MD04 73 
python3 005_0_sham_cosmology_catalogs.py MD04 74 
python3 005_0_sham_cosmology_catalogs.py MD04 75 
python3 005_0_sham_cosmology_catalogs.py MD04 76 
python3 005_0_sham_cosmology_catalogs.py MD04 77 
python3 005_0_sham_cosmology_catalogs.py MD04 78 
python3 005_0_sham_cosmology_catalogs.py MD04 79 
python3 005_0_sham_cosmology_catalogs.py MD04 80 
python3 005_0_sham_cosmology_catalogs.py MD04 81 
python3 005_0_sham_cosmology_catalogs.py MD04 82 
python3 005_0_sham_cosmology_catalogs.py MD04 83 
 
