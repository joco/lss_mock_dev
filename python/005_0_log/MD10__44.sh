#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD10 616 
python3 005_0_sham_cosmology_catalogs.py MD10 617 
python3 005_0_sham_cosmology_catalogs.py MD10 618 
python3 005_0_sham_cosmology_catalogs.py MD10 619 
python3 005_0_sham_cosmology_catalogs.py MD10 620 
python3 005_0_sham_cosmology_catalogs.py MD10 621 
python3 005_0_sham_cosmology_catalogs.py MD10 622 
python3 005_0_sham_cosmology_catalogs.py MD10 623 
python3 005_0_sham_cosmology_catalogs.py MD10 624 
python3 005_0_sham_cosmology_catalogs.py MD10 625 
python3 005_0_sham_cosmology_catalogs.py MD10 626 
python3 005_0_sham_cosmology_catalogs.py MD10 627 
python3 005_0_sham_cosmology_catalogs.py MD10 628 
python3 005_0_sham_cosmology_catalogs.py MD10 629 
 
