#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA1_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 630 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 631 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 632 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 633 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 634 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 635 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 636 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 637 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 638 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 639 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 640 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 641 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 642 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 643 
 
