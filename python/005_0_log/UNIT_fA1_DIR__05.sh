#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA1_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 70 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 71 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 72 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 73 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 74 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 75 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 76 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 77 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 78 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 79 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 80 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 81 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 82 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA1_DIR 83 
 
