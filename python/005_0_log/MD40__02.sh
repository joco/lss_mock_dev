#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 28 
python3 005_0_sham_cosmology_catalogs.py MD40 29 
python3 005_0_sham_cosmology_catalogs.py MD40 30 
python3 005_0_sham_cosmology_catalogs.py MD40 31 
python3 005_0_sham_cosmology_catalogs.py MD40 32 
python3 005_0_sham_cosmology_catalogs.py MD40 33 
python3 005_0_sham_cosmology_catalogs.py MD40 34 
python3 005_0_sham_cosmology_catalogs.py MD40 35 
python3 005_0_sham_cosmology_catalogs.py MD40 36 
python3 005_0_sham_cosmology_catalogs.py MD40 37 
python3 005_0_sham_cosmology_catalogs.py MD40 38 
python3 005_0_sham_cosmology_catalogs.py MD40 39 
python3 005_0_sham_cosmology_catalogs.py MD40 40 
python3 005_0_sham_cosmology_catalogs.py MD40 41 
 
