#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 420 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 421 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 422 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 423 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 424 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 425 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 426 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 427 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 428 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 429 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 430 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 431 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 432 
python3 005_0_sham_cosmology_catalogs.py UNIT_fA2_DIR 433 
 
