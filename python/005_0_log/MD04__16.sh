#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD04 224 
python3 005_0_sham_cosmology_catalogs.py MD04 225 
python3 005_0_sham_cosmology_catalogs.py MD04 226 
python3 005_0_sham_cosmology_catalogs.py MD04 227 
python3 005_0_sham_cosmology_catalogs.py MD04 228 
python3 005_0_sham_cosmology_catalogs.py MD04 229 
python3 005_0_sham_cosmology_catalogs.py MD04 230 
python3 005_0_sham_cosmology_catalogs.py MD04 231 
python3 005_0_sham_cosmology_catalogs.py MD04 232 
python3 005_0_sham_cosmology_catalogs.py MD04 233 
python3 005_0_sham_cosmology_catalogs.py MD04 234 
python3 005_0_sham_cosmology_catalogs.py MD04 235 
python3 005_0_sham_cosmology_catalogs.py MD04 236 
python3 005_0_sham_cosmology_catalogs.py MD04 237 
 
