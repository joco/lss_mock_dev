#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 84 
python3 005_0_sham_cosmology_catalogs.py MD40 85 
python3 005_0_sham_cosmology_catalogs.py MD40 86 
python3 005_0_sham_cosmology_catalogs.py MD40 87 
python3 005_0_sham_cosmology_catalogs.py MD40 88 
python3 005_0_sham_cosmology_catalogs.py MD40 89 
python3 005_0_sham_cosmology_catalogs.py MD40 90 
python3 005_0_sham_cosmology_catalogs.py MD40 91 
python3 005_0_sham_cosmology_catalogs.py MD40 92 
python3 005_0_sham_cosmology_catalogs.py MD40 93 
python3 005_0_sham_cosmology_catalogs.py MD40 94 
python3 005_0_sham_cosmology_catalogs.py MD40 95 
python3 005_0_sham_cosmology_catalogs.py MD40 96 
python3 005_0_sham_cosmology_catalogs.py MD40 97 
 
