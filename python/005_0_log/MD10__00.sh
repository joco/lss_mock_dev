#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD10 0 
python3 005_0_sham_cosmology_catalogs.py MD10 1 
python3 005_0_sham_cosmology_catalogs.py MD10 2 
python3 005_0_sham_cosmology_catalogs.py MD10 3 
python3 005_0_sham_cosmology_catalogs.py MD10 4 
python3 005_0_sham_cosmology_catalogs.py MD10 5 
python3 005_0_sham_cosmology_catalogs.py MD10 6 
python3 005_0_sham_cosmology_catalogs.py MD10 7 
python3 005_0_sham_cosmology_catalogs.py MD10 8 
python3 005_0_sham_cosmology_catalogs.py MD10 9 
python3 005_0_sham_cosmology_catalogs.py MD10 10 
python3 005_0_sham_cosmology_catalogs.py MD10 11 
python3 005_0_sham_cosmology_catalogs.py MD10 12 
python3 005_0_sham_cosmology_catalogs.py MD10 13 
 
