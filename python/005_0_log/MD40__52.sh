#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 728 
python3 005_0_sham_cosmology_catalogs.py MD40 729 
python3 005_0_sham_cosmology_catalogs.py MD40 730 
python3 005_0_sham_cosmology_catalogs.py MD40 731 
python3 005_0_sham_cosmology_catalogs.py MD40 732 
python3 005_0_sham_cosmology_catalogs.py MD40 733 
python3 005_0_sham_cosmology_catalogs.py MD40 734 
python3 005_0_sham_cosmology_catalogs.py MD40 735 
python3 005_0_sham_cosmology_catalogs.py MD40 736 
python3 005_0_sham_cosmology_catalogs.py MD40 737 
python3 005_0_sham_cosmology_catalogs.py MD40 738 
python3 005_0_sham_cosmology_catalogs.py MD40 739 
python3 005_0_sham_cosmology_catalogs.py MD40 740 
python3 005_0_sham_cosmology_catalogs.py MD40 741 
 
