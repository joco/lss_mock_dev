#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD04 420 
python3 005_0_sham_cosmology_catalogs.py MD04 421 
python3 005_0_sham_cosmology_catalogs.py MD04 422 
python3 005_0_sham_cosmology_catalogs.py MD04 423 
python3 005_0_sham_cosmology_catalogs.py MD04 424 
python3 005_0_sham_cosmology_catalogs.py MD04 425 
python3 005_0_sham_cosmology_catalogs.py MD04 426 
python3 005_0_sham_cosmology_catalogs.py MD04 427 
python3 005_0_sham_cosmology_catalogs.py MD04 428 
python3 005_0_sham_cosmology_catalogs.py MD04 429 
python3 005_0_sham_cosmology_catalogs.py MD04 430 
python3 005_0_sham_cosmology_catalogs.py MD04 431 
python3 005_0_sham_cosmology_catalogs.py MD04 432 
python3 005_0_sham_cosmology_catalogs.py MD04 433 
 
