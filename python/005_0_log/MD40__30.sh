#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 420 
python3 005_0_sham_cosmology_catalogs.py MD40 421 
python3 005_0_sham_cosmology_catalogs.py MD40 422 
python3 005_0_sham_cosmology_catalogs.py MD40 423 
python3 005_0_sham_cosmology_catalogs.py MD40 424 
python3 005_0_sham_cosmology_catalogs.py MD40 425 
python3 005_0_sham_cosmology_catalogs.py MD40 426 
python3 005_0_sham_cosmology_catalogs.py MD40 427 
python3 005_0_sham_cosmology_catalogs.py MD40 428 
python3 005_0_sham_cosmology_catalogs.py MD40 429 
python3 005_0_sham_cosmology_catalogs.py MD40 430 
python3 005_0_sham_cosmology_catalogs.py MD40 431 
python3 005_0_sham_cosmology_catalogs.py MD40 432 
python3 005_0_sham_cosmology_catalogs.py MD40 433 
 
