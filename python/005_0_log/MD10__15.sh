#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD10 210 
python3 005_0_sham_cosmology_catalogs.py MD10 211 
python3 005_0_sham_cosmology_catalogs.py MD10 212 
python3 005_0_sham_cosmology_catalogs.py MD10 213 
python3 005_0_sham_cosmology_catalogs.py MD10 214 
python3 005_0_sham_cosmology_catalogs.py MD10 215 
python3 005_0_sham_cosmology_catalogs.py MD10 216 
python3 005_0_sham_cosmology_catalogs.py MD10 217 
python3 005_0_sham_cosmology_catalogs.py MD10 218 
python3 005_0_sham_cosmology_catalogs.py MD10 219 
python3 005_0_sham_cosmology_catalogs.py MD10 220 
python3 005_0_sham_cosmology_catalogs.py MD10 221 
python3 005_0_sham_cosmology_catalogs.py MD10 222 
python3 005_0_sham_cosmology_catalogs.py MD10 223 
 
