#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD10 126 
python3 005_0_sham_cosmology_catalogs.py MD10 127 
python3 005_0_sham_cosmology_catalogs.py MD10 128 
python3 005_0_sham_cosmology_catalogs.py MD10 129 
python3 005_0_sham_cosmology_catalogs.py MD10 130 
python3 005_0_sham_cosmology_catalogs.py MD10 131 
python3 005_0_sham_cosmology_catalogs.py MD10 132 
python3 005_0_sham_cosmology_catalogs.py MD10 133 
python3 005_0_sham_cosmology_catalogs.py MD10 134 
python3 005_0_sham_cosmology_catalogs.py MD10 135 
python3 005_0_sham_cosmology_catalogs.py MD10 136 
python3 005_0_sham_cosmology_catalogs.py MD10 137 
python3 005_0_sham_cosmology_catalogs.py MD10 138 
python3 005_0_sham_cosmology_catalogs.py MD10 139 
 
