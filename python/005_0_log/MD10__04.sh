#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD10 56 
python3 005_0_sham_cosmology_catalogs.py MD10 57 
python3 005_0_sham_cosmology_catalogs.py MD10 58 
python3 005_0_sham_cosmology_catalogs.py MD10 59 
python3 005_0_sham_cosmology_catalogs.py MD10 60 
python3 005_0_sham_cosmology_catalogs.py MD10 61 
python3 005_0_sham_cosmology_catalogs.py MD10 62 
python3 005_0_sham_cosmology_catalogs.py MD10 63 
python3 005_0_sham_cosmology_catalogs.py MD10 64 
python3 005_0_sham_cosmology_catalogs.py MD10 65 
python3 005_0_sham_cosmology_catalogs.py MD10 66 
python3 005_0_sham_cosmology_catalogs.py MD10 67 
python3 005_0_sham_cosmology_catalogs.py MD10 68 
python3 005_0_sham_cosmology_catalogs.py MD10 69 
 
