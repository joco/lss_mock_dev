#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_0_sham_cosmology_catalogs.py MD40 518 
python3 005_0_sham_cosmology_catalogs.py MD40 519 
python3 005_0_sham_cosmology_catalogs.py MD40 520 
python3 005_0_sham_cosmology_catalogs.py MD40 521 
python3 005_0_sham_cosmology_catalogs.py MD40 522 
python3 005_0_sham_cosmology_catalogs.py MD40 523 
python3 005_0_sham_cosmology_catalogs.py MD40 524 
python3 005_0_sham_cosmology_catalogs.py MD40 525 
python3 005_0_sham_cosmology_catalogs.py MD40 526 
python3 005_0_sham_cosmology_catalogs.py MD40 527 
python3 005_0_sham_cosmology_catalogs.py MD40 528 
python3 005_0_sham_cosmology_catalogs.py MD40 529 
python3 005_0_sham_cosmology_catalogs.py MD40 530 
python3 005_0_sham_cosmology_catalogs.py MD40 531 
 
