#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=30MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/30_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/30_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 420 
python3 003_6_agn_magnitudes.py MD04 all 421 
python3 003_6_agn_magnitudes.py MD04 all 422 
python3 003_6_agn_magnitudes.py MD04 all 423 
python3 003_6_agn_magnitudes.py MD04 all 424 
python3 003_6_agn_magnitudes.py MD04 all 425 
python3 003_6_agn_magnitudes.py MD04 all 426 
python3 003_6_agn_magnitudes.py MD04 all 427 
python3 003_6_agn_magnitudes.py MD04 all 428 
python3 003_6_agn_magnitudes.py MD04 all 429 
python3 003_6_agn_magnitudes.py MD04 all 430 
python3 003_6_agn_magnitudes.py MD04 all 431 
python3 003_6_agn_magnitudes.py MD04 all 432 
python3 003_6_agn_magnitudes.py MD04 all 433 
 
