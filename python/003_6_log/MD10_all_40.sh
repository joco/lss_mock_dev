#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=40MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/40_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/40_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 560 
python3 003_6_agn_magnitudes.py MD10 all 561 
python3 003_6_agn_magnitudes.py MD10 all 562 
python3 003_6_agn_magnitudes.py MD10 all 563 
python3 003_6_agn_magnitudes.py MD10 all 564 
python3 003_6_agn_magnitudes.py MD10 all 565 
python3 003_6_agn_magnitudes.py MD10 all 566 
python3 003_6_agn_magnitudes.py MD10 all 567 
python3 003_6_agn_magnitudes.py MD10 all 568 
python3 003_6_agn_magnitudes.py MD10 all 569 
python3 003_6_agn_magnitudes.py MD10 all 570 
python3 003_6_agn_magnitudes.py MD10 all 571 
python3 003_6_agn_magnitudes.py MD10 all 572 
python3 003_6_agn_magnitudes.py MD10 all 573 
 
