#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=27MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/27_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/27_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 378 
python3 003_6_agn_magnitudes.py MD10 all 379 
python3 003_6_agn_magnitudes.py MD10 all 380 
python3 003_6_agn_magnitudes.py MD10 all 381 
python3 003_6_agn_magnitudes.py MD10 all 382 
python3 003_6_agn_magnitudes.py MD10 all 383 
python3 003_6_agn_magnitudes.py MD10 all 384 
python3 003_6_agn_magnitudes.py MD10 all 385 
python3 003_6_agn_magnitudes.py MD10 all 386 
python3 003_6_agn_magnitudes.py MD10 all 387 
python3 003_6_agn_magnitudes.py MD10 all 388 
python3 003_6_agn_magnitudes.py MD10 all 389 
python3 003_6_agn_magnitudes.py MD10 all 390 
python3 003_6_agn_magnitudes.py MD10 all 391 
 
