#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=45MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/45_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/45_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 630 
python3 003_6_agn_magnitudes.py MD10 all 631 
python3 003_6_agn_magnitudes.py MD10 all 632 
python3 003_6_agn_magnitudes.py MD10 all 633 
python3 003_6_agn_magnitudes.py MD10 all 634 
python3 003_6_agn_magnitudes.py MD10 all 635 
python3 003_6_agn_magnitudes.py MD10 all 636 
python3 003_6_agn_magnitudes.py MD10 all 637 
python3 003_6_agn_magnitudes.py MD10 all 638 
python3 003_6_agn_magnitudes.py MD10 all 639 
python3 003_6_agn_magnitudes.py MD10 all 640 
python3 003_6_agn_magnitudes.py MD10 all 641 
python3 003_6_agn_magnitudes.py MD10 all 642 
python3 003_6_agn_magnitudes.py MD10 all 643 
 
