#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=46MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/46_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/46_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 644 
python3 003_6_agn_magnitudes.py MD40 all 645 
python3 003_6_agn_magnitudes.py MD40 all 646 
python3 003_6_agn_magnitudes.py MD40 all 647 
python3 003_6_agn_magnitudes.py MD40 all 648 
python3 003_6_agn_magnitudes.py MD40 all 649 
python3 003_6_agn_magnitudes.py MD40 all 650 
python3 003_6_agn_magnitudes.py MD40 all 651 
python3 003_6_agn_magnitudes.py MD40 all 652 
python3 003_6_agn_magnitudes.py MD40 all 653 
python3 003_6_agn_magnitudes.py MD40 all 654 
python3 003_6_agn_magnitudes.py MD40 all 655 
python3 003_6_agn_magnitudes.py MD40 all 656 
python3 003_6_agn_magnitudes.py MD40 all 657 
 
