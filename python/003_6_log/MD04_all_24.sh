#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=24MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/24_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/24_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 336 
python3 003_6_agn_magnitudes.py MD04 all 337 
python3 003_6_agn_magnitudes.py MD04 all 338 
python3 003_6_agn_magnitudes.py MD04 all 339 
python3 003_6_agn_magnitudes.py MD04 all 340 
python3 003_6_agn_magnitudes.py MD04 all 341 
python3 003_6_agn_magnitudes.py MD04 all 342 
python3 003_6_agn_magnitudes.py MD04 all 343 
python3 003_6_agn_magnitudes.py MD04 all 344 
python3 003_6_agn_magnitudes.py MD04 all 345 
python3 003_6_agn_magnitudes.py MD04 all 346 
python3 003_6_agn_magnitudes.py MD04 all 347 
python3 003_6_agn_magnitudes.py MD04 all 348 
python3 003_6_agn_magnitudes.py MD04 all 349 
 
