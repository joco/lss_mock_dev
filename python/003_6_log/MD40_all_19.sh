#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=19MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/19_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/19_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 266 
python3 003_6_agn_magnitudes.py MD40 all 267 
python3 003_6_agn_magnitudes.py MD40 all 268 
python3 003_6_agn_magnitudes.py MD40 all 269 
python3 003_6_agn_magnitudes.py MD40 all 270 
python3 003_6_agn_magnitudes.py MD40 all 271 
python3 003_6_agn_magnitudes.py MD40 all 272 
python3 003_6_agn_magnitudes.py MD40 all 273 
python3 003_6_agn_magnitudes.py MD40 all 274 
python3 003_6_agn_magnitudes.py MD40 all 275 
python3 003_6_agn_magnitudes.py MD40 all 276 
python3 003_6_agn_magnitudes.py MD40 all 277 
python3 003_6_agn_magnitudes.py MD40 all 278 
python3 003_6_agn_magnitudes.py MD40 all 279 
 
