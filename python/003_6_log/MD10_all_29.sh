#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=29MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/29_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/29_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 406 
python3 003_6_agn_magnitudes.py MD10 all 407 
python3 003_6_agn_magnitudes.py MD10 all 408 
python3 003_6_agn_magnitudes.py MD10 all 409 
python3 003_6_agn_magnitudes.py MD10 all 410 
python3 003_6_agn_magnitudes.py MD10 all 411 
python3 003_6_agn_magnitudes.py MD10 all 412 
python3 003_6_agn_magnitudes.py MD10 all 413 
python3 003_6_agn_magnitudes.py MD10 all 414 
python3 003_6_agn_magnitudes.py MD10 all 415 
python3 003_6_agn_magnitudes.py MD10 all 416 
python3 003_6_agn_magnitudes.py MD10 all 417 
python3 003_6_agn_magnitudes.py MD10 all 418 
python3 003_6_agn_magnitudes.py MD10 all 419 
 
