#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=36MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/36_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/36_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 504 
python3 003_6_agn_magnitudes.py MD10 all 505 
python3 003_6_agn_magnitudes.py MD10 all 506 
python3 003_6_agn_magnitudes.py MD10 all 507 
python3 003_6_agn_magnitudes.py MD10 all 508 
python3 003_6_agn_magnitudes.py MD10 all 509 
python3 003_6_agn_magnitudes.py MD10 all 510 
python3 003_6_agn_magnitudes.py MD10 all 511 
python3 003_6_agn_magnitudes.py MD10 all 512 
python3 003_6_agn_magnitudes.py MD10 all 513 
python3 003_6_agn_magnitudes.py MD10 all 514 
python3 003_6_agn_magnitudes.py MD10 all 515 
python3 003_6_agn_magnitudes.py MD10 all 516 
python3 003_6_agn_magnitudes.py MD10 all 517 
 
