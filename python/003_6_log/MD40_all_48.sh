#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=48MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/48_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/48_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 672 
python3 003_6_agn_magnitudes.py MD40 all 673 
python3 003_6_agn_magnitudes.py MD40 all 674 
python3 003_6_agn_magnitudes.py MD40 all 675 
python3 003_6_agn_magnitudes.py MD40 all 676 
python3 003_6_agn_magnitudes.py MD40 all 677 
python3 003_6_agn_magnitudes.py MD40 all 678 
python3 003_6_agn_magnitudes.py MD40 all 679 
python3 003_6_agn_magnitudes.py MD40 all 680 
python3 003_6_agn_magnitudes.py MD40 all 681 
python3 003_6_agn_magnitudes.py MD40 all 682 
python3 003_6_agn_magnitudes.py MD40 all 683 
python3 003_6_agn_magnitudes.py MD40 all 684 
python3 003_6_agn_magnitudes.py MD40 all 685 
 
