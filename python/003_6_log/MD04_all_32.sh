#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=32MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/32_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/32_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 448 
python3 003_6_agn_magnitudes.py MD04 all 449 
python3 003_6_agn_magnitudes.py MD04 all 450 
python3 003_6_agn_magnitudes.py MD04 all 451 
python3 003_6_agn_magnitudes.py MD04 all 452 
python3 003_6_agn_magnitudes.py MD04 all 453 
python3 003_6_agn_magnitudes.py MD04 all 454 
python3 003_6_agn_magnitudes.py MD04 all 455 
python3 003_6_agn_magnitudes.py MD04 all 456 
python3 003_6_agn_magnitudes.py MD04 all 457 
python3 003_6_agn_magnitudes.py MD04 all 458 
python3 003_6_agn_magnitudes.py MD04 all 459 
python3 003_6_agn_magnitudes.py MD04 all 460 
python3 003_6_agn_magnitudes.py MD04 all 461 
 
