#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=33MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/33_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/33_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 462 
python3 003_6_agn_magnitudes.py MD10 all 463 
python3 003_6_agn_magnitudes.py MD10 all 464 
python3 003_6_agn_magnitudes.py MD10 all 465 
python3 003_6_agn_magnitudes.py MD10 all 466 
python3 003_6_agn_magnitudes.py MD10 all 467 
python3 003_6_agn_magnitudes.py MD10 all 468 
python3 003_6_agn_magnitudes.py MD10 all 469 
python3 003_6_agn_magnitudes.py MD10 all 470 
python3 003_6_agn_magnitudes.py MD10 all 471 
python3 003_6_agn_magnitudes.py MD10 all 472 
python3 003_6_agn_magnitudes.py MD10 all 473 
python3 003_6_agn_magnitudes.py MD10 all 474 
python3 003_6_agn_magnitudes.py MD10 all 475 
 
