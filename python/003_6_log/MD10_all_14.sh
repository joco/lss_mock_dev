#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=14MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/14_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/14_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 196 
python3 003_6_agn_magnitudes.py MD10 all 197 
python3 003_6_agn_magnitudes.py MD10 all 198 
python3 003_6_agn_magnitudes.py MD10 all 199 
python3 003_6_agn_magnitudes.py MD10 all 200 
python3 003_6_agn_magnitudes.py MD10 all 201 
python3 003_6_agn_magnitudes.py MD10 all 202 
python3 003_6_agn_magnitudes.py MD10 all 203 
python3 003_6_agn_magnitudes.py MD10 all 204 
python3 003_6_agn_magnitudes.py MD10 all 205 
python3 003_6_agn_magnitudes.py MD10 all 206 
python3 003_6_agn_magnitudes.py MD10 all 207 
python3 003_6_agn_magnitudes.py MD10 all 208 
python3 003_6_agn_magnitudes.py MD10 all 209 
 
