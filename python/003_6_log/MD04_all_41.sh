#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=41MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/41_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/41_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 574 
python3 003_6_agn_magnitudes.py MD04 all 575 
python3 003_6_agn_magnitudes.py MD04 all 576 
python3 003_6_agn_magnitudes.py MD04 all 577 
python3 003_6_agn_magnitudes.py MD04 all 578 
python3 003_6_agn_magnitudes.py MD04 all 579 
python3 003_6_agn_magnitudes.py MD04 all 580 
python3 003_6_agn_magnitudes.py MD04 all 581 
python3 003_6_agn_magnitudes.py MD04 all 582 
python3 003_6_agn_magnitudes.py MD04 all 583 
python3 003_6_agn_magnitudes.py MD04 all 584 
python3 003_6_agn_magnitudes.py MD04 all 585 
python3 003_6_agn_magnitudes.py MD04 all 586 
python3 003_6_agn_magnitudes.py MD04 all 587 
 
