#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=25MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/25_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/25_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 350 
python3 003_6_agn_magnitudes.py MD40 all 351 
python3 003_6_agn_magnitudes.py MD40 all 352 
python3 003_6_agn_magnitudes.py MD40 all 353 
python3 003_6_agn_magnitudes.py MD40 all 354 
python3 003_6_agn_magnitudes.py MD40 all 355 
python3 003_6_agn_magnitudes.py MD40 all 356 
python3 003_6_agn_magnitudes.py MD40 all 357 
python3 003_6_agn_magnitudes.py MD40 all 358 
python3 003_6_agn_magnitudes.py MD40 all 359 
python3 003_6_agn_magnitudes.py MD40 all 360 
python3 003_6_agn_magnitudes.py MD40 all 361 
python3 003_6_agn_magnitudes.py MD40 all 362 
python3 003_6_agn_magnitudes.py MD40 all 363 
 
