#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=39MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/39_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/39_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 546 
python3 003_6_agn_magnitudes.py MD10 all 547 
python3 003_6_agn_magnitudes.py MD10 all 548 
python3 003_6_agn_magnitudes.py MD10 all 549 
python3 003_6_agn_magnitudes.py MD10 all 550 
python3 003_6_agn_magnitudes.py MD10 all 551 
python3 003_6_agn_magnitudes.py MD10 all 552 
python3 003_6_agn_magnitudes.py MD10 all 553 
python3 003_6_agn_magnitudes.py MD10 all 554 
python3 003_6_agn_magnitudes.py MD10 all 555 
python3 003_6_agn_magnitudes.py MD10 all 556 
python3 003_6_agn_magnitudes.py MD10 all 557 
python3 003_6_agn_magnitudes.py MD10 all 558 
python3 003_6_agn_magnitudes.py MD10 all 559 
 
