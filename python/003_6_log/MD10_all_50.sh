#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=50MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/50_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/50_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 700 
python3 003_6_agn_magnitudes.py MD10 all 701 
python3 003_6_agn_magnitudes.py MD10 all 702 
python3 003_6_agn_magnitudes.py MD10 all 703 
python3 003_6_agn_magnitudes.py MD10 all 704 
python3 003_6_agn_magnitudes.py MD10 all 705 
python3 003_6_agn_magnitudes.py MD10 all 706 
python3 003_6_agn_magnitudes.py MD10 all 707 
python3 003_6_agn_magnitudes.py MD10 all 708 
python3 003_6_agn_magnitudes.py MD10 all 709 
python3 003_6_agn_magnitudes.py MD10 all 710 
python3 003_6_agn_magnitudes.py MD10 all 711 
python3 003_6_agn_magnitudes.py MD10 all 712 
python3 003_6_agn_magnitudes.py MD10 all 713 
 
