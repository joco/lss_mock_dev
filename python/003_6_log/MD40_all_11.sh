#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=11MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/11_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/11_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 154 
python3 003_6_agn_magnitudes.py MD40 all 155 
python3 003_6_agn_magnitudes.py MD40 all 156 
python3 003_6_agn_magnitudes.py MD40 all 157 
python3 003_6_agn_magnitudes.py MD40 all 158 
python3 003_6_agn_magnitudes.py MD40 all 159 
python3 003_6_agn_magnitudes.py MD40 all 160 
python3 003_6_agn_magnitudes.py MD40 all 161 
python3 003_6_agn_magnitudes.py MD40 all 162 
python3 003_6_agn_magnitudes.py MD40 all 163 
python3 003_6_agn_magnitudes.py MD40 all 164 
python3 003_6_agn_magnitudes.py MD40 all 165 
python3 003_6_agn_magnitudes.py MD40 all 166 
python3 003_6_agn_magnitudes.py MD40 all 167 
 
