#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=17MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/17_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/17_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 238 
python3 003_6_agn_magnitudes.py MD40 all 239 
python3 003_6_agn_magnitudes.py MD40 all 240 
python3 003_6_agn_magnitudes.py MD40 all 241 
python3 003_6_agn_magnitudes.py MD40 all 242 
python3 003_6_agn_magnitudes.py MD40 all 243 
python3 003_6_agn_magnitudes.py MD40 all 244 
python3 003_6_agn_magnitudes.py MD40 all 245 
python3 003_6_agn_magnitudes.py MD40 all 246 
python3 003_6_agn_magnitudes.py MD40 all 247 
python3 003_6_agn_magnitudes.py MD40 all 248 
python3 003_6_agn_magnitudes.py MD40 all 249 
python3 003_6_agn_magnitudes.py MD40 all 250 
python3 003_6_agn_magnitudes.py MD40 all 251 
 
