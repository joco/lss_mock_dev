#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=38MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/38_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/38_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 532 
python3 003_6_agn_magnitudes.py MD40 all 533 
python3 003_6_agn_magnitudes.py MD40 all 534 
python3 003_6_agn_magnitudes.py MD40 all 535 
python3 003_6_agn_magnitudes.py MD40 all 536 
python3 003_6_agn_magnitudes.py MD40 all 537 
python3 003_6_agn_magnitudes.py MD40 all 538 
python3 003_6_agn_magnitudes.py MD40 all 539 
python3 003_6_agn_magnitudes.py MD40 all 540 
python3 003_6_agn_magnitudes.py MD40 all 541 
python3 003_6_agn_magnitudes.py MD40 all 542 
python3 003_6_agn_magnitudes.py MD40 all 543 
python3 003_6_agn_magnitudes.py MD40 all 544 
python3 003_6_agn_magnitudes.py MD40 all 545 
 
