#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=52MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/52_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/52_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 728 
python3 003_6_agn_magnitudes.py MD04 all 729 
python3 003_6_agn_magnitudes.py MD04 all 730 
python3 003_6_agn_magnitudes.py MD04 all 731 
python3 003_6_agn_magnitudes.py MD04 all 732 
python3 003_6_agn_magnitudes.py MD04 all 733 
python3 003_6_agn_magnitudes.py MD04 all 734 
python3 003_6_agn_magnitudes.py MD04 all 735 
python3 003_6_agn_magnitudes.py MD04 all 736 
python3 003_6_agn_magnitudes.py MD04 all 737 
python3 003_6_agn_magnitudes.py MD04 all 738 
python3 003_6_agn_magnitudes.py MD04 all 739 
python3 003_6_agn_magnitudes.py MD04 all 740 
python3 003_6_agn_magnitudes.py MD04 all 741 
 
