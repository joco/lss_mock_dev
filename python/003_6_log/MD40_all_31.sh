#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=31MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/31_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/31_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 434 
python3 003_6_agn_magnitudes.py MD40 all 435 
python3 003_6_agn_magnitudes.py MD40 all 436 
python3 003_6_agn_magnitudes.py MD40 all 437 
python3 003_6_agn_magnitudes.py MD40 all 438 
python3 003_6_agn_magnitudes.py MD40 all 439 
python3 003_6_agn_magnitudes.py MD40 all 440 
python3 003_6_agn_magnitudes.py MD40 all 441 
python3 003_6_agn_magnitudes.py MD40 all 442 
python3 003_6_agn_magnitudes.py MD40 all 443 
python3 003_6_agn_magnitudes.py MD40 all 444 
python3 003_6_agn_magnitudes.py MD40 all 445 
python3 003_6_agn_magnitudes.py MD40 all 446 
python3 003_6_agn_magnitudes.py MD40 all 447 
 
