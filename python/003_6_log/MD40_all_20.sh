#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=20MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/20_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/20_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 280 
python3 003_6_agn_magnitudes.py MD40 all 281 
python3 003_6_agn_magnitudes.py MD40 all 282 
python3 003_6_agn_magnitudes.py MD40 all 283 
python3 003_6_agn_magnitudes.py MD40 all 284 
python3 003_6_agn_magnitudes.py MD40 all 285 
python3 003_6_agn_magnitudes.py MD40 all 286 
python3 003_6_agn_magnitudes.py MD40 all 287 
python3 003_6_agn_magnitudes.py MD40 all 288 
python3 003_6_agn_magnitudes.py MD40 all 289 
python3 003_6_agn_magnitudes.py MD40 all 290 
python3 003_6_agn_magnitudes.py MD40 all 291 
python3 003_6_agn_magnitudes.py MD40 all 292 
python3 003_6_agn_magnitudes.py MD40 all 293 
 
