#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=21MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/21_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/21_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 294 
python3 003_6_agn_magnitudes.py MD40 all 295 
python3 003_6_agn_magnitudes.py MD40 all 296 
python3 003_6_agn_magnitudes.py MD40 all 297 
python3 003_6_agn_magnitudes.py MD40 all 298 
python3 003_6_agn_magnitudes.py MD40 all 299 
python3 003_6_agn_magnitudes.py MD40 all 300 
python3 003_6_agn_magnitudes.py MD40 all 301 
python3 003_6_agn_magnitudes.py MD40 all 302 
python3 003_6_agn_magnitudes.py MD40 all 303 
python3 003_6_agn_magnitudes.py MD40 all 304 
python3 003_6_agn_magnitudes.py MD40 all 305 
python3 003_6_agn_magnitudes.py MD40 all 306 
python3 003_6_agn_magnitudes.py MD40 all 307 
 
