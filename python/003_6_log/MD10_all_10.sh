#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=10MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/10_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/10_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 140 
python3 003_6_agn_magnitudes.py MD10 all 141 
python3 003_6_agn_magnitudes.py MD10 all 142 
python3 003_6_agn_magnitudes.py MD10 all 143 
python3 003_6_agn_magnitudes.py MD10 all 144 
python3 003_6_agn_magnitudes.py MD10 all 145 
python3 003_6_agn_magnitudes.py MD10 all 146 
python3 003_6_agn_magnitudes.py MD10 all 147 
python3 003_6_agn_magnitudes.py MD10 all 148 
python3 003_6_agn_magnitudes.py MD10 all 149 
python3 003_6_agn_magnitudes.py MD10 all 150 
python3 003_6_agn_magnitudes.py MD10 all 151 
python3 003_6_agn_magnitudes.py MD10 all 152 
python3 003_6_agn_magnitudes.py MD10 all 153 
 
