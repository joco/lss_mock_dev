#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=37MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/37_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/37_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 518 
python3 003_6_agn_magnitudes.py MD40 all 519 
python3 003_6_agn_magnitudes.py MD40 all 520 
python3 003_6_agn_magnitudes.py MD40 all 521 
python3 003_6_agn_magnitudes.py MD40 all 522 
python3 003_6_agn_magnitudes.py MD40 all 523 
python3 003_6_agn_magnitudes.py MD40 all 524 
python3 003_6_agn_magnitudes.py MD40 all 525 
python3 003_6_agn_magnitudes.py MD40 all 526 
python3 003_6_agn_magnitudes.py MD40 all 527 
python3 003_6_agn_magnitudes.py MD40 all 528 
python3 003_6_agn_magnitudes.py MD40 all 529 
python3 003_6_agn_magnitudes.py MD40 all 530 
python3 003_6_agn_magnitudes.py MD40 all 531 
 
