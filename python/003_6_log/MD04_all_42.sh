#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=42MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/42_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/42_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 588 
python3 003_6_agn_magnitudes.py MD04 all 589 
python3 003_6_agn_magnitudes.py MD04 all 590 
python3 003_6_agn_magnitudes.py MD04 all 591 
python3 003_6_agn_magnitudes.py MD04 all 592 
python3 003_6_agn_magnitudes.py MD04 all 593 
python3 003_6_agn_magnitudes.py MD04 all 594 
python3 003_6_agn_magnitudes.py MD04 all 595 
python3 003_6_agn_magnitudes.py MD04 all 596 
python3 003_6_agn_magnitudes.py MD04 all 597 
python3 003_6_agn_magnitudes.py MD04 all 598 
python3 003_6_agn_magnitudes.py MD04 all 599 
python3 003_6_agn_magnitudes.py MD04 all 600 
python3 003_6_agn_magnitudes.py MD04 all 601 
 
