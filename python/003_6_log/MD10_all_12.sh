#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=12MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/12_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/12_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 168 
python3 003_6_agn_magnitudes.py MD10 all 169 
python3 003_6_agn_magnitudes.py MD10 all 170 
python3 003_6_agn_magnitudes.py MD10 all 171 
python3 003_6_agn_magnitudes.py MD10 all 172 
python3 003_6_agn_magnitudes.py MD10 all 173 
python3 003_6_agn_magnitudes.py MD10 all 174 
python3 003_6_agn_magnitudes.py MD10 all 175 
python3 003_6_agn_magnitudes.py MD10 all 176 
python3 003_6_agn_magnitudes.py MD10 all 177 
python3 003_6_agn_magnitudes.py MD10 all 178 
python3 003_6_agn_magnitudes.py MD10 all 179 
python3 003_6_agn_magnitudes.py MD10 all 180 
python3 003_6_agn_magnitudes.py MD10 all 181 
 
