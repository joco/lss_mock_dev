#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=06MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/06_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/06_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 84 
python3 003_6_agn_magnitudes.py MD04 all 85 
python3 003_6_agn_magnitudes.py MD04 all 86 
python3 003_6_agn_magnitudes.py MD04 all 87 
python3 003_6_agn_magnitudes.py MD04 all 88 
python3 003_6_agn_magnitudes.py MD04 all 89 
python3 003_6_agn_magnitudes.py MD04 all 90 
python3 003_6_agn_magnitudes.py MD04 all 91 
python3 003_6_agn_magnitudes.py MD04 all 92 
python3 003_6_agn_magnitudes.py MD04 all 93 
python3 003_6_agn_magnitudes.py MD04 all 94 
python3 003_6_agn_magnitudes.py MD04 all 95 
python3 003_6_agn_magnitudes.py MD04 all 96 
python3 003_6_agn_magnitudes.py MD04 all 97 
 
