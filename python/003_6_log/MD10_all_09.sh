#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=09MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/09_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/09_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 126 
python3 003_6_agn_magnitudes.py MD10 all 127 
python3 003_6_agn_magnitudes.py MD10 all 128 
python3 003_6_agn_magnitudes.py MD10 all 129 
python3 003_6_agn_magnitudes.py MD10 all 130 
python3 003_6_agn_magnitudes.py MD10 all 131 
python3 003_6_agn_magnitudes.py MD10 all 132 
python3 003_6_agn_magnitudes.py MD10 all 133 
python3 003_6_agn_magnitudes.py MD10 all 134 
python3 003_6_agn_magnitudes.py MD10 all 135 
python3 003_6_agn_magnitudes.py MD10 all 136 
python3 003_6_agn_magnitudes.py MD10 all 137 
python3 003_6_agn_magnitudes.py MD10 all 138 
python3 003_6_agn_magnitudes.py MD10 all 139 
 
