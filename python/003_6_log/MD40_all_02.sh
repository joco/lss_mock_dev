#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=02MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/02_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/02_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 28 
python3 003_6_agn_magnitudes.py MD40 all 29 
python3 003_6_agn_magnitudes.py MD40 all 30 
python3 003_6_agn_magnitudes.py MD40 all 31 
python3 003_6_agn_magnitudes.py MD40 all 32 
python3 003_6_agn_magnitudes.py MD40 all 33 
python3 003_6_agn_magnitudes.py MD40 all 34 
python3 003_6_agn_magnitudes.py MD40 all 35 
python3 003_6_agn_magnitudes.py MD40 all 36 
python3 003_6_agn_magnitudes.py MD40 all 37 
python3 003_6_agn_magnitudes.py MD40 all 38 
python3 003_6_agn_magnitudes.py MD40 all 39 
python3 003_6_agn_magnitudes.py MD40 all 40 
python3 003_6_agn_magnitudes.py MD40 all 41 
 
