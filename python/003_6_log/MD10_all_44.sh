#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=44MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/44_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/44_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 616 
python3 003_6_agn_magnitudes.py MD10 all 617 
python3 003_6_agn_magnitudes.py MD10 all 618 
python3 003_6_agn_magnitudes.py MD10 all 619 
python3 003_6_agn_magnitudes.py MD10 all 620 
python3 003_6_agn_magnitudes.py MD10 all 621 
python3 003_6_agn_magnitudes.py MD10 all 622 
python3 003_6_agn_magnitudes.py MD10 all 623 
python3 003_6_agn_magnitudes.py MD10 all 624 
python3 003_6_agn_magnitudes.py MD10 all 625 
python3 003_6_agn_magnitudes.py MD10 all 626 
python3 003_6_agn_magnitudes.py MD10 all 627 
python3 003_6_agn_magnitudes.py MD10 all 628 
python3 003_6_agn_magnitudes.py MD10 all 629 
 
