#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=49MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/49_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/49_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 686 
python3 003_6_agn_magnitudes.py MD04 all 687 
python3 003_6_agn_magnitudes.py MD04 all 688 
python3 003_6_agn_magnitudes.py MD04 all 689 
python3 003_6_agn_magnitudes.py MD04 all 690 
python3 003_6_agn_magnitudes.py MD04 all 691 
python3 003_6_agn_magnitudes.py MD04 all 692 
python3 003_6_agn_magnitudes.py MD04 all 693 
python3 003_6_agn_magnitudes.py MD04 all 694 
python3 003_6_agn_magnitudes.py MD04 all 695 
python3 003_6_agn_magnitudes.py MD04 all 696 
python3 003_6_agn_magnitudes.py MD04 all 697 
python3 003_6_agn_magnitudes.py MD04 all 698 
python3 003_6_agn_magnitudes.py MD04 all 699 
 
