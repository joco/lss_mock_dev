#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=34MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/34_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/34_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 476 
python3 003_6_agn_magnitudes.py MD10 all 477 
python3 003_6_agn_magnitudes.py MD10 all 478 
python3 003_6_agn_magnitudes.py MD10 all 479 
python3 003_6_agn_magnitudes.py MD10 all 480 
python3 003_6_agn_magnitudes.py MD10 all 481 
python3 003_6_agn_magnitudes.py MD10 all 482 
python3 003_6_agn_magnitudes.py MD10 all 483 
python3 003_6_agn_magnitudes.py MD10 all 484 
python3 003_6_agn_magnitudes.py MD10 all 485 
python3 003_6_agn_magnitudes.py MD10 all 486 
python3 003_6_agn_magnitudes.py MD10 all 487 
python3 003_6_agn_magnitudes.py MD10 all 488 
python3 003_6_agn_magnitudes.py MD10 all 489 
 
