#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=05MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/05_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/05_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 70 
python3 003_6_agn_magnitudes.py MD04 all 71 
python3 003_6_agn_magnitudes.py MD04 all 72 
python3 003_6_agn_magnitudes.py MD04 all 73 
python3 003_6_agn_magnitudes.py MD04 all 74 
python3 003_6_agn_magnitudes.py MD04 all 75 
python3 003_6_agn_magnitudes.py MD04 all 76 
python3 003_6_agn_magnitudes.py MD04 all 77 
python3 003_6_agn_magnitudes.py MD04 all 78 
python3 003_6_agn_magnitudes.py MD04 all 79 
python3 003_6_agn_magnitudes.py MD04 all 80 
python3 003_6_agn_magnitudes.py MD04 all 81 
python3 003_6_agn_magnitudes.py MD04 all 82 
python3 003_6_agn_magnitudes.py MD04 all 83 
 
