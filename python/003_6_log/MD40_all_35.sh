#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=35MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/35_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/35_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 490 
python3 003_6_agn_magnitudes.py MD40 all 491 
python3 003_6_agn_magnitudes.py MD40 all 492 
python3 003_6_agn_magnitudes.py MD40 all 493 
python3 003_6_agn_magnitudes.py MD40 all 494 
python3 003_6_agn_magnitudes.py MD40 all 495 
python3 003_6_agn_magnitudes.py MD40 all 496 
python3 003_6_agn_magnitudes.py MD40 all 497 
python3 003_6_agn_magnitudes.py MD40 all 498 
python3 003_6_agn_magnitudes.py MD40 all 499 
python3 003_6_agn_magnitudes.py MD40 all 500 
python3 003_6_agn_magnitudes.py MD40 all 501 
python3 003_6_agn_magnitudes.py MD40 all 502 
python3 003_6_agn_magnitudes.py MD40 all 503 
 
