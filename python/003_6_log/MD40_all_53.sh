#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=53MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/53_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/53_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 742 
python3 003_6_agn_magnitudes.py MD40 all 743 
python3 003_6_agn_magnitudes.py MD40 all 744 
python3 003_6_agn_magnitudes.py MD40 all 745 
python3 003_6_agn_magnitudes.py MD40 all 746 
python3 003_6_agn_magnitudes.py MD40 all 747 
python3 003_6_agn_magnitudes.py MD40 all 748 
python3 003_6_agn_magnitudes.py MD40 all 749 
python3 003_6_agn_magnitudes.py MD40 all 750 
python3 003_6_agn_magnitudes.py MD40 all 751 
python3 003_6_agn_magnitudes.py MD40 all 752 
python3 003_6_agn_magnitudes.py MD40 all 753 
python3 003_6_agn_magnitudes.py MD40 all 754 
python3 003_6_agn_magnitudes.py MD40 all 755 
 
