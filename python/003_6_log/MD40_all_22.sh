#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=22MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/22_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/22_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 308 
python3 003_6_agn_magnitudes.py MD40 all 309 
python3 003_6_agn_magnitudes.py MD40 all 310 
python3 003_6_agn_magnitudes.py MD40 all 311 
python3 003_6_agn_magnitudes.py MD40 all 312 
python3 003_6_agn_magnitudes.py MD40 all 313 
python3 003_6_agn_magnitudes.py MD40 all 314 
python3 003_6_agn_magnitudes.py MD40 all 315 
python3 003_6_agn_magnitudes.py MD40 all 316 
python3 003_6_agn_magnitudes.py MD40 all 317 
python3 003_6_agn_magnitudes.py MD40 all 318 
python3 003_6_agn_magnitudes.py MD40 all 319 
python3 003_6_agn_magnitudes.py MD40 all 320 
python3 003_6_agn_magnitudes.py MD40 all 321 
 
