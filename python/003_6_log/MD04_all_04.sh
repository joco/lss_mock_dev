#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=04MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/04_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/04_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 56 
python3 003_6_agn_magnitudes.py MD04 all 57 
python3 003_6_agn_magnitudes.py MD04 all 58 
python3 003_6_agn_magnitudes.py MD04 all 59 
python3 003_6_agn_magnitudes.py MD04 all 60 
python3 003_6_agn_magnitudes.py MD04 all 61 
python3 003_6_agn_magnitudes.py MD04 all 62 
python3 003_6_agn_magnitudes.py MD04 all 63 
python3 003_6_agn_magnitudes.py MD04 all 64 
python3 003_6_agn_magnitudes.py MD04 all 65 
python3 003_6_agn_magnitudes.py MD04 all 66 
python3 003_6_agn_magnitudes.py MD04 all 67 
python3 003_6_agn_magnitudes.py MD04 all 68 
python3 003_6_agn_magnitudes.py MD04 all 69 
 
