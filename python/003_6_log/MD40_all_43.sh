#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=43MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/43_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/43_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 602 
python3 003_6_agn_magnitudes.py MD40 all 603 
python3 003_6_agn_magnitudes.py MD40 all 604 
python3 003_6_agn_magnitudes.py MD40 all 605 
python3 003_6_agn_magnitudes.py MD40 all 606 
python3 003_6_agn_magnitudes.py MD40 all 607 
python3 003_6_agn_magnitudes.py MD40 all 608 
python3 003_6_agn_magnitudes.py MD40 all 609 
python3 003_6_agn_magnitudes.py MD40 all 610 
python3 003_6_agn_magnitudes.py MD40 all 611 
python3 003_6_agn_magnitudes.py MD40 all 612 
python3 003_6_agn_magnitudes.py MD40 all 613 
python3 003_6_agn_magnitudes.py MD40 all 614 
python3 003_6_agn_magnitudes.py MD40 all 615 
 
