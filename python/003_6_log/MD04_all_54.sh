#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=54MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/54_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/54_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 756 
python3 003_6_agn_magnitudes.py MD04 all 757 
python3 003_6_agn_magnitudes.py MD04 all 758 
python3 003_6_agn_magnitudes.py MD04 all 759 
python3 003_6_agn_magnitudes.py MD04 all 760 
python3 003_6_agn_magnitudes.py MD04 all 761 
python3 003_6_agn_magnitudes.py MD04 all 762 
python3 003_6_agn_magnitudes.py MD04 all 763 
python3 003_6_agn_magnitudes.py MD04 all 764 
python3 003_6_agn_magnitudes.py MD04 all 765 
python3 003_6_agn_magnitudes.py MD04 all 766 
python3 003_6_agn_magnitudes.py MD04 all 767 
 
