#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=15MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/15_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/15_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 210 
python3 003_6_agn_magnitudes.py MD04 all 211 
python3 003_6_agn_magnitudes.py MD04 all 212 
python3 003_6_agn_magnitudes.py MD04 all 213 
python3 003_6_agn_magnitudes.py MD04 all 214 
python3 003_6_agn_magnitudes.py MD04 all 215 
python3 003_6_agn_magnitudes.py MD04 all 216 
python3 003_6_agn_magnitudes.py MD04 all 217 
python3 003_6_agn_magnitudes.py MD04 all 218 
python3 003_6_agn_magnitudes.py MD04 all 219 
python3 003_6_agn_magnitudes.py MD04 all 220 
python3 003_6_agn_magnitudes.py MD04 all 221 
python3 003_6_agn_magnitudes.py MD04 all 222 
python3 003_6_agn_magnitudes.py MD04 all 223 
 
