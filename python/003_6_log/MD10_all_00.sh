#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=00MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/00_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/00_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 0 
python3 003_6_agn_magnitudes.py MD10 all 1 
python3 003_6_agn_magnitudes.py MD10 all 2 
python3 003_6_agn_magnitudes.py MD10 all 3 
python3 003_6_agn_magnitudes.py MD10 all 4 
python3 003_6_agn_magnitudes.py MD10 all 5 
python3 003_6_agn_magnitudes.py MD10 all 6 
python3 003_6_agn_magnitudes.py MD10 all 7 
python3 003_6_agn_magnitudes.py MD10 all 8 
python3 003_6_agn_magnitudes.py MD10 all 9 
python3 003_6_agn_magnitudes.py MD10 all 10 
python3 003_6_agn_magnitudes.py MD10 all 11 
python3 003_6_agn_magnitudes.py MD10 all 12 
python3 003_6_agn_magnitudes.py MD10 all 13 
 
