#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=18MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/18_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/18_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 252 
python3 003_6_agn_magnitudes.py MD40 all 253 
python3 003_6_agn_magnitudes.py MD40 all 254 
python3 003_6_agn_magnitudes.py MD40 all 255 
python3 003_6_agn_magnitudes.py MD40 all 256 
python3 003_6_agn_magnitudes.py MD40 all 257 
python3 003_6_agn_magnitudes.py MD40 all 258 
python3 003_6_agn_magnitudes.py MD40 all 259 
python3 003_6_agn_magnitudes.py MD40 all 260 
python3 003_6_agn_magnitudes.py MD40 all 261 
python3 003_6_agn_magnitudes.py MD40 all 262 
python3 003_6_agn_magnitudes.py MD40 all 263 
python3 003_6_agn_magnitudes.py MD40 all 264 
python3 003_6_agn_magnitudes.py MD40 all 265 
 
