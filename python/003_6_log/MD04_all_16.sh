#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=16MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/16_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/16_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 224 
python3 003_6_agn_magnitudes.py MD04 all 225 
python3 003_6_agn_magnitudes.py MD04 all 226 
python3 003_6_agn_magnitudes.py MD04 all 227 
python3 003_6_agn_magnitudes.py MD04 all 228 
python3 003_6_agn_magnitudes.py MD04 all 229 
python3 003_6_agn_magnitudes.py MD04 all 230 
python3 003_6_agn_magnitudes.py MD04 all 231 
python3 003_6_agn_magnitudes.py MD04 all 232 
python3 003_6_agn_magnitudes.py MD04 all 233 
python3 003_6_agn_magnitudes.py MD04 all 234 
python3 003_6_agn_magnitudes.py MD04 all 235 
python3 003_6_agn_magnitudes.py MD04 all 236 
python3 003_6_agn_magnitudes.py MD04 all 237 
 
