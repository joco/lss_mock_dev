#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=08MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/08_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/08_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 112 
python3 003_6_agn_magnitudes.py MD40 all 113 
python3 003_6_agn_magnitudes.py MD40 all 114 
python3 003_6_agn_magnitudes.py MD40 all 115 
python3 003_6_agn_magnitudes.py MD40 all 116 
python3 003_6_agn_magnitudes.py MD40 all 117 
python3 003_6_agn_magnitudes.py MD40 all 118 
python3 003_6_agn_magnitudes.py MD40 all 119 
python3 003_6_agn_magnitudes.py MD40 all 120 
python3 003_6_agn_magnitudes.py MD40 all 121 
python3 003_6_agn_magnitudes.py MD40 all 122 
python3 003_6_agn_magnitudes.py MD40 all 123 
python3 003_6_agn_magnitudes.py MD40 all 124 
python3 003_6_agn_magnitudes.py MD40 all 125 
 
