#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=07MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/07_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/07_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 98 
python3 003_6_agn_magnitudes.py MD04 all 99 
python3 003_6_agn_magnitudes.py MD04 all 100 
python3 003_6_agn_magnitudes.py MD04 all 101 
python3 003_6_agn_magnitudes.py MD04 all 102 
python3 003_6_agn_magnitudes.py MD04 all 103 
python3 003_6_agn_magnitudes.py MD04 all 104 
python3 003_6_agn_magnitudes.py MD04 all 105 
python3 003_6_agn_magnitudes.py MD04 all 106 
python3 003_6_agn_magnitudes.py MD04 all 107 
python3 003_6_agn_magnitudes.py MD04 all 108 
python3 003_6_agn_magnitudes.py MD04 all 109 
python3 003_6_agn_magnitudes.py MD04 all 110 
python3 003_6_agn_magnitudes.py MD04 all 111 
 
