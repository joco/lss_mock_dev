#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=51MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/51_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/51_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 714 
python3 003_6_agn_magnitudes.py MD10 all 715 
python3 003_6_agn_magnitudes.py MD10 all 716 
python3 003_6_agn_magnitudes.py MD10 all 717 
python3 003_6_agn_magnitudes.py MD10 all 718 
python3 003_6_agn_magnitudes.py MD10 all 719 
python3 003_6_agn_magnitudes.py MD10 all 720 
python3 003_6_agn_magnitudes.py MD10 all 721 
python3 003_6_agn_magnitudes.py MD10 all 722 
python3 003_6_agn_magnitudes.py MD10 all 723 
python3 003_6_agn_magnitudes.py MD10 all 724 
python3 003_6_agn_magnitudes.py MD10 all 725 
python3 003_6_agn_magnitudes.py MD10 all 726 
python3 003_6_agn_magnitudes.py MD10 all 727 
 
