#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=47MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/47_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/47_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 658 
python3 003_6_agn_magnitudes.py MD10 all 659 
python3 003_6_agn_magnitudes.py MD10 all 660 
python3 003_6_agn_magnitudes.py MD10 all 661 
python3 003_6_agn_magnitudes.py MD10 all 662 
python3 003_6_agn_magnitudes.py MD10 all 663 
python3 003_6_agn_magnitudes.py MD10 all 664 
python3 003_6_agn_magnitudes.py MD10 all 665 
python3 003_6_agn_magnitudes.py MD10 all 666 
python3 003_6_agn_magnitudes.py MD10 all 667 
python3 003_6_agn_magnitudes.py MD10 all 668 
python3 003_6_agn_magnitudes.py MD10 all 669 
python3 003_6_agn_magnitudes.py MD10 all 670 
python3 003_6_agn_magnitudes.py MD10 all 671 
 
