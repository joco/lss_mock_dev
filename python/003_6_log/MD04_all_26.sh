#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=26MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/26_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/26_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 364 
python3 003_6_agn_magnitudes.py MD04 all 365 
python3 003_6_agn_magnitudes.py MD04 all 366 
python3 003_6_agn_magnitudes.py MD04 all 367 
python3 003_6_agn_magnitudes.py MD04 all 368 
python3 003_6_agn_magnitudes.py MD04 all 369 
python3 003_6_agn_magnitudes.py MD04 all 370 
python3 003_6_agn_magnitudes.py MD04 all 371 
python3 003_6_agn_magnitudes.py MD04 all 372 
python3 003_6_agn_magnitudes.py MD04 all 373 
python3 003_6_agn_magnitudes.py MD04 all 374 
python3 003_6_agn_magnitudes.py MD04 all 375 
python3 003_6_agn_magnitudes.py MD04 all 376 
python3 003_6_agn_magnitudes.py MD04 all 377 
 
