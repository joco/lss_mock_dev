#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=03MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/03_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/03_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 42 
python3 003_6_agn_magnitudes.py MD40 all 43 
python3 003_6_agn_magnitudes.py MD40 all 44 
python3 003_6_agn_magnitudes.py MD40 all 45 
python3 003_6_agn_magnitudes.py MD40 all 46 
python3 003_6_agn_magnitudes.py MD40 all 47 
python3 003_6_agn_magnitudes.py MD40 all 48 
python3 003_6_agn_magnitudes.py MD40 all 49 
python3 003_6_agn_magnitudes.py MD40 all 50 
python3 003_6_agn_magnitudes.py MD40 all 51 
python3 003_6_agn_magnitudes.py MD40 all 52 
python3 003_6_agn_magnitudes.py MD40 all 53 
python3 003_6_agn_magnitudes.py MD40 all 54 
python3 003_6_agn_magnitudes.py MD40 all 55 
 
