#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=01MD04 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/01_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/01_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD04 all 14 
python3 003_6_agn_magnitudes.py MD04 all 15 
python3 003_6_agn_magnitudes.py MD04 all 16 
python3 003_6_agn_magnitudes.py MD04 all 17 
python3 003_6_agn_magnitudes.py MD04 all 18 
python3 003_6_agn_magnitudes.py MD04 all 19 
python3 003_6_agn_magnitudes.py MD04 all 20 
python3 003_6_agn_magnitudes.py MD04 all 21 
python3 003_6_agn_magnitudes.py MD04 all 22 
python3 003_6_agn_magnitudes.py MD04 all 23 
python3 003_6_agn_magnitudes.py MD04 all 24 
python3 003_6_agn_magnitudes.py MD04 all 25 
python3 003_6_agn_magnitudes.py MD04 all 26 
python3 003_6_agn_magnitudes.py MD04 all 27 
 
