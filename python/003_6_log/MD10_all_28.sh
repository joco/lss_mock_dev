#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=28MD10 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/28_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/28_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD10 all 392 
python3 003_6_agn_magnitudes.py MD10 all 393 
python3 003_6_agn_magnitudes.py MD10 all 394 
python3 003_6_agn_magnitudes.py MD10 all 395 
python3 003_6_agn_magnitudes.py MD10 all 396 
python3 003_6_agn_magnitudes.py MD10 all 397 
python3 003_6_agn_magnitudes.py MD10 all 398 
python3 003_6_agn_magnitudes.py MD10 all 399 
python3 003_6_agn_magnitudes.py MD10 all 400 
python3 003_6_agn_magnitudes.py MD10 all 401 
python3 003_6_agn_magnitudes.py MD10 all 402 
python3 003_6_agn_magnitudes.py MD10 all 403 
python3 003_6_agn_magnitudes.py MD10 all 404 
python3 003_6_agn_magnitudes.py MD10 all 405 
 
