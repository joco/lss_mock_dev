#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=13MD40 
#SBATCH --output=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/13_out 
#SBATCH --error=/home/comparat/software/linux/lss_mock_dev/python/003_6_log/13_err 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 003_6_agn_magnitudes.py MD40 all 182 
python3 003_6_agn_magnitudes.py MD40 all 183 
python3 003_6_agn_magnitudes.py MD40 all 184 
python3 003_6_agn_magnitudes.py MD40 all 185 
python3 003_6_agn_magnitudes.py MD40 all 186 
python3 003_6_agn_magnitudes.py MD40 all 187 
python3 003_6_agn_magnitudes.py MD40 all 188 
python3 003_6_agn_magnitudes.py MD40 all 189 
python3 003_6_agn_magnitudes.py MD40 all 190 
python3 003_6_agn_magnitudes.py MD40 all 191 
python3 003_6_agn_magnitudes.py MD40 all 192 
python3 003_6_agn_magnitudes.py MD40 all 193 
python3 003_6_agn_magnitudes.py MD40 all 194 
python3 003_6_agn_magnitudes.py MD40 all 195 
 
