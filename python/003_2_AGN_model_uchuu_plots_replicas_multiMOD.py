import time
t0 = time.time()

import glob, os, sys
from astropy.table import Table, Column

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p

import astropy.io.fits as fits
import healpy
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
from scipy.integrate import quad
from scipy.interpolate import interp1d

#import h5py
import numpy as n

#import astropy.io.fits as fits
# import all pathes

env = 'UCHUU'
agn_dir = 'GPX8'
print(env)

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
h = 0.6774
L_box = 1000.0 / h
cosmo = cosmoUNIT
zs = n.arange(0.0000001, 7.1, 0.001)
dm_itp = interp1d(zs, cosmo.distmod(zs).value)

z_dir = sys.argv[1]
z_str = z_dir
mean_z = int(z_dir[1])+int(z_dir[3:])/100.

str_scatter_0 = '0.8'
str_fsat = '10'

p_2_catalogues = n.array( glob.glob( os.path.join(os.environ[env], 'GPX8', z_dir, 'replication_*_*_*', 'glist.fits') ) )
p_2_catalogues.sort()
print(len(p_2_catalogues), 'catalogues to loop over')
for p_2_catalogue in p_2_catalogues:
	path_2_AGN_catalogs = n.array( glob.glob( os.path.join( os.path.dirname(p_2_catalogue), 'AGN_list_sigma_*_fsat_*.fits' ) ) )
	path_2_GAL_catalog = os.path.join( os.path.dirname(p_2_catalogue), 'glist.fits'      )
	path_2_MAG_catalog = os.path.join( os.path.dirname(p_2_catalogue), 'Kmatch_mags.fits')
	print(path_2_AGN_catalogs, path_2_GAL_catalog, path_2_MAG_catalog, time.time()-t0)
	print(os.path.isfile(path_2_GAL_catalog)==False, os.path.isfile(path_2_MAG_catalog)==False)
	print('missing files ?', os.path.isfile(path_2_GAL_catalog)==False or os.path.isfile(path_2_MAG_catalog)==False)
	if os.path.isfile(path_2_GAL_catalog)==False or os.path.isfile(path_2_MAG_catalog)==False :
		print('missing files, passing')
		continue

	p_2_area_t = os.path.join( os.environ[env], 'area_per_replica_'+z_dir+'.fits')
	print(p_2_area_t)
	t_area = Table.read( p_2_area_t )
	replication_dir = p_2_catalogue.split('/')[7]
	ix = float(replication_dir.split('_')[1])
	iy = float(replication_dir.split('_')[2])
	iz = float(replication_dir.split('_')[3])
	#print(ix, iy, iz)
	selection_area = ( t_area['jx'] == ix ) & ( t_area['jy'] == iy ) & ( t_area['jz'] == iz )
	print(t_area[selection_area])
	sky_frac_Dmin = t_area['area_DC_min'][selection_area].sum() / t_area['area_DC_min'].sum()
	sky_frac_Dmax = t_area['area_DC_max'][selection_area].sum() / t_area['area_DC_max'].sum()
	sky_frac = sky_frac_Dmax # ( sky_frac_Dmin + sky_frac_Dmax ) / 2.
	area = t_area['area_DC_max'][selection_area].sum()
	if area<1000.:
		print('area smaller than 1000 deg2, passing')
		continue
	#
	fig_base = z_dir + '_' + replication_dir +'_'
	print(fig_base)
	AGN = {}
	t0 = time.time()
	for path_2_AGN_catalog  in path_2_AGN_catalogs:
		#hd = fits.open(path_2_AGN_catalog)
		# AGN data
		short_name = os.path.basename(path_2_AGN_catalog)[9:-5]
		print(short_name, time.time()-t0)
		AGN[short_name] = Table.read(path_2_AGN_catalog)
		#z = AGN[short_name]['redshift_R']
		#lx = hd[1].data['LX_hard']
		#logNH = hd[1].data['logNH']
		#fx = hd[1].data['FX_soft']
		#lx_0520 = hd[1].data['LX_soft']
		#logm = hd[1].data['galaxy_SMHMR_mass']
		#lsar = lx - logm
		#mag_r = hd[1].data['SDSS_r_AB']
	hg = fits.open(path_2_GAL_catalog)
	hm = fits.open(path_2_MAG_catalog)
	#g_lat = hd[1].data['g_lat']
	# galaxy data
	logm_gal = n.log10(hg[1].data['sm'])
	z_gal = hg[1].data['redshift_R']
	z_maximum = n.max(z_gal)
	z_minimum = n.min(z_gal)
	z_mean = 0.5 * (z_minimum + z_maximum)
	rmag_gal = hm[1].data['rmag']
	kmagABS_gal = hm[1].data['K_mag_abs']

	if mean_z<0.5:
		path_2_GAMA = os.path.join(os.environ['HOME'], 'ptmp_joco/GAMA/', 'forJohan.fits')
		t_gama = Table.read(path_2_GAMA)
		kmag = 8.9 - 2.5*n.log10(t_gama['flux_Kt'])
		zmag = 8.9 - 2.5*n.log10(t_gama['flux_Zt'])
		rmag = 8.9 - 2.5*n.log10(t_gama['flux_rt'])
		imag = 8.9 - 2.5*n.log10(t_gama['flux_it'])
		keep_GAMA = (kmag>0) & (t_gama['Z']>z_minimum) & (t_gama['Z']<z_maximum) & (rmag>0)
		t_gama = Table(t_gama[keep_GAMA])
		kmag = 8.9 - 2.5*n.log10(t_gama['flux_Kt'])
		#zmag = 8.9 - 2.5*n.log10(t_gama['flux_Zt'])
		rmag = 8.9 - 2.5*n.log10(t_gama['flux_rt'])
		#imag = 8.9 - 2.5*n.log10(t_gama['flux_it'])

		KCORR_DATA = n.loadtxt( os.path.join( os.environ['GIT_AGN_MOCK'], 'data/K-corr/VISTA_Ks_kcorrections.txt'), unpack = True)
		# redshift distmod bandpass kcorr_median kcorr_16pc kcorr_84pc
		#bandpass_itp = interp1d(KCORR_DATA[0], KCORR_DATA[2])
		kcorr_itp = interp1d(
			n.hstack(( 0., KCORR_DATA[0])),
			n.hstack((KCORR_DATA[3][0], KCORR_DATA[3])) )
		#kmag = absmag + distmod + bandpass + kcorrection
		# absmag = appmag - distmod - kcorr
		# is properly zero-centred; or at least zero-ish; the median +/- NMAD is 0.05 +/- 0.12.

		dm_values = dm_itp(t_gama['Z'].data)
		#kmag_abs = kmag - ( dm_values + bandpass_itp(t_gama['z_peak']) + kcorr_itp(t_gama['z_peak']) )
		kmag_abs = kmag - ( dm_values + kcorr_itp(t_gama['Z'].data) )
		rmag_abs = rmag - ( dm_values + kcorr_itp(t_gama['Z'].data) )

		t_gama['Kmag_abs'] = n.array(kmag_abs)
		t_gama['Rmag_abs'] = n.array(rmag_abs)
		t_gama['dist_mod'] = dm_values
		t_gama['ms'] = n.array(n.log10( t_gama['StellarMass_50'] ))
		volume_GAMA = (cosmo.comoving_volume(z_maximum).value - cosmo.comoving_volume(z_minimum).value) * n.pi * 60. / 129600.

	if mean_z>0.05:
		path_2_COSMOS = os.path.join(os.environ['HOME'],'ptmp_joco/COSMOS/','photoz_vers2.0_010312.fits')
		t = Table.read(path_2_COSMOS)
		good = (t['photoz']>z_minimum )&( t['photoz']< z_maximum ) & ( t['MK']<0 )&( t['MK']>-40 )&( t['mass_med']<14 )&( t['mass_med']>4 )
		t_cosmos = t[good]
		volume_cosmos = (cosmo.comoving_volume(z_maximum).value - cosmo.comoving_volume(z_minimum).value) * n.pi * 2. / 129600.

	agn_data_dir = os.path.join( os.environ['GIT_AGN_MOCK'], 'data', 'agn' )
	fig_dir      = os.path.join( os.environ['GIT_AGN_MOCK'], 'figures', env, 'agn_multi_model' )
	logNlogS_dir = os.path.join( fig_dir, 'logNlogS' )
	logNlogR_dir = os.path.join( fig_dir, 'logNlogR' )
	XLF_dir      = os.path.join( fig_dir, 'XLF')
	RLF_dir      = os.path.join( fig_dir, 'rLF')
	DC_dir       = os.path.join( fig_dir, 'duty_cycle')
	LSAR_dir     = os.path.join( fig_dir, 'LSAR')
	if os.path.isdir(fig_dir) == False:
		os.system('mkdir -p ' + fig_dir)
	if os.path.isdir(logNlogS_dir) == False:
		os.system('mkdir -p ' + logNlogS_dir)
	if os.path.isdir(logNlogR_dir) == False:
		os.system('mkdir -p ' + logNlogR_dir)
	if os.path.isdir(XLF_dir) == False:
		os.system('mkdir -p ' + XLF_dir)
	if os.path.isdir(DC_dir) == False:
		os.system('mkdir -p ' + DC_dir)
	if os.path.isdir(LSAR_dir) == False:
		os.system('mkdir -p ' + LSAR_dir)
	if os.path.isdir(RLF_dir) == False:
		os.system('mkdir -p ' + RLF_dir)

	f_duty = interp1d(n.array([0., 0.75, 2., 3.5, 10.1]), n.array([0.1, 0.2, 0.3, 0.3, 0.3]))
	f_duty = interp1d(n.array([0.   , 0.25,  0.75,  10.1]) ,
					10**n.array([-0.8, -0.80, -0.40, -0.40]) )

	# redshift binning
	DZ = z_maximum - z_minimum
	all_zs = n.arange(z_minimum, z_maximum, DZ*.99)

	# LF binning
	dlog_lx = 0.05
	lx_bins = n.arange(36, 48, dlog_lx)
	x_lx = lx_bins[:-1] + dlog_lx / 2.

	# LSAR histogram
	dlog_LSAR = 0.1
	LSAR_bins = n.arange(30, 38, dlog_LSAR)
	x_LSAR = LSAR_bins[:-1] + dlog_LSAR / 2.

	# SMF and duty cycle
	dlogM = 0.1
	bins_SMF = n.arange(8, 13, dlogM)
	x_SMF = (bins_SMF[1:] + bins_SMF[:-1]) * 0.5

	# rLF
	dlogR = 0.05
	bins_rLF = n.arange(-30, -8, dlogM)
	x_rLF = (bins_rLF[1:] + bins_rLF[:-1]) * 0.5

	# galaxy stellar mass function of Ilbert 2013
	# mass bins
	mbins = n.arange(8,12.5,0.25)
	path_ilbert13_SMF = os.path.join( os.environ['GIT_AGN_MOCK'], 'data','LF_SMF', "ilbert_2013_mass_function_params.txt")
	smf_ilbert13 = lambda M, M_star, phi_1s, alpha_1s, phi_2s, alpha_2s : ( phi_1s * (M/M_star) ** alpha_1s + phi_2s * (M/M_star) ** alpha_2s ) * n.e ** (-M/M_star) * (M/ M_star)
	smf_ilbert_z_minimum, smf_ilbert_z_maximum, N, M_comp, M_star, phi_1s, alpha_1s, phi_2s, alpha_2s, log_rho_s = n.loadtxt(path_ilbert13_SMF, unpack=True)
	smf_ilbert_fun = n.array([
	lambda mass : smf_ilbert13( mass , 10**M_star[0], phi_1s[0]*10**(-3), alpha_1s[0], phi_2s[0]*10**(-3), alpha_2s[0] )
	, lambda mass : smf_ilbert13( mass , 10**M_star[1], phi_1s[1]*10**(-3), alpha_1s[1], phi_2s[1]*10**(-3), alpha_2s[1] )
	, lambda mass : smf_ilbert13( mass , 10**M_star[2], phi_1s[2]*10**(-3), alpha_1s[2], phi_2s[2]*10**(-3), alpha_2s[2] )
	, lambda mass : smf_ilbert13( mass , 10**M_star[3], phi_1s[3]*10**(-3), alpha_1s[3], phi_2s[3]*10**(-3), alpha_2s[3] )
	, lambda mass : smf_ilbert13( mass , 10**M_star[4], phi_1s[4]*10**(-3), alpha_1s[4], phi_2s[4]*10**(-3), alpha_2s[4] )
	, lambda mass : smf_ilbert13( mass , 10**M_star[5], phi_1s[5]*10**(-3), alpha_1s[5], phi_2s[5]*10**(-3), alpha_2s[5] )
	, lambda mass : smf_ilbert13( mass , 10**M_star[6], phi_1s[6]*10**(-3), alpha_1s[6], phi_2s[6]*10**(-3), alpha_2s[6] )
	, lambda mass : smf_ilbert13( mass , 10**M_star[7], phi_1s[7]*10**(-3), alpha_1s[7], phi_2s[7]*10**(-3), alpha_2s[7] )
	])

	smf_ilbert_name = n.array([ "Il13 "+str(z_minimum)+"<z<"+str(z_maximum) for z_minimum, z_maximum in zip(smf_ilbert_z_minimum,smf_ilbert_z_maximum) ])

	print(z_minimum, '<z<', z_maximum, ', t=', time.time()-t0)
	vol = (cosmo.comoving_volume(z_maximum).value - cosmo.comoving_volume(z_minimum).value) * n.pi * area / 129600.
	DL_mean_z = (cosmo.luminosity_distance(z_mean).to(u.cm)).value
	#print('volume', vol, 'Mpc3')

	# hard X-ray luminosity function Aird 2015)
	def kz_h(z): return 10**(-4.03 - 0.19 * (1 + z))
	def Ls_h(z): return 10**(44.84 - n.log10(((1 + 2.0) / (1 + z))** 3.87 + ((1 + 2.0) / (1 + z))**(-2.12)))
	def phi_h(L, z): return kz_h(z) / ((L / Ls_h(z))**0.48 + (L / Ls_h(z))**2.27)

	p.figure(1, (6, 6))
	p.axes([0.2, 0.18, 0.75, 0.75])
	#sel = ((z_minimum+z_maximum)*0.5>z0)&((z_minimum+z_maximum)*0.5<z1)
	# if len(sel.nonzero()[0])>0:
	# if len(list(set(z_center[sel])))>=2:
	#sel = (sel) & (z_center==z_center[sel][0])
	#p.plot(Lx_c[sel], phi[sel], label='Ha05 '+str(n.round(z_center[sel][0],2)), ls='dashed')

	# Aird 2015
	z_mean = (z_minimum + z_maximum) * 0.5 * n.ones_like(x_lx)
	# mock
	p.plot(x_lx, phi_h(10**x_lx, z_mean), c='cyan', ls='dashed', lw=2, label='Ai15')  # Aird 2-10 keV LADE')

	for kk in list(AGN.keys()):
		label = r'$\sigma=$' + kk.split('_')[1] + r',$f_{sat}=$'  + kk.split('_')[3]
		#z = AGN[kk]['redshift_R']
		lx = AGN[kk]['LX_hard']
		logNH = AGN[kk]['logNH']
		#fx = AGN[kk]['FX_soft']
		#lx_0520 = AGN[kk]['LX_soft']
		#logm = AGN[kk]['galaxy_SMHMR_mass']
		#lsar = lx - logm
		#mag_r = AGN[kk]['SDSS_r_AB']
		# Selections for the histogram
		NH20 = (logNH < 22)
		NH22 = (logNH >= 22) & (logNH < 24)
		NH24 = (logNH >= 24)
		N_nh20 = n.histogram(lx[NH20], lx_bins)[0] / vol / dlog_lx
		N_nh22 = n.histogram(lx[NH22], lx_bins)[0] / vol / dlog_lx
		N_nh24 = n.histogram(lx[NH24], lx_bins)[0] / vol / dlog_lx
		# full histogram
		nhar = n.histogram(lx, lx_bins)[0] / vol / dlog_lx
		nharN = n.histogram(lx, lx_bins)[0]  # /vol/dlog_lx
		#
		p.fill_between(x_lx,
						y1=(nhar) * (1 - (nharN)**(-0.5)),
						y2=(nhar) * (1 + (nharN)**(-0.5)),
						color='g',
						alpha=0.2,
						#label='Mock',
						lw=2, rasterized = True)  # 2-10  keV')
	p.plot(x_lx, N_nh20, label='nH<22', ls='dotted', lw=2)
	p.plot(x_lx, N_nh22, label='22<nH<24', ls='dotted', lw=2)
	p.plot(x_lx, N_nh24, label='24<nH', ls='dotted', lw=2)
	p.xlabel(r'$\log_{10}(L^{2-10\, keV}_X/[erg/s])$')
	p.ylabel(r'$\Phi$=dN/dlogL/dV [1/Mpc$^3$/dex]')
	p.legend(loc=3, fontsize=14)
	p.yscale('log')
	p.xlim((37., 46.5))
	p.ylim((1 / (2 * vol), 1e-2))
	p.title(str(n.round(z_minimum, 2)) + "<z<" + str(n.round(z_maximum, 2)))
	p.grid()
	p.savefig(os.path.join(XLF_dir, fig_base + "XLF_soft_" + z_str + ".png"))
	p.clf()
	print(os.path.join(XLF_dir, fig_base + "XLF_soft_" + z_str + ".png"), 'written')
	#DATA_XLF = n.transpose([x_lx, nhar, (nhar)*(1-(nharN)**(-0.5)), (nhar)*(1+(nharN)**(-0.5)), N_nh20, N_nh22, N_nh24, phi_h(10**x_lx,z_mean)])
	#n.savetxt(os.path.join(XLF_dir, fig_base +'XLF_soft_'+z_str+'.ascii'), DATA_XLF  )

	# XLF_ratio_
	p.figure(1, (6, 6))
	p.axes([0.18, 0.18, 0.75, 0.75])
	p.plot(x_lx, nhar / phi_h(10**x_lx, z_mean), label='2-10 keV')
	for kk in list(AGN.keys()):
		label = r'$\sigma=$' + kk.split('_')[1] + r',$f_{sat}=$'  + kk.split('_')[3]
		#z = AGN[kk]['redshift_R']
		lx = AGN[kk]['LX_hard']
		#logNH = AGN[kk]['logNH']
		#fx = AGN[kk]['FX_soft']
		#lx_0520 = AGN[kk]['LX_soft']
		#logm = AGN[kk]['galaxy_SMHMR_mass']
		#lsar = lx - logm
		#mag_r = AGN[kk]['SDSS_r_AB']
		# full histogram
		nhar = n.histogram(lx, lx_bins)[0] / vol / dlog_lx
		nharN = n.histogram(lx, lx_bins)[0]  # /vol/dlog_lx
		p.fill_between(x_lx,
					y1=1 - (nharN)**(-0.5),
					y2=1 + (nharN)**(-0.5),
					color='green',
					alpha=0.3, rasterized = True )#
					#label='mock ERR')
	p.xlabel(r'$\log_{10}(L^{2-10\, keV}_X/[erg/s])$')
	p.ylabel(r'mock/model')
	p.legend(frameon=False, loc=0)
	p.xlim((37., 46.5))
	p.ylim((0.7, 1.3))
	p.title(str(n.round(z_minimum, 2)) + "<z<" + str(n.round(z_maximum, 2)))
	p.grid()
	p.savefig(os.path.join(XLF_dir, fig_base + "XLF_ratio_" + z_str + ".png"))
	p.clf()
	print(os.path.join(XLF_dir, fig_base +"XLF_ratio_" + z_str + ".png"), 'written')

	# r band LF

	#Schechter_M_z = interp1d(M, phi * 0.7**3.)
	#mrange = n.arange(-24.6, -12.2, 0.01)
	#total_number = n.cumsum(Schechter_M_z(mrange)) * volume * fraction

	p.figure(1, (6, 6))
	p.axes([0.2, 0.18, 0.75, 0.75])
	for kk in list(AGN.keys()):
		label = r'$\sigma=$' + kk.split('_')[1] + r',$f_{sat}=$'  + kk.split('_')[3]
		z = AGN[kk]['redshift_R']
		#lx = AGN[kk]['LX_hard']
		#logNH = AGN[kk]['logNH']
		#fx = AGN[kk]['FX_soft']
		#lx_0520 = AGN[kk]['LX_soft']
		#logm = AGN[kk]['galaxy_SMHMR_mass']
		#lsar = lx - logm
		mag_r = AGN[kk]['SDSS_r_AB']
		DM = cosmo.distmod(z).value
		magABS_r = mag_r - DM
		Ragn  = n.histogram(magABS_r, bins_rLF)[0] / vol / dlogR
		RagnN  = n.histogram(magABS_r, bins_rLF)[0]
		p.fill_between(x_rLF,
						y1=(Ragn) * (1 - (RagnN)**(-0.5)),
						y2=(Ragn) * (1 + (RagnN)**(-0.5)),
						color='b',
						alpha=0.7,
						#label='Uchuu AGN',
						lw=2, rasterized = True)
	# galaxy data
	DM_gal = cosmo.distmod(z_gal).value
	magABS_r_gal = rmag_gal - DM_gal
	Rgal = n.histogram(magABS_r_gal, bins_rLF)[0] / vol / dlogR
	RgalN = n.histogram(magABS_r_gal, bins_rLF)[0]
	p.fill_between(x_rLF,
					y1=(Rgal) * (1 - (RgalN)**(-0.5)),
					y2=(Rgal) * (1 + (RgalN)**(-0.5)),
					color='k',
					alpha=0.7,
					label='Uchuu Galaxies',
					lw=2)
	if mean_z>0.05:
		rmagABS_galCOS = t_cosmos['MR']
		Rgal_cos = n.histogram(rmagABS_galCOS, bins_rLF)[0] / volume_cosmos / dlogR
		RgalN_cos = n.histogram(rmagABS_galCOS, bins_rLF)[0]
		p.fill_between(x_rLF,
						y1=(Rgal_cos) * (1 - (RgalN_cos)**(-0.5)),
						y2=(Rgal_cos) * (1 + (RgalN_cos)**(-0.5)),
						color='g',
						alpha=0.7,
						label='COSMOS',
						lw=2)
	if mean_z<0.5:
		ngal, M, phi, Err = n.loadtxt(os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'LF_loveday_2015', 'lf.txt'), unpack=True)
		p.plot(M, phi, label='loveday 2015 z=0.1')
		rmagABS_galGAMA = t_gama['Rmag_abs']
		Rgal_GAMA = n.histogram(rmagABS_galGAMA, bins_rLF)[0] / volume_GAMA / dlogR
		RgalN_GAMA = n.histogram(rmagABS_galGAMA, bins_rLF)[0]
		p.fill_between(x_rLF,
						y1=(Rgal_GAMA) * (1 - (RgalN_GAMA)**(-0.5)),
						y2=(Rgal_GAMA) * (1 + (RgalN_GAMA)**(-0.5)),
						color='orange',
						alpha=0.7,
						label='GAMA',
						lw=2)
	p.xlabel(r'$M_R$')
	p.ylabel(r'$\Phi$=dN/dlogL/dV [1/Mpc$^3$/dex]')
	p.legend(loc=4, fontsize=14)
	p.yscale('log')
	p.xlim((-28, -8))
	p.ylim((1 / (2 * vol), 1))
	p.title(str(n.round(z_minimum, 2)) + "<z<" + str(n.round(z_maximum, 2)))
	p.grid()
	p.savefig(os.path.join(RLF_dir, fig_base+ "RLF_" + z_str + ".png"))
	p.clf()
	print(os.path.join(RLF_dir, fig_base+ "RLF_" + z_str + ".png"), 'written')



	p.figure(1, (6, 6))
	p.axes([0.2, 0.18, 0.75, 0.75])
	#Ragn  = n.histogram(magABS_r, bins_rLF)[0] / vol / dlogR
	#RagnN  = n.histogram(magABS_r, bins_rLF)[0]
	#p.fill_between(x_rLF,
					#y1=(Ragn) * (1 - (RagnN)**(-0.5)),
					#y2=(Ragn) * (1 + (RagnN)**(-0.5)),
					#color='b',
					#alpha=0.7,
					#label='Mock AGN',
					#lw=2)  # 2-10  keV')

	Rgal = n.histogram(kmagABS_gal, bins_rLF)[0] / vol / dlogR
	RgalN = n.histogram(kmagABS_gal, bins_rLF)[0]

	p.fill_between(x_rLF,
					y1=(Rgal) * (1 - (RgalN)**(-0.5)),
					y2=(Rgal) * (1 + (RgalN)**(-0.5)),
					color='k',
					alpha=0.7,
					label='Uchuu Galaxies',
					lw=2)  # 2-10  keV')


	for kk in list(AGN.keys()):
		label = r'$\sigma=$' + kk.split('_')[1] + r',$f_{sat}=$'  + kk.split('_')[3]
		id_glist = AGN[kk]['ID_glist']
		is_active = n.isin(n.arange(len(kmagABS_gal)), id_glist)
		Rgal_AGN = n.histogram(kmagABS_gal[is_active], bins_rLF)[0] / vol / dlogR
		p.plot(x_rLF, Rgal_AGN, color='b', lw=1, rasterized = True)


	if mean_z>0.05:
		kmagABS_galCOS = t_cosmos['MK']
		Rgal_cos = n.histogram(kmagABS_galCOS, bins_rLF)[0] / volume_cosmos / dlogR
		RgalN_cos = n.histogram(kmagABS_galCOS, bins_rLF)[0]
		p.fill_between(x_rLF,
						y1=(Rgal_cos) * (1 - (RgalN_cos)**(-0.5)),
						y2=(Rgal_cos) * (1 + (RgalN_cos)**(-0.5)),
						color='g',
						alpha=0.7,
						label='COSMOS',
						lw=2)
	if mean_z<0.5:
		# k band LF
		Mk_m_5logh_z0, log10_phi_h3_Mpcm3_magm1 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data/LF_SMF', 'driver-2012-K-LF.txt'), unpack = True )
		p.plot(Mk_m_5logh_z0 - 5 * n.log10(h), 10**log10_phi_h3_Mpcm3_magm1 * 0.7**3, lw=3 , label='Driver 2012, z=0.07') # +5*n.log10(h)
		kmagABS_galGAMA = t_gama['Kmag_abs']
		Rgal_GAMA = n.histogram(kmagABS_galGAMA, bins_rLF)[0] / volume_GAMA / dlogR
		RgalN_GAMA = n.histogram(kmagABS_galGAMA, bins_rLF)[0]
		p.fill_between(x_rLF,
						y1=(Rgal_GAMA) * (1 - (RgalN_GAMA)**(-0.5)),
						y2=(Rgal_GAMA) * (1 + (RgalN_GAMA)**(-0.5)),
						color='orange',
						alpha=0.7,
						label='GAMA',
						lw=2)
	p.xlabel(r'$M_K$')
	p.ylabel(r'$\Phi$=dN/dlogL/dV [1/Mpc$^3$/dex]')
	p.legend(loc=4, fontsize=14)
	p.yscale('log')
	p.xlim((-28, -8))
	p.ylim((1 / (2 * vol), 1))
	p.title(str(n.round(z_minimum, 2)) + "<z<" + str(n.round(z_maximum, 2)))
	p.grid()
	p.savefig(os.path.join(RLF_dir, fig_base+"KLF_" + z_str + ".png"))
	p.clf()
	print(os.path.join(RLF_dir, fig_base+"KLF_" + z_str + ".png"), 'written')


	# LSAR histogram

	p.figure(1, (6, 6))
	p.axes([0.16, 0.15, 0.8, 0.8])

	if z_mean[0]<0.5:
		# Ge17
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_G17_z025.ascii'), unpack=True)
		fun = interp1d(x, 10**(0.5*(y_min+y_max)) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=10**(y_min)/nrm,  y2=10**(y_max)/nrm, label='Ge 17, M>8', alpha=0.5,color='grey')
		# A18
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_A17_010z050_095M100.ascii'), unpack=True)
		fun = interp1d(x, 0.5*(y_min+y_max) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=(y_min)/nrm,  y2=(y_max)/nrm, label='Ai 18, 9.5<M<10', alpha=0.5,color='green')
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_A17_010z050_100M105.ascii'), unpack=True)
		fun = interp1d(x, 0.5*(y_min+y_max) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=(y_min)/nrm,  y2=(y_max)/nrm, label='Ai 18, 10<M<10.5', alpha=0.5,color='red')
		# We17
		#edd_r = 10**n.arange(-6,0,0.1)
		#p.plot(n.arange(-6,0,0.1)+34, xi_LSAR(edd_r)/dlogf, ls='dashed', lw=2, label='We17 z=0.1' )

	if z_mean[0]>=0.5 and z_mean[0]<1. :
		#G17
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_G17_z075.ascii'), unpack=True)
		fun = interp1d(x, 10**(0.5*(y_min+y_max)) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=10**(y_min)/nrm,  y2=10**(y_max)/nrm, alpha=0.5,color='grey')#, label='G17, M>8'
		#A18
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_A17_050z100_095M100.ascii'), unpack=True)
		fun = interp1d(x, 0.5*(y_min+y_max) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=(y_min)/nrm,  y2=(y_max)/nrm,alpha=0.5,color='green')# label='A18, 9.5<M<10',
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_A17_050z100_100M105.ascii'), unpack=True)
		fun = interp1d(x, 0.5*(y_min+y_max) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=(y_min)/nrm,  y2=(y_max)/nrm,alpha=0.5,color='red')# label='A18, 10<M<10.5',

	if z_mean[0]>=1. and z_mean[0]<1.5 :
		#G17
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_G17_z125.ascii'), unpack=True)
		fun = interp1d(x, 10**(0.5*(y_min+y_max)) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=10**(y_min)/nrm,  y2=10**(y_max)/nrm, alpha=0.5,color='grey')# label='G17, M>8',
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_A17_100z150_095M100.ascii'), unpack=True)
		fun = interp1d(x, 0.5*(y_min+y_max) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=(y_min)/nrm,  y2=(y_max)/nrm,alpha=0.5,color='green')# label='A18, 9.5<M<10',
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_A17_100z150_100M105.ascii'), unpack=True)
		fun = interp1d(x, 0.5*(y_min+y_max) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=(y_min)/nrm,  y2=(y_max)/nrm, alpha=0.5,color='red')#label='A18, 10<M<10.5',

	if z_mean[0]>=1.5 and z_mean[0]<2. :
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_G17_z175.ascii'), unpack=True)
		fun = interp1d(x, 10**(0.5*(y_min+y_max)) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=10**(y_min)/nrm,  y2=10**(y_max)/nrm, alpha=0.5,color='grey')#, label='G17, M>8'
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_A17_150z200_095M100.ascii'), unpack=True)
		fun = interp1d(x, 0.5*(y_min+y_max) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=(y_min)/nrm,  y2=(y_max)/nrm, alpha=0.5,color='green')#, label='A18, 9.5<M<10'
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_A17_150z200_100M105.ascii'), unpack=True)
		fun = interp1d(x, 0.5*(y_min+y_max) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=(y_min)/nrm,  y2=(y_max)/nrm, alpha=0.5,color='red')#, label='A18, 10<M<10.5'

	if z_mean[0]>=2. and z_mean[0]<2.5 :
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_G17_z225.ascii'), unpack=True)
		fun = interp1d(x, 10**(0.5*(y_min+y_max)) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=10**(y_min)/nrm,  y2=10**(y_max)/nrm, alpha=0.5,color='grey')#, label='G17, M>8'
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_A17_200z250_100M105.ascii'), unpack=True)
		fun = interp1d(x, 0.5*(y_min+y_max) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=(y_min)/nrm,  y2=(y_max)/nrm, alpha=0.5,color='red')#, label='A18, 10<M<10.5'

	if z_mean[0]>=2.5 and z_mean[0]<3. :
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_G17_z275.ascii'), unpack=True)
		fun = interp1d(x, 10**(0.5*(y_min+y_max)) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=10**(y_min)/nrm,  y2=10**(y_max)/nrm, alpha=0.5,color='grey')#, label='G17, M>8'
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_A17_250z300_100M105.ascii'), unpack=True)
		fun = interp1d(x, 0.5*(y_min+y_max) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=(y_min)/nrm,  y2=(y_max)/nrm, alpha=0.5,color='red')#, label='A18, 10<M<10.5'

	if z_mean[0]>=3 :
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_G17_z350.ascii'), unpack=True)
		fun = interp1d(x, 10**(0.5*(y_min+y_max)) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=10**(y_min)/nrm,  y2=10**(y_max)/nrm, alpha=0.5,color='grey')#, label='G17, M>8'
		x, y_min, y_max = n.loadtxt( os.path.join( agn_data_dir,  'lsar_hist_A17_300z400_100M105.ascii'), unpack=True)
		fun = interp1d(x, 0.5*(y_min+y_max) )
		nrm = quad(fun,x.min(), x.max())[0]
		p.fill_between(x+34, y1=(y_min)/nrm,  y2=(y_max)/nrm,  alpha=0.5,color='red')#label='A18, 10<M<10.5',

	for kk in list(AGN.keys()):
		label = r'$\sigma=$' + kk.split('_')[1] + r',$f_{sat}=$'  + kk.split('_')[3]
		#z = AGN[kk]['redshift_R']
		lx = AGN[kk]['LX_hard']
		#logNH = AGN[kk]['logNH']
		#fx = AGN[kk]['FX_soft']
		#lx_0520 = AGN[kk]['LX_soft']
		logm = AGN[kk]['galaxy_SMHMR_mass']
		lsar = lx - logm
		#mag_r = AGN[kk]['SDSS_r_AB']

		nall = n.histogram(lsar, LSAR_bins)[0] / vol / dlog_LSAR
		nallN = n.histogram(lsar, LSAR_bins)[0]

		zsel = (logm >= 12)
		nall_12 = n.histogram(lsar[zsel], LSAR_bins)[0] / vol / dlog_LSAR
		nallN_12 = n.histogram(lsar[zsel], LSAR_bins)[0]

		zsel = (logm >= 11) & (logm < 12)
		nall_11 = n.histogram(lsar[zsel], LSAR_bins)[0] / vol / dlog_LSAR
		nallN_11 = n.histogram(lsar[zsel], LSAR_bins)[0]

		zsel = (logm >= 10) & (logm < 11)
		nall_10 = n.histogram(lsar[zsel], LSAR_bins)[0] / vol / dlog_LSAR
		nallN_10 = n.histogram(lsar[zsel], LSAR_bins)[0]

		zsel = (logm >= 9) & (logm < 10)
		nall_9 = n.histogram(lsar[zsel], LSAR_bins)[0] / vol / dlog_LSAR
		nallN_9 = n.histogram(lsar[zsel], LSAR_bins)[0]

		zsel = (logm >= 8) & (logm < 9)
		nall_8 = n.histogram(lsar[zsel], LSAR_bins)[0] / vol / dlog_LSAR
		nallN_8 = n.histogram(lsar[zsel], LSAR_bins)[0]

		fun = interp1d(x_LSAR, nall)
		nrm = quad(fun, x_LSAR.min(), x_LSAR.max())[0]
		p.plot(x_LSAR, nall / nrm, 'k', lw=3, rasterized = True )#, label='Mock all M')

		fun = interp1d(x_LSAR, nall_8)
		nrm = quad(fun, x_LSAR.min(), x_LSAR.max())[0]
		p.plot(x_LSAR, nall_8 / nrm, 'magenta', lw=2 , rasterized = True )#label='8<M<9')

		fun = interp1d(x_LSAR, nall_9)
		nrm = quad(fun, x_LSAR.min(), x_LSAR.max())[0]
		p.plot(x_LSAR, nall_9 / nrm, 'g', lw=2 , rasterized = True )#label='9<M<10')

		fun = interp1d(x_LSAR, nall_10)
		nrm = quad(fun, x_LSAR.min(), x_LSAR.max())[0]
		p.plot(x_LSAR, nall_10 / nrm, 'r', lw=2 , rasterized = True )#label='10<M<11')

		fun = interp1d(x_LSAR, nall_11)
		nrm = quad(fun, x_LSAR.min(), x_LSAR.max())[0]
		p.plot(x_LSAR, nall_11 / nrm, 'y', lw=2 , rasterized = True )#label='11<M<12')

	p.xlabel(r'$\log_{10}(\lambda_{SAR})$')
	p.ylabel(r'probability distribution function')
	p.legend(frameon=False, loc=3)
	p.yscale('log')
	p.xlim((30., 35.5))
	p.ylim((1e-4, 4))
	p.title('Specific accretion rate, ' + str(n.round(z_minimum, 2)) + r"<z<" + str(n.round(z_maximum, 2)))
	p.grid()
	p.savefig(os.path.join(LSAR_dir, fig_base+"LSAR_hist_" + z_str + ".png"))
	p.clf()
	print(os.path.join(LSAR_dir, fig_base+"LSAR_hist_" + z_str + ".png"), 'written')

	# DUTY CYCLE
	# print("duty_cycle_AGN")
	N_gal = n.histogram(logm_gal, bins=bins_SMF)[0]

	p.figure(2, (6, 6))
	p.axes([0.16, 0.15, 0.8, 0.8])

	dx = n.log10(0.6777**2)

	if z_mean[0]<=0.35:
		x_41, y_41, y_41_up, y_41_low = n.loadtxt( os.path.join( agn_data_dir, 'duty_cycle_G11_z01_LXhardgt41.ascii'), unpack=True)
		p.fill_between(x_41+dx, y1=10**(y_41_low),  y2=10**(y_41_up), label=r'G11 z=0.1 $L_X>10^{41}$erg s$^{-1}$', alpha=0.5, color='green')

	if z_mean[0]<=0.35:
		x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_S08_z025_LXhardgt42.ascii'), unpack=True)
		p.fill_between(x_42+dx, y1=10**(y_42_low),  y2=10**(y_42_up), label=r'S08 z=0.25 $L_X>10^{42}$erg s$^{-1}$', alpha=0.5, color='brown')

	if z_mean[0]<=0.5:
		x_41, y_41, y_41_up, y_41_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z025_LXhardgt41.ascii'), unpack=True)
		x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z025_LXhardgt42.ascii'), unpack=True)
		x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z025_LXhardgt43.ascii'), unpack=True)
		lab_bib = 'G17 z=0.25'
		#p.fill_between(x_44, y1=10**(y_44_low),  y2=10**(y_44_up), label=lab_bib+r' $L_X>10^{44}$', alpha=0.5, color='magenta')
		p.fill_between(x_43+dx, y1=10**(y_43_low),  y2=10**(y_43_up), label=r' $L_X>10^{43}$erg s$^{-1}$', alpha=0.5,color='blue')
		p.fill_between(x_42+dx, y1=10**(y_42_low),  y2=10**(y_42_up), label=r'$L_X>10^{42}$erg s$^{-1}$', alpha=0.5,color='red')
		p.fill_between(x_41+dx, y1=10**(y_41_low),  y2=10**(y_41_up), label=r'$L_X>10^{41}$erg s$^{-1}$', alpha=0.5,color='black')

	if z_mean[0]>0.5 and z_mean[0]<1. :
		x_41, y_41, y_41_up, y_41_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z075_LXhardgt41.ascii'), unpack=True)
		x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z075_LXhardgt42.ascii'), unpack=True)
		x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z075_LXhardgt43.ascii'), unpack=True)
		x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z075_LXhardgt44.ascii'), unpack=True)
		lab_bib = 'G17 z=0.75'
		p.fill_between(x_44+dx, y1=10**(y_44_low),  y2=10**(y_44_up), label=r' $L_X>10^{44}$erg s$^{-1}$', alpha=0.5, color='magenta')
		p.fill_between(x_43+dx, y1=10**(y_43_low),  y2=10**(y_43_up), label=r'$L_X>10^{43}$erg s$^{-1}$', alpha=0.5,color='blue')
		p.fill_between(x_42+dx, y1=10**(y_42_low),  y2=10**(y_42_up), label=r'$L_X>10^{42}$erg s$^{-1}$', alpha=0.5,color='red')
		p.fill_between(x_41+dx, y1=10**(y_41_low),  y2=10**(y_41_up), label=r'$L_X>10^{41}$erg s$^{-1}$', alpha=0.5,color='black')

	if z_mean[0]>1. and z_mean[0]<1.5 :
		x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z125_LXhardgt42.ascii'), unpack=True)
		x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z125_LXhardgt43.ascii'), unpack=True)
		x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z125_LXhardgt44.ascii'), unpack=True)
		lab_bib = 'G17 z=1.25'
		p.fill_between(x_44+dx, y1=10**(y_44_low),  y2=10**(y_44_up), label=r' $L_X>10^{44}$erg s$^{-1}$', alpha=0.5, color='magenta')
		p.fill_between(x_43+dx, y1=10**(y_43_low),  y2=10**(y_43_up), label=r'$L_X>10^{43}$erg s$^{-1}$', alpha=0.5,color='blue')
		p.fill_between(x_42+dx, y1=10**(y_42_low),  y2=10**(y_42_up), label=r'$L_X>10^{42}$erg s$^{-1}$', alpha=0.5,color='red')
		#p.fill_between(x_41, y1=10**(y_41_low),  y2=10**(y_41_up),label=r'$L_X>10^{41}$', alpha=0.5,color='black')

	if z_mean[0]>1.5 and z_mean[0]<2. :
		x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z175_LXhardgt42.ascii'), unpack=True)
		x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z175_LXhardgt43.ascii'), unpack=True)
		x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z175_LXhardgt44.ascii'), unpack=True)
		lab_bib = 'G17 z=1.75'
		p.fill_between(x_44+dx, y1=10**(y_44_low),  y2=10**(y_44_up), label=r' $L_X>10^{44}$erg s$^{-1}$', alpha=0.5, color='magenta')
		p.fill_between(x_43+dx, y1=10**(y_43_low),  y2=10**(y_43_up), label=r'$L_X>10^{43}$erg s$^{-1}$', alpha=0.5,color='blue')
		p.fill_between(x_42+dx, y1=10**(y_42_low),  y2=10**(y_42_up), label=r'$L_X>10^{42}$erg s$^{-1}$', alpha=0.5,color='red')
		#p.fill_between(x_41, y1=10**(y_41_low),  y2=10**(y_41_up),label=r'$L_X>10^{41}$', alpha=0.5,color='black')

	if z_mean[0]>2. and z_mean[0]<2.5 :
		x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z225_LXhardgt42.ascii'), unpack=True)
		x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z225_LXhardgt43.ascii'), unpack=True)
		x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z225_LXhardgt44.ascii'), unpack=True)
		lab_bib = 'G17 z=2.25'
		p.fill_between(x_44+dx, y1=10**(y_44_low),  y2=10**(y_44_up), label=r' $L_X>10^{44}$erg s$^{-1}$', alpha=0.5, color='magenta')
		p.fill_between(x_43+dx, y1=10**(y_43_low),  y2=10**(y_43_up), label=r'$L_X>10^{43}$erg s$^{-1}$', alpha=0.5,color='blue')
		p.fill_between(x_42+dx, y1=10**(y_42_low),  y2=10**(y_42_up), label=r'$L_X>10^{42}$erg s$^{-1}$', alpha=0.5,color='red')
		#p.fill_between(x_41, y1=10**(y_41_low),  y2=10**(y_41_up),label=r'$L_X>10^{41}$', alpha=0.5,color='black')

	if z_mean[0]>2.5 and z_mean[0]<3. :
		x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z275_LXhardgt42.ascii'), unpack=True)
		x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z275_LXhardgt43.ascii'), unpack=True)
		x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z275_LXhardgt44.ascii'), unpack=True)
		lab_bib = 'G17 z=2.75'
		p.fill_between(x_44+dx, y1=10**(y_44_low),  y2=10**(y_44_up), label=r' $L_X>10^{44}$erg s$^{-1}$', alpha=0.5, color='magenta')
		p.fill_between(x_43+dx, y1=10**(y_43_low),  y2=10**(y_43_up), label=r'$L_X>10^{43}$erg s$^{-1}$', alpha=0.5,color='blue')
		p.fill_between(x_42+dx, y1=10**(y_42_low),  y2=10**(y_42_up), label=r'$L_X>10^{42}$erg s$^{-1}$', alpha=0.5,color='red')
		#p.fill_between(x_41, y1=10**(y_41_low),  y2=10**(y_41_up),label=r'$L_X>10^{41}$', alpha=0.5,color='black')

	if z_mean[0]>3 :
		lab_bib = 'G17 z=3.5'
		x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z350_LXhardgt43.ascii'), unpack=True)
		x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z350_LXhardgt44.ascii'), unpack=True)
		#x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir, 'duty_cycle_H10_z02_LXhardgt42.ascii'), unpack=True)
		p.fill_between(x_44+dx, y1=10**(y_44_low),  y2=10**(y_44_up), label=lab_bib+r' $L_X>10^{44}$erg s$^{-1}$', alpha=0.5, color='magenta')
		p.fill_between(x_43+dx, y1=10**(y_43_low),  y2=10**(y_43_up), label=r'$L_X>10^{43}$erg s$^{-1}$', alpha=0.5,color='blue')
		#p.fill_between(x_42, y1=10**(y_42_low),  y2=10**(y_42_up),label=r'$L_X>10^{42}$', alpha=0.5,color='red')
		#p.fill_between(x_41, y1=10**(y_41_low),  y2=10**(y_41_up),label=r'$L_X>10^{41}$', alpha=0.5,color='black')

	p.axhline(f_duty(z_mean[0]), ls='dashed', lw=2)

	for kk in list(AGN.keys()):
		label = r'$\sigma=$' + kk.split('_')[1] + r',$f_{sat}=$'  + kk.split('_')[3]
		#z = AGN[kk]['redshift_R']
		lx = AGN[kk]['LX_hard']
		#logNH = AGN[kk]['logNH']
		#fx = AGN[kk]['FX_soft']
		#lx_0520 = AGN[kk]['LX_soft']
		logm = AGN[kk]['galaxy_SMHMR_mass']
		#lsar = lx - logm
		#mag_r = AGN[kk]['SDSS_r_AB']

		N_agn_a = n.histogram(logm, bins=bins_SMF)[0]
		y = N_agn_a * 1. / N_gal  # *dc_val
		yerr = y * N_agn_a**(-0.5)  # *dc_val
		p.plot(x_SMF, y, color='grey', rasterized = True)#, label='all AGN')

		tsel = (lx > 41)
		N_agn_41 = n.histogram(logm[tsel], bins=bins_SMF)[0]
		y = N_agn_41 * 1. / N_gal  # *dc_val
		yerr = y * N_agn_41**(-0.5)  # *dc_val
		p.plot(x_SMF, y,  color='black', rasterized = True)# label=r'L$_X>10^{41}$')

		tsel = (lx > 42)
		N_agn_42 = n.histogram(logm[tsel], bins=bins_SMF)[0]
		y = N_agn_42 * 1. / N_gal  # *dc_val
		yerr = y * N_agn_42**(-0.5)  # *dc_val
		p.plot(x_SMF, y,  color='red', rasterized = True)# label=r'L$_X>10^{42}$')

		tsel = (lx > 43)
		N_agn_43 = n.histogram(logm[tsel], bins=bins_SMF)[0]
		y = N_agn_43 * 1. / N_gal  # *dc_val
		yerr = y * N_agn_43**(-0.5)  # *dc_val
		p.plot(x_SMF, y,  color='blue', rasterized = True)# label=r'L$_X>10^{43}$')

		tsel = (lx > 44)
		N_agn_44 = n.histogram(logm[tsel], bins=bins_SMF)[0]
		y = N_agn_44 * 1. / N_gal  # *dc_val
		yerr = y * N_agn_44**(-0.5)  # *dc_val
		p.plot(x_SMF, y,  color='magenta', rasterized = True)# label=r'L$_X>10^{44}$')

	p.xlabel(r'$\log_{10}(M^*/M_\odot)$')
	p.ylabel(r'$f_{AGN}(M^*, ' + str(n.round(z_minimum, 2)) + r"<z<" + str(n.round(z_maximum, 2)) + r')$')
	p.yscale('log')
	p.ylim((1e-6, 0.4))
	p.xlim((8.0, 12.))
	p.grid()
	# , '+str(n.round(z_minimum,2))+"<z<"+str(n.round(z_maximum,2)))
	p.title('Duty cycle')
	#p.legend( loc='upper center', bbox_to_anchor=(0.5, 1.05), ncol=2, fancybox=True, title=str(n.round(z_minimum,2))+"<z<"+str(n.round(z_maximum,2)) )
	p.legend(frameon=False, loc=0, ncol=3, fontsize=12)
	#p.legend( loc=8, ncol=2, fancybox=True, title=str(n.round(z_minimum,2))+"<z<"+str(n.round(z_maximum,2)) )
	p.savefig(os.path.join(DC_dir, fig_base+"duty_cycle_AGN_" + z_str + ".png"))

	p.yscale('linear')
	p.ylim((0, 0.3))
	p.savefig(os.path.join(DC_dir, fig_base+"linear_duty_cycle_AGN_" + z_str + ".png"))
	p.clf()
	print(os.path.join(DC_dir, fig_base+"linear_duty_cycle_AGN_" + z_str + ".png"), 'written')



	p.figure(1, (6, 6))
	p.axes([0.17, 0.15, 0.73, 0.73])
	#for fun, name in zip(smf_ilbert_fun, smf_ilbert_name):
		#p.plot(mbins, fun(10**mbins)/(0.7**3), label=name, ls='dashed', lw=0.5)
	if mean_z>0.15 and mean_z<0.8:
		d1 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'agn', 'AGN_HGSMF_BO16_03_z_08_LX_430.ascii'), unpack = True , delimiter = ',')
		p.plot(d1[0][d1[0]<12], 10**d1[1][d1[0]<12], ls='dashed', color='darkgreen', label='Bo16 43, 43.5, 44, 44.5')
		d1 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'agn', 'AGN_HGSMF_BO16_03_z_08_LX_435.ascii'), unpack = True , delimiter = ',')
		p.plot(d1[0][d1[0]<12], 10**d1[1][d1[0]<12], ls='dashed', color='darkgreen')
		d1 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'agn', 'AGN_HGSMF_BO16_03_z_08_LX_440.ascii'), unpack = True , delimiter = ',')
		p.plot(d1[0][d1[0]<12], 10**d1[1][d1[0]<12], ls='dashed', color='darkgreen', lw=3)
		d1 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'agn', 'AGN_HGSMF_BO16_03_z_08_LX_445.ascii'), unpack = True , delimiter = ',')
		p.plot(d1[0][d1[0]<12], 10**d1[1][d1[0]<12], ls='dashed', color='darkgreen', lw=3)

	if mean_z>0.8 and mean_z<1.5:
		d1 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'agn', 'AGN_HGSMF_BO16_08_z_15_LX_430.ascii'), unpack = True , delimiter = ',')
		p.plot(d1[0][d1[0]<12], 10**d1[1][d1[0]<12], ls='dashed', color='darkgreen', label='Bo16 43, 43.5, 44, 44.5')
		d1 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'agn', 'AGN_HGSMF_BO16_08_z_15_LX_435.ascii'), unpack = True , delimiter = ',')
		p.plot(d1[0][d1[0]<12], 10**d1[1][d1[0]<12], ls='dashed', color='darkgreen')
		d1 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'agn', 'AGN_HGSMF_BO16_08_z_15_LX_440.ascii'), unpack = True , delimiter = ',')
		p.plot(d1[0][d1[0]<12], 10**d1[1][d1[0]<12], ls='dashed', color='darkgreen', lw=3)
		d1 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'agn', 'AGN_HGSMF_BO16_08_z_15_LX_445.ascii'), unpack = True , delimiter = ',')
		p.plot(d1[0][d1[0]<12], 10**d1[1][d1[0]<12], ls='dashed', color='darkgreen', lw=3)

	if mean_z>1.5 and mean_z<2.5:
		d1 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'agn', 'AGN_HGSMF_BO16_15_z_25_LX_430.ascii'), unpack = True , delimiter = ',')
		p.plot(d1[0][d1[0]<12], 10**d1[1][d1[0]<12], ls='dashed', color='darkgreen', label='Bo16 43, 43.5, 44, 44.5')
		d1 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'agn', 'AGN_HGSMF_BO16_15_z_25_LX_435.ascii'), unpack = True , delimiter = ',')
		p.plot(d1[0][d1[0]<12], 10**d1[1][d1[0]<12], ls='dashed', color='darkgreen')
		d1 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'agn', 'AGN_HGSMF_BO16_15_z_25_LX_440.ascii'), unpack = True , delimiter = ',')
		p.plot(d1[0][d1[0]<12], 10**d1[1][d1[0]<12], ls='dashed', color='darkgreen', lw=3)
		d1 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'agn', 'AGN_HGSMF_BO16_15_z_25_LX_445.ascii'), unpack = True , delimiter = ',')
		p.plot(d1[0][d1[0]<12], 10**d1[1][d1[0]<12], ls='dashed', color='darkgreen', lw=3)

	if mean_z>0.05:
		Mgal_COS = n.histogram(t_cosmos['mass_med'], bins=bins_SMF)[0] / volume_cosmos / dlogM
		Mgal_COSN = n.histogram(t_cosmos['mass_med'], bins=bins_SMF)[0]
		p.fill_between(x_SMF,
						y1=(Mgal_COS) * (1 - (Mgal_COSN)**(-0.5)),
						y2=(Mgal_COS) * (1 + (Mgal_COSN)**(-0.5)),
						color='g',
						alpha=0.7,
						label='COSMOS',
						lw=2)
	if mean_z<0.5:
		Mgal_GAMA = n.histogram(t_gama['ms'], bins=bins_SMF)[0] / volume_GAMA / dlogM
		Mgal_GAMAN = n.histogram(t_gama['ms'], bins=bins_SMF)[0]
		p.fill_between(x_SMF,
						y1=(Mgal_GAMA) * (1 - (Mgal_GAMAN)**(-0.5)),
						y2=(Mgal_GAMA) * (1 + (Mgal_GAMAN)**(-0.5)),
						color='orange',
						alpha=0.7,
						label='GAMA',
						lw=2)

	p.plot(x_SMF, N_gal / (vol * dlogM), color='green', label='Uchuu galaxies')
	for kk in list(AGN.keys()):
		label = r'$\sigma=$' + kk.split('_')[1] + r',$f_{sat}=$'  + kk.split('_')[3]
		#z = AGN[kk]['redshift_R']
		lx = AGN[kk]['LX_hard']
		#logNH = AGN[kk]['logNH']
		#fx = AGN[kk]['FX_soft']
		#lx_0520 = AGN[kk]['LX_soft']
		logm = AGN[kk]['galaxy_SMHMR_mass']
		#lsar = lx - logm
		#mag_r = AGN[kk]['SDSS_r_AB']

		N_agn_410 = n.histogram(logm[(lx > 41.0)], bins=bins_SMF)[0]
		N_agn_420 = n.histogram(logm[(lx > 42.0)], bins=bins_SMF)[0]
		N_agn_430 = n.histogram(logm[(lx > 43.0)], bins=bins_SMF)[0]
		N_agn_435 = n.histogram(logm[(lx > 43.5)], bins=bins_SMF)[0]
		N_agn_440 = n.histogram(logm[(lx > 44.0)], bins=bins_SMF)[0]
		N_agn_445 = n.histogram(logm[(lx > 44.5)], bins=bins_SMF)[0]

		p.plot(x_SMF, N_agn_a / (vol * dlogM), color='grey', rasterized = True)#, label='Uchuu AGN ALL, 41, 42, 43, 43.5, 44, 44.5')
		p.plot(x_SMF, N_agn_410 / (vol * dlogM), color='k', ls='dashed', rasterized = True)
		p.plot(x_SMF, N_agn_420 / (vol * dlogM), color='k', ls='dashed', rasterized = True)
		p.plot(x_SMF, N_agn_430 / (vol * dlogM), color='grey', ls='dashed', rasterized = True)
		p.plot(x_SMF, N_agn_435 / (vol * dlogM), color='grey', ls='dashed', rasterized = True)
		p.plot(x_SMF, N_agn_440 / (vol * dlogM), color='grey', ls='dashed', lw=3, rasterized = True)
		p.plot(x_SMF, N_agn_445 / (vol * dlogM), color='grey', ls='dashed', lw=3, rasterized = True)

	p.xlabel(r'$\log_{10}(M^*/[M_\odot])$')
	p.ylabel(r'$\Phi$=dN/dlogL/dV [1/Mpc$^3$/dex]')
	p.legend(frameon=False, loc=0, fontsize=12)
	p.yscale('log')
	p.xlim((8, 12.5))
	p.ylim((1e-8, 1e-1))
	p.title('Stellar mass function, ' + str(n.round(z_minimum, 2)) + "<z<" + str(n.round(z_maximum, 2)))
	p.grid()
	p.savefig(os.path.join(DC_dir, fig_base+"SMF_AGN_" + z_str + ".png"))
	p.clf()
	print(os.path.join(DC_dir, fig_base+"SMF_AGN_" + z_str + ".png"), 'written')
