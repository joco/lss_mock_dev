"""
What it does
------------

Add realistic magnitudes to BG, LRG and FIL (ELG deprecated)

Command to run
--------------

python3 005_2_all_magnitudes.py environmentVAR 
  
"""

#import matplotlib
#matplotlib.use('Agg')
#matplotlib.rcParams.update({'font.size': 14})
#import matplotlib.pyplot as p
from sklearn.neighbors import BallTree

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import extinction

from astropy_healpix import healpy
import sys
import os
import time
from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column
from scipy.optimize import curve_fit
import astropy.io.fits as fits
import h5py
import numpy as n
print('Populates light cone with galaxy SED')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

env = 'UNIT_fA1i_DIR' # sys.argv[1]  #
#HEALPIX_id = int(sys.argv[2])
#print(env, HEALPIX_id)
i0 = int( sys.argv[2] )
i1 = int( sys.argv[3] )

if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

nl = lambda sel : len(sel.nonzero()[0])
zs = n.arange(0.0000001, 7.1, 0.001)
dm_itp = interp1d(zs, cosmo.distmod(zs).value)

root_dir = os.path.join(os.environ[env])

max_distance = 0.1 / 3600. * n.pi / 180.
deg_to_rad = n.pi/180.

# Match to BCG and GAL catalogues 
#print('load and match to BCG and GAL catalogues')
#path_2_out_GAL = os.path.join(root_dir, env + '_eRO_CLU_b8_CM_0_pixS_20.0_4MOST_GAL.fits')
#path_2_out_BCG = os.path.join(root_dir, env + '_eRO_CLU_b8_CM_0_pixS_20.0_4MOST_BCG.fits')
#t_GAL = Table.read(path_2_out_GAL)
#t_BCG = Table.read(path_2_out_BCG)
#coord_GAL = deg_to_rad * n.transpose([t_GAL['DEC'], t_GAL['RA'] ])
#coord_BCG = deg_to_rad * n.transpose([t_BCG['DEC'], t_BCG['RA'] ])

#Tree_GAL = BallTree(coord_GAL, metric='haversine')
#Tree_BCG = BallTree(coord_BCG, metric='haversine')

# Match to AGN catalogues 
#print('load and match to AGN catalogues')
#agn = Table.read('/data42s/comparat/firefly/mocks/2021-04/QMOST/S6_4MOST_ALL_SNR3_IR215.fit.gz')
#qso = Table.read('/data42s/comparat/firefly/mocks/2021-04/QMOST/QSO_4MOST.fits.gz')

#coord_AGN = deg_to_rad * n.transpose([agn['DEC'], agn['RA'] ])
#coord_QSO = deg_to_rad * n.transpose([qso['DEC'], qso['RA'] ])

#Tree_AGN = BallTree(coord_AGN, metric='haversine')
#Tree_QSO = BallTree(coord_QSO, metric='haversine')


#path_2_kids = os.path.join(os.environ['MD10'], '4most_s8_kidsdr4.lsdr9.fits')
#t_kids = Table.read(path_2_kids)
#dup_type = (t_kids['lsdr9_TYPE']=="DUP" )
#dash_type = (t_kids['lsdr9_TYPE']=="-" ) | (t_kids['lsdr9_TYPE']=='-  ')
#good_phot = ( t_kids['rtot']>0 ) & (t_kids['gr'] > -10 ) & (t_kids['ri'] > -10 )  & (t_kids['iz'] > -10 )  & (t_kids['zy'] > -10 )  & (t_kids['yj'] > -10 )  & (t_kids['jh'] > -10 )  & (t_kids['hks'] > -10 ) & (t_kids['zphot']>0)
#keep_kids = (dup_type==False) & (dash_type==False) & (good_phot)
#t_kids = t_kids[keep_kids]

#path_2_COSMOS = os.path.join(os.environ['HOME'],'data1/COSMOS/catalogs/photoz-2.0/photoz_vers2.0_010312_LSDR9only.fits')
path_2_COSMOS = os.path.join(os.environ['UNIT_fA1i_DIR'], 'photoz_vers2.0_010312_LSDR9only.fits')
t = Table.read(path_2_COSMOS)
#t = fits.open(path_2_COSMOS)
good = (t['Separation']<0.4) & (t['photoz']>0. )&( t['photoz']< 2. ) & ( t['Z']>0 )&( t['R']>0 )& ( t['I']>0 )& ( t['J']>0 )& ( t['K']>0 )& ( t['MK']<0 )&( t['MK']>-40 )&( t['mass_med']<14 )&( t['mass_med']>8 )&( t['MK']<-20 )
t_kids = t[good]

#path_2_kids = os.path.join(os.environ['UNIT_fA1i_DIR'], 'G09.GAMADR4+LegacyDR9.galreference+RM.fits')
#t_kids = Table.read(path_2_kids)
#kmag = 8.9 - 2.5*n.log10(t_kids['flux_Kt'])
#zmag = 8.9 - 2.5*n.log10(t_kids['flux_Zt'])
#rmag = 8.9 - 2.5*n.log10(t_kids['flux_rt'])
#imag = 8.9 - 2.5*n.log10(t_kids['flux_it'])
#keep_kids = (kmag>0) & (t_kids['z_peak']>0.01) & (t_kids['z_peak']<1.99) & (zmag>0) & (rmag>0)  & (imag>0)
#t_kids = t_kids[keep_kids]
#kmag = 8.9 - 2.5*n.log10(t_kids['flux_Kt'])
#zmag = 8.9 - 2.5*n.log10(t_kids['flux_Zt'])
#rmag = 8.9 - 2.5*n.log10(t_kids['flux_rt'])
#imag = 8.9 - 2.5*n.log10(t_kids['flux_it'])

#KCORR_DATA = n.loadtxt( os.path.join( os.environ['GIT_AGN_MOCK'], 'data/K-corr/VISTA_Ks_kcorrections.txt'), unpack = True)
## redshift distmod bandpass kcorr_median kcorr_16pc kcorr_84pc
##bandpass_itp = interp1d(KCORR_DATA[0], KCORR_DATA[2])
#kcorr_itp = interp1d(KCORR_DATA[0], KCORR_DATA[3])
##kmag = absmag + distmod + bandpass + kcorrection
## absmag = appmag - distmod - kcorr  
## is properly zero-centred; or at least zero-ish; the median +/- NMAD is 0.05 +/- 0.12.

#dm_values = dm_itp(t_kids['z_peak'])
##kmag_abs = kmag - ( dm_values + bandpass_itp(t_kids['z_peak']) + kcorr_itp(t_kids['z_peak']) )
#kmag_abs = kmag - ( dm_values + kcorr_itp(t_kids['z_peak']) )

##kmag_abs = 8.7 - 2.5*n.log10(t_kids['flux_Kt']) - dm_values
#t_kids['Kmag_abs'] = kmag_abs
#t_kids['dist_mod'] = dm_values

# rescale variables
min_Z = 1.5 # n.min(t_kids['z_peak'])
max_Z = 0.0 # n.max(t_kids['z_peak'])
min_K = -26. #n.min(t_kids['Kmag_abs'])
max_K = -20. #n.max(t_kids['Kmag_abs'])

z_01 = (t_kids['photoz'] - min_Z ) / ( max_Z - min_Z )
k_01 = (t_kids['MK'] - min_K ) / ( max_K - min_K )
Tree_kids = BallTree(n.transpose([z_01, k_01]))

i_01 = (t_kids['MI'] - min_K ) / ( max_K - min_K )
Tree_kids_I = BallTree(n.transpose([z_01, i_01]))
print(len(t_kids),'lines in KIDS catalogue')

sub_survey_names = n.array([ 'BG', 'LRG', 'ELG', 'QSO', 'LyA', 'filament_GAL'])

N_subsurvey = {'BG':1, 'filament_GAL':3, 'LRG':2, 'ELG':3, 'QSO':4, 'LyA':5}
priority_values = {'BG':100, 'filament_GAL':80, 'LRG':99, 'ELG':80, 'QSO':97, 'LyA':98}

# reassigns templates correctly
z_all = n.hstack(( 0., n.arange(0.3, 3., 0.2), 3.5, 4.5, 6. ))
zmins = z_all[:-1]
zmaxs = z_all[1:]

dir_2_OUT = os.path.join(root_dir, "cat_SHAM_ALL")
dir_2_IN = os.path.join(root_dir, "cat_GALAXY_all")

z0, z1, slope, OO, zpt, scatter = n.loadtxt(os.path.join( os.environ['GIT_AGN_MOCK'], 'data', 'logMs-MK-fits', 'look-up-table-2parameters.txt'), unpack=True)
fun = lambda x, a, b : a * x + b

def add_kmag(t_gal):
	#HEALPIX_id=359
	# catalogs
	for zmin, zmax, slope_i, OO_i, zpt_i, scatter_i in zip(z0, z1, slope, OO, zpt, scatter):
		#print(zmin, zmax, p_b)
		s_gal  = (t_gal['redshift_R']>=zmin)  & (t_gal['redshift_R']<=zmax) 
		mag = lambda x : fun( x, slope_i, OO_i)
		if len(t_gal ['K_mag_abs'][s_gal])>0:
			t_gal ['K_mag_abs'][s_gal]  = mag(t_gal ['galaxy_SMHMR_mass'][s_gal])
			t_gal ['K_mag_abs'][s_gal]+=norm.rvs(loc=0, scale=scatter_i, size=len(t_gal['redshift_R'][s_gal]))
	return t_gal 

def add_imag(t_gal):
	#HEALPIX_id=359
	# catalogs
	z0, z1, slope, OO, zpt, scatter = n.loadtxt(os.path.join( os.environ['GIT_AGN_MOCK'], 'data', 'logMs-MI-fits', 'look-up-table-2parameters.txt'), unpack=True)
	fun = lambda x, a, b : a * x + b
	for zmin, zmax, slope_i, OO_i, zpt_i, scatter_i in zip(z0, z1, slope, OO, zpt, scatter):
		#print(zmin, zmax, p_b)
		scatter=0.15
		s_gal  = (t_gal['redshift_R']>=zmin)  & (t_gal['redshift_R']<=zmax)
		mag = lambda x : fun( x, slope_i, OO_i)
		if len(t_gal ['I_mag_abs'][s_gal])>0:
			t_gal ['I_mag_abs'][s_gal]  = mag(t_gal ['galaxy_SMHMR_mass'][s_gal])
			t_gal ['I_mag_abs'][s_gal]+=norm.rvs(loc=0, scale=scatter_i, size=len(t_gal['redshift_R'][s_gal]))
	return t_gal

def add_magnitudes_direct_match_K(t_survey, t_kids2=t_kids, Tree_kids=Tree_kids):
	# simulated quantities
	sim_redshift = t_survey['redshift_R']
	sim_k_mag    = t_survey['K_mag_abs'] # +dm_itp(t_survey['redshift_R'])
	# rescale variables
	SIMz_01      = ( sim_redshift - min_Z ) / ( max_Z - min_Z )
	SIMk_01      = ( sim_k_mag - min_K ) / ( max_K - min_K )
	DATA = n.transpose([SIMz_01, SIMk_01])
	# search nearest neighbour in tree
	dist_out, ids_out = Tree_kids.query(DATA, k=1, return_distance = True)
	kids_ID = n.arange(len(t_kids2))
	ids = n.hstack((ids_out))
	dist_map = n.hstack(( dist_out ))
	id_to_map = kids_ID[ids]
	columns_to_add = n.array(['Id', 'photoz', 'MK', 'MI',
								'U',
								'B',
								'V',
								'G',
								'R',
								'I',
								'Z',
								'J',
								'K',
								'IC',
								'IA',
								'I1',
								'I2',
								'I3',
								'I4',
								 'mass_med', 'sfr_med'
								, 'FLUX_G'
								, 'FLUX_R'
								, 'FLUX_Z'
								, 'FLUX_W1'
								, 'FLUX_W2'
								, 'FLUX_IVAR_G'
								, 'FLUX_IVAR_R'
								, 'FLUX_IVAR_Z'
								, 'FLUX_IVAR_W1'
								, 'FLUX_IVAR_W2'
								, 'MW_TRANSMISSION_G'
								, 'MW_TRANSMISSION_R'
								, 'MW_TRANSMISSION_Z'
								, 'MW_TRANSMISSION_W1'
								, 'MW_TRANSMISSION_W2'
								, 'FIBERFLUX_R'
								, 'SHAPE_R'
								, 'SHAPE_E1'
								, 'SHAPE_E2'
								, 'SERSIC'
								, 'MASKBITS'
								])
	print('outputing results')
	for el in columns_to_add :
		t_survey[el] = t_kids2[el][id_to_map]
	t_survey['distance_match'] = dist_map
	return t_survey #.write(path_2_out)

def add_magnitudes_direct_match_I(t_survey, t_kids2=t_kids, Tree_kids=Tree_kids_I):
	# simulated quantities
	sim_redshift = t_survey['redshift_R']
	sim_k_mag    = t_survey['K_mag_abs'] # +dm_itp(t_survey['redshift_R'])
	# rescale variables
	SIMz_01      = ( sim_redshift - min_Z ) / ( max_Z - min_Z )
	SIMk_01      = ( sim_k_mag - min_K ) / ( max_K - min_K )
	DATA = n.transpose([SIMz_01, SIMk_01])
	# search nearest neighbour in tree
	dist_out, ids_out = Tree_kids.query(DATA, k=1, return_distance = True)
	kids_ID = n.arange(len(t_kids2))
	ids = n.hstack((ids_out))
	dist_map = n.hstack(( dist_out ))
	id_to_map = kids_ID[ids]
	columns_to_add = n.array(['Id', 'photoz', 'MK', 'MI',
								'U',
								#'B',
								#'V',
								'G',
								'R',
								'I',
								'Z',
								'J',
								'K',
								#'IC',
								#'IA',
								'I1',
								'I2',
								#'I3',
								#'I4',
								 'mass_med', 'sfr_med'
								, 'FLUX_G'
								, 'FLUX_R'
								, 'FLUX_Z'
								, 'FLUX_W1'
								, 'FLUX_W2'
								, 'FLUX_IVAR_G'
								, 'FLUX_IVAR_R'
								, 'FLUX_IVAR_Z'
								, 'FLUX_IVAR_W1'
								, 'FLUX_IVAR_W2'
								, 'MW_TRANSMISSION_G'
								, 'MW_TRANSMISSION_R'
								, 'MW_TRANSMISSION_Z'
								, 'MW_TRANSMISSION_W1'
								, 'MW_TRANSMISSION_W2'
								, 'FIBERFLUX_R'
								, 'SHAPE_R'
								, 'SHAPE_E1'
								, 'SHAPE_E2'
								, 'SERSIC'
								, 'MASKBITS'
								])
	print('outputing results')
	for el in columns_to_add :
		t_survey[el] = t_kids2[el][id_to_map]
	t_survey['distance_match'] = dist_map
	return t_survey #.write(path_2_out)

t0 = time.time()
N_pixels = healpy.nside2npix(8)
for HEALPIX_id in n.arange(N_pixels)[i0:i1]: #n.arange(N_pixels)[::-1]:
    print(HEALPIX_id, time.time()-t0)
    path_2_out_K = os.path.join(dir_2_OUT, 'Kmatch_'  + str(HEALPIX_id).zfill(6) + '_C.fit')
    path_2_out_I = os.path.join(dir_2_OUT, 'Imatch_'  + str(HEALPIX_id).zfill(6) + '_C.fit')
    if os.path.isfile(path_2_out_K)==False:
        path_2_in = os.path.join(dir_2_IN, str(HEALPIX_id).zfill(6) + '.fit')
        print(path_2_in)
        t_survey = Table.read(path_2_in)
        initial_ids = n.arange(len(t_survey))
        t_survey['line_id_cat_GALAXY_all'] = initial_ids
        t_in = t_survey[(t_survey['redshift_R']<1.5) & (t_survey['redshift_R']>0.5) & (t_survey['galaxy_SMHMR_mass'] > 5)]
        print('adding k mag abs')
        t_in = add_kmag(t_in)
        #print('adding i mag abs')
        #t_in['I_mag_abs'] = n.zeros(len(t_in['K_mag_abs']))
        #t_in = add_imag(t_in)
        print(len(t_in))
        t_out = add_magnitudes_direct_match_K(t_survey = t_in, t_kids2 = t_kids, Tree_kids=Tree_kids)
        t_out.write(path_2_out_K, overwrite = True)
        #t_out = add_magnitudes_direct_match_I(t_survey = t_in, t_kids2 = t_kids, Tree_kids=Tree_kids_I)
        #t_out.write(path_2_out_I, overwrite = True)
        print(path_2_out_K, path_2_out_I, 'written')


