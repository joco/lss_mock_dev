import numpy as n
import os
import sys
from astropy.coordinates import SkyCoord
import time
t0 = time.time()

# 10e6 full sky randoms :
size = 10000000
uu = n.random.uniform(size=size)
dec = n.arccos(1 - 2 * uu) * 180 / n.pi - 90.
ra = n.random.uniform(size=size) * 2 * n.pi * 180 / n.pi

coords = SkyCoord(ra, dec, unit='deg', frame='icrs')
bb_gal = coords.galactic.b.value
ll_gal = coords.galactic.l.value
print('bb_gal', bb_gal[:10], time.time() - t0, 's')
print('ll_gal', ll_gal[:10], time.time() - t0, 's')

bb_ecl = coords.barycentrictrueecliptic.lat.value
ll_ecl = coords.barycentrictrueecliptic.lon.value
print('bb_ecl', bb_ecl[:10], time.time() - t0, 's')
print('ll_ecl', ll_ecl[:10], time.time() - t0, 's')

nl = lambda selection : len(selection.nonzero()[0])
area = lambda selection : nl(selection)*129600. / ( size * n.pi)

g_lat_10 = (abs(bb_gal)>10)
g_lat_15 = (abs(bb_gal)>15)
g_lat_20 = (abs(bb_gal)>20)
erosita_DE = (ll_gal>180) 
dec5  = dec < 5
dec34 = dec < 34
dec32 = dec < 32
decm80p5 = ( dec < 5 ) & ( dec > -80 )
decm80p10 = ( dec < 10 ) & ( dec > -80 )
decm80p20 = ( dec < 20 ) & ( dec > -80 )
decm70p0 = ( dec < 0 ) & ( dec > -70 )

print("\\item", r'$g_{lon}>180^\circ$', "==", n.round( area(erosita_DE), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>10^\circ$' , "==", n.round( area(g_lat_10)  , 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>15^\circ$' , "==", n.round( area(g_lat_15)  , 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>20^\circ$' , "==", n.round( area(g_lat_20)  , 1), r'deg$^2$')

print("\\item", r'$|g_{lat}|>10^\circ \& g_{lon}>180^\circ$', "==", n.round( area((g_lat_10)&(erosita_DE)), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>15^\circ \& g_{lon}>180^\circ$', "==", n.round( area((g_lat_15)&(erosita_DE)), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>20^\circ \& g_{lon}>180^\circ$', "==", n.round( area((g_lat_20)&(erosita_DE)), 1), r'deg$^2$')

print("\\item", r'$|g_{lat}|>10^\circ \& g_{lon}>180^\circ \& Dec<5$' , "==", n.round( area((g_lat_10)&(erosita_DE)&(dec5 )), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>10^\circ \& g_{lon}>180^\circ \& Dec<34$', "==", n.round( area((g_lat_10)&(erosita_DE)&(dec34)), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>10^\circ \& g_{lon}>180^\circ \& Dec<32$', "==", n.round( area((g_lat_10)&(erosita_DE)&(dec32)), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>10^\circ \& g_{lon}>180^\circ \& Dec>5$' , "==", n.round( area((g_lat_10)&(erosita_DE)&(~dec5 )), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>10^\circ \& g_{lon}>180^\circ \& Dec>34$', "==", n.round( area((g_lat_10)&(erosita_DE)&(~dec34)), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>10^\circ \& g_{lon}>180^\circ \& Dec>32$', "==", n.round( area((g_lat_10)&(erosita_DE)&(~dec32)), 1), r'deg$^2$')

print("\\item", r'$|g_{lat}|>10^\circ \& g_{lon}>180^\circ \& -80<Dec<5$'  , "==", n.round( area((g_lat_10)&(erosita_DE)&(decm80p5)), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>10^\circ \& g_{lon}>180^\circ \& -80<Dec<10$' , "==", n.round( area((g_lat_10)&(erosita_DE)&(decm80p10)), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>10^\circ \& g_{lon}>180^\circ \& -80<Dec<20$' , "==", n.round( area((g_lat_10)&(erosita_DE)&(decm80p20)), 1), r'deg$^2$')

print("\\item", r'$|g_{lat}|>15^\circ \& g_{lon}>180^\circ \& -80<Dec<5$'  , "==", n.round( area((g_lat_15)&(erosita_DE)&(decm80p5)), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>15^\circ \& g_{lon}>180^\circ \& -80<Dec<10$' , "==", n.round( area((g_lat_15)&(erosita_DE)&(decm80p10)), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>15^\circ \& g_{lon}>180^\circ \& -80<Dec<20$' , "==", n.round( area((g_lat_15)&(erosita_DE)&(decm80p20)), 1), r'deg$^2$')

print("\\item", r'$|g_{lat}|>20^\circ \& g_{lon}>180^\circ \& Dec<5$' , "==", n.round( area((g_lat_20)&(erosita_DE)&(dec5 )), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>20^\circ \& g_{lon}>180^\circ \& Dec<34$', "==", n.round( area((g_lat_20)&(erosita_DE)&(dec34)), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>20^\circ \& g_{lon}>180^\circ \& Dec<32$', "==", n.round( area((g_lat_20)&(erosita_DE)&(dec32)), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>20^\circ \& g_{lon}>180^\circ \& Dec>5$' , "==", n.round( area((g_lat_20)&(erosita_DE)&(~dec5 )), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>20^\circ \& g_{lon}>180^\circ \& Dec>34$', "==", n.round( area((g_lat_20)&(erosita_DE)&(~dec34)), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>20^\circ \& g_{lon}>180^\circ \& Dec>32$', "==", n.round( area((g_lat_20)&(erosita_DE)&(~dec32)), 1), r'deg$^2$')


print("\\item", r'$|g_{lat}|>10^\circ \& g_{lon}>180^\circ \& -70<Dec<0$'  , "==", n.round( area((g_lat_10)&(erosita_DE)&(decm70p0)), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>15^\circ \& g_{lon}>180^\circ \& -70<Dec<0$'  , "==", n.round( area((g_lat_15)&(erosita_DE)&(decm70p0)), 1), r'deg$^2$')
print("\\item", r'$|g_{lat}|>20^\circ \& g_{lon}>180^\circ \& -70<Dec<0$'  , "==", n.round( area((g_lat_20)&(erosita_DE)&(decm70p0)), 1), r'deg$^2$')


#hh = fits.open('/home/comparat/data/MCXC/MCXC.fits')
#ra = hh[1].data['RAdeg']
#dec = hh[1].data['DEdeg']

#coords = SkyCoord(ra, dec, unit='deg', frame='icrs')

#bb_gal = coords.galactic.b.value
#ll_gal = coords.galactic.l.value

#bb_ecl = coords.barycentrictrueecliptic.lat.value
#ll_ecl = coords.barycentrictrueecliptic.lon.value

#g_lat_10 = (abs(bb_gal)>10)
#g_lat_15 = (abs(bb_gal)>15)
#g_lat_20 = (abs(bb_gal)>20)
#erosita_DE = (ll_gal>180) 
#dec5  = dec < 5
#dec34 = dec < 34
#dec32 = dec < 32

#in_reg = (dec>32) & (erosita_DE) & (g_lat_10)

#print(hh[1].data[in_reg]['MCXC'])
#print(hh[1].data[in_reg]['OName'])
#print(hh[1].data[in_reg]['AName'])
#for el in hh[1].data[in_reg]['AName']:
	#print(el)

