import time
t0 = time.time()
import os, sys, glob
import numpy as n
from astropy.table import Table, Column, vstack, hstack
from astropy.io import fits


infiles = n.array( glob.glob( '/home/comparat/sf_Shared/data/4most/templates/binned/4most_*.fits' ) )
out_dir = '/home/comparat/sf_Shared/data/4most/templates/fmt25'

for el in infiles:
    t = fits.open(el)[1].data
    tH = fits.open(el)
    out_name2 = os.path.join(out_dir, 'R'+os.path.basename(el)[1:])
    t_template = []
    t_template.append(fits.Column(name='LAMBDA', array= t['LAMBDA'], unit='Angstrom', format='1D' ) )
    t_template.append(fits.Column(name='FLUX_DENSITY', array=t['FLUX_DENSITY'], unit='erg/(Angstrom cm2 s)', format='1D'))
    hdu = fits.BinTableHDU.from_columns(t_template)
    hdu.header['MAG'] = tH[1].header['MAG']
    outf = fits.HDUList([fits.PrimaryHDU(), hdu])
    outf.writeto(out_name2, overwrite=True)
    print( out_name2, 'written', time.time()-t0)



