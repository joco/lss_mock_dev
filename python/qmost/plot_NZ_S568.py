import os, sys, glob
import numpy as np
import healpy
import matplotlib.pyplot as plt
import astropy.io.fits as fits
from matplotlib.path import Path 

working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'qmost')
all_NZ = np.array( glob.glob( os.path.join(os.environ['HOME'], 'data', '4most', 'S?', '*NZ*.fits') ) )
all_NZ.sort()

names = np.array([ el.split('/')[-2] + '_' + el.split('/')[-1][6:-5] for el in all_NZ ])

data = []
for p_2_NZ in all_NZ:
	data.append( fits.open(p_2_NZ)[1].data ) 
data = np.array(data)

plot_file = os.path.join(working_dir, 'S568_NZ.png')
DZ=0.1

plt.figure(1, (10,6))
for dd, label in zip(data, names):
	xx=dd["z_middle"]
	NN=dd["counts"] 
	if label[:2]=='S5':
		plt.step(xx, NN, label=label, lw=4, ls='solid')
	elif label[:2]=='S6':
		plt.step(xx, NN, label=label, lw=3, ls='solid')
	else:
		plt.step(xx, NN, label=label, lw=2, ls='dotted')

plt.grid()
plt.ylim((90,1e6 ))
plt.xlim((0.001, 4.5 ))
plt.xlabel('redshift')
plt.ylabel('Counts (dz=0.1)')
plt.yscale('log')
plt.legend(loc=0)
plt.savefig(plot_file)
plt.clf()

plot_file = os.path.join(working_dir, 'S568_NZ-xlog.png')
DZ=0.1

plt.figure(1, (10,6))
for dd, label in zip(data, names):
	xx=dd["z_middle"]
	NN=dd["counts"] 
	if label[:2]=='S5':
		plt.step(xx, NN, label=label, lw=4, ls='solid')
	elif label[:2]=='S6':
		plt.step(xx, NN, label=label, lw=3, ls='solid')
	else:
		plt.step(xx, NN, label=label, lw=2, ls='dotted')

plt.grid()
plt.ylim((90,1e6 ))
plt.xlim((0.05, 4.5 ))
plt.xlabel('redshift')
plt.ylabel('Counts (dz=0.1)')
plt.yscale('log')
plt.xscale('log')
plt.legend(loc=0)
plt.savefig(plot_file)
plt.clf()
