"""
Merges into a single fits catalog containing the 4FS input columns.

cp ~/data/4most/templates/4most_LRG_zmin_09_zmax_11_EBV_0_01.fits .
cp ~/data/4most/templates/4most_LRG_zmin_07_zmax_09_EBV_0_01.fits .
cp ~/data/4most/templates/4most_LRG_zmin_05_zmax_07_EBV_0_01.fits .
cp ~/data/4most/templates/4most_LRG_zmin_03_zmax_05_EBV_0_01.fits .

cp 4most_LRG_zmin_09_zmax_11_EBV_0_01.fits 4most_LRG_zmin_09_zmax_11_EBV_0_01.fitsDECam_r_AB.fits
cp 4most_LRG_zmin_09_zmax_11_EBV_0_01.fits 4most_LRG_zmin_09_zmax_11_EBV_0_01.fitsDECam_z_AB.fits
cp 4most_LRG_zmin_07_zmax_09_EBV_0_01.fits 4most_LRG_zmin_07_zmax_09_EBV_0_01.fitsDECam_r_AB.fits
cp 4most_LRG_zmin_07_zmax_09_EBV_0_01.fits 4most_LRG_zmin_07_zmax_09_EBV_0_01.fitsDECam_z_AB.fits
cp 4most_LRG_zmin_05_zmax_07_EBV_0_01.fits 4most_LRG_zmin_05_zmax_07_EBV_0_01.fitsDECam_r_AB.fits
cp 4most_LRG_zmin_05_zmax_07_EBV_0_01.fits 4most_LRG_zmin_05_zmax_07_EBV_0_01.fitsDECam_z_AB.fits
cp 4most_LRG_zmin_03_zmax_05_EBV_0_01.fits 4most_LRG_zmin_03_zmax_05_EBV_0_01.fitsDECam_z_AB.fits
cp 4most_LRG_zmin_03_zmax_05_EBV_0_01.fits 4most_LRG_zmin_03_zmax_05_EBV_0_01.fitsDECam_r_AB.fits

"""
import glob
import sys
from astropy_healpix import healpy
import os
from scipy.special import gammainc  # , gamma,  gammaincinv, gammaincc
from scipy.stats import scoreatpercentile
import pandas as pd  # external package
from scipy.special import erf
from astropy.coordinates import SkyCoord
import astropy.constants as cc
import astropy.io.fits as fits
from astropy.table import Table, Column, vstack
import astropy.units as u
import numpy as n
print('CREATES 4FS FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')


path_2_clusterredGAL = 'test_2020_12_14_null_8galaxies_INPUT.fits'
path_2_output = os.path.join('test_2020_12_14_TEMPLATE_MAGTYPE_merged_8galaxies.fits')
path_2_NULL_OUT = 'test_2020_12_14_NULL_8galaxies.fits'

sub_survey_names = n.array([ 'cluster_redGAL', 'filament_GAL'])

t1 = fits.open(path_2_clusterredGAL)[1].data
t1['MAG'][:]=18.0
t1['EXTENT_FLAG'][:]=2
t1['EXTENT_PARAMETER'][:]=1.5
t1['EXTENT_INDEX'][:]=4
t1['RA'][:]=0.0
t1['DEC'][:]=0.0

#s_fil = (t1['SUBSURVEY']=="filament_GAL")
#s_gal = (t1['SUBSURVEY']=="cluster_redGAL")

tpl2 = (t1['TEMPLATE'].strip() + t1['MAG_TYPE']).astype('str').strip()+'.fits'

t2 = Table(t1)
t2['TEMPLATE'] = tpl2

t2.write (path_2_output , overwrite=True)

t3 = Table(t1)
t3.write (path_2_NULL_OUT , overwrite=True)

	
"""
cp $MD10/S5_4MOST_Oct2020.fit /home/comparat/data/4most/S5
cd /home/comparat/data/4most/S5
gzip S5_4MOST_Oct2020.fit 
"""
