import os, glob, sys
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm 
from matplotlib import rc

from astropy.io import fits
from astropy.coordinates import SkyCoord
import astropy.units as u
import numpy as np
import healpy as hp


def mk_radec_plot(ra,dec,nside=32,filename=' '):
    #
    # ra,dec in degrees
    # nside is the healpix parameter setting the size of pixels, must be a power of 2, less than 2**30
    #
    npix = hp.nside2npix(nside)
    pixarea = hp.nside2pixarea(nside, degrees = True)
    print("Pixel area [deg^2]: ",pixarea)
    # convert ra,dec to HEALPix indices and count
    indices = hp.ang2pix(nside, ra, dec, lonlat=True)
    idx, counts = np.unique(indices, return_counts=True)
    # fill the fullsky map
    hpx_map = np.zeros(npix, dtype=int)
    hpx_map[idx]      = counts/pixarea
    # set some fonts
    rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    ## for Palatino and other serif fonts use:
    #rc('font',**{'family':'serif','serif':['Palatino']})
    #rc('text', usetex=True)
    #rc('mathtext.fontset','stixsans')
    # define colour map
    cmap = plt.cm.inferno
    cmap.set_bad('w')
    cmap.set_under('w')
    # make the map with meridians overplotted
    hp.mollview(hpx_map, 
                coord='C', rot=(180,0),
                unit=r'Object counts per degree$^2$', xsize = 1000, cmap=cmap, min=1, norm='log', cbar=None)
    hp.graticule(dpar=30, dmer=45, verbose= True)
    # HA labels
    params = {'mathtext.default': 'regular' }          
    plt.rcParams.update(params)
    #hp.projtext(20,  32,  '$\mathrm{\mathsf{0^h}}$',     color = 'black', fontsize = 16, lonlat=True)
    #hp.projtext(88,  32,  '$\mathrm{\mathsf{6^h}}$',     color = 'black', fontsize = 16, lonlat=True)
    #hp.projtext(177, 32,  '$\mathrm{\mathsf{12^h}}$',    color = 'black', fontsize = 16, lonlat=True) #rotation
    #hp.projtext(264, 32,  '$\mathrm{\mathsf{18^h}}$',    color = 'black', fontsize = 16, lonlat=True)
    #hp.projtext(351, 32,  '$\mathrm{\mathsf{24^h}}$',    color = 'black', fontsize = 16, lonlat=True)
    hp.projtext(20,  32,  '$\mathrm{\mathsf{0^\circ}}$',     color = 'black', fontsize = 16, lonlat=True)
    hp.projtext(88,  32,  '$\mathrm{\mathsf{90^\circ}}$',     color = 'black', fontsize = 16, lonlat=True)
    hp.projtext(177, 32,  '$\mathrm{\mathsf{180^\circ}}$',    color = 'black', fontsize = 16, lonlat=True) #rotation
    hp.projtext(264, 32,  '$\mathrm{\mathsf{270^\circ}}$',    color = 'black', fontsize = 16, lonlat=True)
    hp.projtext(351, 32,  '$\mathrm{\mathsf{360^\circ}}$',    color = 'black', fontsize = 16, lonlat=True)
    plt.title(' ', size=14)
    ax = plt.gca()
    im = ax.get_images()[0]
    # DEC labels
    fig= plt.gcf()
    for ax in fig.get_axes():
        if type(ax) is hp.projaxes.HpxMollweideAxes:
            ax.set_ylim(-1, 0.51)
            ax.set_position([0.02, 0.03, 0.94, 0.95])
            ax.annotate(r'$\, \mathrm{\mathsf{ 0^\circ}}$', xy=(2.04,-0.02),    size=16, annotation_clip=False) 
            ax.annotate(r'$\, \mathrm{\mathsf{ 30^\circ}}$', xy=(1.90,0.38),     size=16) 
            ax.annotate(r'$\, \mathrm{\mathsf{-30^\circ}}$', xy=(1.86,-0.42),    size=16) 
            ax.annotate(r'$\, \mathrm{\mathsf{-60^\circ}}$', xy=(1.38, -0.78),   size=16) 
            ax.annotate(r'%s targets' %len(ra), xy=(-2.0, -0.92),  size=14) # optional, can be removed

    # create colour bar
    cbaxes = fig.add_axes([0.1, 0.15, 0.8, 0.04]) # [left, bottom, width, height]
    tks=[1, 10, 100, 1000, 10000, 100000, 1000000, 10000000]
    cb = plt.colorbar(im,  orientation='horizontal', cax = cbaxes, ticks=tks)             
    cb.set_label('Object counts per degree$\mathrm{\mathsf{^2}}$', fontsize=14)
    cb.ax.tick_params(labelsize=16)
    plt.xlim([0,1])
    # save plot to PDF file
    plt.savefig(filename + '.pdf')
    plt.savefig(filename + '.png')

filename = os.path.join(os.environ['MD10'], 'S5_4MOST_RFIB210.fit')
hdus = fits.open(filename)
ra  = hdus[1].data['RA']
dec = hdus[1].data['DEC']
mk_radec_plot(ra,dec,filename='/home/comparat/afs_comparat/4MOST/erosita_clusters')

sys.exit()

file_list1 = np.array(glob.glob('/data37s/simulation_1/MD/MD_1.0Gpc/*4MOST.fits'))
file_list2 = np.array(glob.glob('/data37s/simulation_1/MD/MD_1.0Gpc/*4MOST.fit'))
file_list = np.hstack((file_list1, file_list2))
file_list.sort()

for el in file_list[::-1][:2]:
	hdus = fits.open(el)
	bn = os.path.basename(el)[:-5]
	ra  = hdus[1].data['RA']
	dec = hdus[1].data['DEC']
	ok = (dec<5)#( abs( bb ) > 20 ) & ( r_mag < 23.5 ) & ( ll > 180)
	mk_radec_plot(ra[ok],dec[ok],filename='/home/comparat/wwwDir/4MOST/sky_plot_'+bn)

def get_ra_dec_erosita(filname):
	hdus = fits.open(filname)
	ra  = hdus[1].data['RA']
	dec = hdus[1].data['DEC']
	ok = (dec<5) & (hdus[1].data['g_lon']>180) & (abs(hdus[1].data['g_lat'])>10)
	return ra[ok],dec[ok]

ra, dec = np.hstack((
  #get_ra_dec_erosita(file_list[0] ),
  get_ra_dec_erosita(file_list[1] ),
  get_ra_dec_erosita(file_list[2] )
  ))
mk_radec_plot(ra,dec,filename='/home/comparat/wwwDir/4MOST/erosita_agn')


from astropy.coordinates import SkyCoord

def get_ra_dec_direct(filname):
	hdus = fits.open(filname)
	print(filname)
	print(hdus[1].data.columns)
	ra  = hdus[1].data['RA']
	dec = hdus[1].data['DEC']
	coords = SkyCoord(ra, dec, unit='deg', frame='icrs')
	g_lat = coords.galactic.b.value
	g_lon = coords.galactic.l.value
	ok = (dec<5) & (g_lon>180) & (abs(g_lat)>10)
	return ra[ok],dec[ok]

#bb_ecl = coords.barycentrictrueecliptic.lat
#ll_ecl = coords.barycentrictrueecliptic.lon

ra, dec = np.hstack((
  get_ra_dec_direct(file_list[5] ),
  get_ra_dec_direct(file_list[-1] ),
  get_ra_dec_direct(file_list[-2] )
  ))
mk_radec_plot(ra,dec,filename='/home/comparat/wwwDir/4MOST/erosita_clusters')

ra, dec = np.hstack((
  get_ra_dec_direct(file_list[-1] ),
  get_ra_dec_direct(file_list[-2] )
  ))
mk_radec_plot(ra,dec,filename='/home/comparat/wwwDir/4MOST/erosita_clusters_only')

ra, dec = get_ra_dec_direct(file_list[5] )
mk_radec_plot(ra,dec,filename='/home/comparat/wwwDir/4MOST/erosita_filaments')

sys.exit()

hdus = fits.open('output_catalogue_S6_LR.fits')
ra  = hdus[1].data['rightascension']
dec = hdus[1].data['declination']
#r_mag = hdus[1].data['sdss_r_magnitude']

#from astropy.coordinates import SkyCoord
#coords = SkyCoord(ra, dec, unit='deg', frame='icrs')
#bb = coords.galactic.b.value
#ll = coords.galactic.l.value
##bb_ecl = coords.barycentrictrueecliptic.lat
##ll_ecl = coords.barycentrictrueecliptic.lon

ok = (dec<5)#( abs( bb ) > 20 ) & ( r_mag < 23.5 ) & ( ll > 180)

mk_radec_plot(ra[ok],dec[ok],filename='/home/comparat/wwwDir/4MOST/sky_plot_S6')
#mk_radec_plot(ra[ok],dec[ok],filename='sky_plot_S6')


hdus = fits.open('output_catalogue_S5_LR.fits')
ra  = hdus[1].data['rightascension']
dec = hdus[1].data['declination']
#r_mag = hdus[1].data['sdss_r_magnitude']

#from astropy.coordinates import SkyCoord
#coords = SkyCoord(ra, dec, unit='deg', frame='icrs')
#bb = coords.galactic.b.value
#ll = coords.galactic.l.value
##bb_ecl = coords.barycentrictrueecliptic.lat
##ll_ecl = coords.barycentrictrueecliptic.lon

ok = (dec<5)#( abs( bb ) > 20 ) & ( r_mag < 23.5 ) & ( ll > 180)

mk_radec_plot(ra[ok],dec[ok],filename='/home/comparat/wwwDir/4MOST/sky_plot_S5')

