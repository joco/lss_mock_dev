import os, sys
import numpy as n
import healpy
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt
import astropy.io.fits as fits
from matplotlib.path import Path 
from astropy.coordinates import SkyCoord
from astropy.table import Table, Column
from sklearn.neighbors import BallTree

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from scipy.interpolate import interp1d

survey = 'S8'
working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'qmost', survey)
catalogue_dir = os.path.join(os.environ['HOME'], 'data', '4most', survey)
# name of the sub survey for which the LSM is created
#sub_survey_names = n.array([ 'cluster_BCG', 'cluster_redGAL', 'filament_GAL'])
sub_survey_names = n.array([ 'BG', 'LRG', 'ELG', 'QSO', 'LyA'])
# loads the catalog
data = fits.open( os.path.join( catalogue_dir, 'CRS_allJuly_cadencecolumn.fits.gz' ) )[1].data
selections = n.array([ (data['SUBSURVEY']==sub_survey_name) for sub_survey_name in sub_survey_names ])

path_2_kids = os.path.join(os.environ['MD10'], '4most_s8_kidsdr4.lsdr9.fits')
t_kids_i = fits.open(path_2_kids)[1].data

dup_type = (t_kids_i ['lsdr9_TYPE']=="DUP")
dash_type = (t_kids_i ['lsdr9_TYPE']=="-")
t_kids = t_kids_i[(dup_type==False) & (dash_type==False)]

env = 'MD10'
if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

zs = n.arange(0.0000001, 7.1, 0.001)
dm_itp = interp1d(zs, cosmo.distmod(zs).value)

def add_magnitudes_direct_match(t_survey, flag = '', t_kids_i=t_kids):
	# simulated quantities
	print('addding magnitudes')
	sim_redshift = t_survey['redshift_R']
	sim_k_mag = t_survey['K_mag_abs']+dm_itp(t_survey['redshift_R'])
	# quantities in KIDS
	if flag =="":
		s_kids = (t_kids_i['zphot']<=n.max(sim_redshift)) & (t_kids_i['zphot']>=n.min(sim_redshift)) 
	else:
		s_kids = (t_kids_i[flag]) & (t_kids_i['zphot']<=n.max(sim_redshift)) & (t_kids_i['zphot']>=n.min(sim_redshift)) 
	t_kids2 = t_kids_i[s_kids]
	kids_k_mag = t_kids2['rtot']-(t_kids2['ri']+t_kids2['iz']+t_kids2['zy']+t_kids2['yj']+t_kids2['jh']+t_kids2['hks'])
	kids_photoz = t_kids2['zphot']
	# input kids into a tree
	print('matching catalogues')
	Tree_kids = BallTree(n.transpose([kids_photoz, kids_k_mag]))
	DATA = n.transpose([sim_redshift, sim_k_mag])
	kids_ID = n.arange(len(t_kids2['rtot']))
	ids_out = Tree_kids.query(DATA, k=1, return_distance = False)
	ids = n.hstack((ids_out))
	id_to_map = kids_ID[ids]
	columns_to_add = n.array(['rtot'          
								, 'rfib'          
								, 'ug'            
								, 'gr'            
								, 'ri'            
								, 'iz'            
								, 'zy'            
								, 'yj'            
								, 'jh'            
								, 'hks'           
								, 'lsdr9_TYPE'    
								, 'lsdr9_RA'      
								, 'lsdr9_DEC'     
								, 'lsdr9_SHAPE_R' 
								, 'lsdr9_SHAPE_E1'
								, 'lsdr9_SHAPE_E2'
								, 'lsdr9_SERSIC'  
								, 'lsdr9_MASKBITS'])
	print('outputing results')
	for el in columns_to_add :
		if el in t_survey.columns :
			t_survey[el] = t_kids2[el][id_to_map]
		else:
			t_survey.add_column(Column(name=el, data=t_kids2[el][id_to_map]))
	return t_survey #.write(path_2_out)

def modify_sersic(t_s0):
	print('assigns magnitudes and sersic types')
	t_s0['MAG'] = t_s0['rtot']
	phot_types = n.unique(t_s0['lsdr9_TYPE'])  
	print('LS types', phot_types)
	sel_DEV = (t_s0['lsdr9_TYPE'] == "DEV")
	sel_EXP = (t_s0['lsdr9_TYPE'] == "EXP")
	sel_PSF = (t_s0['lsdr9_TYPE'] == "PSF")
	sel_REX = (t_s0['lsdr9_TYPE'] == "REX")
	sel_SER = (t_s0['lsdr9_TYPE'] == "SER")
	# extent flags and parameters
	t_s0['EXTENT_FLAG'][sel_PSF] = 0
	t_s0['EXTENT_FLAG'][sel_DEV] = 2
	t_s0['EXTENT_FLAG'][sel_EXP] = 2
	t_s0['EXTENT_FLAG'][sel_REX] = 2
	t_s0['EXTENT_FLAG'][sel_SER] = 2
	#
	t_s0['EXTENT_PARAMETER'][sel_PSF] = 0
	t_s0['EXTENT_PARAMETER'][sel_DEV] = t_s0['lsdr9_SHAPE_R'][sel_DEV]
	t_s0['EXTENT_PARAMETER'][sel_EXP] = t_s0['lsdr9_SHAPE_R'][sel_EXP]
	t_s0['EXTENT_PARAMETER'][sel_REX] = t_s0['lsdr9_SHAPE_R'][sel_REX]
	t_s0['EXTENT_PARAMETER'][sel_SER] = t_s0['lsdr9_SHAPE_R'][sel_SER]
	#
	print('min extent param', t_s0['EXTENT_PARAMETER'][sel_PSF == False].min() )
	print('numbers lower than allowed values', 
	   len(t_s0['EXTENT_PARAMETER'] [ ( t_s0['EXTENT_PARAMETER']<0.1 ) & (sel_PSF == False ) ]), 
	   t_s0['EXTENT_PARAMETER'] [ ( t_s0['EXTENT_PARAMETER']<0.1 ) & (sel_PSF == False ) ]
	   )
	t_s0['EXTENT_PARAMETER'] [ ( t_s0['EXTENT_PARAMETER']<0.1 ) & (sel_PSF == False ) ] = 0.101
	#
	t_s0['EXTENT_INDEX'][sel_PSF] = 0
	t_s0['EXTENT_INDEX'][sel_DEV] = 4
	t_s0['EXTENT_INDEX'][sel_EXP] = 1
	t_s0['EXTENT_INDEX'][sel_REX] = 1
	t_s0['EXTENT_INDEX'][sel_SER] = t_s0['lsdr9_SERSIC'][sel_SER]
	print('min index', t_s0['EXTENT_INDEX'][sel_PSF == False].min() )
	return t_s0

t_kids_i=t_kids

t_survey = Table(data[selections[0]])
flag='iss8bg'
t_s0 = add_magnitudes_direct_match(t_survey, flag = '', t_kids_i=t_kids)
t_s0 = modify_sersic(t_s0)

t_survey = Table(data[selections[1]])
flag='iss8lrg'
t_s1 = add_magnitudes_direct_match(t_survey, flag = '', t_kids_i=t_kids)
t_s1 = modify_sersic(t_s1)

t_survey = Table(data[selections[2]])
flag='iss8elg'
t_s2 = add_magnitudes_direct_match(t_survey, flag = '', t_kids_i=t_kids)
t_s2 = modify_sersic(t_s2)

t_out = n.hstack((t_s0, t_s1, t_s2))
#Table(t_out).write(os.path.join( catalogue_dir, 'CRS_BG_LRG_ELG_withExtentParams.fits' ) )

#Table(t_s0).write(os.path.join( catalogue_dir, 'CRS_oBG_withExtentParams.fits' ) )
Table(t_s1).write(os.path.join( catalogue_dir, 'CRS_oLRG_withExtentParams.fits' ) )
Table(t_s2).write(os.path.join( catalogue_dir, 'CRS_oELG_withExtentParams.fits' ) )

