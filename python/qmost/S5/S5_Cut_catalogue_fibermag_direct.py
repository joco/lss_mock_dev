import os, sys
import numpy as np
import healpy
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt
import astropy.io.fits as fits
from matplotlib.path import Path 
from astropy.coordinates import SkyCoord
from astropy.table import Table, Column

survey = 's5'
working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'qmost', survey)
catalogue_dir = os.path.join(os.environ['HOME'], 'sf_Shared', 'data', '4most', survey)
# name of the sub survey for which the LSM is created
sub_survey_names = np.array([ 'cluster_BCG', 'cluster_redGAL', 'filament_GAL'])
# loads the catalog out_name = os.path.join(out_dir, 'S5_merged_17Jan2023.fits')
data = fits.open(os.path.join(catalogue_dir, 'S5_merged_17Jan2023.fits'))[1].data
# output cat 
p_2_out = os.path.join(catalogue_dir, 'S5_merged_17Jan2023_FIBERMAGcuts.fits')
selections = np.array([(data['SUBSURVEY']==sub_survey_name) for sub_survey_name in sub_survey_names ])
comps = np.array([ 0.5**(1./6.), 0.5**(1./2.), 0.5**(1./2.)])

fiber_mag_max = np.array([ 24.5, 24.5, 24.5 ])
t_exp_max = np.array([ 100., 60., 40. ])
nl = lambda sel : len(sel.nonzero()[0])

N_in_subsurvey = np.array([nl(sel) for sel in selections ])
fiber_mag_max = np.array([ 23.0, 22.0, 19.5 ])

selections_FIBMAG = np.array([(data['SUBSURVEY']==sub_survey_name)& (  (data['CAL_MAG_BLUE']<fib_max+1) | (data['CAL_MAG_GREEN']<fib_max+0.5) | (data['CAL_MAG_RED']<fib_max) ) for sub_survey_name, fib_max in zip(sub_survey_names, fiber_mag_max) ])

num_tot = lambda fiber_mag_max : np.array([nl((sel)& (  (data['CAL_MAG_BLUE']<fib_max) | (data['CAL_MAG_GREEN']<fib_max) | (data['CAL_MAG_RED']<fib_max) ))  for sel, fib_max in zip(selections, fiber_mag_max) ])

numbers = num_tot(fiber_mag_max)
print('all',N_in_subsurvey, '=>')
print('sel',numbers)

final_selection = np.any(selections_FIBMAG, axis=0)
h1 = data[final_selection]
print('total',len(data), '=>', len(h1))
data=0
t_out = []
for nnn, fmt, unit in zip(h1.columns.names, h1.columns.formats, h1.columns.units):
  t_out.append(fits.Column(name = nnn, array = h1[nnn], unit=unit, format=fmt ) )

hdu = fits.BinTableHDU.from_columns(t_out)
hdu.name = 'S05'
hdu.header['FMTDOC'] = 'VIS-MAN-4MOST-47110-1720-0001'
hdu.header['FMTVERS'] = '2.3'

outf = fits.HDUList([fits.PrimaryHDU(), hdu])
outf.writeto(p_2_out, overwrite=True)
print(len(h1), p_2_out, 'written')


