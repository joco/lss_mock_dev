"""
Creates plots after an ETC run

A few general plots.


"""


# imports
import os, sys
import numpy as n
import astropy.io.fits as fits
from scipy.interpolate import interp1d
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p

from scipy.optimize import curve_fit

survey = 'S5'
working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'qmost', survey, 'etc_plots_2022_09_28')
catalogue_dir = os.path.join(os.environ['HOME'], 'data', '4most', survey)
template_dir = os.path.join(os.environ['HOME'], 'data', '4most', 'templates')
# name of the sub survey for which the LSM is created
sub_survey_names = n.array([ 'cluster_BCG', 'cluster_redGAL', 'filament_GAL'])
t_exp_max = n.array([ 100., 60., 40. ])

# loads the catalog
path_2_catalog_SN2 = os.path.join( catalogue_dir, 'S5_merged_07Okt2022_correct_output_ETC2022-10-07_14_41_55.fits' )

#path_2_catalog_SN2 = os.path.join( catalogue_dir, 'S5_4MOST_Dec2020_FIBERMAGcut_correct_output_ETC2020-12-07_14_37_45_SNR2.fits' )
#path_2_catalog_SN3 = os.path.join( catalogue_dir, 'S5_4MOST_Dec2020_FIBERMAGcut_correct_output_ETC2020-12-09_10_08_13_SNR3.fits' )
#path_2_catalog_SN4 = os.path.join( catalogue_dir, 'S5_4MOST_Dec2020_FIBERMAGcut_correct_output_ETC2020-12-09_21_52_03_SNR4.fits' )
#path_2_catalog_SN10 = os.path.join( catalogue_dir, 'S5_4MOST_Dec2020_FIBERMAGcut_correct_output_ETC2020-12-10_07_42_06_SNR10.fits' )
#path_2_catalog_SN5 = os.path.join( catalogue_dir, 'S5_4MOST_Oct2020_FIBERMAGcut_correct_output_ETC2020-10-16_09_59_27.fits')

SN2 = fits.open(path_2_catalog_SN2)[1].data
#SN3 = fits.open(path_2_catalog_SN3)[1].data
#SN4 = fits.open(path_2_catalog_SN4)[1].data
#SN10 = fits.open(path_2_catalog_SN10)[1].data
#SN5 = fits.open(path_2_catalog_SN5)[1].data
rulesets = n.unique(SN2['RULESET'])
rulesets.sort()
sub_survey_names = rulesets
sel_SN2 = n.array([(SN2['RULESET']==rset) for rset in rulesets ])
#sel_SN2 = n.array([(SN2['SUBSURVEY']==sub_survey_name) for sub_survey_name in sub_survey_names ])
#sel_SN3 = n.array([(SN3['SUBSURVEY']==sub_survey_name) for sub_survey_name in sub_survey_names ])
#sel_SN4 = n.array([(SN4['SUBSURVEY']==sub_survey_name) for sub_survey_name in sub_survey_names ])
#sel_SN10 = n.array([(SN10['SUBSURVEY']==sub_survey_name) for sub_survey_name in sub_survey_names ])
#sel_SN5 = n.array([(SN5['SUBSURVEY']==sub_survey_name) for sub_survey_name in sub_survey_names ])
dM = 0.05
mags = n.arange(16, 22.5, dM)

PVAL_sn2 = [ 0.04812614, -1.35275648,  8.18682368]
PVAL_sn3  = [ 0.05517423, -1.59874548, 10.52500111]
PVAL_sn4  = [ 0.05894095, -1.72499971, 11.72692424]
PVAL_sn5 = [  0.06144122, -1.80736249, 12.52472427]
PVAL_sn10 = [ 0.06445075, -1.88553585, 13.40055915]
PVAL_sn20 = [ 0.059883,   -1.66248454, 11.4115031]

times_sn2 = interp1d(10**n.polyval(PVAL_sn2, mags), mags)
times_sn5 = interp1d(10**n.polyval(PVAL_sn5, mags), mags)
times_sn20 = interp1d(10**n.polyval(PVAL_sn20, mags), mags)
times_sn3 = interp1d(10**n.polyval(PVAL_sn3, mags), mags)
times_sn4 = interp1d(10**n.polyval(PVAL_sn4, mags), mags)
times_sn10 = interp1d(10**n.polyval(PVAL_sn10, mags), mags)


def make_plot(data, path_2_fig, sub_id=1, key='SN2'):
	print("=============================================")
	print(path_2_fig)
	#print(sub_survey_names[sub_id])
	print(key)
	MAG    = data['MAG'      ][sel_SN2[sub_id]]
	FMAG   = data['FIBER_MAG'][sel_SN2[sub_id]]
	TEXP_B = data['TEXP_B'   ][sel_SN2[sub_id]]
	TEXP_G = data['TEXP_G'   ][sel_SN2[sub_id]]
	TEXP_D = data['TEXP_D'   ][sel_SN2[sub_id]]
	tmax_val = 100. # t_exp_max[sub_id]
	# one plot per template 
	# A4 figure
	fig = p.figure(0, (8.2, 12), frameon=False )

	# exposure time tracks
	fig.add_subplot(321, ylim=((0.010,1e3)),
					xlabel='MAG', 
					ylabel='TEXP [minutes]',
					yscale='log', title = 'Dark') # title = sub_survey_names[sub_id] +

	#p.plot(MAG, TEXP_B, color='r', marker=',', ls='', label=r'bright N$_{fh}$='+str(n.round(n.sum(TEXP_B)/60.,1)), rasterized=True, alpha=0.1)
	#p.plot(MAG, TEXP_G, color='g', marker=',', ls='', label=r'grey N$_{fh}$='  +str(n.round(n.sum(TEXP_G)/60.,1)), rasterized=True, alpha=0.1)
	p.plot(MAG, TEXP_D, color='b', marker=',', ls='', label=r'dark N$_{fh}$='  +str(n.round(n.sum(TEXP_D)/60.,1)), rasterized=True, alpha=0.1)
	p.axhline(tmax_val, label='TMAX', ls='dashed', color='k')
	p.axhline(7, label='7-10min', ls='dashed', color='cyan')
	p.axhline(10, ls='dashed', color='cyan')
	#p.plot(mags, mags)
	#p.plot(mags, mags**0.5)
	p.grid()
	p.legend( loc=0)
	
	# exposure time tracks
	fig.add_subplot(322, ylim=((0.010,1e3)),
					xlabel='FIBER MAG', 
					#ylabel='TEXP [minutes]',
					yscale='log')

	#p.plot(FMAG, TEXP_B, color='r', marker=',', ls='', label=r'bright N$_{fh}$='+str(n.round(n.sum(TEXP_B)/60.,1)), rasterized=True, alpha=0.1)
	#p.plot(FMAG, TEXP_G, color='g', marker='+', ls='', label=r'grey N$_{fh}$='  +str(n.round(n.sum(TEXP_G)/60.,1)), rasterized=True, alpha=0.1)
	p.plot(FMAG, TEXP_D, color='b', marker='^', ls='', rasterized=True, alpha=0.1) # , label=r'dark N$_{fh}$='  +str(n.round(n.sum(TEXP_D)/60.,1))
	binned_values = n.array([TEXP_D[(FMAG>m-dM/2)&(FMAG<m+dM/2)] for m in mags])
	mean_values = n.log10(n.array([n.mean(binned_values_i) for binned_values_i in  binned_values ]))
	std_values = n.array([n.std(binned_values_i) for binned_values_i in  binned_values ])
	x_fit = mags[(mean_values>-n.inf) & (mean_values<n.inf)]
	y_fit = mean_values[(mean_values>-n.inf) & (mean_values<n.inf)]
	pval=n.polyfit(x_fit, y_fit,deg=2)
	print( pval)
	p.plot(mags, 10**n.polyval(PVAL_sn2, mags), ls='dashed', label='SN2')
	p.plot(mags, 10**n.polyval(PVAL_sn5, mags), ls='dashed', label='SN5')
	p.plot(mags, 10**n.polyval(PVAL_sn10, mags), ls='dashed', label='SN10')
	p.plot(mags, 10**n.polyval(PVAL_sn20, mags), ls='dashed', label='SN20')

	if key=='SN20':
		p.plot(mags, 10**n.polyval(PVAL_sn20, mags), ls='solid', label='SN20')
	if key=='SN5':
		p.plot(mags, 10**n.polyval(PVAL_sn5, mags), ls='solid', label='SN5')
	if key=='SN2':
		p.plot(mags, 10**n.polyval(PVAL_sn2, mags), ls='solid', label='SN2')
	if key=='SN3':
		p.plot(mags, 10**n.polyval(PVAL_sn3, mags), ls='solid', label='SN3')
	if key=='SN4':
		p.plot(mags, 10**n.polyval(PVAL_sn4, mags), ls='solid', label='SN4')
	if key=='SN10':
		p.plot(mags, 10**n.polyval(PVAL_sn10, mags), ls='solid', label='SN10')

	p.axhline(tmax_val,  ls='dashed', color='k')
	p.axhline(7, ls='dashed', color='cyan')
	p.axhline(10, ls='dashed', color='cyan')
	#p.plot(mags, 10**n.polyval(pval, mags), ls='dashed', label=n.round(pval,2))
	p.grid()
	p.legend( loc=0)

	# statistics in catalog
	fig.add_subplot(312, 
					xlabel='MAG, FIBER MAG', 
					ylabel='Counts normed',
					)

	p.hist(MAG, bins=n.arange(n.min(MAG)-0.1, n.max(MAG)+0.1, 0.1 ), histtype='step', lw=2, label='MAG, cumul', cumulative=True, density=True)

	p.hist(FMAG, bins=n.arange(n.min(FMAG)-0.1, n.max(FMAG)+0.1, 0.1 ), histtype='step', lw=2, label='FIBER MAG, cumul', cumulative=True, density=True)

	p.hist(MAG, bins=n.arange(n.min(MAG)-0.1, n.max(MAG)+0.1, 0.1 ), histtype='step', lw=2, density=True)

	p.hist(FMAG, bins=n.arange(n.min(FMAG)-0.1, n.max(FMAG)+0.1, 0.1 ), histtype='step', lw=2, density=True)

	if key=='SN20':
		p.axvline(times_sn20(tmax_val), color='k', ls='dashed')
		p.axvline(times_sn20(10), color='cyan', ls='dashed')
	if key=='SN5':
		p.axvline(times_sn5(tmax_val), color='k', ls='dashed')
		p.axvline(times_sn5(10), color='cyan', ls='dashed')
	if key=='SN2':
		p.axvline(times_sn2(tmax_val), color='k', ls='dashed')
		p.axvline(times_sn2(10), color='cyan', ls='dashed')
	if key=='SN3':
		p.axvline(times_sn3(tmax_val), color='k', ls='dashed')
		p.axvline(times_sn3(10), color='cyan', ls='dashed')
	if key=='SN4':
		p.axvline(times_sn4(tmax_val), color='k', ls='dashed')
		p.axvline(times_sn4(10), color='cyan', ls='dashed')
	if key=='SN10':
		p.axvline(times_sn10(tmax_val), color='k', ls='dashed')
		p.axvline(times_sn10(10), color='cyan', ls='dashed')
	p.grid()
	p.legend( loc=3, title=len(MAG))

	fig.add_subplot(313, 
					xlabel='MAG, FIBER MAG', ylim=((100,1e7)),
					ylabel='N x TEXP (hours)',
					yscale='log')

	p.hist(MAG, weights=TEXP_D/60., bins=n.arange(n.min(MAG)-0.1, n.max(MAG)+0.1, 0.1 ), histtype='step', lw=2, cumulative=True)

	p.hist(FMAG, weights=TEXP_D/60., bins=n.arange(n.min(FMAG)-0.1, n.max(FMAG)+0.1, 0.1 ), histtype='step',lw=2, cumulative=True)

	p.hist(MAG, weights=TEXP_D/60., bins=n.arange(n.min(MAG)-0.1, n.max(MAG)+0.1, 0.1 ), histtype='step',label='MAG', lw=2)

	p.hist(FMAG, weights=TEXP_D/60., bins=n.arange(n.min(FMAG)-0.1, n.max(FMAG)+0.1, 0.1 ), histtype='step',label='FIBER MAG', lw=2)
	if key=='SN20':
		p.axvline(times_sn20(tmax_val), color='k', ls='dashed')
		p.axvline(times_sn20(10), color='cyan', ls='dashed')
	if key=='SN5':
		p.axvline(times_sn5(tmax_val), color='k', ls='dashed')
		p.axvline(times_sn5(10), color='cyan', ls='dashed')
	if key=='SN2':
		p.axvline(times_sn2(tmax_val), color='k', ls='dashed')
		p.axvline(times_sn2(10), color='cyan', ls='dashed')
	if key=='SN3':
		p.axvline(times_sn3(tmax_val), color='k', ls='dashed')
		p.axvline(times_sn3(10), color='cyan', ls='dashed')
	if key=='SN4':
		p.axvline(times_sn4(tmax_val), color='k', ls='dashed')
		p.axvline(times_sn4(10), color='cyan', ls='dashed')
	if key=='SN10':
		p.axvline(times_sn10(tmax_val), color='k', ls='dashed')
		p.axvline(times_sn10(10), color='cyan', ls='dashed')

	p.legend( loc=3)

	p.grid()
	p.savefig(path_2_fig)
	#p.tight_layout()
	p.clf()

#data = SN2
#path_2_fig = os.path.join(working_dir, 'SN3_s05.pdf')
#make_plot(data, path_2_fig, 0, 'SN3')
#path_2_fig = os.path.join(working_dir, 'SN5_s05.pdf')
#make_plot(data, path_2_fig, 1, 'SN5')
#path_2_fig = os.path.join(working_dir, 'SN10_s05.pdf')
#make_plot(data, path_2_fig, 2, 'SN10')

data = SN2
path_2_fig = os.path.join(working_dir, 'SN2_s05.pdf')
make_plot(data, path_2_fig, 0, 'SN2')
path_2_fig = os.path.join(working_dir, 'SN3_s05.pdf')
make_plot(data, path_2_fig, 1, 'SN3')
path_2_fig = os.path.join(working_dir, 'SN4_s05.pdf')
make_plot(data, path_2_fig, 2, 'SN4')
path_2_fig = os.path.join(working_dir, 'SN5_s05.pdf')
make_plot(data, path_2_fig, 3, 'SN5')
path_2_fig = os.path.join(working_dir, 'SN10_s05.pdf')
make_plot(data, path_2_fig, 4, 'SN10')
#path_2_fig = os.path.join(working_dir, 'SN2_s0502.pdf')
#make_plot(data, path_2_fig, 1, 'SN2')
#path_2_fig = os.path.join(working_dir, 'SN2_s0503.pdf')
#make_plot(data, path_2_fig, 2, 'SN2')

sys.exit()

data = SN3
path_2_fig = os.path.join(working_dir, 'SN3_s0501.pdf')
make_plot(data, path_2_fig, 0, 'SN3')
path_2_fig = os.path.join(working_dir, 'SN3_s0502.pdf')
make_plot(data, path_2_fig, 1, 'SN3')
path_2_fig = os.path.join(working_dir, 'SN3_s0503.pdf')
make_plot(data, path_2_fig, 2, 'SN3')

data = SN4
path_2_fig = os.path.join(working_dir, 'SN4_s0501.pdf')
make_plot(data, path_2_fig, 0, 'SN4')
path_2_fig = os.path.join(working_dir, 'SN4_s0502.pdf')
make_plot(data, path_2_fig, 1, 'SN4')
path_2_fig = os.path.join(working_dir, 'SN4_s0503.pdf')
make_plot(data, path_2_fig, 2, 'SN4')

data = SN10
path_2_fig = os.path.join(working_dir, 'SN10_s0501.pdf')
make_plot(data, path_2_fig, 0, 'SN10')
path_2_fig = os.path.join(working_dir, 'SN10_s0502.pdf')
make_plot(data, path_2_fig, 1, 'SN10')
path_2_fig = os.path.join(working_dir, 'SN10_s0503.pdf')
make_plot(data, path_2_fig, 2, 'SN10')

data = SN5
path_2_fig = os.path.join(working_dir, 'SN5_s0501.pdf')
make_plot(data, path_2_fig, 0, 'SN20')
path_2_fig = os.path.join(working_dir, 'SN5_s0502.pdf')
make_plot(data, path_2_fig, 1, 'SN5')
path_2_fig = os.path.join(working_dir, 'SN5_s0503.pdf')
make_plot(data, path_2_fig, 2, 'SN5')

#os.system('tar -czf '+path_2_archive+' '+output_folder)
