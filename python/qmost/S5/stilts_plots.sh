#!/bin/bash

java -jar /home/comparat/software/topcat-full.jar -stilts plot2plane \
   xpix=600 ypix=600 \
   ylog=true xlabel='R_MAG / SDSS r band fiber 1.4arcsec magnitude' \
    ylabel='TEXP_D / min' grid=true fontsize=18 \
   xmin=17 xmax=23 ymin=0.5 ymax=5001 \
   auxmin=0.003 auxmax=1.137 \
   auxvisible=true auxlabel=REDSHIFT \
   legend=true  legpos=0.0,1.0 \
   layer_1=Mark \
      in_1=/home/comparat/data/4most/S5/output_catalogue_S5_LR_4May19.fits \
      x_1=R_MAG y_1=TEXP_D aux_1=REDSHIFT \
      shading_1=aux size_1=5 \
   layer_2=Function \
      fexpr_2='pow(10,0.8*x)*6e-17' color_2=blue thick_2=5 dash_2=3,3 \
      leglabel_2='slope 0.8' \
   layer_3=Function \
      fexpr_3='pow(10,0.4*x)*2e-8' color_3=green thick_3=4 \
      leglabel_3='slope 0.4' \
   layer_4=Function \
      fexpr_4=120 color_4=black thick_4=3 dash_4=12,3,3,3 \
      leglabel_4='2 hours' \
   legseq=_2,_3,_4  \
   omode=out out=Mag_TEXP_D.png



java -jar /home/comparat/software/topcat-full.jar -stilts plot2plane \
   xpix=600 ypix=600 \
   ylog=true xlabel='R_MAG / SDSS r band fiber 1.4arcsec magnitude' \
    ylabel='TEXP_D / min' grid=true  fontsize=18 \
   xmin=17 xmax=23 ymin=0.5 ymax=5001 \
   auxmin=0.003 auxmax=1.137 \
   auxvisible=true auxlabel=REDSHIFT \
   legend=true  legpos=0.0,1.0 \
   layer_1=Mark \
      in_1=/home/comparat/data/4most/S5/output_catalogue_S5_LR_4May19.fits \
      x_1=R_MAG y_1=TEXP_G aux_1=REDSHIFT \
      shading_1=aux size_1=5 \
   layer_2=Function \
      fexpr_2='pow(10,0.8*x)*6e-17' color_2=blue thick_2=5 dash_2=3,3 \
      leglabel_2='slope 0.8' \
   layer_3=Function \
      fexpr_3='pow(10,0.4*x)*2e-8' color_3=green thick_3=4 \
      leglabel_3='slope 0.4' \
   layer_4=Function \
      fexpr_4=120 color_4=black thick_4=3 dash_4=12,3,3,3 \
      leglabel_4='2 hours' \
   legseq=_2,_3,_4  \
   omode=out out=Mag_TEXP_G.png

topcat -stilts plot2plane \
   xpix=600 ypix=600 \
   xlabel=REDSHIFT_ESTIMATE ylabel='MAG / mag' grid=true fontsize=16 \
   xmin=0.02 xmax=1.481 ymin=13.44 ymax=24.71 \
   legend=true \
   in=/data/data/4most/S5/S5_4MOST_Oct2020_FIBERMAGcut_correct_output_ETC2020-10-16_09_59_27.fits x=REDSHIFT_ESTIMATE y=MAG shading=auto \
   layer_1=Mark \
      icmd_1='select "SUBSURVEY==\"cluster_redGAL\""' \
      size_1=2 \
      leglabel_1='1: redgal' \
   layer_2=Mark \
      icmd_2='select "SUBSURVEY==\"cluster_BCG\""' \
      size_2=0 color_2=blue \
      leglabel_2='1: BCG' \
   layer_3=Mark \
      icmd_3='select "SUBSURVEY==\"filament_GAL\""' \
      size_3=0 color_3=black \
      leglabel_3='1: filament'  \
   omode=out out=Mag_Redshift.png 
   
topcat -stilts plot2plane \
   xpix=600 ypix=600 \
   ylog=true xlabel='MAG (AB)' ylabel='Count' grid=true fontsize=16\
   xmin=13 xmax=23 ymin=10 ymax=500000 \
   legend=true legpos=0.0,1.0 \
   in=/data/data/4most/S5/S5_4MOST_Oct2020_FIBERMAGcut_correct_output_ETC2020-10-16_09_59_27.fits \
   x=MAG barform=steps thick=5 \
   layer_1=Histogram \
      icmd_1='select "SUBSURVEY==\"cluster_BCG\""' \
      color_1=blue \
      leglabel_1='1: BCG, z_AB' \
   layer_2=Histogram \
      icmd_2='select "SUBSURVEY==\"cluster_redGAL\""' \
      leglabel_2='1: GAL, z_AB' \
   layer_3=Histogram \
      icmd_3='select "SUBSURVEY==\"filament_GAL\""' \
      color_3=black \
      leglabel_3='1: FIL, r_AB'  \
   omode=out out=Mag_hist.png

topcat -stilts plot2plane \
   xpix=600 ypix=600 \
   ylog=true xlabel='FIBER MAG (AB)' ylabel='Count' grid=true fontsize=16\
   xmin=14.8 xmax=24 ymin=10 ymax=500000 \
   legend=true legpos=0.0,1.0 \
   in=/data/data/4most/S5/S5_4MOST_Oct2020_FIBERMAGcut_correct_output_ETC2020-10-16_09_59_27.fits \
   x=FIBER_MAG barform=steps thick=5 \
   layer_1=Histogram \
      icmd_1='select "SUBSURVEY==\"cluster_BCG\""' \
      color_1=blue \
      leglabel_1='1: BCG, z_AB' \
   layer_2=Histogram \
      icmd_2='select "SUBSURVEY==\"cluster_redGAL\""' \
      leglabel_2='1: GAL, z_AB' \
   layer_3=Histogram \
      icmd_3='select "SUBSURVEY==\"filament_GAL\""' \
      color_3=black \
      leglabel_3='1: FIL, r_AB'  \
   omode=out out=FiberMag_hist.png

topcat -stilts plot2plane \
   xpix=600 ypix=600 \
   ylog=true xlabel='R.A. / deg' ylabel= grid=true fontsize=16 \
   xmin=-4.0E-5 xmax=367 ymin=10 ymax=500000 \
   legend=true legpos=0.0,0.0 \
   in=/data/data/4most/S5/S5_4MOST_Oct2020_FIBERMAGcut_correct_output_ETC2020-10-16_09_59_27.fits x=RA binsize=15.0 barform=steps thick=5 \
   layer_1=Histogram \
      icmd_1='select "SUBSURVEY==\"cluster_BCG\""' \
      color_1=blue \
      leglabel_1='1: BCG' \
   layer_2=Histogram \
      icmd_2='select "SUBSURVEY==\"cluster_redGAL\""' \
      leglabel_2='1: redgal' \
   layer_3=Histogram \
      icmd_3='select "SUBSURVEY==\"filament_GAL\""' \
      color_3=black \
      leglabel_3='1: filament' \
   omode=out out=RA_hist.png
