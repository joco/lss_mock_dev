import os, sys
import numpy as np
import healpy
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt
import astropy.io.fits as fits
from matplotlib.path import Path 
from astropy.coordinates import SkyCoord
from astropy.table import Table, Column

working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'qmost', 'S5')
catalogue_dir = os.path.join(os.environ['HOME'], 'sf_Shared', 'data', '4most', 's5')
# name of the sub survey for which the LSM is created
sub_survey_names = np.array([ 'cluster_BCG', 'cluster_redGAL', 'filament_GAL'])
# loads the catalog
data = fits.open(os.path.join(catalogue_dir, 'S5_merged_17Jan2023_correct_output_ETC2023-01-19_10_49_11.fits'))[1].data
# output cat 
p_2_out = os.path.join(catalogue_dir, 'S5_merged_17Jan2023_FIBERMAGcuts.fits')
selections = np.array([(data['SUBSURVEY']==sub_survey_name) for sub_survey_name in sub_survey_names ])
comps = np.array([ 0.5**(1./6.), 0.5**(1./2.), 0.5**(1./2.)])

fiber_mag_max = np.array([ 24.5, 24.5, 24.5 ])
t_exp_max = np.array([ 100., 60., 40. ])
nl = lambda sel : len(sel.nonzero()[0])

N_in_subsurvey = np.array([nl(sel) for sel in selections ])

#min_fibermag = np.array([np.min(data['CAL_MAG_RED'][sel]) for sel in selections ])
#mea_fibermag = np.array([np.mean(data['CAL_MAG_RED'][sel]) for sel in selections ])
#max_fibermag = np.array([np.max(data['CAL_MAG_RED'][sel]) for sel in selections ])

#min_mag = np.array( [np.min(data['MAG'][sel]) for sel in selections ])
#mea_mag = np.array([np.mean(data['MAG'][sel]) for sel in selections ])
#max_mag = np.array([ np.max(data['MAG'][sel]) for sel in selections ])

texp_d = np.max([data['TEXP_D'], np.ones_like(data['TEXP_D'])*10], axis=0)
texp_g = np.max([data['TEXP_G'], np.ones_like(data['TEXP_D'])*10], axis=0)
texp_b = np.max([data['TEXP_B'], np.ones_like(data['TEXP_D'])*7] , axis=0)

MAG_array = np.arange(18, 27, 0.5)

print('='*100)
print('red')
print('='*100)

num_tot = lambda fiber_mag_max : np.array([nl((sel)&(data['CAL_MAG_RED']<fib_max))  for sel, fib_max in zip(selections, fiber_mag_max) ])

texp_tot = lambda fiber_mag_max : np.array([np.sum(np.min([texp_d, np.ones_like(texp_d)*t_max], axis=0)[sel & (data['CAL_MAG_RED']<fib_max)])*cp*0.75/60/1e6 for sel, fib_max, cp, t_max in zip(selections, fiber_mag_max, comps, t_exp_max) ])

texpDTMatrix = np.array([ texp_tot(np.array([ 1., 1., 1. ])*MAG) for MAG in MAG_array ])

for el1, el2 in zip(MAG_array, texpDTMatrix):
	print(el1, num_tot(np.array([ 1., 1., 1. ])*el1), el2, np.sum(el2))


print('='*100)
print('green')
print('='*100)

num_tot = lambda fiber_mag_max : np.array([nl((sel)&(data['CAL_MAG_GREEN']<fib_max))  for sel, fib_max in zip(selections, fiber_mag_max) ])

texp_tot = lambda fiber_mag_max : np.array([np.sum(np.min([texp_d, np.ones_like(texp_d)*t_max], axis=0)[sel & (data['CAL_MAG_GREEN']<fib_max)])*cp*0.75/60/1e6 for sel, fib_max, cp, t_max in zip(selections, fiber_mag_max, comps, t_exp_max) ])

texpDTMatrix = np.array([ texp_tot(np.array([ 1., 1., 1. ])*MAG) for MAG in MAG_array ])

for el1, el2 in zip(MAG_array, texpDTMatrix):
	print(el1, num_tot(np.array([ 1., 1., 1. ])*el1), el2, np.sum(el2))



print('='*100)
print('blue')
print('='*100)

num_tot = lambda fiber_mag_max : np.array([nl((sel)&(data['CAL_MAG_BLUE']<fib_max))  for sel, fib_max in zip(selections, fiber_mag_max) ])

texp_tot = lambda fiber_mag_max : np.array([np.sum(np.min([texp_d, np.ones_like(texp_d)*t_max], axis=0)[sel & (data['CAL_MAG_BLUE']<fib_max)])*cp*0.75/60/1e6 for sel, fib_max, cp, t_max in zip(selections, fiber_mag_max, comps, t_exp_max) ])

texpDTMatrix = np.array([ texp_tot(np.array([ 1., 1., 1. ])*MAG) for MAG in MAG_array ])

for el1, el2 in zip(MAG_array, texpDTMatrix):
	print(el1, num_tot(np.array([ 1., 1., 1. ])*el1), el2, np.sum(el2))





print('='*100)
print('all bands OR')
print('='*100)

num_tot = lambda fiber_mag_max : np.array([nl((sel)& (  (data['CAL_MAG_BLUE']<fib_max) | (data['CAL_MAG_GREEN']<fib_max) | (data['CAL_MAG_RED']<fib_max) ))  for sel, fib_max in zip(selections, fiber_mag_max) ])

texp_tot = lambda fiber_mag_max : np.array([np.sum(np.min([texp_d, np.ones_like(texp_d)*t_max], axis=0)[sel & (  (data['CAL_MAG_BLUE']<fib_max+1) | (data['CAL_MAG_GREEN']<fib_max+0.5) | (data['CAL_MAG_RED']<fib_max) )])*cp*0.75/60/1e6 for sel, fib_max, cp, t_max in zip(selections, fiber_mag_max, comps, t_exp_max) ])
texpG_tot = lambda fiber_mag_max : np.array([np.sum(np.min([texp_g, np.ones_like(texp_d)*t_max], axis=0)[sel & (  (data['CAL_MAG_BLUE']<fib_max+1) | (data['CAL_MAG_GREEN']<fib_max+0.5) | (data['CAL_MAG_RED']<fib_max) )])*cp*0.75/60/1e6 for sel, fib_max, cp, t_max in zip(selections, fiber_mag_max, comps, t_exp_max) ])
texpB_tot = lambda fiber_mag_max : np.array([np.sum(np.min([texp_b, np.ones_like(texp_d)*t_max], axis=0)[sel & (  (data['CAL_MAG_BLUE']<fib_max+1) | (data['CAL_MAG_GREEN']<fib_max+0.5) | (data['CAL_MAG_RED']<fib_max) )])*cp*0.75/60/1e6 for sel, fib_max, cp, t_max in zip(selections, fiber_mag_max, comps, t_exp_max) ])

texpDTMatrix = np.array([ texp_tot(np.array([ 1., 1., 1. ])*MAG) for MAG in MAG_array ])

for el1, el2 in zip(MAG_array, texpDTMatrix):
	print(el1, num_tot(np.array([ 1., 1., 1. ])*el1), el2, np.sum(el2))


fiber_mag_max = np.array([ 23.0, 22.5, 20.0 ])
numbers = num_tot(fiber_mag_max)
print(numbers)
out = texp_tot(fiber_mag_max)
print('D',out, out.sum())
out = texpG_tot(fiber_mag_max)
print('G',out, out.sum())
out = texpB_tot(fiber_mag_max)
print('B',out, out.sum())

selections_FIBMAG = np.array([(data['SUBSURVEY']==sub_survey_name)& (  (data['CAL_MAG_BLUE']<fib_max+1) | (data['CAL_MAG_GREEN']<fib_max+0.5) | (data['CAL_MAG_RED']<fib_max) ) for sub_survey_name, fib_max in zip(sub_survey_names, fiber_mag_max) ])

#plt.figure(0, (6,6))
#plt.plot(data['CAL_MAG_RED'][selections[2]==False], data['TEXP_D'][selections[2]==False], 'k+', label='S/N>5 red', rasterized=True)
#plt.plot(data['CAL_MAG_RED'][selections[2]], data['TEXP_D'][selections[2]], 'r+', label='S/N>20 red', rasterized=True)
#plt.grid()
##plt.ylim((-90,40))
##plt.xlim((-1,361))
#plt.xlabel('fiber mag')
#plt.ylabel('exposure time (D, minutes)')
#plt.yscale('log')
#plt.tight_layout()
#plt.legend( loc = 0 )
#plt.savefig(os.path.join(working_dir,survey +'_texp_fiberMag.png'))
#plt.clf()


## Re-assigns ruleset
new_rulesets = np.array([ el.replace('Nov21', 'Jan23') for el in data['RULESET'] ])
data['RULESET']= new_rulesets
#data['RULESET'][(data['CAL_MAG_RED']>=20)&(data['CAL_MAG_RED']<21)] = 'Red_SN05_Nov21'
#data['RULESET'][(data['CAL_MAG_RED']>=21)&(data['CAL_MAG_RED']<22)] = 'Red_SN03_Nov21'
#data['RULESET'][(data['CAL_MAG_RED']>=22)] = 'Red_SN02_Nov21'

print('minz',np.min(data['REDSHIFT_ESTIMATE']))
# re-assigning the ones with photoZ=0 to 0.05 (arbitrary value close to 0)
data['REDSHIFT_ESTIMATE'][(data['REDSHIFT_ESTIMATE']==0)]=0.02
print('minz',np.min(data['REDSHIFT_ESTIMATE']))
data['EPOCH'] = 2000.0

final_selection = np.any(selections_FIBMAG, axis=0)
h1 = data[final_selection]
import time
t_out = []
t0 = time.time()
for nnn, fmt, unit in zip(h1.columns.names[:-4], h1.columns.formats[:-4], h1.columns.units[:-4]):
  print(nnn, fmt, time.time()-t0)
  t_out.append(fits.Column(name = nnn, array = h1[nnn], unit=unit, format=fmt ) )

#t_survey = Table()
#for name_i, format_i, unit_i in zip(out_data.columns.names, out_data.columns.formats, out_data.columns.units):
	#t_survey.add_column(Column(name=name_i   , data=out_data[name_i] , unit=unit_i))

#t_survey.remove_columns(['TEXP_D', 'TEXP_G', 'TEXP_B', 'FIBER_MAG'])

##N_obj = len(t_survey)
##CADENCE = np.zeros(N_obj).astype('int64')
##t_survey.add_column(Column(name='CADENCE', data=CADENCE, unit='', format='K'))
##t_survey['DATE_EARLIEST'][:]=0
##t_survey['DATE_LATEST'][:]=0

hdu = fits.BinTableHDU.from_columns(t_out)
hdu.name = 'S05'
hdu.header['FMTDOC'] = 'VIS-MAN-4MOST-47110-1720-0001'
hdu.header['FMTVERS'] = '2.5'

outf = fits.HDUList([fits.PrimaryHDU(), hdu])
outf.writeto(p_2_out, overwrite=True)
print(len(h1), p_2_out, 'written')

