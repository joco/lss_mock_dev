import os, sys
import numpy as np
import healpy
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt
import astropy.io.fits as fits
from matplotlib.path import Path 
from astropy.coordinates import SkyCoord
from astropy.table import Table, Column
from sklearn.neighbors import BallTree

working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'qmost', 'S5')
catalogue_dir = os.path.join(os.environ['HOME'], 'sf_Shared', 'data', '4most', 's5')
# name of the sub survey for which the LSM is created
sub_survey_names = np.array([ 'cluster_BCG', 'cluster_redGAL', 'filament_GAL'])
# output cat 
p_2_out = os.path.join(catalogue_dir, 'S5_merged_17Jan2023_FIBERMAGcuts.fits')
# loads the catalog
data = fits.open(p_2_out)[1].data
selections = np.array([(data['SUBSURVEY']==sub_survey_name) for sub_survey_name in sub_survey_names ])

radius = 0.2 / 3600. * np.pi / 180.
deg_to_rad = np.pi/180.

nl = lambda sel : len(sel.nonzero()[0])
N_in_subsurvey = np.array([nl(sel) for sel in selections ])

def count_pairs(Tree_, coord_, radius = radius):
	test_c = Tree_.query_radius(coord_, r = radius, count_only = True)
	sum_t = np.sum(test_c)
	print(sum_t, time.time()-t0)
	return sum_t

coord_BCG = deg_to_rad * np.transpose([data['DEC'][selections[0]], data['RA'][selections[0]] ])
coord_GAL = deg_to_rad * np.transpose([data['DEC'][selections[1]], data['RA'][selections[1]] ])
coord_FIL = deg_to_rad * np.transpose([data['DEC'][selections[2]], data['RA'][selections[2]] ])
N_BCG = len(coord_BCG)
N_GAL = len(coord_GAL)
N_FIL = len(coord_FIL)
Tree_BCG = BallTree(coord_BCG, metric='haversine')
Tree_GAL = BallTree(coord_GAL, metric='haversine')
Tree_FIL = BallTree(coord_FIL, metric='haversine')
count_BCG_BCG = count_pairs(Tree_BCG, coord_BCG, radius = radius)
count_BCG_GAL = count_pairs(Tree_BCG, coord_GAL, radius = radius)
count_BCG_FIL = count_pairs(Tree_BCG, coord_FIL, radius = radius)
count_GAL_GAL = count_pairs(Tree_GAL, coord_GAL, radius = radius)
count_GAL_FIL = count_pairs(Tree_GAL, coord_FIL, radius = radius)
count_FIL_FIL = count_pairs(Tree_FIL, coord_FIL, radius = radius)

print( 'BCG & ', np.round(100*count_BCG_BCG/N_BCG,1)                                                                                                                                               , " \\\\"        )
print( 'GAL & ', np.round(100*count_BCG_GAL/N_BCG,1),'&', np.round(100*count_GAL_GAL/N_GAL,1)                                                                                                                                , " \\\\"        )
print( 'FIL & ', np.round(100*count_BCG_FIL/N_BCG,1),'&', np.round(100*count_GAL_FIL/N_GAL,1),'&', np.round(100*count_FIL_FIL/N_FIL,1)                                                                                                                 , " \\\\"       )




