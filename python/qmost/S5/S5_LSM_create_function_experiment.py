import os, sys
import numpy as np
import healpy
import matplotlib.pyplot as plt
import astropy.io.fits as fits
from matplotlib.path import Path 
from astropy.coordinates import SkyCoord
import healpy as hp

survey = 'S5'
working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'qmost', survey)
catalogue_dir = os.path.join(os.environ['HOME'], 'data', '4most', survey)
# name of the sub survey for which the LSM is created
sub_survey_names = np.array([ 'cluster_BCG', 'cluster_redGAL', 'filament_GAL'])
# loads the catalog
data = fits.open(os.path.join(catalogue_dir, 'S5_merged_07Okt2022_correct_output_ETC2022-10-07_14_41_55.fits'))[1].data
selections = np.array([(data['SUBSURVEY']==sub_survey_name) for sub_survey_name in sub_survey_names ])

specz_d = fits.open("/home/comparat/data/erosita/validation_archive_confidential.fits")

selection = (specz_d[1].data['z']>0)
RA_spec = specz_d[1].data['ra'][selection]
DEC_spec = specz_d[1].data['dec'][selection]
# compute healpix index for each target
SPECZ_HEALPIX_32   = healpy.ang2pix(32,   np.pi/2. - DEC_spec*np.pi/180. , RA_spec*np.pi/180. , nest=True)
iid, ii_count_i = np.unique(SPECZ_HEALPIX_32, return_counts=True)

NSIDE=32
N_pixels_all = healpy.nside2npix(NSIDE)
pix_ids = np.arange(N_pixels_all)
# ARRAY OF RIGHT ASCENSIONS OF THE PIXEL CENTERS
ra_hp = np.array([healpy.pix2ang(NSIDE,pix_id, nest=True)[1]*180./np.pi for pix_id in pix_ids ])
# ARRAY OF DECLINATION OF THE PIXEL CENTERS
dec_hp = np.array([ (np.pi/2. - healpy.pix2ang(NSIDE,pix_id, nest=True)[0])*180./np.pi for pix_id in pix_ids ])
coords = SkyCoord(ra_hp, dec_hp, unit='deg', frame='icrs')
bb_gal = coords.galactic.b.value
ll_gal = coords.galactic.l.value
bb_ecl = coords.barycentrictrueecliptic.lat
#ll_ecl = coords.barycentrictrueecliptic.lon
# area subtended by each pixel in deg2
area_per_pixel =  healpy.nside2pixarea(NSIDE)*(180/np.pi)**2  
# Initialize all LSM values 
lsm_values = np.zeros(len(ra_hp))

ii_counts = np.ones(len(ra_hp))
ii_counts[iid] = ii_count_i

priority_ecl = abs(bb_ecl.value)+10  # between 10 and 100
val_normed = priority_ecl / np.sum(priority_ecl)
N_specz_deg2 = ii_counts / area_per_pixel
N_specz_deg2_normed = np.log10(N_specz_deg2) / np.sum(np.log10(N_specz_deg2))

erosita = (ll_gal>180)
plot_file = os.path.join(working_dir,survey +'_LSM_test_g_ecl_ero.png')
plt.figure(0, (6,6))
plt.scatter(ra_hp[erosita], dec_hp[erosita], c=np.log10(val_normed[erosita]), s=4, marker='s', edgecolors='face')
cb = plt.colorbar(shrink=0.8)
cb.set_label('|ecliptic lattitude| weight')
plt.grid()
plt.ylim((-90,90))
plt.xlim((-1,361))
plt.xlabel('R.A.')
plt.ylabel('Dec')
plt.savefig(plot_file)
plt.clf()

plot_file = os.path.join(working_dir,survey +'_LSM_test_N_specz_deg2_normed_ero.png')
plt.figure(0, (6,6))
plt.scatter(ra_hp[erosita], dec_hp[erosita], c=np.log10(N_specz_deg2_normed[erosita]), s=4, marker='s', edgecolors='face')
cb = plt.colorbar(shrink=0.8)
cb.set_label('N_specz_deg2_normed')
plt.grid()
plt.ylim((-90,90))
plt.xlim((-1,361))
plt.xlabel('R.A.')
plt.ylabel('Dec')
plt.savefig(plot_file)
plt.clf()

plot_file = os.path.join(working_dir,survey +'_LSM_test_N_specz_deg2_normed_g_ecl_ero.png')
plt.figure(0, (6,6))
plt.scatter(ra_hp[erosita], dec_hp[erosita], c=np.log10(N_specz_deg2_normed[erosita]*val_normed[erosita]), s=4, marker='s', edgecolors='face')
cb = plt.colorbar(shrink=0.8)
cb.set_label('N_specz_deg2_normed x |ecliptic lattitude| weight')
plt.grid()
plt.ylim((-90,90))
plt.xlim((-1,361))
plt.xlabel('R.A.')
plt.ylabel('Dec')
plt.savefig(plot_file)
plt.clf()


plot_file = os.path.join(working_dir,survey +'_LSM_test_g_ecl_all.png')
plt.figure(0, (6,6))
plt.scatter(ra_hp, dec_hp, c=np.log10(val_normed), s=4, marker='s', edgecolors='face')
cb = plt.colorbar(shrink=0.8)
cb.set_label('|ecliptic lattitude| weight')
plt.grid()
plt.ylim((-90,90))
plt.xlim((-1,361))
plt.xlabel('R.A.')
plt.ylabel('Dec')
plt.savefig(plot_file)
plt.clf()

plot_file = os.path.join(working_dir,survey +'_LSM_test_N_specz_deg2_normed_all.png')
plt.figure(0, (6,6))
plt.scatter(ra_hp, dec_hp, c=np.log10(N_specz_deg2_normed), s=4, marker='s', edgecolors='face')
cb = plt.colorbar(shrink=0.8)
cb.set_label('N_specz_deg2_normed')
plt.grid()
plt.ylim((-90,90))
plt.xlim((-1,361))
plt.xlabel('R.A.')
plt.ylabel('Dec')
plt.savefig(plot_file)
plt.clf()


plot_file = os.path.join(working_dir,survey +'_LSM_test_N_specz_deg2_normed_g_ecl_all.png')
plt.figure(0, (6,6))
plt.scatter(ra_hp, dec_hp, c=np.log10(N_specz_deg2_normed*val_normed), s=4, marker='s', edgecolors='face')
cb = plt.colorbar(shrink=0.8)
cb.set_label('N_specz_deg2_normed x |ecliptic lattitude| weight')
plt.grid()
plt.ylim((-90,90))
plt.xlim((-1,361))
plt.xlabel('R.A.')
plt.ylabel('Dec')
plt.savefig(plot_file)
plt.clf()


#########
#########
# 4. Create the fits LSM file
#########
#########
cols = fits.ColDefs([
	fits.Column( "healpix_id"	 ,unit='' ,format="K", array=pix_ids), 
	fits.Column( "RA"            ,unit='deg' ,format="D", array=ra_hp ), 
	fits.Column( "DEC"	         ,unit='deg' ,format="D", array=dec_hp), 
	fits.Column( "LSM"           ,unit='' ,format="D", array=lsm_values)
	])
tbhdu = fits.BinTableHDU.from_columns(cols)
tbhdu.header['author'] = 'JC'
tbhdu.header['HIERARCH SUBSURVEY'] = sub_survey_name
tbhdu.header['NSIDE'] = NSIDE
tbhdu.header['COORD'] = 'equatorial'
tbhdu.header['NESTED'] = 'True'
if os.path.isfile(out_file):
	os.remove(out_file)
tbhdu.writeto(out_file)


DESfile = os.path.join(catalogue_dir, 'DES_footprint.txt')
DESra, DESdec = np.loadtxt(DESfile, unpack=True)
DESnedge= len(DESra)
# isinpoly
DESpoly = Path(np.concatenate(
                    (DESra.reshape((DESnedge,1)),
                     DESdec.reshape((DESnedge,1))),
                    axis=1))

def create_LSM(sub_survey_name, selection):
	out_file = os.path.join(working_dir, survey +'_LSM_'+str(sub_survey_name)+'.fits')
	plot_file = os.path.join(working_dir,survey +'_LSM_'+str(sub_survey_name)+'.png')
	#########
	#########
	# 1. First create the grid of pixels over the full sky
	#########
	#########
	# create the ARRAY OF PIXEL INDEXES
	NSIDE=32
	N_pixels_all = healpy.nside2npix(NSIDE)
	pix_ids = np.arange(N_pixels_all)
	# ARRAY OF RIGHT ASCENSIONS OF THE PIXEL CENTERS
	ra_hp = np.array([healpy.pix2ang(NSIDE,pix_id, nest=True)[1]*180./np.pi for pix_id in pix_ids ])
	# ARRAY OF DECLINATION OF THE PIXEL CENTERS
	dec_hp = np.array([ (np.pi/2. - healpy.pix2ang(NSIDE,pix_id, nest=True)[0])*180./np.pi for pix_id in pix_ids ])
	coords = SkyCoord(ra_hp, dec_hp, unit='deg', frame='icrs')
	bb_gal = coords.galactic.b.value
	ll_gal = coords.galactic.l.value
	#print('bb_gal', bb_gal[:10], time.time() - t0, 's')
	#print('ll_gal', ll_gal[:10], time.time() - t0, 's')
	bb_ecl = coords.barycentrictrueecliptic.lat
	ll_ecl = coords.barycentrictrueecliptic.lon
	#print('bb_ecl', bb_ecl[:10], time.time() - t0, 's')
	#print('ll_ecl', ll_ecl[:10], time.time() - t0, 's')
	# area subtended by each pixel in deg2
	area_per_pixel =  healpy.nside2pixarea(NSIDE)*(180/np.pi)**2  
	# Initialize all LSM values 
	lsm_values = np.zeros(len(ra_hp))
	#########
	#########
	# 2. Retrieve pixels that contain targets
	#########
	#########
	# select coordinates in your catalog file
	RA = data['RA'][selection]
	DEC = data['DEC'][selection]
	# compute healpix index for each target
	DATA_HEALPIX_32   = healpy.ang2pix(32,   np.pi/2. - DEC*np.pi/180. , RA*np.pi/180. , nest=True)
	print('N, DATA_HEALPIX_32', len(DATA_HEALPIX_32), DATA_HEALPIX_32)
	# retrieves a unique and sorted list of healpix indexes containing targets
	unique_healpix_32_indexes = np.unique(DATA_HEALPIX_32)
	unique_healpix_32_indexes.sort()
	N_pixels = len(unique_healpix_32_indexes)
	print('N non-empty pixels, unique_healpix_32_indexes',len(unique_healpix_32_indexes))
	#########
	#########
	# 2. Assigns LSM values
	#########
	#########
	if sub_survey_name=="filament_GAL":
		val_normed = 1/N_pixels
	else:
		# Ecliptic lattitude to prioritize more the deeper area
		priority_ecl = abs(bb_ecl.value)+10  # between 10 and 100
		val_normed = priority_ecl[unique_healpix_32_indexes]/np.sum(priority_ecl[unique_healpix_32_indexes])

	lsm_values[unique_healpix_32_indexes] = val_normed

	# DES footprint
	#ra_hp2 = ra_hp[unique_healpix_32_indexes]
	#dec_hp2 = dec_hp[unique_healpix_32_indexes]
	#print('N non-empty pixels, ra_hp2',len(dec_hp2))
	#tmpradec      = np.zeros((len(ra_hp2),2))

	#tmpradec[:,0] = ra_hp2
	#tmp = (tmpradec[:,0]>270)
	#tmpradec[tmp,0]-= 360.
	#tmpradec[:,1] = dec_hp2
	#des_survey           = DESpoly.contains_points(tmpradec)

	#des_survey    = DESpoly.contains_points(tmpradec)
	#unique_healpix_32_indexes_in_DES = unique_healpix_32_indexes[des_survey]
	## 
	#N_pixels_with_targets = len(unique_healpix_32_indexes)
	#N_pixels_with_targets_in_DES = len(unique_healpix_32_indexes_in_DES)
	#print('N pixels outside DES, and inside',N_pixels_with_targets, N_pixels_with_targets_in_DES)
	#print('Area outside DES, and inside',N_pixels_with_targets*area_per_pixel, N_pixels_with_targets_in_DES*area_per_pixel)
	#norm_per_pixel = 1./(area_per_pixel*N_pixels_with_targets)
	#norm_high = norm_per_pixel * 1.2
	#norm_low = (1. - norm_high*N_pixels_with_targets_in_DES)/(N_pixels_with_targets-N_pixels_with_targets_in_DES)
	
	#lsm_values[unique_healpix_32_indexes_in_DES] = norm_low
	#print('unique lsm values',np.unique(lsm_values))
	print('sum of LSM values:',np.sum(lsm_values))
	#########
	#########
	# 3. Plots the LSM values over RA and DEC
	#########
	#########
	plt.figure(0, (6,6))
	ok = lsm_values>0
	#plt.scatter(ra_hp, dec_hp, c=np.log10(lsm_values), s=4, marker='s', edgecolors='face')
	plt.scatter(ra_hp[ok], dec_hp[ok], c=np.log10(lsm_values[ok]), s=4, marker='s', edgecolors='face')
	cb = plt.colorbar(shrink=0.8)
	cb.set_label('LSM')
	plt.grid()
	plt.ylim((-90,40))
	plt.xlim((-1,361))
	plt.xlabel('R.A.')
	plt.ylabel('Dec')
	plt.title(sub_survey_name)
	plt.savefig(plot_file)
	plt.clf()
	#########
	#########
	# 4. Create the fits LSM file
	#########
	#########
	cols = fits.ColDefs([
		fits.Column( "healpix_id"	 ,unit='' ,format="K", array=pix_ids), 
		fits.Column( "RA"            ,unit='deg' ,format="D", array=ra_hp ), 
		fits.Column( "DEC"	         ,unit='deg' ,format="D", array=dec_hp), 
		fits.Column( "LSM"           ,unit='' ,format="D", array=lsm_values)
		])
	tbhdu = fits.BinTableHDU.from_columns(cols)
	tbhdu.header['author'] = 'JC'
	tbhdu.header['HIERARCH SUBSURVEY'] = sub_survey_name
	tbhdu.header['NSIDE'] = NSIDE
	tbhdu.header['COORD'] = 'equatorial'
	tbhdu.header['NESTED'] = 'True'
	if os.path.isfile(out_file):
		os.remove(out_file)
	tbhdu.writeto(out_file)

for sub_survey_name, selection in zip(sub_survey_names, selections):
	print(sub_survey_name)
	create_LSM(sub_survey_name, selection)
