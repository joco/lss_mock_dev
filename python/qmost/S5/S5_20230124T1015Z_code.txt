
The target selection code is complex for S0501 and S0502

We use eromapper :
https://github.com/ygolomsochtiwsretsulc/eromapper
that mostly wraps around redmapper :
https://github.com/erykoff/redmapper

Then we trim the files with the code available here :
https://gitlab.mpcdf.mpg.de/joco/qmost_s5_targeting

For S0503, the selection is simpler and given here in a python function :


def SF_S0503(sweep_file_name):
  """
  input path to a legacy survey DR10 sweep files
  output selected target table
  """

  data = Table.read(sweep_file_name)
  coords = SkyCoord(data['RA'], data['DEC'], unit='deg', frame='icrs')
  bb_gal = coords.galactic.b.value
  ll_gal = coords.galactic.l.value
  data['bii'] = bb_gal
  data['lii'] = ll_gal

  data_Gal_center = ( data['bii']>0 ) & ( data['bii']<33 ) & (data['lii']>345 )
  data_Gal_plane  = (abs(data['bii'])<20)

  not_psf_type = (data['TYPE'] != "PSF")&(data['PARALLAX']==0)
  not_in_masks = (data['MASKBITS'] & (0 | 1 | 4 | 7 | 8 | 10 | 11 | 13)) == 0
  not_in_fitbs = (data['FITBITS'] & (5 | 6 | 7 | 8 | 12)) == 0
  low_ext = (data['EBV']<0.15)&(data_Gal_center==False)&(data_Gal_plane==False)& (data['lii']>179)

  zeropoint = 22.5
  data['g_mag']  = zeropoint - 2.5 * n.log10(data['FLUX_G']/data['MW_TRANSMISSION_G'])
  data['r_mag']  = zeropoint - 2.5 * n.log10(data['FLUX_R']/data['MW_TRANSMISSION_R'])
  data['i_mag']  = zeropoint - 2.5 * n.log10(data['FLUX_I']/data['MW_TRANSMISSION_I'])
  data['z_mag']  = zeropoint - 2.5 * n.log10(data['FLUX_Z']/data['MW_TRANSMISSION_Z'])
  data['w1_mag'] = zeropoint - 2.5 * n.log10(data['FLUX_W1']/data['MW_TRANSMISSION_W1'])

  selection = lambda z_max : (data['z_mag']<z_max)&(not_psf_type)&(not_in_masks)&(low_ext)&(not_in_fitbs)

  #for z_max in n.arange(17, 21, 0.1):
    #print('mag z<', n.round(z_max, 1), ', N=', nl(selection(z_max)), ', ', n.round(nl(selection(z_max))/25., 2))
  t_out = data[selection(18.5)]
  t_out.write( os.path.join( outdir, 'S0503_'+os.path.basename(sweep_file_name) ), overwrite = True)
  print(sweep_file_name, 'written, N targets=', len(t_out), time.time()-t0, 'seconds')
  return t_out
