#!/bin/bash
python S5_Cut_catalogue_fibermag.py
python S5_shared_targets.py
cd ~/sf_Shared/data/4most/S5
gzip S5_merged_17Jan2023_FIBERMAGcuts.fits

python S5_LSM_create_function.py
python S5_SSM_create_function.py
python S5_generate_sub_survey_params_table.py
python S5_tabulate_NZ.py
python S5_sky_plot.py 
# python S5_plot_ETC.py
# python S5_plot_ETC_templates.py
#
# ln -s S5_AREQ_cluster_BCG_18Mar2022.fits
# ln -s S5_AREQ_cluster_redGAL_18Mar2022.fits
# ln -s S5_AREQ_filament_GAL_18Mar2022.fits
#
# ln -s S5_LSM_cluster_BCG_18Mar2022.fits
# ln -s S5_LSM_cluster_redGAL_18Mar2022.fits
# ln -s S5_LSM_filament_GAL_18Mar2022.fits
#
# ln -s S5_SSM_cluster_BCG_18Mar2022.fits
# ln -s S5_SSM_cluster_redGAL_18Mar2022.fits
# ln -s S5_SSM_filament_GAL_18Mar2022.fits
#
# ln -s S5_TMAX_cluster_BCG_18Mar2022.fits
# ln -s S5_TMAX_cluster_redGAL_18Mar2022.fits
# ln -s S5_TMAX_filament_GAL_18Mar2022.fits
#
# ln -s S5_SUBSURVEY_PARAMS_18Mar2022.fits



========================================================================
NOTE: PLEASE CUT AND PASTE THE ABOVE FORM AND ADDRESS THE QUESTIONS
SEPARATELY FOR EACH SUB SURVEY, INCREMENTING THE SECTION NUMBER BY 1
FOR EACH QUESTION BY SUBSEQUENT SUB SURVEY
========================================================================

Note: guidance on target-selection code, self-shared targets, and astrometric
verification to be appended as for the previous version of this template.
==================================================
Target-selection code:
Please include here *one* file that explains how you selected your
input catalogue.

This file should preferably be a text (ascii/utf8) file because it makes
it easy to find out differences between submissions. This file could be:
simple text that describes the selection or contains the reference or URL to a paper
an SQL or ADQL query that can be run in an archive interface
a python code which can be run on a catalogue (e.g. Gaia, Sloan)
If your selection code is contained in a wrapper format (jupyter notebook,
mathematica notebook): please paste it into a text file and submit it
this way. This is also true for anything that may be defined in a binary
file.

If your survey contains many individual sub-surveys, and they are all
created separately, you may also attach separate codes and provide
them together as a zip or tar file. In this is the case, please also
ensure that each file adheres to the same unique filename structure for
your upload but include also the sub-survey number, e.g., S6_01... for
survey 6 and sub-survey 1.
==================================================
Self-Shared Targets
Surveys must pass their input catalogues through the shared targets
identification tool, to check that all shared targets across subsurveys
are as expected.

In particular, surveys should check the existence of self-shared targets
within individual subsurveys, as these should not exist in a well-designed
catalogue.

No direct action is demanded based on the outputs of the test. However,
the outputs of the tool will provide useful outputs to either:
Confirm that the number of shared targets are as expected; or,
If more shared targets have been identified than expected, produce
some diagnostic plots that show the distribution of shared targets,
the subsurveys over which shared targets have been identified, the
comparison of catalogue properties for shared targets, and the
identification of template mismatched for shared targets.
The shared targets identification tool can be found here:
https://gitlab.4most.eu/iwg1/shared-targets,
with documentation available here:
https://escience.aip.de/readthedocs/iwg1/shared-targets/main/index.html.

The example.py code walks users through an example of how to run the
process over a catalogue formatted in accordance with the 4FS catalogue
format.
==================================================
Astrometric Verification
Surveys who select 4MOST targets from non-Gaia or non-Legacy Survey parent
catalogues must provide evidence for the astrometric precision of these parent
catalogues.

IWG1 provides a software tool to crossmatch the astrometry of the parent catalogue
with the Gaia tables that define the International Celestial Reference Frame
(“frame_rotator_source” ~ 400 000 QSOs - primary astrometric sources and
“agn_cross_id“ ~ 1 600 000 primary and secondary astrometric sources). The
“agn_cross_id” table contains all sources from the “frame_rotator_source” table.
These astrometric reference catalogues are included with the 4MOST astrometric
verification tool.

The astrometric verification tool is available here:
https://gitlab.4most.eu/iwg1/astrometry,
with documentation available here:
https://escience.aip.de/readthedocs/iwg1/astrometry/master/verification/.

Surveys who need to provide evidence for the astrometric precision of their
parent catalogues must upload an astrometric verification file in fits format
using the 4FS-WI. This fits file is a standard output of the 4MOST astrometric
verification tool. It contains the crossmatched sources between the input parent
catalogue and the reference Gaia catalogue with on-sky separations, as well as
RA and DEC offsets, between the two catalogues for each common source. The
header of the astrometric verification file contains information about the matched
catalogues and various match statistics figures.

It may be necessary to upload more than one astrometric verification file in case
you select 4MOST targets from several different parent catalogues that require
astrometric verification.

The example.py code walks you through the astrometric verification process using
an extract from the NOAO Source Catalogue DR2 as an example.
