"""
Creates a fits catalog containing the 4FS input columns.

coordinate cuts (-70<dec<20 & |g_lat|>10)
r_AB<23.0
and 3 z cuts:
z>0.55
z>1.04
z>2.58

ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_CP_4MOST/*000???.fit > lists/cp_4most_agn_all.lis
python concat_file_list_basic.py lists/cp_4most_agn_all.lis /data37s/simulation_1/MD/MD_1.0Gpc/cat_CP_4MOST.fits         

"""
import glob
import sys
from astropy_healpix import healpy
import os
from scipy.stats import scoreatpercentile
import pandas as pd  
from scipy.special import erf
from astropy.coordinates import SkyCoord
import astropy.constants as cc
import astropy.io.fits as fits
from astropy.table import Table, Column
import astropy.units as u
import numpy as n
import extinction
print('CREATES 4FS FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')

env = "MD10"

dir_2_eRO_all = os.path.join(os.environ[env], "cat_AGN-MAG_all")
dir_2_4MOST = os.path.join(os.environ[env], "cat_CP_4MOST")
if os.path.isdir(dir_2_4MOST) == False:
    os.system('mkdir -p ' + dir_2_4MOST)

area_per_cat = healpy.nside2pixarea(8, degrees=True)
N_pixels = healpy.nside2npix(8)
N_subsurvey = {'AGN_ABS':1 }

def compute_pixel(HEALPIX_id):
	#HEALPIX_id = 350
	print(HEALPIX_id)
	path_2_eRO_all_catalog = os.path.join(dir_2_eRO_all, str(HEALPIX_id).zfill(6) + '.fit')
	path_2_AGN_ABS_catalog = os.path.join( dir_2_4MOST, 'AGN_ABS_' + str(HEALPIX_id).zfill(6) + '.fit')

	bitlist = {'AGN_ABS':0 }
	priority_values = {'AGN_ABS':100 }

	# reassigns templates correctly
	z_all = n.hstack(( 0., n.arange(0.3, 3., 0.2), 3.5, 4.5, 6. ))
	zmins = z_all[:-1]
	zmaxs = z_all[1:]

	hd_all = fits.open(path_2_eRO_all_catalog)
	N_agn_all = len(hd_all[1].data['ra'])

	# =============================
	# =============================
	# apply selection cuts
	# =============================
	# =============================
	# X-ray
	# area cut
	detected_all = (hd_all[1].data['dec'] > -70) & (hd_all[1].data['dec'] < 20) & ( abs(hd_all[1].data['g_lat']) > 10)
	# optical cut
	opt_all = ( hd_all[1].data['SDSS_r_AB_attenuated'] < 23.0 ) & ( hd_all[1].data['redshift_R'] > 0.55 )
	# overall erosita selections
	AGN_ABS_all = ( detected_all ) & ( opt_all )

	survbit_all = n.zeros(len(hd_all[1].data['dec']),dtype='int')
	# subsurvey selections 
	# assign target bits
	survbit_all[AGN_ABS_all] += 2**bitlist['AGN_ABS']
	#
	target_bit = survbit_all[AGN_ABS_all]
	# Now create a 4MOST file per sub survey
	# merge cen + sat
	def get_table(subsurvey):
		t_survey = Table()
		bit_value = bitlist[subsurvey]
		sel = ( target_bit & 2**bit_value != 0)

		def create_column(col_name):
			return hd_all[1].data[col_name][AGN_ABS_all][sel]

		ra_array = create_column('ra')
		dec_array = create_column('dec')
		N_obj = len(ra_array)
		if N_obj>0:
			N1 = n.arange(N_obj)
			id_list = N_subsurvey[subsurvey]*1e10 + HEALPIX_id*1e6 + N1
			NAME = n.array([ str(int(el)).zfill(11) for el in id_list ])
			t_survey.add_column(Column(name='NAME', data=NAME, unit=''))
			t_survey.add_column(Column(name='RA', data=ra_array, unit='deg'))
			t_survey.add_column(Column(name='DEC', data=dec_array, unit='deg'))
			PMRA = n.zeros(N_obj)
			t_survey.add_column(Column(name='PMRA', data=PMRA, unit='mas/yr'))
			PMDEC = n.zeros(N_obj)
			t_survey.add_column(Column(name='PMDEC', data=PMDEC, unit='mas/yr'))
			EPOCH = n.ones(N_obj)*2000.
			t_survey.add_column(Column(name='EPOCH', data=EPOCH, unit='yr'))
			# 'RESOLUTION':n.int16, 1I
			RESOLUTION = n.ones(N_obj).astype('int')
			t_survey.add_column(Column(name='RESOLUTION', data=RESOLUTION, unit=''))

			SUBSURVEY = n.ones(N_obj).astype('str')
			SUBSURVEY[:] = subsurvey
			t_survey.add_column(Column(name='SUBSURVEY', data=SUBSURVEY, unit=''))
			# 'PRIORITY':n.int16, 1I
			PRIORITY = n.zeros(N_obj).astype('int') + priority_values[subsurvey]
			t_survey.add_column(Column(name='PRIORITY', data=PRIORITY, unit=''))

			galactic_ebv_array = create_column('ebv')
			t_survey.add_column(Column(name='REDDENING',data=galactic_ebv_array, unit='mag'))

			# REDDENING for templates
			ebv_1000 = (t_survey['REDDENING']*1000).astype('int')
			#print('EBV', n.min(ebv_1000), n.max(ebv_1000))
			ebv_1_0 = ( ebv_1000 > 1000 ) 
			ebv_0_5 = ( ebv_1000 > 500 ) & ( ebv_1000 <= 1000 ) 
			ebv_0_4 = ( ebv_1000 > 400 ) & ( ebv_1000 <= 500 ) 
			ebv_0_3 = ( ebv_1000 > 300 ) & ( ebv_1000 <= 400 ) 
			ebv_0_2 = ( ebv_1000 > 200 ) & ( ebv_1000 <= 300 ) 
			ebv_0_1 = ( ebv_1000 > 100 ) & ( ebv_1000 <= 200 ) 
			ebv_0_0 = ( ebv_1000 <= 100 ) 
			z_name = lambda z0, z1 : "_zmin_"+str(int(10*z0)).zfill(2)+"_zmax_"+str(int(10*z1)).zfill(2)
			# templates
			template_names = n.zeros(N_obj).astype('U100')
			#
			ruleset_array = n.zeros(N_obj).astype('str')
			# 
			if subsurvey == "AGN_ABS" :
				ruleset_array[:] = "AGN_ALL_3PC"

			AGN_type_array = create_column('agn_type')
			AGN_random_number_array = create_column('random')
			z_array = create_column('redshift_R')

			QSO = (AGN_type_array == 11) | (AGN_type_array == 12)
			T2 = (AGN_type_array == 22) | (AGN_type_array == 21)
			ELL = (T2) & (AGN_random_number_array < 0.2)

			for z0, z1 in zip(zmins, zmaxs):
				zsel = (z_array >= z0) & (z_array < z1)
				template_names[(zsel)] = "4most_" + 'qso_BL' +  z_name(z0, z1) + '_EBV_0_01.fits'
				template_names[(QSO) & (zsel) & (ebv_0_0)] = "4most_" +  'qso_BL' + z_name(z0, z1) + '_EBV_0_01.fits'
				template_names[(QSO) & (zsel) & (ebv_0_1)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_1.fits'
				template_names[(QSO) & (zsel) & (ebv_0_2)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_2.fits'
				template_names[(QSO) & (zsel) & (ebv_0_3)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_3.fits'
				template_names[(QSO) & (zsel) & (ebv_0_4)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_4.fits'
				template_names[(QSO) & (zsel) & (ebv_0_5)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_5.fits'
				template_names[(QSO) & (zsel) & (ebv_1_0)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_1_0.fits'
				if z1 < 2.2:
					template_names[(T2) & (zsel) & (ebv_0_0)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_01.fits'
					template_names[(T2) & (zsel) & (ebv_0_1)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_1.fits'
					template_names[(T2) & (zsel) & (ebv_0_2)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_2.fits'
					template_names[(T2) & (zsel) & (ebv_0_3)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_3.fits'
					template_names[(T2) & (zsel) & (ebv_0_4)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_4.fits'
					template_names[(T2) & (zsel) & (ebv_0_5)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_5.fits'
					template_names[(T2) & (zsel) & (ebv_1_0)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_1_0.fits'

					template_names[(ELL) & (zsel) & (ebv_0_0)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_01.fits'
					template_names[(ELL) & (zsel) & (ebv_0_1)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_1.fits'
					template_names[(ELL) & (zsel) & (ebv_0_2)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_2.fits'
					template_names[(ELL) & (zsel) & (ebv_0_3)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_3.fits'
					template_names[(ELL) & (zsel) & (ebv_0_4)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_4.fits'
					template_names[(ELL) & (zsel) & (ebv_0_5)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_5.fits'
					template_names[(ELL) & (zsel) & (ebv_1_0)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_1_0.fits'
				if z1 >= 2.2 and z1 < 6.:
					template_names[(T2) & (zsel) & (ebv_0_0)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_01.fits'
					template_names[(T2) & (zsel) & (ebv_0_1)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_1.fits'
					template_names[(T2) & (zsel) & (ebv_0_2)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_2.fits'
					template_names[(T2) & (zsel) & (ebv_0_3)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_3.fits'
					template_names[(T2) & (zsel) & (ebv_0_4)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_4.fits'
					template_names[(T2) & (zsel) & (ebv_0_5)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_5.fits'
					template_names[(T2) & (zsel) & (ebv_1_0)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_1_0.fits'
					template_names[(ELL) & (zsel) & (ebv_0_0)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_01.fits'
					template_names[(ELL) & (zsel) & (ebv_0_1)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_1.fits'
					template_names[(ELL) & (zsel) & (ebv_0_2)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_2.fits'
					template_names[(ELL) & (zsel) & (ebv_0_3)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_3.fits'
					template_names[(ELL) & (zsel) & (ebv_0_4)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_4.fits'
					template_names[(ELL) & (zsel) & (ebv_0_5)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_5.fits'
					template_names[(ELL) & (zsel) & (ebv_1_0)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_1_0.fits'

			# 'TEMPLATE':str, max 256 char
			t_survey.add_column(Column(name='TEMPLATE', data=template_names, unit=''))
			# 'RULESET':str, max 256 char
			t_survey.add_column(Column(name='RULESET', data=ruleset_array, unit=''))
			# 'REDSHIFT_ESTIMATE':n.float32, 1E
			# 'REDSHIFT_ERROR':n.float32, 1E
			t_survey.add_column(Column(name='REDSHIFT_ESTIMATE', data=z_array, unit=''))
			t_survey.add_column(Column(name='REDSHIFT_ERROR', data=n.ones(N_obj), unit=''))
			# 'EXTENT_FLAG': 1I
			# =1
			# 'EXTENT_PARAMETER': 1E
			# =0
			# 'EXTENT_INDEX': 1E
			# =0
			t_survey.add_column(Column(name='EXTENT_FLAG'     , data=n.zeros(N_obj).astype('int') , unit=''))
			t_survey.add_column(Column(name='EXTENT_PARAMETER', data=n.zeros(N_obj), unit=''))
			t_survey.add_column(Column(name='EXTENT_INDEX'    , data=n.zeros(N_obj), unit=''))
			# 'MAG':n.float32,
			# 'MAG_ERR':n.float32
			# 'MAG_TYPE': str max 256 char
			HSC_RMAG_array = create_column('HSC-r')
			HSC_RMAGERR_array = create_column('HSC-r_err')
			#
			r_v=3.1
			a_v = galactic_ebv_array * r_v
			delta_mag = n.hstack(( n.array([ extinction.fitzpatrick99(n.array([6500.]), el, r_v=3.1, unit='aa') for el in a_v ]) ))
			#rv = av/ebv
			#av = rv x ebv
			extincted_mag = HSC_RMAG_array + delta_mag
			t_survey.add_column(Column(name='MAG', data=extincted_mag, unit='mag'))
			t_survey.add_column(Column(name='MAG_ERR', data=HSC_RMAGERR_array, unit='mag'))
			MAG_TYPE = n.ones(N_obj).astype('str')
			MAG_TYPE[:] = 'DECam_r_AB'
			t_survey.add_column(Column(name='MAG_TYPE', data=MAG_TYPE, unit=''))
			# 'REDDENING':n.float32, 1E
			# 'DATE_EARLIEST':n.float64, JulianDate decimal days # 01-Nov-2022
			# 'DATE_LATEST':n.float64, JulianDate decimal days # 02-Feb-2033
			t_survey.add_column(Column(name='DATE_EARLIEST',data=22305 * n.ones(N_obj), unit='d'))
			t_survey.add_column(Column(name='DATE_LATEST'  ,data=33033 * n.ones(N_obj), unit='d'))
			t_survey.add_column(Column(name='CADENCE'  ,data= n.zeros(N_obj).astype('int64'), unit=''))

			t_survey.add_column(Column(name='target_bit'  ,data=target_bit[sel], unit=''))

			t_survey.add_column(Column(name='dL_cm'  ,data=create_column('dL'), unit='cm'))
			t_survey.add_column(Column(name='galactic_NH'  ,data=create_column('nH'), unit=''))
			t_survey.add_column(Column(name='galaxy_stellar_mass'  ,data=create_column('galaxy_SMHMR_mass'), unit=''))
			t_survey.add_column(Column(name='HALO_Mvir'  ,data=create_column('HALO_Mvir'), unit=''))
			t_survey.add_column(Column(name='AGN_LX_soft'  ,data=create_column('LX_soft'), unit=''))
			t_survey.add_column(Column(name='AGN_FX_soft'  ,data=create_column('FX_soft'), unit=''))
			t_survey.add_column(Column(name='AGN_LX_hard'  ,data=create_column('LX_hard'), unit=''))
			t_survey.add_column(Column(name='AGN_FX_hard'  ,data=create_column('FX_hard'), unit=''))
			t_survey.add_column(Column(name='AGN_nH'  ,data=create_column('logNH'), unit=''))
			t_survey.add_column(Column(name='WISE-W1'  ,data=create_column('WISE-W1'), unit=''))
			t_survey.add_column(Column(name='AGN_SDSS_r_magnitude'  ,data=create_column('SDSS_r_AB_attenuated'), unit=''))
			t_survey.add_column(Column(name='g_lat'  ,data=create_column('g_lat'), unit='deg'))
			t_survey.add_column(Column(name='g_lon'  ,data=create_column('g_lon'), unit='deg'))
			t_survey.add_column(Column(name='ecl_lat'  ,data=create_column('ecl_lat'), unit='deg'))
			#t_survey.add_column(Column(name=''  ,data=create_column(''), unit=''))
			return t_survey
		else:
			return 0.

	subsurvey = 'AGN_ABS'
	print(subsurvey)
	t_out = get_table(subsurvey)
	if t_out != 0:
		t_out.write (path_2_AGN_ABS_catalog  , overwrite=True)
		print(subsurvey, 'N/deg2', len(t_out['RA'])/area_per_cat)

for HEALPIX_id in n.arange(N_pixels):
	compute_pixel(HEALPIX_id)
