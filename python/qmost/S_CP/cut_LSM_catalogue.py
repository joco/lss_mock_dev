"""
Creates a fits catalog containing the 4FS input columns.

coordinate cuts (-70<dec<20 & |g_lat|>10)
r_AB<23.0
and 3 z cuts:
z>0.55
z>1.04
z>2.58

ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_CP_4MOST/*000???.fit > lists/cp_4most_agn_all.lis
python concat_file_list_basic.py lists/cp_4most_agn_all.lis /data37s/simulation_1/MD/MD_1.0Gpc/cat_CP_4MOST.fits         

"""
import glob
import sys
from astropy_healpix import healpy
import os
from scipy.stats import scoreatpercentile
import pandas as pd  
from scipy.special import erf
from astropy.coordinates import SkyCoord
import astropy.constants as cc
import astropy.io.fits as fits
from astropy.table import Table, Column
import astropy.units as u
import numpy as n
print('CREATES 4FS FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')

Deep_LSM = fits.open('MgII_Blue_Deep_LSM.fits')  [1].data
Wide_LSM = fits.open('MgII_Blue_Wide_LSM.fits')  [1].data
hdu = Table.read('/home/comparat/data2/firefly/mocks/2020-06/MDPL2/for_CP/cat_CP_4MOST.fits')

HEALPIX_32 = healpy.ang2pix(32, n.pi/2. - hdu['DEC']*n.pi/180. , hdu['RA']*n.pi/180. , nest=True)
HEALPIX_32_F = healpy.ang2pix(32, n.pi/2. - hdu['DEC'].astype('float16')*n.pi/180. , hdu['RA'].astype('float16')*n.pi/180. , nest=True)

identical = (HEALPIX_32 == HEALPIX_32_F)
diff = (identical == False)
len(diff.nonzero()[0])

dd = Deep_LSM['LSM'][HEALPIX_32]
ww = Wide_LSM['LSM'][HEALPIX_32]
dd2 = Deep_LSM['LSM'][HEALPIX_32_F]
ww2 = Wide_LSM['LSM'][HEALPIX_32_F]
in_footprint = (dd>0)|(ww>0)
in_footprint2 = (dd2>0)|(ww2>0)
common = (in_footprint) & (in_footprint2)
print(len(in_footprint.nonzero()[0]), len(in_footprint2.nonzero()[0]), len(common.nonzero()[0]) )

din_footprint = (dd>0)
din_footprint2 = (dd2>0)
dcommon = (din_footprint) & (din_footprint2)
print(len(din_footprint.nonzero()[0]), len(din_footprint2.nonzero()[0]), len(dcommon.nonzero()[0]) )


t_out = Table(hdu[dcommon])
t_out.write ('/home/comparat/data2/firefly/mocks/2020-06/MDPL2/for_CP/cat_CP_4MOST_LSMfiltered_DEEP.fits'  , overwrite=True)

t_out = Table(hdu[common])
t_out.write ('/home/comparat/data2/firefly/mocks/2020-06/MDPL2/for_CP/cat_CP_4MOST_LSMfiltered.fits'  , overwrite=True)

#chmod ugo+rX /home/comparat/data2/firefly/mocks/2020-06/MDPL2/for_CP/*




Deep_LSM = fits.open('/home/comparat/software/lss_mock_dev/python/qmost/S_CP/MgII_Blue_Deep_LSM.fits')  [1].data
Wide_LSM = fits.open('/home/comparat/software/lss_mock_dev/python/qmost/S_CP/MgII_Blue_Wide_LSM.fits')  [1].data
hdu = Table.read('outcat_CP_4MOST_LSMfiltered.fits')

HEALPIX_32 = healpy.ang2pix(32, n.pi/2. - hdu['DEC']*n.pi/180. , hdu['RA']*n.pi/180. , nest=True)
HEALPIX_32_F = healpy.ang2pix(32, n.pi/2. - hdu['DEC'].astype('float16')*n.pi/180. , hdu['RA'].astype('float16')*n.pi/180. , nest=True)

identical = (HEALPIX_32 == HEALPIX_32_F)
diff = (identical == False)
len(diff.nonzero()[0])

#dd = Deep_LSM['LSM'][HEALPIX_32]
ww = Wide_LSM['LSM'][HEALPIX_32]
#dd2 = Deep_LSM['LSM'][HEALPIX_32_F]
ww2 = Wide_LSM['LSM'][HEALPIX_32_F]
in_footprint = (ww>0)
in_footprint2 = (ww2>0)
common = (in_footprint) & (in_footprint2)
print(len(HEALPIX_32), len(in_footprint.nonzero()[0]), len(in_footprint2.nonzero()[0]), len(common.nonzero()[0]) )

in_footprint = (dd>0)|(ww>0)
in_footprint2 = (dd2>0)|(ww2>0)
common = (in_footprint) & (in_footprint2)
print(len(in_footprint.nonzero()[0]), len(in_footprint2.nonzero()[0]), len(common.nonzero()[0]) )


din_footprint = (dd>0)
din_footprint2 = (dd2>0)
dcommon = (din_footprint) & (din_footprint2)
print(len(din_footprint.nonzero()[0]), len(din_footprint2.nonzero()[0]), len(dcommon.nonzero()[0]) )


t_out = Table(hdu[dcommon])
t_out.write ('/home/comparat/data2/firefly/mocks/2020-06/MDPL2/for_CP/cat_CP_4MOST_LSMfiltered_DEEP.fits'  , overwrite=True)

t_out = Table(hdu[common])
t_out.write ('/home/comparat/data2/firefly/mocks/2020-06/MDPL2/for_CP/cat_CP_4MOST_LSMfiltered.fits'  , overwrite=True)
