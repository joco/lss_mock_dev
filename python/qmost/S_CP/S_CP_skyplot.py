import os, glob, sys
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm 
from matplotlib import rc

from astropy.io import fits
from astropy.coordinates import SkyCoord
import astropy.units as u
import numpy as np
import healpy as hp


def mk_radec_plot(ra,dec,nside=32,filename=' ', title=''):
    #
    # ra,dec in degrees
    # nside is the healpix parameter setting the size of pixels, must be a power of 2, less than 2**30
    #
	npix = hp.nside2npix(nside)
	pixarea = hp.nside2pixarea(nside, degrees = True)
	print("Pixel area [deg^2]: ",pixarea)
	# convert ra,dec to HEALPix indices and count
	indices = hp.ang2pix(nside, ra, dec, lonlat=True)
	idx, counts = np.unique(indices, return_counts=True)
	# fill the fullsky map
	hpx_map = np.zeros(npix, dtype=int)
	hpx_map[idx]      = counts/pixarea
	# set some fonts
	rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
	## for Palatino and other serif fonts use:
	#rc('font',**{'family':'serif','serif':['Palatino']})
	#rc('text', usetex=True)
	#rc('mathtext.fontset','stixsans')
	# define colour map
	cmap = plt.cm.inferno
	cmap.set_bad('w')
	cmap.set_under('w')
	# make the map with meridians overplotted
	hp.mollview(hpx_map, 
				coord='C', rot=(180,0),
				unit=r' Object counts per degree$^2$', xsize = 1000, cmap=cmap, min=1, norm='log', cbar=None)
	hp.graticule(dpar=15, dmer=20, verbose= True)
	# HA labels
	params = {'mathtext.default': 'regular' }          
	plt.rcParams.update(params)
	#hp.projtext(20,  32,  '$\mathrm{\mathsf{0^h}}$',     color = 'black', fontsize = 16, lonlat=True)
	#hp.projtext(88,  32,  '$\mathrm{\mathsf{6^h}}$',     color = 'black', fontsize = 16, lonlat=True)
	#hp.projtext(177, 32,  '$\mathrm{\mathsf{12^h}}$',    color = 'black', fontsize = 16, lonlat=True) #rotation
	#hp.projtext(264, 32,  '$\mathrm{\mathsf{18^h}}$',    color = 'black', fontsize = 16, lonlat=True)
	#hp.projtext(351, 32,  '$\mathrm{\mathsf{24^h}}$',    color = 'black', fontsize = 16, lonlat=True)
	hp.projtext(20,  32,  '$\mathrm{\mathsf{0^\circ}}$',     color = 'black', fontsize = 16, lonlat=True)
	hp.projtext(88,  32,  '$\mathrm{\mathsf{90^\circ}}$',     color = 'black', fontsize = 16, lonlat=True)
	hp.projtext(177, 32,  '$\mathrm{\mathsf{180^\circ}}$',    color = 'black', fontsize = 16, lonlat=True) #rotation
	hp.projtext(264, 32,  '$\mathrm{\mathsf{270^\circ}}$',    color = 'black', fontsize = 16, lonlat=True)
	hp.projtext(351, 32,  '$\mathrm{\mathsf{360^\circ}}$',    color = 'black', fontsize = 16, lonlat=True)
	plt.title(' ', size=14)
	ax = plt.gca()
	im = ax.get_images()[0]
	# DEC labels
	fig= plt.gcf()
	for ax in fig.get_axes():
		if type(ax) is hp.projaxes.HpxMollweideAxes:
			ax.set_ylim(-1, 0.51)
			ax.set_position([0.02, 0.03, 0.94, 0.95])
			ax.annotate(r'$\, \mathrm{\mathsf{ 0^\circ}}$', xy=(2.04,-0.02),    size=16, annotation_clip=False) 
			ax.annotate(r'$\, \mathrm{\mathsf{ 30^\circ}}$', xy=(1.90,0.38),     size=16) 
			ax.annotate(r'$\, \mathrm{\mathsf{-30^\circ}}$', xy=(1.86,-0.42),    size=16) 
			ax.annotate(r'$\, \mathrm{\mathsf{-60^\circ}}$', xy=(1.38, -0.78),   size=16) 
			ax.annotate(r'%s targets' %len(ra), xy=(-2.0, -0.92),  size=14) # optional, can be removed

	# create colour bar
	cbaxes = fig.add_axes([0.1, 0.15, 0.8, 0.04]) # [left, bottom, width, height]
	tks=[1, 10, 100, 1000, 10000, 100000, 1000000, 10000000]
	cb = plt.colorbar(im,  orientation='horizontal', cax = cbaxes, ticks=tks)             
	cb.set_label('Object counts per degree$\mathrm{\mathsf{^2}}$', fontsize=14)
	cb.ax.tick_params(labelsize=16)
	#plt.xlim([0,1])
	# save plot to PDF file
	plt.savefig(filename + '.pdf')
	plt.savefig(filename + '.png')

catalogue_dir = '/home/comparat/data2/firefly/mocks/2020-06/MDPL2/for_CP'

data = fits.open(os.path.join(catalogue_dir, 'cat_CP_4MOST_LSMfiltered_DEEP.fits'))[1].data

ra  = data['RA']
dec = data['DEC']
mk_radec_plot(ra,dec,filename= os.path.join(catalogue_dir, 'sky_deep'), title='wide' )

data = fits.open(os.path.join(catalogue_dir, 'cat_CP_4MOST_LSMfiltered.fits'))[1].data

ra  = data['RA']
dec = data['DEC']
mk_radec_plot(ra,dec,filename= os.path.join(catalogue_dir, 'sky_wide'), title='wide' )
