import os, glob, sys
from astropy.io import fits
from astropy.coordinates import SkyCoord
import astropy.units as u
import numpy as np
import healpy as hp
from astropy.table import Table, Column, vstack, hstack


working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'qmost', 'S6')
catalogue_dir = os.path.join(os.environ['HOME'], 'sf_Shared/data/4most', 's6', '2023_march')
out_name1 = os.path.join(catalogue_dir, 'S6_09Mar2023.fits')


data1 = Table.read(os.path.join(catalogue_dir, 'S6_20230124T1832Z_target_catalogue.fits' ))
data2 = Table.read(os.path.join(catalogue_dir, 'PAQS_catalog_20220303', 'PAQS_catalog_20230303T162414Z.fits'))
data3 = Table.read(os.path.join(catalogue_dir, 'LSDR10_targets_March2023.fits'))
data4 = Table.read(os.path.join(catalogue_dir, 'S6_20230308_AGN_HIGH-Z_final_cat.fits' ))

data1 = data1[ (data1['SUBSURVEY']=="AGN_DEEP") | (data1['SUBSURVEY']=="AGN_WIDE") ]
data1['EPOCH'] = 2000.0

data2['CLASSIFICATION']="TBD"
data2['TEMPLATE'][data2['TEMPLATE']=="template_lib/powerlaw_BR+1.3_temp.fits"] = "powerlaw_BR+1.3_temp.fits"
data2['SUBSURVEY'] = "PAQS"
data2['CAL_MAG_ID_GREEN'] = "GAIA_G_VEGA_PSF"
data2['CAL_MAG_ID_BLUE'] = "GAIA_GBP_VEGA_PSF"
data2['CAL_MAG_ID_RED'] = "GAIA_GRP_VEGA_PSF"

ones = np.ones_like(data2['TEMPLATE']).astype('str')
ones[(data2['RULESET']=="PAQS_BRIGHT")] = "PAQS_LR_BRIGHT_JAN23"
ones[(data2['RULESET']=="PAQS_FAINT")] = "PAQS_LR_FAINT_JAN23"
data2['RULESET'] = ones
#print(data1.info())
#print(data2.info())
#print(data3.info())
#print(data4.info())

re_assign_z = (data3['REDSHIFT_ESTIMATE']>4.5)
data3['REDSHIFT_ESTIMATE'][re_assign_z] = 4.5

data3['COMPLETENESS'] = 0
data3['PARALLAX'] = 0

data4['SUBSURVEY']='AGN_HIGHZ'
data4['MAG_TYPE'][(data4['MAG_TYPE']=="DECam_I_AB")] = "DECam_i_AB"
data4['MAG_TYPE'][(data4['MAG_TYPE']=="DECam_Z_AB")] = "DECam_z_AB"
data4['MAG_TYPE'][(data4['MAG_TYPE']=="DECam_z_AB")] = "DECam_z_AB"
data4['CAL_MAG_ID_RED'] = "DECAM_I_AB_PSF"

data4['CLASSIFICATION']="TBD"
data4['COMPLETENESS'] = 0
data4['PARALLAX'] = 0

# merges catalogues into structure
def COL(colname):
  return np.hstack(( data1[colname].value, data2[colname].value, data3[colname].value, data4[colname].value ))

def set_49(VAL):
  VAL[(VAL<=1.)|(VAL>49.9)|np.isnan(VAL)] = 49.9
  print(np.unique(VAL))
  return VAL

def set_I_B(VAL):
  VAL[(VAL==b'0.0')|(VAL==b'6.90020378795435e-310')|(VAL==b'6.90749317182025e-310')] = "DECAM_G_AB_FIB"
  print(np.unique(VAL))
  return VAL

def set_I_G(VAL):
  VAL[(VAL==b'0.0')|(VAL==b'6.90020378795435e-310')|(VAL==b'6.90749317182025e-310')] = "DECAM_R_AB_FIB"
  print(np.unique(VAL))
  return VAL

def set_I_R(VAL):
  VAL[(VAL==b'0.0')|(VAL==b'6.90020378795435e-310')|(VAL==b'6.90749317182025e-310')] = "DECAM_I_AB_FIB"
  print(np.unique(VAL))
  return VAL


t_survey = []

t_survey.append(fits.Column(name='NAME'             , array=COL('NAME'             ) , unit=''      , format='20A') )
t_survey.append(fits.Column(name='RA'               , array=COL('RA'               ) , unit='deg'   , format='D')   )
t_survey.append(fits.Column(name='DEC'              , array=COL('DEC'              ) , unit='deg'   , format='D')   )
t_survey.append(fits.Column(name='PMRA'             , array=COL('PMRA'             ) , unit='mas/yr', format='E')   )
t_survey.append(fits.Column(name='PMDEC'            , array=COL('PMDEC'            ) , unit='mas/yr', format='E')   )
t_survey.append(fits.Column(name='EPOCH'            , array=COL('EPOCH'            ) , unit='yr'    , format='E')   )
t_survey.append(fits.Column(name='RESOLUTION'       , array=COL('RESOLUTION'       ) , unit=''      , format='I')   )
t_survey.append(fits.Column(name='SUBSURVEY'        , array=COL('SUBSURVEY'        ) , unit=''      , format='15A') )
t_survey.append(fits.Column(name='CADENCE'          , array=COL('CADENCE'          ) , unit=''      , format='K')   )
t_survey.append(fits.Column(name='TEMPLATE'         , array=COL('TEMPLATE'         ) , unit=''      , format='256A'))
t_survey.append(fits.Column(name='RULESET'          , array=COL('RULESET'          ) , unit=''      , format='30A') )
t_survey.append(fits.Column(name='REDSHIFT_ESTIMATE', array=COL('REDSHIFT_ESTIMATE') , unit=''      , format='E')   )
t_survey.append(fits.Column(name='REDSHIFT_ERROR'   , array=COL('REDSHIFT_ERROR'   ) , unit=''      , format='E')   )
t_survey.append(fits.Column(name='EXTENT_FLAG'      , array=COL('EXTENT_FLAG'      ) , unit=''      , format='I')   )
t_survey.append(fits.Column(name='EXTENT_PARAMETER' , array=COL('EXTENT_PARAMETER' ) , unit='arcsec', format='E')   )
t_survey.append(fits.Column(name='EXTENT_INDEX'     , array=COL('EXTENT_INDEX'     ) , unit=''      , format='E')   )
t_survey.append(fits.Column(name='MAG'              , array=COL('MAG'              ) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='MAG_ERR'          , array=COL('MAG_ERR'          ) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='MAG_TYPE'         , array=COL('MAG_TYPE'         ) , unit=''      , format='20A') )
t_survey.append(fits.Column(name='REDDENING'        , array=COL('REDDENING'        ) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='TEMPLATE_REDSHIFT', array=COL('TEMPLATE_REDSHIFT') , unit=''      , format='E')   )
t_survey.append(fits.Column(name='DATE_EARLIEST'    , array=COL('DATE_EARLIEST'    ) , unit='d'     , format='E')   )
t_survey.append(fits.Column(name='DATE_LATEST'      , array=COL('DATE_LATEST'      ) , unit='d'     , format='E')   )

t_survey.append(fits.Column(name='CAL_MAG_BLUE'     , array=set_49(COL('CAL_MAG_BLUE'     )) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='CAL_MAG_ERR_BLUE' , array=set_49(COL('CAL_MAG_ERR_BLUE' )) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='CAL_MAG_ID_BLUE'  , array=set_I_B(COL('CAL_MAG_ID_BLUE'  )) , unit=''      , format='20A'))
t_survey.append(fits.Column(name='CAL_MAG_GREEN'    , array=set_49(COL('CAL_MAG_GREEN'    )) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='CAL_MAG_ERR_GREEN', array=set_49(COL('CAL_MAG_ERR_GREEN')) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='CAL_MAG_ID_GREEN' , array=set_I_G(COL('CAL_MAG_ID_GREEN')) , unit=''      , format='20A') )
t_survey.append(fits.Column(name='CAL_MAG_RED'      , array=set_49(COL('CAL_MAG_RED'      )) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='CAL_MAG_ERR_RED'  , array=set_49(COL('CAL_MAG_ERR_RED'  )) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='CAL_MAG_ID_RED'   , array=set_I_R(COL('CAL_MAG_ID_RED'  )) , unit=''      , format='20A') )
t_survey.append(fits.Column(name='CLASSIFICATION'   , array=COL('CLASSIFICATION'   )      , unit=''       , format='20A') )
t_survey.append(fits.Column(name='COMPLETENESS'     , array=COL('COMPLETENESS') , unit=''       , format='E')   )
t_survey.append(fits.Column(name='PARALLAX'         , array=np.zeros_like(COL('COMPLETENESS'))     , unit='mas'    , format='E')   )


hdu = fits.BinTableHDU.from_columns(t_survey)
hdu.name = 'S06'
hdu.header['FMTDOC'] = 'VIS-MAN-4MOST-47110-1720-0001'
hdu.header['FMTVERS'] = '2.5'

outf = fits.HDUList([fits.PrimaryHDU(), hdu])
outf.writeto(out_name1 , overwrite=True)
print( out_name1, 'written')
