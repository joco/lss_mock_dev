Survey title: AGN survey

List of sub surveys:

- AGN_WIDE: wide-area eROSITA X-ray survey (S601)
- AGN_DEEP: deeper observations of bright target subsets (S602)
- AGN_IR: wide-area WISE mid-IR survey (S603)
- AGN_HIGHZ: High redshift Quasar survey (S604)
- PAQS: Purely Astrometric Quasar Survey (S605)

========================================================================

Survey (or sub survey) number: S601
Survey (or sub survey) title: AGN_WIDE
Author: Andrea Merloni
Email address: am@mpe.mpg.de

------------------------------------------------------------------------
What images and catalogues are used to construct the target catalogue? 

eROSITA eRASS:4 catalog matched to Legacy DR10 counterparts, file='file020_Nway_LS10_eRASScol4_raw_starex_all_photoz.fits' (from Mara Salvato, 14.11.2022; MPE proprietary).

------------------------------------------------------------------------
How are the target coordinates determined (e.g., central pixel, 
brightest pixel, etc)? 

Gaia-registered RA and DEC from Legacy DR10 catalog

------------------------------------------------------------------------
How is the target photometry measured? 

As measured by Legacy Dr10, depending on the best fit morphology of the sources: either PSF (-> Total Magnitude) or extended (FIBERMAG, aperture over 1.5" diameter)

------------------------------------------------------------------------
How are target photometric redshifts and associated uncertainties 
estimated (if relevant)? 

All "REDSHIFT_ESTIMATES" are photometric redshifts computed by Mara Salvato and her team using a Convolution Neural Network (CNN) method, similar to that of Dey et al. (2021). No uncertainties are provided for the time being.

------------------------------------------------------------------------
How is the foreground reddening towards the target calculated? 

With the Schlegel et al. (1998) dust maps.

------------------------------------------------------------------------
What approach and assumptions are made for the selection of the target 
spectral templates? 

We use only three families of templates: Type1 QSO, Type2 AGN and
passive galaxies. 

We assign type1 class to all objects which are
calssified as pdf from the DR10 morphology and all objects with photoz>1.

All the targets (non-PSF at photoz<1) are divided into type2 and passive galaxies using their location in the (dereddened) grzW1 plane:

(dered_mag_z-dered_mag_w1)>1.2*(dered_mag_g-dered_mag_r)-1 -> type 2
(dered_mag_z-dered_mag_w1)<1.2*(dered_mag_g-dered_mag_r)-1 -> ellipticals


Is the same target included more than once in the catalogue but with a
different spectral template and/or ruleset? If yes then please provide
a brief justification.

Not in this subsurvey.

------------------------------------------------------------------------
Any additional critical notes: 

Below a description of the full target selection workflow:

1- Keep only objects with -80<DEC<5

2- Keep only objects with (eROSITA detection likelihood in the 0.2-2.3 keV band) DET_LIKE_0>5

3- Keep only objects whose counterparts in NWAY satisfy: 
match_flag==1 & ncat>1 & p_any>0.01

4- Keep only objects with 16<FIBERMAG_R<22.8 || 16<FIBERMAG_I<22.5 

For PSF sources, convert FIBERMAG to TOTALMAG using TOTALMAG=FIBERMAG-0.273

5- Remove all “GALACTIC” targets, defined as follows:

5.1 Galactic from Gaia astrometry if: PARALLAX/PARALLAX_ERROR > 3. || PM/PM_ERROR > 3. (where PM and PM_ERROR are computed as PM=sqrt(PMRA^2 + PMDEC^2)

5.2 Galactic from grzW1 plane: if ‘dered_mag_g & dered_mag_r & dered_mag_z & dered_mag_W1’ are >0 and <99 then use the Salvato line (i.e (dered_mag_z-dered_mag_W1)<0.8*(dered_mag_g-dered_mag_r) - 1.2

A source is GALACTIC = either 5.1 or 5.2 are true 

7- REDSHIFT_ESTIMATE

take CNN photoz if 0<photoz<7. assume photoz=1 if photoz>7 or photoz<0 


6- CLASSIFICATION: 

	CLASSIFICATION = QSO if (TYPE=psf || photoz>1)
	CLASSIFICATION = AGN [Type 2 AGN template] if !QSO & (dered_mag_z-dered_mag_w1)>1.2*(dered_mag_g-dered_mag_r)-1
	CLASSIFICATION = GAL [Ellitpical tempalte] if !QSO & (dered_mag_z-dered_mag_w1)<1.2*(dered_mag_g-dered_mag_r)-1

7- MAG column: 

- for PSF sources: take TOTALMAG_R if 99>TOTALMAG_R>0, otherwise take TOTALMAG_I
- for EXTENDED sources: take FIBERMAG_R if 99>FIBERMAG_R>0, otherwise take FIBERMAG_I

MAG_ERR: derived from FLUX_IVAR_R,I (symmetric errors in Magnitude)
NOTE: all sources with FLUX_IVAR_R and I ==0. have been removed (177 objects)

For EXTENT_FLAG=1 use  SB_* (Surface brightness) computed as TOTALMAG_*+0.61818 (area of 1.45” fiber in square arcsec) as MAG entry (See point 8 below)


8- EXTENT_FLAG 
	=0 if TYPE=PSF
	=1 if TYPE=!PSF

consider giving the model or compute the fibermag using the computed aperture apflux[4] (1.5”) 

9- CAL_MAGs
	use FIBERMAG_G,R,I  from DR10 catalog

========================================================================

Survey (or sub survey) number: S602
Survey (or sub survey) title: AGN_DEEP
Author: Andrea Merloni
Email address: am@mpe.mpg.de

------------------------------------------------------------------------
What images and catalogues are used to construct the target catalogue? 
Include image and catalogue version numbers when appropriate. 

eROSITA eRASS:4 catalog matched to Legacy DR10 counterparts, file='file020_Nway_LS10_eRASScol4_raw_starex_all_photoz.fits' (from Mara Salvato, 14.11.2022; MPE proprietary).

------------------------------------------------------------------------
How are the target coordinates determined (e.g., central pixel, 
brightest pixel, etc)? 

see S6_01
------------------------------------------------------------------------
How is the target photometry measured? 

see S6_01

------------------------------------------------------------------------
How are target photometric redshifts and associated uncertainties 
estimated (if relevant)? 

see S6_01

------------------------------------------------------------------------
How is the foreground reddening towards the target calculated? 

With the Schlegel et al. (1998) dust maps.

------------------------------------------------------------------------
What approach and assumptions are made for the selection of the target 
spectral templates? 

see S6_01

------------------------------------------------------------------------
Is the same target included more than once in the catalogue but with a
different spectral template and/or ruleset? If yes then please provide
a brief justification.

Not in this subsurvey.

------------------------------------------------------------------------
Any additional critical notes: 

Below a description of the full target selection workflow:

1- Keep only objects with -80<DEC<5

2- Keep only objects with (eROSITA detection likelihood in the 0.2-2.3 keV band) DET_LIKE_0>5

3- Keep only objects whose counterparts in NWAY satisfy: 
match_flag==1 & ncat>1 & p_any>0.01

4- Keep only objects with 16<FIBERMAG_R<20.5 || 16<FIBERMAG_I<20.3 

For PSF sources, convert FIBERMAG to TOTALMAG using TOTALMAG=FIBERMAG-0.273

5- Remove all “GALACTIC” targets, defined as follows:

5.1 Galactic from Gaia astrometry if: PARALLAX/PARALLAX_ERROR > 3. || PM/PM_ERROR > 3. (where PM and PM_ERROR are computed as PM=sqrt(PMRA^2 + PMDEC^2)

5.2 Galactic from grzW1 plane: if ‘dered_mag_g & dered_mag_r & dered_mag_z & dered_mag_W1’ are >0 and <99 then use the Salvato line (i.e (dered_mag_z-dered_mag_W1)<0.8*(dered_mag_g-dered_mag_r) - 1.2

A source is GALACTIC = either 5.1 or 5.2 are true 

7- REDSHIFT_ESTIMATE

take CNN photoz if 0<photoz<7. assume photoz=1 if photoz>7 or photoz<0 


6- CLASSIFICATION: 

	CLASSIFICATION = QSO if (TYPE=psf || photoz>1)
	CLASSIFICATION = AGN [Type 2 AGN template] if !QSO & (dered_mag_z-dered_mag_w1)>1.2*(dered_mag_g-dered_mag_r)-1
	CLASSIFICATION = GAL [Ellitpical tempalte] if !QSO & (dered_mag_z-dered_mag_w1)<1.2*(dered_mag_g-dered_mag_r)-1

7- MAG column: 

- for PSF sources: take TOTALMAG_R if 99>TOTALMAG_R>0, otherwise take TOTALMAG_I
- for EXTENDED sources: take FIBERMAG_R if 99>FIBERMAG_R>0, otherwise take FIBERMAG_I

MAG_ERR: derived from FLUX_IVAR_R,I (symmetric errors in Magnitude)
NOTE: all sources with FLUX_IVAR_R and I ==0. have been removed (177 objects)

For EXTENT_FLAG=1 use  SB_* (Surface brightness) computed as TOTALMAG_*+0.61818 (area of 1.45” fiber in square arcsec) as MAG entry (See point 8 below)


8- EXTENT_FLAG 
	=0 if TYPE=PSF
	=1 if TYPE=!PSF

consider giving the model or compute the fibermag using the computed aperture apflux[4] (1.5”) 

9- CAL_MAGs
	use FIBERMAG_G,R,I  from DR10 catalog

========================================================================

Survey (or sub survey) number: S6
Survey (or sub survey) title: AGN_IR (03)

Author: David Alexander, Carolina Andonie, Sotiria Fotopoulou, Claire Greenwell, David Rosario
Email address: d.m.alexander@durham.ac.uk

Brief description explaining approach (optional):

4MOST S6_03 (AGN_IR) real-target catalogue creation:

1- Start from preliminary LS10 imaging catalogue (covers ~93% of the full S6 footprint)

2- Select targets that meet the WISE IR-AGN selection criteria adopted by Mateos et al. (2012) requiring W1, W2, W3 SNR>5, Rmag<23, and not 'PSF' (i.e., selecting optically extended targets); note the WISE photometry is taken from the Sweep catalogues

3- Match target coordinates to unWISE catalogue via IRSA (https://irsa.ipac.caltech.edu/cgi-bin/Gator/nph-scan?mission=irsa&submit=Select&projshort=WISE; LSDR10_targets_26Aug22_Mateos2_unwise_matches_6as.vot [Table 2])

    - Used 6 arcseconds to check the fraction of further matches, but 2 arcseconds is the limit used for the final catalogue; i.e., LS10 targets with no unWISE counterpart within 2" are removed

    - There are a small fraction of matches where a single unWISE object will match to two targets - these are all included currently

      [Note: step 3 is an update over the August 2022 upload to "fix" a WISE photometry issue in the LS10 catalogue]

4- Remove any targets that are *not* identified as IR AGN from the Assef et al. (2018) W1-W2 criteria based on the *unWISE* photometry

      [Note: step 4 is an update over the August 2022 upload to "fix" a WISE photometry issue in the LS10 catalogue]

5- Spectral template choice: Use a combination of morphology and colors

    - LRG template selected if probability from SED fitting for starburst is higher than for QSO otherwise qso_BL template selected

    - Template with E(B-V)=0.01 selected in all cases (aiming to update to select based on actual galactic E(B-V)

Note this is not the final target selection and the current catalogue has some compromises applied to get submitted by the 1st December deadline. But it represents a good advance over the 29th August upload.


------------------------------------------------------------------------
PLEASE FILL OUT THE FORM BELOW AND ANSWER THE FOLLOWING QUESTIONS FOR
EACH SURVEY/SUB SURVEY CUTTING AND PASTING THE FORM AND QUESTIONS FOR
EACH SUB SURVEY WHEN APPROPRIATE
------------------------------------------------------------------------

SUBSURVEY 03

Survey (or sub survey) number: S6
Survey (or sub survey) title: AGN_IR (03)

Author: Sotiria Fotopoulou, Carolina Andonie, David Alexander, Claire Greenwell, David Rosario
Email address: d.m.alexander@durham.ac.uk

------------------------------------------------------------------------

What images and catalogues are used to construct the target catalogue?
Include image and catalogue version numbers when appropriate.

We used the Sweep catalogues of the preliminary DR10 DESI Legacy Imaging
Surveys which provides an update over Dey et al. (2019), AJ, 157, 168 by
covering more of the Southern sky and including deeper WISE coadded data.

------------------------------------------------------------------------

How are the target coordinates determined (e.g., central pixel,
brightest pixel, etc)?

TBC: the coordinates are from the Legacy Survey catalog, and are calculated
using The Tractor code (see Section 8 of Dey et al 2019, AJ, 157,
168). The reference coordinates are from the Gaia DR2 catalog.

------------------------------------------------------------------------

How is the target photometry measured?

TBC: the Legacy Survey reports the photometry of the sources (Dey et
al. 2019, AJ, 157, 168). The g, r, and z band photometry are taken
from the DECaLS survey, with matched partially deblended photometry in
W1 and W2 from the WISE and NEOWISE surveys, plus W3 and W4 from the
WISE survey.  For details see the Photometry section on
https://www.legacysurvey.org/dr9/description/

------------------------------------------------------------------------

How are target photometric redshifts and associated uncertainties
estimated (if relevant)?

We used the Classification-aided photometric redshift estimation
method of Fotopoulou & Paltani (2018). Briefly, a supervised
classification (Random Forest) is used to determine the best
photometric redshift model library to be used for each source. The
actual photometric redsfhit estimation is done using SED fitting with
LePhare.

TBC: in general, we achieve accuracy sigma=0.03 and fraction of
catastrophic outliers eta=6.1% on 7,444 test sources. For the IR-AGN
(Mateos selection) sample we achieve sigma=0.057 and eta=23.1% on a
test sample of 182 sources. Following a similar supervised learning
approach, we also created a star classifier. SDSS-DR17 spectroscopic
redshifts were used for this work, matched to the Legacy DR10
coordinates with 0.5''.

Note: due to the limited time available a simpler approach was adopted
than that taken for the GAMA09 target upload. More details can be
provided on request.

------------------------------------------------------------------------

How is the foreground reddening towards the target calculated?

TBC: the reddening values are taken from the Legacy Survey Sweep catalogs.
The reddening reported in the Sweep catalogs is from Schlegel et al (1998),
ApJ, 500, 525. (https://ui.adsabs.harvard.edu/abs/1998ApJ...500..525S/abstract)

------------------------------------------------------------------------

What approach and assumptions are made for the selection of the target
spectral templates?

We used the following intrinsic spectral templates defined within the X-ray
AGN survey: Type 1 AGN and luminous red galaxy (LRG).
These spectral templates have been modified to take account of
different amounts of Galactic extinction (E(B-V)) and different
redshifts. There are 7 different E(B-V) selections: 0.01, 0.1, 0.2,
0.3, 0.4, 0.5, and 1.0. There are also 14-15 different redshift
ranges: 0.0-0.3, 0.3-0.5, 0.5-0.7, 0.9-1.1, 1.1-1.3, 1.3-1.5, 1.5-1.7,
1.7-1.9, 1.9-2.1, 2.1-2.3, 2.3-2.5, 2.7-2.9, 2.9-3.5, 3.5-4.5. The
Type 1 AGN has an addition 4.5-6.0 redshift bin.

Spectral templates are selected based on the most-probable target type
calculated from the photo-z code with the following logic:

- Is Prob_starburst larger than Prob_QSO?

  +  If yes then select LRG template (early type galaxy)

- Is Prob_QSO larger Prob_starburst?

  +  If yes then select Type 1 AGN template (QSO)

------------------------------------------------------------------------

Is the same target included more than once in the catalogue but with a
different spectral template and/or ruleset? If yes then please provide
a brief justification.

No duplicates (“self shared” targets) in the IR-AGN catalogue. However, we
have not checked if there are self shared targets with the X-ray AGN
catalogues or the PAQS catalogue. But we shouldn't remove shared
targets across sub surveys since that will mess up the target-selection
probability calculations in IWG4. A useful exercise would be to compare
photometric redshifts and spectral-template selections between the X-ray
AGN and IR AGN sub surveys although the current IR-AGN catalogue is
a simple rough-and-ready first real-target catalogue to meet the 29th
August deadline.

------------------------------------------------------------------------

Any additional critical notes:

None.

========================================================================

Survey (or sub survey) number: S6
Survey (or sub survey) title: AGN_HIGHZ (04)

Author: Manda Banerjii
Email address: M.Banerji-Wright@soton.ac.uk

------------------------------------------------------------------------
What images and catalogues are used to construct the target catalogue?
Include image and catalogue version numbers when appropriate.

This is just a placeholder for the time being, with only one target selected in the G09 area
------------------------------------------------------------------------
How are the target coordinates determined (e.g., central pixel,
brightest pixel, etc)?

TBD
------------------------------------------------------------------------
How is the target photometry measured?

TBD
------------------------------------------------------------------------
How are target photometric redshifts and associated uncertainties
estimated (if relevant)?

------------------------------------------------------------------------
How is the foreground reddening towards the target calculated?

TBD
------------------------------------------------------------------------
What approach and assumptions are made for the selection of the target
spectral templates?

We use an empirical high-z QSO template.
------------------------------------------------------------------------
Is the same target included more than once in the catalogue but with a
different spectral template and/or ruleset? If yes then please provide
a brief justification.

------------------------------------------------------------------------
Any additional critical notes:



========================================================================
Survey (or sub survey) number: S605
Survey (or sub survey) title: PAQS
Author: Jens-Kristian Krogager; Kasper E. Heintz
Email address: jens-kristian.krogager@univ-lyon1.fr; keheintz@nbi.ku.dk

------------------------------------------------------------------------
What images and catalogues are used to construct the target catalogue?
Include image and catalogue version numbers when appropriate.

All data are taken from Gaia DR3. 


------------------------------------------------------------------------
How are the target coordinates determined (e.g., central pixel,
brightest pixel, etc)?

Following the Gaia DR3 astrometric solutions.


------------------------------------------------------------------------
How is the target photometry measured?

Following the Gaia DR3 photometric system.

------------------------------------------------------------------------
How are target photometric redshifts and associated uncertainties
estimated (if relevant)?

Since our survey wants to remain un-biased, we have not included any
photometric redshift estimates. The redshift estimates in our catalog
are therefore assigned in a purely statistical manner to reflect the
expected distribution of sources following the quasar luminosity function
and the expected number of stellar contaminants.


------------------------------------------------------------------------
How is the foreground reddening towards the target calculated?

Using the maps by Schlafly & Finkbeiner (2011)


------------------------------------------------------------------------
What approach and assumptions are made for the selection of the target
spectral templates?

Since our survey is agnostic in terms of sample selection, our targets
span a broad range of different object types. We have therefore generated a
large library of spectral templates in order to probe the full range of spectral
shapes that we expect based on a model of quasar properties:
redshift distribution (from Palanque-Delabrouille et al. 2016);
intrinsic dust reddening, and BALs (from Krawczyk et al. 2015);
as well as expected stellar contamination (following Heintz et al. 2020).
The distribution of spectral shapes matches broadly the range of observed colors
(Gaia Blue-Red) of our candidates. All templates are assigned randomly using a
probabilistic approach.

Our sub-survey operates with only two rules for all targets:
 1. For 'faint' sources 20 < G < 20.5 we require a S/N of 10 per Angstrom
 2. For 'bright' sources G < 20 we require a S/N of 15 per Angstrom

In both cases, the S/N ratio is estimated as the median S/N
between 7700 and 8500 Angstrom.
These rules are kept simple in order not to bias our sample
but still ensures that we have enough signal over the full spectral range.
This has been validated by simulating noise properties using the ESO ETC
on our full set of templates.


------------------------------------------------------------------------
Is the same target included more than once in the sub-survey catalogue
but with a different spectral template and/or ruleset? If yes then
please provide a brief justification.

No


------------------------------------------------------------------------
Any additional critical notes:

Sources have been selected based on three cuts:
 - PM/PM_err < 2
 - PLX/PLX_err < 3
 - G < 20.5 mag
 - Gal. Latitude < -62 deg
Where PM = proper motion and PLX = parallax. All data are taken from Gaia DR3.
(See the ADQL query below)

We have further excluded regions with very high target density as these are caused
by foreground dwarf galaxies with resolved stars. These are excluded by rejecting
healpix pixels with a target density larger than 4*sigma above the median, where
sigma is estimated using the robust median absolute deviation (sigma = 2.5*MAD).
We also reject incomplete healpix pixels along the edge of the footprint where the
target density drops below the median by 2-sigma in order to remove tiles with
incomplete target coverage as this would impact all other surveys in those tiles.


ADQL query used to obtain the raw catalog from Gaia:

    SELECT source_id as gaia_id, ra, dec, pmra, pmdec, pmra_error, pmdec_error,
        parallax AS plx, parallax/parallax_over_error AS plx_error,
        sqrt(power(pmra/pmra_error, 2) + power(pmdec/pmdec_error, 2)) AS PM_SIG,
        phot_g_mean_mag AS MAG,
        1.086/phot_g_mean_flux_over_error AS MAG_ERR,
        phot_bp_mean_mag AS CAL_MAG_BLUE,
        1.086/phot_bp_mean_flux_over_error AS CAL_MAG_ERR_BLUE,
        phot_rp_mean_mag AS CAL_MAG_RED,
        1.086/phot_rp_mean_flux_over_error AS CAL_MAG_ERR_RED,
        qso.classlabel_dsc_joint AS DSC_CLASS,
        qso.redshift_qsoc AS REDSHIFT_ESTIMATE,
        0.5*(qso.redshift_qsoc_upper - qso.redshift_qsoc_lower) AS REDSHIFT_ERROR
    FROM gaiadr3.gaia_source
    LEFT JOIN gaiadr3.qso_candidates AS qso USING (source_id)
    WHERE b < -62
        AND sqrt(power(pmra/pmra_error, 2) + power(pmdec/pmdec_error, 2)) < 2
        AND parallax_over_error < 3
        AND phot_g_mean_mag < 20.5

Our query selects all source that meet our criteria, hence we have no need for downsampling.
The raw catalog is then processed and validated before assigning templates and rules.
The CAL_MAG_GREEN is selected as the Gaia-G band. This is assigned in post-processing of the SQL query result.

Note: We are currently working on a consolidated script to generate the catalog.
We will share that once it's been tested and verified.

========================================================================
NOTE: PLEASE CUT AND PASTE THE ABOVE FORM AND ADDRESS THE QUESTIONS
SEPARATELY FOR EACH SUB SURVEY WHEN APPROPRIATE




