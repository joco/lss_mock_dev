"""
This code selects all the sources in the LS_DR10 region, and that meet the Mateos et al (2012) IR AGN with W1, W2, W3 SNR>5 
 & Rmag<23. The WISE photometry is taken from the Sweep catalogs

"""


def do_AGNIR_selection(cat):
    """ This function selects the sources that meet the Mateos et. al (2012) selection.
        To this purpose, we select sources with Rmag<23, W1, W2 and W3 S/N>5. We convert the W1, W2, and W3 fluxes
        to Vega magnitudes, and the r-band fluxes to AB magnitudes

    """

    cat.add_column(Column(name='MAG_W1_V', data=22.5-2.5*np.log10(cat["flux_w1"]) - 2.699, unit=''))
    cat.add_column(Column(name='MAG_W2_V', data=22.5-2.5*np.log10(cat["flux_w2"]) - 3.339, unit=''))
    cat.add_column(Column(name='MAG_W3_V', data=22.5-2.5*np.log10(cat["flux_w3"]) - 5.174, unit=''))

    cat.add_column(Column(name='MAG_R', data=22.5-2.5*np.log10(cat["flux_r"]), unit=''))


    catalog_Mateos = cat[(cat["MAG_R"]<23.)&(cat["MAG_R"]>0)
                        &(cat["flux_w1"]*np.sqrt(cat["flux_ivar_w1"])>5.) 
                        &(cat["flux_w2"]*np.sqrt(cat["flux_ivar_w2"])>5.) 
                        &(cat["flux_w3"]*np.sqrt(cat["flux_ivar_w3"])>5.)]

    del cat
    # print("clean ram & swap")
    # input()

    catalog_Mateos = catalog_Mateos[(catalog_Mateos["MAG_W1_V"]-catalog_Mateos["MAG_W2_V"]) < (0.315*(catalog_Mateos["MAG_W2_V"]-catalog_Mateos["MAG_W3_V"])+0.796)]
    catalog_Mateos = catalog_Mateos[(catalog_Mateos["MAG_W1_V"]-catalog_Mateos["MAG_W2_V"]) > (0.315*(catalog_Mateos["MAG_W2_V"]-catalog_Mateos["MAG_W3_V"])-0.222)]
    catalog_Mateos = catalog_Mateos[(catalog_Mateos["MAG_W1_V"]-catalog_Mateos["MAG_W2_V"]) > (-3.172*(catalog_Mateos["MAG_W2_V"]-catalog_Mateos["MAG_W3_V"])+7.624) ]
    catalog_Mateos = catalog_Mateos[(catalog_Mateos["MAG_W2_V"]-catalog_Mateos["MAG_W3_V"]) >= 2.157]
    
    return catalog_Mateos

def create4MOSTcat_LSDR10(cat):
    N_obj = len(cat)

    r_mag = cat['MAG_R'] # from LS catalog
    mag_err_r = np.zeros(N_obj).astype('float') # from LS catalog
    t_survey = Table()
    EPOCH = np.ones(N_obj) * 2015.5
    RESOLUTION = np.ones(N_obj).astype('int')
    galactic_ebv_array = cat['ebv'] # from LS catalog
    
    NAME = np.full(len(cat),0)
    for i in range(len(cat)):
        NAME[i] = "63" + str(cat["NUMBER"][i])
    #
    NAME = np.array(NAME,dtype=str)
    t_survey.add_column(Column(name='NAME', data=NAME, unit=''))
    t_survey.add_column(Column(name='RA', data=cat['ra'], unit='deg')) # from LS catalog
    t_survey.add_column(Column(name='DEC', data=cat['dec_'], unit='deg')) # from LS catalog
    t_survey.add_column(Column(name='PMRA', data = np.zeros(N_obj).astype('float'), unit='mas/yr')) # from LS catalog
    t_survey.add_column(Column(name='PMDEC', data = np.zeros(N_obj).astype('float'), unit='mas/yr')) # from LS catalog
    t_survey.add_column(Column(name='EPOCH', data=EPOCH, unit='yr'))
    # 'RESOLUTION':np.int16, 1I
    t_survey.add_column(Column(name='RESOLUTION', data=RESOLUTION, unit=''))
    SUBSURVEY = np.ones(N_obj).astype('str')
    SUBSURVEY[:] = 'AGN_IR'
    t_survey.add_column(Column(name='SUBSURVEY', data=SUBSURVEY, unit=''))
    #
    CADENCE = np.zeros(N_obj).astype('int64')
    t_survey.add_column(Column(name='CADENCE', data=CADENCE, unit=''))
    t_survey.add_column(Column(name='REDDENING', data=galactic_ebv_array, unit='    '))
    #
    # REDDENING for templates
    ebv_1000 = (t_survey['REDDENING'] * 1000).astype('int')
    print('EBV', np.min(ebv_1000), np.max(ebv_1000))
    ebv_1_0 = (ebv_1000 > 1000)
    ebv_0_5 = (ebv_1000 > 500) & (ebv_1000 <= 1000)
    ebv_0_4 = (ebv_1000 > 400) & (ebv_1000 <= 500)
    ebv_0_3 = (ebv_1000 > 300) & (ebv_1000 <= 400)
    ebv_0_2 = (ebv_1000 > 200) & (ebv_1000 <= 300)
    ebv_0_1 = (ebv_1000 > 100) & (ebv_1000 <= 200)
    ebv_0_0 = (ebv_1000 <= 100)
    z_name = lambda z0, z1: "_zmin_" + str(int(10 * z0)).zfill(2) + "_zmax_" + str(int(10 * z1)).zfill(2)
    # templates
    template_names = np.zeros(N_obj).astype('U100')
    #
    ruleset_array = np.zeros(N_obj).astype('str')
    #
    ruleset_array[:] = "AGN_ALL_3PC_NOV21"
    z_array = cat['PHOTOZ_RF'] # zphot from Sotiria
    errz_array = z_array * 0.1 # zphot from Sotiria
    # redshift array for templatess
    z_all = np.hstack((0., np.arange(0.3, 3., 0.2), 3.5, 4.5, 6.))
    zmins = z_all[:-1]
    zmaxs = z_all[1:]
    classification = np.full(N_obj, "QSO", dtype=object)
    #
    for z0, z1 in zip(zmins, zmaxs):                        
        zsel = (z_array >= z0) & (z_array < z1) & ((cat['STAR_PROB_RF']>cat['QSO_PROB_RF']) )
        if len(zsel.nonzero()[0]) > 0:
            template_names[(zsel)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_01.fits'
            template_names[(zsel) & (ebv_0_0)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_01.fits'
            template_names[(zsel) & (ebv_0_1)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_1.fits'
            template_names[(zsel) & (ebv_0_2)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_2.fits'
            template_names[(zsel) & (ebv_0_3)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_3.fits'
            template_names[(zsel) & (ebv_0_4)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_4.fits'
            template_names[(zsel) & (ebv_0_5)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_5.fits'
            template_names[(zsel) & (ebv_1_0)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_1_0.fits'
            classification[(zsel)] = 'GALAXY'
                   
        zsel = (z_array >= z0) & (z_array < z1) & ((cat['QSO_PROB_RF']>cat['STAR_PROB_RF']) )
        if len(zsel.nonzero()[0]) > 0:
            template_names[(zsel)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_01.fits'
            template_names[(zsel) & (ebv_0_0)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_01.fits'
            template_names[(zsel) & (ebv_0_1)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_1.fits'
            template_names[(zsel) & (ebv_0_2)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_2.fits'
            template_names[(zsel) & (ebv_0_3)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_3.fits'
            template_names[(zsel) & (ebv_0_4)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_4.fits'
            template_names[(zsel) & (ebv_0_5)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_5.fits'
            template_names[(zsel) & (ebv_1_0)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_1_0.fits'    
            classification[(zsel)] = 'QSO'
    classification = np.array(classification, dtype=str)   
    # 'TEMPLATE':str, max 256 char
    t_survey.add_column(Column(name='TEMPLATE', data=template_names, unit=''))
    # 'RULESET':str, max 256 char
    t_survey.add_column(Column(name='RULESET', data=ruleset_array, unit=''))
    # 'REDSHIFT_ESTIMATE':np.float32, 1E
    # 'REDSHIFT_ERROR':np.float32, 1E#
    t_survey.add_column(Column(name='TEMPLATE_REDSHIFT',data=z_array, unit='float'))
    t_survey.add_column(Column(name='REDSHIFT_ESTIMATE', data=z_array, unit=''))
    t_survey.add_column(Column(name='REDSHIFT_ERROR', data=errz_array, unit=''))
    # 'EXTENT_FLAG': 1I
    # =1
    # 'EXTENT_PARAMETER': 1E
    # =0
    # 'EXTENT_INDEX': 1E
    # =0
    # - SERSIC == 0 | | SHAPE_R < 0.1 | | z > 2 = > Point - like(EXTENT_0)[5133 = 62 %]
    # - SERSIC > 0 & SHAPE_R > 0.1 & Z < 2 = > extended(EXTENT_2)[3113 = 38 %]
    # ext_flag = 2*np.ones(N_obj).astype('int')
    ext_flag = np.zeros(N_obj).astype('int')
    # ext_par  = cat['SHAPE_R'] # from LS catalog
    # ext_idx  = cat['SERSIC'] # from LS catalog
    # psf_type = cat['TYPE'] # from LS catalog
    # ext_par[ext_par < 0.1] = 0.11
    # ext_par[ext_par > 100] = 99.99
    # ext_idx[ext_idx < 0.1] = 0.11
    # ext_idx[ext_idx > 10] = 9.99
    # ext_flag[psf_type=="PSF"] = 0
    # ext_par[(psf_type!="PSF")&((psf_type!="SER"))] = 1
    # ext_idx[(psf_type!="PSF")] = 2
    # forcing 0s for point sources
    t_survey.add_column(Column(name='EXTENT_FLAG', data=ext_flag, unit=''))
    t_survey.add_column(Column(name='EXTENT_PARAMETER',data=np.empty(N_obj), unit='arcsec'))
    t_survey.add_column(Column(name='EXTENT_INDEX',data=np.empty(N_obj), unit=''))
    # 'MAG':np.float32,
    # 'MAG_ERR':np.float32
    # 'MAG_TYPE': str max 256 char
    t_survey.add_column(Column(name='MAG', data=r_mag, unit='mag'))
    t_survey.add_column(Column(name='MAG_ERR', data=mag_err_r, unit='mag'))
    MAG_TYPE = np.ones(N_obj).astype('str')
    MAG_TYPE[:] = 'DECam_r_AB'
    t_survey.add_column(Column(name='MAG_TYPE', data=MAG_TYPE, unit=''))
    # 'REDDENING':np.float32, 1E
    # 'DATE_EARLIEST':np.float64, JulianDate decimal days # 01-Nov-2022
    # 'DATE_LATEST':np.float64, JulianDate decimal days # 02-Feb-2033
    t_survey.add_column(Column(name='DATE_EARLIEST', data=np.zeros(N_obj), unit='d'))
    t_survey.add_column(Column(name='DATE_LATEST', data=np.zeros(N_obj), unit='d'))
    t_survey.add_column(Column(name='CAL_MAG_BLUE',data=np.empty(N_obj)))
    t_survey.add_column(Column(name='CAL_MAG_ERR_BLUE',data=np.empty(N_obj)))
    t_survey.add_column(Column(name='CAL_MAG_ID_BLUE',data=np.empty(N_obj)))
    t_survey.add_column(Column(name='CAL_MAG_GREEN',data=np.empty(N_obj)))
    t_survey.add_column(Column(name='CAL_MAG_ERR_GREEN', data=np.empty(N_obj)))
    t_survey.add_column(Column(name='CAL_MAG_ID_GREEN',data=np.empty(N_obj)))
    t_survey.add_column(Column(name='CAL_MAG_RED',data=np.empty(N_obj)))
    t_survey.add_column(Column(name='CAL_MAG_ERR_RED',data=np.empty(N_obj)))
    t_survey.add_column(Column(name='CAL_MAG_ID_RED',data=np.empty(N_obj)))
    
    t_survey.add_column(Column(name='CLASSIFICATION', data=classification, unit='str'))
    # t_survey.write(p_2_out, overwrite=True)
    # print(p_2_out, 'written')
    return t_survey


def main():

    """

    ## Reading parent catalog
    print("Reading parent catalog")
    cat_LSDR10 = Table.read("FINAL_legacy_dr10_W123_SNR3.fits", memmap=True)

    ## Calculate WISE magnitues and select Mateos AGNs 
    print("Doing Mateos selection")

    cat_LS_DR10_MateosAGN = do_AGNIR_selection(cat_LSDR10)
    del cat_LSDR10 # delete to empty RAM
    cat_LS_DR10_MateosAGN.write("FINAL_legacy_dr10_W123_SNR5_MateosAGN.fits", overwrite=True)
    """

    cat_LS_DR10_MateosAGN = Table.read("FINAL_legacy_dr10_W123_SNR5_MateosAGN.fits", memmap=True)

    # print("clean ram & swap")
    # input()
    
    print("create catalog in 4MOST format")

    ## create catalog in 4MOST format
    cat_4MOST_mateos = create4MOSTcat_LSDR10(cat_LS_DR10_MateosAGN)

    print("Writing final catalog")
    cat_4MOST_mateos.write("LSDR10_targets_26Aug22_Mateos.fits", overwrite=True)


from astropy.table import Table,Column
import numpy as np

main()


