import os, sys
import numpy as np
import healpy
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt
import astropy.io.fits as fits
from matplotlib.path import Path 
from astropy.coordinates import SkyCoord

survey = 'S6'
working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'qmost', survey)
#catalogue_dir = os.path.join(os.environ['HOME'], 'data', '4most', survey)
catalogue_dir = os.path.join(os.environ['HOME'], 'sf_Shared/data/4most', 's6', '2023_march')
#cat_name = os.path.join(catalogue_dir, 'S6_01Dec2023.fits')
cat_name = os.path.join(catalogue_dir, 'S6_20231130T1524Z_target_catalogue_correct_output_ETC2023-11-30_17_16_29.fits')
# name of the sub survey for which the LSM is created
sub_survey_names = np.array([ 'AGN_WIDE', 'AGN_DEEP', 'AGN_IR', 'AGN_HIGHZ', 'PAQS' ])
# loads the catalog
data = fits.open(cat_name)[1].data
selections = np.array([(data['SUBSURVEY']==sub_survey_name) for sub_survey_name in sub_survey_names ])
#N_sel = np.array([len(data[eee]) for eee in selections ])
#selections = np.array([(data['SUBSURVEY']==sub_survey_name) for sub_survey_name in sub_survey_lsm ])

def create_LSM(sub_survey_name, selection):
	print('='*100)
	print(sub_survey_name)
	out_file = os.path.join(working_dir, survey +'_LSM_'+str(sub_survey_name)+'_01Dec2023.fits')
	plot_file = os.path.join(working_dir,survey +'_LSM_'+str(sub_survey_name)+'_01Dec2023.png')
	#########
	#########
	# 1. First create the grid of pixels over the full sky
	#########
	#########
	# create the ARRAY OF PIXEL INDEXES
	NSIDE=32
	N_pixels_all = healpy.nside2npix(NSIDE)
	pix_ids = np.arange(N_pixels_all)
	# ARRAY OF RIGHT ASCENSIONS OF THE PIXEL CENTERS
	ra_hp = np.array([healpy.pix2ang(NSIDE,pix_id, nest=True)[1]*180./np.pi for pix_id in pix_ids ])
	# ARRAY OF DECLINATION OF THE PIXEL CENTERS
	dec_hp = np.array([ (np.pi/2. - healpy.pix2ang(NSIDE,pix_id, nest=True)[0])*180./np.pi for pix_id in pix_ids ])
	# area subtended by each pixel in deg2
	area_per_pixel =  healpy.nside2pixarea(32)*(180/np.pi)**2  
	# Initialize all LSM values 
	coords = SkyCoord(ra_hp, dec_hp, unit='deg', frame='icrs')
	bb_gal = coords.galactic.b.value
	ll_gal = coords.galactic.l.value
	#print('bb_gal', bb_gal[:10], time.time() - t0, 's')
	#print('ll_gal', ll_gal[:10], time.time() - t0, 's')
	bb_ecl = coords.barycentrictrueecliptic.lat
	ll_ecl = coords.barycentrictrueecliptic.lon
	#print('bb_ecl', bb_ecl[:10], time.time() - t0, 's')
	#print('ll_ecl', ll_ecl[:10], time.time() - t0, 's')
	# area subtended by each pixel in deg2
	area_per_pixel =  healpy.nside2pixarea(NSIDE)*(180/np.pi)**2  
	# Initialize all LSM values 
	lsm_values = np.zeros(len(ra_hp))
	#########
	#########
	# 2. Retrieve pixels that contain targets
	#########
	#########
	# select coordinates in your catalog file
	RA = data['RA'][selection]
	DEC = data['DEC'][selection]
	# compute healpix index for each target
	DATA_HEALPIX_32   = healpy.ang2pix(32,   np.pi/2. - DEC*np.pi/180. , RA*np.pi/180. , nest=True)
	#print('N, DATA_HEALPIX_32', len(DATA_HEALPIX_32), DATA_HEALPIX_32)
	# retrieves a unique and sorted list of healpix indexes containing targets
	unique_healpix_32_indexes = np.unique(DATA_HEALPIX_32)
	unique_healpix_32_indexes.sort()
	N_pixels = len(unique_healpix_32_indexes)
	print('N non-empty healpix 32 pixels, ',len(unique_healpix_32_indexes), ', area=', np.round(len(unique_healpix_32_indexes)*area_per_pixel,2), 'deg2')
	#########
	#########
	# 2. Assigns LSM values
	#########
	#########
	val_normed = 1/N_pixels
	#if sub_survey_name=="AGN_DEEP":
		#val_normed = 1/N_pixels
	#if sub_survey_name=="PAQS":
		#val_normed = 1/N_pixels
	#else:
		#priority_ecl = np.log10(abs(bb_ecl.value)+10)  # between 10 and 90
		#val_normed = priority_ecl[unique_healpix_32_indexes]/np.sum(priority_ecl[unique_healpix_32_indexes])
		#print(val_normed.min(), val_normed.max())

	lsm_values[unique_healpix_32_indexes] = val_normed

	## DES footprint
	#ra_hp2 = ra_hp[unique_healpix_32_indexes]
	#dec_hp2 = dec_hp[unique_healpix_32_indexes]
	#tmpradec      = np.zeros((len(ra_hp2),2))

	#tmpradec[:,0] = ra_hp2
	#tmp = (tmpradec[:,0]>270)
	#tmpradec[tmp,0]-= 360.
	#tmpradec[:,1] = dec_hp2
	#des_survey           = DESpoly.contains_points(tmpradec)

	#des_survey    = DESpoly.contains_points(tmpradec)
	#unique_healpix_32_indexes_in_DES = unique_healpix_32_indexes[des_survey]
	## 
	#N_pixels_with_targets = len(unique_healpix_32_indexes)
	#N_pixels_with_targets_in_DES = len(unique_healpix_32_indexes_in_DES)
	#print('N pixels outside DES, and inside',N_pixels_with_targets, N_pixels_with_targets_in_DES)
	#print('Area outside DES, and inside',N_pixels_with_targets*area_per_pixel, N_pixels_with_targets_in_DES*area_per_pixel)
	#norm_per_pixel = 1./(area_per_pixel*N_pixels_with_targets)
	#norm_high = norm_per_pixel * 1.2
	#norm_low = (1. - norm_high*N_pixels_with_targets_in_DES)/(N_pixels_with_targets-N_pixels_with_targets_in_DES)

	#lsm_values = np.zeros(len(ra_hp))
	#print('lsm values',norm_high, norm_low)
	#lsm_values[unique_healpix_32_indexes] = norm_high
	#lsm_values[unique_healpix_32_indexes_in_DES] = norm_low
	#print('unique lsm values',np.unique(lsm_values))

	#########
	#########
	# 3. Plots the LSM values over RA and DEC
	#########
	#########
	plt.figure(0, (6,6))
	ok = lsm_values>0
	#plt.scatter(ra_hp, dec_hp, c=np.log10(lsm_values), s=4, marker='s', edgecolors='face')
	plt.scatter(ra_hp[ok], dec_hp[ok], c=np.log10(lsm_values[ok]), s=4, marker='s', edgecolors='face')
	cb = plt.colorbar(shrink=0.8)
	cb.set_label('LSM')
	plt.grid()
	plt.ylim((-90,40))
	plt.xlim((-1,361))
	plt.xlabel('R.A.')
	plt.ylabel('Dec')
	plt.title(sub_survey_name)
	plt.savefig(plot_file)
	plt.clf()
	#########
	#########
	# 4. Create the fits LSM file
	#########
	#########
	cols = fits.ColDefs([
		fits.Column( "healpix_id"	 ,unit=''   ,format="I", array=pix_ids   ),
		fits.Column( "RA"            ,unit='deg',format="E", array=ra_hp     ),
		fits.Column( "DEC"	         ,unit='deg',format="E", array=dec_hp    ),
		fits.Column( "LSM"           ,unit=''   ,format="E", array=lsm_values)
		])
	tbhdu = fits.BinTableHDU.from_columns(cols)
	tbhdu.header['SURVEY'] = 'S06'
	tbhdu.header['HIERARCH SUBSURVEY'] = sub_survey_name
	tbhdu.header['NSIDE'] = NSIDE
	tbhdu.header['COORD'] = 'equatorial'
	tbhdu.header['NESTED'] = 'True'
	if os.path.isfile(out_file):
		os.remove(out_file)
	tbhdu.writeto(out_file)
	print('sum of LSM values:',np.sum(tbhdu.data['LSM']))

for sub_survey_name, selection in zip(sub_survey_names, selections):
	create_LSM(sub_survey_name, selection)
