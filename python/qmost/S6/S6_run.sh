#!/bin/bash
# same LSM as for S5 
# SSM WIDE,  IR : 0.5 => 85% completeness
# SSM DEEP
cd $GIT_AGN_MOCK/python/qmost/S6
# python S6_merge_catalogues.py
python S6_LSM_create_function.py
python S6_SSM_create_function.py
python S6_tabulate_NZ.py
python S6_generate_sub_survey_params_table.py
python S6_sky_plot.py
# python S6_plot_ETC.py
