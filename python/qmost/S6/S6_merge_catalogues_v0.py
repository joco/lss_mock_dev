import os, glob, sys
from astropy.io import fits
from astropy.coordinates import SkyCoord
import astropy.units as u
import numpy as np
import healpy as hp
from astropy.table import Table, Column, vstack, hstack


working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'qmost', 'S6')
catalogue_dir = os.path.join(os.environ['HOME'], 'sf_Shared/data/4most', 's6', '2023_march')
out_name1 = os.path.join(catalogue_dir, 'S6_09Mar2023.fits')



# name of the sub survey for which the LSM is created
#sub_survey_names = np.array([ 'AGN_WIDE', 'AGN_DEEP', 'AGN_IR', 'PAQS' ])
# loads the catalog

#data0 = Table.read(os.path.join(catalogue_dir, '2022_11_24_catalog', '4MOST_S6_erass4_WIDE_221122.fits'))

#N_obj = len(data0)

#UID = ( 601 * 1e12 + data0['BRICKID']*1e5 + data0['OBJID'] ).astype('int64')
#uniq_val, uniq_id = np.unique(UID, return_index=True)
#NAME = np.array([str(int(el)).zfill(11) for el in UID])
#data0['NAME'] = NAME

## 'TEMPLATE':str, max 256 char
## templates
#template_names = np.zeros(N_obj).astype('U100')
#template_names[:] = "ROSAT_AGNT1-DR16QSO-stitched-stack-qmost-template.fits"
#template_names[data0['CLASSIFICATION']=="AGN"] = "ROSAT_AGNT2-highZ-stitched-stack-qmost-template.fits"
#template_names[data0['CLASSIFICATION']=="GAL"] = "DR16LRG-stitched-stack-qmost-template.fits"

#z_2935= (data0['REDSHIFT_ESTIMATE']>2.9)&(data0['REDSHIFT_ESTIMATE']<3.5)
#z_3545= (data0['REDSHIFT_ESTIMATE']>3.5)&(data0['REDSHIFT_ESTIMATE']<4.5)
#z_4560= (data0['REDSHIFT_ESTIMATE']>4.5)

#template_names[z_2935] = "4most_qso_BL_zmin_29_zmax_35_EBV_0_01.fits"
#template_names[z_3545] = "4most_qso_BL_zmin_35_zmax_45_EBV_0_01.fits"
#template_names[z_4560] = "4most_qso_BL_zmin_45_zmax_60_EBV_0_01.fits"

#template_redshifts = data0['TEMPLATE_REDSHIFT']
#template_redshifts[z_2935] = (2.9+3.5)/2.
#template_redshifts[z_3545] = (3.5+4.5)/2.
#template_redshifts[z_4560] = (4.5+6.0)/2.

#data0['TEMPLATE'] = template_names
#data0['TEMPLATE_REDSHIFT'] = template_redshifts

#z_array = data0['REDSHIFT_ESTIMATE']
#z_array[z_2935] = (2.9+3.5)/2.
#z_array[z_3545] = (3.5+4.5)/2.
#z_array[z_4560] = (4.5+6.0)/2.
#data0['REDSHIFT_ESTIMATE'] = z_array

#get_undef0 = lambda colname : ( np.isnan(data0[colname]) ) | ( data0[colname] > 49.9) | ( data0[colname] <= 0.0 ) | ( data0[colname] == np.inf )

#data0['CAL_MAG_BLUE'] [get_undef0('CAL_MAG_BLUE')]  = 49.9
#data0['CAL_MAG_GREEN'][get_undef0('CAL_MAG_GREEN')] = 49.9
#data0['CAL_MAG_RED']  [get_undef0('CAL_MAG_RED')]   = 49.9

#data0['CAL_MAG_ERR_BLUE'] [get_undef0('CAL_MAG_ERR_BLUE')]  = 49.9
#data0['CAL_MAG_ERR_GREEN'][get_undef0('CAL_MAG_ERR_GREEN')] = 49.9
#data0['CAL_MAG_ERR_RED']  [get_undef0('CAL_MAG_ERR_RED')]   = 49.9

#data1 = Table.read(os.path.join(catalogue_dir, '2022_11_24_catalog', '4MOST_S6_erass4_DEEP_221122.fits'))

#N_obj = len(data1)

#UID = ( 601 * 1e12 + data1['BRICKID']*1e5 + data1['OBJID'] ).astype('int64')
#uniq_val, uniq_id = np.unique(UID, return_index=True)
#NAME = np.array([str(int(el)).zfill(11) for el in UID])
#data1['NAME'] = NAME

## 'TEMPLATE':str, max 256 char
## templates
#template_names = np.zeros(N_obj).astype('U100')
#template_names[:] = "ROSAT_AGNT1-DR16QSO-stitched-stack-qmost-template.fits"
#template_names[data1['CLASSIFICATION']=="AGN"] = "ROSAT_AGNT2-highZ-stitched-stack-qmost-template.fits"
#template_names[data1['CLASSIFICATION']=="GAL"] = "DR16LRG-stitched-stack-qmost-template.fits"

#z_2935= (data1['REDSHIFT_ESTIMATE']>2.9)&(data1['REDSHIFT_ESTIMATE']<3.5)
#z_3545= (data1['REDSHIFT_ESTIMATE']>3.5)&(data1['REDSHIFT_ESTIMATE']<4.5)
#z_4560= (data1['REDSHIFT_ESTIMATE']>4.5)

#template_names[z_2935] = "4most_qso_BL_zmin_29_zmax_35_EBV_0_01.fits"
#template_names[z_3545] = "4most_qso_BL_zmin_35_zmax_45_EBV_0_01.fits"
#template_names[z_4560] = "4most_qso_BL_zmin_45_zmax_60_EBV_0_01.fits"

#template_redshifts = data1['TEMPLATE_REDSHIFT']
#template_redshifts[z_2935] = (2.9+3.5)/2.
#template_redshifts[z_3545] = (3.5+4.5)/2.
#template_redshifts[z_4560] = (4.5+6.0)/2.

#data1['TEMPLATE'] = template_names
#data1['TEMPLATE_REDSHIFT'] = template_redshifts

#z_array = data1['REDSHIFT_ESTIMATE']
#z_array[z_2935] = (2.9+3.5)/2.
#z_array[z_3545] = (3.5+4.5)/2.
#z_array[z_4560] = (4.5+6.0)/2.
#data1['REDSHIFT_ESTIMATE'] = z_array

#get_undef = lambda colname : ( np.isnan(data1[colname]) ) | ( data1[colname] > 49.9) | ( data1[colname] <= 0.0 ) | ( data1[colname] == np.inf )

#data1['CAL_MAG_BLUE'] [get_undef('CAL_MAG_BLUE')]  = 49.9
#data1['CAL_MAG_GREEN'][get_undef('CAL_MAG_GREEN')] = 49.9
#data1['CAL_MAG_RED']  [get_undef('CAL_MAG_RED')]   = 49.9

#data1['CAL_MAG_ERR_BLUE'] [get_undef('CAL_MAG_ERR_BLUE')]  = 49.9
#data1['CAL_MAG_ERR_GREEN'][get_undef('CAL_MAG_ERR_GREEN')] = 49.9
#data1['CAL_MAG_ERR_RED']  [get_undef('CAL_MAG_ERR_RED')]   = 49.9


##
#print('data1')
#print( np.unique(data1['RULESET']) )
#print( np.unique(data1['MAG_TYPE']) )

## merges catalogues into structure
#def COL(colname):
  #return np.hstack(( data0[colname].value, data1[colname].value, data2[colname].value ))

#

data1 = Table.read(os.path.join(catalogue_dir, 'S6_20230124T1832Z_target_catalogue.fits' ))
data2 = Table.read(os.path.join(catalogue_dir, 'PAQS_catalog_20220303', 'PAQS_catalog_20230303T162414Z.fits'))
data3 = Table.read(os.path.join(catalogue_dir, 'LSDR10_targets_March2023.fits'))
data4 = Table.read(os.path.join(catalogue_dir, 'S6_20230308_AGN_HIGH-Z_final_cat.fits' ))

data1 = data1[ (data1['SUBSURVEY']=="AGN_DEEP") | (data1['SUBSURVEY']=="AGN_WIDE") ]
data1['EPOCH'] = 2000.0

data2['CLASSIFICATION']="TBD"
data2['TEMPLATE'][data2['TEMPLATE']=="template_lib/powerlaw_BR+1.3_temp.fits"] = "powerlaw_BR+1.3_temp.fits"
data2['SUBSURVEY'] = "PAQS"
data2['CAL_MAG_ID_GREEN'] = "GAIA_G_VEGA_PSF"
data2['CAL_MAG_ID_BLUE'] = "GAIA_GBP_VEGA_PSF"
data2['CAL_MAG_ID_RED'] = "GAIA_GRP_VEGA_PSF"

ones = np.ones_like(data2['TEMPLATE']).astype('str')
ones[(data2['RULESET']=="PAQS_BRIGHT")] = "PAQS_LR_BRIGHT_JAN23"
ones[(data2['RULESET']=="PAQS_FAINT")] = "PAQS_LR_FAINT_JAN23"
data2['RULESET'] = ones
#print(data1.info())
#print(data2.info())
#print(data3.info())
#print(data4.info())
# 'TEMPLATE':str, max 256 char
# templates

re_assign_z = (data3['REDSHIFT_ESTIMATE']>4.5)
data3['REDSHIFT_ESTIMATE'][re_assign_z] = 4.5
#data3['TEMPLATE'][re_assign_z] = '4most_AGN_type2_zmin_45_zmax_60_EBV_0_01.fits'
N_obj = len(data3)
#is_t2 = np.array( [ 'AGN_type2' in tpln for tpln in data3['TEMPLATE'] ] )
#is_el = np.array( [ 'LRG'       in tpln for tpln in data3['TEMPLATE'] ] )
#is_t1 = np.array( [ 'qso_BL'    in tpln for tpln in data3['TEMPLATE'] ] )

#data3['CLASSIFICATION'][ data3['CLASSIFICATION']=="GALAXY" ] = "GAL"

# cap redshifts at 5.5
# re-assign templates
# for ELL gal above redshift 2, use the type 2 AGN ones. Below use the LRG fixed redshift template

#el_zLT21 = (is_el) & (data3['REDSHIFT_ESTIMATE']<2.1) # do not change
#el_zGT21 = (is_el) & (data3['REDSHIFT_ESTIMATE']>=2.1) # change to t2 template

#out_tpl = data3['TEMPLATE'][el_zGT21]
#new_tpl = np.array([ el.replace('LRG','AGN_type2') for el in out_tpl ])
#data3['TEMPLATE'][el_zGT21] = new_tpl

#out_tpl = data3['TEMPLATE']
#new_tpl = np.array([ 'R'+el[1:] for el in out_tpl ])
#data3['TEMPLATE'] = new_tpl

#z1_tpl = np.array([ float(el.split('_')[-4])/10. for el in data3['TEMPLATE'] ])
#z0_tpl = np.array([ float(el.split('_')[-6])/10. for el in data3['TEMPLATE'] ])
#z_tpl = (z0_tpl+z1_tpl)/2.

nl = lambda sel : len(sel.nonzero()[0])

##z_2935 = (data3['REDSHIFT_ESTIMATE']>2.9)&(data3['REDSHIFT_ESTIMATE']<3.5)
##z_3545 = (data3['REDSHIFT_ESTIMATE']>3.5)&(data3['REDSHIFT_ESTIMATE']<4.5)
##z_4560 = (data3['REDSHIFT_ESTIMATE']>4.5)

##template_names[(z_2935) & (is_t1)] = "4most_qso_BL_zmin_29_zmax_35_EBV_0_01.fits"
##template_names[(z_3545) & (is_t1)] = "4most_qso_BL_zmin_35_zmax_45_EBV_0_01.fits"
##template_names[(z_4560) & (is_t1)] = "4most_qso_BL_zmin_45_zmax_60_EBV_0_01.fits"

##template_redshifts = data3['TEMPLATE_REDSHIFT']
##template_redshifts[(z_2935) & (is_t1)] = (2.9+3.5)/2.
##template_redshifts[(z_3545) & (is_t1)] = (3.5+4.5)/2.
##template_redshifts[(z_4560) & (is_t1)] = (4.5+6.0)/2.

##data3['TEMPLATE'] = template_names
#data3['TEMPLATE_REDSHIFT'] = z_tpl

##z_array = data3['REDSHIFT_ESTIMATE']
##z_array[z_2935] = (2.9+3.5)/2.
##z_array[z_3545] = (3.5+4.5)/2.
##z_array[z_4560] = (4.5+6.0)/2.
##data3['REDSHIFT_ESTIMATE'] = z_array



##bad_template = (data3['TEMPLATE']=='0.0')|(data3['NAME']=='63862')
##tp1 = (data3['TEMPLATE'] == "4most_LRG_zmin_21_zmax_23_EBV_0_01.fits"	)
##tp2 = (data3['TEMPLATE'] == "4most_LRG_zmin_23_zmax_25_EBV_0_01.fits" )
##tp3 = (data3['TEMPLATE'] == "4most_LRG_zmin_21_zmax_23_EBV_0_1.fits"  )
##good_area = (data3['DEC']>-80)&(data3['DEC']<5)&(bad_template==False)&(tp1==False)&(tp2==False)&(tp3==False)
##data3 = data3[good_area]

#zmin = np.array([ 0.1 * float( el.split('_')[-6] ) for el in data3['TEMPLATE'] ])
#zmax = np.array([ 0.1 * float( el.split('_')[-4] ) for el in data3['TEMPLATE'] ])
#z_tpl = ( zmin + zmax ) /2.
#data3['TEMPLATE_REDSHIFT'] = z_tpl
data3['COMPLETENESS'] = 0
data3['PARALLAX'] = 0

data4['SUBSURVEY']='AGN_HIGHZ'
data4['MAG_TYPE'][(data4['MAG_TYPE']=="DECam_I_AB")] = "DECam_i_AB"
data4['MAG_TYPE'][(data4['MAG_TYPE']=="DECam_Z_AB")] = "DECam_z_AB"
data4['MAG_TYPE'][(data4['MAG_TYPE']=="DECam_z_AB")] = "DECam_z_AB"
data4['CAL_MAG_ID_RED'] = "DECAM_I_AB_PSF"

data4['CLASSIFICATION']="TBD"
data4['COMPLETENESS'] = 0
data4['PARALLAX'] = 0

# merges catalogues into structure
def COL(colname):
  return np.hstack(( data1[colname].value, data2[colname].value, data3[colname].value, data4[colname].value ))

def set_49(VAL):
  VAL[(VAL<=1.)|(VAL>49.9)|np.isnan(VAL)] = 49.9
  print(np.unique(VAL))
  return VAL

def set_I_B(VAL):
  VAL[(VAL==b'0.0')|(VAL==b'6.90020378795435e-310')|(VAL==b'6.90749317182025e-310')] = "DECAM_G_AB_FIB"
  print(np.unique(VAL))
  return VAL

def set_I_G(VAL):
  VAL[(VAL==b'0.0')|(VAL==b'6.90020378795435e-310')|(VAL==b'6.90749317182025e-310')] = "DECAM_R_AB_FIB"
  print(np.unique(VAL))
  return VAL

def set_I_R(VAL):
  VAL[(VAL==b'0.0')|(VAL==b'6.90020378795435e-310')|(VAL==b'6.90749317182025e-310')] = "DECAM_I_AB_FIB"
  print(np.unique(VAL))
  return VAL


# merge

#old_rulesets = COL('RULESET')
#RS_dict = {}
#RS_dict[b'AGN_ALL_3PC_NOV21' ] =  'AGN_ALL_3PC_JAN23'
#RS_dict[b'AGN_ALL_SN10_NOV21'] =  'AGN_ALL_SN10_JAN23'
#RS_dict[b'PAQS_LR_BRIGHT'    ] = 'PAQS_LR_BRIGHT_JAN23'
#RS_dict[b'PAQS_LR_FAINT'     ] = 'PAQS_LR_FAINT_JAN23'
# AGN2_ZGT15_3PC_MAR3
#NEW_rulesets = np.array([RS_dict[el]  for el in old_rulesets])

t_survey = []

#NAME = np.array([str(int(el)).zfill(11) for el in UID])
t_survey.append(fits.Column(name='NAME'             , array=COL('NAME'             ) , unit=''      , format='20A') )
t_survey.append(fits.Column(name='RA'               , array=COL('RA'               ) , unit='deg'   , format='D')   )
t_survey.append(fits.Column(name='DEC'              , array=COL('DEC'              ) , unit='deg'   , format='D')   )
t_survey.append(fits.Column(name='PMRA'             , array=COL('PMRA'             ) , unit='mas/yr', format='E')   )
t_survey.append(fits.Column(name='PMDEC'            , array=COL('PMDEC'            ) , unit='mas/yr', format='E')   )
t_survey.append(fits.Column(name='EPOCH'            , array=COL('EPOCH'            ) , unit='yr'    , format='E')   )
t_survey.append(fits.Column(name='RESOLUTION'       , array=COL('RESOLUTION'       ) , unit=''      , format='I')   )
t_survey.append(fits.Column(name='SUBSURVEY'        , array=COL('SUBSURVEY'        ) , unit=''      , format='15A') )
t_survey.append(fits.Column(name='CADENCE'          , array=COL('CADENCE'          ) , unit=''      , format='K')   )
t_survey.append(fits.Column(name='TEMPLATE'         , array=COL('TEMPLATE'         ) , unit=''      , format='256A'))
t_survey.append(fits.Column(name='RULESET'          , array=COL('RULESET'          ) , unit=''      , format='30A') )
t_survey.append(fits.Column(name='REDSHIFT_ESTIMATE', array=COL('REDSHIFT_ESTIMATE') , unit=''      , format='E')   )
t_survey.append(fits.Column(name='REDSHIFT_ERROR'   , array=COL('REDSHIFT_ERROR'   ) , unit=''      , format='E')   )
t_survey.append(fits.Column(name='EXTENT_FLAG'      , array=COL('EXTENT_FLAG'      ) , unit=''      , format='I')   )
t_survey.append(fits.Column(name='EXTENT_PARAMETER' , array=COL('EXTENT_PARAMETER' ) , unit='arcsec', format='E')   )
t_survey.append(fits.Column(name='EXTENT_INDEX'     , array=COL('EXTENT_INDEX'     ) , unit=''      , format='E')   )
t_survey.append(fits.Column(name='MAG'              , array=COL('MAG'              ) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='MAG_ERR'          , array=COL('MAG_ERR'          ) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='MAG_TYPE'         , array=COL('MAG_TYPE'         ) , unit=''      , format='20A') )
t_survey.append(fits.Column(name='REDDENING'        , array=COL('REDDENING'        ) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='TEMPLATE_REDSHIFT', array=COL('TEMPLATE_REDSHIFT') , unit=''      , format='E')   )
t_survey.append(fits.Column(name='DATE_EARLIEST'    , array=COL('DATE_EARLIEST'    ) , unit='d'     , format='E')   )
t_survey.append(fits.Column(name='DATE_LATEST'      , array=COL('DATE_LATEST'      ) , unit='d'     , format='E')   )

t_survey.append(fits.Column(name='CAL_MAG_BLUE'     , array=set_49(COL('CAL_MAG_BLUE'     )) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='CAL_MAG_ERR_BLUE' , array=set_49(COL('CAL_MAG_ERR_BLUE' )) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='CAL_MAG_ID_BLUE'  , array=set_I_B(COL('CAL_MAG_ID_BLUE'  )) , unit=''      , format='20A'))
t_survey.append(fits.Column(name='CAL_MAG_GREEN'    , array=set_49(COL('CAL_MAG_GREEN'    )) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='CAL_MAG_ERR_GREEN', array=set_49(COL('CAL_MAG_ERR_GREEN')) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='CAL_MAG_ID_GREEN' , array=set_I_G(COL('CAL_MAG_ID_GREEN')) , unit=''      , format='20A') )
t_survey.append(fits.Column(name='CAL_MAG_RED'      , array=set_49(COL('CAL_MAG_RED'      )) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='CAL_MAG_ERR_RED'  , array=set_49(COL('CAL_MAG_ERR_RED'  )) , unit='mag'   , format='E')   )
t_survey.append(fits.Column(name='CAL_MAG_ID_RED'   , array=set_I_R(COL('CAL_MAG_ID_RED'  )) , unit=''      , format='20A') )
#classification = COL('CLASSIFICATION'   )
#classification[classification=="GALAXY"] = "GAL"
t_survey.append(fits.Column(name='CLASSIFICATION'   , array=COL('CLASSIFICATION'   )      , unit=''       , format='20A') )
t_survey.append(fits.Column(name='COMPLETENESS'     , array=COL('COMPLETENESS') , unit=''       , format='E')   )
t_survey.append(fits.Column(name='PARALLAX'         , array=np.zeros_like(COL('COMPLETENESS'))     , unit='mas'    , format='E')   )


hdu = fits.BinTableHDU.from_columns(t_survey)
hdu.name = 'S06'
hdu.header['FMTDOC'] = 'VIS-MAN-4MOST-47110-1720-0001'
hdu.header['FMTVERS'] = '2.5'

outf = fits.HDUList([fits.PrimaryHDU(), hdu])
outf.writeto(out_name1 , overwrite=True)
print( out_name1, 'written')

#cd /media/sf_Shared/data/4most/s6/
#gzip S6_sub0102030405_merged_28Nov2022.fits

sys.exit()

#data3.remove_columns(['CAL_MAG_BLUE',  'CAL_MAG_ERR_BLUE',
  #'CAL_MAG_ID_BLUE',
    #'CAL_MAG_GREEN',
#'CAL_MAG_ERR_GREEN',
 #'CAL_MAG_ID_GREEN',
      #'CAL_MAG_RED',
  #'CAL_MAG_ERR_RED',
   #'CAL_MAG_ID_RED', 'CLASSIFICATION'])

## merge
## remove columns
#data5.remove_columns(['CAL_MAG_BLUE',  'CAL_MAG_ERR_BLUE',
  #'CAL_MAG_ID_BLUE',
    #'CAL_MAG_GREEN',
#'CAL_MAG_ERR_GREEN',
 #'CAL_MAG_ID_GREEN',
      #'CAL_MAG_RED',
  #'CAL_MAG_ERR_RED',
   #'CAL_MAG_ID_RED', 'TEMPLATE_REDSHIFT'])
#data5['SUBSURVEY']='AGN_HIGHZ'

#dataO = vstack((data1, data2, data4, data5))
#dataO['MAG_TYPE'][(dataO['MAG_TYPE']=="DECam_I_AB")] = "DECam_i_AB"
#dataO['MAG_TYPE'][(dataO['MAG_TYPE']=="DECam_Z_AB")] = "DECam_z_AB"
#dataO['MAG_TYPE'][(dataO['MAG_TYPE']=="DECam_R_AB")] = "DECam_r_AB"

#ebv = dataO['REDDENING']
#dataO.remove_columns(['REDDENING'])
#dataO.add_column(Column(name='REDDENING',data=ebv.astype('float32')                , unit=''))

#print('dataO')
#print( np.unique(dataO['RULESET']) )
#print( np.unique(dataO['SUBSURVEY']) )
#print( np.unique(dataO['MAG_TYPE']) )

#resolution = dataO['RESOLUTION']
#dataO['RESOLUTION'] = resolution.astype('int')
#CADENCE = dataO['CADENCE']
#dataO['CADENCE'] = CADENCE.astype('int')
#EXTENT_FLAG = dataO['EXTENT_FLAG']
#dataO['EXTENT_FLAG'] = EXTENT_FLAG.astype('int')

#dataO.write(out_name, overwrite = True)

#RS_dict = {}
#RS_dict['AGN_ALL_3PC_NOV21' ] =  'AGN_ALL_3PC_JAN23'
#RS_dict['AGN_ALL_SN10_NOV21'] =  'AGN_ALL_SN10_JAN23'
#RS_dict['PAQS_LR_BRIGHT'    ] = 'PAQS_LR_BRIGHT_JAN23'
#RS_dict['PAQS_LR_FAINT'     ] = 'PAQS_LR_FAINT_JAN23'


#The following dependencies are missing: 4most_AGN_type2_zmin_45_zmax_60_EBV_0_01.fits (spec.template). Please make sure they are uploaded beforehand.
#The following error message(s) were found: There was a problem confirming the format version of the ruleset file 'S6_20211123T1052Z_rulesets.csv'. The target cat. has FMTVERS=2.5 which requires a ruleset updated according to FMTVERS=2.4. There was a problem confirming the format version of the ruleset file 'S6_20211123T1052Z_rulesets.csv'. The target cat. has FMTVERS=2.5 which requires a ruleset updated according to FMTVERS=2.4. There was a problem confirming the format version of the ruleset file 'S6_20220411T1224Z_rulesets.csv'. The target cat. has FMTVERS=2.5 which requires a ruleset updated according to FMTVERS=2.4. There was a problem confirming the format version of the ruleset file 'S6_20220411T1224Z_rulesets.csv'. The target cat. has FMTVERS=2.5 which requires a ruleset updated according to FMTVERS=2.4.

#The ETC consistency check returned an error (return code: 1).
#STDOUT was:
#4MOST Exposure Time Calculator version 2.2.2
#* catalog file: /data/QFSwifiles/targetcatalog/S6/S6_24Jan2023.fits
#* rulesets file: /data/QFSwifiles/rulesets/S6/consolidated_S6_24Jan2023_rulesets.csv
#* rules file: /data/QFSwifiles/rules/S6/consolidated_S6_24Jan2023_rules.csv
#* templates dir: /data/QFSwifiles/spectraltemplates/S6

#Total 1625085 entries
#Total 700 target/magtype/redshift combinations
#Error: Target 4most_AGN_type2_zmin_45_zmax_60_EBV_0_01.fits [DECam_r_AB] z=-0.05: [Errno 2] No such file or directory: '4most_AGN_type2_zmin_45_zmax_60_EBV_0_01.fits'
#Error: Target 4most_AGN_type2_zmin_45_zmax_60_EBV_0_01.fits [DECam_r_AB] z=0.05: [Errno 2] No such file or directory: '4most_AGN_type2_zmin_45_zmax_60_EBV_0_01.fits'
#Error: Target 4most_AGN_type2_zmin_45_zmax_60_EBV_0_01.fits [DECam_r_AB] z=-0.10: [Errno 2] No such file or directory: '4most_AGN_type2_zmin_45_zmax_60_EBV_0_01.fits'
#Error: Target 4most_AGN_type2_zmin_45_zmax_60_EBV_0_01.fits [DECam_r_AB] z=0.00: [Errno 2] No such file or directory: '4most_AGN_type2_zmin_45_zmax_60_EBV_0_01.fits'
#Total 4 rulesets
#Error: Ruleset AGN_ALL_3PC_NOV21 [lrs]: 'AGN_ALL_3PC_NOV21'
#Error: Ruleset AGN_ALL_SN10_NOV21 [lrs]: 'AGN_ALL_SN10_NOV21'
#Error: Ruleset PAQS_LR_FAINT [lrs]: 'PAQS_LR_FAINT'
#Error: Ruleset PAQS_LR_BRIGHT [lrs]: 'PAQS_LR_BRIGHT'
#Error: Consistency check failed

#STDERR was:
#WARNING: UnitsWarning: 'erg/s/cm**2/Angstrom' contains multiple slashes, which is discouraged by the FITS standard [astropy.units.format.generic]
