"""
Creates a fits catalog containing the 4FS input columns.
DR16_ELG-stitched-stack-qmost-template.fits
DR16LRG-stitched-stack-qmost-template.fits
ROSAT_AGNT1-DR16QSO-stitched-stack-qmost-template.fits
ROSAT_AGNT2-highZ-stitched-stack-qmost-template.fits

"""
print('CREATES 4FS FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')

import os
import sys
import astropy.io.fits as fits
import numpy as n
from astropy.coordinates import SkyCoord
from astropy.table import Table, Column

nl = lambda sel : len(sel.nonzero()[0])

survey = 'S6'
working_dir = os.path.join(os.environ['HOME'], 'data', '4most', survey)
catalog_input = os.path.join(working_dir, 'S6_4MOST_ALL_SNR3_IR215_14Apr21_FIBERMAGcut_225_formatApril22.fits')
t_survey = Table.read(catalog_input)
path_2_out = os.path.join(working_dir, 'S6_restFrame_templates.fits')


t2 = n.array([ 'Qmost_AGN_type2'== EL[:15] for EL in t_survey['TEMPLATE'] ])
t1 = n.array([ 'Qmost_qso_BL'== EL[:12] for EL in t_survey['TEMPLATE'] ])
eg = n.array([ 'Qmost_LRG' == EL[:9] for EL in t_survey['TEMPLATE'] ])

print( nl(t1), nl(t2), nl(eg) )
print( nl(t1 | t2 | eg) )

t_survey.remove_column('TEMPLATE')
t_survey['TEMPLATE'] = "ROSAT_AGNT1-DR16QSO-stitched-stack-qmost-template.fits"
t_survey['TEMPLATE'][t2] = "ROSAT_AGNT2-highZ-stitched-stack-qmost-template.fits"
t_survey['TEMPLATE'][t1] = "ROSAT_AGNT1-DR16QSO-stitched-stack-qmost-template.fits"
t_survey['TEMPLATE'][eg] = "DR16LRG-stitched-stack-qmost-template.fits"

keep1 = ( t1 ) & (t_survey['REDSHIFT_ESTIMATE']<4)
keep2 = ( t2 ) & (t_survey['REDSHIFT_ESTIMATE']<1.5)
keep3 = ( eg ) & (t_survey['REDSHIFT_ESTIMATE']<1.)
keep = (keep1) | (keep2) | (keep3)
t_survey[keep].write(path_2_out, overwrite = True)


'DR16_ELG-stitched-stack-qmost-template.fits'
'DR16LRG-stitched-stack-qmost-template.fits'
'ROSAT_AGNT1-DR16QSO-stitched-stack-qmost-template.fits'
'ROSAT_AGNT2-highZ-stitched-stack-qmost-template.fits'

sys.exit()

t_survey = t_survey[ ( t_survey['RA'] < 359.99 ) ]

N_obj = len(t_survey)
t_out = Table()
#  limit size of the string columns to the size of the longer string in the corresponding columns.
# 'NAME':str, max 256 char
t_out.add_column(Column(name='NAME', data=t_survey['NAME'], unit=''))
# 'RA':n.float64, 1D
# 'DEC':n.float64, 1D
t_out.add_column(Column(name='RA', data=t_survey['RA'].astype('float64'), unit='deg'))
t_out.add_column(Column(name='DEC', data=t_survey['DEC'].astype('float64'), unit='deg'))
# 'PMRA':n.float32, 1E
# 'PMDEC':n.float32, 1E
# 'EPOCH':n.float32, 1E
PMRA = n.zeros(N_obj)
t_out.add_column(Column(name='PMRA', data=PMRA.astype('float32'), unit='mas/yr'))
PMDEC = n.zeros(N_obj)
t_out.add_column(Column(name='PMDEC', data=PMDEC.astype('float32'), unit='mas/yr'))
EPOCH = n.ones(N_obj)*2015.5
t_out.add_column(Column(name='EPOCH', data=EPOCH.astype('float32'), unit='yr'))
# 'RESOLUTION':n.int16, 1I
RESOLUTION = n.ones(N_obj).astype('int16')
t_out.add_column(Column(name='RESOLUTION', data=RESOLUTION, unit=''))
# 'SUBSURVEY':str, max 256 char
t_out.add_column(Column(name='SUBSURVEY', data=t_survey['SUBSURVEY'], unit=''))

NEWtemplates = n.array([ 'Q'+el[1:] for el in t_survey['TEMPLATE'] ])

# 'TEMPLATE':str, max 256 char
t_out.add_column(Column(name='TEMPLATE', data=NEWtemplates, unit=''))
# 'RULESET':str, max 256 char
 #AGN_ALL_3PC
#AGN_ALL_SN10

NEWrulesets = n.array([ el+'_NOV21' for el in t_survey['RULESET'] ])

t_out.add_column(Column(name='RULESET', data=NEWrulesets, unit=''))
# 'REDSHIFT_ESTIMATE':n.float32, 1E
# 'REDSHIFT_ERROR':n.float32, 1E
t_out.add_column(Column(name='REDSHIFT_ESTIMATE', data=t_survey['REDSHIFT_ESTIMATE'].astype('float32'), unit=''))
t_out.add_column(Column(name='REDSHIFT_ERROR', data=(0.5*n.ones(N_obj)).astype('float32'), unit=''))
# 'MAG':n.float32,
# 'MAG_ERR':n.float32
# 'MAG_TYPE': str max 256 char
t_out.add_column(Column(name='MAG', data=t_survey['MAG'].astype('float32'), unit='mag'))
t_out.add_column(Column(name='MAG_ERR', data = (0.01 * n.ones(N_obj)).astype('float32'), unit='mag'))
t_out.add_column(Column(name='MAG_TYPE', data=t_survey['MAG_TYPE'], unit=''))
# 'REDDENING':n.float32, 1E
t_out.add_column(Column(name='REDDENING',data=t_survey['REDDENING'].astype('float32'), unit=''))
# 'DATE_EARLIEST':n.float64, JulianDate decimal days # 01-Nov-2022
# 'DATE_LATEST':n.float64, JulianDate decimal days # 02-Feb-2033
t_out.add_column(Column(name='DATE_EARLIEST',data = n.zeros(N_obj).astype('float64'), unit='d'))
t_out.add_column(Column(name='DATE_LATEST'  ,data = n.zeros(N_obj).astype('float64'), unit='d'))
CADENCE = n.zeros(N_obj).astype('int64')
t_out.add_column(Column(name='CADENCE', data=CADENCE, unit=''))
# extent flags and parameters
# 'EXTENT_FLAG': 1I
# =1
# 'EXTENT_PARAMETER': 1E
# =0
# 'EXTENT_INDEX': 1E
# =0
t_out.add_column(Column(name='EXTENT_FLAG'     , data=n.zeros(N_obj).astype('int16') , unit=''))
t_out.add_column(Column(name='EXTENT_PARAMETER', data=n.zeros(N_obj).astype('float32') , unit='arcsec'))
t_out.add_column(Column(name='EXTENT_INDEX'    , data=n.zeros(N_obj).astype('float32') , unit=''))


t_out.write(path_2_out, overwrite = True)


