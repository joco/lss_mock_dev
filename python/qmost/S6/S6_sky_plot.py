import os, glob, sys
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm 
from matplotlib import rc

from astropy.io import fits
from astropy.coordinates import SkyCoord
import astropy.units as u
import numpy as np
import healpy as hp


def mk_radec_plot(ra,dec,nside=32,filename=' ', title='', subN=1):
    #
    # ra,dec in degrees
    # nside is the healpix parameter setting the size of pixels, must be a power of 2, less than 2**30
    #
	npix = hp.nside2npix(nside)
	pixarea = hp.nside2pixarea(nside, degrees = True)
	print('='*100)
	print("Pixel area [deg^2]: ",pixarea)
	# convert ra,dec to HEALPix indices and count
	indices = hp.ang2pix(nside, ra, dec, lonlat=True)
	idx, counts = np.unique(indices, return_counts=True)
	# fill the fullsky map
	hpx_map = np.zeros(npix, dtype=int)
	hpx_map[idx]      = counts/pixarea
	print([np.min(hpx_map[idx]), np.max(hpx_map[idx]), np.mean(hpx_map[idx]), np.std(hpx_map[idx])]/pixarea)
	print('='*100)
	# set some fonts
	rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
	## for Palatino and other serif fonts use:
	#rc('font',**{'family':'serif','serif':['Palatino']})
	#rc('text', usetex=True)
	#rc('mathtext.fontset','stixsans')
	# define colour map
	cmap = plt.cm.inferno
	cmap.set_bad('w')
	cmap.set_under('w')
	# make the map with meridians overplotted
	hp.mollview(hpx_map, 
				coord='C', rot=(180,0),
				unit=r' Object counts per degree$^2$', xsize = 1000, cmap=cmap, min=1, norm='log', cbar=None)
	hp.graticule(dpar=15, dmer=20, verbose= True)
	# HA labels
	params = {'mathtext.default': 'regular' }          
	plt.rcParams.update(params)
	#hp.projtext(20,  32,  '$\mathrm{\mathsf{0^h}}$',     color = 'black', fontsize = 16, lonlat=True)
	#hp.projtext(88,  32,  '$\mathrm{\mathsf{6^h}}$',     color = 'black', fontsize = 16, lonlat=True)
	#hp.projtext(177, 32,  '$\mathrm{\mathsf{12^h}}$',    color = 'black', fontsize = 16, lonlat=True) #rotation
	#hp.projtext(264, 32,  '$\mathrm{\mathsf{18^h}}$',    color = 'black', fontsize = 16, lonlat=True)
	#hp.projtext(351, 32,  '$\mathrm{\mathsf{24^h}}$',    color = 'black', fontsize = 16, lonlat=True)
	hp.projtext(20,  32,  '$\mathrm{\mathsf{0^\circ}}$',     color = 'black', fontsize = 16, lonlat=True)
	hp.projtext(88,  32,  '$\mathrm{\mathsf{90^\circ}}$',     color = 'black', fontsize = 16, lonlat=True)
	hp.projtext(177, 32,  '$\mathrm{\mathsf{180^\circ}}$',    color = 'black', fontsize = 16, lonlat=True) #rotation
	hp.projtext(264, 32,  '$\mathrm{\mathsf{270^\circ}}$',    color = 'black', fontsize = 16, lonlat=True)
	hp.projtext(351, 32,  '$\mathrm{\mathsf{360^\circ}}$',    color = 'black', fontsize = 16, lonlat=True)
	plt.title(title, size=14)
	ax = plt.gca()
	im = ax.get_images()[0]
	# DEC labels
	fig= plt.gcf()
	for ax in fig.get_axes():
		if type(ax) is hp.projaxes.HpxMollweideAxes:
			ax.set_ylim(-1, 0.51)
			ax.set_position([0.02, 0.03, 0.94, 0.95])
			ax.annotate(r'$\, \mathrm{\mathsf{ 0^\circ}}$', xy=(2.04,-0.02),    size=16, annotation_clip=False) 
			ax.annotate(r'$\, \mathrm{\mathsf{ 30^\circ}}$', xy=(1.90,0.38),     size=16) 
			ax.annotate(r'$\, \mathrm{\mathsf{-30^\circ}}$', xy=(1.86,-0.42),    size=16) 
			ax.annotate(r'$\, \mathrm{\mathsf{-60^\circ}}$', xy=(1.38, -0.78),   size=16) 
			ax.annotate(r'%s targets' %len(ra), xy=(-2.0, -0.92),  size=14) # optional, can be removed

	# create colour bar
	cbaxes = fig.add_axes([0.1, 0.15, 0.8, 0.04]) # [left, bottom, width, height]
	if subN == 0 :
		tks=[1, 2, 5, 10, 20, 50, 100, 200]#, 500, 1000, 10000, 100000, 1000000, 10000000]
	if subN == 1 :
		tks=[ 1, 2, 5, 10, 20, 50, 100]#, 500, 1000, 10000, 100000, 1000000, 10000000]
	if subN == 2 :
		tks=[ 1, 2, 5, 10, 20, 50]#, 500, 1000, 10000, 100000, 1000000, 10000000]
	if subN == 3 :
		tks=[ 1, 2, 5, 10, 20, 50]#, 500, 1000, 10000, 100000, 1000000, 10000000]
	if subN == 4 :
		tks=[ 1, 2, 5, 10, 20, 50, 100]#, 500, 1000, 10000, 100000, 1000000, 10000000]
	if subN == 5 :
		tks=[1, 2, 5, 10, 20, 50, 100, 200, 500]#, 500, 1000, 10000, 100000, 1000000, 10000000]
	cb = plt.colorbar(im,  orientation='horizontal', cax = cbaxes, ticks=tks)
	cb.set_label('Object counts per degree$\mathrm{\mathsf{^2}}$', fontsize=14)
	cb.ax.tick_params(labelsize=16)
	#plt.xlim([0,1])
	# save plot to PDF file
	plt.savefig(filename + '.pdf')
	plt.savefig(filename + '.png')

survey = 'S6'
working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'qmost', survey)
catalogue_dir = os.path.join(os.environ['HOME'], 'sf_Shared/data/4most', 's6', '2023_march')
out_name1 = os.path.join(catalogue_dir, 'S6_09Mar2023.fits')
# name of the sub survey for which the LSM is created
sub_survey_names = np.array([ 'AGN_WIDE', 'AGN_DEEP', 'AGN_IR', 'AGN_HIGHZ', 'PAQS' ])
# loads the catalog
data = fits.open(out_name1)[1].data

for jj, sub_name in enumerate(sub_survey_names):
	sub_1 = (data['SUBSURVEY']==sub_name)
	ra  = data['RA'][sub_1]
	dec = data['DEC'][sub_1]
	print(sub_name, len(ra))
	mk_radec_plot(ra,dec,filename= os.path.join(working_dir, 'sky_'+sub_name), title=sub_name, subN = jj )

ra  = data['RA']
dec = data['DEC']
mk_radec_plot(ra,dec,filename= os.path.join(working_dir, 'S6_4MOST_09Mar2023_sky_all'), title='S6 all targets', subN=5 )
