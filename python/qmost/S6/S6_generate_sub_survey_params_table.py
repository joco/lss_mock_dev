"""
4MOST 4FS_WI SSM script

python script

Generate a fits file containing an example Small Scale Merit SSM

python dependencies:
 - os, sys, numpy, scipy
 - astropy

> pip install ...

It creates a SSM file that interpolates the SSM function required for a sub survey.

"""
import os, sys
import numpy as np

import astropy.io.fits as fits

survey = 'S6'
working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'qmost', survey)
# name of the sub survey for which the LSM is created
sub_survey_names = np.array([ 'AGN_WIDE', 'AGN_DEEP', 'AGN_IR', 'AGN_HIGHZ', 'PAQS' ])
# loads the catalog
t_exp_max = np.array([ 120.  , 240.  , 120. , 120.  , 120.      ])
goal_area = np.array([ 10000., 1000., 10000., 5000., 1863.9033 ])
#area_MAX = np.array([ 11928.04, 1493.94, 13150.05, 5549.41, 2329.88 ])
#num_ALL      =  np.array([1078087,   21832,  172819,   16388,  191068])
num_required =  np.array([725_000,   15_000,  110_000,   12_000,  110_000])

#sub_survey_name = sub_survey_names[0]
#tmax_value = t_exp_max[0]
#area_required = goal_area[0]

################################################
################################################
################################################
# AREQ, TMAX
################################################
################################################
################################################

def create_files(sub_survey_name, tmax_value, area_required, num_req):
	print(sub_survey_name, tmax_value, area_required, num_req)
	# name of the output file
	out_file_TMAX = os.path.join(working_dir, survey +'_TMAX_'+str(sub_survey_name)+'_09Mar2023.fits')
	out_file_AREQ = os.path.join(working_dir, survey +'_AREQ_'+str(sub_survey_name)+'_09Mar2023.fits')
	#plot_file = os.path.join(working_dir,survey +'_LSM_'+str(sub_survey_name)+'.png')

	cols = fits.ColDefs([
		fits.Column( "SUBSURVEY"	 ,unit='' ,format="20A", array=np.array([sub_survey_name])), 
		fits.Column( "TMAX"         ,unit='min',format="1E" , array=np.array([tmax_value]) ),
		fits.Column( "TMAX_SKY_COND",unit=''   ,format="1A" , array=np.array(["D"]) )
		])
	tbhdu = fits.BinTableHDU.from_columns(cols)
	tbhdu.header['SURVEY'] = 'S06'
	tbhdu.header['HIERARCH SUBSURVEY'] = sub_survey_name
	print( out_file_TMAX )
	if os.path.isfile(out_file_TMAX):
		os.system("rm "+out_file_TMAX)
	tbhdu.writeto(out_file_TMAX)

	cols = fits.ColDefs([
		fits.Column( "SUBSURVEY"	 ,unit=''     ,format="256A", array=np.array([sub_survey_name])),
		fits.Column( "AREA_REQ"      ,unit='deg2' ,format="1E", array=np.array([area_required]) ),
		fits.Column( "NUM_REQ"      ,unit='' ,format="K", array=np.array([num_req]) )
		])
	tbhdu = fits.BinTableHDU.from_columns(cols)
	tbhdu.header['SURVEY'] = 'S06'
	tbhdu.header['HIERARCH SUBSURVEY'] = sub_survey_name
	print( out_file_AREQ )
	if os.path.isfile(out_file_AREQ):
		os.system("rm "+out_file_AREQ)
	tbhdu.writeto(out_file_AREQ)

for sub_survey_name, tmax_value, area_required, num_req in zip(sub_survey_names,t_exp_max, goal_area, num_required ):
	create_files(sub_survey_name, tmax_value, area_required, num_req)

lsm_files  = np.array([ survey +'_LSM_'+str(sub_survey_name)+'_09Mar2023.fits' for sub_survey_name in sub_survey_names ])
ssm_files  = np.array([ survey +'_SSM_'+str(sub_survey_name)+'_09Mar2023.fits' for sub_survey_name in sub_survey_names ])
tmax_files = np.array([ survey +'_TMAX_'+str(sub_survey_name)+'_09Mar2023.fits' for sub_survey_name in sub_survey_names ])
areq_files = np.array([ survey +'_AREQ_'+str(sub_survey_name)+'_09Mar2023.fits' for sub_survey_name in sub_survey_names ])

out_file_PARA = os.path.join(working_dir, survey +'_SUBSURVEY_PARAMS_09Mar2023.fits')

cols = fits.ColDefs([
	fits.Column( "SUBSURVEY"	  ,unit='' ,format="256A", array = sub_survey_names),
	fits.Column( "LSM_FILENAME"   ,unit='' ,format="256A", array  = lsm_files  ),
	fits.Column( "SSM_FILENAME"   ,unit='' ,format="256A", array  = ssm_files  ),
	fits.Column( "TMAX_FILENAME"  ,unit='' ,format="256A", array = tmax_files ),
	fits.Column( "AREQ_FILENAME"  ,unit='' ,format="256A", array = areq_files ),
	])
tbhdu = fits.BinTableHDU.from_columns(cols)
#tbhdu.header['author'] = 'JC'
print( out_file_PARA )
if os.path.isfile(out_file_PARA):
	os.system("rm "+out_file_PARA)
tbhdu.writeto(out_file_PARA)
 

