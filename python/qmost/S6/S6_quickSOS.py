import os, sys
import numpy as np
import healpy
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt
import astropy.io.fits as fits
from matplotlib.path import Path 
from astropy.coordinates import SkyCoord
from astropy.table import Table, Column

survey = 'S6'
working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'qmost', survey)
catalogue_dir = os.path.join(os.environ['HOME'], 'data', '4most', survey)
# name of the sub survey for which the LSM is created
sub_survey_names = np.array([ 'AGN_WIDE', 'AGN_DEEP', 'AGN_IR' ])
# loads the catalog
data = fits.open(os.path.join(catalogue_dir, 'S6_4MOST_ALL_SNR3_IR215_14Apr21_correct_output_ETC2021-04-16_16_44_54.fits'))[1].data
# output cat 
p_2_out = os.path.join(catalogue_dir, 'S6_4MOST_ALL_SNR3_IR215_14Apr21_FIBERMAGcut_225.fits')
selections = np.array([(data['SUBSURVEY']==sub_survey_name) for sub_survey_name in sub_survey_names ])
comps = np.array([ 0.5/0.625, 0.5**(1./6.5), 0.5/0.625])

t_exp_max = np.array([ 120., 240., 120. ])
goal_area =  np.array([ 10000., 1000., 10000. ])
nl = lambda sel : len(sel.nonzero()[0])

N_in_subsurvey = np.array([nl(sel) for sel in selections ])
#min_fibermag = np.array([np.min(data['FIBER_MAG'][sel]) for sel in selections ])
#mea_fibermag = np.array([np.mean(data['FIBER_MAG'][sel]) for sel in selections ])
#max_fibermag = np.array([np.max(data['FIBER_MAG'][sel]) for sel in selections ])

texp_d = np.max([data['TEXP_D'], np.ones_like(data['TEXP_D'])*10], axis=0)
texp_g = np.max([data['TEXP_G'], np.ones_like(data['TEXP_D'])*10], axis=0)
texp_b = np.max([data['TEXP_B'], np.ones_like(data['TEXP_D'])*7] , axis=0)

num_tot = lambda fiber_mag_max : np.array([nl((sel)&(data['NO_SEEING_FIBER_MAG']<fib_max))  for sel, fib_max in zip(selections, fiber_mag_max) ])

texp_tot = lambda fiber_mag_max : np.array([np.sum(np.min([texp_d, np.ones_like(texp_d)*t_max], axis=0)[sel & (data['NO_SEEING_FIBER_MAG']<fib_max)])*cp*0.75/60/1e6 for sel, fib_max, cp, t_max in zip(selections, fiber_mag_max, comps, t_exp_max) ])

texpB_tot = lambda fiber_mag_max : np.array([np.sum(np.min([texp_b, np.ones_like(texp_d)*t_max], axis=0)[sel & (data['NO_SEEING_FIBER_MAG']<fib_max)])*cp*0.75/60/1e6 for sel, fib_max, cp, t_max in zip(selections, fiber_mag_max, comps, t_exp_max) ])

texpG_tot = lambda fiber_mag_max : np.array([np.sum(np.min([texp_g, np.ones_like(texp_d)*t_max], axis=0)[sel & (data['NO_SEEING_FIBER_MAG']<fib_max)])*cp*0.75/60/1e6 for sel, fib_max, cp, t_max in zip(selections, fiber_mag_max, comps, t_exp_max) ])

fiber_mag_max = np.array([ 24.5, 24.5, 24.5 ])
texpDTMatrix = np.array([ texp_tot(np.array([ 1., 1., 1. ])*MAG) for MAG in np.arange(18, 24, 0.5) ])
for el1, el2 in zip(np.arange(18, 24, 0.5), texpDTMatrix):
	print(el1, num_tot(np.array([ 1., 1., 1. ])*el1), el2, np.sum(el2))

fiber_mag_max = np.array([ 22.5, 22.5, 22.5 ])
numbers = num_tot(fiber_mag_max)
print(numbers)
out = texp_tot(fiber_mag_max)
print('D',out, out.sum())
out = texpG_tot(fiber_mag_max)
print('G',out, out.sum())
out = texpB_tot(fiber_mag_max)
print('B',out, out.sum())

selections_FIBMAG = np.array([(data['SUBSURVEY']==sub_survey_name)&(data['NO_SEEING_FIBER_MAG']<fib_max) for sub_survey_name, fib_max in zip(sub_survey_names, fiber_mag_max) ])

plt.figure(0, (6,6))
plt.plot(data['NO_SEEING_FIBER_MAG'][selections[2]==False], data['TEXP_D'][selections[2]==False], 'k+', label='S/N>2-3', rasterized=True)
plt.plot(data['NO_SEEING_FIBER_MAG'][selections[1]], data['TEXP_D'][selections[1]], 'r+', label='S/N>15', rasterized=True)
plt.grid()
#plt.ylim((-90,40))
#plt.xlim((-1,361))
plt.xlabel('fiber mag')
plt.ylabel('exposure time (D, minutes)')
plt.yscale('log')
plt.tight_layout()
plt.legend(loc=0)
plt.savefig(os.path.join(working_dir,survey +'_texp_fiberMag.png'))
plt.clf()

final_selection = np.any(selections_FIBMAG, axis=0) 
out_data = data[final_selection]

# Re-assigns ruleset

#data['RULESET'][(data['NO_SEEING_FIBER_MAG']<20)] = 'Red_SN10'
#data['RULESET'][(data['NO_SEEING_FIBER_MAG']>=20)&(data['NO_SEEING_FIBER_MAG']<21)] = 'Red_SN05'
#data['RULESET'][(data['NO_SEEING_FIBER_MAG']>=21)&(data['NO_SEEING_FIBER_MAG']<22)] = 'Red_SN03'
#data['RULESET'][(data['NO_SEEING_FIBER_MAG']>=22)] = 'Red_SN02'


t_survey = Table()
for name_i, format_i, unit_i in zip(out_data.columns.names, out_data.columns.formats, out_data.columns.units):
	t_survey.add_column(Column(name=name_i   , data=out_data[name_i] , unit=unit_i))

t_survey.remove_columns(['CADENCE', 'AIRMASS_EXACT', 'AIRMASS', 'SEEING', 'SEEING_EXACT', 'FIBER_MAG', 'NO_SEEING_FIBER_MAG', 'TEXP_D', 'TEXP_G', 'TEXP_B'])

N_obj = len(t_survey)
CADENCE = np.zeros(N_obj).astype('int64')
t_survey.add_column(Column(name='CADENCE', data=CADENCE, unit='', format='K'))
t_survey['DATE_EARLIEST'][:]=0
t_survey['DATE_LATEST'][:]=0

t_survey.write (p_2_out, overwrite=True)
