"""
S5 SSM

It creates a SSM file that interpolates the SSM function required for a sub survey.

DEEP, IR: 80%, 80% 
WIDE: 85%
fun = lambda p0 : 0.5**p0
"""
import os, sys
import numpy as np
from scipy.special import erf
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt
import astropy.io.fits as fits
from scipy.interpolate import interp1d

survey = 'S6'
working_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'qmost', survey)
# name of the sub survey for which the LSM is created
sub_survey_names = np.array([ 'AGN_WIDE', 'AGN_DEEP', 'AGN_IR', 'AGN_HIGHZ', 'PAQS'  ])

def create_SSM_file(sub_survey_name):
	# name of the output file
	out_file = os.path.join(working_dir, survey +'_SSM_'+str(sub_survey_name)+'_09Mar2023.fits')
	plot_file = os.path.join(working_dir,survey +'_SSM_'+str(sub_survey_name)+'_09Mar2023.png')
	#########
	#########
	# 1. First create the grid of pixels to interpolate on
	#########
	#########
	x = np.arange(0.0, 1.01, 0.01)
	ssm_values = np.zeros(len(x))
	#########
	#########
	# 2. Choose the SSM function that fits your needs
	#########
	#########
	f2 = lambda x, p0: x**p0 
	if sub_survey_name == 'AGN_WIDE' : 
		#p0 = 4.3
		f3 = lambda x, p1: p1*x
		p1 = 0.625
		ssm_fun = lambda x : f3(x, p1)
		ssm_values = ssm_fun(x)

	if sub_survey_name == 'AGN_IR' : 
		#p0 = 3.2
		f3 = lambda x, p1: p1*x
		p1 = 0.625
		ssm_fun = lambda x : f3(x, p1)
		ssm_values = ssm_fun(x)

	if sub_survey_name == 'AGN_DEEP' :
		p0 = 6.5
		ssm_fun = lambda x : f2(x, p0)
		ssm_values = ssm_fun(x)

	if sub_survey_name == 'AGN_HIGHZ' :
		p0 = 6.5
		ssm_fun = lambda x : f2(x, p0)
		ssm_values = ssm_fun(x)

	if sub_survey_name == 'PAQS' :
		ssm_values = np.zeros_like(x)
		comp = x
		C_req = 0.7
		ssm_values = 0.5 + 0.5 / ( 1. - C_req ) * comp - 0.5 / ( 1. - C_req)*C_req
		# for low completeness :
		ssm_values[comp <= C_req] = comp[comp <= C_req] * 0.5 / C_req
	#########
	#########
	# 2. Assigns SSM values
	#########
	#########
	ssm_values[ssm_values<0] = 0
	ssm_values[ssm_values>1] = 1
	ssm_values[-1] = 1
	itp = interp1d(ssm_values, x)
	print('SSM values:',itp(0.45), itp(0.5), itp(0.55))
	cols = fits.ColDefs([
		fits.Column( "completeness"	 ,unit='' ,format="E", array=x),
		fits.Column( "SSM"           ,unit='' ,format="E", array=ssm_values )
		])
	tbhdu = fits.BinTableHDU.from_columns(cols)
	tbhdu.header['SURVEY'] = 'S06'
	tbhdu.header['HIERARCH SUBSURVEY'] = sub_survey_name
	#tbhdu.header['p0'] = p0
	if os.path.isfile(out_file):
		os.remove(out_file)
	tbhdu.writeto(out_file)
	# Creates the Figure
	plt.figure(0, (6,6))
	plt.plot(x, ssm_values)
	plt.grid()
	plt.ylim((-0.05,1.05))
	plt.xlim((-0.05,1.05))
	plt.xlabel('Completeness (fraction)')
	plt.ylabel('Small scale merit value')
	plt.title(sub_survey_name)
	plt.savefig(plot_file)
	plt.clf()

for sub_survey_name in sub_survey_names:
	print(sub_survey_name)
	create_SSM_file(sub_survey_name)
