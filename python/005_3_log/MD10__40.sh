#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_3_4most_catalog.py MD10 560 
python3 005_3_4most_catalog.py MD10 561 
python3 005_3_4most_catalog.py MD10 562 
python3 005_3_4most_catalog.py MD10 563 
python3 005_3_4most_catalog.py MD10 564 
python3 005_3_4most_catalog.py MD10 565 
python3 005_3_4most_catalog.py MD10 566 
python3 005_3_4most_catalog.py MD10 567 
python3 005_3_4most_catalog.py MD10 568 
python3 005_3_4most_catalog.py MD10 569 
python3 005_3_4most_catalog.py MD10 570 
python3 005_3_4most_catalog.py MD10 571 
python3 005_3_4most_catalog.py MD10 572 
python3 005_3_4most_catalog.py MD10 573 
 
