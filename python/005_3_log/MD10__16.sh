#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_3_4most_catalog.py MD10 224 
python3 005_3_4most_catalog.py MD10 225 
python3 005_3_4most_catalog.py MD10 226 
python3 005_3_4most_catalog.py MD10 227 
python3 005_3_4most_catalog.py MD10 228 
python3 005_3_4most_catalog.py MD10 229 
python3 005_3_4most_catalog.py MD10 230 
python3 005_3_4most_catalog.py MD10 231 
python3 005_3_4most_catalog.py MD10 232 
python3 005_3_4most_catalog.py MD10 233 
python3 005_3_4most_catalog.py MD10 234 
python3 005_3_4most_catalog.py MD10 235 
python3 005_3_4most_catalog.py MD10 236 
python3 005_3_4most_catalog.py MD10 237 
 
