#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_3_4most_catalog.py MD10 504 
python3 005_3_4most_catalog.py MD10 505 
python3 005_3_4most_catalog.py MD10 506 
python3 005_3_4most_catalog.py MD10 507 
python3 005_3_4most_catalog.py MD10 508 
python3 005_3_4most_catalog.py MD10 509 
python3 005_3_4most_catalog.py MD10 510 
python3 005_3_4most_catalog.py MD10 511 
python3 005_3_4most_catalog.py MD10 512 
python3 005_3_4most_catalog.py MD10 513 
python3 005_3_4most_catalog.py MD10 514 
python3 005_3_4most_catalog.py MD10 515 
python3 005_3_4most_catalog.py MD10 516 
python3 005_3_4most_catalog.py MD10 517 
 
