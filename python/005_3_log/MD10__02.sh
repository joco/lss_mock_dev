#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_3_4most_catalog.py MD10 28 
python3 005_3_4most_catalog.py MD10 29 
python3 005_3_4most_catalog.py MD10 30 
python3 005_3_4most_catalog.py MD10 31 
python3 005_3_4most_catalog.py MD10 32 
python3 005_3_4most_catalog.py MD10 33 
python3 005_3_4most_catalog.py MD10 34 
python3 005_3_4most_catalog.py MD10 35 
python3 005_3_4most_catalog.py MD10 36 
python3 005_3_4most_catalog.py MD10 37 
python3 005_3_4most_catalog.py MD10 38 
python3 005_3_4most_catalog.py MD10 39 
python3 005_3_4most_catalog.py MD10 40 
python3 005_3_4most_catalog.py MD10 41 
 
