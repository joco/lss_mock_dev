#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_3_4most_catalog.py MD10 518 
python3 005_3_4most_catalog.py MD10 519 
python3 005_3_4most_catalog.py MD10 520 
python3 005_3_4most_catalog.py MD10 521 
python3 005_3_4most_catalog.py MD10 522 
python3 005_3_4most_catalog.py MD10 523 
python3 005_3_4most_catalog.py MD10 524 
python3 005_3_4most_catalog.py MD10 525 
python3 005_3_4most_catalog.py MD10 526 
python3 005_3_4most_catalog.py MD10 527 
python3 005_3_4most_catalog.py MD10 528 
python3 005_3_4most_catalog.py MD10 529 
python3 005_3_4most_catalog.py MD10 530 
python3 005_3_4most_catalog.py MD10 531 
 
