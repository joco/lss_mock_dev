"""
HAM procedure for Uchuu between tabulated AGN files and the light cone

reads tabulated AGNs

gathers all galaxies in same redshift bin (delta=0.05) and their stellar mass

python 003_1_AGN_model_uchuu.py 0.05
python 003_1_AGN_model_uchuu.py 0.1
python 003_1_AGN_model_uchuu.py 0.15
python 003_1_AGN_model_uchuu.py 0.2
python 003_1_AGN_model_uchuu.py 0.25
python 003_1_AGN_model_uchuu.py 0.3
python 003_1_AGN_model_uchuu.py 0.35
python 003_1_AGN_model_uchuu.py 0.4
python 003_1_AGN_model_uchuu.py 0.45
python 003_1_AGN_model_uchuu.py 0.5
python 003_1_AGN_model_uchuu.py 0.55
python 003_1_AGN_model_uchuu.py 0.6
python 003_1_AGN_model_uchuu.py 0.65
python 003_1_AGN_model_uchuu.py 0.7
python 003_1_AGN_model_uchuu.py 0.75
python 003_1_AGN_model_uchuu.py 0.8
python 003_1_AGN_model_uchuu.py 0.85
python 003_1_AGN_model_uchuu.py 0.9
python 003_1_AGN_model_uchuu.py 0.95
python 003_1_AGN_model_uchuu.py 1.0
python 003_1_AGN_model_uchuu.py 1.05
python 003_1_AGN_model_uchuu.py 1.1
python 003_1_AGN_model_uchuu.py 1.15
python 003_1_AGN_model_uchuu.py 1.2
python 003_1_AGN_model_uchuu.py 1.25
python 003_1_AGN_model_uchuu.py 1.3
python 003_1_AGN_model_uchuu.py 1.35
python 003_1_AGN_model_uchuu.py 1.4
python 003_1_AGN_model_uchuu.py 1.45
python 003_1_AGN_model_uchuu.py 1.5
python 003_1_AGN_model_uchuu.py 1.55
python 003_1_AGN_model_uchuu.py 1.6
python 003_1_AGN_model_uchuu.py 1.65
python 003_1_AGN_model_uchuu.py 1.7
python 003_1_AGN_model_uchuu.py 1.75
python 003_1_AGN_model_uchuu.py 1.8
python 003_1_AGN_model_uchuu.py 1.85
python 003_1_AGN_model_uchuu.py 1.9
python 003_1_AGN_model_uchuu.py 1.95
python 003_1_AGN_model_uchuu.py 2.0
python 003_1_AGN_model_uchuu.py 2.05
python 003_1_AGN_model_uchuu.py 2.1
python 003_1_AGN_model_uchuu.py 2.15
python 003_1_AGN_model_uchuu.py 2.2
python 003_1_AGN_model_uchuu.py 2.25
python 003_1_AGN_model_uchuu.py 2.3
python 003_1_AGN_model_uchuu.py 2.35
python 003_1_AGN_model_uchuu.py 2.4
python 003_1_AGN_model_uchuu.py 2.45
python 003_1_AGN_model_uchuu.py 2.5
python 003_1_AGN_model_uchuu.py 2.55
python 003_1_AGN_model_uchuu.py 2.6
python 003_1_AGN_model_uchuu.py 2.65
python 003_1_AGN_model_uchuu.py 2.7
python 003_1_AGN_model_uchuu.py 2.75
python 003_1_AGN_model_uchuu.py 2.8
python 003_1_AGN_model_uchuu.py 2.85
python 003_1_AGN_model_uchuu.py 2.9
python 003_1_AGN_model_uchuu.py 2.95
python 003_1_AGN_model_uchuu.py 3.0
python 003_1_AGN_model_uchuu.py 3.05
python 003_1_AGN_model_uchuu.py 3.1
python 003_1_AGN_model_uchuu.py 3.15
python 003_1_AGN_model_uchuu.py 3.2
python 003_1_AGN_model_uchuu.py 3.25
python 003_1_AGN_model_uchuu.py 3.3
python 003_1_AGN_model_uchuu.py 3.35
python 003_1_AGN_model_uchuu.py 3.4
python 003_1_AGN_model_uchuu.py 3.45
python 003_1_AGN_model_uchuu.py 3.5
python 003_1_AGN_model_uchuu.py 3.55
python 003_1_AGN_model_uchuu.py 3.6
python 003_1_AGN_model_uchuu.py 3.65
python 003_1_AGN_model_uchuu.py 3.7
python 003_1_AGN_model_uchuu.py 3.75
python 003_1_AGN_model_uchuu.py 3.8
python 003_1_AGN_model_uchuu.py 3.85
python 003_1_AGN_model_uchuu.py 3.9
python 003_1_AGN_model_uchuu.py 3.95
python 003_1_AGN_model_uchuu.py 4.0
python 003_1_AGN_model_uchuu.py 4.05
python 003_1_AGN_model_uchuu.py 4.1
python 003_1_AGN_model_uchuu.py 4.15
python 003_1_AGN_model_uchuu.py 4.2
python 003_1_AGN_model_uchuu.py 4.25
python 003_1_AGN_model_uchuu.py 4.3
python 003_1_AGN_model_uchuu.py 4.35
python 003_1_AGN_model_uchuu.py 4.4
python 003_1_AGN_model_uchuu.py 4.45
python 003_1_AGN_model_uchuu.py 4.5
python 003_1_AGN_model_uchuu.py 4.55
python 003_1_AGN_model_uchuu.py 4.6
python 003_1_AGN_model_uchuu.py 4.65
python 003_1_AGN_model_uchuu.py 4.7
python 003_1_AGN_model_uchuu.py 4.75
python 003_1_AGN_model_uchuu.py 4.8
python 003_1_AGN_model_uchuu.py 4.85
python 003_1_AGN_model_uchuu.py 4.9
python 003_1_AGN_model_uchuu.py 4.95
python 003_1_AGN_model_uchuu.py 5.0
python 003_1_AGN_model_uchuu.py 5.05
python 003_1_AGN_model_uchuu.py 5.1
python 003_1_AGN_model_uchuu.py 5.15
python 003_1_AGN_model_uchuu.py 5.2
python 003_1_AGN_model_uchuu.py 5.25
python 003_1_AGN_model_uchuu.py 5.3
python 003_1_AGN_model_uchuu.py 5.35
python 003_1_AGN_model_uchuu.py 5.4
python 003_1_AGN_model_uchuu.py 5.45
python 003_1_AGN_model_uchuu.py 5.5
python 003_1_AGN_model_uchuu.py 5.55
python 003_1_AGN_model_uchuu.py 5.6
python 003_1_AGN_model_uchuu.py 5.65
python 003_1_AGN_model_uchuu.py 5.7
python 003_1_AGN_model_uchuu.py 5.75
python 003_1_AGN_model_uchuu.py 5.8
python 003_1_AGN_model_uchuu.py 5.85
python 003_1_AGN_model_uchuu.py 5.9
python 003_1_AGN_model_uchuu.py 5.95

"""
import time
t0=time.time()
import numpy as n
import os, sys, glob
print(sys.argv)
import h5py
from astropy.table import Table, Column
from astropy.coordinates import SkyCoord
import healpy
from scipy.interpolate import interp1d
import astropy.io.fits as fits
from scipy.special import erf
from scipy.stats import norm
import scipy
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u

import extinction
import astropy.constants as cc
from scipy.interpolate import interp2d
from scipy.interpolate import NearestNDInterpolator
from scipy.interpolate import interp1d


from dustmaps.planck import PlanckQuery
planck = PlanckQuery()

#path_2_NH_map = '/home/users/dae/comparat/h1_maps/H14PI/asu.fit'
path_2_NH_map = '/ptmp/joco/h1_maps/H14PI/asu.fit'
NH_DATA2 = fits.open(path_2_NH_map)[1].data
nh_val = NH_DATA2['NHI']

cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
h = 0.6774
cosmo = cosmoUNIT

z_array = n.arange(0, 15, 0.001)
dc_to_z = interp1d(cosmo.comoving_distance(z_array), z_array)

d_L = cosmo.luminosity_distance(z_array)
dl_cmXX = (d_L.to(u.cm)).value
dL_interpolation = interp1d(z_array, dl_cmXX)

l_box = 2000.0
L_box = l_box/h

##
##
## OPENS AGN FILE
##
##
log10_FX_lim = -16
env = "UCHUU"
FACTOR = 1.0
scatter_0 = 1.0

# z bins in which AGN are tabulated
z_min = n.round(float(sys.argv[1]),2)
print(z_min)
DZ=0.05
z_bins = n.arange(0.05, 6., DZ)
jj = n.argwhere( n.round(z_bins,2)==z_min)[0][0]
print(jj)
z_max = z_min + DZ
z_mean = (z_max + z_min)/2.

p_2_AGN = os.path.join( os.environ[env], 'LX_table_' + str(jj).zfill(2) + '_' +str(n.round(z_min,2))+ '.fits')
AGN = Table.read(p_2_AGN)
N_AGN_tabulated = len(AGN)
##
##
## OPENS Galaxy light cone
##
##

# determines which snapshot to open :
N_snap, Z_snap, A_snap, DC_max, DC_min, NN_replicas = n.loadtxt(os.path.join(os.environ[env], 'snap_list_with_border.txt'), unpack=True)
Z_snap_max = dc_to_z(DC_max)
Z_snap_min = dc_to_z(DC_min)

snap_selection_zmin = ( z_min >= Z_snap_min) & ( z_min <= Z_snap_max )
snap_selection_zmax = ( z_max >= Z_snap_min) & ( z_max <= Z_snap_max )


z_names = n.array([  "z0p00"
					,"z0p02"
					,"z0p05"
					,"z0p09"
					,"z0p14"
					,"z0p19"
					,"z0p25"
					,"z0p30"
					,"z0p36"
					,"z0p43"
					,"z0p49"
					,"z0p56"
					,"z0p63"
					,"z0p70"
					,"z0p78"
					,"z0p86"
					,"z0p94"
					,"z1p03"
					,"z1p12"
					,"z1p22"
					,"z1p32"
					,"z1p43"
					,"z1p54"
					,"z1p65"
					,"z1p77"
					,"z1p90"
					,"z2p03"
					,"z2p17"
					,"z2p31"
					,"z2p46"
					,"z2p62"
					,"z2p78"
					,"z2p95"
					,"z3p13"
					,"z3p32"
					,"z3p61"
					,"z3p93"
					,"z4p27"
					,"z4p63"
					,"z5p15"
					,"z5p73"
					,"z6p35"
					,"z7p03"
					,"z7p76"
					,"z8p59"
					,"z9p47"
					,"z10p44"
					,"z11p51"
					,"z12p69"
					,"z13p96" ])[::-1]

print( z_names [n.argwhere(snap_selection_zmax)[0][0]:n.argwhere(snap_selection_zmin)[0][0]+1] )
#z_name = z_names[ID_in_0]
#id_snap = n.arange(len(z_names))[::-1][ID_in_0]
#print(z_name, id_snap)
snap_list = z_names [n.argwhere(snap_selection_zmax)[0][0]:n.argwhere(snap_selection_zmin)[0][0]+1]

"""
# verification it works
for jj, z_bins_i in enumerate(z_bins):
    z_min = z_bins_i
    z_max = z_min + DZ
    print("="*100)
    p_2_out = path_2_agnOut_file + str(jj).zfill(2) + '_' +str(n.round(z_bins_i,2))+ '.fits'
    print('starts ' , p_2_out, time.time() - t0)
    snap_selection_zmin = ( z_min >= Z_snap_min) & ( z_min <= Z_snap_max )
    snap_selection_zmax = ( z_max >= Z_snap_min) & ( z_max <= Z_snap_max )
    print(n.argwhere(snap_selection_zmin)[0][0], n.argwhere(snap_selection_zmax)[0][0])
    #print(N_snap[n.argwhere(snap_selection_zmax)[0][0]:n.argwhere(snap_selection_zmin)[0][0]+1])
    print( z_names [n.argwhere(snap_selection_zmax)[0][0]:n.argwhere(snap_selection_zmin)[0][0]+1] )
"""
lc_files = []
for snap_i in snap_list:
    lc_files.append( glob.glob( os.path.join(os.environ[env], 'GPX8', snap_i, 'replication_*', 'glist.fits' ) ) )
lc_files = n.hstack(n.array(lc_files))
print(lc_files)

f_duty = interp1d(n.array([0., 0.75, 2., 3.5, 10.1]), n.array([0.1, 0.2, 0.3, 0.3, 0.3]))

def get_z_mass_id(p_2_lcfile):
    tt = fits.open(p_2_lcfile)
    ZZ = tt[1].data['redshift_R']
    MM = n.log10(tt[1].data['sm'])
    gNH = tt[1].data['nH']
    gEBV = tt[1].data['ebv']
    f_duty_realization = f_duty(ZZ)
    active = (n.random.random(size=len(ZZ)) <= f_duty_realization) & (ZZ >= z_min) & (ZZ < z_max) & (gNH>0) & (gEBV>=0)
    IDS = n.arange(len(ZZ))[active]
    print(p_2_lcfile, len(ZZ[active]))
    return IDS, ZZ[active], MM[active], gNH[active], gEBV[active]

IDS, ZZ, MM, gNH, gEBV, ID_F = [], [], [], [], [], []
for ii, lc_file in enumerate(lc_files):
    aa, bb, cc, dd, ee = get_z_mass_id(lc_file)
    IDS.append(aa)
    ZZ .append(bb)
    MM .append(cc)
    gNH.append(dd)
    gEBV.append(ee)
    ID_F.append(n.ones_like(aa)*ii)

logm = n.hstack(MM)
z    = n.hstack(ZZ)
galactic_NH  = n.hstack(gNH)
galactic_ebv = n.hstack(gEBV)
ids  = n.hstack(IDS)
idf  = n.hstack(ID_F)
mass_sort_idx = n.argsort(logm)
N_active = len(z)

# match the exact number of tabulated AGN and active galaxies

if N_AGN_tabulated < N_active  :
    # too many active galaxies (box resolution is higher than AGN tabulated
    # just populate the higher stellar mass ones in the HAM
    id_selection = mass_sort_idx [N_active - N_AGN_tabulated : ]
    logm = logm [id_selection]
    z    = z    [id_selection]
    galactic_NH = galactic_NH[id_selection]
    galactic_ebv = galactic_ebv[id_selection]
    ids  = ids  [id_selection]
    idf  = idf  [id_selection]

if N_AGN_tabulated > N_active  :
    # too many active galaxies (box resolution is higher than AGN tabulated
    # just populate the higher stellar mass ones in the HAM
    AGN = AGN[N_AGN_tabulated - N_active :]

n_agn = len(z)
dl_cm = dL_interpolation(z)
#
#
# HAM procedure
#
#

M_scatt = logm + AGN['scatter_LX_Ms']
ids_M_scatt = n.argsort(M_scatt)
# output numbers
lx = n.zeros_like(logm)
lx[ids_M_scatt] = AGN['LX_hard']
lsar = n.zeros_like(lx)
lsar[ids_M_scatt] = AGN['LX_hard'] - logm[ids_M_scatt]

# ===============================
# Obscured fractions
# ===============================
# model from equations 4-11, 12-15 of Comparat et al. 2019

# too many CTK at high luminosity
# Eq. 4
#def f_thick(LXhard, z): return 0.30
def thick_LL(z, lx0 = 41.5): return lx0 + n.arctan(z*5)*1.5
def f_thick(LXhard, z): return 0.30 * (0.5 + 0.5 * erf((thick_LL(z) - LXhard) / 0.25))

# too many absorbed ones
# Eq. 7
def f_2(LXhard, z): return 0.9 * (41 / LXhard)**0.5

# fiducial
# Eq. 8
def f_1(LXhard, z): return f_thick(LXhard, z) + 0.01 + erf(z / 4.) * 0.3

# Eq. 10
def LL(z, lx0 = 43.2): return lx0 + erf(z) * 1.2

# Eq. 5,6
def fraction_ricci(LXhard, z, width = 0.6): return f_1(LXhard,z) + (f_2(LXhard, z) - f_1(LXhard,z)) * (0.5 + 0.5 * erf((LL(z) - LXhard) / width))

# initializes logNH
logNH = n.zeros(n_agn)

# obscuration, after the equations above
randomNH = n.random.rand(n_agn)

# unobscured 20-22
#frac_thin = fraction_ricci(lsar, z)
frac_thin = fraction_ricci(lx, z)
thinest = (randomNH >= frac_thin)

# thick obscuration, 24-26
thick = (randomNH < f_thick(lx, z))
#thick = (randomNH < thick_fraction)

# obscured 22-24
obscured = (thinest == False) & (thick == False)

# assigns logNH values randomly :
logNH[thick] = n.random.uniform(24, 26, len(logNH[thick]))
logNH[obscured] = n.random.uniform(22, 24, len(logNH[obscured]))
logNH[thinest] = n.random.uniform(20, 22, len(logNH[thinest]))

# ===============================
# Assigns flux
# ===============================

NHS = n.arange(20, 26 + 0.05, 0.4)
# hard X-ray 2-10 keV rest-frame ==>> 2-10 obs frame

path_2_RF_obs_hard = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    "data",
    "xray_k_correction",
    "v3_fraction_observed_A15_RF_hard_Obs_hard_fscat_002.txt")

obscuration_z_grid, obscuration_nh_grid, obscuration_fraction_obs_erosita = n.loadtxt(
    path_2_RF_obs_hard, unpack=True)
obscuration_itp_H_H = interp2d(
    obscuration_z_grid,
    obscuration_nh_grid,
    obscuration_fraction_obs_erosita)

percent_observed_itp = interp1d(
    n.hstack((20 - 0.1, NHS, 26 + 0.1)),
    n.hstack((
        obscuration_itp_H_H(z_mean, 20.)[0],
        n.array([obscuration_itp_H_H(z_i, logNH_i)[0] for z_i, logNH_i in zip(z_mean * n.ones_like(NHS), NHS)]),
        obscuration_itp_H_H(z_mean, 26.)[0])))
percent_observed_H_H = percent_observed_itp(logNH)

lx_obs_frame_2_10 = lx + n.log10(percent_observed_H_H)
fx_2_10 = 10**(lx_obs_frame_2_10) / (4 * n.pi * (dl_cm)**2.) # / h**3
#print('fx_2_10', fx_2_10, time.time() - t0)
#print('lx_obs_frame_2_10', lx_obs_frame_2_10, time.time() - t0)

# obs X-ray 2-10 keV ==>> obs 0.5-2
# v3_fraction_observed_A15_RF_hard_Obs_soft_fscat_
# path_2_hard_RF_obs_soft
# link to X-ray K-correction and attenuation curves
path_2_hard_RF_obs_soft = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    "data",
    "xray_k_correction",
    "v3_fraction_observed_A15_RF_hard_Obs_soft_fscat_002.txt")

obscuration_z_grid, obscuration_nh_grid, obscuration_fraction_obs_erosita = n.loadtxt(path_2_hard_RF_obs_soft, unpack=True)
obscuration_itp_H_S = interp2d(
    obscuration_z_grid,
    obscuration_nh_grid,
    obscuration_fraction_obs_erosita)

percent_observed_itp = interp1d(
    n.hstack((20 - 0.1, NHS, 26 + 0.1)),
    n.hstack((
        obscuration_itp_H_S(z_mean, 20.)[0],
        n.array([obscuration_itp_H_S(z_i, logNH_i)[0] for z_i, logNH_i in zip(z_mean * n.ones_like(NHS), NHS)]),
        obscuration_itp_H_S(z_mean, 26.)[0])))

percent_observed_H_S = percent_observed_itp(logNH)


path_2_hard_RF_obs_soft_3D = n.array( glob.glob( os.path.join(
    os.environ['GIT_AGN_MOCK'],
    "data",
    "xray_k_correction",
    "v3_fraction_observed_A15_RF_hard_Obs_soft_fscat_002_GALnH_*.txt") ) )
path_2_hard_RF_obs_soft_3D.sort()
# create the 3D interpolation
grid_z, grid_nh, grid_galnh, transmission = [], [], [], []
for el in path_2_hard_RF_obs_soft_3D:
	grid_z_i, grid_nh_i, grid_galnh_i, transmission_i = n.loadtxt(el, unpack=True)
	grid_z      .append(grid_z_i      )
	grid_nh     .append(grid_nh_i     )
	grid_galnh  .append(grid_galnh_i  )
	transmission.append(transmission_i)

points = n.transpose([
	n.hstack( grid_z ),
	n.hstack( grid_nh ),
	n.hstack( grid_galnh )])

values = n.hstack( transmission)

ITP3d = NearestNDInterpolator(points, values)

percent_observed_H_S_galNH = ITP3d(z, logNH, n.log10(galactic_NH))

lx_05_20_unATT = lx + n.log10(percent_observed_H_S)
fx_05_20_log_unATT = lx_05_20_unATT - n.log10(4 * n.pi) - 2*n.log10(dl_cm)
lx_05_20 = lx + n.log10(percent_observed_H_S_galNH)
fx_05_20_log = lx_05_20 - n.log10(4 * n.pi) - 2*n.log10(dl_cm)
#fx_05_20 = 10**lx_05_20 / (4 * n.pi * (dl_cm)**2.)
#fx_05_20 = fx_2_10 * percent_observed_H_S
#lx_05_20 = fx_05_20 * (4 * n.pi * (dl_cm)**2.) # / h**3

# Adds type 11, 12, 21, 22
# Follows Merloni et al. 2014
# equation 16 of Comparat et al. 2019

def fraction_22p21_merloni(lx): return (
    0.5 + 0.5 * erf((-lx + 44.) / 0.9)) * 0.69 + 0.26

# LF in the mock, starting parameters
dlogf = 0.05
Lbin_min = 36
fbins = n.arange(Lbin_min, 48, dlogf)
xf = fbins[:-1] + dlogf / 2.

def compute_agn_type(z, lx, logNH, fbins=fbins, n_agn=n_agn):
    """
    Assigns a type to an AGN population

    parameters:
    - z: redshift
    - lx: hard X-ray luminosity (log10)
    - logNH: nH value (log10)

    return: array of AGN types
    """
    # boundary between the 22 and the 21 populations
    limit = fraction_22p21_merloni((fbins[1:] + fbins[:-1]) * 0.5)
    # selection per obscuration intensity
    nh_21 = (logNH <= 22.)
    nh_23 = (logNH > 22.)  # &(logNH<=26.)
    # initiate columns to compute
    opt_type = n.zeros(n_agn).astype('int')
    rd = n.random.rand(n_agn)
    # compute histograms of LX for different obscurations
    nall = n.histogram(lx, fbins)[0]       # all
    nth = n.histogram(lx[nh_23], fbins)[0]  # thin
    nun = n.histogram(lx[nh_21], fbins)[0]  # unobscured
    fr_thk = nth * 1. / nall  # fraction of obscured
    fr_un = nun * 1. / nall  # fraction of unobscured
    # first get the type 12: NH absorption but optically unobscured
    # to be chosen in obscured population
    n_per_bin_12 = (fr_thk - limit) * nall
    sel_12 = (n.ones(len(z)) == 0)
    for bin_low, bin_high, num_needed, nn_un in zip(
            fbins[:-1], fbins[1:], n_per_bin_12.astype('int'), nth):
        if num_needed > 0 and nn_un > 0:
            frac_needed = num_needed * 1. / nn_un
            sel_12 = (sel_12) | (
                (lx > bin_low) & (
                    lx < bin_high) & (nh_23) & (
                    rd < frac_needed))
    t_12 = (nh_23) & (sel_12)
    # second the types 21
    # to be chosen in nun
    n_per_bin_21 = (-fr_thk + limit) * nall
    sel_21 = (n.ones(len(z)) == 0)
    for bin_low, bin_high, num_needed, nn_un in zip(
            fbins[:-1], fbins[1:], n_per_bin_21.astype('int'), nun):
        if num_needed > 0 and nn_un > 0:
            frac_needed = num_needed * 1. / nn_un
            sel_21 = (sel_21) | (
                (lx > bin_low) & (
                    lx < bin_high) & (nh_21) & (
                    rd < frac_needed))
    t_21 = (nh_21) & (sel_21)
    # finally the types 11 and 22
    t_11 = (nh_21) & (t_21 == False)
    t_22 = (nh_23) & (t_12 == False)
    opt_type[t_22] = 22
    opt_type[t_12] = 12
    opt_type[t_11] = 11
    opt_type[t_21] = 21
    return opt_type


opt_type = compute_agn_type(z, lx, logNH)
#print('opt_type', opt_type, time.time() - t0)

# observed r-band magnitude from X-ray

def r_mean(log_FX0520): return -2. * log_FX0520 - 7.

def scatter_t1(n_agn_int): return norm.rvs(loc=0.0, scale=1.0, size=n_agn_int)

random_number = n.random.rand(n_agn)
empirical_mag_r = r_mean(fx_05_20_log_unATT) + scatter_t1(int(n_agn))
#print('empirical_mag_r', empirical_mag_r, time.time() - t0)

# ===============================
# EXTINCTION
# ===============================
# x ray extinction from our Galaxy
#NH_DATA = n.loadtxt(path_2_NH_attenuation, unpack=True)
#nh_law = interp1d(
    #n.hstack(
        #(-10.**25, 10**n.hstack(
            #(10., NH_DATA[0], 25)))), n.hstack(
                #(1., 1., 1. / NH_DATA[1], 0.00001)))

#attenuation = nh_law(galactic_NH[s1])
#agn_rxay_flux_05_20_observed = 10**fx_05_20_log * attenuation
#print('agn_rxay_flux_05_20_observed',agn_rxay_flux_05_20_observed,time.time() - t0)

# optical extinction, Fitzpatrick 99
ebv_values = n.hstack((n.arange(0., 5., 0.01), 10**n.arange(1, 4, 0.1)))
ext_values = n.array([extinction.fitzpatrick99(
    n.array([6231.]), 3.1 * EBV, r_v=3.1, unit='aa')[0] for EBV in ebv_values])
ext_interp = interp1d(ebv_values, ext_values)
agn_rmag_observed = empirical_mag_r + ext_interp(galactic_ebv)
#print('agn_rmag_observed', agn_rmag_observed, time.time() - t0)

# ===============================
# Writing results
# ===============================
for ii, p_2_in in enumerate(lc_files):
    p_2_out = os.path.join( os.path.dirname(p_2_in), 'AGN_list.fits')
    s1 = (idf == ii)
    f1 = Table()
    f1['ID_glist'] = ids.astype('int')[s1]
    f1['redshift_R'] = z[s1]
    f1['galaxy_SMHMR_mass'] = logm[s1]

    f1['LX_hard'] = lx                     [s1]
    f1['LX_soft'] = lx_05_20               [s1]
    f1['FX_soft'] = 10**fx_05_20_log_unATT [s1]
    f1['FX_soft_attenuated'] = 10**fx_05_20_log[s1]
    f1['FX_hard'] = fx_2_10      [s1]
    f1['logNH'] = logNH          [s1]
    f1['agn_type'] = opt_type    [s1]
    f1['random_r_AB'] = random_number [s1]
    f1['SDSS_r_AB'] = empirical_mag_r  [s1]
    f1['SDSS_r_AB_attenuated'] = agn_rmag_observed  [s1]
    f1.write(p_2_out, overwrite = True)
    print(p_2_out, 'written', time.time()-t0)
