"""
Each files has the structure:
— ‘Lproj' # line of sight projection length in kph/h
— ‘redshift’ # redshift of the snapshot 
— 'resolution [kpc/h]’ # width-length of one pixel in kpc/h 
— ‘halos’
    |— ‘a number for each halo’
        |— 'M200c', 'M200m', 'M500c', ‘M500m’ # mass in the grav-only run in 10^10 Msol/h
        |— 'hydro_M200c', 'hydro_M200m', 'hydro_M500c', ‘hydro_M500m’ #  mass in the hydro run in 10^10 Msol/h
        |— ‘has_profile’ # bool flagging if this cluster has a profile
        |— ‘dm_Sigma’ # projected mass in Gravity-only, 10^10 Msol/h (so to get a surface density, divide this by the bin area). Also note, the z~0.78 snapshot has not grav-only maps
        |— ‘hydro_Sigma’ # projected mass in hydro, 10^10 Msol/h


step 1 : attach (resscale) to a DM halo in the light cone
step 2 : predict reduced shear profile (DES, HSC, KIDS, ...)
         https://arxiv.org/pdf/2103.16212.pdf offers a complete list of what is known to date

"""

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt


import h5py

from scipy.special import erf
from astropy.table import Table, Column, vstack, hstack
import sys, os, time
import astropy.units as u
import astropy.io.fits as fits
import numpy as n
import numpy as np
from scipy.interpolate import interp1d
from scipy.interpolate import interp2d
print('CREATES WL CLUSTER FILES')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()


fig_dir = os.path.join( os.environ['GIT_AGN_MOCK'], 'figures/WL' )

env = "UNIT_fA1i_DIR" #sys.argv[1] #
pixel_size_image = 20. #float(sys.argv[2]) # 20 or 2
b_HS = 8.0 # float(sys.argv[3])
cov_mat_option = '0' #sys.argv[4]
logM500c_min = 13.0 # float(sys.argv[5]) # 13.0
logFX_min = -14.5 # float(sys.argv[6]) # 13.0

root_dir = os.path.join(os.environ[env])

path_2_out = os.path.join(root_dir, "UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS_Sept2021_withShear_v3_zSlice_0.fits")
t = Table.read(path_2_out)

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)

cosmoBox1 = FlatLambdaCDM(H0=70.4 * u.km / u.s / u.Mpc, Om0=0.272)
h = 0.704
hz = lambda redshift : cosmoBox1.H( redshift ).value/100


R_values = np.insert(np.logspace(-2, 0.6, 27), 0, 0)
xR = (R_values[:-1]+ R_values[1:])/2.

# Sigma(R), Sigma (<R)
# Figure 3 of Grandis et al.
# AT R

fig_name = 'gammat_profile_HD_at_R.png'
plt.figure(2, (7,6))

Sigma_values_dm_inR_avg = n.mean( t['gammat_profile'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(xR, Sigma_values_dm_inR_avg, label='1.6-2.7e14')
Sigma_values_dm_inR_avg = n.mean(t['gammat_profile'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(xR, Sigma_values_dm_inR_avg, label='2.7-5.3e14')
Sigma_values_dm_inR_avg = n.mean(t['gammat_profile'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(xR, Sigma_values_dm_inR_avg, label='5.3-8.3e14')
Sigma_values_dm_inR_avg = n.mean(t['gammat_profile'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(xR, Sigma_values_dm_inR_avg, label='8.3-23.6e14')
plt.grid()
#plt.xlim((4e-3, 11))
#plt.ylim((3e12, 2e16))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$\gamma(R)$')
plt.title('z=0.25, Hydro')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()


fig_name = 'kappa_profile_HD_at_R.png'
plt.figure(2, (7,6))

Sigma_values_dm_inR_avg = n.mean( t['kappa_profile'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(xR, Sigma_values_dm_inR_avg, label='1.6-2.7e14')
Sigma_values_dm_inR_avg = n.mean(t['kappa_profile'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(xR, Sigma_values_dm_inR_avg, label='2.7-5.3e14')
Sigma_values_dm_inR_avg = n.mean(t['kappa_profile'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(xR, Sigma_values_dm_inR_avg, label='5.3-8.3e14')
Sigma_values_dm_inR_avg = n.mean(t['kappa_profile'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(xR, Sigma_values_dm_inR_avg, label='8.3-23.6e14')
plt.grid()
#plt.xlim((4e-3, 11))
#plt.ylim((3e12, 2e16))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$\kappa(R)$')
plt.title('z=0.25, Hydro')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()

fig_name = 'gt_profile_HD_at_R.png'
plt.figure(2, (7,6))

Sigma_values_dm_inR_avg = n.mean( t['g_t'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(xR, Sigma_values_dm_inR_avg, label='1.6-2.7e14')
Sigma_values_dm_inR_avg = n.mean(t['g_t'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(xR, Sigma_values_dm_inR_avg, label='2.7-5.3e14')
Sigma_values_dm_inR_avg = n.mean(t['g_t'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(xR, Sigma_values_dm_inR_avg, label='5.3-8.3e14')
Sigma_values_dm_inR_avg = n.mean(t['g_t'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(xR, Sigma_values_dm_inR_avg, label='8.3-23.6e14')
plt.grid()
#plt.xlim((4e-3, 11))
#plt.ylim((3e12, 2e16))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$g_t(R)$')
plt.title('z=0.25, Hydro')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()


sys.exit()

fig_name = 'sigma_hd_at_R.png'
plt.figure(2, (7,6))

Sigma_values_hd_inR_avg = n.mean( t['Sigma_values_hd_atR'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(xR, Sigma_values_hd_inR_avg, label='1.6-2.7e14')
Sigma_values_hd_inR_avg = n.mean(t['Sigma_values_hd_atR'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(xR, Sigma_values_hd_inR_avg, label='2.7-5.3e14')
Sigma_values_hd_inR_avg = n.mean(t['Sigma_values_hd_atR'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(xR, Sigma_values_hd_inR_avg, label='5.3-8.3e14')
Sigma_values_hd_inR_avg = n.mean(t['Sigma_values_hd_atR'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(xR, Sigma_values_hd_inR_avg, label='8.3-23.6e14')
plt.grid()
plt.xlim((4e-3, 11))
plt.ylim((3e12, 2e16))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$\Sigma(R)$ $[h M_\odot Mpc^{-2}]$')
plt.title('z=0.25, HYDRO, Centered')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()


fig_name = 'sigma_hd_at_R_Rmis.png'
plt.figure(2, (7,6))

Sigma_values_hd_inR_avg = n.mean( t['Sigma_values_hd_atR_Rmis'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(xR, Sigma_values_hd_inR_avg, label='1.6-2.7e14')
Sigma_values_hd_inR_avg = n.mean(t['Sigma_values_hd_atR_Rmis'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(xR, Sigma_values_hd_inR_avg, label='2.7-5.3e14')
Sigma_values_hd_inR_avg = n.mean(t['Sigma_values_hd_atR_Rmis'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(xR, Sigma_values_hd_inR_avg, label='5.3-8.3e14')
Sigma_values_hd_inR_avg = n.mean(t['Sigma_values_hd_atR_Rmis'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(xR, Sigma_values_hd_inR_avg, label='8.3-23.6e14')
plt.grid()
plt.xlim((4e-3, 11))
plt.ylim((3e12, 2e16))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$\Sigma(R)$ $[h M_\odot Mpc^{-2}]$')
plt.title(r'$z=0.25$, HYDRO, with R$_{mis}$')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()

# WITHIN R
fig_name = 'sigma_dm_in_R.png'
plt.figure(2, (7,6))

Sigma_values_dm_inR_avg = n.mean( t['Sigma_values_dm_inR0'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_dm_inR_avg, label='1.6-2.7e14 R0', marker='o', ls='')
Sigma_values_dm_inR_avg = n.mean(t['Sigma_values_dm_inR0'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_dm_inR_avg, label='2.7-5.3e14 R0', marker='o', ls='')
Sigma_values_dm_inR_avg = n.mean(t['Sigma_values_dm_inR0'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_dm_inR_avg, label='5.3-8.3e14 R0', marker='o', ls='')
Sigma_values_dm_inR_avg = n.mean(t['Sigma_values_dm_inR0'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_dm_inR_avg, label='8.3-23.6e14 R0', marker='o', ls='')

#Sigma_values_dm_inR_avg = n.mean( t['Sigma_values_dm_inR1'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
#plt.plot(R_values[1:]/1000., Sigma_values_dm_inR_avg, label='1.6-2.7e14 R1', marker='x', ls='')
#Sigma_values_dm_inR_avg = n.mean(t['Sigma_values_dm_inR1'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
#plt.plot(R_values[1:]/1000., Sigma_values_dm_inR_avg, label='2.7-5.3e14 R1', marker='x', ls='')
#Sigma_values_dm_inR_avg = n.mean(t['Sigma_values_dm_inR1'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
#plt.plot(R_values[1:]/1000., Sigma_values_dm_inR_avg, label='5.3-8.3e14 R1', marker='x', ls='')
#Sigma_values_dm_inR_avg = n.mean(t['Sigma_values_dm_inR1'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
#plt.plot(R_values[1:]/1000., Sigma_values_dm_inR_avg, label='8.3-23.6e14 R1', marker='x', ls='')

plt.grid()
plt.xlim((4e-3, 11))
plt.ylim((3e12, 2e16))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$\Sigma(<R)$ $[h M_\odot Mpc^{-2}]$')
plt.title('z=0.25, DM')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()


fig_name = 'sigma_hd_in_R.png'
plt.figure(2, (7,6))

Sigma_values_hd_inR_avg = n.mean( t['Sigma_values_hd_inR0'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='1.6-2.7e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['Sigma_values_hd_inR0'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='2.7-5.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['Sigma_values_hd_inR0'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='5.3-8.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['Sigma_values_hd_inR0'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='8.3-23.6e14 R0', marker='o', ls='')

#Sigma_values_hd_inR_avg = n.mean( t['Sigma_values_hd_inR1'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
#plt.plot(R_values[1:]/1000., Sigma_values_hd_inR_avg, label='1.6-2.7e14 R1', marker='x', ls='')
#Sigma_values_hd_inR_avg = n.mean(t['Sigma_values_hd_inR1'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
#plt.plot(R_values[1:]/1000., Sigma_values_hd_inR_avg, label='2.7-5.3e14 R1', marker='x', ls='')
#Sigma_values_hd_inR_avg = n.mean(t['Sigma_values_hd_inR1'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
#plt.plot(R_values[1:]/1000., Sigma_values_hd_inR_avg, label='5.3-8.3e14 R1', marker='x', ls='')
#Sigma_values_hd_inR_avg = n.mean(t['Sigma_values_hd_inR1'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
#plt.plot(R_values[1:]/1000., Sigma_values_hd_inR_avg, label='8.3-23.6e14 R1', marker='x', ls='')

plt.grid()
plt.xlim((4e-3, 11))
plt.ylim((3e12, 2e16))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$\Sigma(<R)$ $[h M_\odot Mpc^{-2}]$')
plt.title('z=0.25, HYDRO')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()



fig_name = 'gamma_hd_in_R.png'
plt.figure(2, (7,6))

Sigma_values_hd_inR_avg = n.mean( t['gamma_hdR0_R'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='1.6-2.7e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_hdR0_R'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='2.7-5.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_hdR0_R'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='5.3-8.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_hdR0_R'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='8.3-23.6e14 R0', marker='o', ls='')

plt.grid()
plt.xlim((4e-3, 11))
#plt.ylim((3e12, 2e16))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$\gamma(R)$')
plt.title('z=0.25, HYDRO, centered')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()


fig_name = 'gamma_hd_in_R_Rmis.png'
plt.figure(2, (7,6))

Sigma_values_hd_inR_avg = n.mean( t['gamma_hdR0_R_Rmis'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='1.6-2.7e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_hdR0_R_Rmis'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='2.7-5.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_hdR0_R_Rmis'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='5.3-8.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_hdR0_R_Rmis'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='8.3-23.6e14 R0', marker='o', ls='')

plt.grid()
plt.xlim((4e-3, 11))
#plt.ylim((3e12, 2e16))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$\gamma(R)$')
plt.title('z=0.25, HYDRO, with R$_{mis}$')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()



fig_name = 'kappa_hd_in_R.png'
plt.figure(2, (7,6))

Sigma_values_hd_inR_avg = n.mean( t['kappa_hd_R'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='1.6-2.7e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['kappa_hd_R'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='2.7-5.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['kappa_hd_R'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='5.3-8.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['kappa_hd_R'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='8.3-23.6e14 R0', marker='o', ls='')


plt.grid()
plt.xlim((4e-3, 11))
#plt.ylim((3e12, 2e16))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$\kappa(R)$')
plt.title('z=0.25, HYDRO, centered')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()



fig_name = 'kappa_hd_in_R_Rmis.png'
plt.figure(2, (7,6))

Sigma_values_hd_inR_avg = n.mean( t['kappa_hd_R_Rmis'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='1.6-2.7e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['kappa_hd_R_Rmis'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='2.7-5.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['kappa_hd_R_Rmis'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='5.3-8.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['kappa_hd_R_Rmis'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='8.3-23.6e14 R0', marker='o', ls='')


plt.grid()
plt.xlim((4e-3, 11))
plt.ylim((1e-3, 6))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$\kappa(R)$')
plt.title('z=0.25, HYDRO, with R$_{mis}$')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()



fig_name = 'gamma_t_hd_in_R.png'
plt.figure(2, (7,6))

Sigma_values_hd_inR_avg = n.mean( t['gamma_t_hd_R'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='1.6-2.7e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_t_hd_R'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='2.7-5.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_t_hd_R'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='5.3-8.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_t_hd_R'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='8.3-23.6e14 R0', marker='o', ls='')


plt.grid()
plt.xlim((4e-3, 11))
plt.ylim((1e-3, 6))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$g_t(R)$')
plt.title('z=0.25, HYDRO, centered')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()


fig_name = 'gamma_t_hd_in_R_Rmis.png'
plt.figure(2, (7,6))

Sigma_values_hd_inR_avg = n.mean( t['gamma_t_hd_R_Rmis'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='1.6-2.7e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_t_hd_R_Rmis'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='2.7-5.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_t_hd_R_Rmis'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='5.3-8.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_t_hd_R_Rmis'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='8.3-23.6e14 R0', marker='o', ls='')


plt.grid()
plt.xlim((4e-3, 11))
plt.ylim((1e-3, 6))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$g_t(R)$')
plt.title('z=0.25, HYDRO, with R$_{mis}$')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()


fig_name = 'gamma_t_meas_hd_in_R.png'
plt.figure(2, (7,6))

Sigma_values_hd_inR_avg = n.mean( t['gamma_t_measured_diluted_hd_R'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='1.6-2.7e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_t_measured_diluted_hd_R'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='2.7-5.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_t_measured_diluted_hd_R'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='5.3-8.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_t_measured_diluted_hd_R'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='8.3-23.6e14 R0', marker='o', ls='')

plt.grid()
plt.xlim((4e-3, 11))
plt.ylim((1e-3, 6))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$g^{meas}_t(R)$')
plt.title('z=0.25, HYDRO, centered')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()


fig_name = 'gamma_t_meas_hd_in_R_Rmis.png'
plt.figure(2, (7,6))

Sigma_values_hd_inR_avg = n.mean( t['gamma_t_measured_diluted_hd_R_Rmis'][(t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='1.6-2.7e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_t_measured_diluted_hd_R_Rmis'][(t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='2.7-5.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_t_measured_diluted_hd_R_Rmis'][(t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='5.3-8.3e14 R0', marker='o', ls='')
Sigma_values_hd_inR_avg = n.mean(t['gamma_t_measured_diluted_hd_R_Rmis'][(t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14)], axis = 0)
plt.plot(R_values[:-1]/1000., Sigma_values_hd_inR_avg, label='8.3-23.6e14 R0', marker='o', ls='')


plt.grid()
plt.xlim((4e-3, 11))
#plt.ylim((3e12, 2e16))
plt.xscale('log')
plt.yscale('log')
plt.xlabel('R [Mpc/h]')
plt.ylabel(r'$g^{meas}_t(R)$')
plt.title('z=0.25, HYDRO, with R$_{mis}$')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()

# convert to angular observed space
def get_obs_prof(s1 = (t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14) &(t['redshift_R']>0.3) & (t['redshift_R']<0.35)):
    CV = 1/cosmoUNIT.kpc_comoving_per_arcmin(t['redshift_R'][s1]).to(u.Mpc/u.arcmin)
    RR_amin = n.array([ R_values[:-1]/1000./CV_i for CV_i in CV ])
    R_itp = 10**n.arange( n.log10(RR_amin.T[0].max()*1.1), n.log10(RR_amin.T[-1].min()*0.9), 0.05)
    gamma_t_meas_itp = n.array([ interp1d(xx, yy)(R_itp) for xx, yy in zip(RR_amin, t['gamma_t_measured_diluted_hd_R_Rmis'][s1]) ])
    Sigma_values_hd_inR_avg = n.mean( gamma_t_meas_itp, axis = 0)
    return R_itp, Sigma_values_hd_inR_avg


fig_name = 'gamma_t_meas_hd_in_R_Rmis_angles.png'
plt.figure(2, (7,6))

x0, y0 = get_obs_prof(s1 = (t['HALO_M200c']*0.6777>1.6e14) & (t['HALO_M200c']*0.6777<2.7e14) &(t['redshift_R']>0.3) & (t['redshift_R']<0.35))
plt.plot(x0, y0, label='1.6-2.7e14', marker='o', ls='')
x0, y0 = get_obs_prof(s1 = (t['HALO_M200c']*0.6777>2.7e14) & (t['HALO_M200c']*0.6777<5.3e14) &(t['redshift_R']>0.3) & (t['redshift_R']<0.35))
plt.plot(x0, y0, label='2.7-5.3e14', marker='o', ls='')
x0, y0 = get_obs_prof(s1 = (t['HALO_M200c']*0.6777>5.3e14) & (t['HALO_M200c']*0.6777<8.3e14) &(t['redshift_R']>0.3) & (t['redshift_R']<0.35))
plt.plot(x0, y0, label='5.3-8.3e14', marker='o', ls='')
x0, y0 = get_obs_prof(s1 = (t['HALO_M200c']*0.6777>8.3e14) & (t['HALO_M200c']*0.6777<23.6e14) &(t['redshift_R']>0.3) & (t['redshift_R']<0.35))
plt.plot(x0, y0, label='8.3-23.6e14', marker='o', ls='')


plt.grid()
plt.xlim((4e-3, 2))
#plt.ylim((3e12, 2e16))
plt.xscale('log')
plt.yscale('log')
plt.xlabel(r'$\theta$ [arc min]')
plt.ylabel(r'$g^{meas}_t(\theta)$')
plt.title('z=0.3-0.35, HYDRO, with R$_{mis}$')
plt.legend(loc=3)
plt.savefig( os.path.join( fig_dir, fig_name) )
plt.clf()

