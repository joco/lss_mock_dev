"""
What it does
------------

Add realistic magnitudes to BG, LRG and FIL (ELG deprecated)

Command to run
--------------

python3 005_2_all_magnitudes.py environmentVAR 

topcat -stilts plot2plane \
   xpix=616 ypix=482 \
   ylog=true xlabel='8.9-2.5\times \log_{10}(flux\_Kt)' \
    ylabel='N(>m)\; [deg^{-2}]' grid=true texttype=latex \
    fontsize=17 fontstyle=serif fontweight=italic \
   xmin=13 xmax=21 ymin=1 ymax=40000 \
   legend=false \
   layer=Histogram \
      in='/home/comparat/data/UNIT_fA1i/full_000000 (2).fit' \
      x='8.9-2.5*log10(flux_Kt)' \
       weight='(768. * PI)/129600.' \
      binsize=-84 cumulative=true barform=steps thick=5 

topcat -stilts plot2plane \
   xpix=1178 ypix=565 \
   xlabel='8.9-2.5*log10(flux_Kt)' ylabel='2.5*log10(flux_Jt/flux_Kt)' \
   xmin=11.57 xmax=24.7 ymin=-3.27 ymax=1.56 \
   auxmin=0.022 auxmax=1.494 \
   auxvisible=true auxlabel=redshift_R \
   legend=false \
   layer=Grid \
      in='/home/comparat/data/UNIT_fA1i/full_000000 (2).fit' \
      x='8.9-2.5*log10(flux_Kt)' y='2.5*log10(flux_Jt/flux_Kt)' weight=redshift_R 
      
"""

#import matplotlib
#matplotlib.use('Agg')
#matplotlib.rcParams.update({'font.size': 14})
#import matplotlib.pyplot as p
from sklearn.neighbors import BallTree

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import extinction

from astropy_healpix import healpy
import sys
import os
import time
from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column
from scipy.optimize import curve_fit
import astropy.io.fits as fits
import h5py
import numpy as n
print('CREATES FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

env = 'UNIT_fA1i_DIR' # sys.argv[1]  # 'UNIT_fA1i_DIR'
#HEALPIX_id = int(sys.argv[2])
#print(env, HEALPIX_id)

if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

nl = lambda sel : len(sel.nonzero()[0])
zs = n.arange(0.0000001, 7.1, 0.001)
dm_itp = interp1d(zs, cosmo.distmod(zs).value)

root_dir = os.path.join(os.environ[env])

max_distance = 0.1 / 3600. * n.pi / 180.
deg_to_rad = n.pi/180.

# Match to BCG and GAL catalogues 
#print('load and match to BCG and GAL catalogues')
#path_2_out_GAL = os.path.join(root_dir, env + '_eRO_CLU_b8_CM_0_pixS_20.0_4MOST_GAL.fits')
#path_2_out_BCG = os.path.join(root_dir, env + '_eRO_CLU_b8_CM_0_pixS_20.0_4MOST_BCG.fits')
#t_GAL = Table.read(path_2_out_GAL)
#t_BCG = Table.read(path_2_out_BCG)
#coord_GAL = deg_to_rad * n.transpose([t_GAL['DEC'], t_GAL['RA'] ])
#coord_BCG = deg_to_rad * n.transpose([t_BCG['DEC'], t_BCG['RA'] ])

#Tree_GAL = BallTree(coord_GAL, metric='haversine')
#Tree_BCG = BallTree(coord_BCG, metric='haversine')

# Match to AGN catalogues 
#print('load and match to AGN catalogues')
#agn = Table.read('/data42s/comparat/firefly/mocks/2021-04/QMOST/S6_4MOST_ALL_SNR3_IR215.fit.gz')
#qso = Table.read('/data42s/comparat/firefly/mocks/2021-04/QMOST/QSO_4MOST.fits.gz')

#coord_AGN = deg_to_rad * n.transpose([agn['DEC'], agn['RA'] ])
#coord_QSO = deg_to_rad * n.transpose([qso['DEC'], qso['RA'] ])

#Tree_AGN = BallTree(coord_AGN, metric='haversine')
#Tree_QSO = BallTree(coord_QSO, metric='haversine')


#path_2_kids = os.path.join(os.environ['MD10'], '4most_s8_kidsdr4.lsdr9.fits')
#t_kids = Table.read(path_2_kids)
#dup_type = (t_kids['lsdr9_TYPE']=="DUP" )
#dash_type = (t_kids['lsdr9_TYPE']=="-" ) | (t_kids['lsdr9_TYPE']=='-  ')
#good_phot = ( t_kids['rtot']>0 ) & (t_kids['gr'] > -10 ) & (t_kids['ri'] > -10 )  & (t_kids['iz'] > -10 )  & (t_kids['zy'] > -10 )  & (t_kids['yj'] > -10 )  & (t_kids['jh'] > -10 )  & (t_kids['hks'] > -10 ) & (t_kids['zphot']>0)
#keep_kids = (dup_type==False) & (dash_type==False) & (good_phot)
#t_kids = t_kids[keep_kids]
path_2_kids = os.path.join(os.environ['UNIT_fA1i_DIR'], 'G09.GAMADR4+LegacyDR9.galreference+RM.fits')
t_kids = Table.read(path_2_kids)
kmag = 8.9 - 2.5*n.log10(t_kids['flux_Kt'])
zmag = 8.9 - 2.5*n.log10(t_kids['flux_Zt'])
rmag = 8.9 - 2.5*n.log10(t_kids['flux_rt'])
imag = 8.9 - 2.5*n.log10(t_kids['flux_it'])
keep_kids = (kmag>0) & (t_kids['z_peak']>0.01) & (t_kids['z_peak']<1.99) & (zmag>0) & (rmag>0)  & (imag>0)
t_kids = t_kids[keep_kids]
kmag = 8.9 - 2.5*n.log10(t_kids['flux_Kt'])
zmag = 8.9 - 2.5*n.log10(t_kids['flux_Zt'])
rmag = 8.9 - 2.5*n.log10(t_kids['flux_rt'])
imag = 8.9 - 2.5*n.log10(t_kids['flux_it'])

KCORR_DATA = n.loadtxt( os.path.join( os.environ['GIT_AGN_MOCK'], 'data/K-corr/VISTA_Ks_kcorrections.txt'), unpack = True)
# redshift distmod bandpass kcorr_median kcorr_16pc kcorr_84pc
#bandpass_itp = interp1d(KCORR_DATA[0], KCORR_DATA[2])
kcorr_itp = interp1d(KCORR_DATA[0], KCORR_DATA[3])
#kmag = absmag + distmod + bandpass + kcorrection
# absmag = appmag - distmod - kcorr  
# is properly zero-centred; or at least zero-ish; the median +/- NMAD is 0.05 +/- 0.12.

dm_values = dm_itp(t_kids['z_peak'])
#kmag_abs = kmag - ( dm_values + bandpass_itp(t_kids['z_peak']) + kcorr_itp(t_kids['z_peak']) )
kmag_abs = kmag - ( dm_values + kcorr_itp(t_kids['z_peak']) )

#kmag_abs = 8.7 - 2.5*n.log10(t_kids['flux_Kt']) - dm_values
t_kids['Kmag_abs'] = kmag_abs
t_kids['dist_mod'] = dm_values

# rescale variables
min_Z = 1.5 # n.min(t_kids['z_peak'])
max_Z = 0.0 # n.max(t_kids['z_peak'])
min_K = -26. #n.min(t_kids['Kmag_abs'])
max_K = -20. #n.max(t_kids['Kmag_abs'])

z_01 = (t_kids['z_peak'] - min_Z ) / ( max_Z - min_Z )
k_01 = (t_kids['Kmag_abs'] - min_K ) / ( max_K - min_K )
Tree_kids = BallTree(n.transpose([z_01, k_01]))
print(len(t_kids),'lines in KIDS catalogue')

sub_survey_names = n.array([ 'BG', 'LRG', 'ELG', 'QSO', 'LyA', 'filament_GAL'])

N_subsurvey = {'BG':1, 'filament_GAL':3, 'LRG':2, 'ELG':3, 'QSO':4, 'LyA':5}
priority_values = {'BG':100, 'filament_GAL':80, 'LRG':99, 'ELG':80, 'QSO':97, 'LyA':98}

# reassigns templates correctly
z_all = n.hstack(( 0., n.arange(0.3, 3., 0.2), 3.5, 4.5, 6. ))
zmins = z_all[:-1]
zmaxs = z_all[1:]

dir_2_OUT = os.path.join(root_dir, "cat_SHAM_ALL")
dir_2_IN = os.path.join(root_dir, "cat_GALAXY_all")

def add_magnitudes_direct_match(t_survey, t_kids2=t_kids, Tree_kids=Tree_kids):
	# simulated quantities
	sim_redshift = t_survey['redshift_R']
	sim_k_mag    = t_survey['K_mag_abs'] # +dm_itp(t_survey['redshift_R'])
	# rescale variables
	SIMz_01      = ( sim_redshift - min_Z ) / ( max_Z - min_Z )
	SIMk_01      = ( sim_k_mag - min_K ) / ( max_K - min_K )
	DATA = n.transpose([SIMz_01, SIMk_01])
	# search nearest neighbour in tree
	dist_out, ids_out = Tree_kids.query(DATA, k=1, return_distance = True)
	kids_ID = n.arange(len(t_kids2))
	ids = n.hstack((ids_out))
	dist_map = n.hstack(( dist_out ))
	id_to_map = kids_ID[ids]
	columns_to_add = n.array(['CATAID', 'z_peak', 'Kmag_abs', 'dist_mod', 'flux_FUVt',
								'flux_err_FUVt',
								'flux_NUVt',
								'flux_err_NUVt',
								'flux_ut',
								'flux_err_ut',
								'flux_gt',
								'flux_err_gt',
								'flux_rt',
								'flux_err_rt',
								'flux_it',
								'flux_err_it',
								'flux_Zt',
								'flux_err_Zt',
								'flux_Yt',
								'flux_err_Yt',
								'flux_Jt',
								'flux_err_Jt',
								'flux_Ht',
								'flux_err_Ht',
								'flux_Kt',
								'flux_err_Kt',
								'flux_W1t',
								'flux_err_W1t',
								'flux_W2t',
								'flux_err_W2t'
								, 'FIBERFLUX_R'
								, 'SHAPE_R'
								, 'SHAPE_E1'
								, 'SHAPE_E2'
								, 'SERSIC'
								, 'MASKBITS'
								, 'RM_p_mem', 'RM_cg'])
	print('outputing results')
	for el in columns_to_add :
		t_survey[el] = t_kids2[el][id_to_map]
	t_survey['distance_match'] = dist_map
	return t_survey #.write(path_2_out)

t0 = time.time()
N_pixels = healpy.nside2npix(8)
for HEALPIX_id in n.arange(N_pixels): #n.arange(N_pixels)[::-1]:
    print(HEALPIX_id, time.time()-t0)
    path_2_out = os.path.join(dir_2_OUT, 'full_'  + str(HEALPIX_id).zfill(6) + '.fit')
    path_2_in = os.path.join(dir_2_IN, str(HEALPIX_id).zfill(6) + '.fit')
    print(path_2_in)
    t_survey = Table.read(path_2_in)
    t_in = t_survey[(t_survey['redshift_R']<1.5)&(t_survey['redshift_R']>0.01)]
    print(len(t_in))
    t_out = add_magnitudes_direct_match(t_survey = t_in, t_kids2 = t_kids, Tree_kids=Tree_kids)
    t_out.write(path_2_out, overwrite = True)
    print(path_2_out, 'written')


