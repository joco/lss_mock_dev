import os, glob, sys, time
t0 = time.time()

from astropy.table import Table, Column
import astropy.io.fits as fits
from astropy_healpix import healpy
from scipy.stats import scoreatpercentile
from scipy.stats import norm
from scipy.interpolate import interp1d
import numpy as n

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p

import astropy.units as u
from astropy.cosmology import FlatLambdaCDM

all_sims = n.array( glob.glob( os.path.join( '/home/comparat/data/erosita/eFEDS/simulations', 'Sim11_*_eFEDS_det_V16C_b0_Sc1Cat.fits') ) )

clu = Table.read( os.path.join( os.environ['MD10'], 'MD10_eRO_CLU_wCount_March2020.fit') )
agn = Table.read( os.path.join( os.environ['MD10'], 'MD10_AGN_wCount_March2020.fit') )