#!/bin/bash

# RUN ON DS43
pyCONDA 

nohup sh md40_r000_all.sh > md40_r000_all.log & # ds52
nohup sh md04_r000_all.sh > md04_r000_all.log & # ds52
nohup sh md10_r000_all.sh > md10_r000_all.log & # ds52
# $MD25 at MPCDF

nohup sh unit_fA1i_r000_all.sh  > fA1i_r000_all.log & # ds52 restart on 13 dec
nohup sh unit_fA1_r000_all.sh   > fA1_r000_all.log & # ds52 restart on 13 dec
nohup sh unit_fA2_r000_all.sh   > fA2_r000_all.log & # ds52 restart on 13 dec

nohup python 001_HMF.py MD40 > md40_001_hmf.log &
nohup python 002_2PCF.py MD40 > md40_002_2pcf.log &
