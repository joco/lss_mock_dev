"""
What it does
------------

"""
from astropy.table import Table, Column
from astropy_healpix import healpy
import sys
import os, glob
import time
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import astropy.constants as cc
import astropy.io.fits as fits
from scipy.special import erf
from scipy.stats import norm
from scipy.interpolate import interp2d
from scipy.interpolate import interp1d
from scipy.stats import scoreatpercentile
import h5py
import numpy as n
from sklearn import mixture

print('Tabulates mass functions in bins of XOFF')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

env = 'MD40' 
# initializes pathes to files
test_dir = os.path.join(os.environ[env], 'Mass_Xoff_Concentration')
path_2_snapshots = n.array(glob.glob(os.path.join(test_dir, 'distinct_?.*.fits')))
path_2_snapshot = os.path.join(test_dir, 'HMF/g_sigma/g_sigma_1.0.fit')
out_file = os.path.join(test_dir, 'HMF/g_sigma/g_sigma_1.0_BR_fit.fit')
out_training = os.path.join(test_dir, 'HMF/g_sigma/g_sigma_1.0_BR_training.fit')
path_2_snapshot_data = os.path.join(test_dir, 'distinct_1.0.fits')



hd = fits.open(path_2_snapshot)
s = hd[1].data['log1_sigma']
s_low = hd[1].data['log1_sigma_low']
s_high = hd[1].data['log1_sigma_high']



#distinct_1.0.fits

## QUARTILE bins at redshift 0
xoff_bins_Q = n.log10(n.array([0.0005,  4.28608992e-02, 6.59490117e-02, 1.00837135e-01, 5.7516e-01]))

## DECILE bins at redshift 0
xoff_bins_D = n.log10(n.array([0.0005, 2.8200e-02, 3.8350e-02, 4.7280e-02, 5.6230e-02, 6.5950e-02, 7.7280e-02, 9.1650e-02, 1.1230e-01, 1.4953e-01, 5.7516e-01]))

xoff_val = (xoff_bins_D[1:]+xoff_bins_D[:-1])/2.
xoff_val_high = xoff_bins_D[1:]
xoff_val_low = xoff_bins_D[:-1]

X_i = []
Y_i = []
for ii in range(10):
	X_i.append( n.transpose([ s_low, s_high, n.ones_like(s)*xoff_val_low[ii], n.ones_like(s)*xoff_val_high[ii]  ]))
	Y_i.append( hd[1].data['gD'+str(ii)])

X_0 = n.vstack((X_i))
Y_0 = n.hstack((Y_i))

good = (Y_0>0)
X = X_0[good]
Y = Y_0[good]


X_i = []
Y_i = []
for ii in range(10):
	X_i.append( n.transpose([ s, n.ones_like(s)*xoff_val[ii]  ]))
	Y_i.append( hd[1].data['gD'+str(ii)])

X_0 = n.vstack((X_i))
Y_0 = n.hstack((Y_i))

good = (Y_0>0)
X = X_0[good]
Y = Y_0[good]

##from sklearn import linear_model
##reg = linear_model.BayesianRidge(tol=1e-6, fit_intercept=False, compute_score=True)
##reg.set_params(alpha_init=1.8, lambda_init=0.001)
##reg.fit(X, Y)
##ymean, ystd = reg.predict(X, return_std=True)

## make 1e6 random points in each bin
#N_per_bin = 1000000

#random_Sigma = []
#random_Xoff = []
#for x_i, y_i in zip(X,Y):
	#s_min, s_max, x_min, x_max = x_i
	#N_to_add = int(y_i * N_per_bin)
	#random_Sigma.append(n.random.uniform(s_min, s_max, N_to_add))
	#random_Xoff.append(n.random.uniform(x_min, x_max, N_to_add))

#random_Sigma = n.hstack(( random_Sigma ))
#random_Xoff = n.hstack(( random_Xoff ))
#X_train = n.transpose([random_Sigma, random_Xoff]) 
hd1 = fits.open(path_2_snapshot_data)

X_train = n.transpose([n.log10(hd1[1].data['Mvir']), n.log10(hd1[1].data['Xoff']/hd1[1].data['Rvir'])]) 
n.random.shuffle(X_train)

#t = Table()
#t.add_column(Column(name='log_1_o_sigma', data=X_train.T[0], unit=''))
#t.add_column(Column(name='Xoff', data=X_train.T[1], unit=''))
#t.write(out_training, overwrite=True)

clf = mixture.GaussianMixture(n_components=9)
clf.fit(X_train[:500000])
X_OUT = clf.sample(1000000)[0]
print(clf.covariances_)
print(clf.means_)
print(clf.weights_)

t = Table()
t.add_column(Column(name='logmass', data=X_OUT.T[0], unit=''))
t.add_column(Column(name='logXoff', data=X_OUT.T[1], unit=''))
t.write(out_training, overwrite=True)

sys.exit()


t = Table()
t.add_column(Column(name='log_1_o_sigma', data=X.T[0], unit=''))
t.add_column(Column(name='Xoff', data=X.T[1], unit=''))
t.add_column(Column(name='g_data', data=Y, unit=''))
t.add_column(Column(name='g_predicted_2', data=Z3, unit=''))
t.add_column(Column(name='g_predicted_4', data=Z4, unit=''))
t.add_column(Column(name='g_predicted_6', data=Z6, unit=''))
t.add_column(Column(name='g_predicted_8', data=Z8, unit=''))
t.write(out_file, overwrite=True)


#boundaries = scoreatpercentile(x0ff, n.arange(0,101,10)) # deciles
#boundaries = scoreatpercentile(x0ff, n.arange(0,101,25)) # quartiles


m_bins = n.arange(9,16,0.05)

for p2s in path_2_snapshots:
	out_file = os.path.join(test_dir, 'HMF', 'HMF_'+os.path.basename(p2s)[:-1])
	hd = fits.open(p2s)
	x0ff = hd[1].data['Xoff']/hd[1].data['rvir']
	mass = n.log10(hd[1].data['Mvir'])

	def get_mhist(selection) : 
		return n.histogram(mass[selection], bins = m_bins)[0]

	DATA = n.zeros((len(xoff_bins_D)-1, len(m_bins)-1))
	for jj, (x_min, x_max) in enumerate(zip(xoff_bins_D[:-1], xoff_bins_D[1:])):
		DATA[jj] = get_mhist( (x0ff>=x_min) & (x0ff<x_max) )

	DATA2 = n.zeros((len(xoff_bins_Q)-1, len(m_bins)-1))
	for jj, (x_min, x_max) in enumerate(zip(xoff_bins_Q[:-1], xoff_bins_Q[1:])):
		DATA2[jj] = get_mhist( (x0ff>=x_min) & (x0ff<x_max) )

	t = Table()
	t.add_column(Column(name='log_mvir_min', data=m_bins[:-1], unit=''))
	t.add_column(Column(name='log_mvir_max', data=m_bins[1:], unit=''))
	t.add_column(Column(name='Ntotal', data=DATA.sum(axis=0), unit=''))
	t.add_column(Column(name='D0', data=DATA[0], unit=''))
	t.add_column(Column(name='D1', data=DATA[1], unit=''))
	t.add_column(Column(name='D2', data=DATA[2], unit=''))
	t.add_column(Column(name='D3', data=DATA[3], unit=''))
	t.add_column(Column(name='D4', data=DATA[4], unit=''))
	t.add_column(Column(name='D5', data=DATA[5], unit=''))
	t.add_column(Column(name='D6', data=DATA[6], unit=''))
	t.add_column(Column(name='D7', data=DATA[7], unit=''))
	t.add_column(Column(name='D8', data=DATA[8], unit=''))
	t.add_column(Column(name='D9', data=DATA[9], unit=''))
	t.add_column(Column(name='Q0', data=DATA2[0], unit=''))
	t.add_column(Column(name='Q1', data=DATA2[1], unit=''))
	t.add_column(Column(name='Q2', data=DATA2[2], unit=''))
	t.add_column(Column(name='Q3', data=DATA2[3], unit=''))
	t.write(out_file, overwrite=True)
