#!/bin/bash
# 
# Xoff is in column 42, not 44 for BigMD, needs to modify the 000_process_hlists_snapshot.py script specifically
# 

# 
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_1.00000.list.bz2

python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.90740.list.bz2

python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.85940.list.bz2

python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.81440.list.bz2

python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.70030.list.bz2

python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.60430.list.bz2

python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.50000.list.bz2

python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.40900.list.bz2

python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.08800.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.10200.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.11500.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.13500.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.15600.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.19600.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.22700.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.25700.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.28700.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.31800.list.bz2

python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.53000.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.54440.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.55040.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.55630.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.56230.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.56840.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.57430.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.58030.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.58640.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.59240.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.59830.list.bz2
# 
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.61040.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.61630.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.62230.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.62840.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.63440.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.64030.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.64640.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.65240.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.65830.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.66430.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.67040.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.67630.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.68230.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.68840.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.69440.list.bz2
# 
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.70640.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.71240.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.71830.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.72430.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.73630.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.74230.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.75440.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.76030.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.77240.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.77830.list.bz2
# python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.78730.list.bz2


python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.92515.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.95600.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.97071.list.bz2

python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.81730.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.82340.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.82630.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.83240.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.83530.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.84140.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.84430.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.85040.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.85330.list.bz2

python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.86230.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.86840.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.87130.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.87730.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.88030.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.88630.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.88930.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.89530.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.89840.list.bz2
python 000_process_hlists_snapshot.py 63 MD25 5000000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.90430.list.bz2

-rw-rw-rw-+ 1 joco mpexa         9883 Apr  1 01:36 hlist_0.08800.list.bz2
-rw-rw-rw-+ 1 joco mpexa       608869 Apr  1 01:36 hlist_0.10200.list.bz2
-rw-rw-rw-+ 1 joco mpexa      8328859 Apr  1 01:36 hlist_0.11500.list.bz2
-rw-rw-rw-+ 1 joco mpexa    110178721 Apr  1 01:36 hlist_0.13500.list.bz2
-rw-rw-rw-+ 1 joco mpexa    589572307 Apr  1 01:36 hlist_0.15600.list.bz2
-rw-rw-rw-+ 1 joco mpexa   3097827123 Apr  1 01:36 hlist_0.19600.list.bz2
-rw-rw-rw-+ 1 joco mpexa   6690206182 Apr  1 01:36 hlist_0.22700.list.bz2
-rw-rw-rw-+ 1 joco mpexa  10778289584 Apr  1 01:36 hlist_0.25700.list.bz2
-rw-rw-rw-+ 1 joco mpexa  14973409643 Apr  1 01:36 hlist_0.28700.list.bz2
-rw-rw-rw-+ 1 joco mpexa   3372005906 Apr  1 01:36 hlist_0.31800.list.bz2
-rw-rw-rw-+ 1 joco mpexa  24875473290 Apr  1 01:36 hlist_0.40900.list.bz2
-rw-rw-rw-+ 1 joco mpexa  27497707483 Apr  1 01:36 hlist_0.50000.list.bz2
-rw-rw-rw-+ 1 joco mpexa  28097955962 Apr  1 01:36 hlist_0.53000.list.bz2
-rw-rw-rw-+ 1 joco mpexa  28130033859 Apr  1 01:36 hlist_0.54440.list.bz2
-rw-rw-rw-+ 1 joco mpexa  28307031077 Apr  1 01:36 hlist_0.55040.list.bz2
-rw-rw-rw-+ 1 joco mpexa  28381581221 Apr  1 01:36 hlist_0.55630.list.bz2
-rw-rw-rw-+ 1 joco mpexa  28491308895 Apr  1 01:36 hlist_0.56230.list.bz2
-rw-rw-rw-+ 1 joco mpexa  28409780838 Apr  1 01:36 hlist_0.56840.list.bz2
-rw-rw-rw-+ 1 joco mpexa  28658336917 Apr  1 01:36 hlist_0.57430.list.bz2
-rw-rw-rw-+ 1 joco mpexa  28753780394 Apr  1 01:36 hlist_0.58030.list.bz2
-rw-rw-rw-+ 1 joco mpexa  28675602468 Apr  1 01:36 hlist_0.58640.list.bz2
-rw-rw-rw-+ 1 joco mpexa  28919317385 Apr  1 01:36 hlist_0.59240.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29017213877 Apr  1 01:36 hlist_0.59830.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29067182324 Apr  1 01:36 hlist_0.60430.list.bz2
-rw-rw-rw-+ 1 joco mpexa  28973010200 Apr  1 01:36 hlist_0.61040.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29197775928 Apr  1 01:36 hlist_0.61630.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29243787360 Apr  1 01:36 hlist_0.62230.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29146380185 Apr  1 01:36 hlist_0.62840.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29370336104 Apr  1 01:36 hlist_0.63440.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29428088087 Apr  1 01:36 hlist_0.64030.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29309762927 Apr  1 01:36 hlist_0.64640.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29526350015 Apr  1 01:36 hlist_0.65240.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29575140068 Apr  1 01:36 hlist_0.65830.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29620529827 Apr  1 01:36 hlist_0.66430.list.bz2
-rw-rw-rw-+ 1 joco mpexa  27468418969 Apr  1 01:36 hlist_0.67040.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29697581434 Apr  1 01:36 hlist_0.67630.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29728801313 Apr  1 01:36 hlist_0.68230.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29608395314 Apr  1 01:36 hlist_0.68840.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29810888771 Apr  1 01:36 hlist_0.69440.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29855321158 Apr  1 01:36 hlist_0.70030.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29867222019 Apr  1 01:36 hlist_0.70640.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29943099618 Apr  1 01:36 hlist_0.71240.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29997739966 Apr  1 01:36 hlist_0.71830.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29981547297 Apr  1 01:36 hlist_0.72430.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29891652538 Apr  1 01:36 hlist_0.73630.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30051264658 Apr  1 01:36 hlist_0.74230.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29911132345 Apr  1 01:36 hlist_0.75440.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30074902662 Mar  1 01:35 hlist_0.76030.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29897177030 Apr  1 01:36 hlist_0.77240.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30082640934 Apr  1 01:36 hlist_0.77830.list.bz2
-rw-rw-rw-+ 1 joco mpexa  29910541917 Apr  1 01:36 hlist_0.78730.list.bz2
-rw-rw-rw-+ 1 joco mpexa 104969682342 Apr  1 01:36 hlist_0.79040.list
-rw-rw-rw-+ 1 joco mpexa  97893553636 Apr  1 01:36 hlist_0.80230.list
-rw-rw-rw-+ 1 joco mpexa  65271564632 Apr  1 01:36 hlist_0.80840.list
-rw-rw-rw-+ 1 joco mpexa  30149207937 Apr  1 01:36 hlist_0.81440.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30148930846 Apr  1 01:36 hlist_0.81730.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30192790519 Apr  1 01:36 hlist_0.82340.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30216281613 Apr  1 01:36 hlist_0.82630.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30291703780 Apr  1 01:36 hlist_0.83240.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30321941777 Apr  1 01:36 hlist_0.83530.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30368998802 Apr  1 01:36 hlist_0.84140.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30379967195 Apr  1 01:36 hlist_0.84430.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30452790073 Apr  1 01:36 hlist_0.85040.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30500444945 Apr  1 01:36 hlist_0.85330.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30571604010 Apr  1 01:36 hlist_0.85940.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30616799042 Apr  1 01:36 hlist_0.86230.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30680646868 Apr  1 01:36 hlist_0.86840.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30709252845 Apr  1 01:36 hlist_0.87130.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30807621026 Apr  1 01:36 hlist_0.87730.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30842911619 Apr  1 01:36 hlist_0.88030.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30929815079 Apr  1 01:36 hlist_0.88630.list.bz2
-rw-rw-rw-+ 1 joco mpexa  30983853597 Apr  1 01:36 hlist_0.88930.list.bz2
-rw-rw-rw-+ 1 joco mpexa 130073602350 Apr  1 01:36 hlist_0.89530.list
-rw-rw-rw-+ 1 joco mpexa  31170020041 Apr  1 01:36 hlist_0.89840.list.bz2
-rw-rw-rw-+ 1 joco mpexa  31285374696 Apr  1 01:36 hlist_0.90430.list.bz2
-rw-rw-rw-+ 1 joco mpexa  31332033684 Apr  1 01:36 hlist_0.90740.list.bz2
-rw-rw-rw-+ 1 joco mpexa 132601854943 Apr  1 01:36 hlist_0.92515.list
-rw-rw-rw-+ 1 joco mpexa 133834855267 Apr  1 01:36 hlist_0.95600.list
-rw-rw-rw-+ 1 joco mpexa 134708361890 Apr  1 01:36 hlist_0.97071.list
-rw-rw-rw-+ 1 joco mpexa  29500577976 Apr  1 01:36 hlist_1.00000.list.bz2


#scale(0) id(1) desc_scale(2) desc_id(3) num_prog(4) pid(5) upid(6) desc_pid(7) phantom(8) sam_mvir(9) mvir(10) rvir(11) rs(12) vrms(13) mmp?(14) scale_of_last_MM(15) vmax(16) x(17) y(18) z(19) vx(20) vy(21) vz(22) Jx(23) Jy(24) Jz(25) Spin(26) Breadth_first_ID(27) Depth_first_ID(28) Tree_root_ID(29) Orig_halo_ID(30) Snap_num(31) Next_coprogenitor_depthfirst_ID(32) Last_progenitor_depthfirst_ID(33) Last_mainleaf_depthfirst_ID(34) Rs_Klypin(35) Mmvir_all(36) M200b(37) M200c(38) M500c(39) M2500c(40) Xoff(41) Voff(42) Spin_Bullock(43) b_to_a(44) c_to_a(45) A[x](46) A[y](47) A[z](48) b_to_a(500c)(49) c_to_a(500c)(50) A[x](500c)(51) A[y](500c)(52) A[z](500c)(53) T/|U|(54) M_pe_Behroozi(55) M_pe_Diemer(56) Halfmass_Radius(57) Macc(58) Mpeak(59) Vacc(60) Vpeak(61) Halfmass_Scale(62) Acc_Rate_Inst(63) Acc_Rate_100Myr(64) Acc_Rate_1*Tdyn(65) Acc_Rate_2*Tdyn(66) Acc_Rate_Mpeak(67) Acc_Log_Vmax_Inst(68) Acc_Log_Vmax_1*Tdyn(69) Mpeak_Scale(70) Acc_Scale(71) First_Acc_Scale(72) First_Acc_Mvir(73) First_Acc_Vmax(74) Vmax\@Mpeak(75) Tidal_Force_Tdyn(76) Log_(Vmax/Vmax_max(Tdyn;Tmpeak))(77) Time_to_future_merger(78) Future_merger_MMP_ID(79)

# Mvir Rvir rs vrms x y z Spin M200c Xoff Spin_Bullock 
$11, $12, $13, $14, $18, $19, $20, $27, $39, $42, $44