"""
What it does
------------

Computes the halo mass function in Xoff bins 

python3 001_HMF.py environmentVAR 

arguments
---------

environmentVAR: environment variable linking to the directory where files are e.g. "MD10"
It will then work in the directory : $environmentVAR/hlists/fits/

fileBasename: base name of the file e.g. all_1.00000

Dependencies
------------

import time, os, sys, numpy, scipy, astropy, h5py, extinction, matplotlib


topcat -stilts plot2plane \
   xpix=1200 ypix=600 \
   xlabel='M vir' ylabel='Fraction of halos in Xoff Decile' grid=true fontsize=17 \
   xmin=12.9 xmax=16.0 ymin=0.06 ymax=0.18 \
   legend=true legpos=1.0,0.0 \
   in=/data/data/MultiDark/Mass_Xoff_Concentration/HMF_distinct_1.0.fit x='(log_mvir_min+log_mvir_max)/2.' \
   layer_01=Line \
      y_01=D0/Ntotal \
      thick_01=3 dash_01=3,3 \
      leglabel_01=D0/N \
   layer_02=Line \
      y_02=D1/Ntotal \
      color_02=blue thick_02=3 dash_02=3,3 \
      leglabel_02=D1/N \
   layer_03=Line \
      y_03=D2/Ntotal \
      color_03=green thick_03=3 dash_03=3,3 \
      leglabel_03=D2/N \
   layer_04=Line \
      y_04=D3/Ntotal \
      color_04=grey thick_04=3 \
      leglabel_04=D3/N \
   layer_05=Mark \
      y_05=D4/Ntotal \
      shading_05=auto color_05=magenta \
      leglabel_05=D4/N \
   layer_06=Line \
      y_06=D4/Ntotal \
      color_06=magenta thick_06=3 \
      leglabel_06=D4/N \
   layer_07=Line \
      y_07=D5/Ntotal \
      color_07=cyan thick_07=2 \
      leglabel_07=D5/N \
   layer_08=Line \
      y_08=D6/Ntotal \
      color_08=orange thick_08=2 \
      leglabel_08=D6/N \
   layer_09=Line \
      y_09=D7/Ntotal \
      color_09=pink thick_09=2 \
      leglabel_09=D7/N \
   layer_10=Line \
      y_10=D8/Ntotal \
      color_10=yellow thick_10=2 \
      leglabel_10=D8/N \
   layer_11=Line \
      y_11=D9/Ntotal \
      color_11=black thick_11=2 \
      leglabel_11=D9/N \
      omode=out out=$MD40/Mass_Xoff_Concentration/HMF_distinct_1.0_Xoff_D.png


topcat -stilts plot2plane \
   xpix=1200 ypix=600 \
   xlabel='M vir' ylabel='Fraction of halos in Xoff Decile' grid=true fontsize=17 \
   xmin=12.9 xmax=16.0 ymin=0.0 ymax=0.60 \
   legend=true legpos=1.0,0.0 \
   in=/data/data/MultiDark/Mass_Xoff_Concentration/HMF_distinct_1.0.fit x='(log_mvir_min+log_mvir_max)/2.' \
   layer_01=Line \
      y_01=Q0/Ntotal \
      thick_01=3 dash_01=3,3 \
      leglabel_01=Q0/N \
   layer_02=Line \
      y_02=Q1/Ntotal \
      color_02=blue thick_02=3 dash_02=3,3 \
      leglabel_02=Q1/N \
   layer_03=Line \
      y_03=Q2/Ntotal \
      color_03=green thick_03=3 dash_03=3,3 \
      leglabel_03=Q2/N \
   layer_04=Line \
      y_04=Q3/Ntotal \
      color_04=grey thick_04=3 \
      leglabel_04=Q3/N \
      omode=out out=$MD40/Mass_Xoff_Concentration/HMF_distinct_1.0_Xoff_Q.png

"""
from astropy.table import Table, Column

from astropy_healpix import healpy
import sys
import os, glob
import time
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import astropy.constants as cc
import astropy.io.fits as fits
from scipy.special import erf
from scipy.stats import norm
from scipy.interpolate import interp2d
from scipy.interpolate import interp1d
from scipy.stats import scoreatpercentile
import h5py
import numpy as n
print('Tabulates mass functions in bins of XOFF')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

env = sys.argv[1] # 'MD40' 
# initializes pathes to files
test_dir = os.path.join(os.environ[env], 'Mass_Xoff_Concentration')
path_2_snapshots = n.array(glob.glob(os.path.join(test_dir, 'distinct_?.*.fits')))

#boundaries = scoreatpercentile(x0ff, n.arange(0,101,10)) # deciles
#boundaries = scoreatpercentile(x0ff, n.arange(0,101,25)) # quartiles

# QUARTILE bins at redshift 0
xoff_bins_Q = n.array([0.,  4.28608992e-02, 6.59490117e-02, 1.00837135e-01, 5.7516e-01])

# DECILE bins at redshift 0
xoff_bins_D = n.array([0., 2.8200e-02, 3.8350e-02, 4.7280e-02, 5.6230e-02, 6.5950e-02, 7.7280e-02, 9.1650e-02, 1.1230e-01, 1.4953e-01, 5.7516e-01])

m_bins = n.arange(9,16,0.05)

for p2s in path_2_snapshots:
	out_file = os.path.join(test_dir, 'HMF', 'HMF_'+os.path.basename(p2s)[:-1])
	hd = fits.open(p2s)
	x0ff = hd[1].data['Xoff']/hd[1].data['rvir']
	mass = n.log10(hd[1].data['Mvir'])

	def get_mhist(selection) : 
		return n.histogram(mass[selection], bins = m_bins)[0]

	DATA = n.zeros((len(xoff_bins_D)-1, len(m_bins)-1))
	for jj, (x_min, x_max) in enumerate(zip(xoff_bins_D[:-1], xoff_bins_D[1:])):
		DATA[jj] = get_mhist( (x0ff>=x_min) & (x0ff<x_max) )

	DATA2 = n.zeros((len(xoff_bins_Q)-1, len(m_bins)-1))
	for jj, (x_min, x_max) in enumerate(zip(xoff_bins_Q[:-1], xoff_bins_Q[1:])):
		DATA2[jj] = get_mhist( (x0ff>=x_min) & (x0ff<x_max) )

	t = Table()
	t.add_column(Column(name='log_mvir_min', data=m_bins[:-1], unit=''))
	t.add_column(Column(name='log_mvir_max', data=m_bins[1:], unit=''))
	t.add_column(Column(name='Ntotal', data=DATA.sum(axis=0), unit=''))
	t.add_column(Column(name='D0', data=DATA[0], unit=''))
	t.add_column(Column(name='D1', data=DATA[1], unit=''))
	t.add_column(Column(name='D2', data=DATA[2], unit=''))
	t.add_column(Column(name='D3', data=DATA[3], unit=''))
	t.add_column(Column(name='D4', data=DATA[4], unit=''))
	t.add_column(Column(name='D5', data=DATA[5], unit=''))
	t.add_column(Column(name='D6', data=DATA[6], unit=''))
	t.add_column(Column(name='D7', data=DATA[7], unit=''))
	t.add_column(Column(name='D8', data=DATA[8], unit=''))
	t.add_column(Column(name='D9', data=DATA[9], unit=''))
	t.add_column(Column(name='Q0', data=DATA2[0], unit=''))
	t.add_column(Column(name='Q1', data=DATA2[1], unit=''))
	t.add_column(Column(name='Q2', data=DATA2[2], unit=''))
	t.add_column(Column(name='Q3', data=DATA2[3], unit=''))
	t.write(out_file, overwrite=True)
