"""
What it does
------------

Computes the clustering of halos as a function of mass and Xoff

python3 002_2PCF.py environmentVAR 

arguments
---------

environmentVAR: environment variable linking to the directory where files are e.g. "MD10"

Dependencies
------------

import time, os, sys, numpy, scipy, astropy, h5py, extinction, matplotlib
CorrFunc

"""
import pandas as pd
import Corrfunc
from Corrfunc.theory import wp
from Corrfunc.theory import xi
from astropy.table import Table, Column
import sys, os, glob, time
#from astropy.cosmology import FlatLambdaCDM
#import astropy.units as u
#import astropy.constants as cc
import astropy.io.fits as fits
#from scipy.stats import scoreatpercentile
import numpy as n
print('Tabulates correlation functions in bins of XOFF')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

env = sys.argv[1] # 'MD40' 
# initializes pathes to files
test_dir = os.path.join(os.environ[env], 'Mass_Xoff_Concentration')
path_2_snapshots = n.array(glob.glob(os.path.join(test_dir, 'distinct_?.*.fits')))
#boundaries = scoreatpercentile(x0ff, n.arange(0,101,10)) # deciles
#boundaries = scoreatpercentile(x0ff, n.arange(0,101,25)) # quartiles

# QUARTILE bins at redshift 0
xoff_bins_Q = n.array([0.,  4.28608992e-02, 6.59490117e-02, 1.00837135e-01, 5.7516e-01])
# DECILE bins at redshift 0
#xoff_bins_D = n.array([0., 2.8200e-02, 3.8350e-02, 4.7280e-02, 5.6230e-02, 6.5950e-02, 7.7280e-02, 9.1650e-02, 1.1230e-01, 1.4953e-01, 5.7516e-01])

m_mins = n.arange(13.5,15,0.1)


for p2s in path_2_snapshots: 
	out_file = os.path.join(test_dir, 'CorrelationFunctions', 'CorrelationFunction_'+os.path.basename(p2s)[:-5])
	hd = fits.open(p2s)
	t = Table.read(p2s)
	df = t.to_pandas()
	# defines arrays
	mass = df.Mvir
	X = df.x 
	Y = df.y 
	Z = df.z
	x0ff = df.Xoff / df.Rvir

	boxsize = 4000.0
	nthreads = 1
	pimax = 40.0
	bins = n.arange(4, 40, 4) 

	for min_mass in m_mins: # = 14.0
		for jj, (x_min, x_max) in enumerate(zip(xoff_bins_Q[:-1], xoff_bins_Q[1:])):
			print(jj, x_min, x_max)
			s1 = (x0ff>=x_min) & (x0ff<x_max) & (mass>10**min_mass) & (X>0) & (X<boxsize)& (Y>0) & (Y<boxsize)& (Z>0) & (Z<boxsize)
			x, y, z = n.array(X[s1]), n.array(Y[s1]), n.array(Z[s1])
			xi_results = xi(boxsize, nthreads, bins, x, y, z )
			print(xi_results)
			t_out = Table()
			for name_col in xi_results.dtype.names:
				t_out.add_column(Column(name=name_col, data=xi_results[name_col], unit='' ) )	
			t_out.write(out_file+'_M_gt_'+str(min_mass)+'_'+str(x_min)+'_'+str(x_max)+'.fit', overwrite=True)
		
