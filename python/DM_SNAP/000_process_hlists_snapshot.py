"""

Replicates the snapshot over the sky and extracts halos in a given shell of comoving distance
Does full sky replication

Parameters
==========

 * l_box: length of the box in Mpc/h, example 400
 * h: unitless hubble parameter h=H0/100mk/s/Mpc, example 0.6777
 * N_skip: how many lines of header should be skipped while reading the halo file
 * env: environment variable linking to the simulation directory, example MD04 MD10, UNIT_fA1_DIR ...
 * path_2_header: path to the header of the output file
 * path_2_in_0: path to the rockstar hlist file, example /path/2/simulation/hlist_1.00000.list.bz2

Processing
==========

bunzip the file (optional)

Generates and executes awk commands to replicate and filter the halo files. Lines 120 - 129 are the most important lines of the script.

Concatenate all replicated files with a header

Convert to a fits file

At MPCDF


module load jdk
conda activate pyCONDA
cd $GIT_AGN_MOCK/python/DM_SNAP/
python 000_process_hlists_snapshot.py 63 MD25 2400000000000.0 /u/joco/ptmp_joco/simulations/MD_2.5Gpc/hlists/hlist_0.08800.list.bz2

outputs
=======

 * directory(path_2_in_0)/replication_$expansionParameter/all the files: contains a single file per replication. Deleted after the computation finishes.
 * directory(path_2_in_0)/replicated_$expansionParameter/one summary file: contains one file with all replication concatenated. Could be deleted after the computation is done.
 * $env/fits/all_'+$expansionParameter+'.fits') the former file converted to fits staged into the final directory. Keep this one ! It is the shell containing all the halos


stilts tpipe in="+out_file+" ifmt=ascii omode=out ofmt=fits out=

stilts tpipe in=snapshot_0.088.list  ifmt=ascii omode=out ofmt=fits out=distinct_0.088.fits 
stilts tpipe in=snapshot_0.102.list  ifmt=ascii omode=out ofmt=fits out=distinct_0.102.fits 
stilts tpipe in=snapshot_0.115.list  ifmt=ascii omode=out ofmt=fits out=distinct_0.115.fits 
stilts tpipe in=snapshot_0.135.list  ifmt=ascii omode=out ofmt=fits out=distinct_0.135.fits 
stilts tpipe in=snapshot_0.156.list  ifmt=ascii omode=out ofmt=fits out=distinct_0.156.fits 
stilts tpipe in=snapshot_0.196.list  ifmt=ascii omode=out ofmt=fits out=distinct_0.196.fits 
stilts tpipe in=snapshot_0.227.list  ifmt=ascii omode=out ofmt=fits out=distinct_0.227.fits 
stilts tpipe in=snapshot_0.257.list  ifmt=ascii omode=out ofmt=fits out=distinct_0.257.fits 
stilts tpipe in=snapshot_0.287.list  ifmt=ascii omode=out ofmt=fits out=distinct_0.287.fits 
stilts tpipe in=snapshot_0.318.list  ifmt=ascii omode=out ofmt=fits out=distinct_0.318.fits 
stilts tpipe in=snapshot_0.409.list  ifmt=ascii omode=out ofmt=fits out=distinct_0.409.fits 
stilts tpipe in=snapshot_0.53.list   ifmt=ascii omode=out ofmt=fits out=distinct_0.53.fits  
stilts tpipe in=snapshot_0.5444.list ifmt=ascii omode=out ofmt=fits out=distinct_0.5444.fits
stilts tpipe in=snapshot_0.5504.list ifmt=ascii omode=out ofmt=fits out=distinct_0.5504.fits
stilts tpipe in=snapshot_0.5563.list ifmt=ascii omode=out ofmt=fits out=distinct_0.5563.fits
stilts tpipe in=snapshot_0.5623.list ifmt=ascii omode=out ofmt=fits out=distinct_0.5623.fits
stilts tpipe in=snapshot_0.5684.list ifmt=ascii omode=out ofmt=fits out=distinct_0.5684.fits
stilts tpipe in=snapshot_0.5743.list ifmt=ascii omode=out ofmt=fits out=distinct_0.5743.fits
stilts tpipe in=snapshot_0.5803.list ifmt=ascii omode=out ofmt=fits out=distinct_0.5803.fits
stilts tpipe in=snapshot_0.5864.list ifmt=ascii omode=out ofmt=fits out=distinct_0.5864.fits
stilts tpipe in=snapshot_0.5924.list ifmt=ascii omode=out ofmt=fits out=distinct_0.5924.fits
stilts tpipe in=snapshot_0.5983.list ifmt=ascii omode=out ofmt=fits out=distinct_0.5983.fits
stilts tpipe in=snapshot_0.5.list    ifmt=ascii omode=out ofmt=fits out=distinct_0.5.fits   
stilts tpipe in=snapshot_0.6043.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6043.fits
stilts tpipe in=snapshot_0.6104.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6104.fits
stilts tpipe in=snapshot_0.6163.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6163.fits
stilts tpipe in=snapshot_0.6223.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6223.fits
stilts tpipe in=snapshot_0.6284.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6284.fits
stilts tpipe in=snapshot_0.6344.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6344.fits
stilts tpipe in=snapshot_0.6403.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6403.fits
stilts tpipe in=snapshot_0.6464.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6464.fits
stilts tpipe in=snapshot_0.6524.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6524.fits
stilts tpipe in=snapshot_0.6583.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6583.fits
stilts tpipe in=snapshot_0.6643.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6643.fits
stilts tpipe in=snapshot_0.6704.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6704.fits
stilts tpipe in=snapshot_0.6763.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6763.fits
stilts tpipe in=snapshot_0.6823.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6823.fits
stilts tpipe in=snapshot_0.6884.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6884.fits
stilts tpipe in=snapshot_0.6944.list ifmt=ascii omode=out ofmt=fits out=distinct_0.6944.fits
stilts tpipe in=snapshot_0.7003.list ifmt=ascii omode=out ofmt=fits out=distinct_0.7003.fits
stilts tpipe in=snapshot_0.7064.list ifmt=ascii omode=out ofmt=fits out=distinct_0.7064.fits
stilts tpipe in=snapshot_0.7124.list ifmt=ascii omode=out ofmt=fits out=distinct_0.7124.fits
stilts tpipe in=snapshot_0.7183.list ifmt=ascii omode=out ofmt=fits out=distinct_0.7183.fits
stilts tpipe in=snapshot_0.7243.list ifmt=ascii omode=out ofmt=fits out=distinct_0.7243.fits
stilts tpipe in=snapshot_0.7363.list ifmt=ascii omode=out ofmt=fits out=distinct_0.7363.fits
stilts tpipe in=snapshot_0.7423.list ifmt=ascii omode=out ofmt=fits out=distinct_0.7423.fits
stilts tpipe in=snapshot_0.7544.list ifmt=ascii omode=out ofmt=fits out=distinct_0.7544.fits
stilts tpipe in=snapshot_0.7603.list ifmt=ascii omode=out ofmt=fits out=distinct_0.7603.fits
stilts tpipe in=snapshot_0.7724.list ifmt=ascii omode=out ofmt=fits out=distinct_0.7724.fits
stilts tpipe in=snapshot_0.7783.list ifmt=ascii omode=out ofmt=fits out=distinct_0.7783.fits
stilts tpipe in=snapshot_0.7873.list ifmt=ascii omode=out ofmt=fits out=distinct_0.7873.fits
stilts tpipe in=snapshot_0.8144.list ifmt=ascii omode=out ofmt=fits out=distinct_0.8144.fits
stilts tpipe in=snapshot_0.8173.list ifmt=ascii omode=out ofmt=fits out=distinct_0.8173.fits
stilts tpipe in=snapshot_0.8234.list ifmt=ascii omode=out ofmt=fits out=distinct_0.8234.fits
stilts tpipe in=snapshot_0.8263.list ifmt=ascii omode=out ofmt=fits out=distinct_0.8263.fits
stilts tpipe in=snapshot_0.8324.list ifmt=ascii omode=out ofmt=fits out=distinct_0.8324.fits
stilts tpipe in=snapshot_0.8353.list ifmt=ascii omode=out ofmt=fits out=distinct_0.8353.fits
stilts tpipe in=snapshot_0.8414.list ifmt=ascii omode=out ofmt=fits out=distinct_0.8414.fits
stilts tpipe in=snapshot_0.8443.list ifmt=ascii omode=out ofmt=fits out=distinct_0.8443.fits

stilts tpipe in=snapshot_0.8504.list ifmt=ascii omode=out ofmt=fits out=distinct_0.8504.fits
stilts tpipe in=snapshot_0.8533.list ifmt=ascii omode=out ofmt=fits out=distinct_0.8533.fits
stilts tpipe in=snapshot_0.8594.list ifmt=ascii omode=out ofmt=fits out=distinct_0.8594.fits
stilts tpipe in=snapshot_0.8623.list ifmt=ascii omode=out ofmt=fits out=distinct_0.8623.fits

stilts tpipe in=snapshot_1.0.list ifmt=ascii omode=out ofmt=fits out=distinct_1.0.fits

snapshot_1.0.list
"""
print('runs 000_process_hlists_snapshot.py with arguments' )
import time
t0=time.time()
import numpy as n
import os, sys, subprocess
print(sys.argv)

N_skip = int(sys.argv[1])
env = sys.argv[2]
Mmin_str = sys.argv[3] # '9.0e9'
path_2_in_0 = sys.argv[4]

# deduce the L_box without h
path_2_header =os.path.join( os.environ['GIT_AGN_MOCK'], 'python', 'DM_SNAP', 'header')

# if the file is zipped, first unzip
# else does nothing
# unzipped name is assigned in the variable: path_2_in
if path_2_in_0.split('.')[-1] == 'bz2':
    print('bunzip2 the file')
    os.chdir(os.path.dirname(path_2_in_0))
    os.system('bunzip2 ' + path_2_in_0)
    path_2_in = path_2_in_0[:-4]
else:
    path_2_in = path_2_in_0


def get_a(path_2_in):
    """
    Function to retrieves the expansion parameter from the name of the file, not form the header as headers can vary from version to version.
    """
    alp = os.path.basename(path_2_in).split('_')[1][:7]
    print('a=', alp)
    return float(alp)
    
a_snap = get_a(path_2_in)

# creates the directory where the outputs will be temporarily stored :
out_dir = os.path.join(os.environ[env], 'Mass_Xoff_Concentration')
if os.path.isdir(out_dir) == False:
	os.mkdir(out_dir)

######################
# WRITES and executes THE AWK COMMANDS
######################
# N_skip=64
def transform_rockstar_out_catalog_into_small_ascii_catalog_distinct(path_2_in, path_2_out, Mmin_str=Mmin_str, N_skip=N_skip):
    """
    writes out only the interesting columns and the x,y,z coordinates 
    
    parameters
    
    * path_2_in: path to input files
    * path_2_out: path to output files
    * Mmin_str: minimum virial mass string
    * N_skip: number of lines skipped at the beginning of the file (header), typically 64 for the Rockstar halo files
    
    """
    gawk_start = """gawk 'NR>""" + str(N_skip)
    # mvir mass filter
    gawk_filter_mass = """ {if ( ( $39 >= """+Mmin_str+""" )  && ( $6==-1 ) """
    # print columns
    # Mvir Rvir rs x y z Xoff 
    gawk_print = """) print  $11, $12, $13, $14, $18, $19, $20, $27, $39, $40, $41, $42, $44, $46}' """
    gawk_end = path_2_in + " > " + path_2_out
    # full command :
    gawk_command = gawk_start + gawk_filter_mass + gawk_print + gawk_end
    print(gawk_command)
    print('-------------------------------')
    os.system(gawk_command)

# filter columns and distinct halos above a certain mass
path_2_out = os.path.join( out_dir, 'snapshot_noHead_'+str(a_snap)+'.list')
transform_rockstar_out_catalog_into_small_ascii_catalog_distinct(path_2_in, path_2_out, Mmin_str=Mmin_str, N_skip=N_skip)
# add the header
out_file = os.path.join( out_dir, 'snapshot_'+str(a_snap)+'.list')
command = 'cat '+path_2_header+' ' + path_2_out + ' > '+ out_file
print(command)
os.system(command)

out_fits = os.path.join(out_dir, 'distinct_'+str(a_snap)+".fits")
# convert the ascii file to the fits format 
stilts_command = "stilts tpipe in="+out_file+" ifmt=ascii omode=out ofmt=fits out="+out_fits
print(stilts_command)
os.system(stilts_command)

print('deletes intermediate files')
os.system('rm '+ out_file)
os.system('rm '+ path_2_out)

print('bzip2 ', path_2_in)
os.chdir(os.path.dirname(path_2_in))
os.system('bzip2 ' + path_2_in)

print('finished, dt=', time.time()-t0, 'seconds')
