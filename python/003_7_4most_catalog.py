"""
Creates a fits catalog containing the 4FS input columns.

Create the ID array
remove unwanted column
check rulesets and templates names are correct


AGN_DEEP:
Footprints: Union of WAVES WIDE in the eROSITA_DE part of the sky, PLUS the old DEEP area around the SEP.

WAVES WIDE area (see https://wavesurvey.org/project/survey-design/):

North: RA=[157:225]; DEC=[-3:4]

South: RA=[330:52.5]; DEC=[-27:-35]

Selection: All eRASS:8 AGN with z<0.85 and R_AB(attenuated)<20.5 [this should ensure a good mix of type 1 and type 2, to about 10 objects per deg^2; to be verified]

RULES and RULESETS: As for the AGN, but replacing the SNR/AA limit to 10 everywhere

T_Max=240 min

A_Req = 1000 deg^2 (check that this is smaller than area provided]
SSM=0.5 -> Completeness of 90%

LSM=FLAT over the footprints

(My estimate is that this should cost about 4*10^4 Fiber Hours)
AGN_WIDE:

Footprints: All eROSITA_DE comprised between DEC=[-80:+5] (like the Clusters, same LSM as the clusters)

Selection: All eRASS:8 with r_AB<22.8 (as before) [NOTE: we will remove faint optical counterparts from the old DEEP region]; Need to remove the new DEEP sources from this
RULES an RULESETS unchanged
T_max unchanged

A_req unchanged
SSM unchanged


"""
import glob
import sys
import healpy
import os
from scipy.stats import scoreatpercentile
import pandas as pd  # external package
from scipy.special import erf
from astropy.coordinates import SkyCoord
import astropy.constants as cc
import astropy.io.fits as fits
from astropy.table import Table, Column
import astropy.units as u
import numpy as n
import extinction
print('CREATES 4FS FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')

# astropy
#from lib_model_agn import *
#option = sys.argv[1]
#option = 'SNR3'
#option = 'eRASS8'
#option = 'eRASS3'

env = sys.argv[1] # 'UNIT_fA1i_DIR'

root_dir = os.path.join(os.environ[env])

dir_2_eRO_all = os.path.join(root_dir, "cat_AGN-MAG_all")
dir_2_AGN_all = os.path.join(root_dir, "cat_AGN_all")

dir_2_4MOST = os.path.join(root_dir, "cat_AGN_4MOST")

if os.path.isdir(dir_2_4MOST) == False:
    os.system('mkdir -p ' + dir_2_4MOST)

path_2_flux_limits = os.path.join(os.environ['GIT_AGN_MOCK'], "data", "erosita", "flux_limits.fits")
flux_lim_data = fits.open(path_2_flux_limits) 
#flux_limit_eRASS3, flux_limit_eRASS8, flux_limit_SNR3
FX_LIM = flux_lim_data[1].data['flux_limit_SNR3']
#FX_LIM = n.log10(2e-17 )


area_per_cat = healpy.nside2pixarea(8, degrees=True)
N_pixels = healpy.nside2npix(8)
N_subsurvey = {'AGN_WIDE':1, 'AGN_DEEP':2, 'AGN_IR':3, 'QSO':4, 'LyA':5 }

def compute_pixel(HEALPIX_id):
	#HEALPIX_id = 64
	print(HEALPIX_id)
	path_2_eRO_all_catalog = os.path.join(dir_2_eRO_all, str(HEALPIX_id).zfill(6) + '.fit')
	path_2_AGN_all_catalog = os.path.join(dir_2_AGN_all, str(HEALPIX_id).zfill(6) + '.fit')
	path_2_AGN_WIDE_catalog = os.path.join( dir_2_4MOST, 'AGN_WIDE_' + str(HEALPIX_id).zfill(6) + '.fit')
	path_2_AGN_DEEP_catalog = os.path.join( dir_2_4MOST, 'AGN_DEEP_' + str(HEALPIX_id).zfill(6) + '.fit')
	path_2_AGN_IR_catalog = os.path.join( dir_2_4MOST, 'AGN_IR_' + str(HEALPIX_id).zfill(6) + '.fit')
	path_2_QSO_catalog = os.path.join( dir_2_4MOST, 'QSO_' + str(HEALPIX_id).zfill(6) + '.fit')
	path_2_LyA_catalog = os.path.join( dir_2_4MOST, 'LyA_' + str(HEALPIX_id).zfill(6) + '.fit')

	bitlist = {'AGN_WIDE':0, 'AGN_DEEP':1, 'AGN_IR':2, 'QSO':3, 'LyA':4 }
	priority_values = {'AGN_WIDE':100, 'AGN_DEEP':100, 'AGN_IR':99, 'QSO':99, 'LyA':100 }

	# reassigns templates correctly
	z_all = n.hstack(( 0., n.arange(0.3, 3., 0.2), 3.5, 4.5, 6. ))
	zmins = z_all[:-1]
	zmaxs = z_all[1:]

	hd_all = fits.open(path_2_eRO_all_catalog)
	hd_agn = fits.open(path_2_AGN_all_catalog)
	N_agn_all = len(hd_all[1].data['ra'])

	pix_ids = healpy.ang2pix(512, n.pi/2. - hd_all[1].data['g_lat'] *n.pi /180., hd_all[1].data['g_lon']*n.pi /180., nest=True)
	FX_LIM_value_cen = 10**( FX_LIM[pix_ids] )
	# =============================
	# =============================
	# eROSITA SAMPLE
	# apply flux limit erosita
	# =============================
	# =============================
	# X-ray selections, cat_AGN-MAG_all
	detected_all = (hd_all[1].data['FX_soft'] > FX_LIM_value_cen) & ( hd_all[1].data['g_lon'] > 180) & ( hd_all[1].data['dec'] < 5 ) & ( hd_all[1].data['dec'] > -80 )
	print(len((detected_all).nonzero()[0])*768 )
	# optical cut
	opt_all = ( hd_all[1].data['SDSS_r_AB_attenuated'] < 23.5 )
	# overall erosita selections
	ero_all = ( detected_all ) & ( opt_all )
	# optical t1
	opt_t11_all = (opt_all) & (hd_all[1].data['agn_type']==11)
	# type 2 AGN
	T2_all = (hd_all[1].data['agn_type'] == 22) | (hd_all[1].data['agn_type'] == 21)
	# wise selection
	wise_def_all = (hd_all[1].data['WISE-W1'] < 26) & (hd_all[1].data['WISE-W1_err'] < 1) & (hd_all[1].data['WISE-W1'] < 18.0 ) # 18.85)
	# IR sample, not detected in X-ray i.e. additional targets
	agn_ir_all =   (T2_all) & (opt_all) & (wise_def_all) & ( hd_all[1].data['SDSS_r_AB_attenuated'] < 21.5 ) # ( detected_all == False) &
	# area selection
	area_erosita_all = (abs(hd_all[1].data['g_lat']) > 10) & (hd_all[1].data['dec'] < 5) & ( hd_all[1].data['dec'] > -80 )
	# area opt-IR
	area_optIR_all = (abs(hd_all[1].data['g_lat']) > 10) & (hd_all[1].data['dec'] < 5) & ( hd_all[1].data['dec'] > -80 )
	# selection booleans
	is_erosita_all = ( (ero_all) & (area_erosita_all) )
	is_optIR_all   = ( (agn_ir_all) & (area_optIR_all) ) 
	is_optT11_all  = ( (opt_t11_all) & (area_optIR_all) )
	# complete selection for all 4most sub surveys
	sel_all = ( is_erosita_all ) | ( is_optIR_all ) | ( is_optT11_all )
	# =============================
	# =============================
	# COSMO SAMPLE
	# apply optical flux limits
	# =============================
	# =============================
	# optical + type selections, cat_AGN_all
	s8_area_optIR_all = (abs(hd_agn[1].data['g_lat']) > 10) & (hd_agn[1].data['dec'] < 5) & ( hd_agn[1].data['dec'] > -80 )
	s8_opt_all = ( hd_agn[1].data['SDSS_r_AB_attenuated'] < 23.5 )
	s8_opt_t11_all = (s8_opt_all) & (hd_agn[1].data['agn_type']==11)
	s8_is_optT11_all = ( (s8_opt_t11_all) & (s8_area_optIR_all) )
	# S8, QSO
	s8_QSO_all = ( s8_is_optT11_all ) & (hd_agn[1].data['SDSS_r_AB_attenuated'] < 22.7 ) & ( hd_agn[1].data['redshift_R'] >=0.9 ) & ( hd_agn[1].data['redshift_R'] < 2.2 )
	# S8, Lya
	s8_LyA_all = ( s8_is_optT11_all ) & (hd_agn[1].data['SDSS_r_AB_attenuated'] < 22.7 ) & ( hd_agn[1].data['redshift_R'] >= 2.2 ) 
	# S8 all
	s8_sel_all = (s8_QSO_all) | (s8_LyA_all)
	
	# =============================
	# Define sub surveys
	# =============================
	# initialize target bits
	survbit_all = n.zeros(len(hd_all[1].data['dec']),dtype='int')
	# subsurvey selections 
	# S6, AGN_DEEP
	WavesNorth = ( hd_all[1].data['ra'] > 157 ) & ( hd_all[1].data['ra'] < 225 ) & ( hd_all[1].data['dec'] > -3 ) & ( hd_all[1].data['dec'] < 4 ) 
	WavesSouth = (( hd_all[1].data['ra'] > 330 ) | ( hd_all[1].data['ra'] < 52.5 )) & ( hd_all[1].data['dec'] > -35 ) & ( hd_all[1].data['dec'] < -27 ) 
	AGN_DEEP_all = (detected_all) & ( ( WavesNorth )|( WavesSouth )|( abs(hd_all[1].data['ecl_lat']) > 80 ) ) & ( hd_all[1].data['SDSS_r_AB_attenuated'] < 20.5 ) & ( hd_all[1].data['redshift_R'] < 0.85 )
	# (hd_all[1].data['SDSS_r_AB_attenuated'] < 23.5 ) # & ( hd_all[1].data['redshift_R'] >=0.9 ) & ( hd_all[1].data['redshift_R'] < 2.2 )
	# ( is_erosita_all ) & ( abs(hd_all[1].data['ecl_lat']) > 80 )
	# S6, AGN_WIDE
	AGN_WIDE_all = ( is_erosita_all ) & ( hd_all[1].data['SDSS_r_AB_attenuated'] < 22.8 ) & (AGN_DEEP_all==False) # ( abs(hd_all[1].data['ecl_lat']) < 80 )
	# S6, AGN_IR 
	AGN_IR_all = ( is_optIR_all ) 
	# S8, QSO
	QSO_all = ( is_optT11_all ) & (hd_all[1].data['SDSS_r_AB_attenuated'] < 22.5 ) & ( hd_all[1].data['redshift_R'] >=0.9 ) & ( hd_all[1].data['redshift_R'] < 2.2 )
	# S8, Lya
	LyA_all = ( is_optT11_all ) & (hd_all[1].data['SDSS_r_AB_attenuated'] < 22.5 ) & ( hd_all[1].data['redshift_R'] >= 2.2 ) 
	# assign target bits
	survbit_all[AGN_WIDE_all] += 2**bitlist['AGN_WIDE']
	#
	survbit_all[AGN_DEEP_all] += 2**bitlist['AGN_DEEP']
	#
	survbit_all[AGN_IR_all] += 2**bitlist['AGN_IR']
	#
	survbit_all[QSO_all] += 2**bitlist['QSO']
	#
	survbit_all[LyA_all] += 2**bitlist['LyA']
	#
	target_bit = survbit_all[sel_all]

	
	# subsurveys for S8 COSMO
	s8_survbit_all = n.zeros(len(hd_agn[1].data['dec']),dtype='int')
	# subsurvey selections 
	s8_survbit_all[s8_QSO_all] += 2**bitlist['QSO']
	#
	s8_survbit_all[s8_LyA_all] += 2**bitlist['LyA']
	#
	s8_target_bit = s8_survbit_all[s8_sel_all]


	# Now create a 4MOST file per sub survey
	# merge cen + sat
	def get_table(subsurvey, hd_all=hd_all, target_bit=target_bit, sel_all=sel_all):
		t_survey = Table()
		bit_value = bitlist[subsurvey]
		sel = ( target_bit & 2**bit_value != 0)

		def create_column(col_name, hd_all=hd_all, sel_all=sel_all):
			return hd_all[1].data[col_name][sel_all][sel]

		ra_array = create_column('ra')
		dec_array = create_column('dec')
		N_obj = len(ra_array)
		if N_obj>0:
			N1 = n.arange(N_obj)
			id_list = N_subsurvey[subsurvey]*1e10 + HEALPIX_id*1e6 + N1
			NAME = n.array([ str(int(el)).zfill(11) for el in id_list ])
			t_survey.add_column(Column(name='NAME', data=NAME, unit=''))
			t_survey.add_column(Column(name='RA', data=ra_array, unit='deg'))
			t_survey.add_column(Column(name='DEC', data=dec_array, unit='deg'))
			PMRA = n.zeros(N_obj)
			t_survey.add_column(Column(name='PMRA', data=PMRA, unit='mas/yr'))
			PMDEC = n.zeros(N_obj)
			t_survey.add_column(Column(name='PMDEC', data=PMDEC, unit='mas/yr'))
			EPOCH = n.ones(N_obj)*2000.
			t_survey.add_column(Column(name='EPOCH', data=EPOCH, unit='yr'))
			# 'RESOLUTION':n.int16, 1I
			RESOLUTION = n.ones(N_obj).astype('int')
			t_survey.add_column(Column(name='RESOLUTION', data=RESOLUTION, unit=''))

			SUBSURVEY = n.ones(N_obj).astype('str')
			SUBSURVEY[:] = subsurvey
			t_survey.add_column(Column(name='SUBSURVEY', data=SUBSURVEY, unit=''))
			# 'PRIORITY':n.int16, 1I
			PRIORITY = n.zeros(N_obj).astype('int') + priority_values[subsurvey]
			t_survey.add_column(Column(name='PRIORITY', data=PRIORITY, unit=''))

			galactic_ebv_array = create_column('ebv')
			t_survey.add_column(Column(name='REDDENING',data=galactic_ebv_array, unit='mag'))

			# REDDENING for templates
			ebv_1000 = (t_survey['REDDENING']*1000).astype('int')
			#print('EBV', n.min(ebv_1000), n.max(ebv_1000))
			ebv_1_0 = ( ebv_1000 > 1000 ) 
			ebv_0_5 = ( ebv_1000 > 500 ) & ( ebv_1000 <= 1000 ) 
			ebv_0_4 = ( ebv_1000 > 400 ) & ( ebv_1000 <= 500 ) 
			ebv_0_3 = ( ebv_1000 > 300 ) & ( ebv_1000 <= 400 ) 
			ebv_0_2 = ( ebv_1000 > 200 ) & ( ebv_1000 <= 300 ) 
			ebv_0_1 = ( ebv_1000 > 100 ) & ( ebv_1000 <= 200 ) 
			ebv_0_0 = ( ebv_1000 <= 100 ) 
			z_name = lambda z0, z1 : "_zmin_"+str(int(10*z0)).zfill(2)+"_zmax_"+str(int(10*z1)).zfill(2)
			# templates
			template_names = n.zeros(N_obj).astype('U100')
			#
			ruleset_array = n.zeros(N_obj).astype('str')
			# 
			if subsurvey == "AGN_WIDE" or subsurvey == "AGN_IR":
				ruleset_array[:] = "AGN_ALL_3PC"
			if subsurvey == "AGN_DEEP":
				ruleset_array[:] = "AGN_ALL_SN10"
			if subsurvey == "QSO" or subsurvey == "LyA":
				ruleset_array[:] = "COSMO_AGN"

			AGN_type_array = create_column('agn_type')
			AGN_random_number_array = create_column('random')
			z_array = create_column('redshift_R')

			QSO = (AGN_type_array == 11) | (AGN_type_array == 12)
			T2 = (AGN_type_array == 22) | (AGN_type_array == 21)
			ELL = (T2) & (AGN_random_number_array < 0.2)

			for z0, z1 in zip(zmins, zmaxs):
				zsel = (z_array >= z0) & (z_array < z1)
				template_names[(zsel)] = "4most_" + 'qso_BL' +  z_name(z0, z1) + '_EBV_0_01.fits'
				template_names[(QSO) & (zsel) & (ebv_0_0)] = "4most_" +  'qso_BL' + z_name(z0, z1) + '_EBV_0_01.fits'
				template_names[(QSO) & (zsel) & (ebv_0_1)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_1.fits'
				template_names[(QSO) & (zsel) & (ebv_0_2)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_2.fits'
				template_names[(QSO) & (zsel) & (ebv_0_3)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_3.fits'
				template_names[(QSO) & (zsel) & (ebv_0_4)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_4.fits'
				template_names[(QSO) & (zsel) & (ebv_0_5)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_0_5.fits'
				template_names[(QSO) & (zsel) & (ebv_1_0)] = "4most_" + 'qso_BL' + z_name(z0, z1) + '_EBV_1_0.fits'
				if z1 < 2.2:
					template_names[(T2) & (zsel) & (ebv_0_0)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_01.fits'
					template_names[(T2) & (zsel) & (ebv_0_1)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_1.fits'
					template_names[(T2) & (zsel) & (ebv_0_2)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_2.fits'
					template_names[(T2) & (zsel) & (ebv_0_3)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_3.fits'
					template_names[(T2) & (zsel) & (ebv_0_4)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_4.fits'
					template_names[(T2) & (zsel) & (ebv_0_5)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_5.fits'
					template_names[(T2) & (zsel) & (ebv_1_0)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_1_0.fits'

					template_names[(ELL) & (zsel) & (ebv_0_0)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_01.fits'
					template_names[(ELL) & (zsel) & (ebv_0_1)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_1.fits'
					template_names[(ELL) & (zsel) & (ebv_0_2)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_2.fits'
					template_names[(ELL) & (zsel) & (ebv_0_3)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_3.fits'
					template_names[(ELL) & (zsel) & (ebv_0_4)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_4.fits'
					template_names[(ELL) & (zsel) & (ebv_0_5)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_0_5.fits'
					template_names[(ELL) & (zsel) & (ebv_1_0)] = "4most_" + 'LRG' + z_name(z0, z1) + '_EBV_1_0.fits'
				if z1 >= 2.2 and z1 < 6.:
					template_names[(T2) & (zsel) & (ebv_0_0)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_01.fits'
					template_names[(T2) & (zsel) & (ebv_0_1)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_1.fits'
					template_names[(T2) & (zsel) & (ebv_0_2)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_2.fits'
					template_names[(T2) & (zsel) & (ebv_0_3)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_3.fits'
					template_names[(T2) & (zsel) & (ebv_0_4)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_4.fits'
					template_names[(T2) & (zsel) & (ebv_0_5)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_5.fits'
					template_names[(T2) & (zsel) & (ebv_1_0)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_1_0.fits'
					template_names[(ELL) & (zsel) & (ebv_0_0)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_01.fits'
					template_names[(ELL) & (zsel) & (ebv_0_1)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_1.fits'
					template_names[(ELL) & (zsel) & (ebv_0_2)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_2.fits'
					template_names[(ELL) & (zsel) & (ebv_0_3)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_3.fits'
					template_names[(ELL) & (zsel) & (ebv_0_4)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_4.fits'
					template_names[(ELL) & (zsel) & (ebv_0_5)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_0_5.fits'
					template_names[(ELL) & (zsel) & (ebv_1_0)] = "4most_" + 'AGN_type2' + z_name(z0, z1) + '_EBV_1_0.fits'

			# 'TEMPLATE':str, max 256 char
			t_survey.add_column(Column(name='TEMPLATE', data=template_names, unit=''))
			# 'RULESET':str, max 256 char
			t_survey.add_column(Column(name='RULESET', data=ruleset_array, unit=''))
			# 'REDSHIFT_ESTIMATE':n.float32, 1E
			# 'REDSHIFT_ERROR':n.float32, 1E
			t_survey.add_column(Column(name='REDSHIFT_ESTIMATE', data=z_array, unit=''))
			t_survey.add_column(Column(name='REDSHIFT_ERROR', data=n.ones(N_obj), unit=''))
			# 'EXTENT_FLAG': 1I
			# =1
			# 'EXTENT_PARAMETER': 1E
			# =0
			# 'EXTENT_INDEX': 1E
			# =0
			t_survey.add_column(Column(name='EXTENT_FLAG'     , data=n.zeros(N_obj).astype('int') , unit=''))
			t_survey.add_column(Column(name='EXTENT_PARAMETER', data=n.zeros(N_obj), unit=''))
			t_survey.add_column(Column(name='EXTENT_INDEX'    , data=n.zeros(N_obj), unit=''))
			# 'MAG':n.float32,
			# 'MAG_ERR':n.float32
			# 'MAG_TYPE': str max 256 char
			SDSS_RMAG_array = create_column('SDSS_r_AB_attenuated')
			if subsurvey=='AGN_WIDE' or  subsurvey=='AGN_DEEP' or subsurvey=='AGN_IR' :
				HSC_RMAGERR_array = create_column('HSC-r_err')
			else:
				HSC_RMAGERR_array = n.ones_like(SDSS_RMAG_array)*0.05 #('HSC-r_err')
			#
			#r_v=3.1
			#a_v = galactic_ebv_array * r_v
			#delta_mag = n.hstack(( n.array([ extinction.fitzpatrick99(n.array([6500.]), el, r_v=3.1, unit='aa') for el in a_v ]) ))
			##rv = av/ebv
			##av = rv x ebv
			#extincted_mag = HSC_RMAG_array + delta_mag
			t_survey.add_column(Column(name='MAG', data=SDSS_RMAG_array, unit='mag'))
			t_survey.add_column(Column(name='MAG_ERR', data=HSC_RMAGERR_array, unit='mag'))
			MAG_TYPE = n.ones(N_obj).astype('str')
			MAG_TYPE[:] = 'DECam_r_AB'
			t_survey.add_column(Column(name='MAG_TYPE', data=MAG_TYPE, unit=''))
			# 'REDDENING':n.float32, 1E
			# 'DATE_EARLIEST':n.float64, JulianDate decimal days # 01-Nov-2022
			# 'DATE_LATEST':n.float64, JulianDate decimal days # 02-Feb-2033
			t_survey.add_column(Column(name='DATE_EARLIEST',data=2459945.5  * n.ones(N_obj), unit='d'))
			t_survey.add_column(Column(name='DATE_LATEST'  ,data=2462501.5 * n.ones(N_obj), unit='d'))

			t_survey.add_column(Column(name='target_bit'  ,data=target_bit[sel], unit=''))

			t_survey.add_column(Column(name='dL_cm'  ,data=create_column('dL'), unit='cm'))
			t_survey.add_column(Column(name='galactic_NH'  ,data=create_column('nH'), unit=''))
			t_survey.add_column(Column(name='galaxy_stellar_mass'  ,data=create_column('galaxy_SMHMR_mass'), unit=''))
			t_survey.add_column(Column(name='HALO_Mvir'  ,data=create_column('HALO_Mvir'), unit=''))
			t_survey.add_column(Column(name='HALO_pid'  ,data=create_column('HALO_pid'), unit=''))
			t_survey.add_column(Column(name='LX_soft'  ,data=create_column('LX_soft'), unit=''))
			t_survey.add_column(Column(name='FX_soft'  ,data=create_column('FX_soft'), unit=''))
			t_survey.add_column(Column(name='LX_hard'  ,data=create_column('LX_hard'), unit=''))
			t_survey.add_column(Column(name='FX_hard'  ,data=create_column('FX_hard'), unit=''))
			t_survey.add_column(Column(name='agn_type'  ,data=create_column('agn_type'), unit=''))
			t_survey.add_column(Column(name='logNH'  ,data=create_column('logNH'), unit=''))
			t_survey.add_column(Column(name='random'  ,data=AGN_random_number_array, unit=''))
			if subsurvey=='AGN_WIDE' or  subsurvey=='AGN_DEEP' or subsurvey=='AGN_IR' :
				t_survey.add_column(Column(name='GALEX-NUV'  ,data=create_column('GALEX-NUV'), unit=''))
				t_survey.add_column(Column(name='GALEX-FUV'  ,data=create_column('GALEX-FUV'), unit=''))
				t_survey.add_column(Column(name='WISE-W1'  ,data=create_column('WISE-W1'), unit=''))
				t_survey.add_column(Column(name='WISE-W2'  ,data=create_column('WISE-W2'), unit=''))
				t_survey.add_column(Column(name='HSC-g'  ,data=create_column('HSC-g'), unit=''))
				t_survey.add_column(Column(name='HSC-r'  ,data=create_column('HSC-r'), unit=''))
				t_survey.add_column(Column(name='HSC-i'  ,data=create_column('HSC-i'), unit=''))
				t_survey.add_column(Column(name='HSC-z'  ,data=create_column('HSC-z'), unit=''))
				t_survey.add_column(Column(name='HSC-y'  ,data=create_column('HSC-y'), unit=''))
				t_survey.add_column(Column(name='VISTA-J'  ,data=create_column('VISTA-J'), unit=''))
				t_survey.add_column(Column(name='VISTA-H'  ,data=create_column('VISTA-H'), unit=''))
				t_survey.add_column(Column(name='VISTA-K'  ,data=create_column('VISTA-K'), unit=''))
			t_survey.add_column(Column(name='g_lat'  ,data=create_column('g_lat'), unit='deg'))
			t_survey.add_column(Column(name='g_lon'  ,data=create_column('g_lon'), unit='deg'))
			t_survey.add_column(Column(name='ecl_lat'  ,data=create_column('ecl_lat'), unit='deg'))
			#t_survey.add_column(Column(name=''  ,data=create_column(''), unit=''))
			return t_survey
		else:
			return 0.

	subsurvey = 'AGN_WIDE'
	print(subsurvey)
	t_out = get_table(subsurvey)
	if t_out != 0:
		t_out.write (path_2_AGN_WIDE_catalog  , overwrite=True)
		print(subsurvey, 'N/deg2', len(t_out['RA'])/area_per_cat)

	subsurvey = 'AGN_DEEP'
	print(subsurvey)
	t_out = get_table(subsurvey)
	if t_out != 0:
		t_out.write (path_2_AGN_DEEP_catalog  , overwrite=True)
		print(subsurvey, 'N/deg2', len(t_out['RA'])/area_per_cat)

	subsurvey = 'AGN_IR'
	print(subsurvey)
	t_out = get_table(subsurvey)
	if t_out != 0:
		t_out.write (path_2_AGN_IR_catalog  , overwrite=True)
		print(subsurvey, 'N/deg2', len(t_out['RA'])/area_per_cat)

	subsurvey = 'QSO'
	print(subsurvey)
	t_out = get_table(subsurvey, hd_agn, s8_target_bit, s8_sel_all)
	if t_out != 0:
		t_out.write (path_2_QSO_catalog  , overwrite=True)
		print(subsurvey, 'N/deg2', len(t_out['RA'])/area_per_cat)

	subsurvey = 'LyA'
	print(subsurvey)
	t_out = get_table(subsurvey, hd_agn, s8_target_bit, s8_sel_all)
	if t_out != 0:
		t_out.write (path_2_LyA_catalog  , overwrite=True)
		print(subsurvey, 'N/deg2', len(t_out['RA'])/area_per_cat)


for HEALPIX_id in n.arange(N_pixels):
	compute_pixel(HEALPIX_id)
