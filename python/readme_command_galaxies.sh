cd $GIT_AGN_MOCK/python/

# Computes COORDINATES and basic GALAXY properties
#
# Step 1: computes coordinates
# script: 001_coordinates.py 
# inputs: ${env}/fits/all_?.?????.fits
# outputs: ${env}/fits/all_?.?????_coordinates.fits
#
# Step 2: computes approximate values of stellar mass and star formation rate
# script: 002_0_galaxy.py
# inputs: ${env}/fits/all_?.?????.fits
# outputs: ${env}/fits/all_?.?????_galaxy.fits 
nohup nice -n 19 python run_md_gal.py MD04          > run_gal_MD04_all.log & #  DONE           
nohup nice -n 19 python run_md_gal.py MD10          > logs/run_gal_MD10_all.log & #  DONE           
nohup nice -n 19 python run_md_gal.py MD40          > logs/run_gal_MD40_all.log & #  DONE           
nohup nice -n 19 python run_md_gal.py UNIT_fA1_DIR  > logs/run_gal_UNIT_fA1_DIR_all.log & #  DONE  
nohup nice -n 19 python run_md_gal.py UNIT_fA2_DIR  > logs/run_gal_UNIT_fA2_DIR_all.log & #  DONE 
nohup nice -n 19 python run_md_gal.py UNIT_fA1i_DIR > logs/run_gal_UNIT_fA1i_DIR_all.log & # DONE 
nohup nice -n 19 python run_md_gal.py UNIT_fA2i_DIR > logs/run_gal_UNIT_fA2i_DIR_all.log & # DONE 

rsync -avz joco@draco01.mpcdf.mpg.de:~/ptmp_joco/simulations/MD_1.0Gpc/UniverseMachine/sfr_catalog_0.1* .
rsync -avz joco@draco01.mpcdf.mpg.de:~/ptmp_joco/simulations/MD_1.0Gpc/UniverseMachine/sfr_catalog_0.2* .
rsync -avz joco@draco01.mpcdf.mpg.de:~/ptmp_joco/simulations/MD_1.0Gpc/UniverseMachine/sfr_catalog_0.3* .
rsync -avz joco@draco01.mpcdf.mpg.de:~/ptmp_joco/simulations/MD_1.0Gpc/UniverseMachine/sfr_catalog_0.4* .
rsync -avz joco@draco01.mpcdf.mpg.de:~/ptmp_joco/simulations/MD_1.0Gpc/UniverseMachine/sfr_catalog_0.5* .
rsync -avz joco@draco01.mpcdf.mpg.de:~/ptmp_joco/simulations/MD_1.0Gpc/UniverseMachine/sfr_catalog_0.6* .
rsync -avz joco@draco01.mpcdf.mpg.de:~/ptmp_joco/simulations/MD_1.0Gpc/UniverseMachine/sfr_catalog_0.7* .
rsync -avz joco@draco01.mpcdf.mpg.de:~/ptmp_joco/simulations/MD_1.0Gpc/UniverseMachine/sfr_catalog_0.8* .
rsync -avz joco@draco01.mpcdf.mpg.de:~/ptmp_joco/simulations/MD_1.0Gpc/UniverseMachine/sfr_catalog_0.9* .
rsync -avz joco@draco01.mpcdf.mpg.de:~/ptmp_joco/simulations/MD_1.0Gpc/UniverseMachine/sfr_catalog_1.0* .

# nohup python3 001_coordinates.py MD10 all_0.13920 > logs/MD10_all_0.13920001_0_coord.log &
# nohup python3 002_0_galaxy.py MD10 all_0.13920 > logs/MD10_all_0.13920002_0_galaxy.log &

# VERIFY FILES 
# DONE, March 16. Up to redshift 4.2 or so
# Can be re-run in a monthwhen more light cone shells are present
# 
ls $MD40/fits/all_?.?????.fits          | wc -l 
ls $MD40/fits/all_?.?????_coordinates.fits   | wc -l 
ls $MD40/fits/all_?.?????_galaxy.fits   | wc -l 

ls $MD04/fits/all_?.?????.fits          | wc -l 
ls $MD04/fits/all_?.?????_coordinates.fits   | wc -l 
ls $MD04/fits/all_?.?????_galaxy.fits   | wc -l 

ls $UNIT_fA1_DIR/fits/all_?.?????.fits          | wc -l 
ls $UNIT_fA1_DIR/fits/all_?.?????_coordinates.fits   | wc -l 
ls $UNIT_fA1_DIR/fits/all_?.?????_galaxy.fits   | wc -l 

ls $UNIT_fA1i_DIR/fits/all_?.?????.fits          | wc -l 
ls $UNIT_fA1i_DIR/fits/all_?.?????_coordinates.fits   | wc -l 
ls $UNIT_fA1i_DIR/fits/all_?.?????_galaxy.fits   | wc -l 

ll $UNIT_fA1_DIR/fits/all_?.?????.fits                 > lists/UNIT_fA1_fits_all_fits.list 
ll $UNIT_fA1_DIR/fits/all_?.?????_coordinates.fits     > lists/UNIT_fA1_fits_all_fits_coordinates.list 
ll $UNIT_fA1_DIR/fits/all_?.?????_galaxy.fits          > lists/UNIT_fA1_fits_all_fits_galaxy.list 

ll $UNIT_fA1i_DIR/fits/all_?.?????.fits                > lists/UNIT_fA1i_fits_all_fits.list 
ll $UNIT_fA1i_DIR/fits/all_?.?????_coordinates.fits    > lists/UNIT_fA1i_fits_all_fits_coordinates.list  
ll $UNIT_fA1i_DIR/fits/all_?.?????_galaxy.fits         > lists/UNIT_fA1i_fits_all_fits_galaxy.list 


ls $UNIT_fA2_DIR/fits/all_?.?????.fits          | wc -l 
ls $UNIT_fA2_DIR/fits/all_?.?????_coordinates.fits   | wc -l 
ls $UNIT_fA2_DIR/fits/all_?.?????_galaxy.fits   | wc -l 

ls $MD10/fits/all_?.?????.fits          | wc -l 
ls $MD10/fits/all_?.?????_coordinates.fits   | wc -l 
ls $MD10/fits/all_?.?????_galaxy.fits   | wc -l 

ll $MD10/fits/all_?.?????.fits               > lists/MD10_fits_all_fits.list 
ll $MD10/fits/all_?.?????_coordinates.fits   > lists/MD10_fits_all_fits_coordinates.list 
ll $MD10/fits/all_?.?????_galaxy.fits        > lists/MD10_fits_all_fits_galaxy.list 

wc -l lists/MD10_fits_all_fits.list 
wc -l lists/MD10_fits_all_fits_coordinates.list
wc -l lists/MD10_fits_all_fits_galaxy.list 

# kate lists/MD10_fits_all_fits.list 
# kate lists/MD10_fits_all_fits_coordinates.list
# kate lists/MD10_fits_all_fits_galaxy.list 

# figures in $GIT_AGN_MOCK/figures/${env}/galaxy/ 

# PIXELIZED GALAXY CATALOGS run 
#
# Step 1: pixelizes the catalogue by combining columns from the dark matter halo file, the coordinate and the galaxy files
# script: 002_1_galaxy_catalogs.py 
# inputs: ${env}/fits/all_?.?????.fits, ${env}/fits/all_?.?????_coordinates.fits, ${env}/fits/all_?.?????_galaxy.fits
# outputs:
# 768 files in : ${env}/fits/cat_GALAXY_?.?????/000???.fit 
# 
# Step 2
# Then concatenates the all healpix catalogs into $MD10/cat_GALAXY_all/
# TOPCAT commands
nohup nice -n 19 python run_galaxy_pixelized_catalogs.py MD04          > logs/run_gal_pixelize_MD04_all.log & # running he1srv            
nohup nice -n 19 python run_galaxy_pixelized_catalogs.py MD10          > logs/run_gal_pixelize_MD10_all.log & # running he1srv           
nohup nice -n 19 python run_galaxy_pixelized_catalogs.py MD40          > logs/run_gal_pixelize_MD40_all.log & # running he1srv           
nohup nice -n 19 python run_galaxy_pixelized_catalogs.py UNIT_fA1_DIR  > logs/run_gal_pixelize_UNIT_fA1_DIR_all.log & #  running he1srv    
nohup nice -n 19 python run_galaxy_pixelized_catalogs.py UNIT_fA2_DIR  > logs/run_gal_pixelize_UNIT_fA2_DIR_all.log & #  running he1srv    
nohup nice -n 19 python run_galaxy_pixelized_catalogs.py UNIT_fA1i_DIR > logs/run_gal_pixelize_UNIT_fA1i_DIR_all.log & # running he1srv    

nohup sh 002_1_galaxy_catalogs_UNIT_fA1.sh  > logs/002_1_galaxy_catalogs_UNIT_fA1.log  &
nohup sh 002_1_galaxy_catalogs_UNIT_fA1i.sh > logs/002_1_galaxy_catalogs_UNIT_fA1i.log &


nohup sh run_galaxy_pixelized_catalogs_MD10_commands.sh > logs/run_gal_pixelize_MD10_all.log &
sh run_galaxy_pixelized_catalogs_MD04_commands.sh 
 
ls $MD40/fits/all_?.?????_galaxy.fits   | wc -l 
ls $MD40/fits/cat_GALAXY_all_?.?????/000767.fit | wc -l
ls $MD40/cat_GALAXY_all/000???.fit | wc -l
ll $MD40/fits/cat_GALAXY_all_?.?????/000767.fit 
ll $MD40/cat_GALAXY_all/000767.fit

ls $MD04/fits/all_?.?????_galaxy.fits   | wc -l 
ls $MD04/fits/cat_GALAXY_all_?.?????/000767.fit | wc -l
ls $MD04/cat_GALAXY_all/000???.fit | wc -l
ll $MD04/fits/cat_GALAXY_all_?.?????/000767.fit 
ll $MD04/cat_GALAXY_all/000767.fit

ls $UNIT_fA1_DIR/fits/all_?.?????_galaxy.fits   | wc -l 
ls $UNIT_fA1_DIR/fits/cat_GALAXY_all_?.?????/000767.fit | wc -l
ls $UNIT_fA1_DIR/cat_GALAXY_all/000???.fit | wc -l
ll $UNIT_fA1_DIR/fits/cat_GALAXY_all_?.?????/000767.fit 
ll $UNIT_fA1_DIR/cat_GALAXY_all/000767.fit

ls $UNIT_fA2_DIR/fits/all_?.?????_galaxy.fits   | wc -l 
ls $UNIT_fA2_DIR/fits/cat_GALAXY_all_?.?????/000767.fit | wc -l
ls $UNIT_fA2_DIR/cat_GALAXY_all/000???.fit | wc -l
ll $UNIT_fA2_DIR/fits/cat_GALAXY_all_?.?????/000767.fit 
ll $UNIT_fA2_DIR/cat_GALAXY_all/000767.fit

ls $UNIT_fA1i_DIR/fits/all_?.?????_galaxy.fits   | wc -l 
ls $UNIT_fA1i_DIR/fits/cat_GALAXY_all_?.?????/000767.fit | wc -l
ls $UNIT_fA1i_DIR/cat_GALAXY_all/000???.fit | wc -l
ll $UNIT_fA1i_DIR/fits/cat_GALAXY_all_?.?????/000767.fit 
ll $UNIT_fA1i_DIR/cat_GALAXY_all/000767.fit

ls $MD10/fits/all_?.?????_galaxy.fits   | wc -l 
ls $MD10/fits/cat_GALAXY_all_?.?????/000767.fit | wc -l
ls $MD10/cat_GALAXY_all/000???.fit | wc -l
ll $MD10/fits/cat_GALAXY_all_?.?????/000767.fit 
ll $MD10/cat_GALAXY_all/000767.fit


# Add K-band absolute magnitude to the pixelized galaxy catalogs
# script: 002_2_galaxy_Kmag.py
# inputs: ${env}/cat_GALAXY_all/*.fit
# outputs: updates ${env}/cat_GALAXY_all/*.fit
nohup nice -n 19 python 002_2_galaxy_Kmag.py MD04 > logs/run_gal_Kmag_MD04_all.log & # 
nohup nice -n 19 python 002_2_galaxy_Kmag.py MD10 > logs/run_gal_Kmag_MD10_all.log & # 
nohup nice -n 19 python 002_2_galaxy_Kmag.py MD40 > logs/run_gal_Kmag_MD40_all.log & # 
nohup nice -n 19 python 002_2_galaxy_Kmag.py UNIT_fA1_DIR > logs/run_gal_Kmag_UNIT_fA1_DIR_all.log & # 
nohup nice -n 19 python 002_2_galaxy_Kmag.py UNIT_fA2_DIR > logs/run_gal_Kmag_UNIT_fA2_DIR_all.log & # 
nohup nice -n 19 python 002_2_galaxy_Kmag.py UNIT_fA1i_DIR > logs/run_gal_Kmag_UNIT_fA1i_DIR_all.log & # 

nohup sh 002_2_log/all_UNIT_fA1_DIR.sh  > 002_2_log/run_gal_Kmag_UNIT_fA1_DIR_all.log  &  
nohup sh 002_2_log/all_UNIT_fA1i_DIR.sh > 002_2_log/run_gal_Kmag_UNIT_fA1i_DIR_all.log &  

python 002_2_galaxy_Kmag_WriteScripts.py

cd $GIT_AGN_MOCK/python/002_2_log/
pyCONDA
ipython

import os
import glob
import numpy as n
scripts = sorted(n.array(glob.glob("MD04_*.sh")))
for script in scripts:
    os.system('sbatch ' + script)

import os
import glob
import numpy as n
scripts = sorted(n.array(glob.glob("MD10_*.sh")))
for script in scripts:
    os.system('sbatch ' + script)

import os
import glob
import numpy as n
scripts = sorted(n.array(glob.glob("UNIT_fA1_DIR_*.sh")))
for script in scripts:
    os.system('sbatch ' + script)

scripts = sorted(n.array(glob.glob("UNIT_fA1i_DIR_*.sh")))
for script in scripts:
    os.system('sbatch ' + script)


fstruct $MD04/cat_GALAXY_all/000767.fit
fstruct $MD10/cat_GALAXY_all/000767.fit
fstruct $UNIT_fA1_DIR/cat_GALAXY_all/000000.fit
# MD40, MD04 DONE
# MD10 ongoing
# needs to have 20 columns and the last one should be :
# 20 K_mag_abs D mag

nohup nice -n 19 python 002_2_galaxy_Kmag.py UNIT_fA1_DIR 000  > logs/run_gal_Kmag_UNIT_fA1_DIR_all_000.log & # 
nohup nice -n 19 python 002_2_galaxy_Kmag.py MD10 731  > logs/run_gal_Kmag_MD10_all_731.log & # 

cp $MD10/cat_GALAXY_all/000000.fit /data42s/comparat/firefly/mocks/2020-03/MDPL2/cat_GALAXY_all/000000.fit 


# simple light cone build for clustering analysis
# script: 006_0_LIGHT_CONE.py
# selects distinct halos Mvir > 1e13 until redshift 2
# inputs: ${env}/fits/all_?.?????.fits, ${env}/fits/all_?.?????_coordinates.fits, ${env}/fits/all_?.?????_galaxy.fits
# outputs: ${env}/fits/all_?.?????_LC.fits
# merges all columns from the inputs into a single light cone shell fits file
nohup nice -n 19  python run_LC.py MD04           >  logs/LC_MD04.log &                    
nohup nice -n 19  python run_LC.py MD10           >  logs/LC_MD10.log &                    
nohup nice -n 19  python run_LC.py MD40           >  logs/LC_MD40.log &                    
nohup nice -n 19  python run_LC.py UNIT_fA1_DIR   >  logs/LC_UNIT_fA1_DIR.log &  
nohup nice -n 19  python run_LC.py UNIT_fA2_DIR   >  logs/LC_UNIT_fA2_DIR.log &    
nohup nice -n 19  python run_LC.py UNIT_fA1i_DIR  >  logs/LC_UNIT_fA1i_DIR.log &  

ls $MD40/fits/*_LC.fits > fit_list_galaxiesLC.list
stilts tcat in=@fit_list_galaxiesLC.list ifmt=fits omode=out ofmt=fits out=$MD40/MD40_LC.fit

ls $MD40/cat_GALAXY_all/000???.fit > fit_list_galaxiesLC.list
stilts tcat in=@fit_list_galaxiesLC.list ifmt=fits omode=out ofmt=fits out=$MD40/MD40_LC.fit

ls $MD04/fits/*_LC.fits > fit_list_galaxiesLC.list
stilts tcat in=@fit_list_galaxiesLC.list ifmt=fits omode=out ofmt=fits out=$MD04/MD04_LC.fit

ls $MD10/fits/*_LC.fits > fit_list_galaxiesLC.list
stilts tcat in=@fit_list_galaxiesLC.list ifmt=fits omode=out ofmt=fits out=$MD10/MD10_LC.fit


cp $MD40/MD40_LC.fit /data42s/comparat/firefly/mocks/2020-03/HMD/ 
cp $MD04/MD04_LC.fit /data42s/comparat/firefly/mocks/2020-03/SMDPL/ 
cp $MD10/MD10_LC.fit /data42s/comparat/firefly/mocks/2020-03/MDPL2/ 


# ds52
cd $MD10/cat_GALAXY_all
for i in {0..9}
do
   tar cjf /data42s/comparat/firefly/mocks/2020-06/MDPL2/cat_GALAXY_all/00000$i.fit.tar.bz2 00000$i.fit
done

# ds52
cd $MD10/cat_GALAXY_all
for i in {10..99}
do
   tar cjf /data42s/comparat/firefly/mocks/2020-06/MDPL2/cat_GALAXY_all/0000$i.fit.tar.bz2 0000$i.fit
done

# ds52
cd $MD10/cat_GALAXY_all
for i in {100..767}
do
   tar cjf /data42s/comparat/firefly/mocks/2020-06/MDPL2/cat_GALAXY_all/000$i.fit.tar.bz2 000$i.fit
done

# ds52
cd $MD10/cat_AGN_all
for i in {0..9}
do
   tar cjf /data42s/comparat/firefly/mocks/2020-06/MDPL2/cat_AGN_all/00000$i.fit.tar.bz2 00000$i.fit
done

# ds52
cd $MD10/cat_AGN_all
for i in {10..99}
do
   tar cjf /data42s/comparat/firefly/mocks/2020-06/MDPL2/cat_AGN_all/0000$i.fit.tar.bz2 0000$i.fit
done

# ds23
cd $MD10/cat_AGN_all
for i in {100..767}
do
   tar cjf /data42s/comparat/firefly/mocks/2020-06/MDPL2/cat_AGN_all/000$i.fit.tar.bz2 000$i.fit
done



# ds23
cd $MD10/cat_AGN_SIMPUT
for i in {0..9}
do
   tar cjf /data42s/comparat/firefly/mocks/2020-06/MDPL2/cat_AGN_SIMPUT/00000$i.fit.tar.bz2 SIMPUT_00000${i}_*.fit
done

# ds23
cd $MD10/cat_AGN_SIMPUT
for i in {10..99}
do
   tar cjf /data42s/comparat/firefly/mocks/2020-06/MDPL2/cat_AGN_SIMPUT/0000$i.fit.tar.bz2 SIMPUT_0000${i}_*.fit
done

# ds23
cd $MD10/cat_AGN_SIMPUT
for i in {100..767}
do
   tar cjf /data42s/comparat/firefly/mocks/2020-06/MDPL2/cat_AGN_SIMPUT/000$i.fit.tar.bz2 SIMPUT_000${i}_*.fit
done


cd $MD10/cat_AGN_SIMPUT
tar cjf /data42s/comparat/firefly/mocks/2020-06/MDPL2/cat_AGN_SIMPUT/000000.fit.tar.bz2 SIMPUT_000000_*.fit

tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000000.fit.tar.gz 000000.fit

cd $MD10/cat_GALAXY_all/
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000000.fit.tar.gz  000000.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000001.fit.tar.gz  000001.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000002.fit.tar.gz  000002.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000003.fit.tar.gz  000003.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000004.fit.tar.gz  000004.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000005.fit.tar.gz  000005.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000006.fit.tar.gz  000006.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000007.fit.tar.gz  000007.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000008.fit.tar.gz  000008.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000009.fit.tar.gz  000009.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000010.fit.tar.gz  000010.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000011.fit.tar.gz  000011.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000012.fit.tar.gz  000012.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000013.fit.tar.gz  000013.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000014.fit.tar.gz  000014.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000015.fit.tar.gz  000015.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000016.fit.tar.gz  000016.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000017.fit.tar.gz  000017.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000018.fit.tar.gz  000018.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000019.fit.tar.gz  000019.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000020.fit.tar.gz  000020.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000021.fit.tar.gz  000021.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000022.fit.tar.gz  000022.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000023.fit.tar.gz  000023.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000024.fit.tar.gz  000024.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000025.fit.tar.gz  000025.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000026.fit.tar.gz  000026.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000027.fit.tar.gz  000027.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000028.fit.tar.gz  000028.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000029.fit.tar.gz  000029.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000030.fit.tar.gz  000030.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000031.fit.tar.gz  000031.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000032.fit.tar.gz  000032.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000033.fit.tar.gz  000033.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000034.fit.tar.gz  000034.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000035.fit.tar.gz  000035.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000036.fit.tar.gz  000036.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000037.fit.tar.gz  000037.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000038.fit.tar.gz  000038.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000039.fit.tar.gz  000039.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000040.fit.tar.gz  000040.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000041.fit.tar.gz  000041.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000042.fit.tar.gz  000042.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000043.fit.tar.gz  000043.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000044.fit.tar.gz  000044.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000045.fit.tar.gz  000045.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000046.fit.tar.gz  000046.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000047.fit.tar.gz  000047.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000048.fit.tar.gz  000048.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000049.fit.tar.gz  000049.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000050.fit.tar.gz  000050.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000051.fit.tar.gz  000051.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000052.fit.tar.gz  000052.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000053.fit.tar.gz  000053.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000054.fit.tar.gz  000054.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000055.fit.tar.gz  000055.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000056.fit.tar.gz  000056.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000057.fit.tar.gz  000057.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000058.fit.tar.gz  000058.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000059.fit.tar.gz  000059.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000060.fit.tar.gz  000060.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000061.fit.tar.gz  000061.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000062.fit.tar.gz  000062.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000063.fit.tar.gz  000063.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000064.fit.tar.gz  000064.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000065.fit.tar.gz  000065.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000066.fit.tar.gz  000066.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000067.fit.tar.gz  000067.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000068.fit.tar.gz  000068.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000069.fit.tar.gz  000069.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000070.fit.tar.gz  000070.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000071.fit.tar.gz  000071.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000072.fit.tar.gz  000072.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000073.fit.tar.gz  000073.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000074.fit.tar.gz  000074.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000075.fit.tar.gz  000075.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000076.fit.tar.gz  000076.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000077.fit.tar.gz  000077.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000078.fit.tar.gz  000078.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000079.fit.tar.gz  000079.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000080.fit.tar.gz  000080.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000081.fit.tar.gz  000081.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000082.fit.tar.gz  000082.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000083.fit.tar.gz  000083.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000084.fit.tar.gz  000084.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000085.fit.tar.gz  000085.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000086.fit.tar.gz  000086.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000087.fit.tar.gz  000087.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000088.fit.tar.gz  000088.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000089.fit.tar.gz  000089.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000090.fit.tar.gz  000090.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000091.fit.tar.gz  000091.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000092.fit.tar.gz  000092.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000093.fit.tar.gz  000093.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000094.fit.tar.gz  000094.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000095.fit.tar.gz  000095.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000096.fit.tar.gz  000096.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000097.fit.tar.gz  000097.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000098.fit.tar.gz  000098.fit
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000099.fit.tar.gz  000099.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000100.fit.tar.gz  000100.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000101.fit.tar.gz  000101.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000102.fit.tar.gz  000102.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000103.fit.tar.gz  000103.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000104.fit.tar.gz  000104.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000105.fit.tar.gz  000105.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000106.fit.tar.gz  000106.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000107.fit.tar.gz  000107.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000108.fit.tar.gz  000108.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000109.fit.tar.gz  000109.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000110.fit.tar.gz  000110.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000111.fit.tar.gz  000111.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000112.fit.tar.gz  000112.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000113.fit.tar.gz  000113.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000114.fit.tar.gz  000114.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000115.fit.tar.gz  000115.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000116.fit.tar.gz  000116.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000117.fit.tar.gz  000117.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000118.fit.tar.gz  000118.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000119.fit.tar.gz  000119.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000120.fit.tar.gz  000120.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000121.fit.tar.gz  000121.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000122.fit.tar.gz  000122.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000123.fit.tar.gz  000123.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000124.fit.tar.gz  000124.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000125.fit.tar.gz  000125.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000126.fit.tar.gz  000126.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000127.fit.tar.gz  000127.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000128.fit.tar.gz  000128.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000129.fit.tar.gz  000129.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000130.fit.tar.gz  000130.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000131.fit.tar.gz  000131.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000132.fit.tar.gz  000132.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000133.fit.tar.gz  000133.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000134.fit.tar.gz  000134.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000135.fit.tar.gz  000135.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000136.fit.tar.gz  000136.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000137.fit.tar.gz  000137.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000138.fit.tar.gz  000138.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000139.fit.tar.gz  000139.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000140.fit.tar.gz  000140.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000141.fit.tar.gz  000141.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000142.fit.tar.gz  000142.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000143.fit.tar.gz  000143.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000144.fit.tar.gz  000144.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000145.fit.tar.gz  000145.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000146.fit.tar.gz  000146.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000147.fit.tar.gz  000147.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000148.fit.tar.gz  000148.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000149.fit.tar.gz  000149.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000150.fit.tar.gz  000150.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000151.fit.tar.gz  000151.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000152.fit.tar.gz  000152.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000153.fit.tar.gz  000153.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000154.fit.tar.gz  000154.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000155.fit.tar.gz  000155.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000156.fit.tar.gz  000156.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000157.fit.tar.gz  000157.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000158.fit.tar.gz  000158.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000159.fit.tar.gz  000159.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000160.fit.tar.gz  000160.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000161.fit.tar.gz  000161.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000162.fit.tar.gz  000162.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000163.fit.tar.gz  000163.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000164.fit.tar.gz  000164.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000165.fit.tar.gz  000165.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000166.fit.tar.gz  000166.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000167.fit.tar.gz  000167.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000168.fit.tar.gz  000168.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000169.fit.tar.gz  000169.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000170.fit.tar.gz  000170.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000171.fit.tar.gz  000171.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000172.fit.tar.gz  000172.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000173.fit.tar.gz  000173.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000174.fit.tar.gz  000174.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000175.fit.tar.gz  000175.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000176.fit.tar.gz  000176.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000177.fit.tar.gz  000177.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000178.fit.tar.gz  000178.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000179.fit.tar.gz  000179.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000180.fit.tar.gz  000180.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000181.fit.tar.gz  000181.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000182.fit.tar.gz  000182.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000183.fit.tar.gz  000183.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000184.fit.tar.gz  000184.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000185.fit.tar.gz  000185.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000186.fit.tar.gz  000186.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000187.fit.tar.gz  000187.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000188.fit.tar.gz  000188.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000189.fit.tar.gz  000189.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000190.fit.tar.gz  000190.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000191.fit.tar.gz  000191.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000192.fit.tar.gz  000192.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000193.fit.tar.gz  000193.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000194.fit.tar.gz  000194.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000195.fit.tar.gz  000195.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000196.fit.tar.gz  000196.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000197.fit.tar.gz  000197.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000198.fit.tar.gz  000198.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000199.fit.tar.gz  000199.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000200.fit.tar.gz  000200.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000201.fit.tar.gz  000201.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000202.fit.tar.gz  000202.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000203.fit.tar.gz  000203.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000204.fit.tar.gz  000204.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000205.fit.tar.gz  000205.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000206.fit.tar.gz  000206.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000207.fit.tar.gz  000207.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000208.fit.tar.gz  000208.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000209.fit.tar.gz  000209.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000210.fit.tar.gz  000210.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000211.fit.tar.gz  000211.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000212.fit.tar.gz  000212.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000213.fit.tar.gz  000213.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000214.fit.tar.gz  000214.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000215.fit.tar.gz  000215.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000216.fit.tar.gz  000216.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000217.fit.tar.gz  000217.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000218.fit.tar.gz  000218.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000219.fit.tar.gz  000219.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000220.fit.tar.gz  000220.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000221.fit.tar.gz  000221.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000222.fit.tar.gz  000222.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000223.fit.tar.gz  000223.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000224.fit.tar.gz  000224.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000225.fit.tar.gz  000225.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000226.fit.tar.gz  000226.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000227.fit.tar.gz  000227.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000228.fit.tar.gz  000228.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000229.fit.tar.gz  000229.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000230.fit.tar.gz  000230.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000231.fit.tar.gz  000231.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000232.fit.tar.gz  000232.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000233.fit.tar.gz  000233.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000234.fit.tar.gz  000234.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000235.fit.tar.gz  000235.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000236.fit.tar.gz  000236.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000237.fit.tar.gz  000237.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000238.fit.tar.gz  000238.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000239.fit.tar.gz  000239.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000240.fit.tar.gz  000240.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000241.fit.tar.gz  000241.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000242.fit.tar.gz  000242.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000243.fit.tar.gz  000243.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000244.fit.tar.gz  000244.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000245.fit.tar.gz  000245.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000246.fit.tar.gz  000246.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000247.fit.tar.gz  000247.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000248.fit.tar.gz  000248.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000249.fit.tar.gz  000249.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000250.fit.tar.gz  000250.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000251.fit.tar.gz  000251.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000252.fit.tar.gz  000252.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000253.fit.tar.gz  000253.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000254.fit.tar.gz  000254.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000255.fit.tar.gz  000255.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000256.fit.tar.gz  000256.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000257.fit.tar.gz  000257.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000258.fit.tar.gz  000258.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000259.fit.tar.gz  000259.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000260.fit.tar.gz  000260.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000261.fit.tar.gz  000261.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000262.fit.tar.gz  000262.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000263.fit.tar.gz  000263.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000264.fit.tar.gz  000264.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000265.fit.tar.gz  000265.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000266.fit.tar.gz  000266.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000267.fit.tar.gz  000267.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000268.fit.tar.gz  000268.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000269.fit.tar.gz  000269.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000270.fit.tar.gz  000270.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000271.fit.tar.gz  000271.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000272.fit.tar.gz  000272.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000273.fit.tar.gz  000273.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000274.fit.tar.gz  000274.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000275.fit.tar.gz  000275.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000276.fit.tar.gz  000276.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000277.fit.tar.gz  000277.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000278.fit.tar.gz  000278.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000279.fit.tar.gz  000279.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000280.fit.tar.gz  000280.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000281.fit.tar.gz  000281.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000282.fit.tar.gz  000282.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000283.fit.tar.gz  000283.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000284.fit.tar.gz  000284.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000285.fit.tar.gz  000285.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000286.fit.tar.gz  000286.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000287.fit.tar.gz  000287.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000288.fit.tar.gz  000288.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000289.fit.tar.gz  000289.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000290.fit.tar.gz  000290.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000291.fit.tar.gz  000291.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000292.fit.tar.gz  000292.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000293.fit.tar.gz  000293.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000294.fit.tar.gz  000294.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000295.fit.tar.gz  000295.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000296.fit.tar.gz  000296.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000297.fit.tar.gz  000297.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000298.fit.tar.gz  000298.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000299.fit.tar.gz  000299.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000300.fit.tar.gz  000300.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000301.fit.tar.gz  000301.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000302.fit.tar.gz  000302.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000303.fit.tar.gz  000303.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000304.fit.tar.gz  000304.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000305.fit.tar.gz  000305.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000306.fit.tar.gz  000306.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000307.fit.tar.gz  000307.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000308.fit.tar.gz  000308.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000309.fit.tar.gz  000309.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000310.fit.tar.gz  000310.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000311.fit.tar.gz  000311.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000312.fit.tar.gz  000312.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000313.fit.tar.gz  000313.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000314.fit.tar.gz  000314.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000315.fit.tar.gz  000315.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000316.fit.tar.gz  000316.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000317.fit.tar.gz  000317.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000318.fit.tar.gz  000318.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000319.fit.tar.gz  000319.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000320.fit.tar.gz  000320.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000321.fit.tar.gz  000321.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000322.fit.tar.gz  000322.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000323.fit.tar.gz  000323.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000324.fit.tar.gz  000324.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000325.fit.tar.gz  000325.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000326.fit.tar.gz  000326.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000327.fit.tar.gz  000327.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000328.fit.tar.gz  000328.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000329.fit.tar.gz  000329.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000330.fit.tar.gz  000330.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000331.fit.tar.gz  000331.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000332.fit.tar.gz  000332.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000333.fit.tar.gz  000333.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000334.fit.tar.gz  000334.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000335.fit.tar.gz  000335.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000336.fit.tar.gz  000336.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000337.fit.tar.gz  000337.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000338.fit.tar.gz  000338.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000339.fit.tar.gz  000339.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000340.fit.tar.gz  000340.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000341.fit.tar.gz  000341.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000342.fit.tar.gz  000342.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000343.fit.tar.gz  000343.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000344.fit.tar.gz  000344.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000345.fit.tar.gz  000345.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000346.fit.tar.gz  000346.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000347.fit.tar.gz  000347.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000348.fit.tar.gz  000348.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000349.fit.tar.gz  000349.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000350.fit.tar.gz  000350.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000351.fit.tar.gz  000351.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000352.fit.tar.gz  000352.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000353.fit.tar.gz  000353.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000354.fit.tar.gz  000354.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000355.fit.tar.gz  000355.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000356.fit.tar.gz  000356.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000357.fit.tar.gz  000357.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000358.fit.tar.gz  000358.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000359.fit.tar.gz  000359.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000360.fit.tar.gz  000360.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000361.fit.tar.gz  000361.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000362.fit.tar.gz  000362.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000363.fit.tar.gz  000363.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000364.fit.tar.gz  000364.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000365.fit.tar.gz  000365.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000366.fit.tar.gz  000366.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000367.fit.tar.gz  000367.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000368.fit.tar.gz  000368.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000369.fit.tar.gz  000369.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000370.fit.tar.gz  000370.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000371.fit.tar.gz  000371.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000372.fit.tar.gz  000372.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000373.fit.tar.gz  000373.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000374.fit.tar.gz  000374.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000375.fit.tar.gz  000375.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000376.fit.tar.gz  000376.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000377.fit.tar.gz  000377.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000378.fit.tar.gz  000378.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000379.fit.tar.gz  000379.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000380.fit.tar.gz  000380.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000381.fit.tar.gz  000381.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000382.fit.tar.gz  000382.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000383.fit.tar.gz  000383.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000384.fit.tar.gz  000384.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000385.fit.tar.gz  000385.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000386.fit.tar.gz  000386.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000387.fit.tar.gz  000387.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000388.fit.tar.gz  000388.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000389.fit.tar.gz  000389.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000390.fit.tar.gz  000390.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000391.fit.tar.gz  000391.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000392.fit.tar.gz  000392.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000393.fit.tar.gz  000393.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000394.fit.tar.gz  000394.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000395.fit.tar.gz  000395.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000396.fit.tar.gz  000396.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000397.fit.tar.gz  000397.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000398.fit.tar.gz  000398.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000399.fit.tar.gz  000399.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000400.fit.tar.gz  000400.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000401.fit.tar.gz  000401.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000402.fit.tar.gz  000402.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000403.fit.tar.gz  000403.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000404.fit.tar.gz  000404.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000405.fit.tar.gz  000405.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000406.fit.tar.gz  000406.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000407.fit.tar.gz  000407.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000408.fit.tar.gz  000408.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000409.fit.tar.gz  000409.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000410.fit.tar.gz  000410.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000411.fit.tar.gz  000411.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000412.fit.tar.gz  000412.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000413.fit.tar.gz  000413.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000414.fit.tar.gz  000414.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000415.fit.tar.gz  000415.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000416.fit.tar.gz  000416.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000417.fit.tar.gz  000417.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000418.fit.tar.gz  000418.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000419.fit.tar.gz  000419.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000420.fit.tar.gz  000420.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000421.fit.tar.gz  000421.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000422.fit.tar.gz  000422.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000423.fit.tar.gz  000423.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000424.fit.tar.gz  000424.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000425.fit.tar.gz  000425.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000426.fit.tar.gz  000426.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000427.fit.tar.gz  000427.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000428.fit.tar.gz  000428.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000429.fit.tar.gz  000429.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000430.fit.tar.gz  000430.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000431.fit.tar.gz  000431.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000432.fit.tar.gz  000432.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000433.fit.tar.gz  000433.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000434.fit.tar.gz  000434.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000435.fit.tar.gz  000435.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000436.fit.tar.gz  000436.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000437.fit.tar.gz  000437.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000438.fit.tar.gz  000438.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000439.fit.tar.gz  000439.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000440.fit.tar.gz  000440.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000441.fit.tar.gz  000441.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000442.fit.tar.gz  000442.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000443.fit.tar.gz  000443.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000444.fit.tar.gz  000444.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000445.fit.tar.gz  000445.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000446.fit.tar.gz  000446.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000447.fit.tar.gz  000447.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000448.fit.tar.gz  000448.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000449.fit.tar.gz  000449.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000450.fit.tar.gz  000450.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000451.fit.tar.gz  000451.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000452.fit.tar.gz  000452.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000453.fit.tar.gz  000453.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000454.fit.tar.gz  000454.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000455.fit.tar.gz  000455.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000456.fit.tar.gz  000456.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000457.fit.tar.gz  000457.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000458.fit.tar.gz  000458.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000459.fit.tar.gz  000459.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000460.fit.tar.gz  000460.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000461.fit.tar.gz  000461.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000462.fit.tar.gz  000462.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000463.fit.tar.gz  000463.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000464.fit.tar.gz  000464.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000465.fit.tar.gz  000465.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000466.fit.tar.gz  000466.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000467.fit.tar.gz  000467.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000468.fit.tar.gz  000468.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000469.fit.tar.gz  000469.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000470.fit.tar.gz  000470.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000471.fit.tar.gz  000471.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000472.fit.tar.gz  000472.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000473.fit.tar.gz  000473.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000474.fit.tar.gz  000474.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000475.fit.tar.gz  000475.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000476.fit.tar.gz  000476.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000477.fit.tar.gz  000477.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000478.fit.tar.gz  000478.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000479.fit.tar.gz  000479.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000480.fit.tar.gz  000480.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000481.fit.tar.gz  000481.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000482.fit.tar.gz  000482.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000483.fit.tar.gz  000483.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000484.fit.tar.gz  000484.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000485.fit.tar.gz  000485.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000486.fit.tar.gz  000486.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000487.fit.tar.gz  000487.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000488.fit.tar.gz  000488.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000489.fit.tar.gz  000489.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000490.fit.tar.gz  000490.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000491.fit.tar.gz  000491.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000492.fit.tar.gz  000492.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000493.fit.tar.gz  000493.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000494.fit.tar.gz  000494.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000495.fit.tar.gz  000495.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000496.fit.tar.gz  000496.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000497.fit.tar.gz  000497.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000498.fit.tar.gz  000498.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000499.fit.tar.gz  000499.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000500.fit.tar.gz  000500.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000501.fit.tar.gz  000501.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000502.fit.tar.gz  000502.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000503.fit.tar.gz  000503.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000504.fit.tar.gz  000504.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000505.fit.tar.gz  000505.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000506.fit.tar.gz  000506.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000507.fit.tar.gz  000507.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000508.fit.tar.gz  000508.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000509.fit.tar.gz  000509.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000510.fit.tar.gz  000510.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000511.fit.tar.gz  000511.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000512.fit.tar.gz  000512.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000513.fit.tar.gz  000513.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000514.fit.tar.gz  000514.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000515.fit.tar.gz  000515.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000516.fit.tar.gz  000516.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000517.fit.tar.gz  000517.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000518.fit.tar.gz  000518.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000519.fit.tar.gz  000519.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000520.fit.tar.gz  000520.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000521.fit.tar.gz  000521.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000522.fit.tar.gz  000522.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000523.fit.tar.gz  000523.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000524.fit.tar.gz  000524.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000525.fit.tar.gz  000525.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000526.fit.tar.gz  000526.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000527.fit.tar.gz  000527.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000528.fit.tar.gz  000528.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000529.fit.tar.gz  000529.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000530.fit.tar.gz  000530.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000531.fit.tar.gz  000531.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000532.fit.tar.gz  000532.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000533.fit.tar.gz  000533.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000534.fit.tar.gz  000534.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000535.fit.tar.gz  000535.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000536.fit.tar.gz  000536.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000537.fit.tar.gz  000537.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000538.fit.tar.gz  000538.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000539.fit.tar.gz  000539.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000540.fit.tar.gz  000540.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000541.fit.tar.gz  000541.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000542.fit.tar.gz  000542.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000543.fit.tar.gz  000543.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000544.fit.tar.gz  000544.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000545.fit.tar.gz  000545.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000546.fit.tar.gz  000546.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000547.fit.tar.gz  000547.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000548.fit.tar.gz  000548.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000549.fit.tar.gz  000549.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000550.fit.tar.gz  000550.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000551.fit.tar.gz  000551.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000552.fit.tar.gz  000552.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000553.fit.tar.gz  000553.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000554.fit.tar.gz  000554.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000555.fit.tar.gz  000555.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000556.fit.tar.gz  000556.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000557.fit.tar.gz  000557.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000558.fit.tar.gz  000558.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000559.fit.tar.gz  000559.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000560.fit.tar.gz  000560.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000561.fit.tar.gz  000561.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000562.fit.tar.gz  000562.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000563.fit.tar.gz  000563.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000564.fit.tar.gz  000564.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000565.fit.tar.gz  000565.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000566.fit.tar.gz  000566.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000567.fit.tar.gz  000567.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000568.fit.tar.gz  000568.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000569.fit.tar.gz  000569.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000570.fit.tar.gz  000570.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000571.fit.tar.gz  000571.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000572.fit.tar.gz  000572.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000573.fit.tar.gz  000573.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000574.fit.tar.gz  000574.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000575.fit.tar.gz  000575.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000576.fit.tar.gz  000576.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000577.fit.tar.gz  000577.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000578.fit.tar.gz  000578.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000579.fit.tar.gz  000579.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000580.fit.tar.gz  000580.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000581.fit.tar.gz  000581.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000582.fit.tar.gz  000582.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000583.fit.tar.gz  000583.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000584.fit.tar.gz  000584.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000585.fit.tar.gz  000585.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000586.fit.tar.gz  000586.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000587.fit.tar.gz  000587.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000588.fit.tar.gz  000588.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000589.fit.tar.gz  000589.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000590.fit.tar.gz  000590.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000591.fit.tar.gz  000591.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000592.fit.tar.gz  000592.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000593.fit.tar.gz  000593.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000594.fit.tar.gz  000594.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000595.fit.tar.gz  000595.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000596.fit.tar.gz  000596.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000597.fit.tar.gz  000597.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000598.fit.tar.gz  000598.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000599.fit.tar.gz  000599.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000600.fit.tar.gz  000600.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000601.fit.tar.gz  000601.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000602.fit.tar.gz  000602.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000603.fit.tar.gz  000603.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000604.fit.tar.gz  000604.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000605.fit.tar.gz  000605.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000606.fit.tar.gz  000606.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000607.fit.tar.gz  000607.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000608.fit.tar.gz  000608.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000609.fit.tar.gz  000609.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000610.fit.tar.gz  000610.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000611.fit.tar.gz  000611.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000612.fit.tar.gz  000612.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000613.fit.tar.gz  000613.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000614.fit.tar.gz  000614.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000615.fit.tar.gz  000615.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000616.fit.tar.gz  000616.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000617.fit.tar.gz  000617.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000618.fit.tar.gz  000618.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000619.fit.tar.gz  000619.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000620.fit.tar.gz  000620.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000621.fit.tar.gz  000621.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000622.fit.tar.gz  000622.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000623.fit.tar.gz  000623.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000624.fit.tar.gz  000624.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000625.fit.tar.gz  000625.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000626.fit.tar.gz  000626.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000627.fit.tar.gz  000627.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000628.fit.tar.gz  000628.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000629.fit.tar.gz  000629.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000630.fit.tar.gz  000630.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000631.fit.tar.gz  000631.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000632.fit.tar.gz  000632.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000633.fit.tar.gz  000633.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000634.fit.tar.gz  000634.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000635.fit.tar.gz  000635.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000636.fit.tar.gz  000636.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000637.fit.tar.gz  000637.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000638.fit.tar.gz  000638.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000639.fit.tar.gz  000639.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000640.fit.tar.gz  000640.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000641.fit.tar.gz  000641.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000642.fit.tar.gz  000642.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000643.fit.tar.gz  000643.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000644.fit.tar.gz  000644.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000645.fit.tar.gz  000645.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000646.fit.tar.gz  000646.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000647.fit.tar.gz  000647.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000648.fit.tar.gz  000648.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000649.fit.tar.gz  000649.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000650.fit.tar.gz  000650.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000651.fit.tar.gz  000651.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000652.fit.tar.gz  000652.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000653.fit.tar.gz  000653.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000654.fit.tar.gz  000654.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000655.fit.tar.gz  000655.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000656.fit.tar.gz  000656.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000657.fit.tar.gz  000657.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000658.fit.tar.gz  000658.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000659.fit.tar.gz  000659.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000660.fit.tar.gz  000660.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000661.fit.tar.gz  000661.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000662.fit.tar.gz  000662.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000663.fit.tar.gz  000663.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000664.fit.tar.gz  000664.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000665.fit.tar.gz  000665.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000666.fit.tar.gz  000666.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000667.fit.tar.gz  000667.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000668.fit.tar.gz  000668.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000669.fit.tar.gz  000669.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000670.fit.tar.gz  000670.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000671.fit.tar.gz  000671.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000672.fit.tar.gz  000672.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000673.fit.tar.gz  000673.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000674.fit.tar.gz  000674.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000675.fit.tar.gz  000675.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000676.fit.tar.gz  000676.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000677.fit.tar.gz  000677.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000678.fit.tar.gz  000678.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000679.fit.tar.gz  000679.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000680.fit.tar.gz  000680.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000681.fit.tar.gz  000681.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000682.fit.tar.gz  000682.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000683.fit.tar.gz  000683.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000684.fit.tar.gz  000684.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000685.fit.tar.gz  000685.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000686.fit.tar.gz  000686.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000687.fit.tar.gz  000687.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000688.fit.tar.gz  000688.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000689.fit.tar.gz  000689.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000690.fit.tar.gz  000690.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000691.fit.tar.gz  000691.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000692.fit.tar.gz  000692.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000693.fit.tar.gz  000693.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000694.fit.tar.gz  000694.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000695.fit.tar.gz  000695.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000696.fit.tar.gz  000696.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000697.fit.tar.gz  000697.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000698.fit.tar.gz  000698.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000699.fit.tar.gz  000699.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000700.fit.tar.gz  000700.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000701.fit.tar.gz  000701.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000702.fit.tar.gz  000702.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000703.fit.tar.gz  000703.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000704.fit.tar.gz  000704.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000705.fit.tar.gz  000705.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000706.fit.tar.gz  000706.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000707.fit.tar.gz  000707.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000708.fit.tar.gz  000708.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000709.fit.tar.gz  000709.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000710.fit.tar.gz  000710.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000711.fit.tar.gz  000711.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000712.fit.tar.gz  000712.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000713.fit.tar.gz  000713.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000714.fit.tar.gz  000714.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000715.fit.tar.gz  000715.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000716.fit.tar.gz  000716.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000717.fit.tar.gz  000717.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000718.fit.tar.gz  000718.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000719.fit.tar.gz  000719.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000720.fit.tar.gz  000720.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000721.fit.tar.gz  000721.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000722.fit.tar.gz  000722.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000723.fit.tar.gz  000723.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000724.fit.tar.gz  000724.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000725.fit.tar.gz  000725.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000726.fit.tar.gz  000726.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000727.fit.tar.gz  000727.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000728.fit.tar.gz  000728.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000729.fit.tar.gz  000729.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000730.fit.tar.gz  000730.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000731.fit.tar.gz  000731.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000732.fit.tar.gz  000732.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000733.fit.tar.gz  000733.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000734.fit.tar.gz  000734.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000735.fit.tar.gz  000735.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000736.fit.tar.gz  000736.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000737.fit.tar.gz  000737.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000738.fit.tar.gz  000738.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000739.fit.tar.gz  000739.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000740.fit.tar.gz  000740.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000741.fit.tar.gz  000741.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000742.fit.tar.gz  000742.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000743.fit.tar.gz  000743.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000744.fit.tar.gz  000744.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000745.fit.tar.gz  000745.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000746.fit.tar.gz  000746.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000747.fit.tar.gz  000747.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000748.fit.tar.gz  000748.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000749.fit.tar.gz  000749.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000750.fit.tar.gz  000750.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000751.fit.tar.gz  000751.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000752.fit.tar.gz  000752.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000753.fit.tar.gz  000753.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000754.fit.tar.gz  000754.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000755.fit.tar.gz  000755.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000756.fit.tar.gz  000756.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000757.fit.tar.gz  000757.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000758.fit.tar.gz  000758.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000759.fit.tar.gz  000759.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000760.fit.tar.gz  000760.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000761.fit.tar.gz  000761.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000762.fit.tar.gz  000762.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000763.fit.tar.gz  000763.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000764.fit.tar.gz  000764.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000765.fit.tar.gz  000765.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000766.fit.tar.gz  000766.fit
tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_galaxy_catalogue/000767.fit.tar.gz  000767.fit
