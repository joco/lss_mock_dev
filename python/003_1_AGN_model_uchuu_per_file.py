"""
HAM procedure for Uchuu between tabulated AGN files and the light cone
for jj in n.arange(15,650):
    print('python 003_1_AGN_model_uchuu_per_file.py '+str(jj))


"""
import time
t0=time.time()
import numpy as n
import os, sys, glob
import h5py
from astropy.table import Table, Column, vstack
from astropy.coordinates import SkyCoord
import healpy
from scipy.interpolate import interp1d
import astropy.io.fits as fits
from scipy.special import erf
from scipy.stats import norm
import scipy
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u

import extinction
import astropy.constants as cc
from scipy.interpolate import interp2d
from scipy.interpolate import NearestNDInterpolator
from scipy.interpolate import interp1d


from dustmaps.planck import PlanckQuery
planck = PlanckQuery()

#path_2_NH_map = '/home/users/dae/comparat/h1_maps/H14PI/asu.fit'
path_2_NH_map = '/ptmp/joco/h1_maps/H14PI/asu.fit'
NH_DATA2 = fits.open(path_2_NH_map)[1].data
nh_val = NH_DATA2['NHI']

cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
h = 0.6774
cosmo = cosmoUNIT

z_array = n.arange(0, 15, 0.001)
dc_to_z = interp1d(cosmo.comoving_distance(z_array), z_array)

d_L = cosmo.luminosity_distance(z_array)
dl_cmXX = (d_L.to(u.cm)).value
dL_interpolation = interp1d(z_array, dl_cmXX)

l_box = 2000.0
L_box = l_box/h
z_dir = sys.argv[1]
mean_z = int(z_dir[1])+int(z_dir[3:])/100.
scatter_0 = float(sys.argv[2])
str_scatter_0 = sys.argv[2]
str_fsat = sys.argv[3]
f_sat = float(sys.argv[3]) / 100.
print(sys.argv)
print(scatter_0, f_sat)

env = 'UCHUU'
p_2_catalogues = n.array( glob.glob( os.path.join(os.environ[env], 'GPX8', z_dir, 'replication_*_*_*', 'glist.fits') ) )
p_2_catalogues.sort()
print(len(p_2_catalogues), 'catalogues to loop over')

for p_2_catalogue in p_2_catalogues:
    print( 'starts', p_2_catalogue )
    p_2_catalogue_out = os.path.join( os.path.dirname(p_2_catalogue), 'AGN_list_sigma_'+str_scatter_0+'_fsat_'+str_fsat+'.fits')
    snap_name = p_2_catalogue.split('/')[6]
    p_2_area_t = os.path.join( os.environ[env], 'area_per_replica_'+snap_name+'.fits')
    print(p_2_area_t)
    t_area = Table.read( p_2_area_t )
    replication_dir = p_2_catalogue.split('/')[7]
    ix = float(replication_dir.split('_')[1])
    iy = float(replication_dir.split('_')[2])
    iz = float(replication_dir.split('_')[3])
    #print(ix, iy, iz)
    selection_area = ( t_area['jx'] == ix ) & ( t_area['jy'] == iy ) & ( t_area['jz'] == iz )
    print(t_area[selection_area])
    sky_frac_Dmin = t_area['area_DC_min'][selection_area].sum() / t_area['area_DC_min'].sum()
    sky_frac_Dmax = t_area['area_DC_max'][selection_area].sum() / t_area['area_DC_max'].sum()
    sky_frac = sky_frac_Dmax # ( sky_frac_Dmin + sky_frac_Dmax ) / 2.
    print('sky fraction', sky_frac, sky_frac_Dmin, sky_frac_Dmax)#, len(t_area['area_DC_min'][selection_area])==1)
    ##sky_frac = 1. / 8.
    if sky_frac==0.:
        print('no area')
        continue
    tt = fits.open(p_2_catalogue)
    ZZ = tt[1].data['redshift_R']
    z_min = n.min(ZZ)
    z_max = n.max(ZZ)
    z_mean = (z_max + z_min)/2.
    print(z_min,'<z<',z_max)
    ##
    ##
    ## OPENS AGN FILE
    ##
    ##
    #log10_FX_lim = -16
    env = "UCHUU"

    # z bins in which AGN are tabulated
    DZ=0.01
    z_bins = n.arange(0.0, 6., DZ)
    #z_ups = z_bins+DZ
    #jj = n.argmin( abs(z_bins - z_mean))
    #print(jj)
    j0 = n.searchsorted(z_bins, z_min, side='right')-1
    j1 = n.searchsorted(z_bins, z_max, side='right')-1

    AGN_ALL = []
    for jj in n.arange(j0, j1+1, 1):
        p_2_AGN = os.path.join( os.environ[env], 'AGN_LX_tables', 'LX_table_' +str(n.round(z_bins,2)[jj])+ '.fits')
        AGN = Table.read(p_2_AGN)
        print(p_2_AGN, len(AGN))
        N_AGN_tabulated = len(AGN)
        # only works for the first replication d_comoving 2 Gpc/h !
        z_sel=(AGN['z']>=z_min)&(AGN['z']<=z_max) & (n.random.random(size=N_AGN_tabulated)<sky_frac)
        AGN_ALL.append( AGN[z_sel] )

    AGN = vstack(( AGN_ALL ))
    AGN = AGN[n.argsort(AGN['LX_hard'])]
    N_AGN_tabulated = len(AGN)
    AGN_ALL = 0
    ##
    ##
    ## OPENS Galaxy light cone
    ##
    ##

    #0.25 -1.02 -0.79 -0.56
    #0.75 -0.72 -0.36 0.0
    #1.25 -0.86 -0.43 0.0
    #1.75 -0.58 -0.29 0.0
    #2.25 -0.72 -0.36 0.0
    #2.75 -0.86 -0.43 0.0
    #3.5  -0.86 -0.43 0.0

    #f_duty = interp1d(n.array([0., 0.75, 2., 3.5, 10.1]), n.array([0.1, 0.2, 0.3, 0.3, 0.3]))
    f_duty_1 = interp1d(n.array([0.   , 0.25,  0.75, 1.75, 10.1]) ,
                    10**n.array([-1.0, -0.90, -0.75, -0.6, -0.40]) )
    S_trans  = 1.5
    LX_LIM_DC = 40.
    Zval = z_min
    DC_MAX = f_duty_1(z_min)
    x_M = n.arange(6, 13.5, 0.01)
    # add exponential cut-ff below 1e9.5
    #def f_DC_mod(MS):
        #return DC_MAX * (0.5 + 0.5 * erf((MS - (LX_LIM_DC-(30.4+Zval/5.))) / S_trans)) #* n.e**(2*(MS-9))
    def f_DC_mod(MS):
        return DC_MAX * (0.5 + 0.5 * erf((MS - (LX_LIM_DC-(30.4+Zval/5.))) / S_trans)) * ( 0.5 + 0.5 * erf((MS - 9.25) / 0.15) )
        #return DC_MAX * (0.5 + 0.5 * erf((MS - (LX_LIM_DC-(30.4+Zval/5.))) / S_trans)) * ( 0.5 + 0.5 * erf((MS - 9.5) / 0.05) )
    f_duty = interp1d( x_M, f_DC_mod(x_M) )
    #f_duty_E = interp1d( x_M, f_DC_mod_E(x_M) )
    #print(f_duty([8, 8.5, 9, 9.5, 10]))
    print('duty cycle vs Ms: 8,12.5 by 0.5', f_duty([8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 12.5]))

    def get_z_mass_id(p_2_lcfile):
        print(p_2_lcfile)
        tt = fits.open(p_2_lcfile)
        # DOWNSAMPLES to the f_sat
        ZZ = tt[1].data['redshift_R']
        MM = n.log10(tt[1].data['sm'])
        HALO_pid = tt[1].data['upid']
        cen = (HALO_pid==-1)
        sat = (cen==False)
        N_galaxies = len(HALO_pid)
        N_galaxies_cen = len(HALO_pid[cen])
        N_galaxies_sat = len(HALO_pid[sat])
        native_f_sat = N_galaxies_sat*1./N_galaxies_cen
        print('native N, cen, sat, f_sat', N_galaxies, N_galaxies_cen, N_galaxies_sat, native_f_sat)
        rds = n.random.random(N_galaxies)
        if f_sat > native_f_sat:
            # downsample centrals
            print('downsamples centrals')
            N_cen_goal = N_galaxies_sat / f_sat
            print('N cen goal', N_cen_goal)
            sel_cen = (rds < N_cen_goal / N_galaxies_cen)
            all_cen = (cen)&(sel_cen)
            keep = (sat)|(all_cen)
            N_kept1 = len(keep.nonzero()[0])
            #print(N_kept1)
        if f_sat <= native_f_sat:
            # downsample sat
            print('downsamples sat')
            N_sat_goal = N_galaxies_cen * f_sat
            print('N sat goal', N_sat_goal)
            sel_sat = (rds<N_sat_goal/N_galaxies_sat)
            all_sat = (sat)&(sel_sat)
            keep = (cen)|(all_sat)
            N_kept1 = len(keep.nonzero()[0])
            #print(N_kept1)
        # APPLY DUTY CYCLE
        print('renormalizing DC by', N_galaxies/N_kept1)
        f_duty_realization = f_duty(MM)*N_galaxies/N_kept1
        active = (n.random.random(size=len(ZZ)) <= f_duty_realization)
        z_range = (ZZ >= z_min) & (ZZ < z_max) & (tt[1].data['nH']>0) & (tt[1].data['ebv']>=0)
        # outputs
        IDS = n.arange(len(ZZ))      [active & keep & z_range]
        ZZ = tt[1].data['redshift_R'][active & keep & z_range]
        MM = n.log10(tt[1].data['sm'][active & keep & z_range])
        gNH = tt[1].data['nH']       [active & keep & z_range]
        gEBV = tt[1].data['ebv']     [active & keep & z_range]
        print('N AGN', len(IDS))
        return IDS, ZZ, MM, gNH, gEBV

    ids, z, logm, galactic_NH, galactic_ebv = get_z_mass_id(p_2_catalogue)
    mass_sort_idx = n.argsort(logm)
    N_active = len(z)
    print('step0, N AGN_tabulated', N_AGN_tabulated)
    print('step0, N active', N_active)

    # match the exact number of tabulated AGN and active galaxies

    if N_AGN_tabulated < N_active  :
        # too many active galaxies (box resolution is higher than AGN tabulated
        # just populate the higher stellar mass ones in the HAM
        id_selection = mass_sort_idx [ - N_AGN_tabulated : ]
        logm = logm [id_selection]
        z    = z    [id_selection]
        galactic_NH = galactic_NH[id_selection]
        galactic_ebv = galactic_ebv[id_selection]
        ids  = ids  [id_selection]

    if N_AGN_tabulated > N_active  :
        # too many active galaxies (box resolution is higher than AGN tabulated
        # just populate the higher stellar mass ones in the HAM
        AGN = AGN[ - N_active :]

    N_active = len(z)
    print('step1, N active', N_active)
    #print(N_active, len(AGN))
    n_agn = len(z)
    dl_cm = dL_interpolation(z)
    #
    #
    # HAM procedure
    #
    #
    rds = norm.rvs(loc=0, scale=scatter_0, size=len(logm))
    AGN['scatter_LX_Ms'] = rds
    M_scatt = logm + AGN['scatter_LX_Ms']
    ids_M_scatt = n.argsort(M_scatt)
    # output numbers
    lx = n.zeros_like(logm)
    lx[ids_M_scatt] = AGN['LX_hard']
    lsar = n.zeros_like(lx)
    lsar[ids_M_scatt] = AGN['LX_hard'] - logm[ids_M_scatt]

    # ===============================
    # Obscured fractions
    # ===============================
    # model from equations 4-11, 12-15 of Comparat et al. 2019

    # too many CTK at high luminosity
    # Eq. 4
    #def f_thick(LXhard, z): return 0.30
    def thick_LL(z, lx0 = 41.5): return lx0 + n.arctan(z*5)*1.5
    def f_thick(LXhard, z): return 0.30 * (0.5 + 0.5 * erf((thick_LL(z) - LXhard) / 0.25))

    # too many absorbed ones
    # Eq. 7
    def f_2(LXhard, z): return 0.9 * (41 / LXhard)**0.5

    # fiducial
    # Eq. 8
    def f_1(LXhard, z): return f_thick(LXhard, z) + 0.01 + erf(z / 4.) * 0.3

    # Eq. 10
    def LL(z, lx0 = 43.2): return lx0 + erf(z) * 1.2

    # Eq. 5,6
    def fraction_ricci(LXhard, z, width = 0.6): return f_1(LXhard,z) + (f_2(LXhard, z) - f_1(LXhard,z)) * (0.5 + 0.5 * erf((LL(z) - LXhard) / width))

    # initializes logNH
    logNH = n.zeros(n_agn)

    # obscuration, after the equations above
    randomNH = n.random.rand(n_agn)

    # unobscured 20-22
    #frac_thin = fraction_ricci(lsar, z)
    frac_thin = fraction_ricci(lx, z)
    thinest = (randomNH >= frac_thin)

    # thick obscuration, 24-26
    thick = (randomNH < f_thick(lx, z))
    #thick = (randomNH < thick_fraction)

    # obscured 22-24
    obscured = (thinest == False) & (thick == False)

    # assigns logNH values randomly :
    logNH[thick] = n.random.uniform(24, 26, len(logNH[thick]))
    logNH[obscured] = n.random.uniform(22, 24, len(logNH[obscured]))
    logNH[thinest] = n.random.uniform(20, 22, len(logNH[thinest]))

    # ===============================
    # Assigns flux
    # ===============================

    NHS = n.arange(20, 26 + 0.05, 0.4)
    # hard X-ray 2-10 keV rest-frame ==>> 2-10 obs frame

    path_2_RF_obs_hard = os.path.join(
        os.environ['GIT_AGN_MOCK'],
        "data",
        "xray_k_correction",
        "v3_fraction_observed_A15_RF_hard_Obs_hard_fscat_002.txt")

    obscuration_z_grid, obscuration_nh_grid, obscuration_fraction_obs_erosita = n.loadtxt(
        path_2_RF_obs_hard, unpack=True)
    obscuration_itp_H_H = interp2d(
        obscuration_z_grid,
        obscuration_nh_grid,
        obscuration_fraction_obs_erosita)

    percent_observed_itp = interp1d(
        n.hstack((20 - 0.1, NHS, 26 + 0.1)),
        n.hstack((
            obscuration_itp_H_H(z_mean, 20.)[0],
            n.array([obscuration_itp_H_H(z_i, logNH_i)[0] for z_i, logNH_i in zip(z_mean * n.ones_like(NHS), NHS)]),
            obscuration_itp_H_H(z_mean, 26.)[0])))
    percent_observed_H_H = percent_observed_itp(logNH)

    lx_obs_frame_2_10 = lx + n.log10(percent_observed_H_H)
    fx_2_10 = 10**(lx_obs_frame_2_10) / (4 * n.pi * (dl_cm)**2.) # / h**3
    #print('fx_2_10', fx_2_10, time.time() - t0)
    #print('lx_obs_frame_2_10', lx_obs_frame_2_10, time.time() - t0)

    # obs X-ray 2-10 keV ==>> obs 0.5-2
    # v3_fraction_observed_A15_RF_hard_Obs_soft_fscat_
    # path_2_hard_RF_obs_soft
    # link to X-ray K-correction and attenuation curves
    path_2_hard_RF_obs_soft = os.path.join(
        os.environ['GIT_AGN_MOCK'],
        "data",
        "xray_k_correction",
        "v3_fraction_observed_A15_RF_hard_Obs_soft_fscat_002.txt")

    obscuration_z_grid, obscuration_nh_grid, obscuration_fraction_obs_erosita = n.loadtxt(path_2_hard_RF_obs_soft, unpack=True)
    obscuration_itp_H_S = interp2d(
        obscuration_z_grid,
        obscuration_nh_grid,
        obscuration_fraction_obs_erosita)

    percent_observed_itp = interp1d(
        n.hstack((20 - 0.1, NHS, 26 + 0.1)),
        n.hstack((
            obscuration_itp_H_S(z_mean, 20.)[0],
            n.array([obscuration_itp_H_S(z_i, logNH_i)[0] for z_i, logNH_i in zip(z_mean * n.ones_like(NHS), NHS)]),
            obscuration_itp_H_S(z_mean, 26.)[0])))

    percent_observed_H_S = percent_observed_itp(logNH)


    path_2_hard_RF_obs_soft_3D = n.array( glob.glob( os.path.join(
        os.environ['GIT_AGN_MOCK'],
        "data",
        "xray_k_correction",
        "v3_fraction_observed_A15_RF_hard_Obs_soft_fscat_002_GALnH_*.txt") ) )
    path_2_hard_RF_obs_soft_3D.sort()
    # create the 3D interpolation
    grid_z, grid_nh, grid_galnh, transmission = [], [], [], []
    for el in path_2_hard_RF_obs_soft_3D:
        grid_z_i, grid_nh_i, grid_galnh_i, transmission_i = n.loadtxt(el, unpack=True)
        grid_z      .append(grid_z_i      )
        grid_nh     .append(grid_nh_i     )
        grid_galnh  .append(grid_galnh_i  )
        transmission.append(transmission_i)

    points = n.transpose([
        n.hstack( grid_z ),
        n.hstack( grid_nh ),
        n.hstack( grid_galnh )])

    values = n.hstack( transmission)

    ITP3d = NearestNDInterpolator(points, values)

    percent_observed_H_S_galNH = ITP3d(z, logNH, n.log10(galactic_NH))

    lx_05_20_unATT = lx + n.log10(percent_observed_H_S)
    fx_05_20_log_unATT = lx_05_20_unATT - n.log10(4 * n.pi) - 2*n.log10(dl_cm)
    lx_05_20 = lx + n.log10(percent_observed_H_S_galNH)
    fx_05_20_log = lx_05_20 - n.log10(4 * n.pi) - 2*n.log10(dl_cm)
    #fx_05_20 = 10**lx_05_20 / (4 * n.pi * (dl_cm)**2.)
    #fx_05_20 = fx_2_10 * percent_observed_H_S
    #lx_05_20 = fx_05_20 * (4 * n.pi * (dl_cm)**2.) # / h**3

    # Adds type 11, 12, 21, 22
    # Follows Merloni et al. 2014
    # equation 16 of Comparat et al. 2019

    def fraction_22p21_merloni(lx): return (
        0.5 + 0.5 * erf((-lx + 44.) / 0.9)) * 0.69 + 0.26

    # LF in the mock, starting parameters
    dlogf = 0.05
    Lbin_min = 36
    fbins = n.arange(Lbin_min, 48, dlogf)
    xf = fbins[:-1] + dlogf / 2.

    def compute_agn_type(z, lx, logNH, fbins=fbins, n_agn=n_agn):
        """
        Assigns a type to an AGN population

        parameters:
        - z: redshift
        - lx: hard X-ray luminosity (log10)
        - logNH: nH value (log10)

        return: array of AGN types
        """
        # boundary between the 22 and the 21 populations
        limit = fraction_22p21_merloni((fbins[1:] + fbins[:-1]) * 0.5)
        # selection per obscuration intensity
        nh_21 = (logNH <= 22.)
        nh_23 = (logNH > 22.)  # &(logNH<=26.)
        # initiate columns to compute
        opt_type = n.zeros(n_agn).astype('int')
        rd = n.random.rand(n_agn)
        # compute histograms of LX for different obscurations
        nall = n.histogram(lx, fbins)[0]       # all
        nth = n.histogram(lx[nh_23], fbins)[0]  # thin
        nun = n.histogram(lx[nh_21], fbins)[0]  # unobscured
        fr_thk = nth * 1. / nall  # fraction of obscured
        fr_un = nun * 1. / nall  # fraction of unobscured
        # first get the type 12: NH absorption but optically unobscured
        # to be chosen in obscured population
        n_per_bin_12 = (fr_thk - limit) * nall
        sel_12 = (n.ones(len(z)) == 0)
        for bin_low, bin_high, num_needed, nn_un in zip(
                fbins[:-1], fbins[1:], n_per_bin_12.astype('int'), nth):
            if num_needed > 0 and nn_un > 0:
                frac_needed = num_needed * 1. / nn_un
                sel_12 = (sel_12) | (
                    (lx > bin_low) & (
                        lx < bin_high) & (nh_23) & (
                        rd < frac_needed))
        t_12 = (nh_23) & (sel_12)
        # second the types 21
        # to be chosen in nun
        n_per_bin_21 = (-fr_thk + limit) * nall
        sel_21 = (n.ones(len(z)) == 0)
        for bin_low, bin_high, num_needed, nn_un in zip(
                fbins[:-1], fbins[1:], n_per_bin_21.astype('int'), nun):
            if num_needed > 0 and nn_un > 0:
                frac_needed = num_needed * 1. / nn_un
                sel_21 = (sel_21) | (
                    (lx > bin_low) & (
                        lx < bin_high) & (nh_21) & (
                        rd < frac_needed))
        t_21 = (nh_21) & (sel_21)
        # finally the types 11 and 22
        t_11 = (nh_21) & (t_21 == False)
        t_22 = (nh_23) & (t_12 == False)
        opt_type[t_22] = 22
        opt_type[t_12] = 12
        opt_type[t_11] = 11
        opt_type[t_21] = 21
        return opt_type


    opt_type = compute_agn_type(z, lx, logNH)
    #print('opt_type', opt_type, time.time() - t0)

    # observed r-band magnitude from X-ray

    def r_mean(log_FX0520): return -2. * log_FX0520 - 7.

    def scatter_t1(n_agn_int): return norm.rvs(loc=0.0, scale=1.0, size=n_agn_int)

    random_number = n.random.rand(n_agn)
    empirical_mag_r = r_mean(fx_05_20_log_unATT) + scatter_t1(int(n_agn))
    #print('empirical_mag_r', empirical_mag_r, time.time() - t0)

    # ===============================
    # EXTINCTION
    # ===============================
    # x ray extinction from our Galaxy
    #NH_DATA = n.loadtxt(path_2_NH_attenuation, unpack=True)
    #nh_law = interp1d(
        #n.hstack(
            #(-10.**25, 10**n.hstack(
                #(10., NH_DATA[0], 25)))), n.hstack(
                    #(1., 1., 1. / NH_DATA[1], 0.00001)))

    #attenuation = nh_law(galactic_NH)
    #agn_rxay_flux_05_20_observed = 10**fx_05_20_log * attenuation
    #print('agn_rxay_flux_05_20_observed',agn_rxay_flux_05_20_observed,time.time() - t0)

    # optical extinction, Fitzpatrick 99
    ebv_values = n.hstack((n.arange(0., 5., 0.01), 10**n.arange(1, 4, 0.1)))
    ext_values = n.array([extinction.fitzpatrick99(
        n.array([6231.]), 3.1 * EBV, r_v=3.1, unit='aa')[0] for EBV in ebv_values])
    ext_interp = interp1d(ebv_values, ext_values)
    agn_rmag_observed = empirical_mag_r + ext_interp(galactic_ebv)
    #print('agn_rmag_observed', agn_rmag_observed, time.time() - t0)

    # ===============================
    # Writing results
    # ===============================
    f1 = Table()
    f1['ID_glist'] = ids.astype('int')
    f1['redshift_R'] = z
    f1['galaxy_SMHMR_mass'] = logm
    f1['LX_hard'] = lx
    f1['LX_soft'] = lx_05_20
    f1['FX_soft'] = 10**fx_05_20_log_unATT
    f1['FX_soft_attenuated'] = 10**fx_05_20_log
    f1['FX_hard'] = fx_2_10
    f1['logNH'] = logNH
    f1['agn_type'] = opt_type
    f1['random_r_AB'] = random_number
    f1['SDSS_r_AB'] = empirical_mag_r
    f1['SDSS_r_AB_attenuated'] = agn_rmag_observed
    f1.write(p_2_catalogue_out, overwrite = True)
    print(p_2_catalogue_out, 'written', time.time()-t0)
