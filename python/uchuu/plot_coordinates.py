import sys, glob
import os
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p
import numpy as n
import astropy.io.fits as fits
import time
from astropy.table import Table, Column

print('Plots coordinate data')
t0 = time.time()

pix_ID = str(0).zfill(4)
sim_dir = os.path.join(os.environ["HOME"], 'data/Uchuu/HPX8/')
file_list = n.array( glob.glob( os.path.join(sim_dir, 'halodir_0??', pix_ID+'.fits.gz') ) )
file_list.sort()

fig_dir = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    'figures',
    'Uchuu',
    'coordinates',
)
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)

for path_2_light_cone in file_list:
	f=Table.read(path_2_light_cone)
	zr = f['redshift_R']
	zs = f['redshift_S']
	#g_lat = f['g_lat']
	#g_lon = f['g_lon']
	#ecl_lat = f['ecl_lat']
	#ecl_lon = f['ecl_lon']
	ra = f['RA']
	dec = f['DEC']
	#dL = f['dL']
	#NH = f['NH']
	#ebv = f['ebv']
	N_obj = len(zr)
	sat = (f['pid']>=0)
	rds = n.random.random(N_obj)
	sel = (rds < 1e5 / N_obj) 
	fig_out = os.path.join(fig_dir, pix_ID + '_redshift_M200c_' + path_2_light_cone.split('/')[-2] + '.png')
	XX = zr[sel]
	YY = n.log10(f['M200c'][sel])
	p.figure(0, (5.5, 5.5))
	p.tight_layout()
	p.plot(XX, YY, 'k,', rasterized=True)
	p.xlabel('redshift')
	p.ylabel('log 10 M200c')
	p.grid()
	p.savefig(fig_out)
	p.clf()
	fig_out = os.path.join(fig_dir, pix_ID + '_redshift_SMHMR_mass_' + path_2_light_cone.split('/')[-2] + '.png')
	XX = zr[sel]
	YY = f['SMHMR_mass'][sel]
	p.figure(0, (5.5, 5.5))
	p.tight_layout()
	p.plot(XX, YY, 'k,', rasterized=True)
	p.xlabel('redshift')
	p.ylabel('SMHMR_mass')
	p.grid()
	p.savefig(fig_out)
	p.clf()
	fig_out = os.path.join(fig_dir, pix_ID + '_redshift_SMHMR_mass_SAT_' + path_2_light_cone.split('/')[-2] + '.png')
	XX_s = zr[(sel) & (sat)]
	XX_c = zr[(sel) & (sat==False)]
	YY_s = f['SMHMR_mass'][(sel) & (sat)]
	YY_c = f['SMHMR_mass'][(sel) & (sat==False)]
	p.figure(0, (5.5, 5.5))
	p.tight_layout()
	p.plot(XX_c, YY_c, 'r,', rasterized=True)
	p.plot(XX_s, YY_s, 'b,', rasterized=True)
	p.xlabel('redshift')
	p.ylabel('SMHMR_mass')
	p.grid()
	p.savefig(fig_out)
	p.clf()
	fig_out = os.path.join(fig_dir, pix_ID + '_redshift_star_formation_rate_' + path_2_light_cone.split('/')[-2] + '.png')
	XX = zr[sel]
	YY = f['star_formation_rate'][sel]
	p.figure(0, (5.5, 5.5))
	p.tight_layout()
	p.plot(XX, YY, 'k,', rasterized=True)
	p.xlabel('redshift')
	p.ylabel('star_formation_rate')
	p.grid()
	p.savefig(fig_out)
	p.clf()
	fig_out = os.path.join(fig_dir, pix_ID + '_redshift_declination_' + path_2_light_cone.split('/')[-2] + '.png')
	XX = zr[sel]
	YY = dec[sel]
	p.figure(0, (5.5, 5.5))
	p.tight_layout()
	p.plot(XX, YY, 'k,', rasterized=True)
	p.xlabel('redshift')
	p.ylabel('Dec.')
	p.grid()
	p.savefig(fig_out)
	p.clf()
	fig_out = os.path.join(fig_dir, pix_ID + '_redshift_RA_' + path_2_light_cone.split('/')[-2] + '.png')
	XX = zr[sel]
	YY = ra[sel]
	p.figure(0, (5.5, 5.5))
	p.tight_layout()
	p.plot(XX, YY, 'k,', rasterized=True)
	p.xlabel('redshift')
	p.ylabel('R.A.')
	p.grid()
	p.savefig(fig_out)
	p.clf()

## redshift distributions
#fig_out = os.path.join(fig_dir, 'zr-minus-zs_hist_' + baseName + '.png')

#dz = zr - zs

#X = n.log10(abs(dz[dz > 0]))

#p.figure(1, (5.5, 5.5))
#p.tight_layout()
#p.hist(X[X > -6], histtype='step', rasterized=True, lw=4)
#p.title(baseName)
#p.xlabel('log10(|redshift R-redshiftS|)')
#p.ylabel('Counts')
#p.grid()
#p.yscale('log')
##cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(E(B-V))$')
##ax = p.gca()
## ax.invert_xaxis()
#p.savefig(fig_out)
#p.clf()

#fig_out = os.path.join(fig_dir, 'zr_hist_' + baseName + '.png')

#X = zr

#p.figure(1, (5.5, 5.5))
#p.tight_layout()
#p.hist(X, histtype='step', rasterized=True, lw=4)
#p.title(baseName)
#p.xlabel('redshift R')
#p.ylabel('Counts')
#p.yscale('log')
#p.grid()
##cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(E(B-V))$')
##ax = p.gca()
## ax.invert_xaxis()
#p.savefig(fig_out)
#p.clf()

#fig_out = os.path.join(fig_dir, 'zs_hist_' + baseName + '.png')

#X = zs

#p.figure(1, (5.5, 5.5))
#p.tight_layout()
#p.hist(X, histtype='step', rasterized=True, lw=4)
#p.title(baseName)
#p.xlabel('redshift S')
#p.ylabel('Counts')
#p.yscale('log')
#p.grid()
##cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(E(B-V))$')
##ax = p.gca()
## ax.invert_xaxis()
#p.savefig(fig_out)
#p.clf()


#fig_out = os.path.join(fig_dir, 'dL_hist_' + baseName + '.png')

#X = n.log10(dL)

#p.figure(1, (5.5, 5.5))
#p.tight_layout()
#p.hist(X, histtype='step', rasterized=True, lw=4)
#p.title(baseName)
#p.xlabel('dL [cm]')
#p.ylabel('Counts')
#p.yscale('log')
#p.grid()
##cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(E(B-V))$')
##ax = p.gca()
## ax.invert_xaxis()
#p.savefig(fig_out)
#p.clf()

## ebv plot

#fig_out = os.path.join(fig_dir, 'ra_dec_ebv_' + baseName + '.png')

#XX = ra[sel]
#YY = dec[sel]
#cc = n.log10(ebv[sel])

#p.figure(0, (8., 5.5))
#p.tight_layout()
#p.scatter(
    #XX,
    #YY,
    #c=cc,
    #s=3,
    #marker='s',
    #edgecolor='face',
    #cmap='Paired',
    #vmin=-3,
    #vmax=2,
    #rasterized=True)
#p.title(baseName)
#p.xlabel('R.A.')
#p.ylabel('Dec.')
#p.yscale('log')
#p.grid()
#cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(E(B-V))$')
#ax = p.gca()
## ax.invert_xaxis()
## p.savefig(fig_out)
#p.clf()

#fig_out = os.path.join(fig_dir, 'g_lat_g_lon_ebv_' + baseName + '.png')

#XX = g_lon[sel]
#YY = g_lat[sel]
#cc = n.log10(ebv[sel])

#p.figure(0, (8., 5.5))
#p.tight_layout()
#p.scatter(
    #XX,
    #YY,
    #c=cc,
    #s=3,
    #marker='s',
    #edgecolor='face',
    #cmap='Paired',
    #vmin=-3,
    #vmax=2,
    #rasterized=True)
#p.title(baseName)
#p.xlabel('galactic lon.')
#p.ylabel('galactic lat.')
#p.grid()
#cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(E(B-V))$')
#p.savefig(fig_out)
#p.clf()

#fig_out = os.path.join(fig_dir, 'ecl_lat_ecl_lon_ebv_' + baseName + '.png')

#XX = ecl_lon[sel]
#YY = ecl_lat[sel]
#cc = n.log10(ebv[sel])

#p.figure(0, (8., 5.5))
#p.tight_layout()
#p.scatter(
    #XX,
    #YY,
    #c=cc,
    #s=3,
    #marker='s',
    #edgecolor='face',
    #cmap='Paired',
    #vmin=-3,
    #vmax=2,
    #rasterized=True)
#p.title(baseName)
#p.xlabel('ecliptic lon.')
#p.ylabel('ecliptic lat.')
#p.grid()
#cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(E(B-V))$')
#p.savefig(fig_out)
#p.clf()

## NH plot

#fig_out = os.path.join(fig_dir, 'ra_dec_nh_' + baseName + '.png')

#XX = ra[sel]
#YY = dec[sel]
#cc = n.log10(NH[sel])

#p.figure(0, (8., 5.5))
#p.tight_layout()
#p.scatter(
    #XX,
    #YY,
    #c=cc,
    #s=3,
    #marker='s',
    #edgecolor='face',
    #cmap='Paired',
    #vmin=19.5,
    #vmax=22,
    #rasterized=True)
#p.title(baseName)
#p.xlabel('R.A.')
#p.ylabel('Dec.')
#p.grid()
#cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(n_H/cm^{-2})$')
#p.savefig(fig_out)
#p.clf()


#fig_out = os.path.join(fig_dir, 'g_lat_g_lon_nh_' + baseName + '.png')

#XX = g_lon[sel]
#YY = g_lat[sel]
#cc = n.log10(NH[sel])

#p.figure(0, (8., 5.5))
#p.tight_layout()
#p.scatter(
    #XX,
    #YY,
    #c=cc,
    #s=3,
    #marker='s',
    #edgecolor='face',
    #cmap='Paired',
    #vmin=19.5,
    #vmax=22,
    #rasterized=True)
#p.title(baseName)
#p.xlabel('galactic lon.')
#p.ylabel('galactic lat.')
#p.grid()
#cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(n_H/cm^{-2})$')
#p.savefig(fig_out)
#p.clf()


#fig_out = os.path.join(fig_dir, 'ecl_lat_ecl_lon_nh_' + baseName + '.png')

#XX = ecl_lon[sel]
#YY = ecl_lat[sel]
#cc = n.log10(NH[sel])

#p.figure(0, (8., 5.5))
#p.tight_layout()
#p.scatter(
    #XX,
    #YY,
    #c=cc,
    #s=3,
    #marker='s',
    #edgecolor='face',
    #cmap='Paired',
    #vmin=19.5,
    #vmax=22,
    #rasterized=True)
#p.title(baseName)
#p.xlabel('ecliptic lon.')
#p.ylabel('ecliptic lat.')
#p.grid()
#cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(n_H/cm^{-2})$')
#p.savefig(fig_out)
#p.clf()
