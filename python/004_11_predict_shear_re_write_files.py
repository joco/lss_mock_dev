"""
Each files has the structure:
— ‘Lproj' # line of sight projection length in kph/h
— ‘redshift’ # redshift of the snapshot 
— 'resolution [kpc/h]’ # width-length of one pixel in kpc/h 
— ‘halos’
    |— ‘a number for each halo’
        |— 'M200c', 'M200m', 'M500c', ‘M500m’ # mass in the grav-only run in 10^10 Msol/h
        |— 'hydro_M200c', 'hydro_M200m', 'hydro_M500c', ‘hydro_M500m’ #  mass in the hydro run in 10^10 Msol/h
        |— ‘has_profile’ # bool flagging if this cluster has a profile
        |— ‘dm_Sigma’ # projected mass in Gravity-only, 10^10 Msol/h (so to get a surface density, divide this by the bin area). Also note, the z~0.78 snapshot has not grav-only maps
        |— ‘hydro_Sigma’ # projected mass in hydro, 10^10 Msol/h


step 1 : attach (resscale) to a DM halo in the light cone
step 2 : predict reduced shear profile (DES, HSC, KIDS, ...)
         https://arxiv.org/pdf/2103.16212.pdf offers a complete list of what is known to date

"""

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt


import h5py

from scipy.special import erf
from astropy.table import Table, Column, vstack, hstack
import sys, os, time
import astropy.units as u
import astropy.io.fits as fits
import numpy as n
from scipy.interpolate import interp1d
from scipy.interpolate import interp2d
print('CREATES WL profiles for eROSITA CLUSTER')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

path_2_WL_3 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_052.h5"
path_2_WL_2 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_072.h5"
path_2_WL_1 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_096.h5"
path_2_WL_0 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_116.h5"
# at redshifts 0.25, 0.48, 0.78, 1.18

f0 = h5py.File(path_2_WL_0, 'r')
print(path_2_WL_0, 'opened')
print(list(f0.keys()))
print('z=',f0['redshift'][...])

f1 = h5py.File(path_2_WL_1, 'r')
print(path_2_WL_1, 'opened')
print(list(f1.keys()))
print('z=',f1['redshift'][...])

f2 = h5py.File(path_2_WL_2, 'r')
print(path_2_WL_2, 'opened')
print(list(f2.keys()))
print('z=',f2['redshift'][...])

f3 = h5py.File(path_2_WL_3, 'r')
print(path_2_WL_3, 'opened')
print(list(f3.keys()))
print('z=',f3['redshift'][...])

# redshift of the snapshot
z0 = f0['redshift'][...]
z1 = f1['redshift'][...]
z2 = f2['redshift'][...]
z3 = f3['redshift'][...]

# line of sight projection length in kph/h
Lproj0 = f0['Lproj'][...]
Lproj1 = f1['Lproj'][...]
Lproj2 = f2['Lproj'][...]
Lproj3 = f3['Lproj'][...]

# width-length of one pixel in kpc/h
R0 = f0["resolution [kpc"]["h]"][...]
R1 = f1["resolution [kpc"]["h]"][...]
R2 = f2["resolution [kpc"]["h]"][...]
R3 = f3["resolution [kpc"]["h]"][...]

# matching to the M500c of the simulation
print('matches between both simulations using M500c (DM only run)')
halo_list0 = list(f0['halos'].keys())
halo_list1 = list(f1['halos'].keys())
halo_list2 = list(f2['halos'].keys())
halo_list3 = list(f3['halos'].keys())

#list(f0['halos'][h_i].keys())
#['GPOS',
 #'M200c',
 #'M200m',
 #'M500c',
 #'M500m',
 #'Rvir',
 #'dm_Sigma',
 #'has_profile',
 #'hydro_GPOS',
 #'hydro_M200c',
 #'hydro_M200m',
 #'hydro_M500c',
 #'hydro_M500m',
 #'hydro_Rvir',
 #'hydro_Sigma',
 #'hydro_name',
 #'match_dist']
def re_write_files(halo_list0, f0, path_2_WL_0):
    # rotation 0
    ts0 = []
    # rotation 1
    ts1 = []
    # rotation 2
    ts2 = []

    for h_i in halo_list0:
        if f0['halos'][h_i]['has_profile'][...]==1:
            tab = Table()
            #M500c_hi_dict_0[ n.round( n.log10(f0['halos'][h_i]['M500c'][...]*1e10 / hz(z0) ), 3) ] = h_i
            #all_m500c.append( n.round( n.log10(f0['halos'][h_i]['M500c'][...]*1e10 / hz(z0) ), 3) )
            tab['GPOS']        =  f0['halos'][h_i]['GPOS']       [...]
            tab['M200c']       =  f0['halos'][h_i]['M200c']      [...]
            tab['M200m']       =  f0['halos'][h_i]['M200m']      [...]
            tab['M500c']       =  f0['halos'][h_i]['M500c']      [...]
            tab['M500m']       =  f0['halos'][h_i]['M500m']      [...]
            tab['Rvir']        =  f0['halos'][h_i]['Rvir']       [...]
            tab['dm_Sigma']    =  f0['halos'][h_i]['dm_Sigma']   [...]
            tab['has_profile'] =  f0['halos'][h_i]['has_profile'][...]
            tab['hydro_GPOS']  =  f0['halos'][h_i]['hydro_GPOS'] [...]
            tab['hydro_M200c'] =  f0['halos'][h_i]['hydro_M200c'][...]
            tab['hydro_M200m'] =  f0['halos'][h_i]['hydro_M200m'][...]
            tab['hydro_M500c'] =  f0['halos'][h_i]['hydro_M500c'][...]
            tab['hydro_M500m'] =  f0['halos'][h_i]['hydro_M500m'][...]
            tab['hydro_Rvir']  =  f0['halos'][h_i]['hydro_Rvir'] [...]
            tab['hydro_Sigma'] =  f0['halos'][h_i]['hydro_Sigma'][...]
            tab['hydro_name']  =  f0['halos'][h_i]['hydro_name'] [...].astype('int')
            tab['match_dist']  =  f0['halos'][h_i]['match_dist'] [...]
            ts0.append(tab[0])
            ts1.append(tab[1])
            ts2.append(tab[2])
        else:
            print(h_i, 'no profile')

    ts0 = vstack(( ts0 ))
    ts1 = vstack(( ts1 ))
    ts2 = vstack(( ts2 ))

    all_m500c = n.array(ts0['M500c'])
    sort_m500c_id = n.argsort(all_m500c)

    ts0 = ts0[sort_m500c_id]
    ts1 = ts1[sort_m500c_id]
    ts2 = ts2[sort_m500c_id]

    ts0.write(path_2_WL_0[:-3]+'_Rotation0.fits', overwrite = True)
    ts1.write(path_2_WL_0[:-3]+'_Rotation1.fits', overwrite = True)
    ts2.write(path_2_WL_0[:-3]+'_Rotation2.fits', overwrite = True)


re_write_files(halo_list0, f0, path_2_WL_0)
re_write_files(halo_list1, f1, path_2_WL_1)
re_write_files(halo_list3, f3, path_2_WL_3)



def re_write_files_noDM(halo_list0, f0, path_2_WL_0):
    # rotation 0
    ts0 = []
    # rotation 1
    ts1 = []
    # rotation 2
    ts2 = []

    for h_i in halo_list0:
        if f0['halos'][h_i]['has_profile'][...]==1:
            tab = Table()
            #M500c_hi_dict_0[ n.round( n.log10(f0['halos'][h_i]['M500c'][...]*1e10 / hz(z0) ), 3) ] = h_i
            #all_m500c.append( n.round( n.log10(f0['halos'][h_i]['M500c'][...]*1e10 / hz(z0) ), 3) )
            tab['GPOS']        =  f0['halos'][h_i]['GPOS']       [...]
            tab['M200c']       =  f0['halos'][h_i]['M200c']      [...]
            tab['M200m']       =  f0['halos'][h_i]['M200m']      [...]
            tab['M500c']       =  f0['halos'][h_i]['M500c']      [...]
            tab['M500m']       =  f0['halos'][h_i]['M500m']      [...]
            tab['Rvir']        =  f0['halos'][h_i]['Rvir']       [...]
            #tab['dm_Sigma']    =  f0['halos'][h_i]['dm_Sigma']   [...]
            tab['has_profile'] =  f0['halos'][h_i]['has_profile'][...]
            tab['hydro_GPOS']  =  f0['halos'][h_i]['hydro_GPOS'] [...]
            tab['hydro_M200c'] =  f0['halos'][h_i]['hydro_M200c'][...]
            tab['hydro_M200m'] =  f0['halos'][h_i]['hydro_M200m'][...]
            tab['hydro_M500c'] =  f0['halos'][h_i]['hydro_M500c'][...]
            tab['hydro_M500m'] =  f0['halos'][h_i]['hydro_M500m'][...]
            tab['hydro_Rvir']  =  f0['halos'][h_i]['hydro_Rvir'] [...]
            tab['hydro_Sigma'] =  f0['halos'][h_i]['hydro_Sigma'][...]
            tab['hydro_name']  =  f0['halos'][h_i]['hydro_name'] [...].astype('int')
            tab['match_dist']  =  f0['halos'][h_i]['match_dist'] [...]
            ts0.append(tab[0])
            ts1.append(tab[1])
            ts2.append(tab[2])
        else:
            print(h_i, 'no profile')

    ts0 = vstack(( ts0 ))
    ts1 = vstack(( ts1 ))
    ts2 = vstack(( ts2 ))

    all_m500c = n.array(ts0['M500c'])
    sort_m500c_id = n.argsort(all_m500c)

    ts0 = ts0[sort_m500c_id]
    ts1 = ts1[sort_m500c_id]
    ts2 = ts2[sort_m500c_id]

    ts0.write(path_2_WL_0[:-3]+'_Rotation0.fits', overwrite = True)
    ts1.write(path_2_WL_0[:-3]+'_Rotation1.fits', overwrite = True)
    ts2.write(path_2_WL_0[:-3]+'_Rotation2.fits', overwrite = True)

re_write_files_noDM(halo_list2, f2, path_2_WL_2)
