"""
What it does
------------

Computes the X-ray cluster model in each shell

References
----------

comparat et al. in prep

Command to run
--------------

python3 004_0_cluster.py environmentVAR fileBasename image_boolean

arguments
---------

environmentVAR: environment variable linking to the directory where files are e.g. "MD10"
It will then work in the directory : $environmentVAR/hlists/fits/

fileBasename: base name of the file e.g. all_1.00000

image_boolean: yes or no will write or not the images for sixte / simput simulations

Dependencies
------------

import time, os, sys, numpy, scipy, astropy, h5py, extinction, matplotlib


244 objects : 

182 XXL, 
49 HIFLUGS
12 XCOP
"""
from astropy.table import Table, Column
from astropy_healpix import healpy
import sys
import os
import time
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import astropy.constants as cc
import astropy.io.fits as fits
from scipy.special import erf
from scipy.stats import norm
from scipy.interpolate import interp2d
from scipy.interpolate import interp1d
import h5py
import numpy as n
from lib_cluster import write_img, create_matrix
from scipy.interpolate import interp1d
print('computes LX_r500x/LX_rvir vs redshift to retrieve optimal aperture')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

env = 'MD04'

# simulation setup
if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

z_snap = 0.03
omega = lambda zz: cosmo.Om0*(1+zz)**3. / cosmo.efunc(zz)**2
DeltaVir_bn98 = lambda zz : (18.*n.pi**2. + 82.*(omega(zz)-1)- 39.*(omega(zz)-1)**2.) /omega(zz)
rho_critical = (cosmo.critical_density(z_snap).to(u.Msun*u.kpc**(-3))  ).value

# cosmological volume
zmin = 0.
zmax = 1.5

path_2_cbp = os.path.join(os.environ['GIT_CBP'])

Mpc=3.0856776e+24
msun=1.98892e33

covor=n.loadtxt(os.path.join(path_2_cbp, 'covmat_xxl_hiflugcs_xcop.txt'))
xgrid_ext=n.loadtxt(os.path.join(path_2_cbp, 'radial_binning.txt'))
mean_log=n.loadtxt(os.path.join(path_2_cbp, 'mean_pars.txt'))
coolfunc=n.loadtxt(os.path.join(path_2_cbp, 'coolfunc.dat'))

md10 = fits.open(os.path.join( os.environ['MD10'], "MD10_eRO_CLU_b8_CM_0_pixS_20.0_wCount_June2020.fit") )
M500MvirRatio = []
for z in n.arange(0.05, 1.2, 0.05):
	s1 = (md10[1].data['redshift_R']>z-0.05/2.) & (md10[1].data['redshift_R']<z+0.05/2.)
	M500MvirRatio.append( n.median(md10[1].data['HALO_M500c'][s1]/md10[1].data['HALO_Mvir'][s1]) )

OverdensityRatio = []
for z in n.arange(0.05, 1.2, 0.05):
	OverdensityRatio.append(DeltaVir_bn98(z)/500)

Ratio = (n.array(OverdensityRatio)*n.array(M500MvirRatio))**(1./3.)
itp_r500c_over_rVir = interp1d(
	n.hstack((0., n.arange(0.05, 1.2, 0.05), 2)), 
	n.hstack((Ratio[0], Ratio, Ratio[-1])) )


r500c_size = []
for z in n.arange(0.05, 1.2, 0.05):
	conversion = cosmo.arcsec_per_kpc_proper(z).value
	s1 = (md10[1].data['redshift_R']>z-0.05/2.) & (md10[1].data['redshift_R']<z+0.05/2.)
	r500c_size.append( [z, n.median(md10[1].data['HALO_Rvir'][s1]), n.std(md10[1].data['HALO_Rvir'][s1]), conversion, itp_r500c_over_rVir(z)] )

DATA = n.array(r500c_size)
n.savetxt(os.path.join( os.environ['MD10'], 'typical_size.ascii'), DATA, header='z median_Rvir std_Rvir kpc_2_arcseconds r500c_over_rvir')

def calc_lx(prof,kt,m5,z,fraction=1):
	"""
	Compute the X-ray luminosity in the profile
	to be extended to 3x r500c
	
	Compute r_{500c} :
		r_{500c} = \left(\frac{3 M_{500c}}{ 4. \pi 500 \rho_c(z)  }\right)^{1/3} [ cm ].
	
	profile_emission = profile x rescale_factor
	rescale_factor = \sqrt(kT/10.0) E^3(z)
	CF(kT) = cooling function, show the curve
	L_X(r) = \Sigma_{<r}( profile_emission r_{500c}^2 2 \pi x CF(kT) Mpc=3.0856776e+24 dx )
	L_{500c} = L_X(1)
	
	"""
	ez2 = cosmo.efunc(z)**2
	rhoc = cosmo.critical_density(z).value
	r500 = n.power(m5*msun/4.*3./n.pi/500./rhoc,1./3.)
	#Dvir = DeltaVir_bn98(z)
	#rvir = n.power(m5*msun/4.*3./n.pi/500./rhoc,1./3.)
	resfact = n.sqrt(kt/10.0)*n.power(ez2,3./2.)
	prof_em = prof * resfact # emission integral
	tlambda = n.interp(kt,coolfunc[:,0],coolfunc[:,1]) # cooling function
	dx = n.empty(len(xgrid_ext))
	dx[0]=xgrid_ext[0]
	dx[1:len(xgrid_ext)]=(n.roll(xgrid_ext,-1)-xgrid_ext)[:len(xgrid_ext)-1]
	#print(prof_em*xgrid_ext*r500**2*2.*n.pi*tlambda*Mpc*dx)
	lxcum = n.cumsum(prof_em*xgrid_ext*r500**2*2.*n.pi*tlambda*Mpc*dx) # riemann integral
	lx_500=n.interp(fraction,xgrid_ext,lxcum) # evaluated at R500
	return lx_500

nsim = 100000
profs=n.exp(n.random.multivariate_normal(mean_log,covor,size=nsim))

allz_i  = profs[:,len(mean_log)-3]
allkt_i = profs[:,len(mean_log)-1]
allm5_i = profs[:,len(mean_log)-2]

profiles_i = profs[:,:len(xgrid_ext)]

in_zbin = (allz_i<zmax+0.05)&(allz_i>zmin-0.05)

allz     = allz_i    [in_zbin]
allkt    = allkt_i   [in_zbin]
allm5    = allm5_i   [in_zbin]
profiles = profiles_i[in_zbin]
nsim2 = len(allz)

# maximum is 1.6 r500c
r_norm = 1./itp_r500c_over_rVir(allz)

alllx       = n.empty(nsim2)
alllx_rvir  = n.empty(nsim2)
for i, rvir_frac in zip(range(nsim2), r_norm):
	tprof=profiles[i]
	alllx[i]          =calc_lx(tprof,allkt[i],allm5[i],allz[i], 1)
	alllx_rvir[i] =calc_lx(tprof,allkt[i],allm5[i],allz[i], rvir_frac)

frac_LX = alllx/alllx_rvir 
DATA = n.transpose([ allz, allkt, allm5, alllx, alllx_rvir, frac_LX ])
n.savetxt(os.path.join( os.environ['MD10'], 'LX_Lvir.ascii'), DATA, header='z kT M500c LX_500c LX_vir fraction')

