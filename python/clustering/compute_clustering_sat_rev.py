import glob
import h5py
import os
import sys
import matplotlib.pyplot as p
import matplotlib
import numpy as n
import astropy.io.fits as fits
import time
t0 = time.time()
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})

env = sys.argv[1]  # 'MD04'
print(env)

laptop = True
laptop = False
if laptop:
    CUTE_cmd = "/home/comparat/software/CUTE/CUTE/CUTE "
else:
    CUTE_cmd = "~/darksim/software/CUTE/CUTE/CUTE "

test_dir = os.path.join(os.environ[env], 'hlists', 'fits')

if env == 'MD04':
    M_bins = 10**n.arange(8, 15, 0.5)
else:
    M_bins = 10**n.arange(11, 15, 0.5)

clustering_dir = os.path.join(test_dir, 'clustering')
if os.path.isdir(clustering_dir) == False:
    os.system('mkdir -p ' + clustering_dir)

fig_dir = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    'figures',
    env,
    'clustering_DMhalos')
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)


class Pathes:
    def __init__(self):
        self.p1 = 0


pathes = Pathes()
pathes.path_2_randoms = os.path.join(clustering_dir, 'random-ra-dec.txt')

# loads randoms
raR, decR = n.loadtxt(pathes.path_2_randoms, unpack=True)
print('randoms loaded', time.time() - t0)

# clustering in the data

# out_dir = '/data36s/comparat/AGN_clustering/angular_clustering/'


# clustering as a function of halo mass
def run_clustering_Mvir_bins(pathes):
    # opens light cone file
    f1 = fits.open(pathes.path_2_light_cone)
    mvir = f1[1].data['Mvir']
    print('N objects in mvir', len(mvir), 't=', time.time() - t0)
    # print(mvir)
    f1.close()
    # sky coordinates
    h5_file = os.path.join(pathes.path_2_coordinate_file)
    f = h5py.File(h5_file, "r")
    #zr = f['/coordinates/redshift_R'].value
    zs = f['/coordinates/redshift_S'].value
    ra = f['/coordinates/ra'].value
    dec = f['/coordinates/dec'].value
    print('N objects in ra', len(ra), 't=', time.time() - t0)
    rds_all = n.random.random(len(ra))
    f.close()
    # creates sub samples (full sky)
    for m0, m1 in zip(M_bins[:-1], M_bins[1:]):
        print(n.log10(m0), '<log M<', n.log10(m1), 't=', time.time() - t0)
        suffix = '_' + str(n.round(n.log10(m0), 1)) + \
            '_mvir_' + str(n.round(n.log10(m1), 1))
        if os.path.isfile(
                pathes.path_2_radecz_basename +
                suffix +
                '.data') == False:
            sel_mass = (mvir >= m0) & (mvir < m1)
            N_obj = len(zs[sel_mass])
            if N_obj > 1000:
                sel = (rds_all < 5e5 / N_obj) & (sel_mass)
                N_data = len(ra[sel])
                print('N in bin', N_data, 't=', time.time() - t0)
                if N_data > 1000:
                    ra_D, dec_D, z_D, w_D = ra[sel], dec[sel], zs[sel], n.ones(
                        N_data).astype('float')
                    n.savetxt(pathes.path_2_radecz_basename + suffix +
                              '.data', n.transpose([ra_D, dec_D, z_D, w_D]))
                    N_rds = 10 * N_data  # len(raR)
                    print("D,R=", N_data, N_rds, 't=', time.time() - t0)
                    dz = 0.005
                    zmin = n.min(zs[sel])
                    zmax = n.max(zs[sel])
                    # redshift histogram to mimick NZ
                    z_bins = n.arange(zmin, zmax + 2 * dz, dz)
                    nn, bb = n.histogram(z_D, bins=z_bins)
                    rdsz = []
                    for jj, (zlow, zhigh) in enumerate(
                            zip(z_bins[:-1], z_bins[1:])):
                        #print(zlow, zhigh)
                        inter = n.random.uniform(
                            low=zlow, high=zhigh, size=int(
                                1000 * nn[jj]))
                        rdsz.append(inter)

                    rds = n.hstack((rdsz))
                    n.random.shuffle(rds)
                    RR = rds[:N_rds]  # -dz/2.
                    print(
                        "number of randoms",
                        len(rds),
                        'subsampled to',
                        len(RR),
                        't=',
                        time.time() - t0)
                    n.random.shuffle(raR)
                    n.random.shuffle(decR)
                    ra_R, dec_R, z_R, w_R = raR[:N_rds], decR[:N_rds], RR, n.ones_like(
                        RR).astype('float')
                    n.savetxt(pathes.path_2_radecz_basename + suffix +
                              '.random', n.transpose([ra_R, dec_R, z_R, w_R]))

                    # Monopole
                    f = open(
                        pathes.path_2_radecz_basename +
                        suffix +
                        '.monopole.ini',
                        'w')
                    f.write(
                        'data_filename= ' +
                        pathes.path_2_radecz_basename +
                        suffix +
                        '.data \n')
                    f.write(
                        'random_filename= ' +
                        pathes.path_2_radecz_basename +
                        suffix +
                        '.random \n')
                    f.write('input_format= 2 \n')
                    f.write(
                        'output_filename= ' +
                        pathes.path_2_radecz_basename +
                        suffix +
                        '.monopole.2pcf \n')
                    f.write('corr_type= monopole \n')
                    f.write('omega_M= 0.307 \n')
                    f.write('omega_L= 0.693 \n')
                    f.write('w= -1 \n')
                    f.write('log_bin= 0 \n')
                    f.write('dim1_max= 160. \n')
                    #f.write('dim1_min_logbin= 0.01 \n')
                    f.write('dim1_nbin= 40 \n')
                    f.write('dim2_max= 160. \n')
                    f.write('dim2_nbin= 40 \n')
                    f.write('dim3_min= 0.00 \n')
                    f.write('dim3_max= 3. \n')
                    f.write('dim3_nbin= 1 \n')
                    f.close()
                    os.system(
                        CUTE_cmd +
                        pathes.path_2_radecz_basename +
                        suffix +
                        '.monopole.ini')
                    # Angular clustering
                    f = open(
                        pathes.path_2_radecz_basename +
                        suffix +
                        '.wtheta.ini',
                        'w')
                    f.write(
                        'data_filename= ' +
                        pathes.path_2_radecz_basename +
                        suffix +
                        '.data \n')
                    f.write(
                        'random_filename= ' +
                        pathes.path_2_radecz_basename +
                        suffix +
                        '.random \n')
                    f.write('input_format= 2 \n')
                    f.write(
                        'output_filename= ' +
                        pathes.path_2_radecz_basename +
                        suffix +
                        '.wtheta.2pcf \n')
                    f.write('corr_type= angular \n')
                    f.write('omega_M= 0.307 \n')
                    f.write('omega_L= 0.693 \n')
                    f.write('w= -1 \n')
                    f.write('log_bin= 10 \n')
                    f.write('dim1_min_logbin= 0.001 \n')
                    f.write('dim1_max= 1. \n')
                    f.write('dim1_nbin= 60 \n')
                    f.write('dim2_max= 160. \n')
                    f.write('dim2_nbin= 40 \n')
                    f.write('dim3_min= 0.00 \n')
                    f.write('dim3_max= 3. \n')
                    f.write('dim3_nbin= 1 \n')
                    f.write('use_pm= 0 \n')
                    f.write('n_pix_sph= 1000000 \n')
                    f.close()
                    os.system(
                        CUTE_cmd +
                        pathes.path_2_radecz_basename +
                        suffix +
                        '.wtheta.ini')

                    fig = p.figure(3, (12, 16))
                    # wedge plot
                    fig.add_subplot(3, 2, 1)

                    p.plot(
                        ra_D,
                        dec_D,
                        marker=',',
                        color='b',
                        alpha=0.5,
                        rasterized=True,
                        ls='None',
                        label=str(
                            len(ra_D)))
                    p.xlabel('R.A. [deg]')
                    p.ylabel('Dec. [deg]')
                    p.legend(frameon=False, loc=0)
                    p.title('data')

                    fig.add_subplot(3, 2, 2)
                    p.plot(
                        ra_R,
                        dec_R,
                        marker=',',
                        color='b',
                        alpha=0.1,
                        rasterized=True,
                        ls='None',
                        label=str(
                            len(ra_R)))
                    p.xlabel('R.A. [deg]')
                    p.ylabel('Dec. [deg]')
                    p.legend(frameon=False, loc=0)
                    p.title('randoms')

                    fig.add_subplot(3, 3, 4)
                    # KS-test RA
                    nn_R, bb, pp = p.hist(ra_R, bins=n.arange(
                        361), histtype='step', cumulative=True, normed=True, lw=4)
                    nn_D, bb, pp = p.hist(
                        ra_D, bins=bb, histtype='step', cumulative=True, normed=True, lw=2)
                    KS_out = n.max(nn_R - nn_D)
                    p.title(str(n.round(KS_out, 4)))
                    p.xlabel('R.A. [deg]')
                    p.ylabel('cdf')
                    p.grid()

                    fig.add_subplot(3, 3, 5)
                    # KS-test DEC
                    nn_R, bb, pp = p.hist(dec_R, bins=n.arange(
                        181), histtype='step', cumulative=True, normed=True, lw=4)
                    nn_D, bb, pp = p.hist(
                        dec_D, bins=bb, histtype='step', cumulative=True, normed=True, lw=2)
                    KS_out = n.max(nn_R - nn_D)
                    p.title(str(n.round(KS_out, 4)))
                    p.ylabel('Dec. [deg]')
                    p.ylabel('cdf')
                    p.grid()

                    fig.add_subplot(3, 3, 6)
                    # KS test redshift
                    p.hist(
                        z_D,
                        bins=z_bins,
                        histtype='step',
                        cumulative=True,
                        normed=True,
                        rasterized=True,
                        lw=4)
                    p.hist(
                        z_R,
                        bins=z_bins,
                        histtype='step',
                        cumulative=True,
                        normed=True,
                        rasterized=True,
                        lw=2)
                    p.xlabel('redshift')
                    p.ylabel('cdf')
                    # p.xscale('log')
                    # p.yscale('log')
                    p.title('median z=' + str(n.round(n.median(z_D), 3)))
                    p.grid()

                    fig.add_subplot(3, 2, 5)
                    # w theta
                    DATA = n.loadtxt(
                        pathes.path_2_radecz_basename +
                        suffix +
                        '.wtheta.2pcf',
                        unpack=True)
                    p.errorbar(DATA[0], DATA[1], yerr=DATA[1]
                               * DATA[2]**(-0.5), rasterized=True)
                    p.plot(
                        DATA[0],
                        0.1 * DATA[0]**-0.8,
                        label=r'$0.1\theta^{-0.8}$')
                    p.xlabel(r'$\theta$ [deg]')
                    p.ylabel(r'$w(\theta)$ ')
                    p.xscale('log')
                    p.legend()
                    p.title(
                        os.path.basename(
                            pathes.path_2_radecz_basename) +
                        suffix)
                    p.yscale('log')
                    p.grid()

                    fig.add_subplot(3, 2, 6)
                    # w theta
                    DATA = n.loadtxt(
                        pathes.path_2_radecz_basename +
                        suffix +
                        '.monopole.2pcf',
                        unpack=True)
                    p.errorbar(DATA[0], DATA[1], yerr=DATA[1]
                               * DATA[2]**(-0.5), rasterized=True)
                    p.plot(DATA[0], 10 * DATA[0]**-1.8, label=r'$10s^{-1.8}$')
                    p.xlabel('s [Mpc]')
                    p.ylabel(r'$\xi(s)$')
                    p.xscale('log')
                    #p.title(os.path.basename(pathes.path_2_radecz_basename) + suffix)
                    p.yscale('log')
                    p.grid()

                    p.savefig(
                        pathes.path_2_figure +
                        suffix +
                        ".clustering.png")
                    p.clf()
                    print(
                        pathes.path_2_radecz_basename +
                        suffix,
                        'finished',
                        't=',
                        time.time() -
                        t0)
        else:
            print('done')


all_files = sorted(
    n.array(
        glob.glob(
            os.path.join(
                test_dir,
                'sat_?.?????.fits'))))
#all_files = n.array(glob.glob(os.path.join(test_dir, 'all_?.?????.fits')))

for fff in all_files[::-1]:
    print(fff)
    baseName = os.path.basename(fff)[:-5]
    pathes = Pathes()
    pathes.path_2_light_cone = os.path.join(test_dir, baseName + '.fits')
    pathes.path_2_coordinate_file = os.path.join(
        test_dir, baseName + '_coordinates.h5')
    pathes.path_2_galaxy_file = os.path.join(test_dir, baseName + '_galaxy.h5')
    pathes.path_2_agn_file = os.path.join(test_dir, baseName + '_agn.h5')
    pathes.path_2_eRO_catalog = os.path.join(
        test_dir, baseName + '_eRO_AGN.fit')
    pathes.path_2_radecz_basename = os.path.join(clustering_dir, baseName)
    pathes.path_2_figure = os.path.join(fig_dir, baseName)
    run_clustering_Mvir_bins(pathes)
