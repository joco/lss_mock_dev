import sys
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
import matplotlib.pyplot as p
import numpy as n
import lc_setup
import h5py    # HDF5 support
import os
import glob
import numpy as n
from scipy.interpolate import interp1d

import matplotlib
matplotlib.use('Agg')

cosmoMD = FlatLambdaCDM(H0=67.77 * u.km / u.s / u.Mpc, Om0=0.307115)

#lc_id = sys.argv[1]


theory_dir = '/data17s/darksim/theory/camb'
"""
/data17s/darksim/theory/camb/xi_0.05.dat  /data17s/darksim/theory/camb/xi_1.55.dat
/data17s/darksim/theory/camb/xi_0.0.dat   /data17s/darksim/theory/camb/xi_1.5.dat
/data17s/darksim/theory/camb/xi_0.15.dat  /data17s/darksim/theory/camb/xi_1.65.dat
/data17s/darksim/theory/camb/xi_0.1.dat   /data17s/darksim/theory/camb/xi_1.6.dat
/data17s/darksim/theory/camb/xi_0.25.dat  /data17s/darksim/theory/camb/xi_1.75.dat
/data17s/darksim/theory/camb/xi_0.2.dat   /data17s/darksim/theory/camb/xi_1.7.dat
/data17s/darksim/theory/camb/xi_0.35.dat  /data17s/darksim/theory/camb/xi_1.85.dat
/data17s/darksim/theory/camb/xi_0.3.dat   /data17s/darksim/theory/camb/xi_1.8.dat
/data17s/darksim/theory/camb/xi_0.45.dat  /data17s/darksim/theory/camb/xi_1.95.dat
/data17s/darksim/theory/camb/xi_0.4.dat   /data17s/darksim/theory/camb/xi_1.9.dat
/data17s/darksim/theory/camb/xi_0.55.dat  /data17s/darksim/theory/camb/xi_2.05.dat
/data17s/darksim/theory/camb/xi_0.5.dat   /data17s/darksim/theory/camb/xi_2.0.dat
/data17s/darksim/theory/camb/xi_0.65.dat  /data17s/darksim/theory/camb/xi_2.15.dat
/data17s/darksim/theory/camb/xi_0.6.dat   /data17s/darksim/theory/camb/xi_2.1.dat
/data17s/darksim/theory/camb/xi_0.75.dat  /data17s/darksim/theory/camb/xi_2.25.dat
/data17s/darksim/theory/camb/xi_0.7.dat   /data17s/darksim/theory/camb/xi_2.2.dat
/data17s/darksim/theory/camb/xi_0.85.dat  /data17s/darksim/theory/camb/xi_2.35.dat
/data17s/darksim/theory/camb/xi_0.8.dat   /data17s/darksim/theory/camb/xi_2.3.dat
/data17s/darksim/theory/camb/xi_0.95.dat  /data17s/darksim/theory/camb/xi_2.45.dat
/data17s/darksim/theory/camb/xi_0.9.dat   /data17s/darksim/theory/camb/xi_2.4.dat
/data17s/darksim/theory/camb/xi_1.05.dat  /data17s/darksim/theory/camb/xi_2.55.dat
/data17s/darksim/theory/camb/xi_1.0.dat   /data17s/darksim/theory/camb/xi_2.5.dat
/data17s/darksim/theory/camb/xi_1.15.dat  /data17s/darksim/theory/camb/xi_2.65.dat
/data17s/darksim/theory/camb/xi_1.1.dat   /data17s/darksim/theory/camb/xi_2.6.dat
/data17s/darksim/theory/camb/xi_1.25.dat  /data17s/darksim/theory/camb/xi_2.75.dat
/data17s/darksim/theory/camb/xi_1.2.dat   /data17s/darksim/theory/camb/xi_2.7.dat
/data17s/darksim/theory/camb/xi_1.35.dat  /data17s/darksim/theory/camb/xi_2.85.dat
/data17s/darksim/theory/camb/xi_1.3.dat   /data17s/darksim/theory/camb/xi_2.8.dat
/data17s/darksim/theory/camb/xi_1.45.dat  /data17s/darksim/theory/camb/xi_2.95.dat
/data17s/darksim/theory/camb/xi_1.4.dat   /data17s/darksim/theory/camb/xi_2.9.dat

"""


def get_z_mean(Lname, tracer_name):
    path_2_lc = '/data17s/darksim/MD/MD_1.0Gpc/h5_lc/lc_' + Lname + '.hdf5'
    setup = lc_setup.cones[Lname]
    f = h5py.File(path_2_lc, 'r')
    if tracer_name == '4MOST_BGLZ':
        is_gal = (
            f['/sky_position/selection'].value) & (f['/4MOST_COSMO_tracers/is_BG_lz'].value)
    if tracer_name == '4MOST_BGHZ':
        is_gal = (
            f['/sky_position/selection'].value) & (f['/4MOST_COSMO_tracers/is_BG_hz'].value)
    if tracer_name == '4MOST_ELG':
        is_gal = (
            f['/sky_position/selection'].value) & (f['/4MOST_COSMO_tracers/is_ELG'].value)
    if tracer_name == 'EBOSS_ELG':
        is_gal = (
            f['/sky_position/selection'].value) & (f['/EBOSS_tracers/is_ELG'].value)
    if tracer_name == '4MOST_QSO':
        is_gal = (
            f['/sky_position/selection'].value) & (f['/4MOST_COSMO_tracers/is_QSO'].value)
    if tracer_name == 'eRo_AGN':
        is_gal = (f['/sky_position/selection'].value) & (f['/agn_properties/agn_activity'].value ==
                                                         1) & (f['/agn_properties/rxay_flux_05_20'].value > 1e-14)
    if tracer_name == 'eRo_CLUS':
        is_gal = (f['/sky_position/selection'].value) & (
            f['/cluster_properties/rxay_flux_05_24'].value > 1e-14)
    if tracer_name == 'eRo_CGAL':
        is_gal = (f['/sky_position/selection'].value) & (f['/cluster_galaxies/cluster_flux_05_24'].value > 1e-14) & (
            f['/optical_galaxy_magnitudes/sdss_mag_r'].value < 23.5)  # &(f['/cluster_galaxies/red_sequence_flag'].value)

    n_gal = len(f['/sky_position/redshift_S'].value[is_gal])
    outH = n.histogram(
        f['/sky_position/redshift_S'].value[is_gal],
        bins=n.arange(
            0,
            10,
            0.1))
    z_mean = n.mean(f['/sky_position/redshift_S'].value[is_gal])
    # , outH[0], outH[1]/setup.area
    return z_mean, len(
        f['/sky_position/redshift_S'].value[is_gal]) / setup.area


def run_bias(Lname, tracer_name, z_th='0.2'):
    out_dir = '/data17s/darksim/MD/MD_1.0Gpc/h5_lc/clustering_catalogs_' + Lname + '/'
    pcf_name = glob.glob(
        os.path.join(
            out_dir,
            Lname +
            '_' +
            tracer_name +
            '_*.2pcf'))[0]
    xi_file = 'xi_' + z_th + '.dat'
    rs, xs = n.loadtxt(os.path.join(theory_dir, xi_file), unpack=True)
    DAT = n.loadtxt(pcf_name, unpack=True)
    ref = interp1d(rs, xs)
    sel = (DAT[0] > 4) & (DAT[0] < 40)
    data = interp1d(DAT[0][sel], DAT[1][sel])
    return n.mean(data.y / ref(data.x))


OUT_DATA = []

lc_id = "C1"
setup = lc_setup.cones[lc_id]
z_mean, density = get_z_mean(lc_id, '4MOST_BGLZ')
print(z_mean, density)
z_th = str(int(z_mean * 20) / 20.)
print(z_th)
bglz = run_bias(lc_id, '4MOST_BGLZ', z_th=z_th)
print(bglz)
OUT_DATA.append([setup.z_start, setup.z_reach, z_mean, density, bglz])

lc_id = "C2"
setup = lc_setup.cones[lc_id]
z_mean, density = get_z_mean(lc_id, '4MOST_BGLZ')
print(z_mean, density)
z_th = str(int(z_mean * 20) / 20.)
print(z_th)
bglz = run_bias(lc_id, '4MOST_BGLZ', z_th=z_th)
print(bglz)
OUT_DATA.append([setup.z_start, setup.z_reach, z_mean, density, bglz])

n.savetxt('/data17s/darksim/MD/MD_1.0Gpc/h5_lc/bglz_bias.txt',
          OUT_DATA, header='zmin zmax zmean density_per_deg2 bias')


OUT_DATA = []

lc_id = "C1"
setup = lc_setup.cones[lc_id]
z_mean, density = get_z_mean(lc_id, '4MOST_BGHZ')
z_th = str(int(z_mean * 20) / 20.)
bglz = run_bias(lc_id, '4MOST_BGHZ', z_th=z_th)
OUT_DATA.append([setup.z_start, setup.z_reach, z_mean, density, bglz])

lc_id = "C2"
setup = lc_setup.cones[lc_id]
z_mean, density = get_z_mean(lc_id, '4MOST_BGHZ')
z_th = str(int(z_mean * 20) / 20.)
bglz = run_bias(lc_id, '4MOST_BGHZ', z_th=z_th)
OUT_DATA.append([setup.z_start, setup.z_reach, z_mean, density, bglz])

lc_id = "C3"
setup = lc_setup.cones[lc_id]
z_mean, density = get_z_mean(lc_id, '4MOST_BGHZ')
z_th = str(int(z_mean * 20) / 20.)
bglz = run_bias(lc_id, '4MOST_BGHZ', z_th=z_th)
OUT_DATA.append([setup.z_start, setup.z_reach, z_mean, density, bglz])

n.savetxt('/data17s/darksim/MD/MD_1.0Gpc/h5_lc/bghz_bias.txt',
          OUT_DATA, header='zmin zmax zmean density_per_deg2 bias')


def get_data(lc_id="C1", tracer_name='4MOST_ELG'):
    setup = lc_setup.cones[lc_id]
    z_mean, density = get_z_mean(lc_id, tracer_name)
    z_th = str(int(z_mean * 20) / 20.)
    bglz = run_bias(lc_id, tracer_name, z_th=z_th)
    return [setup.z_start, setup.z_reach, z_mean, density, bglz]


OUT_DATA = []
OUT_DATA.append(get_data(lc_id="C1", tracer_name='4MOST_ELG'))
OUT_DATA.append(get_data(lc_id="C2", tracer_name='4MOST_ELG'))
OUT_DATA.append(get_data(lc_id="C3", tracer_name='4MOST_ELG'))
OUT_DATA.append(get_data(lc_id="C4", tracer_name='4MOST_ELG'))
n.savetxt('/data17s/darksim/MD/MD_1.0Gpc/h5_lc/elg_bias.txt',
          OUT_DATA, header='zmin zmax zmean density_per_deg2 bias')


OUT_DATA = []
OUT_DATA.append(get_data(lc_id="C2", tracer_name='4MOST_QSO'))
OUT_DATA.append(get_data(lc_id="C3", tracer_name='4MOST_QSO'))
OUT_DATA.append(get_data(lc_id="C4", tracer_name='4MOST_QSO'))
OUT_DATA.append(get_data(lc_id="C5", tracer_name='4MOST_QSO'))
n.savetxt('/data17s/darksim/MD/MD_1.0Gpc/h5_lc/qso_bias.txt',
          OUT_DATA, header='zmin zmax zmean density_per_deg2 bias')


OUT_DATA = []
OUT_DATA.append(get_data(lc_id="C1", tracer_name='eRo_AGN'))
OUT_DATA.append(get_data(lc_id="C2", tracer_name='eRo_AGN'))
OUT_DATA.append(get_data(lc_id="C3", tracer_name='eRo_AGN'))
OUT_DATA.append(get_data(lc_id="C4", tracer_name='eRo_AGN'))
OUT_DATA.append(get_data(lc_id="C5", tracer_name='eRo_AGN'))
n.savetxt('/data17s/darksim/MD/MD_1.0Gpc/h5_lc/ero_agn_bias.txt',
          OUT_DATA, header='zmin zmax zmean density_per_deg2 bias')


OUT_DATA = []
OUT_DATA.append(get_data(lc_id="C1", tracer_name='eRo_CLUS'))
OUT_DATA.append(get_data(lc_id="C2", tracer_name='eRo_CLUS'))
n.savetxt('/data17s/darksim/MD/MD_1.0Gpc/h5_lc/ero_clus_bias.txt',
          OUT_DATA, header='zmin zmax zmean density_per_deg2 bias')


OUT_DATA = []
OUT_DATA.append(get_data(lc_id="C1", tracer_name='eRo_CGAL'))
OUT_DATA.append(get_data(lc_id="C2", tracer_name='eRo_CGAL'))
n.savetxt('/data17s/darksim/MD/MD_1.0Gpc/h5_lc/ero_cgal_bias.txt',
          OUT_DATA, header='zmin zmax zmean density_per_deg2 bias')
