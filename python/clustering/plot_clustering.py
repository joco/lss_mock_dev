import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
import matplotlib.pyplot as p
import numpy as n
import h5py    # HDF5 support
import os
import glob
import numpy as n
from scipy.interpolate import interp1d

import matplotlib
matplotlib.use('Agg')

cosmoMD = FlatLambdaCDM(H0=67.77 * u.km / u.s / u.Mpc, Om0=0.307115)

dir_2_clustering_2rxs = os.path.join(
    os.environ['GIT_MAKESAMPLE'],
    'data/wtheta_2RXS')


listWtheta2RXS = sorted(
    n.array(
        glob.glob(
            os.path.join(
                dir_2_clustering_2rxs,
                '*.wtheta'))))
path_2_2rxs = listWtheta2RXS[0]

md04_wth_list = n.array(
    glob.glob(
        os.path.join(
            os.environ['MD10'],
            'hlist',
            'fits',
            'clustering',
            '*.wtheta.2pcf')))
md10_wth_list = n.array(
    glob.glob(
        os.path.join(
            os.environ['MD04'],
            'hlists',
            'fits',
            'clustering',
            '*.wtheta.2pcf')))

wth_list = md10_wth_list
prefix = 'md10'

wth_list = md04_wth_list
prefix = 'md04'


def plot_set(wth_list, prefix):
    cos = n.array([os.path.basename(el).split('_')[0] for el in wth_list])
    aexp = n.array([os.path.basename(el).split('_')[1] for el in wth_list])
    aexp_u = n.unique(aexp)
    for coo in ['sat', 'all']:
        for ae in aexp_u:
            sel = (aexp == ae) & (cos == coo)
            suffix = coo + '_' + ae
            p.figure(1, (10., 10.))
            DATA = n.loadtxt(listWtheta2RXS[2], unpack=True)
            p.errorbar(DATA[0], DATA[1], yerr=DATA[1] * DATA[2]
                       ** (-0.5), rasterized=True, label='2RXS', lw=3)
            for path_2_sim in wth_list[sel]:
                DATA = n.loadtxt(path_2_sim, unpack=True)
                p.errorbar(DATA[0],
                           DATA[1],
                           yerr=DATA[1] * DATA[2]**(-0.5),
                           rasterized=True,
                           label=os.path.basename(path_2_sim)[:-12])
            p.plot(DATA[0], 0.1 * DATA[0]**-0.8, label=r'$0.1\theta^{-0.8}$')
            p.xlabel(r'$\theta$ [deg]')
            p.ylabel(r'$w(\theta)$ ')
            p.xscale('log')
            p.legend()
            p.yscale('log')
            p.grid()
            p.savefig(
                os.path.join(
                    fig_dir,
                    prefix +
                    '_clustering_' +
                    suffix +
                    '.png'))
            p.clf()


wth_list = md10_wth_list
prefix = 'MD10'
fig_dir = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    'figures',
    prefix,
    'clustering_DMhalos_2RXS')
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)

plot_set(wth_list, prefix)

wth_list = md04_wth_list
prefix = 'MD04'
fig_dir = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    'figures',
    prefix,
    'clustering_DMhalos_2RXS')
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)

plot_set(wth_list, prefix)
