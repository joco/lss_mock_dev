import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
import matplotlib.pyplot as p
import numpy as n
import h5py    # HDF5 support
import os
import glob
import numpy as n
from scipy.interpolate import interp1d

import matplotlib
matplotlib.use('Agg')

cosmoMD = FlatLambdaCDM(H0=67.77 * u.km / u.s / u.Mpc, Om0=0.307115)

dir_2_clustering_2rxs = os.path.join(
    os.environ['GIT_MAKESAMPLE'],
    'data/wtheta_2RXS')
listWtheta2RXS = sorted(
    n.array(
        glob.glob(
            os.path.join(
                dir_2_clustering_2rxs,
                '*.wtheta'))))
path_2_2rxs = listWtheta2RXS[0]

md04_wth_list = n.array(
    glob.glob(
        os.path.join(
            os.environ['MD04'],
            'hlists',
            'fits',
            'clustering',
            '*.wtheta.2pcf')))
md10_wth_list = n.array(
    glob.glob(
        os.path.join(
            os.environ['MD10'],
            'hlist',
            'fits',
            'clustering',
            'MD__80_20_*.wtheta.2pcf')))


p.figure(2, (10., 15.))
DATA = n.loadtxt(listWtheta2RXS[2], unpack=True)
p.errorbar(DATA[0], DATA[1], yerr=DATA[1] * DATA[2]
           ** (-0.5), rasterized=True, label='2RXS')

for path_2_sim in md10_wth_list:
    DATA = n.loadtxt(path_2_sim, unpack=True)
    p.errorbar(DATA[0], DATA[1], yerr=DATA[1] * DATA[2]**(-0.5),
               rasterized=True, label=os.path.basename(path_2_sim)[:-12])


p.plot(DATA[0], 0.1 * DATA[0]**-0.8, label=r'$0.1\theta^{-0.8}$')
p.xlabel(r'$\theta$ [deg]')
p.ylabel(r'$w(\theta)$ ')
p.xlim((0.01, 1))
p.xscale('log')
p.legend()
p.yscale('log')
p.grid()
p.savefig(os.path.join(dir_2_clustering_2rxs, 'clustering.png'))
p.clf()
