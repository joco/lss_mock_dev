
cd clustering
# run once to generate the randoms :
# RUN DS43, or DS52 (NOT HE1)
# python lc_create_random_ra_dec.py MD04 #           # DONE !
# python lc_create_random_ra_dec.py MD10 #           # DONE !
# python lc_create_random_ra_dec.py UNIT_fA1_DIR     # DONE !
# python lc_create_random_ra_dec.py UNIT_fA2_DIR     # DONE !
# python lc_create_random_ra_dec.py UNIT_fA1i_DIR    # DONE !

# RUN DS43, or DS52 (NOT HE1)

nohup nice -n 19 python3 compute_clustering.py MD04 > compute_clustering_MD04_all.log & # 
nohup nice -n 19 python3 compute_clustering.py MD10 > compute_clustering_MD10_all.log & # 
nohup nice -n 19 python3 compute_clustering.py UNIT_fA1_DIR  > compute_clustering_UNIT_fA1_DIR_all.log & # 
nohup nice -n 19 python3 compute_clustering.py UNIT_fA2_DIR  > compute_clustering_UNIT_fA2_DIR_all.log & # 
nohup nice -n 19 python3 compute_clustering.py UNIT_fA1i_DIR > compute_clustering_UNIT_fA1i_DIR_all.log & # 

nohup nice -n 19 python3 compute_clustering_sat_rev.py MD04 > compute_clustering_MD04_sat.log & # 
nohup nice -n 19 python3 compute_clustering_sat_rev.py MD10 > compute_clustering_MD10_sat.log & # 
nohup nice -n 19 python3 compute_clustering_sat_rev.py UNIT_fA1_DIR   > compute_clustering_UNIT_fA1_DIR_sat.log & # 
nohup nice -n 19 python3 compute_clustering_sat_rev.py UNIT_fA2_DIR   > compute_clustering_UNIT_fA2_DIR_sat.log & # 
nohup nice -n 19 python3 compute_clustering_sat_rev.py UNIT_fA1i_DIR  > compute_clustering_UNIT_fA1i_DIR_sat.log & # 

