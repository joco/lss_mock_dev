import glob
import h5py
import os
import sys
from astropy.coordinates import SkyCoord
import matplotlib.pyplot as p
import matplotlib
import numpy as n
import astropy.io.fits as fits
import time
t0 = time.time()
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})

env = 'MD10'  # sys.argv[1] # 'MD04'

#ftyp = '_80_20'
#f_all = 0.8
#f_sat = 1. - f_all

ftyp = '_90_10'
f_all = 0.9
f_sat = 1.-f_all

#FX_min = 8e-14
#suffix = '_FX_8em14'

FX_min = 4e-13
suffix = '_FX_4em13'

print(env, ftyp)

#laptop = True
laptop = False
if laptop:
    CUTE_cmd = "/home/comparat/software/CUTE/CUTE/CUTE "
else:
    CUTE_cmd = "~/darksim/software/CUTE/CUTE/CUTE "


if env == 'MD04':
    test_dir = os.path.join(os.environ[env], 'hlists', 'fits')
    # halo properties
    M_bins = 10**n.arange(8, 15, 0.5)
if env == 'MD10':
    test_dir = os.path.join(os.environ[env], 'hlist', 'fits')
    # halo properties
    M_bins = 10**n.arange(11, 15, 0.5)

clustering_dir = os.path.join(os.environ[env], 'clustering')
if os.path.isdir(clustering_dir) == False:
    os.system('mkdir -p ' + clustering_dir)

fig_dir = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    'figures',
    env,
    'clustering_AGN')
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)


class Pathes:
    def __init__(self):
        self.p1 = 0


pathes = Pathes()
pathes.path_2_randoms = os.path.join(clustering_dir, 'random-ra-dec.txt')
pathes.path_2_radecz_basename = os.path.join(
    clustering_dir, env + ftyp + suffix + "_eRO_AGN")
pathes.path_2_figure = os.path.join(fig_dir, env + ftyp + suffix + "_eRO_AGN")


# loads randoms
raR_i, decR_i = n.loadtxt(pathes.path_2_randoms, unpack=True)
coords = SkyCoord(raR_i, decR_i, unit='deg', frame='icrs')
g_lat_R = coords.galactic.b.value
sel = (abs(decR_i) < 80) & (abs(g_lat_R) > 20.)
raR, decR = raR_i[sel], decR_i[sel]
print('randoms loaded', time.time() - t0)

def get_AGN(HEALPIX_ID):
# create the AGN set
	def get_data(data, frac):
		zs = data['redshift_S']
		ra = data['ra']
		dec = data['dec']
		FX = data['AGN_FX_soft']
		mvir = data['HALO_Mvir']
		RDS = n.random.random(len(dec))
		sel = (abs(dec) < 80) & (abs(data['g_lat']) > 20.) & (FX > FX_min) & (RDS < frac)
		#ra_D, dec_D, z_D, w_D = ra[sel], dec[sel], zs[sel], n.ones(N_data).astype('int')
		return ra[sel], dec[sel], zs[sel], n.ones(
			len(ra[sel])).astype('int'), mvir[sel]

	pathes.path_2_sat_data = os.path.join(os.environ[env], 'cat_AGN-MAG_sat', HEALPIX_ID.zfill(6)+".fit")
	pathes.path_2_all_data = os.path.join(os.environ[env], 'cat_AGN-MAG_all', HEALPIX_ID.zfill(6)+".fit")

	# clustering in the data
	data_sat = fits.open(pathes.path_2_sat_data)[1].data
	data_all = fits.open(pathes.path_2_all_data)[1].data

	ra_D_s, dec_D_s, z_D_s, w_D_s, mvir_D_s = get_data(data_sat, f_sat)
	ra_D_a, dec_D_a, z_D_a, w_D_a, mvir_D_a = get_data(data_all, f_all)

	ra_D = n.hstack((ra_D_a, ra_D_s))
	dec_D = n.hstack((dec_D_a, dec_D_s))
	z_D = n.hstack((z_D_a, z_D_s))
	w_D = n.hstack((w_D_a, w_D_s))
	mvir_D = n.hstack((mvir_D_a, mvir_D_s))
	print(HEALPIX_ID, len(ra_D))
	return ra_D, dec_D, z_D, w_D

ra_D, dec_D, z_D, w_D = [], [], [], []
for HEALPIX_ID in n.arange(healpy.nside2npix(8)):
	#print(HEALPIX_ID)
	out = get_AGN(HEALPIX_ID)
	ra_D.append(out[0])
	dec_D.append(out[1])
	z_D.append(out[2])
	w_D.append(out[3])

ra_D  = n.hstack( ra_D  )
dec_D = n.hstack( dec_D )
z_D   = n.hstack( z_D   )
w_D   = n.hstack( w_D   )

#p.figure(1, (10, 10))

#FX_min = 1e-14
#lab = 'FX_1em14'
#s1 = (abs(data_all['dec']) < 80) & (abs(data_all['g_lat']) > 20.) & (
    #data_all['redshift_S'] < 0.9) & (data_all['AGN_FX_soft'] > FX_min)
#p.plot(
    #data_all['redshift_S'][s1],
    #data_all['HALO_Mvir'][s1],
    #ls="None",
    #marker=',',
    #label=lab)

#FX_min = 3e-14
#lab = 'FX_3em14'
#s1 = (abs(data_all['dec']) < 80) & (abs(data_all['g_lat']) > 20.) & (
    #data_all['redshift_S'] < 0.9) & (data_all['AGN_FX_soft'] > FX_min)
#p.plot(
    #data_all['redshift_S'][s1],
    #data_all['HALO_Mvir'][s1],
    #ls="None",
    #marker=',',
    #label=lab)

#FX_min = 1e-13
#lab = 'FX_1em13'
#s1 = (abs(data_all['dec']) < 80) & (abs(data_all['g_lat']) > 20.) & (
    #data_all['redshift_S'] < 0.9) & (data_all['AGN_FX_soft'] > FX_min)
#p.plot(
    #data_all['redshift_S'][s1],
    #data_all['HALO_Mvir'][s1],
    #ls="None",
    #marker=',',
    #label=lab)


#p.legend(frameon=False, loc=0)
#p.savefig(os.path.join(fig_dir, "z-mvir-agn.png"))
#p.clf()


n.savetxt(pathes.path_2_radecz_basename +
          '.data', n.transpose([ra_D, dec_D, z_D, w_D]))

print(
    "number of data",
    len(ra_D),
    't=',
    time.time() -
    t0)
N_data = len(ra_D)
N_rds = 20 * N_data  # len(raR)
print("D,R=", N_data, N_rds, 't=', time.time() - t0)
dz = 0.005
zmin = n.min(z_D)
zmax = n.max(z_D)
# redshift histogram to mimick NZ
z_bins = n.arange(zmin, zmax + 2 * dz, dz)
nn, bb = n.histogram(z_D, bins=z_bins)
rdsz = []
for jj, (zlow, zhigh) in enumerate(zip(z_bins[:-1], z_bins[1:])):
    #print(zlow, zhigh)
    inter = n.random.uniform(low=zlow, high=zhigh, size=int(1000 * nn[jj]))
    rdsz.append(inter)

rds = n.hstack((rdsz))
n.random.shuffle(rds)
RR = rds[:N_rds]  # -dz/2.
print(
    "number of randoms",
    len(rds),
    'subsampled to',
    len(RR),
    't=',
    time.time() -
    t0)
# n.random.shuffle(raR)
# n.random.shuffle(decR)
ra_R, dec_R, z_R, w_R = raR[:N_rds], decR[:N_rds], RR, n.ones_like(
    RR).astype('int')
n.savetxt(pathes.path_2_radecz_basename +
          '.random', n.transpose([ra_R, dec_R, z_R, w_R]))

# Monopole
f = open(pathes.path_2_radecz_basename + '.monopole.ini', 'w')
f.write(
    'data_filename= ' +
    pathes.path_2_radecz_basename + '.data \n')
f.write(
    'random_filename= ' +
    pathes.path_2_radecz_basename + '.random \n')
f.write('input_format= 2 \n')
f.write(
    'output_filename= ' +
    pathes.path_2_radecz_basename + '.monopole.2pcf \n')
f.write('corr_type= monopole \n')
f.write('omega_M= 0.307 \n')
f.write('omega_L= 0.693 \n')
f.write('w= -1 \n')
f.write('log_bin= 0 \n')
f.write('dim1_max= 160. \n')
#f.write('dim1_min_logbin= 0.01 \n')
f.write('dim1_nbin= 40 \n')
f.write('dim2_max= 160. \n')
f.write('dim2_nbin= 40 \n')
f.write('dim3_min= 0.00 \n')
f.write('dim3_max= 3. \n')
f.write('dim3_nbin= 1 \n')
f.close()
os.system(CUTE_cmd + pathes.path_2_radecz_basename + suffix + '.monopole.ini')
# Angular clustering
f = open(pathes.path_2_radecz_basename + suffix + '.wtheta.ini', 'w')
f.write(
    'data_filename= ' +
    pathes.path_2_radecz_basename +'.data \n')
f.write(
    'random_filename= ' +
    pathes.path_2_radecz_basename +'.random \n')
f.write('input_format= 2 \n')
f.write(
    'output_filename= ' +
    pathes.path_2_radecz_basename +'.wtheta.2pcf \n')
f.write('corr_type= angular \n')
f.write('omega_M= 0.307 \n')
f.write('omega_L= 0.693 \n')
f.write('w= -1 \n')
f.write('log_bin= 5 \n')
f.write('dim1_min_logbin= 0.001 \n')
f.write('dim1_max= 10. \n')
f.write('dim1_nbin= 20 \n')
f.write('dim2_max= 160. \n')
f.write('dim2_nbin= 40 \n')
f.write('dim3_min= 0.00 \n')
f.write('dim3_max= 3. \n')
f.write('dim3_nbin= 1 \n')
f.write('use_pm= 0 \n')
f.write('n_pix_sph= 1000000 \n')
f.close()
os.system(CUTE_cmd + pathes.path_2_radecz_basename + suffix + '.wtheta.ini')


fig = p.figure(3, (12, 16))
# wedge plot
fig.add_subplot(3, 2, 1)

p.plot(
    ra_D,
    dec_D,
    marker=',',
    color='b',
    alpha=0.5,
    rasterized=True,
    ls='None',
    label=str(
        len(ra_D)))
p.xlabel('R.A. [deg]')
p.ylabel('Dec. [deg]')
p.legend(frameon=False, loc=0)
p.title('data')

fig.add_subplot(3, 2, 2)
p.plot(
    ra_R,
    dec_R,
    marker=',',
    color='b',
    alpha=0.1,
    rasterized=True,
    ls='None',
    label=str(
        len(ra_R)))
p.xlabel('R.A. [deg]')
p.ylabel('Dec. [deg]')
p.legend(frameon=False, loc=0)
p.title('randoms')

fig.add_subplot(3, 3, 4)
# KS-test RA
nn_R, bb, pp = p.hist(ra_R, bins=n.arange(
    361), histtype='step', cumulative=True, normed=True, lw=4)
nn_D, bb, pp = p.hist(ra_D, bins=bb, histtype='step',
                      cumulative=True, normed=True, lw=2)
KS_out = n.max(nn_R - nn_D)
p.title(str(n.round(KS_out, 4)))
p.xlabel('R.A. [deg]')
p.ylabel('cdf')
p.grid()

fig.add_subplot(3, 3, 5)
# KS-test DEC
nn_R, bb, pp = p.hist(dec_R + 90, bins=n.arange(181),
                      histtype='step', cumulative=True, normed=True, lw=4)
nn_D, bb, pp = p.hist(dec_D + 90, bins=bb, histtype='step',
                      cumulative=True, normed=True, lw=2)
KS_out = n.max(nn_R - nn_D)
p.title(str(n.round(KS_out, 4)))
p.ylabel('Dec. [deg]')
p.ylabel('cdf')
p.grid()

fig.add_subplot(3, 3, 6)
# KS test redshift
p.hist(
    z_D,
    bins=z_bins,
    histtype='step',
    cumulative=True,
    normed=True,
    rasterized=True,
    lw=4)
p.hist(
    z_R,
    bins=z_bins,
    histtype='step',
    cumulative=True,
    normed=True,
    rasterized=True,
    lw=2)
p.xlabel('redshift')
p.ylabel('cdf')
# p.xscale('log')
# p.yscale('log')
p.title('median z=' + str(n.round(n.median(z_D), 3)))
p.grid()

fig.add_subplot(3, 2, 5)
# w theta
DATA = n.loadtxt(
    pathes.path_2_radecz_basename +'.wtheta.2pcf',
    unpack=True)
p.errorbar(DATA[0], DATA[1], yerr=DATA[1] * DATA[2]**(-0.5), rasterized=True)
p.plot(DATA[0], 0.1 * DATA[0]**-0.8, label=r'$0.1\theta^{-0.8}$')
p.xlabel(r'$\theta$ [deg]')
p.ylabel(r'$w(\theta)$ ')
p.xscale('log')
p.legend()
p.title(os.path.basename(pathes.path_2_radecz_basename) )
p.yscale('log')
p.grid()

fig.add_subplot(3, 2, 6)
# w theta
DATA = n.loadtxt(
    pathes.path_2_radecz_basename + '.monopole.2pcf',
    unpack=True)
p.errorbar(DATA[0], DATA[1], yerr=DATA[1] * DATA[2]**(-0.5), rasterized=True)
p.plot(DATA[0], 10 * DATA[0]**-1.8, label=r'$10s^{-1.8}$')
p.xlabel('s [Mpc]')
p.ylabel(r'$\xi(s)$')
p.xscale('log')
#p.title(os.path.basename(pathes.path_2_radecz_basename) + suffix)
p.yscale('log')
p.grid()

p.savefig(pathes.path_2_figure + ".clustering.png")
p.clf()

print(
    pathes.path_2_radecz_basename,
    'finished',
    't=',
    time.time() -
    t0)
