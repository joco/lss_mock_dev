import numpy as n
import os
import sys

env = sys.argv[1]  # 'MD10

test_dir = os.path.join(os.environ[env], 'hlists', 'fits')

clustering_dir = os.path.join(test_dir, 'clustering')
if os.path.isdir(clustering_dir) == False:
    os.system('mkdir -p ' + clustering_dir)

# 10e6 full sky randoms :
size = 10000000
for i in range(1):
    uu = n.random.uniform(size=size)
    dec = n.arccos(1 - 2 * uu) * 180 / n.pi - 90.
    ra = n.random.uniform(size=size) * 2 * n.pi * 180 / n.pi
    n.savetxt(os.path.join(clustering_dir, 'random-ra-dec.txt'),
              n.transpose([ra, dec]), header="RA DEC")
