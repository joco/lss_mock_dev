"""
What it does
------------

Computes the stellar model

python3 008_0_stars.py

Dependencies
------------

gaia catalogs : 

/data42s/comparat/data2/firefly/gaia_cat/

$GIT_AGN_MOCK/data/logNlogS/stars-jschmitt.ascii

ls /data40s/erosim/eRASS/eRASS8_stars_simput/*.fit > stars.list
stilts tcat in=@stars.list ifmt=fits omode=out out=/data40s/erosim/eRASS/eRASS8_stars_simput/full_catalogue.fits ofmt=fits

cp /data40s/erosim/eRASS/eRASS8_stars_simput/full_catalogue.fits /data42s/comparat/firefly/mocks/2020-03/SFC/input_catalogues/
chmod ugo+rX /data42s/comparat/firefly/mocks/2020-03/SFC/input_catalogues/*

topcat -stilts plot2sky \
   xpix=1543 ypix=721 \
   projection=aitoff viewsys=galactic crowd=7.2 gridaa=true texttype=antialias fontsize=16 fontweight=bold \
   auxmap=accent auxfunc=log auxmin=0.001 auxmax=0.1 auxcrowd=3.5 \
   auxvisible=true auxlabel='density of sources per arcmin**2' \
   legend=false \
   layer=SkyDensity \
      in=/data40s/erosim/eRASS/eRASS8_stars_simput/full_catalogue.fits \
      lon=RA lat=DEC datasys=equatorial \
      level=5 combine=count-per-unit perunit=arcmin2 \
      leglabel='stars' \
      omode=out out=$GIT_AGN_MOCK/figures/stars/star_density.png

"""
import sys, glob, os, time
#import extinction
#from astropy.cosmology import FlatLambdaCDM
#import astropy.units as u
#import astropy.constants as cc
import astropy.io.fits as fits
#from astropy.table import Table, Column
#from scipy.special import erf
#from scipy.stats import norm
#from scipy.interpolate import interp2d
from scipy.interpolate import interp1d
#import h5py
import numpy as n
from astropy_healpix import healpy
print('Creates AGN mock catalogue ')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()


logS, logN = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data','logNlogS','stars-jschmitt.ascii'), unpack=True )
N_all = (10**logN) * 129600/(n.pi*142)

itp = interp1d(n.log10(N_all[::-1]), logS[::-1])
all_ids = n.arange(1,int(N_all.max()),1)
src_ids = 1e7+all_ids
all_fluxes = 10**itp( n.log10( all_ids ) )
N_Xray_stars = len(all_ids)  

gaia_dir = '/data42s/comparat/firefly/gaia_cat'
output_dir = '/data40s/erosim/eRASS/eRASS8_stars_simput'

gaia_cat_list_i = n.array( glob.glob( os.path.join(gaia_dir, '*.fits') ) )
gaia_cat_list_i.sort()
faint15 = n.array(['15' in el for el in gaia_cat_list_i ])
faint16 = n.array(['16' in el for el in gaia_cat_list_i ])
bright = (faint15==False)&(faint16==False)
gaia_cat_list = gaia_cat_list_i[bright]

all_HD = []
for el in gaia_cat_list:
	hdu = fits.open(el)
	all_HD.append(hdu[1].header['NAXIS2'])

all_gaia_stars = n.sum(all_HD)

fraction = N_Xray_stars*1.01 / all_gaia_stars

def get_ra_dec(filename, fraction):
	print(filename, fraction)
	hdu = fits.open(filename)
	s1 = (n.random.random(hdu[1].header['NAXIS2'])<fraction)
	return n.transpose([hdu[1].data['ra'][s1],  hdu[1].data['dec'][s1]])      

DATA = []	
for el in gaia_cat_list:
	DATA.append( get_ra_dec(el, fraction) )

ra_i, dec_i = n.vstack((DATA)).T
ra  = ra_i [:N_Xray_stars]
dec = dec_i[:N_Xray_stars]
HEALPIX_8 = healpy.ang2pix(8, n.pi/2. - dec*n.pi/180. , ra*n.pi/180. , nest=True)

s1 = 'spectra/stellar_spectrum_10kT_0008_100z_0000.fits' + """[SPECTRUM][#row==1]"""

for HEALPIX_8_id in n.arange(healpy.nside2npix(8)):
	"""
	Loops over healpix pixels and writes the files to path_2_eRO_catalog
	"""
	path_2_SMPT_catalog = os.path.join(output_dir, str(HEALPIX_8_id).zfill(6) + '.fit')
	print(path_2_SMPT_catalog)
	sf = (HEALPIX_8 == HEALPIX_8_id)

	N_stars_out = len(ra[sf])

	tpl = n.array([ s1 for ii in n.arange(N_stars_out) ])
	
	hdu_cols = fits.ColDefs([
		fits.Column(name="SRC_ID", format='K', unit='', array=src_ids[sf]), 
		fits.Column(name="RA", format='D', unit='deg', array=ra[sf]), 
		fits.Column(name="DEC", format='D', unit='deg', array=dec[sf]), 
		fits.Column(name="E_MIN", format='D', unit='keV', array=n.ones(N_stars_out) * 0.5), 
		fits.Column(name="E_MAX", format='D', unit='keV', array=n.ones(N_stars_out) * 2.0), 
		fits.Column(name="FLUX", format='D', unit='erg/s/cm**2', array=all_fluxes[sf]), 
		fits.Column(name="SPECTRUM", format='100A', unit='', array=tpl), 
		#fits.Column(name="n_energy_bins", format='K', unit='', array=data_n_e_b)
	])

	hdu = fits.BinTableHDU.from_columns(hdu_cols)

	hdu.name = 'SRC_CAT'
	hdu.header['HDUCLASS'] = 'HEASARC/SIMPUT'
	hdu.header['HDUCLAS1'] = 'SRC_CAT'
	hdu.header['HDUVERS'] = '1.1.0'
	hdu.header['RADESYS'] = 'FK5'
	hdu.header['EQUINOX'] = 2000.0

	outf = fits.HDUList([fits.PrimaryHDU(), hdu])  # ,  ])
	if os.path.isfile(path_2_SMPT_catalog):
		os.system("rm " + path_2_SMPT_catalog)
	outf.writeto(path_2_SMPT_catalog, overwrite=True)
	print(path_2_SMPT_catalog, 'written', time.time() - t0)
