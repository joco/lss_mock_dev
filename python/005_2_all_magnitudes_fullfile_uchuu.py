"""
What it does
------------

Add realistic magnitudes to BG, LRG and FIL (ELG deprecated)

Command to run
--------------

nohup python3 005_2_all_magnitudes_fullfile_uchuu.py 0 8     >log0  &
nohup python3 005_2_all_magnitudes_fullfile_uchuu.py 8 16    >log8  &
nohup python3 005_2_all_magnitudes_fullfile_uchuu.py 16 24   >log16 &
nohup python3 005_2_all_magnitudes_fullfile_uchuu.py 24 32   >log24 &
nohup python3 005_2_all_magnitudes_fullfile_uchuu.py 32 40   >log32 &
nohup python3 005_2_all_magnitudes_fullfile_uchuu.py 40 48   >log40 &
nohup python3 005_2_all_magnitudes_fullfile_uchuu.py 48 56   >log48 &
nohup python3 005_2_all_magnitudes_fullfile_uchuu.py 56 500  >log56 &

"""

#import matplotlib
#matplotlib.use('Agg')
#matplotlib.rcParams.update({'font.size': 14})
#import matplotlib.pyplot as p
from sklearn.neighbors import BallTree

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import extinction
import healpy
#from astropy_healpix import healpy
import sys, os, glob
import time
from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column
from scipy.optimize import curve_fit
import astropy.io.fits as fits
import h5py
import numpy as n
print('Populates light cone with galaxy SED')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

z_dir = sys.argv[1]
mean_z = int(z_dir[1])+int(z_dir[3:])/100.
print(z_dir, mean_z)
cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
h = 0.6774
L_box = 1000.0 / h
cosmo = cosmoUNIT

nl = lambda sel : len(sel.nonzero()[0])
zs = n.arange(0.0000001, 7.1, 0.001)
dm_itp = interp1d(zs, cosmo.distmod(zs).value)

max_distance = 0.1 / 3600. * n.pi / 180.
deg_to_rad = n.pi/180.

if mean_z<0.8 :
	print('uses KIDS as reference catalog')
	path_2_kids = os.path.join('/ptmp/joco/GAMA', 'G09.GAMADR4+LegacyDR9.galreference+RM.fits')
	t_ref = Table.read(path_2_kids)
	kmag = 8.9 - 2.5*n.log10(t_ref['flux_Kt'])
	keep_kids = (kmag>0) & (t_ref['z_peak']>0.01) & (t_ref['z_peak']<0.85)
	t_ref = t_ref[keep_kids]
	t_ref['kmag'] = 8.9 - 2.5*n.log10(t_ref['flux_Kt'])
	t_ref['zmag'] = 8.9 - 2.5*n.log10(t_ref['flux_Zt'])
	t_ref['rmag'] = 8.9 - 2.5*n.log10(t_ref['flux_rt'])
	t_ref['imag'] = 8.9 - 2.5*n.log10(t_ref['flux_it'])
	t_ref['gmag'] = 8.9 - 2.5*n.log10(t_ref['flux_gt'])
	t_ref['umag'] = 8.9 - 2.5*n.log10(t_ref['flux_ut'])
	#
	# K-corrected K band absolute magnitudes
	#
	KCORR_DATA = n.loadtxt( os.path.join( os.environ['GIT_AGN_MOCK'], 'data/K-corr/VISTA_Ks_kcorrections.txt'), unpack = True)
	# redshift distmod bandpass kcorr_median kcorr_16pc kcorr_84pc
	kcorr_itp = interp1d(KCORR_DATA[0], KCORR_DATA[3])
	# kmag = absmag + distmod + bandpass + kcorrection
	# absmag = appmag - distmod - kcorr
	# is properly zero-centred; or at least zero-ish; the median +/- NMAD is 0.05 +/- 0.12.
	dm_values = dm_itp(t_ref['z_peak'].data.data)
	kmag_abs = t_ref['kmag'] - ( dm_values + kcorr_itp(t_ref['z_peak'].data.data) )
	t_ref['Kmag_abs'] = kmag_abs
	#
	# rescale redshift variable
	#
	min_Z = 0.85
	max_Z = 0.0
	# rescale variables
	min_K = -28. #n.min(t_ref['Kmag_abs'])
	max_K = -15. #n.max(t_ref['Kmag_abs'])
	z_01 = (t_ref['z_peak'] - min_Z ) / ( max_Z - min_Z )
	k_01 = (t_ref['Kmag_abs'] - min_K ) / ( max_K - min_K )

if mean_z>=0.8 :
	print('uses COSMOS as reference catalog')
	path_2_COSMOS = os.path.join('/ptmp/joco/COSMOS', 'photoz_vers2.0_010312.fits')
	t = Table.read(path_2_COSMOS)
	good = (t['photoz']>0.75 )&( t['photoz']< 6. ) & ( t['K']>0 )& ( t['MK']<0 )&( t['MK']>-40 )&( t['mass_med']<14 )&( t['mass_med']>6 )
	t_ref = t[good]
	t_ref['Kmag_abs'] = t_ref['MK']
	t_ref['umag'] = t_ref['U']
	t_ref['gmag'] = t_ref['G']
	t_ref['rmag'] = t_ref['R']
	t_ref['imag'] = t_ref['I']
	t_ref['zmag'] = t_ref['Z']
	t_ref['kmag'] = t_ref['K']
	#
	# rescale redshift variable
	#
	min_Z = 0.75
	max_Z = 6.0
	# rescale variables
	min_K = -28. #n.min(t_ref['Kmag_abs'])
	max_K = -15. #n.max(t_ref['Kmag_abs'])
	z_01 = (t_ref['photoz'] - min_Z ) / ( max_Z - min_Z )
	k_01 = (t_ref['Kmag_abs'] - min_K ) / ( max_K - min_K )

print(len(t_ref),'lines in reference catalogue')



Tree_kids = BallTree(n.transpose([z_01, k_01]))

def add_kmag(t_gal):
	t_gal ['K_mag_abs'] = -99.
	z0, z1, slope, OO, zpt, scatter = n.loadtxt(os.path.join( os.environ['GIT_AGN_MOCK'], 'data', 'logMs-MK-fits', 'look-up-table-2parameters.txt'), unpack=True)
	fun = lambda x, a, b : a * x + b
	for zmin, zmax, slope_i, OO_i, zpt_i, scatter_i in zip(z0, z1, slope, OO, zpt, scatter):
		#print(zmin, zmax, p_b)
		scatter_i = 0.15
		s_gal  = (t_gal['redshift_R']>=zmin)  & (t_gal['redshift_R']<=zmax) 
		mag = lambda x : fun( x, slope_i, OO_i)
		if len(t_gal ['K_mag_abs'][s_gal])>0:
			t_gal ['K_mag_abs'][s_gal]  = mag(n.log10(t_gal ['sm'][s_gal]))
			t_gal ['K_mag_abs'][s_gal]+=norm.rvs(loc=0, scale=scatter_i, size=len(t_gal['redshift_R'][s_gal]))
	return t_gal 

def add_magnitudes_direct_match_K(t_survey, t_ref2=t_ref, Tree_kids=Tree_kids):
	print('starting direct match')
	# simulated quantities
	sim_redshift = t_survey['redshift_R']
	sim_k_mag    = t_survey['K_mag_abs']
	# rescale variables
	SIMz_01      = ( sim_redshift - min_Z ) / ( max_Z - min_Z )
	SIMk_01      = ( sim_k_mag - min_K ) / ( max_K - min_K )
	DATA = n.transpose([SIMz_01, SIMk_01])
	# search nearest neighbour in tree
	dist_out, ids_out = Tree_kids.query(DATA, k=1, return_distance = True)
	kids_ID = n.arange(len(t_ref2))
	ids = n.hstack((ids_out))
	dist_map = n.hstack(( dist_out ))
	id_to_map = kids_ID[ids]
	columns_to_add = n.array([  'umag',
								'gmag',
								'rmag',
								'imag',
								'zmag',
								'kmag' ])

	print('outputing results')
	t_out = Table()
	t_out['ID_glist']   = n.arange(len(t_survey))
	t_out['redshift_R'] = sim_redshift
	t_out['log10_sm']   = n.log10(t_survey ['sm'])
	t_out['K_mag_abs']  = sim_k_mag
	for el in columns_to_add :
		t_out[el] = t_ref2[el][id_to_map]
	t_out['distance_match'] = dist_map
	return t_out


p_2_catalogues = n.array( glob.glob( os.path.join(os.environ['UCHUU'], 'GPX8', z_dir, 'replication_*_*_*', 'glist.fits') ) )
p_2_catalogues.sort()
t0 = time.time()
print(len(p_2_catalogues), 'catalogues to loop over')
for p_2_catalogue in p_2_catalogues:
	print('='*100)
	p_2_catalogue_out = os.path.join( os.path.dirname(p_2_catalogue), 'Kmatch_mags.fits')
	print(p_2_catalogue, p_2_catalogue_out)
	#if os.path.isfile(p_2_catalogue_out)==False:
	t_survey = Table.read(p_2_catalogue)
	print('N in catalog',len(t_survey))
	t_survey = add_kmag(t_survey)
	print('K mag abs added', len(t_survey))
	t_out = add_magnitudes_direct_match_K(t_survey = t_survey, t_ref2 = t_ref, Tree_kids=Tree_kids)
	print('matched magnitudes from ref cat added', len(t_out))
	t_out.write(p_2_catalogue_out, overwrite = True)
	print(p_2_catalogue_out, 'written, t=', time.time()-t0)


