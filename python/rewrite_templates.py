import numpy as n
import sys, glob, os
dir_in = os.path.join( os.environ['HOME'], 'data/4most/templates_2021' )
dir_ou = os.path.join( os.environ['HOME'], 'data/4most/templates_2022' )
tpl_list = n.array( glob.glob ( os.path.join( dir_in, '*.fits') ) )

from astropy.table import Table
import astropy
from astropy.table import Table
from astropy.io import fits
import astropy.units as u

for p2tp in tpl_list:
    output1_tpl =  os.path.join( dir_ou, 'Q'+os.path.basename(p2tp)[1:] )
    data = Table.read(p2tp)
    hdu_cols = fits.ColDefs([
        fits.Column(name="LAMBDA", format='D', unit='Angstrom', array = data['LAMBDA'] ),
        fits.Column(name="FLUX_DENSITY", format='D', unit='erg / (Angstrom * cm * cm * s)', array = data['FLUX_DENSITY'] )
        ])

    hdu = fits.BinTableHDU.from_columns(hdu_cols)

    hdu.name = 'SPECTRUM'
    hdu.header['MAG'] = 14

    outf = fits.HDUList([fits.PrimaryHDU(), hdu])  # ,  ])

    outf.writeto(output1_tpl, overwrite=True)
    print(output1_tpl, 'written')#
