"""
Concatenates a list of files

input: 
 - file containing a list of files to concatenate
 - path to the output file
example input : 

 ls $MD04/fits/cat_AGN_all_*/000000.fit > fit_list_MD04_all_000000_AGN_.list
 - p_2_file_list = "fit_list_MD04_all_000000_AGN_.list" 
 - path_2_output = "/data17s/darksim/simulation_3/MD/MD_0.4Gpc/cat_AGN_all/000000.fit" 
 
"""
import numpy as n
from astropy.table import Table, Column, vstack, hstack
import sys, os, glob 

p_2_file_list = sys.argv[1]
path_2_output = sys.argv[2]
option = sys.argv[3]

file_list = n.loadtxt(p_2_file_list, dtype='str')
if option=="erosita":
	filter_area = lambda g_lat, g_lon, DEC : (abs(g_lat)>10 )&( g_lon>180 )&( DEC>-80 )&( DEC<=5 )
if option=="xgal":
	filter_area = lambda g_lat, g_lon, DEC : (abs(g_lat)>10 )&( DEC>-80 )&( DEC<=20 )

t = Table.read(file_list[0])
t = t [ filter_area(t['g_lat'], t['g_lon'], t['DEC']) ]
if 'MASK_BIT' in t.columns.keys():
	t.remove_column('MASK_BIT') 


for p_2_file in file_list[1:]:
	print(p_2_file)
	t1 = Table.read(p_2_file)
	t1 = t1 [ filter_area(t1['g_lat'], t1['g_lon'], t1['DEC']) ]
	if len(t1)>0:
		if 'MASK_BIT' in t1.columns.keys():
			t1.remove_column('MASK_BIT') 
		t = vstack(( t, t1))

t_out = Table(t)
t_out.write(path_2_output, overwrite=True)
