"""
HAM procedure for Uchuu between tabulated AGN files and the light cone
for jj in n.arange(15,650):
    print('python 003_1_AGN_model_uchuu_per_file.py '+str(jj))


"""
import time
t0=time.time()
import numpy as n
import os, sys, glob
import h5py
from astropy.table import Table, Column, vstack
from astropy.coordinates import SkyCoord
import healpy
from scipy.interpolate import interp1d
import astropy.io.fits as fits
from scipy.special import erf
from scipy.stats import norm
import scipy
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u

import extinction
import astropy.constants as cc
from scipy.interpolate import interp2d
from scipy.interpolate import NearestNDInterpolator
from scipy.interpolate import interp1d

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p

cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
h = 0.6774
cosmo = cosmoUNIT

# mid point linear
#0.25 0.095 0.185 0.275
#0.75 0.191 0.595 1.0
#1.25 0.138 0.569 1.0
#1.75 0.263 0.632 1.0
#2.25 0.191 0.595 1.0
#2.75 0.138 0.569 1.0
#3.5  0.138 0.569 1.0

# mid point log10
#0.25 -1.02 -0.79 -0.56
#0.75 -0.72 -0.36 0.0
#1.25 -0.86 -0.43 0.0
#1.75 -0.58 -0.29 0.0
#2.25 -0.72 -0.36 0.0
#2.75 -0.86 -0.43 0.0
#3.5  -0.86  -0.43 0.0


f_duty_1 = interp1d(n.array([0.   , 0.25,  0.75,  10.1]) ,
                10**n.array([-0.8, -0.80, -0.40, -0.40]) )

f_duty = interp1d(n.array([0., 0.75, 2., 3.5, 10.1]),
                  n.array([0.1, 0.2, 0.3, 0.3, 0.3]))

agn_data_dir = os.path.join( os.environ['GIT_AGN_MOCK'], 'data', 'agn' )
fig_dir      = os.path.join( os.environ['GIT_AGN_MOCK'], 'figures' )
DC_dir       = os.path.join( fig_dir, 'duty_cycle')
if os.path.isdir(DC_dir) == False:
    os.system('mkdir -p ' + DC_dir)

from scipy.special import erf
def f_DC(MS, MS0, sigma0, DC_MAX):
    return DC_MAX * (0.5 + 0.5 * erf((MS - MS0) / sigma0))
def f_DC02(MS, MS0, DC_MAX):
    return DC_MAX * (0.5 + 0.5 * erf((MS - MS0) / 1.2))

from scipy.optimize import curve_fit

x_M = n.arange(7,12.5, 0.01)

# DUTY CYCLE
# print("duty_cycle_AGN")

DATA_OUT = []
for z_mean in n.array([0.25, 0.75, 1.25, 1.75, 2.25, 2.75, 3.5]):
    print(z_mean)
    p.figure(2, (6, 6))
    p.axes([0.16, 0.15, 0.8, 0.8])

    dx = n.log10(0.6777**2)


    if z_mean<=0.5:
        x_41, y_41, y_41_up, y_41_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z025_LXhardgt41.ascii'), unpack=True)
        x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z025_LXhardgt42.ascii'), unpack=True)
        x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z025_LXhardgt43.ascii'), unpack=True)
        lab_bib = 'G17 z=0.25'
        y_41_up[y_41_up>=0 ] = 0
        y_42_up[y_42_up>=0 ] = 0
        y_43_up[y_43_up>=0 ] = 0
        p.fill_between(x_43+dx, y1=10**(y_43_low),  y2=10**(y_43_up), label=r' $L_X>10^{43}$erg s$^{-1}$', alpha=0.5,color='blue')
        p.fill_between(x_42+dx, y1=10**(y_42_low),  y2=10**(y_42_up), label=r'$L_X>10^{42}$erg s$^{-1}$', alpha=0.5,color='red')
        p.fill_between(x_41+dx, y1=10**(y_41_low),  y2=10**(y_41_up), label=r'$L_X>10^{41}$erg s$^{-1}$', alpha=0.5,color='black')

    if z_mean>0.5 and z_mean<1. :
        x_41, y_41, y_41_up, y_41_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z075_LXhardgt41.ascii'), unpack=True)
        x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z075_LXhardgt42.ascii'), unpack=True)
        x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z075_LXhardgt43.ascii'), unpack=True)
        x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z075_LXhardgt44.ascii'), unpack=True)
        lab_bib = 'G17 z=0.75'
        y_41_up[y_41_up>=0 ] = 0
        y_42_up[y_42_up>=0 ] = 0
        y_43_up[y_43_up>=0 ] = 0
        y_44_up[y_44_up>=0 ] = 0
        p.fill_between(x_44+dx, y1=10**(y_44_low),  y2=10**(y_44_up), label=r' $L_X>10^{44}$erg s$^{-1}$', alpha=0.5, color='magenta')
        p.fill_between(x_43+dx, y1=10**(y_43_low),  y2=10**(y_43_up), label=r'$L_X>10^{43}$erg s$^{-1}$', alpha=0.5,color='blue')
        p.fill_between(x_42+dx, y1=10**(y_42_low),  y2=10**(y_42_up), label=r'$L_X>10^{42}$erg s$^{-1}$', alpha=0.5,color='red')
        p.fill_between(x_41+dx, y1=10**(y_41_low),  y2=10**(y_41_up), label=r'$L_X>10^{41}$erg s$^{-1}$', alpha=0.5,color='black')

    if z_mean>1. and z_mean<1.5 :
        x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z125_LXhardgt42.ascii'), unpack=True)
        x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z125_LXhardgt43.ascii'), unpack=True)
        x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z125_LXhardgt44.ascii'), unpack=True)
        lab_bib = 'G17 z=1.25'
        y_42_up[y_42_up>=0 ] = 0
        y_43_up[y_43_up>=0 ] = 0
        y_44_up[y_44_up>=0 ] = 0
        p.fill_between(x_44+dx, y1=10**(y_44_low),  y2=10**(y_44_up), label=r' $L_X>10^{44}$erg s$^{-1}$', alpha=0.5, color='magenta')
        p.fill_between(x_43+dx, y1=10**(y_43_low),  y2=10**(y_43_up), label=r'$L_X>10^{43}$erg s$^{-1}$', alpha=0.5,color='blue')
        p.fill_between(x_42+dx, y1=10**(y_42_low),  y2=10**(y_42_up), label=r'$L_X>10^{42}$erg s$^{-1}$', alpha=0.5,color='red')

    if z_mean>1.5 and z_mean<2. :
        x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z175_LXhardgt42.ascii'), unpack=True)
        x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z175_LXhardgt43.ascii'), unpack=True)
        x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z175_LXhardgt44.ascii'), unpack=True)
        lab_bib = 'G17 z=1.75'
        y_42_up[y_42_up>=0 ] = 0
        y_43_up[y_43_up>=0 ] = 0
        y_44_up[y_44_up>=0 ] = 0
        p.fill_between(x_44+dx, y1=10**(y_44_low),  y2=10**(y_44_up), label=r' $L_X>10^{44}$erg s$^{-1}$', alpha=0.5, color='magenta')
        p.fill_between(x_43+dx, y1=10**(y_43_low),  y2=10**(y_43_up), label=r'$L_X>10^{43}$erg s$^{-1}$', alpha=0.5,color='blue')
        p.fill_between(x_42+dx, y1=10**(y_42_low),  y2=10**(y_42_up), label=r'$L_X>10^{42}$erg s$^{-1}$', alpha=0.5,color='red')

    if z_mean>2. and z_mean<2.5 :
        x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z225_LXhardgt42.ascii'), unpack=True)
        x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z225_LXhardgt43.ascii'), unpack=True)
        x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z225_LXhardgt44.ascii'), unpack=True)
        lab_bib = 'G17 z=2.25'
        y_42_up[y_42_up>=0 ] = 0
        y_43_up[y_43_up>=0 ] = 0
        y_44_up[y_44_up>=0 ] = 0
        p.fill_between(x_44+dx, y1=10**(y_44_low),  y2=10**(y_44_up), label=r' $L_X>10^{44}$erg s$^{-1}$', alpha=0.5, color='magenta')
        p.fill_between(x_43+dx, y1=10**(y_43_low),  y2=10**(y_43_up), label=r'$L_X>10^{43}$erg s$^{-1}$', alpha=0.5,color='blue')
        p.fill_between(x_42+dx, y1=10**(y_42_low),  y2=10**(y_42_up), label=r'$L_X>10^{42}$erg s$^{-1}$', alpha=0.5,color='red')

    if z_mean>2.5 and z_mean<3. :
        x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z275_LXhardgt42.ascii'), unpack=True)
        x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z275_LXhardgt43.ascii'), unpack=True)
        x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z275_LXhardgt44.ascii'), unpack=True)
        lab_bib = 'G17 z=2.75'
        y_42_up[y_42_up>=0 ] = 0
        y_43_up[y_43_up>=0 ] = 0
        y_44_up[y_44_up>=0 ] = 0
        p.fill_between(x_44+dx, y1=10**(y_44_low),  y2=10**(y_44_up), label=r' $L_X>10^{44}$erg s$^{-1}$', alpha=0.5, color='magenta')
        p.fill_between(x_43+dx, y1=10**(y_43_low),  y2=10**(y_43_up), label=r'$L_X>10^{43}$erg s$^{-1}$', alpha=0.5,color='blue')
        p.fill_between(x_42+dx, y1=10**(y_42_low),  y2=10**(y_42_up), label=r'$L_X>10^{42}$erg s$^{-1}$', alpha=0.5,color='red')

    if z_mean>3 :
        lab_bib = 'G17 z=3.5'
        x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z350_LXhardgt43.ascii'), unpack=True)
        x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z350_LXhardgt44.ascii'), unpack=True)
        y_43_up[y_43_up>=0 ] = 0
        y_44_up[y_44_up>=0 ] = 0
        p.fill_between(x_44+dx, y1=10**(y_44_low),  y2=10**(y_44_up), label=lab_bib+r' $L_X>10^{44}$erg s$^{-1}$', alpha=0.5, color='magenta')
        p.fill_between(x_43+dx, y1=10**(y_43_low),  y2=10**(y_43_up), label=r'$L_X>10^{43}$erg s$^{-1}$', alpha=0.5,color='blue')

    # varying sigma model
    if z_mean<1. :
        x_data = x_41+dx
        y_data = (10**(y_41_low) + 10**(y_41_up) )/2.
        y_data_err = (10**(y_41_up) - 10**(y_41_low) )/2./y_data
        out = curve_fit(f_DC, x_data, y_data, p0=(10, 0.2, 0.3), sigma=y_data_err, bounds=([8, 0.05, 0.05], [12.5, 2.5, 0.5]))
        label=str(n.round(out[0][0],3))+', '+ str(n.round(out[0][1],3))+', '+ str(n.round(out[0][2],3))
        p.plot(x_M, f_DC(x_M, out[0][0], out[0][1], out[0][2]), label=label, lw=2, color='k')
        print(41-out[0][0])
        DATA_OUT.append([z_mean, 41., out[0][0], out[0][1], out[0][2]])

    if z_mean<3. :
        x_data = x_42+dx
        y_data = (10**(y_42_low) + 10**(y_42_up) )/2.
        y_data_err = (10**(y_42_up) - 10**(y_42_low) )/2./y_data
        out = curve_fit(f_DC, x_data, y_data, p0=(10, 0.2, 0.3), sigma=y_data_err, bounds=([8, 0.05, 0.05], [12.5, 2.5, 0.5]))
        label=str(n.round(out[0][0],3))+', '+ str(n.round(out[0][1],3))+', '+ str(n.round(out[0][2],3))
        p.plot(x_M, f_DC(x_M, out[0][0], out[0][1], out[0][2]), label=label, lw=2, color='r')
        print(42-out[0][0])
        DATA_OUT.append([z_mean, 42., out[0][0], out[0][1], out[0][2]])

    x_data = x_43+dx
    y_data = (10**(y_43_low) + 10**(y_43_up) )/2.
    y_data_err = (10**(y_43_up) - 10**(y_43_low) )/2./y_data
    out = curve_fit(f_DC, x_data, y_data, p0=(10, 0.2, 0.3), sigma=y_data_err, bounds=([8, 0.05, 0.05], [12.5, 2.5, 0.5]))
    label=str(n.round(out[0][0],3))+', '+ str(n.round(out[0][1],3))+', '+ str(n.round(out[0][2],3))
    p.plot(x_M, f_DC(x_M, out[0][0], out[0][1], out[0][2]), label=label, lw=2, color='b')
    print(43-out[0][0])
    DATA_OUT.append([z_mean, 43., out[0][0], out[0][1], out[0][2]])

    if z_mean>0.5 :
        x_data = x_44+dx
        y_data = (10**(y_44_low) + 10**(y_44_up) )/2.
        y_data_err = (10**(y_44_up) - 10**(y_44_low) )/2./y_data
        out = curve_fit(f_DC, x_data, y_data, p0=(10, 0.2, 0.3), sigma=y_data_err, bounds=([8, 0.05, 0.05], [12.5, 2.5, 0.5]))
        label=str(n.round(out[0][0],3))+', '+ str(n.round(out[0][1],3))+', '+ str(n.round(out[0][2],3))
        p.plot(x_M, f_DC(x_M, out[0][0], out[0][1], out[0][2]), label=label, lw=2, color='magenta')
        print(44-out[0][0])
        DATA_OUT.append([z_mean, 44., out[0][0], out[0][1], out[0][2]])

    ### fixed sigma model
    ##if z_mean<1. :
        ##x_data = x_41+dx
        ##y_data = (10**(y_41_low) + 10**(y_41_up) )/2.
        ##y_data_err = (10**(y_41_up) - 10**(y_41_low) )/2./y_data
        ##out = curve_fit(f_DC02, x_data, y_data, p0=(10, 0.3), sigma=y_data_err)
        ##label=str(n.round(out[0][0],3))+', '+ str(n.round(out[0][1],3))
        ##p.plot(x_M, f_DC02(x_M, out[0][0], out[0][1]), label=label, lw=2, color='k', ls='dashed')
        ##print(41-out[0][0])

    ##if z_mean<3. :
        ##x_data = x_42+dx
        ##y_data = (10**(y_42_low) + 10**(y_42_up) )/2.
        ##y_data_err = (10**(y_42_up) - 10**(y_42_low) )/2./y_data
        ##out = curve_fit(f_DC02, x_data, y_data, p0=(10, 0.3), sigma=y_data_err)
        ##label=str(n.round(out[0][0],3))+', '+ str(n.round(out[0][1],3))
        ##p.plot(x_M, f_DC02(x_M, out[0][0], out[0][1]), label=label, lw=2, color='r', ls='dashed')
        ##print(42-out[0][0])

    ##x_data = x_43+dx
    ##y_data = (10**(y_43_low) + 10**(y_43_up) )/2.
    ##y_data_err = (10**(y_43_up) - 10**(y_43_low) )/2./y_data
    ##out = curve_fit(f_DC02, x_data, y_data, p0=(10, 0.3), sigma=y_data_err)
    ##label=str(n.round(out[0][0],3))+', '+ str(n.round(out[0][1],3))
    ##p.plot(x_M, f_DC02(x_M, out[0][0], out[0][1]), label=label, lw=2, color='b', ls='dashed')
    ##print(43-out[0][0])

    ##if z_mean>0.5 :
        ##x_data = x_44+dx
        ##y_data = (10**(y_44_low) + 10**(y_44_up) )/2.
        ##y_data_err = (10**(y_44_up) - 10**(y_44_low) )/2./y_data
        ##out = curve_fit(f_DC02, x_data, y_data, p0=(10, 0.3), sigma=y_data_err)
        ##label=str(n.round(out[0][0],3))+', '+ str(n.round(out[0][1],3))
        ##p.plot(x_M, f_DC02(x_M, out[0][0], out[0][1]), label=label, lw=2, color='magenta')
        ##print(44-out[0][0])


    #M_transition = LX_LIM-30.5
    S_trans = 1.5
    #z_mean = 0.25
    def f_DC_mod(MS, LX_LIM, DC_MAX, Zval):
        return DC_MAX * (0.5 + 0.5 * erf((MS - (LX_LIM-(30.4+Zval/5.))) / S_trans))

    #for LX_low in n.arange(40,45, 1):
        #DC_MAX = f_duty(z_mean)
        #p.plot(x_M, f_DC_mod(x_M, LX_low, DC_MAX, z_mean), lw=2, color='yellow', ls='solid')

    for LX_low in n.arange(38, 42, 2):
        DC_MAX = f_duty_1(z_mean)
        p.plot(x_M, f_DC_mod(x_M, LX_low, DC_MAX, z_mean), lw=2, color='orange', ls='dotted')


    p.axhline(f_duty(z_mean), ls='dashed', lw=2, color='r')
    p.axhline(f_duty_1(z_mean), ls='dotted', lw=2, color='k')


    p.xlabel(r'$\log_{10}(M^*/M_\odot)$')
    p.ylabel(r'$f_{AGN}(M^*, z\sim '+str(n.round(z_mean,2))+')$')
    p.yscale('log')
    p.ylim((1e-6, 1.1))
    p.xlim((8.0, 12.8))
    p.grid()
    p.title('Duty cycle')
    p.legend(frameon=False, loc=0, ncol=2, fontsize=10)
    p.savefig(os.path.join(DC_dir, "duty_cycle_AGN_"+str(n.round(z_mean,2))+".png"))

    p.yscale('linear')
    p.ylim((0, 1.02))
    p.savefig(os.path.join(DC_dir, "linear_duty_cycle_AGN_"+str(n.round(z_mean,2))+".png"))
    p.clf()
    print(os.path.join(DC_dir, "linear_duty_cycle_AGN_"+str(n.round(z_mean,2))+".png"), 'written')

ttt = n.transpose(DATA_OUT)
t = Table()
t['z'] = ttt[0]
t['LX']= ttt[1]
t['M0'] = ttt[2]
t['s0'] = ttt[3]
t['DC'] = ttt[4]
t.write(os.path.join(DC_dir, 'summary_fits.fits'), overwrite = True)

for z_mean in n.array([0.25, 0.75, 1.25, 1.75, 2.25, 2.75, 3.5]):
    #print(z_mean)
    p.figure(2, (6, 6))
    p.axes([0.16, 0.15, 0.8, 0.8])

    dx = n.log10(0.6777**2)


    if z_mean<=0.5:
        x_41, y_41, y_41_up, y_41_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z025_LXhardgt41.ascii'), unpack=True)
        x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z025_LXhardgt42.ascii'), unpack=True)
        x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z025_LXhardgt43.ascii'), unpack=True)
        lab_bib = 'G17 z=0.25'
        y_41_up[y_41_up>=0 ] = 0
        y_42_up[y_42_up>=0 ] = 0
        y_43_up[y_43_up>=0 ] = 0
        print(z_mean, n.round( y_41_low[n.argmax(x_41)],3), n.round((y_41_up[n.argmax(x_41)]+ y_41_low[n.argmax(x_41)])/2., 3), n.round(y_41_up[n.argmax(x_41)],3))

    if z_mean>0.5 and z_mean<1. :
        x_41, y_41, y_41_up, y_41_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z075_LXhardgt41.ascii'), unpack=True)
        x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z075_LXhardgt42.ascii'), unpack=True)
        x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z075_LXhardgt43.ascii'), unpack=True)
        x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z075_LXhardgt44.ascii'), unpack=True)
        lab_bib = 'G17 z=0.75'
        y_41_up[y_41_up>=0 ] = 0
        y_42_up[y_42_up>=0 ] = 0
        y_43_up[y_43_up>=0 ] = 0
        y_44_up[y_44_up>=0 ] = 0
        print(z_mean, n.round( y_41_low[n.argmax(x_41)],3), n.round((y_41_up[n.argmax(x_41)]+ y_41_low[n.argmax(x_41)])/2., 3), n.round(y_41_up[n.argmax(x_41)],3))

    if z_mean>1. and z_mean<1.5 :
        x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z125_LXhardgt42.ascii'), unpack=True)
        x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z125_LXhardgt43.ascii'), unpack=True)
        x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z125_LXhardgt44.ascii'), unpack=True)
        lab_bib = 'G17 z=1.25'
        y_42_up[y_42_up>=0 ] = 0
        y_43_up[y_43_up>=0 ] = 0
        y_44_up[y_44_up>=0 ] = 0
        print(z_mean, n.round( y_42_low[n.argmax(x_42)],3), n.round((y_42_up[n.argmax(x_42)]+ y_42_low[n.argmax(x_42)])/2., 3), n.round(y_42_up[n.argmax(x_42)],3))

    if z_mean>1.5 and z_mean<2. :
        x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z175_LXhardgt42.ascii'), unpack=True)
        x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z175_LXhardgt43.ascii'), unpack=True)
        x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z175_LXhardgt44.ascii'), unpack=True)
        lab_bib = 'G17 z=1.75'
        y_42_up[y_42_up>=0 ] = 0
        y_43_up[y_43_up>=0 ] = 0
        y_44_up[y_44_up>=0 ] = 0
        print(z_mean, n.round( y_42_low[n.argmax(x_42)],3), n.round((y_42_up[n.argmax(x_42)]+ y_42_low[n.argmax(x_42)])/2., 3), n.round(y_42_up[n.argmax(x_42)],3))

    if z_mean>2. and z_mean<2.5 :
        x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z225_LXhardgt42.ascii'), unpack=True)
        x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z225_LXhardgt43.ascii'), unpack=True)
        x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z225_LXhardgt44.ascii'), unpack=True)
        lab_bib = 'G17 z=2.25'
        y_42_up[y_42_up>=0 ] = 0
        y_43_up[y_43_up>=0 ] = 0
        y_44_up[y_44_up>=0 ] = 0
        print(z_mean, n.round( y_42_low[n.argmax(x_42)],3), n.round((y_42_up[n.argmax(x_42)]+ y_42_low[n.argmax(x_42)])/2., 3), n.round(y_42_up[n.argmax(x_42)],3))

    if z_mean>2.5 and z_mean<3. :
        x_42, y_42, y_42_up, y_42_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z275_LXhardgt42.ascii'), unpack=True)
        x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z275_LXhardgt43.ascii'), unpack=True)
        x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z275_LXhardgt44.ascii'), unpack=True)
        lab_bib = 'G17 z=2.75'
        y_42_up[y_42_up>=0 ] = 0
        y_43_up[y_43_up>=0 ] = 0
        y_44_up[y_44_up>=0 ] = 0
        print(z_mean, n.round( y_42_low[n.argmax(x_42)],3), n.round((y_42_up[n.argmax(x_42)]+ y_42_low[n.argmax(x_42)])/2., 3), n.round(y_42_up[n.argmax(x_42)],3))

    if z_mean>3 :
        lab_bib = 'G17 z=3.5'
        x_43, y_43, y_43_up, y_43_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z350_LXhardgt43.ascii'), unpack=True)
        x_44, y_44, y_44_up, y_44_low = n.loadtxt( os.path.join( agn_data_dir,  'duty_cycle_G17_z350_LXhardgt44.ascii'), unpack=True)
        y_43_up[y_43_up>=0 ] = 0
        y_44_up[y_44_up>=0 ] = 0
        print(z_mean, n.round( y_42_low[n.argmax(x_42)],3), n.round((y_42_up[n.argmax(x_42)]+ y_42_low[n.argmax(x_42)])/2., 3), n.round(y_42_up[n.argmax(x_42)],3))
p.clf()
