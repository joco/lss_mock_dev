"""
What it does
------------

Plots the CXB

"""
from astropy_healpix import healpy
import glob, sys
import os, time
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
from scipy.integrate import quad
from scipy.interpolate import interp1d
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p
import astropy.io.fits as fits
#import h5py
import numpy as n
print('PLOT CXB')
print('------------------------------------------------')
print('------------------------------------------------')

t0 = time.time()
env = 'MD10'

# simulation setup

if env == "MD10" or env == "MD04":
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT


p_2_Ccounts = os.path.join(os.environ['MD10'], 'CLUSTER_counts', 'simulated_photons_ccdA.fits')
p_2_Acounts = os.path.join(os.environ['MD10'], 'AGN_counts', 'simulated_photons_ccd1.fits')
p_2_eroarf = '/home/comparat/software/sixte-2.5.10/share/sixte/instruments/srg/eroarf_combined_20180613.fits'

hdu_clu = fits.open(p_2_Ccounts)
signal_C = hdu_clu[1].data['SIGNAL']
hdu_agn = fits.open(p_2_Acounts)
signal_A = hdu_agn[1].data['SIGNAL']
hdu_arf = fits.open(p_2_eroarf)

x_bins = n.hstack(( hdu_arf[1].data['ENERG_LO'], hdu_arf[1].data['ENERG_HI'][-1] ))
x_energy = (hdu_arf[1].data['ENERG_LO'] + hdu_arf[1].data['ENERG_HI'])/2.
d_kev = hdu_arf[1].data['ENERG_HI'] - hdu_arf[1].data['ENERG_LO']

itp_response = interp1d(n.hstack((0., x_energy)), n.hstack(( hdu_arf[1].data['SPECRESP'][0], hdu_arf[1].data['SPECRESP'])))
response_A = itp_response(signal_A)
response_C = itp_response(signal_C)

d_log10_kev = 0.1
x_bins = 10**n.arange(-1, 1., d_log10_kev)
d_kev = x_bins[1:] - x_bins[:-1]
x_cxb = (x_bins[1:] + x_bins[:-1])/2.
NN_A, BB = n.histogram(signal_A , bins=x_bins)
NN_A_per_cm2, BB = n.histogram(signal_A, weights=1/response_A , bins=x_bins)
poisson_err_A = NN_A**-0.5*NN_A_per_cm2

NN_C, BB = n.histogram(signal_C , bins=x_bins)
NN_C_per_cm2, BB = n.histogram(signal_C, weights=1/response_C , bins=x_bins)
poisson_err_C = NN_C**-0.5*NN_C_per_cm2

max_T = n.max(hdu_agn[1].data['TIME'])
min_T = n.min(hdu_agn[1].data['TIME'])
Delta_t = max_T - min_T

area = 4*n.pi #  129600/n.pi

CXB_A_val =  NN_A_per_cm2 * x_cxb**2 / (d_kev * area ) #* Delta_t)
CXB_C_val =  NN_C_per_cm2 * x_cxb**2 / (d_kev * area ) #* Delta_t)
#CXB_val_up = (NN_per_cm2 + poisson_err)* x_cxb**2 / (d_kev * area )
#CXB_val_low = (NN_per_cm2 - poisson_err)* x_cxb**2 / (d_kev * area )

cxb_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'CXB')
cxb_swift_bat = os.path.join(cxb_dir, 'Swift-BAT-2008.ascii')
swift_bat_08_E_keV, swift_bat_08_CXB_up, swift_bat_08_CXB_low = n.loadtxt(cxb_swift_bat, unpack=True)

chandra_2016 = os.path.join(cxb_dir, 'Chandra-2016.ascii')
chandra_16_E_keV, chandra_16_CXB_up, chandra_16_CXB_low = n.loadtxt(chandra_2016, unpack=True)

ASCA_1995 = os.path.join(cxb_dir, 'ASCA-1995.ascii')
ASCA_1995_E_keV, ASCA_1995_CXB_up, ASCA_1995_CXB_low = n.loadtxt(ASCA_1995, unpack=True)

RXTE_2003 = os.path.join(cxb_dir, 'RXTE-2003.ascii')
RXTE_2003_E_keV, RXTE_2003_CXB_up, RXTE_2003_CXB_low = n.loadtxt(RXTE_2003, unpack=True)

plot_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'agn')
p_2_figure = os.path.join(plot_dir, "CXB_AGN.png")

p.figure(1, (6, 6))
p.fill_between(swift_bat_08_E_keV, y2=swift_bat_08_CXB_up, y1=swift_bat_08_CXB_low, label='Swift-BAT 2008', alpha=0.3)
p.fill_between(chandra_16_E_keV, y2=chandra_16_CXB_up, y1=chandra_16_CXB_low, label='Chandra 2016', alpha=0.3)
p.fill_between(ASCA_1995_E_keV, y2=ASCA_1995_CXB_up, y1=ASCA_1995_CXB_low, label='ASCA 1995', alpha=0.3)
p.fill_between(RXTE_2003_E_keV, y2=RXTE_2003_CXB_up, y1=RXTE_2003_CXB_low, label='RXTE 2003', alpha=0.3)
soft_ero = ( x_cxb>0.1 ) & ( x_cxb < 9 )
p.plot(x_cxb[soft_ero], CXB_A_val[soft_ero]/100,  label='MD10 AGN')
p.plot(x_cxb[soft_ero], CXB_C_val[soft_ero]/100/7,  label='MD10 Cluster')
#p.fill_between(x_cxb[soft_ero], y2=CXB_val_up[soft_ero]/100, y1=CXB_val_low[soft_ero]/100,  label='MD10 eROSITA AGN') 
p.xlabel('Energy [keV]')
p.ylabel(r'Intensity  [$keV^2 cm^{-2} s^{-1} keV^{-1} sr^{-1}$]')
p.legend(loc=4)
p.xscale('log')
p.yscale('log')
#p.xlim((-17, -11.5))
#p.ylim((-2, 4.2))
# p.title('Mocks')
p.grid()
p.savefig(p_2_figure)
p.clf()


sys.exit()

for id_z in n.arange(len(all_zs)):
    zmin = all_zs[id_z]
    zmax = all_zs[id_z] + DZ
    z_mean = 0.5 * (zmin + zmax)
    file_list = sorted(n.array(glob.glob(os.path.join(XLF_dir,
                                                      'XLF_soft_*_' + str(n.round(zmin,
                                                                                  2)) + '_z_' + str(n.round(zmax,
                                                                                                            2)) + '.ascii'))))
    data = []
    for ff in file_list:
        data.append(n.loadtxt(ff, unpack=True))
        # DATA_XLF = n.transpose([xf, nhar, (nhar)*(1-(nharN)**(-0.5)), (nhar)*(1+(nharN)**(-0.5)), N_nh20, N_nh22, N_nh24, phi_h(10**xf,z_mean)])

p.figure(1, (6, 6))
p.axes([0.2, 0.18, 0.75, 0.75])
# Aird 2015
z_mean = (zmin + zmax) * 0.5 * n.ones_like(xf)
# mock
p.plot(xf, phi_h(10**xf, z_mean), c='cyan', ls='dashed',
       lw=2, label='Ai15')  # Aird 2-10 keV LADE')
p.fill_between(xf,
               y1=(nhar) * (1 - (nharN)**(-0.5)),
               y2=(nhar) * (1 + (nharN)**(-0.5)),
               color='g',
               alpha=0.7,
               label='Mock',
               lw=2)  # 2-10  keV')

p.plot(xf, N_nh20, label='nH<22', ls='dotted', lw=2)
p.plot(xf, N_nh22, label='22<nH<24', ls='dotted', lw=2)
p.plot(xf, N_nh24, label='24<nH', ls='dotted', lw=2)


p.xlabel(r'$\log_{10}(L^{2-10\, keV}_X/[erg/s])$')
p.ylabel(r'$\Phi$=dN/dlogL/dV [1/Mpc$^3$/dex]')
p.legend(loc=3, fontsize=14)
p.yscale('log')
p.xlim((37., 46.5))
p.ylim((1 / (2 * vol), 1e-2))
p.title(str(n.round(zmin, 2)) + "<z<" + str(n.round(zmax, 2)))
p.grid()
p.savefig(os.path.join(XLF_dir, "XLF_soft_" + baseName + ".png"))
p.clf()

DATA_XLF = n.transpose([xf,
                        nhar,
                        (nhar) * (1 - (nharN)**(-0.5)),
                        (nhar) * (1 + (nharN)**(-0.5)),
                        N_nh20,
                        N_nh22,
                        N_nh24,
                        phi_h(10**xf,
                              z_mean)])
n.savetxt(os.path.join(XLF_dir, 'XLF_soft_' + baseName + '.ascii'), DATA_XLF)


# XLF_ratio_
p.figure(1, (6, 6))
p.axes([0.18, 0.18, 0.75, 0.75])
p.plot(xf, nhar / phi_h(10**xf, z_mean), label='2-10 keV')
p.fill_between(xf, y1=1 - (nharN)**(-0.5), y2=1 + (nharN) **
               (-0.5), color='green', alpha=0.3, label='mock ERR')
p.xlabel(r'$\log_{10}(L^{2-10\, keV}_X/[erg/s])$')
p.ylabel(r'mock/model')
p.legend(frameon=False, loc=0)
p.xlim((37., 46.5))
p.ylim((0.7, 1.3))
p.title(str(n.round(zmin, 2)) + "<z<" + str(n.round(zmax, 2)))
p.grid()
p.savefig(os.path.join(XLF_dir, "XLF_ratio_" + baseName + ".png"))
p.clf()


# LSAR binning
dlogf = 0.1
fbins = n.arange(30, 38, dlogf)
xf = n.arange(30, 38, dlogf)[:-1] + dlogf / 2.

nall = n.histogram(lsar, fbins)[0] / vol / dlogf
nallN = n.histogram(lsar, fbins)[0]

zsel = (logm >= 12)
nall_12 = n.histogram(lsar, fbins)[0] / vol / dlogf
nallN_12 = n.histogram(lsar, fbins)[0]

zsel = (logm >= 11) & (logm < 12)
nall_11 = n.histogram(lsar, fbins)[0] / vol / dlogf
nallN_11 = n.histogram(lsar, fbins)[0]

zsel = (logm >= 10) & (logm < 11)
nall_10 = n.histogram(lsar, fbins)[0] / vol / dlogf
nallN_10 = n.histogram(lsar, fbins)[0]

zsel = (logm >= 9) & (logm < 10)
nall_9 = n.histogram(lsar, fbins)[0] / vol / dlogf
nallN_9 = n.histogram(lsar, fbins)[0]

p.figure(1, (6, 6))
p.axes([0.16, 0.15, 0.8, 0.8])

#nall, nallN, nall_9, nallN_9, nall_10, nallN_10, nall_11, nallN_11, nall_12, nallN_12 = get_LSAR_hist(fbins = fbins, zmin=zmin, zmax=zmax)

fun = interp1d(xf, nall)
nrm = quad(fun, xf.min(), xf.max())[0]
p.plot(xf, nall / nrm, 'k', lw=3)  # , label='mock all'

fun = interp1d(xf, nall_9)
nrm = quad(fun, xf.min(), xf.max())[0]
p.plot(xf, nall_9 / nrm, 'g', lw=2)  # , label='9-10'

fun = interp1d(xf, nall_10)
nrm = quad(fun, xf.min(), xf.max())[0]
p.plot(xf, nall_10 / nrm, 'r', lw=2)  # , label='10-11'

p.xlabel(r'$\log_{10}(\lambda_{SAR})$')
p.ylabel(r'probability distribution function')
#p.legend(frameon=False, loc=3)
p.yscale('log')
p.xlim((30., 35.5))
p.ylim((1e-4, 4))
p.title('Specific accretion rate, ' +
        str(n.round(zmin, 2)) +
        r"<z<" +
        str(n.round(zmax, 2)))
p.grid()
p.savefig(os.path.join(XLF_dir, "LSAR_hist_" + baseName + ".png"))
p.clf()


# TABULATE LOG N LOG S, fx

def get_lognlogs_replicas(fx, area):
    log_f_05_20 = n.log10(fx[fx > 0])
    out = n.histogram(log_f_05_20, bins=n.arange(-18, -8., 0.2))
    # cumulative number density per square degrees
    x_out = 0.5 * (out[1][1:] + out[1][:-1])
    N_out = n.array([n.sum(out[0][ii:]) for ii in range(len(out[0]))])
    c_out = n.array([n.sum(out[0][ii:]) for ii in range(len(out[0]))]) / area
    c_out_up = (1 + N_out**(-0.5)) * c_out
    c_out_low = (1 - N_out**(-0.5)) * c_out
    c_err = (n.log10(c_out_up) - n.log10(c_out_low)) / 2.
    return x_out, c_out, c_err


Xgal = (abs(g_lat) > 20)
out = get_lognlogs_replicas(fx[Xgal], area=area)
DATA_X = n.transpose(out)[(out[1] > 0) & (out[2] != n.inf)]
n.savetxt(
    os.path.join(
        logNlogS_dir,
        'logNlogS_soft_' +
        baseName +
        '.ascii'),
    DATA_X)


# TABULATE LOG N LOG R, mag_r


def get_lognlogr(mag_r, area):
    # cumulative number density per square degrees
    dlogf = 0.1
    fbins = n.arange(8, 40 + 2 * dlogf, dlogf)
    #xf = fbins[:-1]+dlogf/2.
    out = n.histogram(mag_r, bins=fbins)
    x_out = 0.5 * (out[1][1:] + out[1][:-1])
    # n.array([n.sum(out[0][ii:]) for ii in range(len(out[0])) ])
    N_out = n.cumsum(out[0])
    # n.array([n.sum(out[0][ii:]) for ii in range(len(out[0])) ]) / area
    c_out = N_out / area
    c_out_up = (1 + N_out**(-0.5)) * c_out
    c_out_low = (1 - N_out**(-0.5)) * c_out
    c_err = (n.log10(c_out_up) - n.log10(c_out_low)) / 2.
    return x_out, c_out, c_err


out = get_lognlogr(mag_r[Xgal], area=area)
DATA_R = n.transpose(out)[(out[1] > 0) & (out[2] != n.inf)]
n.savetxt(
    os.path.join(
        logNlogR_dir,
        'logNlogR_optical_' +
        baseName +
        '.ascii'),
    DATA_R)


sys.exit()

#print('opens galaxy file ', time.time() - t0)
f3 = fits.open(path_2_galaxy_file)
mass = f3[1].data['SMHMR_mass']  # log of the stellar mass
f3.close()

f_duty = interp1d(n.array([0., 0.75, 2., 3.5, 10.1]), n.array([0.1, 0.2, 0.3, 0.3, 0.3]))

dlogM = 0.1
bins = n.arange(8, 13, dlogM)
x_SMF = (bins[1:] + bins[:-1]) * 0.5
N_gal = n.histogram(mass, bins=bins)[0]

p.figure(2, (6, 6))
p.axes([0.16, 0.15, 0.8, 0.8])

p.axhline(f_duty(z_mean[0]), ls='dashed', lw=2)

N_agn_a = n.histogram(logm, bins=bins)[0]
y = N_agn_a * 1. / N_gal  # *dc_val
yerr = y * N_agn_a**(-0.5)  # *dc_val
p.errorbar(x_SMF, y, yerr=yerr, color='grey', label='all')

tsel = (lx > 41)
N_agn_41 = n.histogram(logm[tsel], bins=bins)[0]
y = N_agn_41 * 1. / N_gal  # *dc_val
yerr = y * N_agn_41**(-0.5)  # *dc_val
p.errorbar(x_SMF, y, yerr=yerr, color='black', label=r'L$_X>10^{41}$')

tsel = (lx > 42)
N_agn_42 = n.histogram(logm[tsel], bins=bins)[0]
y = N_agn_42 * 1. / N_gal  # *dc_val
yerr = y * N_agn_42**(-0.5)  # *dc_val
p.errorbar(x_SMF, y, yerr=yerr, color='red', label=r'L$_X>10^{42}$')

tsel = (lx > 43)
N_agn_43 = n.histogram(logm[tsel], bins=bins)[0]
y = N_agn_43 * 1. / N_gal  # *dc_val
yerr = y * N_agn_43**(-0.5)  # *dc_val
p.errorbar(x_SMF, y, yerr=yerr, color='blue', label=r'L$_X>10^{43}$')

tsel = (lx > 44)
N_agn_44 = n.histogram(logm[tsel], bins=bins)[0]
y = N_agn_44 * 1. / N_gal  # *dc_val
yerr = y * N_agn_44**(-0.5)  # *dc_val
p.errorbar(x_SMF, y, yerr=yerr, color='magenta', label=r'L$_X>10^{44}$')

p.xlabel(r'$\log_{10}(M^*/M_\odot)$')
p.ylabel(r'$f_{AGN}(M^*, ' + str(n.round(zmin, 2)) +
         r"<z<" + str(n.round(zmax, 2)) + r')$')
p.yscale('log')
p.ylim((5e-5, 0.4))
p.xlim((9.5, 12.))
p.grid()
p.title('Duty cycle')  # , '+str(n.round(zmin,2))+"<z<"+str(n.round(zmax,2)))
#p.legend( loc='upper center', bbox_to_anchor=(0.5, 1.05), ncol=2, fancybox=True, title=str(n.round(zmin,2))+"<z<"+str(n.round(zmax,2)) )
p.legend(frameon=False, loc=0)
#p.legend( loc=8, ncol=2, fancybox=True, title=str(n.round(zmin,2))+"<z<"+str(n.round(zmax,2)) )
p.savefig(os.path.join(fig_dir, "duty_cycle_AGN_" + baseName + ".png"))
p.clf()


p.figure(1, (6, 6))
p.axes([0.17, 0.15, 0.73, 0.73])

p.plot(x_SMF, N_gal / (vol * dlogM), color='green', label='all galaxies')
p.plot(x_SMF, N_agn_a / (vol * dlogM), color='grey', label='all AGN')
p.plot(x_SMF, N_agn_41 / (vol * dlogM), color='black', label=r'L$_X>10^{41}$')
p.plot(x_SMF, N_agn_42 / (vol * dlogM), color='red', label=r'L$_X>10^{42}$')
p.plot(x_SMF, N_agn_43 / (vol * dlogM), color='blue', label=r'L$_X>10^{43}$')
p.plot(x_SMF, N_agn_44 / (vol * dlogM),
       color='magenta', label=r'L$_X>10^{44}$')

p.xlabel(r'$\log_{10}(M^*/[M_\odot])$')
p.ylabel(r'$\Phi$=dN/dlogL/dV [1/Mpc$^3$/dex]')
p.legend(frameon=False, loc=0, fontsize=12)
p.yscale('log')
p.xlim((9, 12.5))
p.ylim((1e-8, 1e-1))
p.title('Stellar mass function, ' +
        str(n.round(zmin, 2)) +
        "<z<" +
        str(n.round(zmax, 2)))
p.grid()
p.savefig(os.path.join(fig_dir, "SMF_AGN_" + baseName + ".png"))
p.clf()


p.figure(1, (6, 6))
p.axes([0.17, 0.15, 0.73, 0.73])

p.plot(logm, lx, 'k,', label='all AGNs')

p.xlabel(r'$\log_{10}(M^*/[M_\odot])$')
p.ylabel(r'$\log_{10}(L^{2-10\, keV}_X/[erg/s])$')
p.legend(frameon=False, loc=0, fontsize=12)
# p.yscale('log')
# p.xlim((9,12.5))
# p.ylim((1e-8,1e-1))
p.title('LX-stellar mass, ' +
        str(n.round(zmin, 2)) +
        "<z<" +
        str(n.round(zmax, 2)))
p.grid()
p.savefig(os.path.join(fig_dir, "LX_MASS_" + baseName + ".png"))
p.clf()
