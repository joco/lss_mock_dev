"""
Merges into a single fits catalog containing the 4FS input columns.


"""
import glob
import sys
import healpy
import os
from scipy.special import gammainc  # , gamma,  gammaincinv, gammaincc
from scipy.stats import scoreatpercentile
import pandas as pd  # external package
from scipy.special import erf
from astropy.coordinates import SkyCoord
import astropy.constants as cc
import astropy.io.fits as fits
from astropy.table import Table, Column, vstack
import astropy.units as u
import numpy as n
import extinction
print('CREATES 4FS FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')

root_dir = '/data42s/comparat/firefly/mocks/2021-04/QMOST'
dir_2_QSO  = os.path.join(root_dir, 'S8_QSO_w_contamination.fits.gz'     )
dir_2_LyA  = os.path.join(root_dir, 'S8_LyA.fits.gz'     )
dir_2__BG  = os.path.join(root_dir, 'S8_BG.fits.gz'      )
dir_2_LRG  = os.path.join(root_dir, 'S8_LRG.fits.gz'     )

t_QSO  = Table.read(  dir_2_QSO  )
t_LyA  = Table.read(  dir_2_LyA  )
t__BG  = Table.read(  dir_2__BG  )
t_LRG  = Table.read(  dir_2_LRG  )
# downsamples to the right total amount
sLya = (n.random.random(len(t_LyA)) < (7500*50.+1)/len(t_LyA))
t_LyA = t_LyA[sLya]

nl = lambda sel : len(sel.nonzero()[0])
path_2_output = os.path.join(root_dir, 'S8_4MOST_03May21.fits.gz')


t_QSO.remove_columns(['target_bit','dL_cm','galactic_NH','galaxy_stellar_mass','HALO_Mvir','HALO_pid','LX_soft','FX_soft','LX_hard','FX_hard','agn_type','logNH','random','g_lat','g_lon','ecl_lat','MASK_BIT'])
t_LyA.remove_columns(['target_bit','dL_cm','galactic_NH','galaxy_stellar_mass','HALO_Mvir','HALO_pid','LX_soft','FX_soft','LX_hard','FX_hard','agn_type','logNH','random','g_lat','g_lon','ecl_lat','MASK_BIT'])
t_QSO['CADENCE']=0
t_LyA['CADENCE']=0

t__BG.remove_columns(['g_lat','g_lon','ecl_lat','ecl_lon','redshift_R','redshift_S','dL','nH','ebv','galaxy_SMHMR_mass','galaxy_star_formation_rate','galaxy_is_quiescent','galaxy_LX_hard','galaxy_mag_abs_r','galaxy_mag_r','HALO_id','HALO_pid','HALO_Mvir','HALO_Rvir','HALO_rs','HALO_scale_of_last_MM','HALO_vmax','HALO_x','HALO_y','HALO_z','HALO_vx','HALO_vy','HALO_vz','HALO_M200c','HALO_M500c','HALO_Xoff','HALO_b_to_a_500c','HALO_c_to_a_500c','HALO_Acc_Rate_1_Tdyn','K_mag_abs','CENTRAL_Mvir','rtot','rfib','ug','gr','ri','iz','zy','yj','jh','hks','lsdr9_TYPE','lsdr9_RA','lsdr9_DEC','lsdr9_SHAPE_R','lsdr9_SHAPE_E1','lsdr9_SHAPE_E2','lsdr9_SERSIC','lsdr9_MASKBITS','MASK_BIT'])
t_LRG.remove_columns(['g_lat','g_lon','ecl_lat','ecl_lon','redshift_R','redshift_S','dL','nH','ebv','galaxy_SMHMR_mass','galaxy_star_formation_rate','galaxy_is_quiescent','galaxy_LX_hard','galaxy_mag_abs_r','galaxy_mag_r','HALO_id','HALO_pid','HALO_Mvir','HALO_Rvir','HALO_rs','HALO_scale_of_last_MM','HALO_vmax','HALO_x','HALO_y','HALO_z','HALO_vx','HALO_vy','HALO_vz','HALO_M200c','HALO_M500c','HALO_Xoff','HALO_b_to_a_500c','HALO_c_to_a_500c','HALO_Acc_Rate_1_Tdyn','K_mag_abs','CENTRAL_Mvir','rtot','rfib','ug','gr','ri','iz','zy','yj','jh','hks','lsdr9_TYPE','lsdr9_RA','lsdr9_DEC','lsdr9_SHAPE_R','lsdr9_SHAPE_E1','lsdr9_SHAPE_E2','lsdr9_SERSIC','lsdr9_MASKBITS','MASK_BIT'])

table_S8 = vstack((t__BG, t_LRG, t_QSO, t_LyA))

table_S8['DATE_EARLIEST'][:] = 0.
table_S8['DATE_LATEST'][:] = 0.
table_S8['CADENCE'][:]=0

good_templates = n.array([ table_S8['TEMPLATE'][ii][:5] == '4most' for ii in n.arange(len(table_S8)) ]) 
print( nl(good_templates), len(table_S8) )
table_S8 = table_S8[good_templates]
table_S8.write (path_2_output , overwrite=True)


