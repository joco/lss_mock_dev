"""
What it does
------------

Add realistic magnitudes to BG, LRG and FIL (ELG deprecated)

Command to run
--------------

python3 005_2_all_magnitudes.py environmentVAR 

"""
from sklearn.neighbors import BallTree
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import extinction

from astropy_healpix import healpy
import sys
import os
import time
from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column
from scipy.optimize import curve_fit
import astropy.io.fits as fits
import h5py
import numpy as n
print('CREATES FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

env =  sys.argv[1]  # 'UNIT_fA1i_DIR'
i0 = int( sys.argv[2] )
i1 = int( sys.argv[3] )
option = sys.argv[4]

if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

nl = lambda sel : len(sel.nonzero()[0])
zs = n.arange(0.0000001, 7.1, 0.001)
dm_itp = interp1d(zs, cosmo.distmod(zs).value)

root_dir = os.path.join(os.environ[env])

deg_to_rad = n.pi/180.
max_distance = 0.1 / 3600. * deg_to_rad

# Match to BCG and GAL catalogues 
print('load and match to BCG and GAL catalogues')
path_2_out_GAL = os.path.join(root_dir, 'S5_CGAL.fits')
t_GAL = Table.read(path_2_out_GAL)
t_GAL['HEALPIX_8'] = healpy.ang2pix(8, n.pi/2. - t_GAL['DEC']*n.pi/180. , t_GAL['RA']*n.pi/180. , nest=True)
x = ((t_GAL['HALO_x']-t_GAL['HOST_HALO_x'])**2. + (t_GAL['HALO_y']-t_GAL['HOST_HALO_y'])**2. + (t_GAL['HALO_z']-t_GAL['HOST_HALO_z'])**2.)**0.5
t_BCG = t_GAL[(x==0)]
# only consider quiescent galaxies in the clusters
t_GAL = t_GAL[t_GAL['is_quiescent']]

# changes galaxies at 5xR200c
t_CHA =  Table.read( os.path.join(root_dir, 'CHANCES_gals.fits') )
t_CHA['HEALPIX_8'] = healpy.ang2pix(8, n.pi/2. - t_CHA['DEC']*n.pi/180. , t_CHA['RA']*n.pi/180. , nest=True)

coord_GAL = deg_to_rad * n.transpose([t_GAL['DEC'], t_GAL['RA'] ])
coord_BCG = deg_to_rad * n.transpose([t_BCG['DEC'], t_BCG['RA'] ])
coord_CHA = deg_to_rad * n.transpose([t_CHA['DEC'], t_CHA['RA'] ])


# Match to AGN catalogues 
print('load and match to AGN catalogues')
agn = Table.read('/data42s/comparat/firefly/mocks/2021-04/QMOST/S6_4MOST_ALL_SNR3_IR215.fit.gz')
agn['HEALPIX_8'] = healpy.ang2pix(8, n.pi/2. - agn['DEC']*n.pi/180. , agn['RA']*n.pi/180. , nest=True)
qso = Table.read('/data42s/comparat/firefly/mocks/2021-04/QMOST/QSO_4MOST.fits.gz')
qso['HEALPIX_8'] = healpy.ang2pix(8, n.pi/2. - qso['DEC']*n.pi/180. , qso['RA']*n.pi/180. , nest=True)
coord_AGN = deg_to_rad * n.transpose([agn['DEC'], agn['RA'] ])
coord_QSO = deg_to_rad * n.transpose([qso['DEC'], qso['RA'] ])


uniq_HPX_agn = n.unique(agn['HEALPIX_8'])
uniq_HPX_qso = n.unique(qso['HEALPIX_8'])
uniq_HPX_GAL = n.unique(t_GAL['HEALPIX_8'])
uniq_HPX_BCG = n.unique(t_BCG['HEALPIX_8'])
uniq_HPX_CHA = n.unique(t_CHA['HEALPIX_8'])

uniq_HPX = n.unique( n.hstack(( uniq_HPX_agn,
            uniq_HPX_qso,
            uniq_HPX_GAL,
            uniq_HPX_BCG,
            uniq_HPX_CHA)) )


path_2_kids = os.path.join(os.environ['UNIT_fA1i_DIR'], 'G09.GAMADR4+LegacyDR9.galreference+RM_Kabs_Iabs.fits')
t_kids = Table.read(path_2_kids)
kmag = 8.9 - 2.5*n.log10(t_kids['flux_Kt'])
zmag = 8.9 - 2.5*n.log10(t_kids['flux_Zt'])
rmag = 8.9 - 2.5*n.log10(t_kids['flux_rt'])
imag = 8.9 - 2.5*n.log10(t_kids['flux_it'])
keep_kids = (t_kids['RM_p_mem']>0) & (kmag>0) & (t_kids['z_peak']>0.01) & (t_kids['z_peak']<1.99) & (zmag>0) & (rmag>0)  & (imag>0)
t_kids = t_kids[keep_kids]
kmag = 8.9 - 2.5*n.log10(t_kids['flux_Kt'])
zmag = 8.9 - 2.5*n.log10(t_kids['flux_Zt'])
rmag = 8.9 - 2.5*n.log10(t_kids['flux_rt'])
imag = 8.9 - 2.5*n.log10(t_kids['flux_it'])

KCORR_DATA = n.loadtxt( os.path.join( os.environ['GIT_AGN_MOCK'], 'data/K-corr/VISTA_Ks_kcorrections.txt'), unpack = True)
# redshift distmod bandpass kcorr_median kcorr_16pc kcorr_84pc
#bandpass_itp = interp1d(KCORR_DATA[0], KCORR_DATA[2])
kcorr_itp = interp1d(KCORR_DATA[0], KCORR_DATA[3])
#kmag = absmag + distmod + bandpass + kcorrection
# absmag = appmag - distmod - kcorr
# is properly zero-centred; or at least zero-ish; the median +/- NMAD is 0.05 +/- 0.12.

dm_values = dm_itp(t_kids['z_peak'].data.data)
#kmag_abs = kmag - ( dm_values + bandpass_itp(t_kids['z_peak']) + kcorr_itp(t_kids['z_peak']) )
kmag_abs = kmag - ( dm_values + kcorr_itp(t_kids['z_peak'].data.data) )

#kmag_abs = 8.7 - 2.5*n.log10(t_kids['flux_Kt']) - dm_values
t_kids['Kmag_abs'] = kmag_abs
t_kids['dist_mod'] = dm_values

KCORR_DATA = n.loadtxt( os.path.join( os.environ['GIT_AGN_MOCK'], 'data/K-corr/KiDS_i_kcorrections.txt'), unpack = True)
# redshift distmod bandpass kcorr_median kcorr_16pc kcorr_84pc
#bandpass_itp = interp1d(KCORR_DATA[0], KCORR_DATA[2])
kcorr_itp_i = interp1d(KCORR_DATA[0], KCORR_DATA[3])
#kmag = absmag + distmod + bandpass + kcorrection
# absmag = appmag - distmod - kcorr
# is properly zero-centred; or at least zero-ish; the median +/- NMAD is 0.05 +/- 0.12.
imag_abs = imag - ( dm_values + kcorr_itp_i(t_kids['z_peak'].data.data) )

t_kids['Imag_abs'] = imag_abs

#t_kids.write(os.path.join(os.environ['HOME'], 'data', 'UNIT_fA1i', 'G09.GAMADR4+LegacyDR9.galreference+RM_Kabs_Iabs.fits'), overwrite = True)
#t_kids.write(os.path.join(os.environ['UNIT_fA1i_DIR'], 'G09.GAMADR4+LegacyDR9.galreference+RM_Kabs_Iabs.fits'), overwrite = True)
t_kids = t_kids[ ( t_kids['Kmag_abs'] < -20 ) ]


# rescale variables
min_Z = 1.5 # n.min(t_kids['z_peak'])
max_Z = 0.0 # n.max(t_kids['z_peak'])
min_K = -26. #n.min(t_kids['Kmag_abs'])
max_K = -20. #n.max(t_kids['Kmag_abs'])

z_01 = (t_kids['z_peak'] - min_Z ) / ( max_Z - min_Z )

k_01 = (t_kids['Kmag_abs'] - min_K ) / ( max_K - min_K )
Tree_kids = BallTree(n.transpose([z_01, k_01]))

i_01 = (t_kids['Imag_abs'] - min_K ) / ( max_K - min_K )
Tree_kids_I = BallTree(n.transpose([z_01, i_01]))
print(len(t_kids),'lines in KIDS catalogue')

sub_survey_names = n.array([ 'BG', 'LRG', 'ELG', 'QSO', 'LyA', 'filament_GAL'])

N_subsurvey = {'BG':1, 'filament_GAL':3, 'LRG':2, 'ELG':3, 'QSO':4, 'LyA':5}
priority_values = {'BG':100, 'filament_GAL':80, 'LRG':99, 'ELG':80, 'QSO':97, 'LyA':98}

# reassigns templates correctly
z_all = n.hstack(( 0., n.arange(0.3, 3., 0.2), 3.5, 4.5, 6. ))
zmins = z_all[:-1]
zmaxs = z_all[1:]

dir_2_OUT = os.path.join(root_dir, "cat_SHAM_ALL")
dir_2_IN = os.path.join(root_dir, "cat_GALAXY_all")


def add_REDMAPPER_direct_match_K(t_survey, t_kids2=t_kids, Tree_kids=Tree_kids):
	# simulated quantities
	sim_redshift = t_survey['redshift_R']
	sim_k_mag    = t_survey['K_mag_abs'] # +dm_itp(t_survey['redshift_R'])
	# rescale variables
	SIMz_01      = ( sim_redshift - min_Z ) / ( max_Z - min_Z )
	SIMk_01      = ( sim_k_mag - min_K ) / ( max_K - min_K )
	DATA = n.transpose([SIMz_01, SIMk_01])
	# search nearest neighbour in tree
	dist_out, ids_out = Tree_kids.query(DATA, k=1, return_distance = True)
	kids_ID = n.arange(len(t_kids2))
	ids = n.hstack((ids_out))
	dist_map = n.hstack(( dist_out ))
	id_to_map = kids_ID[ids]
	columns_to_add = n.array(['CATAID', 'z_peak', 'Kmag_abs', 'Imag_abs', 'dist_mod', 'flux_FUVt',
								#'flux_err_FUVt',
								'flux_NUVt',
								#'flux_err_NUVt',
								'flux_ut',
								#'flux_err_ut',
								'flux_gt',
								#'flux_err_gt',
								'flux_rt',
								#'flux_err_rt',
								'flux_it',
								#'flux_err_it',
								'flux_Zt',
								#'flux_err_Zt',
								'flux_Yt',
								#'flux_err_Yt',
								'flux_Jt',
								#'flux_err_Jt',
								'flux_Ht',
								#'flux_err_Ht',
								'flux_Kt',
								#'flux_err_Kt',
								'flux_W1t',
								#'flux_err_W1t',
								'flux_W2t',
								#'flux_err_W2t'
								 'FIBERFLUX_R'
								, 'SHAPE_R'
								, 'SHAPE_E1'
								, 'SHAPE_E2'
								, 'SERSIC'
								, 'MASKBITS'])
								#, 'RM_p_mem', 'RM_cg'])
	print('outputing results')
	for el in columns_to_add :
		t_survey[el] = t_kids2[el][id_to_map]
	t_survey['distance_match'] = dist_map
	return t_survey #.write(path_2_out)


def make_table(path_2_out_K):
    t0 = time.time()
    print(path_2_out_K)
    # full table
    t_survey = Table.read(path_2_out_K)
    coord_SV = deg_to_rad * n.transpose([t_survey['DEC'], t_survey['RA'] ])
    # First deal with AGN and QSO
    # makes the trees
    t_survey['is_S6AGN'] = n.zeros(len(t_survey))==-1
    t_survey['is_S8QSO'] =  n.zeros(len(t_survey))==-1
    t_survey['is_S5GAL'] = n.zeros(len(t_survey))==-1
    t_survey['is_S5BCG'] = n.zeros(len(t_survey))==-1
    t_survey['is_CHANCES'] = n.zeros(len(t_survey))==-1
    if HEALPIX_id in uniq_HPX_agn:
        Tree_AGN = BallTree(coord_AGN[agn['HEALPIX_8']==HEALPIX_id], metric='haversine')
        dist_agn, idx_agn = Tree_AGN.query(coord_SV, return_distance=True)
        match_agn = idx_agn[ ( dist_agn < max_distance ) ]
        match_agn_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_agn )) < max_distance ) ]
        print(len(match_agn_d1_position), 'AGN matches', time.time()-t0)
        t_survey['is_S6AGN'] = ( n.hstack(( dist_agn )) < max_distance )

    if HEALPIX_id in uniq_HPX_qso:
        Tree_QSO = BallTree(coord_QSO[qso['HEALPIX_8']==HEALPIX_id], metric='haversine')
        dist_qso, idx_qso = Tree_QSO.query(coord_SV, return_distance=True)
        match_qso = idx_qso[ ( dist_qso < max_distance ) ]
        match_qso_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_qso )) < max_distance ) ]
        print(len(match_qso_d1_position), 'QSO matches', time.time()-t0)
        t_survey['is_S8QSO'] = ( n.hstack(( dist_qso )) < max_distance )

    if HEALPIX_id in uniq_HPX_GAL:
        Tree_GAL = BallTree(coord_GAL[t_GAL['HEALPIX_8']==HEALPIX_id], metric='haversine')
        dist_gal, idx_gal = Tree_GAL.query(coord_SV, return_distance=True)
        match_gal = idx_gal[ ( dist_gal < max_distance ) ]
        match_gal_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_gal )) < max_distance ) ]
        print(len(match_gal_d1_position), 'gal matches', time.time()-t0)
        t_survey['is_S5GAL'] = ( n.hstack(( dist_gal )) < max_distance )
        if len(t_survey[t_survey['is_S5GAL']])>=1:
            t_out = add_REDMAPPER_direct_match_K(t_survey = t_survey[t_survey['is_S5GAL']], t_kids2 = t_kids, Tree_kids=Tree_kids)
            t_survey[t_survey['is_S5GAL']] = t_out
            if option=='C':
                t_survey['U'][t_survey['is_S5GAL']] =  8.9 - 2.5 * n.log10( t_out['flux_ut'] )
                t_survey['G'][t_survey['is_S5GAL']] =  8.9 - 2.5 * n.log10( t_out['flux_gt'] )
                t_survey['R'][t_survey['is_S5GAL']] =  8.9 - 2.5 * n.log10( t_out['flux_rt'] )
                t_survey['I'][t_survey['is_S5GAL']] =  8.9 - 2.5 * n.log10( t_out['flux_it'] )
                t_survey['Z'][t_survey['is_S5GAL']] =  8.9 - 2.5 * n.log10( t_out['flux_Zt'] )
                t_survey['J'][t_survey['is_S5GAL']] =  8.9 - 2.5 * n.log10( t_out['flux_Jt'] )
                t_survey['K'][t_survey['is_S5GAL']] =  8.9 - 2.5 * n.log10( t_out['flux_Kt'] )
                t_survey['I1'][t_survey['is_S5GAL']] =  8.9 - 2.5 * n.log10( t_out['flux_W1t'])
                t_survey['I2'][t_survey['is_S5GAL']] =  8.9 - 2.5 * n.log10( t_out['flux_W2t'])

    if HEALPIX_id in uniq_HPX_BCG:
        Tree_BCG = BallTree(coord_BCG[t_BCG['HEALPIX_8']==HEALPIX_id], metric='haversine')
        dist_bcg, idx_bcg = Tree_BCG.query(coord_SV, return_distance=True)
        match_bcg = idx_bcg[ ( dist_bcg < max_distance ) ]
        match_bcg_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_bcg )) < max_distance ) ]
        print(len(match_bcg_d1_position), 'bcg matches', time.time()-t0)
        t_survey['is_S5BCG'] = ( n.hstack(( dist_bcg )) < max_distance )
        if len(t_survey[t_survey['is_S5BCG']])>=1:
            t_out = add_REDMAPPER_direct_match_K(t_survey = t_survey[t_survey['is_S5BCG']], t_kids2 = t_kids, Tree_kids=Tree_kids)
            t_survey[t_survey['is_S5BCG']] = t_out
            if option=='C':
                t_survey['U'][t_survey['is_S5BCG']] =  8.9 - 2.5 * n.log10( t_out['flux_ut'] )
                t_survey['G'][t_survey['is_S5BCG']] =  8.9 - 2.5 * n.log10( t_out['flux_gt'] )
                t_survey['R'][t_survey['is_S5BCG']] =  8.9 - 2.5 * n.log10( t_out['flux_rt'] )
                t_survey['I'][t_survey['is_S5BCG']] =  8.9 - 2.5 * n.log10( t_out['flux_it'] )
                t_survey['Z'][t_survey['is_S5BCG']] =  8.9 - 2.5 * n.log10( t_out['flux_Zt'] )
                t_survey['J'][t_survey['is_S5BCG']] =  8.9 - 2.5 * n.log10( t_out['flux_Jt'] )
                t_survey['K'][t_survey['is_S5BCG']] =  8.9 - 2.5 * n.log10( t_out['flux_Kt'] )
                t_survey['I1'][t_survey['is_S5BCG']] =  8.9 - 2.5 * n.log10( t_out['flux_W1t'])
                t_survey['I2'][t_survey['is_S5BCG']] =  8.9 - 2.5 * n.log10( t_out['flux_W2t'])


    if HEALPIX_id in uniq_HPX_CHA:
        Tree_CHA = BallTree(coord_CHA[t_CHA['HEALPIX_8']==HEALPIX_id], metric='haversine')
        dist_cha, idx_cha = Tree_CHA.query(coord_SV, return_distance=True)
        match_cha = idx_cha[ ( dist_cha < max_distance ) ]
        match_cha_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_cha )) < max_distance ) ]
        print(len(match_cha_d1_position), 'cha matches', time.time()-t0)
        t_survey['is_CHANCES'] = ( n.hstack(( dist_cha )) < max_distance )


    print('over writes file', time.time()-t0)
    t_survey.write(path_2_out_K, overwrite = True)
    print(path_2_out_K, 'written', time.time()-t0)

for HEALPIX_id in n.arange(768)[i0:i1]:
    print(HEALPIX_id, time.time()-t0)
    if option=="K":
        path_2_out_K = os.path.join(dir_2_OUT, 'Kmatch_'  + str(HEALPIX_id).zfill(6) + '.fit')
        if os.path.isfile(path_2_out_K) :
            if 'is_S5BCG' in fits.open(path_2_out_K)[1].data.columns.names :
                print(path_2_out_K, 'already done')
            else:
                make_table(path_2_out_K)
        else:
            make_table(path_2_out_K)

    if option=="C":
        path_2_out_K = os.path.join(dir_2_OUT, 'Kmatch_'  + str(HEALPIX_id).zfill(6) + '_C.fit')
        if os.path.isfile(path_2_out_K) :
            if 'is_S5BCG' in fits.open(path_2_out_K)[1].data.columns.names :
                print(path_2_out_K, 'already done')
            else:
                make_table(path_2_out_K)
        else:
            make_table(path_2_out_K)
