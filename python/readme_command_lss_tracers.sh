#####################################################
#
# 4MOST / SDSS-V mock catalogue
# 4MOST S5-S6-S8
# SDSS-V BHM
#
#####################################################

# tabulate K mag abs - stellar mass relation
python 005_1_Iband_magnitudes.py
python 005_1_Kband_magnitudes.py

# assign magnitudes to catalogues using KIDS
python 005_2_all_magnitudes_fullfile.py
# same using COSMOS
python 005_2_all_magnitudes_fullfile_C.py


ls $MD10/cat_SHAM_COSMO/BG_000???.fit
ls $MD10/cat_SHAM_COSMO/LRG_000???.fit
ls $MD10/cat_SHAM_COSMO/S5GAL_000???.fit
# ls $MD10/cat_SHAM_COSMO/ELG_000???.fit

# rm $MD10/cat_SHAM_COSMO/BG_000???.fit
# rm $MD10/cat_SHAM_COSMO/LRG_000???.fit
# rm $MD10/cat_SHAM_COSMO/S5GAL_000???.fit
# rm $MD10/cat_SHAM_COSMO/ELG_000???.fit

######################################
# 4MOST cosmology 
######################################

# preparation scripts 
# tabulate NZ, only run once
# python 005_NZ_tabulateJson.py
# cd $GIT_AGN_MOCK/data/cosmo-4most/
# python 4most_ts_redshifttruth.py --dz=0.1 -n=2 --zmax=0.6


# write QMOST S8 cosmology catalogues and S5 Filament sub survey catalogue
# abundance matching with the NZ tabulated in advance
# script: 005_0_sham_cosmology_catalogs.py
# inputs: 
#    ${env}/cat_GALAXY_all/000???.fit
# outputs: 
#    ${env}/cat_SHAM_COSMO/
#       S5GAL_*
#       BG_*
#       LRG_*
#       ELG_*

# execute the commands in the following script for MD10 and for MD04

# writes slurm scripts to be executed on he1srv
# script 005_0_sham_cosmology_catalogs_WriteScripts.py
# output: cd $GIT_AGN_MOCK/python/005_0_log/*.sh
# python 005_0_sham_cosmology_catalogs_WriteScripts.py

# # # STEP 1
# # # Generates single tracer catalogues
nohup sh 005_0_sham_cosmology_catalogs_RUN.sh > 005_0_sham_cosmology_catalogs_RUN.log & 
nohup python3 005_0_sham_filament_catalogs.py UNIT_fA1i_DIR > 005_0_S5filament_catalogs_RUN.log &
# single command for test
# python3 005_0_sham_cosmology_catalogs.py UNIT_fA1i_DIR 000 


# ssh he1srv
# cd $GIT_AGN_MOCK/python/005_0_log/
# pyCONDA
# ipython
# 
# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("MD04_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)
# 
# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("MD10_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)
# 
# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("UNIT_fA1i_DIR_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)
# 
# 
# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("UNIT_fA2_DIR_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)
# 
# 
# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("UNIT_fA1_DIR_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)



# # # STEP 2
# Adds all magnitudes and the r-band fiber magnitude in each file 
# And all 4MOST columns
# script: 005_2_all_magnitudes.py
# inputs: 
#    ${env}/cat_SHAM_COSMO/
#       S5GAL_*
#       BG_*
#       LRG_*
#       ELG_*
# outputs: updates the files
# python 005_2_all_magnitudes.py UNIT_fA1i_DIR

# fits the stellar mass absolute magnitude relation
python 005_1_Iband_magnitudes.py
python 005_1_Kband_magnitudes.py

cd $GIT_AGN_MOCK/python
# # tabulate files on the full sky matched to KIDS
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 000 050 > mag_050.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 050 100 > mag_100.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 150 200 > mag_150.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 250 300 > mag_250.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 350 400 > mag_350.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 450 500 > mag_450.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 550 600 > mag_550.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 650 700 > mag_650.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 750 800 > mag_750.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 100 150 > mag_100.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 200 250 > mag_200.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 300 350 > mag_300.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 400 450 > mag_400.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 500 550 > mag_500.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 600 650 > mag_600.log &
nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 700 750 > mag_700.log &

nohup python 005_2_all_magnitudes_fullfile.py UNIT_fA1i_DIR 000 800 > mag_800.log &

cd $GIT_AGN_MOCK/python

# tabulate files on the full sky matched to COSMOS
python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 0 800
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 000 050 > magC_000.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 050 100 > magC_050.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 150 200 > magC_150.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 250 300 > magC_250.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 350 400 > magC_350.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 450 500 > magC_450.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 550 600 > magC_550.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 650 700 > magC_650.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 750 800 > magC_750.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 100 150 > magC_100.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 200 250 > magC_200.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 300 350 > magC_300.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 400 450 > magC_400.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 500 550 > magC_500.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 600 650 > magC_600.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 700 750 > magC_700.log &
nohup python 005_2_all_magnitudes_fullfile_C.py UNIT_fA1i_DIR 750 800 > magC_800.log &

python 005_2_all_magnitudes_fullfile_LF_plot.py

python 005_2_all_magnitudes_fullfile.py MD04
python 005_2_all_magnitudes_fullfile_C.py MD04

# select galaxies in R200c
python 004_1_galaxies_around_eROSITA_clusters.py UNIT_fA1i_DIR UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS_Sept2021.fits
# select galaxies in 5xR200c
python 004_1_galaxies_around_eROSITA_CHANCES_clusters.py UNIT_fA1i_DIR UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS_Sept2021.fits
# add red sequence boolean and QU_SFR
python 004_3_cluster_red_galaxies_eROSITA_CHANCES.py

# concatenate galaxy catalogues :
cd $UNIT_fA1i_DIR
ls cat_CHANCES_clu/*.fit > 4most_chances.list
stilts tcat in=@4most_chances.list ifmt=fits omode=out ofmt=fits out=CHANCES_gals.fits
cd $UNIT_fA1i_DIR
ls cat_eROCGAL/*.fit > 4most_CGAL.list
stilts tcat in=@4most_CGAL.list ifmt=fits omode=out ofmt=fits out=S5_CGAL.fits

cd $GIT_AGN_MOCK/python

# # change RS SED
# # booleans for selection
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 000 050 K > final_mag050_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 050 100 K > final_mag100_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 100 150 K > final_mag150_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 150 200 K > final_mag200_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 200 250 K > final_mag250_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 250 300 K > final_mag300_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 300 350 K > final_mag350_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 350 400 K > final_mag400_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 400 450 K > final_mag450_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 450 500 K > final_mag500_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 500 550 K > final_mag550_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 550 600 K > final_mag600_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 600 650 K > final_mag650_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 650 700 K > final_mag700_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 700 750 K > final_mag750_K.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 750 800 K > final_mag800_K.log &
cd $GIT_AGN_MOCK/python

nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 000 050 C > final_mag050_C.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 050 100 C > final_mag100_C.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 100 150 C > final_mag150_C.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 150 200 C > final_mag200_C.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 250 300 C > final_mag300_C.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 300 350 C > final_mag350_C.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 350 400 C > final_mag400_C.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 400 450 C > final_mag450_C.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 450 500 C > final_mag500_C.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 500 550 C > final_mag550_C.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 550 600 C > final_mag600_C.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 600 650 C > final_mag650_C.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 650 700 C > final_mag700_C.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 700 750 C > final_mag750_C.log &
nohup python 005_3_final_magnitudes_CLU_AGN.py  UNIT_fA1i_DIR 750 800 C > final_mag800_C.log &

cd $GIT_AGN_MOCK/python

nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 000 050 S5 > final_QMOSTcat050_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 050 100 S5 > final_QMOSTcat100_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 100 150 S5 > final_QMOSTcat150_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 150 200 S5 > final_QMOSTcat200_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 250 300 S5 > final_QMOSTcat300_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 300 350 S5 > final_QMOSTcat350_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 350 400 S5 > final_QMOSTcat400_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 400 450 S5 > final_QMOSTcat450_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 450 500 S5 > final_QMOSTcat500_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 500 550 S5 > final_QMOSTcat550_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 550 600 S5 > final_QMOSTcat600_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 600 650 S5 > final_QMOSTcat650_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 650 700 S5 > final_QMOSTcat700_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 700 750 S5 > final_QMOSTcat750_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 750 800 S5 > final_QMOSTcat800_C.log &

cd $GIT_AGN_MOCK/python

nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 000 050 S8 > final_QMOSTcat050_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 050 100 S8 > final_QMOSTcat100_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 100 150 S8 > final_QMOSTcat150_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 150 200 S8 > final_QMOSTcat200_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 250 300 S8 > final_QMOSTcat300_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 300 350 S8 > final_QMOSTcat350_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 350 400 S8 > final_QMOSTcat400_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 400 450 S8 > final_QMOSTcat450_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 450 500 S8 > final_QMOSTcat500_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 500 550 S8 > final_QMOSTcat550_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 550 600 S8 > final_QMOSTcat600_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 600 650 S8 > final_QMOSTcat650_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 650 700 S8 > final_QMOSTcat700_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 700 750 S8 > final_QMOSTcat750_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 750 800 S8 > final_QMOSTcat800_C.log &


nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 000 025 S8 > final_QMOSTcatS8000_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 025 050 S8 > final_QMOSTcatS8025_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 050 075 S8 > final_QMOSTcatS8050_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 075 100 S8 > final_QMOSTcatS8075_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 100 125 S8 > final_QMOSTcatS8100_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 125 150 S8 > final_QMOSTcatS8125_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 150 175 S8 > final_QMOSTcatS8150_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 175 200 S8 > final_QMOSTcatS8175_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 200 225 S8 > final_QMOSTcatS8200_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 225 250 S8 > final_QMOSTcatS8225_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 250 275 S8 > final_QMOSTcatS8250_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 275 300 S8 > final_QMOSTcatS8275_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 300 325 S8 > final_QMOSTcatS8300_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 325 350 S8 > final_QMOSTcatS8325_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 350 375 S8 > final_QMOSTcatS8350_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 375 400 S8 > final_QMOSTcatS8375_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 400 425 S8 > final_QMOSTcatS8400_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 425 450 S8 > final_QMOSTcatS8425_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 450 475 S8 > final_QMOSTcatS8450_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 475 500 S8 > final_QMOSTcatS8475_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 500 525 S8 > final_QMOSTcatS8500_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 525 550 S8 > final_QMOSTcatS8525_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 550 575 S8 > final_QMOSTcatS8550_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 575 600 S8 > final_QMOSTcatS8575_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 600 625 S8 > final_QMOSTcatS8600_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 625 650 S8 > final_QMOSTcatS8625_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 650 675 S8 > final_QMOSTcatS8650_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 675 700 S8 > final_QMOSTcatS8675_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 700 725 S8 > final_QMOSTcatS8700_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 725 750 S8 > final_QMOSTcatS8725_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 750 775 S8 > final_QMOSTcatS8750_C.log &

cd $GIT_AGN_MOCK/python

nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 000 050 S15 > final_QMOSTcat050_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 050 100 S15 > final_QMOSTcat100_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 100 150 S15 > final_QMOSTcat150_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 150 200 S15 > final_QMOSTcat200_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 250 300 S15 > final_QMOSTcat300_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 300 350 S15 > final_QMOSTcat350_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 350 400 S15 > final_QMOSTcat400_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 400 450 S15 > final_QMOSTcat450_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 450 500 S15 > final_QMOSTcat500_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 500 550 S15 > final_QMOSTcat550_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 550 600 S15 > final_QMOSTcat600_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 600 650 S15 > final_QMOSTcat650_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 650 700 S15 > final_QMOSTcat700_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 700 750 S15 > final_QMOSTcat750_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 750 800 S15 > final_QMOSTcat800_C.log &

cd $GIT_AGN_MOCK/python

nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 000 050 S18 > final_QMOSTcat050_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 050 100 S18 > final_QMOSTcat100_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 100 150 S18 > final_QMOSTcat150_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 150 200 S18 > final_QMOSTcat200_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 250 300 S18 > final_QMOSTcat300_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 300 350 S18 > final_QMOSTcat350_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 350 400 S18 > final_QMOSTcat400_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 400 450 S18 > final_QMOSTcat450_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 450 500 S18 > final_QMOSTcat500_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 500 550 S18 > final_QMOSTcat550_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 550 600 S18 > final_QMOSTcat600_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 600 650 S18 > final_QMOSTcat650_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 650 700 S18 > final_QMOSTcat700_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 700 750 S18 > final_QMOSTcat750_C.log &
nohup python 005_3_QMOST_format.py  UNIT_fA1i_DIR 750 800 S18 > final_QMOSTcat800_C.log &


cp 005_3_QMOST_format.py 005_3_QMOST_format_ALLcolumns.py

# only once
# python rewrite_templates.py


cd $UNIT_fA1i_DIR
ls cat_QMOST_all/is_S50*.fit > qmost_s50.list
stilts tcat in=@qmost_s50.list ifmt=fits omode=out ofmt=fits out=S05_all.fits

cd $GIT_AGN_MOCK/python
python 004_9_4most_merge.py S05_all.fits S05_all_formatApril22.fits



cd $UNIT_fA1i_DIR
ls cat_QMOST_all/is_S801*.fit > qmost_s801.list
stilts tcat in=@qmost_s801.list ifmt=fits omode=out ofmt=fits out=S0801.fits

cd $GIT_AGN_MOCK/python
python 004_9_4most_merge.py S0801.fits S0801_formatApril22.fits



cd $UNIT_fA1i_DIR
ls cat_QMOST_all/is_S802*.fit > qmost_s802.list
stilts tcat in=@qmost_s802.list ifmt=fits omode=out ofmt=fits out=S0802.fits

cd $GIT_AGN_MOCK/python
python 004_9_4most_merge.py S0802.fits S0802_formatApril22.fits



cd $UNIT_fA1i_DIR
ls cat_QMOST_all/is_S15*.fit > qmost_s15.list
stilts tcat in=@qmost_s15.list ifmt=fits omode=out ofmt=fits out=S15.fits

cd $GIT_AGN_MOCK/python
python 004_9_4most_merge.py S15.fits S15_formatApril22.fits



cd $UNIT_fA1i_DIR
ls cat_QMOST_all/is_S18*.fit > qmost_s18.list
stilts tcat in=@qmost_s18.list ifmt=fits omode=out ofmt=fits out=S18.fits

cd $GIT_AGN_MOCK/python
python 004_9_4most_merge.py S18.fits S18_formatApril22.fits



cd /home/comparat/data2/firefly/mocks/2022-02
rsync -avz $UNIT_fA1i_DIR/S05_all_formatApril22.fits  .
rsync -avz $UNIT_fA1i_DIR/S0801_formatApril22.fits  .
rsync -avz $UNIT_fA1i_DIR/S0802_formatApril22.fits  .
rsync -avz $UNIT_fA1i_DIR/S15_formatApril22.fits  .
rsync -avz $UNIT_fA1i_DIR/S18_formatApril22.fits  .
chmod ugo+rX *

topcat *_formatApril22.fits


# # # # python 005_2_all_magnitudes_WriteScripts.py
# # # # nohup sh 005_2_all_magnitudes_RUN.sh > 005_2_log/005_2_all_magnitudes_RUN.log & 
# # # # nohup sh 005_3_4most_catalog_RUN.sh > 005_3_4most_catalog_RUN.log & 
nohup sh 005_4_add_footprint_pixels_RUN.sh > 005_4_add_footprint_pixels_RUN.log & 

# 
# # # # nohup nice -n 19 python 005_2_all_magnitudes.py   MD10 >  logs/005_2_all_mag_MD10.log &
# # # # nohup nice -n 19 python 005_2_all_magnitudes.py   MD04 >  logs/005_2_all_mag_MD04.log &
# # # # python 005_0_sham_cosmology_catalogs.py MD10 000
# # # # python 005_2_all_magnitudes.py MD10 000
# # # # python 005_3_4most_catalog.py MD10 000
# # # # 
# # # # python 005_0_sham_cosmology_catalogs.py MD10 562
# # # # python 005_2_all_magnitudes.py MD10 562
# # # # python 005_3_4most_catalog.py MD10 562 
# # # # 

# 
# ssh he1srv
# cd $GIT_AGN_MOCK/python/005_2_log/
# pyCONDA
# ipython

# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("MD04_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)

# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("MD10_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)



# converts to the QMOST format
# script: 005_3_4most_catalog.py
# inputs: 
#    ${env}/cat_SHAM_COSMO/
#       S5GAL_*
#       BG_*
#       LRG_*
#       ELG_*
# outputs: updates the files
# nohup python 005_3_4most_catalog.py    MD10 >  logs/005_3_4most_cat_MD10.log &                                 
# nohup python 005_3_4most_catalog.py    MD04 >  logs/005_3_4most_cat_MD04.log &


# ssh he1srv
# cd $GIT_AGN_MOCK/python/005_3_log/
# pyCONDA
# ipython

# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("MD04_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)
# ipython

# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("MD10_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)


###############################
#
# Concatenates ctalogues
#
###############################
cd $GIT_AGN_MOCK/python/

ls $UNIT_fA1i_DIR/cat_SHAM_COSMO/S5GAL_*.fit > lists/4most_bgs5.lis
python concat_file_list_maskbit.py lists/4most_bgs5.lis /data24s/comparat/simulation/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001/FILAMENTS5_4MOST.fits erosita
# stilts tcat in=@lists/4most_bgs5.lis ifmt=fits icmd='select "abs(g_lat)>10 && g_lon>180 && DEC>-80 && DEC<=5 "' omode=out ofmt=fits out=$UNIT_fA1i_DIR/FILAMENTS5_4MOST.fits

# rm 4most_bgs5.lis
ls $UNIT_fA1i_DIR/cat_SHAM_COSMO/BG_*.fit > lists/4most_bg.lis
python concat_file_list_maskbit.py lists/4most_bg.lis /data24s/comparat/simulation/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001/FILAMENTS5_4MOST.fits/BG_4MOST.fits xgal
cd $UNIT_fA1i_DIR
ls cat_SHAM_COSMO/S5GAL_*.fit > 4most_bg.list
stilts tcat in=@4most_bg.list ifmt=fits omode=out ofmt=fits out=S5GAL_allSky.fits
# rm 4most_bg.lis

ls $UNIT_fA1i_DIR/cat_SHAM_COSMO/LRG_*.fit > lists/4most_lrg.lis
python concat_file_list_maskbit.py lists/4most_lrg.lis /data24s/comparat/simulation/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001/LRG_4MOST.fits xgal


# cd $GIT_AGN_MOCK/python/
# 
# ls $MD10/cat_SHAM_COSMO/S5GAL_*.fit > lists/4most_bgs5.lis
# python concat_file_list_maskbit.py lists/4most_bgs5.lis /data37s/simulation_1/MD/MD_1.0Gpc/FILAMENTS5_4MOST.fits erosita
# # stilts tcat in=@lists/4most_bgs5.lis ifmt=fits icmd='select "abs(g_lat)>10 && g_lon>180 && DEC>-80 && DEC<=5 "' omode=out ofmt=fits out=$MD10/FILAMENTS5_4MOST.fits
# 
# # rm 4most_bgs5.lis
# ls $MD10/cat_SHAM_COSMO/BG_*.fit > lists/4most_bg.lis
# python concat_file_list_maskbit.py lists/4most_bg.lis /data37s/simulation_1/MD/MD_1.0Gpc/BG_4MOST.fits xgal
# # stilts tcat in=@lists/4most_bg.lis ifmt=fits icmd='select "abs(g_lat)>10 && DEC<=20 "' omode=out ofmt=fits out=$MD10/BG_4MOST.fits
# # rm 4most_bg.lis
# ls $MD10/cat_SHAM_COSMO/LRG_*.fit > lists/4most_lrg.lis
# python concat_file_list_maskbit.py lists/4most_lrg.lis /data37s/simulation_1/MD/MD_1.0Gpc/LRG_4MOST.fits xgal

#stilts tcat in=@lists/4most_lrg.lis ifmt=fits icmd='select "abs(g_lat)>10 && DEC<=20 "' omode=out ofmt=fits out=$MD10/LRG_4MOST.fits
# rm 4most_lrg.lis

# stilts tcat in=@list_to_merge1.list ifmt=fits omode=out ofmt=fits out=eSASS_merged_wAGN_wCLU_rseppi_epoch1_field_1.fit
# stilts tcat in=@list_to_merge2.list ifmt=fits omode=out ofmt=fits out=eSASS_merged_wAGN_wCLU_rseppi_epoch1_field_2.fit

ls $MD10/cat_SHAM_COSMO/ELG_0000*.fit > lists/4most_elg.lis
python concat_file_list_maskbit.py lists/4most_elg.lis /data37s/simulation_1/MD/MD_1.0Gpc/ELG_4MOST_0.fits xgal
ls $MD10/cat_SHAM_COSMO/ELG_0001*.fit > lists/4most_elg.lis
python concat_file_list_maskbit.py lists/4most_elg.lis /data37s/simulation_1/MD/MD_1.0Gpc/ELG_4MOST_1.fits xgal
ls $MD10/cat_SHAM_COSMO/ELG_0002*.fit > lists/4most_elg.lis
python concat_file_list_maskbit.py lists/4most_elg.lis /data37s/simulation_1/MD/MD_1.0Gpc/ELG_4MOST_2.fits xgal
ls $MD10/cat_SHAM_COSMO/ELG_0003*.fit > lists/4most_elg.lis
python concat_file_list_maskbit.py lists/4most_elg.lis /data37s/simulation_1/MD/MD_1.0Gpc/ELG_4MOST_3.fits xgal
ls $MD10/cat_SHAM_COSMO/ELG_0004*.fit > lists/4most_elg.lis
python concat_file_list_maskbit.py lists/4most_elg.lis /data37s/simulation_1/MD/MD_1.0Gpc/ELG_4MOST_4.fits xgal
ls $MD10/cat_SHAM_COSMO/ELG_0005*.fit > lists/4most_elg.lis
python concat_file_list_maskbit.py lists/4most_elg.lis /data37s/simulation_1/MD/MD_1.0Gpc/ELG_4MOST_5.fits xgal
ls $MD10/cat_SHAM_COSMO/ELG_0006*.fit > lists/4most_elg.lis
python concat_file_list_maskbit.py lists/4most_elg.lis /data37s/simulation_1/MD/MD_1.0Gpc/ELG_4MOST_6.fits xgal
ls $MD10/cat_SHAM_COSMO/ELG_0007*.fit > lists/4most_elg.lis
python concat_file_list_maskbit.py lists/4most_elg.lis /data37s/simulation_1/MD/MD_1.0Gpc/ELG_4MOST_7.fits xgal

ls $MD10/ELG_4MOST_?.fits > lists/4most_elg.lis
python concat_file_list_maskbit.py lists/4most_elg.lis /data37s/simulation_1/MD/MD_1.0Gpc/ELG_4MOST.fits xgal
# stilts tcat in=@lists/4most_elg.lis ifmt=fits icmd='select "DEC<5 && DEC>-80 "' omode=out ofmt=fits out=$MD10/ELG_4MOST.fits

# rm 4most_elg.lis

# ls $MD04/cat_SHAM_COSMO/S5GAL_*.fit > 4most_bgs5.lis
# stilts tcat in=@4most_bgs5.lis ifmt=fits omode=out ofmt=fits out=$MD04/FILAMENTS5_4MOST.fits
# rm 4most_bgs5.lis
# ls $MD04/cat_SHAM_COSMO/BG_*.fit > 4most_bg.lis
# stilts tcat in=@4most_bg.lis ifmt=fits omode=out ofmt=fits out=$MD04/BG_4MOST.fits
# rm 4most_bg.lis
# ls $MD04/cat_SHAM_COSMO/LRG_*.fit > 4most_lrg.lis
# stilts tcat in=@4most_lrg.lis ifmt=fits omode=out ofmt=fits out=$MD04/LRG_4MOST.fits
# rm 4most_lrg.lis

# Add footprint bits
# script: 005_4_add_footprint.py
# inputs: catalogue file (any)    
# outputs: updates the files with the footprint bit

# S5
# python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2020-03/MDPL2/S5_BCG_4MOST.fit
# python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2020-03/MDPL2/S5_CGAL_4MOST.fit
# python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2020-03/MDPL2/S5_BCG_4MOST.fit
# python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2021-04/QMOST/QSO_4MOST.fits
#    
# nohup python 005_4_add_footprint.py MD10 S5_BCG_4MOST.fit       >  logs/lss_005_4_footprint_MD10_S5_BCG_4MOST.log &  
# nohup python 005_4_add_footprint.py MD10 S5_CGAL_4MOST.fit      >  logs/lss_005_4_footprint_MD10_S5_CGAL_4MOST.log &  
# nohup python 005_4_add_footprint.py MD10 FILAMENTS5_4MOST.fits  >  logs/lss_005_4_footprint_MD10_FILAMENTS5_4MOST.log &  
# 
# # python 005_4_add_footprint.py MD04 S5_BCG_4MOST.fit
# # python 005_4_add_footprint.py MD04 S5_CGAL_4MOST.fit
# # python 005_4_add_footprint.py MD04 FILAMENTS5_4MOST.fits
# 
# # S8
# nohup python 005_4_add_footprint.py MD10 BG_4MOST.fits   >  logs/lss_005_4_footprint_MD10_S8_BG_4MOST.log &  
# nohup python 005_4_add_footprint.py MD10 LRG_4MOST.fits  >  logs/lss_005_4_footprint_MD10_S8_LRG_4MOST.log &  
# nohup python 005_4_add_footprint.py MD10 ELG_4MOST.fits  >  logs/lss_005_4_footprint_MD10_S8_ELG_4MOST.log & 

nohup python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2021-04/QMOST/QSO_4MOST.fits >  logs/lss_005_4_footprint_UNIT_S8_QSO_4MOST.log & 
nohup python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2021-04/QMOST/LyA_4MOST.fits >  logs/lss_005_4_footprint_UNIT_S8_LyA_4MOST.log & 
nohup python 005_4_add_footprint.py  /data42s/comparat/firefly/mocks/2021-04/QMOST/BG_4MOST.fits >  logs/lss_005_4_footprint_UNIT_S8_BG_4MOST.log & 
nohup python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2021-04/QMOST/LRG_4MOST.fits >  logs/lss_005_4_footprint_UNIT_S8_LRG_4MOST.log & 

# python 005_4_add_footprint.py MD10 QSO_4MOST.fits
# python 005_4_add_footprint.py MD10 LyA_4MOST.fits
# # 
# python 005_4_add_footprint.py MD04 BG_4MOST.fits
# python 005_4_add_footprint.py MD04 LRG_4MOST.fits
# python 005_4_add_footprint.py MD04 QSO_4MOST.fits
# python 005_4_add_footprint.py MD04 LyA_4MOST.fits

# S6
# python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2020-03/QMOST/S6_4MOST_R-228-234.fit

###############################
#
# Merges high-z low-z parts
#
###############################
# S8 cosmology
# public file
# /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_QSO_LyA.fits
# /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_ELG.fits
# /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_BG.fits
# /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_LRG.fits

# cp $MD10/LyA_4MOST.fits  /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_LyA.fits
# cp $MD10/QSO_4MOST.fits  /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_QSO.fits
# 
# cp $MD10/BG_4MOST.fits   /data42s/comparat/firefly/mocks/2020-10/MDPL2/QMOST/
# cp $MD10/LRG_4MOST.fits  /data42s/comparat/firefly/mocks/2020-10/MDPL2/QMOST/
# cp $MD10/ELG_4MOST.fits  /data42s/comparat/firefly/mocks/2020-10/MDPL2/QMOST/
# 
# cd /data42s/comparat/firefly/mocks/2020-10/MDPL2/QMOST/
# gzip -k --rsyncable BG_4MOST.fits 
# gzip -k --rsyncable LRG_4MOST.fits
# gzip -k --rsyncable ELG_4MOST.fits
# /uufs/chpc.utah.edu/common/home/sdss41/ebosswork/eboss/spectro/redux/v5_13_2/spectra/lite/
# # create random ra-dec for QSO at density 50/deg2
# 
# gzip -k --rsyncable AGN_DEEP_4MOST.fits
# gzip -k --rsyncable AGN_IR_4MOST.fits
# gzip -k --rsyncable AGN_WIDE_4MOST.fits
# gzip -k --rsyncable BG_4MOST.fits
# gzip -k --rsyncable FILAMENTS5_4MOST.fits
# gzip -k --rsyncable LRG_4MOST.fits
# gzip -k --rsyncable LyA_4MOST.fits
# gzip -k --rsyncable QSO_4MOST.fits
# gzip -k --rsyncable UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_4MOST_ALL.fits
# gzip -k --rsyncable UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_4MOST_BCG.fits
# gzip -k --rsyncable UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_4MOST_GAL.fits
# gzip -k --rsyncable UNIT_fA1i_DIRS6_4MOST_ALL_SNR3_IR215.fit

python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2021-04/QMOST/BG_4MOST.fits 
python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2021-04/QMOST/LRG_4MOST.fits 
python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2021-04/QMOST/LyA_4MOST.fits  
python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2021-04/QMOST/QSO_4MOST.fits 
python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2021-04/QMOST/S8_4MOST_03May21.fits.gz 

# applyt the footprint to the S8 catalogues
python 005_5_cut_S8_catalogs.py  

# create random RA, DEC 
# output: /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_random_ra-dec-50perdeg2.fits
python 005_6_contamination_S8_QSO_make_randoms.py

# adds the random points to the QSO catalogue :
# output: /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8footprint_QSO_w_contamination.fits
python 005_6_contamination_S8_QSO.py

python 005_5_merge.py
cd /data42s/comparat/firefly/mocks/2021-04/QMOST/
chmod ugo+rX *

# final sub survey catalogues :
# /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8footprint_LyA.fits
# /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8footprint_QSO_w_contamination.fits
# /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8footprint_BG.fits 
# /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8footprint_LRG.fits
# /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8footprint_ELG.fits
ls $UNIT_fA1i_DIR/cat_GALAXY_all/000???.fit > flist
stilts tcat in=@flist ifmt=fits icmd='select "redshift_R<1.05 && galaxy_SMHMR_mass>10 "' icmd='keepcols "RA DEC redshift_R redshift_S galaxy_SMHMR_mass HALO_id HALO_pid HALO_Mvir"'  omode=out ofmt=fits out=$UNIT_fA1i_DIR/LC_zlt1_Mgt10.fits

# S8 Combine MD04 (low Z) and MD10 (high Z)
# stilts tpipe in=$MD04/QSO_4MOST.fits ifmt=fits omode=out ofmt=fits out=$MD04/S8_AGN_low_z.fits cmd='select "redshift_estimate<=0.3 "'
# stilts tcat ifmt=fits in=$MD10/QSO_4MOST.fits in=$MD10/LyA_4MOST.fits ocmd='select "redshift_estimate>0.3 "' out=$MD10/S8_AGN_hig_z.fit

# summary file
# stilts tcat ifmt=fits in=$MD10/LyA_4MOST.fits in=$MD10/QSO_4MOST.fits in=$MD10/BG_4MOST.fits in=$MD10/LRG_4MOST.fits  out=/data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_QSO_LyA.fits
# 
# stilts tcat ifmt=fits in1=$MD10/BG_4MOST.fits in2=$MD10/BG_4MOST.fits icmd1='select "redshift_estimate>0.3 "' icmd2='select "redshift_estimate<=0.3 "' out=/data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_BG.fits
# 
# stilts tcat ifmt=fits in1=$MD10/LRG_4MOST.fits in2=$MD10/LRG_4MOST.fits icmd1='select "redshift_estimate>0.3 "' icmd2='select "redshift_estimate<=0.3 "' out=/data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_LRG.fits
# 
# stilts tcat ifmt=fits in1=$MD10/ELG_4MOST.fits in2=$MD10/ELG_4MOST.fits icmd1='select "redshift_estimate>0.3 "' icmd2='select "redshift_estimate<=0.3 "' out=/data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_ELG.fits

# 
# # Merges BCG and CGAL catalogs into a single 4MOST catalogue
# # writes catalogues in the 4MOST format
# # script: 004_9_4most_merge.py
# # inputs: ${env}/S5_BCG_4MOST.fit, ${env}/S5_CGAL_4MOST.fit, ${env}/FILAMENTS5_4MOST.fit
# # outputs: ${env}/S5_4MOST.fit
# # nohup nice -n 19 python 004_9_4most_merge.py MD04 > 004_9_4most_catalog_MD04.log & 
# nohup nice -n 19 python 004_9_4most_merge.py UNIT_fA1i_DIR > logs/004_9_4most_catalog_UNIT.log & 
# 
# cp $MD10/S5_4MOST.fit /data42s/comparat/firefly/mocks/2021-04/QMOST/QMOST_S5_CLUSTER.fits

# stilts tpipe in=$MD04/S5_4MOST.fit ifmt=fits omode=out ofmt=fits out=$MD04/S5_4MOST_low_z.fits cmd='select "REDSHIFT_ESTIMATE<0.3"'
# stilts tpipe in=$MD10/S5_4MOST.fit ifmt=fits omode=out ofmt=fits out=$MD10/S5_4MOST_hig_z.fit cmd='select "REDSHIFT_ESTIMATE>=0.3"'

# ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0000*.fit > bestmost.list
# stilts tcat in=@bestmost.list  ifmt=fits icmd='select "abs(g_lat)>10"' omode=out ofmt=fits out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost0.fits
# ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0001*.fit > bestmost.list
# stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost1.fits
# ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0002*.fit > bestmost.list
# stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost2.fits
# ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0003*.fit > bestmost.list
# stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost3.fits
# ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0004*.fit > bestmost.list
# stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost4.fits
# ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0005*.fit > bestmost.list
# stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost5.fits
# ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0006*.fit > bestmost.list
# stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost6.fits
# ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0007*.fit > bestmost.list
# stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost7.fits


# stilts tcat ifmt=fits in=$MD04/S5_4MOST_low_z.fits in=$MD10/S5_4MOST_hig_z.fit omode=out out=/data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S5_CLUSTER.fits

# S5 clusters
# public file
# /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S5_CLUSTER.fits
# /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S6_AGN.fits

# compresses the files 
# cd /data42s/comparat/firefly/mocks/2020-03/QMOST
# gzip -k --rsyncable QMOST_S5_CLUSTER.fits
# gzip -k --rsyncable QMOST_S8footprint_QSO_w_contamination.fits
# gzip -k --rsyncable QMOST_S8footprint_LyA.fits
# gzip -k --rsyncable QMOST_S8footprint_ELG.fits
# gzip -k --rsyncable QMOST_S8footprint_BG.fits
# gzip -k --rsyncable QMOST_S8footprint_LRG.fits
# 
cd $GIT_AGN_MOCK/python
nohup python 005_7_shared_targets.py full > 005_7_shared_targets_full.tex &
nohup python 005_7_shared_targets.py small > 005_7_shared_targets_small.tex &
# 
# ### PROVIDE the LIGHT CONE FILES by running
# 
# tar -czf /home/comparat/data2/firefly/mocks/2020-03/MDPL2/LC/all_0.21690.fits.tar.gz /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_0.21690.fits
