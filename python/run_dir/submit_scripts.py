import os
import glob
import numpy as n

scripts = sorted(n.array(glob.glob("*.sh")))

for script in scripts:
    os.system('sbatch ' + script)
