#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=8 
#SBATCH --job-name=MD04_sat_0.90740 
 
. /home_local/4FSOpsim/py36he2srv/bin/activate 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
ipython3 001_coordinates.py MD04 sat_0.90740 
python3 002_galaxy.py MD04 sat_0.90740 
python3 003_agn.py MD04 sat_0.90740 
python3 004_eRo_AGN_catalog.py MD04 sat_0.90740 
 
