"""
What it does
------------

Add all 4MOST columns

Link to template

redden magnitudes...

add flag is_in_footprint

Command to run
--------------

python3 009_1_4most_catalog.py MD04
python3 009_1_4most_catalog.py MD10


ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0000*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits icmd='select "abs(g_lat)>10"' omode=out ofmt=fits out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost0.fits

chmod ugo+rX /home/comparat/data2/firefly/mocks/2020-10/MDPL2/*
ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0001*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost1.fits
ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0002*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost2.fits
ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0003*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost3.fits
ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0004*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost4.fits
ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0005*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost5.fits
ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0006*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost6.fits
ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_bestmost/0007*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/MDPL2/bestmost7.fits

chmod ugo+rX /home/comparat/data2/firefly/mocks/2020-10/MDPL2/*

then concatenate catalogues over the footprint de <10 ?

"""
import sys
import os
import time
from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column
from scipy.optimize import curve_fit
import astropy.io.fits as fits
import h5py
import numpy as n
print('CREATES FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()
import extinction

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
cosmoMD = FlatLambdaCDM(
	H0=67.77 * u.km / u.s / u.Mpc,
	Om0=0.307115)  # , Ob0=0.048206)
h = 0.6777
L_box = 1000.0 / h
cosmo = cosmoMD

zs = n.arange(0.00001, 2.1, 0.0001)
dm_itp = interp1d(zs, cosmo.distmod(zs).value)

#import astropy.io.fits as fits
# import all pathes

env = sys.argv[1]

root_dir = os.path.join(os.environ[env])
dir_2_OUT = os.path.join(root_dir, "cat_bestmost")

sub_survey_names = n.array([ 'euclid'])
N_subsurvey = {'euclid':1}
priority_values = {'euclid':100}

# reassigns templates correctly
z_all = n.hstack(( 0., n.arange(0.3, 3., 0.2), 3.5, 4.5, 6. ))
zmins = z_all[:-1]
zmaxs = z_all[1:]

def make_4most_Catalogue(HEALPIX_id):
	print(HEALPIX_id)
	path_2_CAT    = os.path.join(dir_2_OUT, str(HEALPIX_id).zfill(6) + '.fit')

	def add_4most_columns(subsurvey , t_survey  ):
		N_obj = len(t_survey)
		#  limit size of the string columns to the size of the longer string in the corresponding columns. 
		Mass = t_survey['galaxy_SMHMR_mass']
		SFR = t_survey['galaxy_star_formation_rate']
		sSFR = SFR-Mass
		SF_gal = sSFR>-11
		# 'NAME':str, max 256 char
		N1 = n.arange(len(t_survey['ebv']))
		id_list = N_subsurvey[subsurvey]*1e10 + HEALPIX_id*1e6 +  N1
		NAME = n.array([ str(int(el)).zfill(11) for el in id_list ])
		t_survey.add_column(Column(name='NAME', data=NAME, unit=''))
		# 'RA':n.float64, 1D
		# 'DEC':n.float64, 1D
		# 'PMRA':n.float32, 1E
		# 'PMDEC':n.float32, 1E
		# 'EPOCH':n.float32, 1E
		PMRA = n.zeros(N_obj)
		t_survey.add_column(Column(name='PMRA', data=PMRA, unit='mas/yr'))
		PMDEC = n.zeros(N_obj)
		t_survey.add_column(Column(name='PMDEC', data=PMDEC, unit='mas/yr'))
		EPOCH = n.ones(N_obj)*2000.
		t_survey.add_column(Column(name='EPOCH', data=EPOCH, unit='yr'))
		# 'RESOLUTION':n.int16, 1I
		RESOLUTION = n.ones(N_obj).astype('int')
		t_survey.add_column(Column(name='RESOLUTION', data=RESOLUTION, unit=''))
		# 'SUBSURVEY':str, max 256 char
		SUBSURVEY = n.ones(N_obj).astype('str')
		SUBSURVEY[:] = subsurvey
		t_survey.add_column(Column(name='SUBSURVEY', data=SUBSURVEY, unit=''))
		# 'PRIORITY':n.int16, 1I
		PRIORITY = n.zeros(N_obj).astype('int') + priority_values[subsurvey]
		t_survey.add_column(Column(name='PRIORITY', data=PRIORITY, unit=''))
		# EBV for templates
		ebv_1000 = (t_survey['ebv']*1000).astype('int')
		#print('EBV', n.min(ebv_1000), n.max(ebv_1000))
		ebv_1_0 = ( ebv_1000 > 1000 ) 
		ebv_0_5 = ( ebv_1000 > 500 ) & ( ebv_1000 <= 1000 ) 
		ebv_0_4 = ( ebv_1000 > 400 ) & ( ebv_1000 <= 500 ) 
		ebv_0_3 = ( ebv_1000 > 300 ) & ( ebv_1000 <= 400 ) 
		ebv_0_2 = ( ebv_1000 > 200 ) & ( ebv_1000 <= 300 ) 
		ebv_0_1 = ( ebv_1000 > 100 ) & ( ebv_1000 <= 200 ) 
		ebv_0_0 = ( ebv_1000 <= 100 ) 
		z_name = lambda z0, z1 : "_zmin_"+str(int(10*z0)).zfill(2)+"_zmax_"+str(int(10*z1)).zfill(2)
		# templates
		template_names = n.zeros(N_obj).astype('U100')
		ruleset_array = n.zeros(N_obj).astype('str')
		# S8 BG or LRG
		ruleset_array[:] = "euclid"
		for z0,z1 in zip(zmins,zmaxs):
			zsel = (t_survey['redshift_R']>=z0) & (t_survey['redshift_R']<z1)
			if len(zsel.nonzero()[0])>0:
				#ruleset_array[zsel] = "COSMO_RedGAL"
				template_names[(zsel)]               = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
				template_names[(zsel)&(ebv_0_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
				template_names[(zsel)&(ebv_0_1)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_1.fits'   
				template_names[(zsel)&(ebv_0_2)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_2.fits'   
				template_names[(zsel)&(ebv_0_3)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_3.fits'   
				template_names[(zsel)&(ebv_0_4)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_4.fits'   
				template_names[(zsel)&(ebv_0_5)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_5.fits'   
				template_names[(zsel)&(ebv_1_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_1_0.fits'   

		template_names_SF = n.zeros(N_obj).astype('U100')

		for z0,z1 in zip(zmins,zmaxs):
			zsel = (t_survey['redshift_R']>=z0)&(t_survey['redshift_R']<z1)
			if len(zsel.nonzero()[0])>0:
				#ruleset_array[zsel] = "ELG"
				template_names_SF[(zsel)]               = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_01.fits' 
				template_names_SF[(zsel)&(ebv_0_0)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_01.fits' 
				template_names_SF[(zsel)&(ebv_0_1)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_1.fits'  
				template_names_SF[(zsel)&(ebv_0_2)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_2.fits'  
				template_names_SF[(zsel)&(ebv_0_3)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_3.fits'  
				template_names_SF[(zsel)&(ebv_0_4)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_4.fits'  
				template_names_SF[(zsel)&(ebv_0_5)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_5.fits'  
				template_names_SF[(zsel)&(ebv_1_0)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_1_0.fits'  

		template_names[SF_gal] = template_names_SF[SF_gal]
		# 'TEMPLATE':str, max 256 char
		t_survey.add_column(Column(name='TEMPLATE', data=template_names, unit=''))
		# 'RULESET':str, max 256 char
		t_survey.add_column(Column(name='RULESET', data=ruleset_array, unit=''))
		# 'REDSHIFT_ESTIMATE':n.float32, 1E
		# 'REDSHIFT_ERROR':n.float32, 1E
		t_survey.add_column(Column(name='REDSHIFT_ESTIMATE', data=t_survey['redshift_R'], unit=''))
		t_survey.add_column(Column(name='REDSHIFT_ERROR', data=n.ones(N_obj), unit=''))
		# 'MAG':n.float32,
		# 'MAG_ERR':n.float32
		# 'MAG_TYPE': str max 256 char
		r_v=3.1
		a_v = t_survey['ebv'] * r_v
		delta_mag = n.hstack(( n.array([ extinction.fitzpatrick99(n.array([7500.]), el, r_v=3.1, unit='aa') for el in a_v ]) ))
		#rv = av/ebv
		#av = rv x ebv
		extincted_mag = t_survey['mag_I'] + delta_mag
		t_survey.add_column(Column(name='MAG', data=extincted_mag, unit='mag'))
		t_survey.add_column(Column(name='MAG_ERR', data=0.01 * n.ones(N_obj), unit='mag'))
		MAG_TYPE = n.ones(N_obj).astype('str')
		MAG_TYPE[:] = 'DECam_i_AB'
		t_survey.add_column(Column(name='MAG_TYPE', data=MAG_TYPE, unit=''))
		# 'REDDENING':n.float32, 1E
		t_survey.add_column(Column(name='REDDENING',data=t_survey['ebv'], unit='mag'))
		# 'DATE_EARLIEST':n.float64, JulianDate decimal days # 01-Nov-2022
		# 'DATE_LATEST':n.float64, JulianDate decimal days # 02-Feb-2033
		t_survey.add_column(Column(name='DATE_EARLIEST',data=22305 * n.ones(N_obj), unit='d'))
		t_survey.add_column(Column(name='DATE_LATEST'  ,data=33033 * n.ones(N_obj), unit='d'))
		CADENCE = n.zeros(N_obj).astype('int')
		t_survey.add_column(Column(name='CADENCE', data=CADENCE, unit=''))
		# extent flags and parameters
		# 'EXTENT_FLAG': 1I
		# =1
		# 'EXTENT_PARAMETER': 1E
		# =0
		# 'EXTENT_INDEX': 1E
		# =0
		t_survey.add_column(Column(name='EXTENT_FLAG'     , data=n.zeros(N_obj).astype('int') , unit=''))
		t_survey.add_column(Column(name='EXTENT_PARAMETER', data=n.zeros(N_obj), unit=''))
		t_survey.add_column(Column(name='EXTENT_INDEX'    , data=n.zeros(N_obj), unit=''))
		return t_survey

	t_bg = Table.read(path_2_CAT)
	subsurvey = 'euclid'
	t_survey = t_bg 
	if 'PMRA' in t_survey.columns :
		t_out = update_4most_columns(subsurvey = subsurvey, t_survey = t_survey)
	else:
		t_out = add_4most_columns(subsurvey = subsurvey, t_survey = t_survey)
	t_out.write (path_2_CAT  , overwrite=True)
	

import healpy
N_pixels = healpy.nside2npix(8)
for HEALPIX_id in n.arange(N_pixels):
	try :
		make_4most_Catalogue(HEALPIX_id)
	except(FileNotFoundError):
		print('FileNotFoundError')

#def update_4most_columns(subsurvey , t_survey  ):
	#N_obj = len(t_survey)
	##  limit size of the string columns to the size of the longer string in the corresponding columns. 
	## 'NAME':str, max 256 char
	#N1 = n.arange(len(t_survey['ebv']))
	#id_list = N_subsurvey[subsurvey]*1e10 + HEALPIX_id*1e6 +  N1
	#NAME = n.array([ str(int(el)).zfill(11) for el in id_list ])
	#t_survey['NAME'] = NAME
	#PMRA = n.zeros(N_obj)
	#t_survey['PMRA'] = PMRA
	#PMDEC = n.zeros(N_obj)
	#t_survey['PMDEC'] = PMDEC
	#EPOCH = n.ones(N_obj)*2000.
	#t_survey['EPOCH'] = EPOCH
	## 'RESOLUTION':n.int16, 1I
	#RESOLUTION = n.ones(N_obj).astype('int')
	#t_survey['RESOLUTION'] = RESOLUTION
	## 'SUBSURVEY':str, max 256 char
	#SUBSURVEY = n.ones(N_obj).astype('str')
	#SUBSURVEY[:] = subsurvey
	#t_survey['SUBSURVEY'] = SUBSURVEY
	## 'PRIORITY':n.int16, 1I
	#PRIORITY = n.zeros(N_obj).astype('int') + priority_values[subsurvey]
	#t_survey['PRIORITY'] = PRIORITY
	## EBV for templates
	#ebv_1000 = (t_survey['ebv']*1000).astype('int')
	##print('EBV', n.min(ebv_1000), n.max(ebv_1000))
	#ebv_1_0 = ( ebv_1000 > 1000 ) 
	#ebv_0_5 = ( ebv_1000 > 500 ) & ( ebv_1000 <= 1000 ) 
	#ebv_0_4 = ( ebv_1000 > 400 ) & ( ebv_1000 <= 500 ) 
	#ebv_0_3 = ( ebv_1000 > 300 ) & ( ebv_1000 <= 400 ) 
	#ebv_0_2 = ( ebv_1000 > 200 ) & ( ebv_1000 <= 300 ) 
	#ebv_0_1 = ( ebv_1000 > 100 ) & ( ebv_1000 <= 200 ) 
	#ebv_0_0 = ( ebv_1000 <= 100 ) 
	#z_name = lambda z0, z1 : "_zmin_"+str(int(10*z0)).zfill(2)+"_zmax_"+str(int(10*z1)).zfill(2)
	## templates
	#template_names = n.zeros(N_obj).astype('U100')
	#ruleset_array = n.zeros(N_obj).astype('str')
	## S8 BG or LRG
	#if subsurvey == 'BG' or subsurvey == 'LRG':
		#ruleset_array[:] = "COSMO_RedGAL"
		#for z0,z1 in zip(zmins,zmaxs):
			#zsel = (t_survey['redshift_R']>=z0) & (t_survey['redshift_R']<z1)
			#if len(zsel.nonzero()[0])>0:
				##ruleset_array[zsel] = "COSMO_RedGAL"
				#template_names[(zsel)]               = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
				#template_names[(zsel)&(ebv_0_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
				#template_names[(zsel)&(ebv_0_1)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_1.fits'   
				#template_names[(zsel)&(ebv_0_2)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_2.fits'   
				#template_names[(zsel)&(ebv_0_3)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_3.fits'   
				#template_names[(zsel)&(ebv_0_4)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_4.fits'   
				#template_names[(zsel)&(ebv_0_5)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_5.fits'   
				#template_names[(zsel)&(ebv_1_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_1_0.fits'   
	## S5 filament gal
	#if subsurvey == 'filament_GAL' :
		#ruleset_array[:] = "RedGAL"
		#for z0,z1 in zip(zmins,zmaxs):
			#zsel = (t_survey['redshift_R']>=z0) & (t_survey['redshift_R']<z1)
			#if len(zsel.nonzero()[0])>0:
				##ruleset_array[zsel] = "RedGAL"
				#template_names[(zsel)]               = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
				#template_names[(zsel)&(ebv_0_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
				#template_names[(zsel)&(ebv_0_1)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_1.fits'   
				#template_names[(zsel)&(ebv_0_2)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_2.fits'   
				#template_names[(zsel)&(ebv_0_3)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_3.fits'   
				#template_names[(zsel)&(ebv_0_4)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_4.fits'   
				#template_names[(zsel)&(ebv_0_5)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_5.fits'   
				#template_names[(zsel)&(ebv_1_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_1_0.fits'   
	## S8 ELG
	#if subsurvey == 'ELG' :
		#ruleset_array[:] = "ELG"
		#for z0,z1 in zip(zmins,zmaxs):
			#zsel = (t_survey['redshift_R']>=z0)&(t_survey['redshift_R']<z1)
			#if len(zsel.nonzero()[0])>0:
				##ruleset_array[zsel] = "ELG"
				#template_names[(zsel)]               = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_01.fits' 
				#template_names[(zsel)&(ebv_0_0)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_01.fits' 
				#template_names[(zsel)&(ebv_0_1)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_1.fits'  
				#template_names[(zsel)&(ebv_0_2)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_2.fits'  
				#template_names[(zsel)&(ebv_0_3)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_3.fits'  
				#template_names[(zsel)&(ebv_0_4)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_4.fits'  
				#template_names[(zsel)&(ebv_0_5)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_5.fits'  
				#template_names[(zsel)&(ebv_1_0)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_1_0.fits'  

	## 'TEMPLATE':str, max 256 char
	#t_survey['TEMPLATE'] = template_names
	## 'RULESET':str, max 256 char
	#t_survey['RULESET'] = ruleset_array
	## 'REDSHIFT_ESTIMATE':n.float32, 1E
	## 'REDSHIFT_ERROR':n.float32, 1E
	#t_survey['REDSHIFT_ESTIMATE'] = t_survey['redshift_R']
	#t_survey['REDSHIFT_ERROR'] = n.ones(N_obj)
	## 'MAG':n.float32,
	## 'MAG_ERR':n.float32
	## 'MAG_TYPE': str max 256 char
	#r_v=3.1
	#a_v = t_survey['ebv'] * r_v
	#delta_mag = n.hstack(( n.array([ extinction.fitzpatrick99(n.array([6500.]), el, r_v=3.1, unit='aa') for el in a_v ]) ))
	##rv = av/ebv
	##av = rv x ebv
	#extincted_mag = t_survey['rtot'] + delta_mag
	#t_survey['MAG'] = extincted_mag
	#t_survey['MAG_ERR'] = 0.01 * n.ones(N_obj)
	#MAG_TYPE = n.ones(N_obj).astype('str')
	#MAG_TYPE[:] = 'DECam_r_AB'
	#t_survey['MAG_TYPE'] = MAG_TYPE
	## 'REDDENING':n.float32, 1E
	#t_survey['REDDENING'] = t_survey['ebv']
	## 'DATE_EARLIEST':n.float64, JulianDate decimal days # 01-Nov-2022
	## 'DATE_LATEST':n.float64, JulianDate decimal days # 02-Feb-2033
	#t_survey['DATE_EARLIEST'] = 22305 * n.ones(N_obj)
	#t_survey['DATE_LATEST'  ] = 33033 * n.ones(N_obj)
	#t_survey['CADENCE'  ] = n.zeros(N_obj).astype('int')
	## 'EXTENT_FLAG': 1I
	## =1
	## 'EXTENT_PARAMETER': 1E
	## =0
	## 'EXTENT_INDEX': 1E
	## =0
	#t_survey['EXTENT_FLAG'     ] = n.zeros(N_obj).astype('int') 
	#t_survey['EXTENT_PARAMETER'] = n.zeros(N_obj)
	#t_survey['EXTENT_INDEX'    ] = n.zeros(N_obj)
	#print('assigns magnitudes and sersic types')
	#phot_types = n.unique(t_survey['lsdr9_TYPE'])  
	#print('LS types', phot_types)
	#sel_DEV = (t_survey['lsdr9_TYPE'] == "DEV")
	#sel_EXP = (t_survey['lsdr9_TYPE'] == "EXP")
	#sel_PSF = (t_survey['lsdr9_TYPE'] == "PSF")
	#sel_REX = (t_survey['lsdr9_TYPE'] == "REX")
	#sel_SER = (t_survey['lsdr9_TYPE'] == "SER")
	## extent flags and parameters
	#t_survey['EXTENT_FLAG'][sel_PSF] = 0
	#t_survey['EXTENT_FLAG'][sel_DEV] = 2
	#t_survey['EXTENT_FLAG'][sel_EXP] = 2
	#t_survey['EXTENT_FLAG'][sel_REX] = 2
	#t_survey['EXTENT_FLAG'][sel_SER] = 2
	##
	#t_survey['EXTENT_PARAMETER'][sel_PSF] = 0
	#t_survey['EXTENT_PARAMETER'][sel_DEV] = t_survey['lsdr9_SHAPE_R'][sel_DEV]
	#t_survey['EXTENT_PARAMETER'][sel_EXP] = t_survey['lsdr9_SHAPE_R'][sel_EXP]
	#t_survey['EXTENT_PARAMETER'][sel_REX] = t_survey['lsdr9_SHAPE_R'][sel_REX]
	#t_survey['EXTENT_PARAMETER'][sel_SER] = t_survey['lsdr9_SHAPE_R'][sel_SER]
	##
	#print('min extent param', t_survey['EXTENT_PARAMETER'][sel_PSF == False].min() )
	#t_survey['EXTENT_PARAMETER'] [ ( t_survey['EXTENT_PARAMETER']<0.1 ) & (sel_PSF == False ) ] = 0.101
	##
	#t_survey['EXTENT_INDEX'][sel_PSF] = 0
	#t_survey['EXTENT_INDEX'][sel_DEV] = 4
	#t_survey['EXTENT_INDEX'][sel_EXP] = 1
	#t_survey['EXTENT_INDEX'][sel_REX] = 1
	#t_survey['EXTENT_INDEX'][sel_SER] = t_survey['lsdr9_SERSIC'][sel_SER]
	#print('min index', t_survey['EXTENT_INDEX'][sel_PSF == False].min() )
	#return t_survey

