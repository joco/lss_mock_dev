"""
What it does
------------

Take the complete cluster galaxy satellite FILE:  $ENV_eRO_CLU_SAT.fit
Re-assigns the is_quiescent flag as a function of radius as well as star formation rates.

It increases the number of quiescent galaxies in the vicinity of clusters

References
----------

 * Hennig et al. 2017, https://ui.adsabs.harvard.edu/abs/2017MNRAS.467.4015H
 * Biffi et al. Dolag et al. Magneticum, https://ui.adsabs.harvard.edu/#abs/2018MNRAS.tmp.2317B

Command to run
--------------

python3 004_3_cluster_red_galaxies.py environmentVAR

arguments
---------

environmentVAR: environment variable linking to the directory where files are e.g. "MD10"
It will then work in the directory : $environmentVAR/hlists/fits/

Dependencies
------------

import time, os, sys, numpy, astropy


"""
import glob
from scipy.optimize import newton
import sys, os, time
from scipy.stats import norm
from scipy.interpolate import interp1d 
#import astropy.io.fits as fits
import numpy as n
from scipy.special import erf
from astropy.table import Table, Column
print('Adjusts red sequence of galaxies around clusters')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()


nl = lambda selection: len(selection.nonzero()[0])

env =  'UNIT_fA1i_DIR' # sys.argv[1]
# "MD10"
delta_crit = '200c'
#CLU_catalog_name = sys.argv[2]

HEALPIX_id = 64
dir_2_gal_all = os.path.join(os.environ[env], "cat_GALAXY_all")
path_2_gal_all_catalog = os.path.join(dir_2_gal_all, str(HEALPIX_id).zfill(6) + '.fit')
path_2_ER_file = os.path.join(os.environ[env], "cat_eROCGAL", str(HEALPIX_id).zfill(6) + '.fit' )
path_2_CH_file = os.path.join(os.environ[env], "cat_CHANCES_clu", str(HEALPIX_id).zfill(6) + '.fit' )


# simulation setup
if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

omega = lambda zz: cosmo.Om0*(1+zz)**3. / cosmo.efunc(zz)**2
DeltaVir_bn98 = lambda zz : (18.*n.pi**2. + 82.*(omega(zz)-1)- 39.*(omega(zz)-1)**2.)/omega(zz)

# model of the quiescent fraction vs redshift and radius
def frac_old(x, z_cluster): return (erf((-n.log10(x) + 0.1)/0.6)+0.9)*0.38 * (1+z_cluster)**(-0.65)+0.22

# functions to compute the SFR for quiescent galaxies
def beta_z(z): return -0.57 * z + 1.43
def alpha_z(z): return 6.32 * z - 16.26
def mean_SFR_Q(mass, z): return mass * beta_z(z) + alpha_z(z)
def scale_z(z): return -0.34 * z + 0.99

def add_quiescent_flag(path_2_file):
    # INCREASE QUIESCENT FRACTIONS !
    print(path_2_file)
    hdu_clu = Table.read(path_2_file)
    x = ((hdu_clu['HALO_x']-hdu_clu['HOST_HALO_x'])**2. + (hdu_clu['HALO_y']-hdu_clu['HOST_HALO_y'])**2. + (hdu_clu['HALO_z']-hdu_clu['HOST_HALO_z'])**2.)**0.5
    #is_quiescent = hdu_clu['is_quiescent']
    zr_CLU = hdu_clu['redshift_R']
    mass = hdu_clu['galaxy_SMHMR_mass']
    log_sfr = hdu_clu['galaxy_star_formation_rate']

    # defined quenched using UNIVERSE Machine mass and SFR
    log_ssfr = log_sfr - mass
    is_quiescent = (log_ssfr < -10.3)
    print('N_quiescent start',len(is_quiescent.nonzero()[0]))
    #print('N_SF_already_there',len(zr_CLU)-len(is_quiescent.nonzero()[0]))

    HOST_HALO_Mvir = hdu_clu['HOST_HALO_Mvir'] / h
    HOST_HALO_Rvir = hdu_clu['HOST_HALO_Rvir']
    HOST_HALO_M500c = hdu_clu['HOST_HALO_M500c'] / h
    HOST_HALO_R500c = (DeltaVir_bn98(hdu_clu['redshift_R'])/500. * HOST_HALO_M500c / HOST_HALO_Mvir)**(1./3.)*HOST_HALO_Rvir
    HOST_HALO_M200c = hdu_clu['HOST_HALO_M200c'] / h
    HOST_HALO_R200c = (DeltaVir_bn98(hdu_clu['redshift_R'])/200. * HOST_HALO_M200c / HOST_HALO_Mvir)**(1./3.)*HOST_HALO_Rvir

    if delta_crit == '200c' :
        frac_rvir = HOST_HALO_R200c/HOST_HALO_Rvir
        RADIUS = HOST_HALO_R200c
    if delta_crit == '500c' :
        frac_rvir = HOST_HALO_R500c/HOST_HALO_Rvir
        RADIUS = HOST_HALO_R500c
    if delta_crit == 'vir' :
        frac_rvir = HOST_HALO_Rvir/HOST_HALO_Rvir
        RADIUS = hdu_clu_bin['HOST_HALO_Rvir']
    if delta_crit == '2rvir' :
        frac_rvir = 2.*n.ones_like(HOST_HALO_R200c)
        RADIUS = 2.*hdu_clu_bin['HOST_HALO_Rvir']

    #R_MIN_frac = 0.0
    #R_MAX_frac = 0.1
    for R_MIN_frac, R_MAX_frac in zip(n.arange(0,1,0.1), n.arange(0,1,0.1)+0.1):
        #print(R_MIN_frac, R_MAX_frac)
        s_RMAX = ( x <= RADIUS * R_MAX_frac / 1000. ) & ( x > RADIUS * R_MIN_frac / 1000. )
        f_red = frac_old(x[s_RMAX], zr_CLU[s_RMAX])
        rds = n.random.random(len(x[s_RMAX]))
        is_red = (rds < f_red)
        #is_1rvir = (x <= RADIUS * R_MAX_frac / 1000.)
        N_red_1rvir_needed = nl(is_red)
        N_quiescent_1rvir_already_there = nl(is_quiescent[s_RMAX])
        N_SF_1rvir_already_there = nl((is_quiescent[s_RMAX]==False))
        #print('N_red_1rvir_needed', N_red_1rvir_needed)
        #print('N_quiescent_1rvir_already_there', N_quiescent_1rvir_already_there)
        #print('N_SF_1rvir_already_there', N_SF_1rvir_already_there)
        N_red_1rvir_to_assign = N_red_1rvir_needed - N_quiescent_1rvir_already_there
        #print('N_red_1rvir_to_assign', N_red_1rvir_to_assign)
        if N_red_1rvir_to_assign<0:
            N_red_1rvir_to_assign = abs(N_red_1rvir_to_assign)
            if N_red_1rvir_to_assign<N_quiescent_1rvir_already_there:
                #print('remove QU and make them SF', N_red_1rvir_to_assign)
                to_sample_from = ( s_RMAX ) & ( is_quiescent )
                ids_to_sample_from = n.arange(len(to_sample_from))[to_sample_from]
                ids_2_reassign = n.random.choice( ids_to_sample_from, N_red_1rvir_to_assign )
                is_quiescent[ids_2_reassign] = False
            else:
                N_red_1rvir_to_assign = N_quiescent_1rvir_already_there
                #print('remove QU and make them SF', N_red_1rvir_to_assign)
                to_sample_from = ( s_RMAX ) & ( is_quiescent )
                ids_to_sample_from = n.arange(len(to_sample_from))[to_sample_from]
                ids_2_reassign = n.random.choice( ids_to_sample_from, N_red_1rvir_to_assign )
                is_quiescent[ids_2_reassign] = False

        elif N_red_1rvir_to_assign>=0:
            #print('remove SF and make them QU', N_red_1rvir_to_assign)
            if N_red_1rvir_to_assign<N_SF_1rvir_already_there:
                to_sample_from = ( s_RMAX ) & ( is_quiescent == False )
                ids_to_sample_from = n.arange(len(to_sample_from))[to_sample_from]
                ids_2_reassign = n.random.choice( ids_to_sample_from, N_red_1rvir_to_assign )
                is_quiescent[ids_2_reassign] = True
            else:
                N_red_1rvir_to_assign = N_SF_1rvir_already_there
                to_sample_from = ( s_RMAX ) & ( is_quiescent == False )
                ids_to_sample_from = n.arange(len(to_sample_from))[to_sample_from]
                ids_2_reassign = n.random.choice( ids_to_sample_from, N_red_1rvir_to_assign )
                is_quiescent[ids_2_reassign] = True
        else:
            print('already well balanced')

    # defined quenched using UNIVERSE Machine mass and SFR
    print('N_quiescent_final',len(is_quiescent.nonzero()[0]))

    rds3 = norm.rvs(loc=0, scale=1., size=len(hdu_clu) )
    log_sfr_Q = mean_SFR_Q(mass, zr_CLU) + rds3

    # update the file SFR and is_quiescent columns
    hdu_clu['galaxy_star_formation_rate_Quiescent'] = log_sfr_Q
    hdu_clu['is_quiescent'] = is_quiescent
    hdu_clu.write(path_2_file, overwrite=True)

path_2_ER_files = n.array( glob.glob( os.path.join(os.environ[env], "cat_eROCGAL", '000???.fit' )    ) )
path_2_CH_files = n.array( glob.glob( os.path.join(os.environ[env], "cat_CHANCES_clu", '000???.fit' )) )
for path_2_ER_file in path_2_ER_files:
    add_quiescent_flag(path_2_ER_file)
for path_2_CH_file in path_2_CH_files:
    add_quiescent_flag(path_2_CH_file)

###### find them among the non_quiescent
#####SF = (is_quiescent == False) & (is_1rvir)
#####rds2 = n.random.random(len(x[SF]))
#####is_1rvir2 = (x[SF] < RADIUS[SF]/1000.)
#####f_red2 = frac_old(x[SF], zr_CLU[SF])


#####def func(DELTA_X):
	#####change_2_red = (rds2 < f_red2 - DELTA_X)
	#####N_new_red = len(change_2_red[is_1rvir2].nonzero()[0])
	###### print(N_new_red)
	#####return N_red_1rvir_to_assign - N_new_red  # , change_2_red


#####VALS = interp1d(n.array([ func(xxx) for xxx in n.arange(0,1,0.001) ]), n.arange(0,1,0.001) )
######print(VALS(0))
#####change_2_red = (rds2 < f_red2 - VALS(0))[(x[SF] < RADIUS[SF]/1000.)]

#####is_quiescent[(SF) & (x < RADIUS/1000.)] = change_2_red

#####rds3 = norm.rvs(loc=0, scale=1., size=len(
	#####zr_CLU[(SF) & (x < 1)])) * scale_z(zr_CLU[(SF) & (x < 1)])
#####log_sfr_Q = mean_SFR_Q(mass[(SF) & (x < 1)], zr_CLU[(SF) & (x < 1)]) + rds3
###### change SFR for the quiesent selection
#####log_sfr[(SF) & (x < 1)] = log_sfr_Q

######else:
	######print('simulation is too incomplete to change red fractions')
