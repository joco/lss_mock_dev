"""
What it does
------------

Add J magnitudes to galaxy catalogue
Cuts at 10.5 in stellar mass

Command to run
--------------

python3 009_0_J_magnitude.py MD10


python3 009_0_J_magnitude.py MD04

ls /data17s/darksim/simulation_3/MD/MD_0.4Gpc/cat_bestmost/0000*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits icmd='select "abs(g_lat)>10"' omode=out ofmt=fits out=/home/comparat/data2/firefly/mocks/2020-10/SMDPL/bestmost0.fits
ls /data17s/darksim/simulation_3/MD/MD_0.4Gpc/cat_bestmost/0001*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/SMDPL/bestmost1.fits
ls /data17s/darksim/simulation_3/MD/MD_0.4Gpc/cat_bestmost/0002*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/SMDPL/bestmost2.fits
ls /data17s/darksim/simulation_3/MD/MD_0.4Gpc/cat_bestmost/0003*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/SMDPL/bestmost3.fits
ls /data17s/darksim/simulation_3/MD/MD_0.4Gpc/cat_bestmost/0004*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/SMDPL/bestmost4.fits
ls /data17s/darksim/simulation_3/MD/MD_0.4Gpc/cat_bestmost/0005*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/SMDPL/bestmost5.fits
ls /data17s/darksim/simulation_3/MD/MD_0.4Gpc/cat_bestmost/0006*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/SMDPL/bestmost6.fits
ls /data17s/darksim/simulation_3/MD/MD_0.4Gpc/cat_bestmost/0007*.fit > bestmost.list
stilts tcat in=@bestmost.list  ifmt=fits omode=out ofmt=fits icmd='select "abs(g_lat)>10"' out=/home/comparat/data2/firefly/mocks/2020-10/SMDPL/bestmost7.fits
cd /home/comparat/data2/firefly/mocks/2020-10/SMDPL
chmod ugo+rX *

"""

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u

#from astropy_healpix import healpy
import sys
import os
import time
from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column
from scipy.optimize import curve_fit
#import linmix
import astropy.io.fits as fits
#import h5py
import numpy as n
print('CREATES FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

env = sys.argv[1]
print(env)

if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT


zs = n.arange(0.0000001, 7.1, 0.001)
dm_itp = interp1d(zs, cosmo.distmod(zs).value)

root_dir = os.path.join(os.environ[env])

dir_2_OUT = os.path.join(root_dir, "cat_bestmost")
dir_2_IN = os.path.join(root_dir, "cat_GALAXY_all")


def add_MAG(t_survey):
	sim_redshift = t_survey['redshift_R']
	DM = dm_itp(sim_redshift)
	Mass = t_survey['galaxy_SMHMR_mass']
	SFR = t_survey['galaxy_star_formation_rate']
	M_J = -25.84 +2.4*(12-Mass)-0.3*(SFR-Mass+11.5)
	m_J = M_J + DM
	M_I=M_J+0.5+0.3*(Mass-10)
	m_I = M_I + DM
	#selection = (sim_redshift<0.2) & (t_survey['DEC']<20) & (t_survey['DEC']>-80) & ( m_I < 19.5 )
	selection = (sim_redshift>0.1) & (sim_redshift<0.6) & (t_survey['DEC']<20) & (t_survey['DEC']>-80) & ( m_J < 20.5 )
	return t_survey[selection], m_J[selection], m_I[selection] 


import healpy
N_pixels = healpy.nside2npix(8)
for HEALPIX_id in n.arange(N_pixels):
	print(HEALPIX_id)
	path_2_output = os.path.join(dir_2_OUT, str(HEALPIX_id).zfill(6) + '.fit')
	path_2_input = os.path.join(dir_2_IN, str(HEALPIX_id).zfill(6) + '.fit')
	t_survey = Table.read( path_2_input )
	t_out, j_mag, i_mag = add_MAG(t_survey)
	if len(t_out)>0:
		t_out.add_column(Column(name='mag_J', data=j_mag, unit='mag'))
		t_out.add_column(Column(name='mag_I', data=i_mag, unit='mag'))
		print(path_2_output)
		t_out.write (path_2_output  , overwrite=True)

"""
file_name = '/home/comparat/data2/firefly/mocks/2020-10/SMDPL/000000.fit'
t_survey = Table.read(file_name)
sim_redshift = t_survey['redshift_R']
DM = dm_itp(sim_redshift)
Mass = t_survey['galaxy_SMHMR_mass']
SFR = t_survey['galaxy_star_formation_rate']
M_J = -25.84 +2.4*(12-Mass)-0.3*(SFR-Mass+11.5)
m_J = M_J + DM
M_I=M_J+0.5+0.3*(Mass-10)
m_I = M_I + DM
t_survey.add_column(Column(name='mag_J', data=m_J, unit='mag'))
t_survey.add_column(Column(name='mag_I', data=m_I, unit='mag'))
t_survey.write (file_name  , overwrite=True)

file_name = '/home/comparat/data2/firefly/mocks/2020-10/MDPL2/000000.fit'
t_survey = Table.read(file_name)
sim_redshift = t_survey['redshift_R']
DM = dm_itp(sim_redshift)
Mass = t_survey['galaxy_SMHMR_mass']
SFR = t_survey['galaxy_star_formation_rate']
M_J = -25.84 +2.4*(12-Mass)-0.3*(SFR-Mass+11.5)
m_J = M_J + DM
M_I=M_J+0.5+0.3*(Mass-10)
m_I = M_I + DM
t_survey.add_column(Column(name='mag_J', data=m_J, unit='mag'))
t_survey.add_column(Column(name='mag_I', data=m_I, unit='mag'))
t_survey.write (file_name  , overwrite=True)

"""
