"""
Each files has the structure:
— ‘Lproj' # line of sight projection length in kph/h
— ‘redshift’ # redshift of the snapshot 
— 'resolution [kpc/h]’ # width-length of one pixel in kpc/h 
— ‘halos’
    |— ‘a number for each halo’
        |— 'M200c', 'M200m', 'M500c', ‘M500m’ # mass in the grav-only run in 10^10 Msol/h
        |— 'hydro_M200c', 'hydro_M200m', 'hydro_M500c', ‘hydro_M500m’ #  mass in the hydro run in 10^10 Msol/h
        |— ‘has_profile’ # bool flagging if this cluster has a profile
        |— ‘dm_Sigma’ # projected mass in Gravity-only, 10^10 Msol/h (so to get a surface density, divide this by the bin area). Also note, the z~0.78 snapshot has not grav-only maps
        |— ‘hydro_Sigma’ # projected mass in hydro, 10^10 Msol/h


step 1 : attach (resscale) to a DM halo in the light cone
step 2 : predict reduced shear profile (DES, HSC, KIDS, ...)
         https://arxiv.org/pdf/2103.16212.pdf offers a complete list of what is known to date

python 004_11_predict_shear_v2.py
"""

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as plt


import h5py

from scipy.special import erf
from astropy.table import Table, Column, vstack, hstack
import sys, os, time
import astropy.units as u
import astropy.io.fits as fits
import numpy as n
from scipy.interpolate import interp1d
from scipy.interpolate import interp2d
print('CREATES WL profiles for eROSITA CLUSTER')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()


fig_dir = os.path.join( os.environ['GIT_AGN_MOCK'], 'figures/WL' )

env = "UNIT_fA1i_DIR"
root_dir = os.path.join(os.environ[env])
path_2_catalog = os.path.join(root_dir, "UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS_Sept2021.fits")
path_2_out = os.path.join(root_dir, "UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS_Sept2021_withShear_v2.fits")

t_1 = Table.read(path_2_catalog)
print(path_2_catalog, 'opened')

s_area = ( abs( t_1['DEC'] ) < 10 ) & ( abs(t_1['g_lat']) > 20 )
s_1 = ( s_area ) & ( t_1['HALO_pid']==-1 ) & ( t_1['galaxy_SMHMR_mass']>10. ) & ( n.log10(t_1['HALO_Mvir']) > 13 ) & ( t_1['detectable'] ) & ( n.log10(t_1['HALO_M500c']) > 14.0 )
t_2 = t_1[s_1]
print('N in catalogue = ', len(t_1))
print('Select: distinct haloes with stellar mass>1e10 & Mvir>1e13 & M500c>1e14 & detectable by eROSITA (eRASS8). Equatorial area selected : |Glat|>10 & |dec<10|')
print('N clusters of interest = ', len(t_2))

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
# UNIT cosmology setup
cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)

# Magneticum cosmology setup
cosmoBox1 = FlatLambdaCDM(H0=70.4 * u.km / u.s / u.Mpc, Om0=0.272)
h = 0.704
hz = lambda redshift : cosmoBox1.H( redshift ).value/100

#
map_unit = 1e10 # Msun/h
#
X_res = 20. # kpc/h

path_2_WL_3_0 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_052_Rotation0.fits"
path_2_WL_2_0 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_072_Rotation0.fits"
path_2_WL_1_0 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_096_Rotation0.fits"
path_2_WL_0_0 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_116_Rotation0.fits"

path_2_WL_3_1 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_052_Rotation1.fits"
path_2_WL_2_1 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_072_Rotation1.fits"
path_2_WL_1_1 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_096_Rotation1.fits"
path_2_WL_0_1 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_116_Rotation1.fits"

path_2_WL_3_2 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_052_Rotation2.fits"
path_2_WL_2_2 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_072_Rotation2.fits"
path_2_WL_1_2 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_096_Rotation2.fits"
path_2_WL_0_2 = "/home/comparat/data/WLSim/halocat_Box1a_maps_mr_116_Rotation2.fits"

# at redshifts 0.25, 0.48, 0.78, 1.18

z0 = 0.25208907
z1 = 0.47019408
z2 = 0.78263327
z3 = 1.17889506

# reads all rotations
ts0 = vstack(( Table.read(path_2_WL_0_0), Table.read(path_2_WL_0_1), Table.read(path_2_WL_0_2) ))
ts0 = ts0[n.argsort(ts0['M500c'])]
print('mass maps loaded:', path_2_WL_0_0, path_2_WL_0_1, path_2_WL_0_2)
# MATCHING

# slice the lightcone to assign to the closest redshift snapshot
print('retrieve clusters within z<', (z0 + z1)/2)
sel0 = ( t_2['redshift_R'] <= (z0 + z1)/2 )
t = t_2[sel0]
print('N clusters in that range', len(t))

print('matches with sigma -- M relation')
# use M-sigma relation to find the right halo at a different redshift
from colossus.cosmology import cosmology
my_CM = {'flat': True, 'H0':70.4, 'Om0': 0.272, 'Ob0': 0.0457, 'sigma8': 0.809, 'ns': 0.963}
CM = cosmology.setCosmology('my_cosmo', my_CM)
print(CM)
from colossus.lss import mass_function
from colossus.lss import peaks
nu = n.arange(0.4, 5.0, 0.01)
M = peaks.massFromPeakHeight(nu, z0)
deltac = peaks.collapseOverdensity(corrections=True, z=z0)
sigma_m = deltac/nu
itp_z0 = interp1d(sigma_m, M)

peak_heights_z = peaks.peakHeight(t['HALO_M500c']*0.6777, t['redshift_R'])
deltac_z = peaks.collapseOverdensity(corrections=True, z=t['redshift_R'])
sigma_m_mock = deltac_z/peak_heights_z
m_z0 = itp_z0(sigma_m_mock)

# id match in M500c
idx_sort = n.searchsorted( n.log10( ts0['M500c']*1e10 / hz(z0) ), n.log10(m_z0) )
idx_sort[idx_sort==len(ts0['M500c'])] = len(ts0['M500c']) - 1
#
# make sure all profiles are used not the sorted ones 
# 0,1,2
# 

##
#
# quantities for the eROSITA mock halo catalogue
#
##

##
#
# RICHNESS
#
##
print('Computes richness')

from scipy.stats import norm
ln_A_lambda = n.log( 76.9 ) # N(78.5, 8.2^2)
B_lambda = 1.020            # N(1.02, 0.08^2)
C_lambda = 0.29             # N(0.29, 0.27^2)
sigma_lambda = 0.23         # lnSigma_lambda : N(ln(0.23, 0.16^2)  
# Should we draw the parameters ? No, but we could do different flavours of it.

avg_lambda = lambda M200c, zz : ln_A_lambda + B_lambda * n.log( M200c / 3e14 ) + C_lambda * n.log ( cosmoBox1.efunc(zz) / cosmoBox1.efunc(0.6) )
sig2_tot = lambda M200c, zz : n.e**( 2*n.log(sigma_lambda) ) + (n.e**(avg_lambda(M200c, zz)) - 1 ) / (n.e**( 2*avg_lambda(M200c, zz) ) )
rvs_lam = lambda M200c, zz : n.e**(norm.rvs(loc=avg_lambda(M200c, zz), scale=sig2_tot(M200c, zz), size=1 ))[0]

R_lambda = lambda richness : (richness/100)**0.2 # Mpc/h

richness = n.array([ rvs_lam(m2, z2) for m2, z2 in zip(t['HALO_M200c']*0.6777, t['redshift_R']) ])
t['lambda'] = richness
t['R_lambda'] = R_lambda(richness)

##
#
# MIS-CENTERING
#
##
print('computes mis-centering')
from scipy.stats import rayleigh
from scipy.stats import rv_histogram
# Bleem 2020 with a Gama distribution
# B20_rho = 0.87
# B20_sig = 0.12
# B20_tau = 0.69
# G20 parameters (mean)
rho  = 0.63 # N(0.63, 0.06^2)
sig0 = 0.07 # N(0.07, 0.02^2)
sig1 = 0.25 # N(0.25, 0.07^2)
# Look at I-non's paper to decide the functional form and parameters

rvs0 = rayleigh.rvs(scale=sig0, size = 1000000 )
rvs1 = rayleigh.rvs(scale=sig1, size = 1000000 )
out0, bins0 = n.histogram(rvs0, bins = 100000)
out1 = n.histogram(rvs1, bins=bins0)[0]
pds_hist = (out0).astype('float')*rho+(1-rho)*(out1).astype('float')
hist_dist = rv_histogram((pds_hist, bins0))

Rmis = hist_dist.rvs(size=len(t)) * t['R_lambda']
t['Rmis'] = Rmis

# draw a uniform RVS
# if smaller than rho => component 0 draw R0, else component 1 draw R0
 

##
#
# Source redshift distribution
#
##
print('computes Source redshift distribution')
z_s0 = 0.2
delta_beta = 0.025
sigma_beta = lambda z_cl : 0.02 * (1+z_cl**2)**2 / 1.49**2
P_zs = lambda z_s : 0.5 * z_s0 ** -3 * z_s**2 * n.e**( - z_s / z_s0 )
#
# to be updated with HSC S19A, DES Yr3, KIDS DR4.1 N(z) in due time
# in ~ 1 month
# 
z_array = n.arange(0.001, 2, 0.001)
z_bins = n.hstack(( z_array - 0.001/2., z_array.max() + 0.001/2. ))
PDZ = P_zs(z_array)
hist_dist_z = rv_histogram((PDZ, z_bins))
rd_zs = hist_dist_z.rvs(size=1000000)

mean_zs = n.array([  n.mean(rd_zs[rd_zs>zCL+0.1]) for zCL in t['redshift_R'] ])
t['mean_zs'] = mean_zs

##
#
# Critical surface density
#
##
print('Critical surface density')
from lenspack.utils import sigma_critical
sig_crit = sigma_critical( t['redshift_R'], t['mean_zs'], cosmoBox1).to( u.solMass / (u.Mpc*u.Mpc) )
t['sigma_critical_raw'] = sig_crit
t['sigma_critical'] = sig_crit * ( 1 + delta_beta + sigma_beta(t['redshift_R']))
# assume some source density with pdz (weights)
# create array of source redshifts
# evaluate sigma critical at each SUM ( z_s X pdz(z_s) )/ (sum (pdz(z_s))
# 


##
#
# Kappa profile
#
##

R_values = 10**n.arange(1.2, 3.65, 0.2)
#R_values = n.hstack(( 10**n.arange(1.2, 2.1, 0.2), 10**n.arange(2.2, 2.95, 0.1), 10**n.arange(3, 3.65, 0.05) ))
t['Sigma_values_dm_atR_Rmis'] = n.zeros( (len(t), len(R_values) -1 ) )
t['Sigma_values_hd_atR_Rmis'] = n.zeros( (len(t), len(R_values) -1 ) )
t['Sigma_values_dm_atR']      = n.zeros( (len(t), len(R_values) -1 ) )
t['Sigma_values_hd_atR']      = n.zeros( (len(t), len(R_values) -1 ) )

t['Sigma_values_dm_inR1_Rmis'] = n.zeros( (len(t), len(R_values) -1 ) )
t['Sigma_values_hd_inR1_Rmis'] = n.zeros( (len(t), len(R_values) -1 ) )
t['Sigma_values_dm_inR1']      = n.zeros( (len(t), len(R_values) -1 ) )
t['Sigma_values_hd_inR1']      = n.zeros( (len(t), len(R_values) -1 ) )

t['Sigma_values_dm_inR0_Rmis'] = n.zeros( (len(t), len(R_values) -1 ) )
t['Sigma_values_hd_inR0_Rmis'] = n.zeros( (len(t), len(R_values) -1 ) )
t['Sigma_values_dm_inR0']      = n.zeros( (len(t), len(R_values) -1 ) )
t['Sigma_values_hd_inR0']      = n.zeros( (len(t), len(R_values) -1 ) )

# 
# one could pre-save maps at fixed Rmis in the magneticum files to gain time
# 

for input_id, clu_id in zip(n.arange(len(t)), idx_sort):
    print(input_id, clu_id)

    X_array = X_res * ( n.arange(406) - 406/2 ) + X_res/2 + t['Rmis'][input_id]*1000
    # physical mis-centring might nedd more information
    X_grid = n.meshgrid(X_array, X_array)[0]
    R_grid = (X_grid**2 + X_grid**2)**0.5

    def get_dm_hd_atR(clu_id):
        Sigma_dm_in_dR_pkpc2 = lambda R0, R1 : n.sum(ts0['dm_Sigma']   [clu_id][ ( R_grid >= R0 ) & ( R_grid < R1 )])/(n.pi * ( (R1/1000)**2 - (R0/1000)**2) )
        Sigma_hd_in_dR_pkpc2 = lambda R0, R1 : n.sum(ts0['hydro_Sigma'][clu_id][ ( R_grid >= R0 ) & ( R_grid < R1 )])/(n.pi * ( (R1/1000)**2 - (R0/1000)**2) )
        Sigma_values_dm_indR = map_unit * n.array([Sigma_dm_in_dR_pkpc2(R0, R1) for R0, R1 in zip(R_values[:-1], R_values[1:]) ])
        Sigma_values_hd_indR = map_unit * n.array([Sigma_hd_in_dR_pkpc2(R0, R1) for R0, R1 in zip(R_values[:-1], R_values[1:]) ])
        return Sigma_values_dm_indR, Sigma_values_hd_indR

    Sigma_values_dm_inR, Sigma_values_hd_inR = get_dm_hd_atR(clu_id)
    t['Sigma_values_dm_atR_Rmis'] [input_id] = Sigma_values_dm_inR
    t['Sigma_values_hd_atR_Rmis'] [input_id] = Sigma_values_hd_inR

    print(input_id, clu_id)
    def get_dm_hd_RR(clu_id):
        Sigma_dm_in_R_pkpc2 = lambda RR : n.sum(ts0['dm_Sigma']   [clu_id][ ( R_grid < RR ) ]) #/ ( len( n.ravel ( ts0['dm_Sigma'][clu_id][ ( R_grid < RR ) ] ) ) *X_res * X_res )
        Sigma_hd_in_R_pkpc2 = lambda RR : n.sum(ts0['hydro_Sigma'][clu_id][ ( R_grid < RR ) ]) #/ ( len( n.ravel ( ts0['dm_Sigma'][clu_id][ ( R_grid < RR ) ] ) ) *X_res * X_res )
        #label = r"$\log_{10}(M_{500c}[h^{-1}M_\odot])=$"+str(n.round(n.log10(ts0['M500c'][clu_id]*1e10),1))
        Sigma_values_dm_inR = map_unit * n.array([Sigma_dm_in_R_pkpc2(RR) for RR in R_values[1:]])/(n.pi * (R_values[1:]/1000)**2)# - n.hstack(( 0., R_values[:-1]/1000)) )
        Sigma_values_hd_inR = map_unit * n.array([Sigma_hd_in_R_pkpc2(RR) for RR in R_values[1:]])/(n.pi * (R_values[1:]/1000)**2)# - n.hstack(( 0., R_values[:-1]/1000)) )
        return Sigma_values_dm_inR, Sigma_values_hd_inR

    Sigma_values_dm_inR, Sigma_values_hd_inR = get_dm_hd_RR(clu_id)

    t['Sigma_values_dm_inR1_Rmis'] [input_id] = Sigma_values_dm_inR
    t['Sigma_values_hd_inR1_Rmis'] [input_id] = Sigma_values_hd_inR


    print(input_id, clu_id)
    def get_dm_hd_RR(clu_id):
        Sigma_dm_in_R_pkpc2 = lambda RR : n.sum(ts0['dm_Sigma']   [clu_id][ ( R_grid < RR ) ]) #/ ( len( n.ravel ( ts0['dm_Sigma'][clu_id][ ( R_grid < RR ) ] ) ) *X_res * X_res )
        Sigma_hd_in_R_pkpc2 = lambda RR : n.sum(ts0['hydro_Sigma'][clu_id][ ( R_grid < RR ) ]) #/ ( len( n.ravel ( ts0['dm_Sigma'][clu_id][ ( R_grid < RR ) ] ) ) *X_res * X_res )
        #label = r"$\log_{10}(M_{500c}[h^{-1}M_\odot])=$"+str(n.round(n.log10(ts0['M500c'][clu_id]*1e10),1))
        Sigma_values_dm_inR = map_unit * n.array([Sigma_dm_in_R_pkpc2(RR) for RR in R_values[:-1]])/(n.pi * (R_values[:-1]/1000)**2)# - n.hstack(( 0., R_values[:-1]/1000)) )
        Sigma_values_hd_inR = map_unit * n.array([Sigma_hd_in_R_pkpc2(RR) for RR in R_values[:-1]])/(n.pi * (R_values[:-1]/1000)**2)# - n.hstack(( 0., R_values[:-1]/1000)) )
        return Sigma_values_dm_inR, Sigma_values_hd_inR

    Sigma_values_dm_inR, Sigma_values_hd_inR = get_dm_hd_RR(clu_id)

    t['Sigma_values_dm_inR0_Rmis'] [input_id] = Sigma_values_dm_inR
    t['Sigma_values_hd_inR0_Rmis'] [input_id] = Sigma_values_hd_inR




X_array = X_res * ( n.arange(406) - 406/2 ) + X_res/2
X_grid = n.meshgrid(X_array, X_array)[0]
R_grid = (X_grid**2 + X_grid**2)**0.5

for input_id, clu_id in zip(n.arange(len(t)), idx_sort):
    print(input_id, clu_id)

    #R_values = n.hstack(( 10**n.arange(1.2, 2.1, 0.2), 10**n.arange(2.2, 2.95, 0.1), 10**n.arange(3, 3.65, 0.05) ))
    #R_values_atR = n.hstack(( 10**n.arange(1.2, 2.1, 0.2), 10**n.arange(2.2, 2.95, 0.1), 10**n.arange(3, 3.65, 0.05) ))

    def get_dm_hd_atR(clu_id):
        Sigma_dm_in_dR_pkpc2 = lambda R0, R1 : n.sum(ts0['dm_Sigma']   [clu_id][ ( R_grid >= R0 ) & ( R_grid < R1 )])/(n.pi * ( (R1/1000)**2 - (R0/1000)**2) )
        Sigma_hd_in_dR_pkpc2 = lambda R0, R1 : n.sum(ts0['hydro_Sigma'][clu_id][ ( R_grid >= R0 ) & ( R_grid < R1 )])/(n.pi * ( (R1/1000)**2 - (R0/1000)**2) )
        Sigma_values_dm_indR = map_unit * n.array([Sigma_dm_in_dR_pkpc2(R0, R1) for R0, R1 in zip(R_values[:-1], R_values[1:]) ])
        Sigma_values_hd_indR = map_unit * n.array([Sigma_hd_in_dR_pkpc2(R0, R1) for R0, R1 in zip(R_values[:-1], R_values[1:]) ])
        return Sigma_values_dm_indR, Sigma_values_hd_indR

    Sigma_values_dm_inR, Sigma_values_hd_inR = get_dm_hd_atR(clu_id)

    t['Sigma_values_dm_atR'] [input_id] = Sigma_values_dm_inR
    t['Sigma_values_hd_atR'] [input_id] = Sigma_values_hd_inR

    print(input_id, clu_id)
    def get_dm_hd_RR(clu_id):
        Sigma_dm_in_R_pkpc2 = lambda RR : n.sum(ts0['dm_Sigma']   [clu_id][ ( R_grid < RR ) ]) #/ ( len( n.ravel ( ts0['dm_Sigma'][clu_id][ ( R_grid < RR ) ] ) ) *X_res * X_res )
        Sigma_hd_in_R_pkpc2 = lambda RR : n.sum(ts0['hydro_Sigma'][clu_id][ ( R_grid < RR ) ]) #/ ( len( n.ravel ( ts0['dm_Sigma'][clu_id][ ( R_grid < RR ) ] ) ) *X_res * X_res )
        #label = r"$\log_{10}(M_{500c}[h^{-1}M_\odot])=$"+str(n.round(n.log10(ts0['M500c'][clu_id]*1e10),1))
        Sigma_values_dm_inR = map_unit * n.array([Sigma_dm_in_R_pkpc2(RR) for RR in R_values[1:]])/(n.pi * (R_values[1:]/1000)**2)# - n.hstack(( 0., R_values[:-1]/1000)) )
        Sigma_values_hd_inR = map_unit * n.array([Sigma_hd_in_R_pkpc2(RR) for RR in R_values[1:]])/(n.pi * (R_values[1:]/1000)**2)# - n.hstack(( 0., R_values[:-1]/1000)) )
        return Sigma_values_dm_inR, Sigma_values_hd_inR

    Sigma_values_dm_inR, Sigma_values_hd_inR = get_dm_hd_RR(clu_id)

    t['Sigma_values_dm_inR1'] [input_id] = Sigma_values_dm_inR
    t['Sigma_values_hd_inR1'] [input_id] = Sigma_values_hd_inR


    print(input_id, clu_id)
    def get_dm_hd_RR(clu_id):
        Sigma_dm_in_R_pkpc2 = lambda RR : n.sum(ts0['dm_Sigma']   [clu_id][ ( R_grid < RR ) ]) #/ ( len( n.ravel ( ts0['dm_Sigma'][clu_id][ ( R_grid < RR ) ] ) ) *X_res * X_res )
        Sigma_hd_in_R_pkpc2 = lambda RR : n.sum(ts0['hydro_Sigma'][clu_id][ ( R_grid < RR ) ]) #/ ( len( n.ravel ( ts0['dm_Sigma'][clu_id][ ( R_grid < RR ) ] ) ) *X_res * X_res )
        #label = r"$\log_{10}(M_{500c}[h^{-1}M_\odot])=$"+str(n.round(n.log10(ts0['M500c'][clu_id]*1e10),1))
        Sigma_values_dm_inR = map_unit * n.array([Sigma_dm_in_R_pkpc2(RR) for RR in R_values[:-1]])/(n.pi * (R_values[:-1]/1000)**2)# - n.hstack(( 0., R_values[:-1]/1000)) )
        Sigma_values_hd_inR = map_unit * n.array([Sigma_hd_in_R_pkpc2(RR) for RR in R_values[:-1]])/(n.pi * (R_values[:-1]/1000)**2)# - n.hstack(( 0., R_values[:-1]/1000)) )
        return Sigma_values_dm_inR, Sigma_values_hd_inR

    Sigma_values_dm_inR, Sigma_values_hd_inR = get_dm_hd_RR(clu_id)

    t['Sigma_values_dm_inR0'] [input_id] = Sigma_values_dm_inR
    t['Sigma_values_hd_inR0'] [input_id] = Sigma_values_hd_inR



t['gamma_dmR1_R']      = n.divide( (t['Sigma_values_dm_inR1'] - t['Sigma_values_dm_atR'] ).T, t['sigma_critical']).T
t['gamma_dmR1_R_Rmis'] = n.divide( (t['Sigma_values_dm_inR1_Rmis'] - t['Sigma_values_dm_atR_Rmis'] ).T, t['sigma_critical']).T
t['gamma_hdR1_R']      = n.divide( (t['Sigma_values_hd_inR1'] - t['Sigma_values_hd_atR'] ).T, t['sigma_critical']).T
t['gamma_hdR1_R_Rmis'] = n.divide( (t['Sigma_values_hd_inR1_Rmis'] - t['Sigma_values_hd_atR_Rmis'] ).T, t['sigma_critical']).T

t['gamma_dmR0_R']      = n.divide( (t['Sigma_values_dm_inR0'] - t['Sigma_values_dm_atR'] ).T, t['sigma_critical']).T
t['gamma_dmR0_R_Rmis'] = n.divide( (t['Sigma_values_dm_inR0_Rmis'] - t['Sigma_values_dm_atR_Rmis'] ).T, t['sigma_critical']).T
t['gamma_hdR0_R']      = n.divide( (t['Sigma_values_hd_inR0'] - t['Sigma_values_hd_atR'] ).T, t['sigma_critical']).T
t['gamma_hdR0_R_Rmis'] = n.divide( (t['Sigma_values_hd_inR0_Rmis'] - t['Sigma_values_hd_atR_Rmis'] ).T, t['sigma_critical']).T

t['kappa_dm_R']      = n.divide( (t['Sigma_values_dm_atR'] ).T, t['sigma_critical']).T
t['kappa_dm_R_Rmis'] = n.divide( (t['Sigma_values_dm_atR_Rmis'] ).T, t['sigma_critical']).T
t['kappa_hd_R']      = n.divide( (t['Sigma_values_hd_atR'] ).T, t['sigma_critical']).T
t['kappa_hd_R_Rmis'] = n.divide( (t['Sigma_values_hd_atR_Rmis'] ).T, t['sigma_critical']).T

t['gamma_t_dm_R']       = t['gamma_dmR1_R']      / ( 1 - t['kappa_dm_R']      )
t['gamma_t_dm_R_Rmis']  = t['gamma_dmR1_R_Rmis'] / ( 1 - t['kappa_dm_R_Rmis'] )
t['gamma_t_hd_R']       = t['gamma_hdR1_R']      / ( 1 - t['kappa_hd_R']      )
t['gamma_t_hd_R_Rmis']  = t['gamma_hdR1_R_Rmis'] / ( 1 - t['kappa_hd_R_Rmis'] )

m_shear_bias = 0.01
sigma_m = 0.01

t['gamma_t_measured_dm_R']       = ( 1+ m_shear_bias + sigma_m ) * t['gamma_t_dm_R']
t['gamma_t_measured_dm_R_Rmis']  = ( 1+ m_shear_bias + sigma_m ) * t['gamma_t_dm_R_Rmis']
t['gamma_t_measured_hd_R']       = ( 1+ m_shear_bias + sigma_m ) * t['gamma_t_hd_R']
t['gamma_t_measured_hd_R_Rmis']  = ( 1+ m_shear_bias + sigma_m ) * t['gamma_t_hd_R_Rmis']

##
#
# cluster member contamination
#
##

A_fcl = 0.3
B_fcl = 0.15
c_fcl = 2.
f_bar = lambda X : X
f_cl = lambda radius, richness, R_lambda : A_fcl * ( richness / 67 ) ** B_fcl
f_cl = 1.

t['gamma_t_measured_diluted_dm_R']       = ( f_cl ) * t['gamma_t_dm_R']
t['gamma_t_measured_diluted_dm_R_Rmis']  = ( f_cl ) * t['gamma_t_dm_R_Rmis']
t['gamma_t_measured_diluted_hd_R']       = ( f_cl ) * t['gamma_t_hd_R']
t['gamma_t_measured_diluted_hd_R_Rmis']  = ( f_cl ) * t['gamma_t_hd_R_Rmis']



# Add uncertainties and scatter the prediction !

# Average to emulate stacks 

t.write(path_2_out, overwrite = True)

