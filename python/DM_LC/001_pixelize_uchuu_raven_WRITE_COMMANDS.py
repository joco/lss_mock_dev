
import numpy as n
import os, sys, glob
import healpy

LC_dir = os.path.join( os.environ["UCHUU"], "LC" )
list_dir = glob.glob( os.path.join( LC_dir, 'halodir_*' ) )

NSIDE = 16
grid = n.arange(6*512).reshape(6,512)

for el in list_dir: 
	snap_name = el.split('/')[-1]
	for pix_id, element in enumerate(grid):
		fn = '001_pixelize_uchuu_raven_COMMANDS/'+snap_name+"""_"""+str(pix_id).zfill(4)+'.sh'
		f=open(fn, 'w')
		f.write("""#!/bin/bash -l                              \n""")
		f.write("""#SBATCH -J PYTHON_SEQ                       \n""")
		f.write("""#SBATCH -o ./"""+snap_name+"""_"""+str(pix_id).zfill(4)+""".%j.out \n""")
		f.write("""#SBATCH -e ./"""+snap_name+"""_"""+str(pix_id).zfill(4)+""".%j.err \n""")
		f.write("""#SBATCH -D ./                               \n""")
		f.write("""#SBATCH --ntasks=1                          \n""")
		f.write("""#SBATCH --cpus-per-task=1                   \n""")
		f.write("""#SBATCH --mem=3800M                         \n""")
		f.write("""#SBATCH --time=20:00:00                      \n""")
		f.write("""module purge                                \n""")
		f.write("""module load anaconda/3/2020.02              \n""")
		f.write("""conda activate eroconda                     \n""")
		f.write("""export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK \n""")
		f.write("cd /u/joco/software/lss_mock_dev/python/DM_LC \n")
		for value in element :
			f.write('srun python 001_pixelize_uchuu_raven.py '+el+" "+str(value).zfill(4)+' \n')
		f.write(' \n')
		f.close()
