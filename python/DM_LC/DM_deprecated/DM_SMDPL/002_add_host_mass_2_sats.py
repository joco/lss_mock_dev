import numpy as n
import os
import sys

path_2_in = '1.00000'  # sys.argv[1]

top_dir = '/data17s/darksim/MD/MD_0.4Gpc/hlists/fits/'

cenFileName = os.path.join(top_dir, 'all_' + path_2_in + ".fits")
satFileName = os.path.join(top_dir, 'sat_' + path_2_in + ".fits")
outFileName = os.path.join(top_dir, 'satellites_' + path_2_in + ".fits")

command = """stilts tmatch2 ifmt1=fits in1=""" + satFileName + """ ifmt2=fits in2=""" + cenFileName + \
    """ icmd2='delcols "vmax x y z vx vy vz M200c M500c b_to_a_500c c_to_a_500c"' find=best matcher=exact join=all1 fixcols=all suffix1="" suffix2="_host" values1=pid values2=id omode=out ofmt=fits out=""" + outFileName

os.system(command)

# to a second depth and third depth match ...
