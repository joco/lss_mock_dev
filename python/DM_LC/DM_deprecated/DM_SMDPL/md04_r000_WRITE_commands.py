#!/bin/bash
import numpy as n
import glob

xx = sorted(
    n.array(
        glob.glob('/data17s/darksim/MD/MD_0.4Gpc/hlists/hlist*.list.bz2')))

for x in xx[::-1]:
    cmd = "python 000_process_hlists.py " + x
    cmd2 = "python 001_process_hlists_sat.py " + x[:-4]
    print(cmd)
    print(cmd2)
    print('  ')
