import numpy as n
import os
import sys
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from scipy.interpolate import interp1d

cosmoMD = FlatLambdaCDM(H0=67.77 * u.km / u.s / u.Mpc, Om0=0.307115)
L_box = 400.0 / 0.6777

# read shell list
A_snap = n.loadtxt(
    os.path.join(
        os.environ['GIT_AGN_MOCK'],
        'python',
        'DM_SMDPL',
        'snap_list.txt'),
    unpack=True)
Z_snap = 1. / A_snap - 1.
N_snap = n.arange(len(A_snap))

z_array = n.arange(0, 20., 0.001)
dcs = cosmoMD.comoving_distance(z_array)
dc_to_z = interp1d(dcs, z_array)
z_to_dc = interp1d(z_array, dcs)

DC_snap = z_to_dc(Z_snap)
DC_borders = 0.5 * (DC_snap[:-1] + DC_snap[1:])

DC_max = n.hstack((DC_snap[0], DC_borders))
DC_min = n.hstack((DC_borders, DC_snap[-1]))
n.savetxt(
    os.path.join(
        os.environ['GIT_AGN_MOCK'],
        'python',
        'DM_SMDPL',
        'snap_list_with_border.txt'),
    n.transpose(
        [
            N_snap,
            Z_snap,
            A_snap,
            DC_max,
            DC_min]))
