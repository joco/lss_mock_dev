"""
Replicates the snapshot over the sky and extracts halos in a given shell of comoving distance
Computes the coordinates of the 8 summits of the translated snapshot.

"""
import numpy as n
import os
import sys

L_box = 4000.0 / 0.6777
simulation_dir = 'DM_HMDPL'
N_pavement = 1

path_2_in_0 = sys.argv[1]

if path_2_in_0.split('.')[-1] == 'bz2':
    print('bunzip2 the file')
    os.chdir(os.path.dirname(path_2_in_0))
    os.system('bunzip2 ' + path_2_in_0)
    path_2_in = path_2_in_0[:-4]
else:
    path_2_in = path_2_in_0

git_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', simulation_dir)
os.chdir(git_dir)

N_snap, Z_snap, A_snap, DC_max, DC_min = n.loadtxt(
    os.path.join(git_dir, 'snap_list_with_border.txt'), unpack=True)


def get_a(path_2_in):
    alp = os.path.basename(path_2_in).split('_')[1][:7]
    print('a=', alp)
    return float(alp)


a_snap = get_a(path_2_in)
sel = (A_snap == a_snap)

# creates the directory where the outputs will be temporarily stored :
out_dir = os.path.join(
    os.path.dirname(path_2_in),
    'replication_' + str(a_snap) + '/')
if os.path.isdir(out_dir) == False:
    os.mkdir(out_dir)

out_dir_2 = os.path.join(
    os.path.dirname(path_2_in),
    'replicated_' + str(a_snap) + '/')
if os.path.isdir(out_dir_2) == False:
    os.mkdir(out_dir_2)

# regular pavement
pts_i = n.arange(-1 * N_pavement, N_pavement, 1)
ix_i, iy_i, iz_i = n.meshgrid(pts_i, pts_i, pts_i)
ix, iy, iz = n.ravel(ix_i), n.ravel(iy_i), n.ravel(iz_i)
# print(ix,iy,iz)
pts = pts_i * L_box
x0_i, y0_i, z0_i = n.meshgrid(pts, pts, pts)
x0, y0, z0 = n.ravel(x0_i), n.ravel(y0_i), n.ravel(z0_i)

######################
# WRITES and executes THE AWK COMMANDS
######################
# writes out only the interesting columns and the translated x,y,z coordinates
# id Mvir Rvir rs scale_of_last_MM vmax x y z vx vy vz M200c M500c
# b_to_a_500c c_to_a_500c Acc_Rate_1_Tdyn


def transform_rockstar_out_catalog_into_small_ascii_catalog_distinct(
        path_2_in, path_2_out, xt, D2_min, D2_max, N_skip=64):
    x0, y0, z0 = xt
    gawk_start = """gawk 'NR>""" + str(N_skip)
    # distinct halos and > 100 bound particles
    gawk_filter_distinct = """ {if ( ( $11 >= 1.0e11 ) && ( $6==-1 ) """
    # minimum distance (shell)
    gawk_filter_dmin = """&& ( ($18/0.6777+""" + str(x0) + """) ** 2 + ($19/0.6777+""" + str(
        y0) + """) ** 2 + ($20/0.6777+""" + str(z0) + """) ** 2 >=  """ + str(D2_min) + """ ) """
    # maximum distance (shell)
    gawk_filter_dmax = """&& ( ($18/0.6777+""" + str(x0) + """) ** 2 + ($19/0.6777+""" + str(
        y0) + """) ** 2 + ($20/0.6777+""" + str(z0) + """) ** 2 <  """ + str(D2_max) + """ ) """
    # print columns
    gawk_print = """) print  $2, $11, $12, $13, $16, $17, $18/0.6777+""" + str(x0) + """, $19/0.6777+""" + str(
        y0) + """, $20/0.6777+""" + str(z0) + """, $21, $22, $23, $41, $42, $52, $53, $67}' """
    gawk_end = path_2_in + " > " + path_2_out
    # full command :
    gawk_command = gawk_start + gawk_filter_distinct + \
        gawk_filter_dmax + gawk_filter_dmin + gawk_print + gawk_end
    #gawk_command = gawk_start + gawk_filter_distinct + gawk_print + gawk_end
    print(gawk_command)
    print('-------------------------------')
    os.system(gawk_command)
    # header = """# id Mvir Rvir rs scale_of_last_MM vmax x y z vx vy vz M200c
    # M500c b_to_a_500c c_to_a_500c Acc_Rate_1_Tdyn """


XT = n.transpose([x0, y0, z0])
iXT = n.transpose([ix, iy, iz])
"""
# Eight summits of the box
X0 = n.array([ 0., 0., 0. ])*L_box
X1 = n.array([ 1., 0., 0. ])*L_box
X2 = n.array([ 0., 1., 0. ])*L_box
X3 = n.array([ 0., 0., 1. ])*L_box
X4 = n.array([ 1., 1., 0. ])*L_box
X5 = n.array([ 1., 0., 1. ])*L_box
X6 = n.array([ 0., 1., 1. ])*L_box
X7 = n.array([ 1., 1., 1. ])*L_box

d_X = lambda Xi : n.sum((Xi+XT)**2, axis=1)

dist_2_box_summits = n.array([d_X(X0), d_X(X1), d_X(X2), d_X(X3), d_X(X4), d_X(X5), d_X(X6), d_X(X7)])

# for each box replication, compute the furthest point to 0,0,0
arr_max = n.max(dist_2_box_summits, axis=0)
# for each box replication, compute the closest point to 0,0,0
arr_min = n.min(dist_2_box_summits, axis=0)

selection1 = (n.floor(DC_min[sel][0]/L_box)*L_box <= arr_min**0.5 ) & ( arr_min**0.5 < n.ceil(DC_max[sel][0]/L_box)*L_box  )
selection2 = (n.floor(DC_min[sel][0]/L_box)*L_box <= arr_max**0.5 ) & ( arr_max**0.5 < n.ceil(DC_max[sel][0]/L_box)*L_box  )
selection = (selection1) | (selection2)
print(iXT[selection])"""
for jj in range(len(iXT)):
    jx, jy, jz = iXT[jj]
    path_2_out = os.path.join(
        out_dir,
        'replication_' +
        str(jx) +
        '_' +
        str(jy) +
        '_' +
        str(jz) +
        '_hlist_' +
        str(a_snap) +
        '.list')
    transform_rockstar_out_catalog_into_small_ascii_catalog_distinct(
        path_2_in, path_2_out, XT[jj], DC_min[sel][0]**2, DC_max[sel][0]**2)


out = os.listdir(out_dir)
fs = n.array([os.path.getsize(os.path.join(out_dir, oo)) for oo in out])
to_delete = n.array(out)[(fs == 0)]
n.array([os.remove(os.path.join(out_dir, tdl)) for tdl in to_delete])

out_file = os.path.join(out_dir_2, 'all_' + str(a_snap) + '.list')

all_files = n.array([os.path.join(out_dir, el)
                     for el in n.array(out)[(fs > 0)]])

to_concat = " ".join(all_files)
command = 'cat ' + git_dir + '/header ' + to_concat + ' > ' + out_file
print(command)
os.system(command)

out_fits = os.path.join(os.path.dirname(path_2_in), 'fits',
                        'all_' + os.path.basename(path_2_in).split('_')[-1])

stilts_command = "stilts tpipe in=" + out_file + \
    " ifmt=ascii omode=out ofmt=fits out=" + out_fits[:-5] + ".fits"
print(stilts_command)
os.system(stilts_command)

#os.system('rm -rf '+out_dir_2)
os.system('rm -rf ' + out_dir)
