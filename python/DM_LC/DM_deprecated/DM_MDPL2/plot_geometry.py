import sys
import os
import matplotlib.pyplot as p
import matplotlib
import numpy as n
import h5py
import time
print('Plots geometry data')
t0 = time.time()


matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})

env = 'MD10'
L_box = 1000.0 / 0.6777

fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'geometry')
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)

git_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'DM_MDPL2')
os.chdir(git_dir)

N_snap, Z_snap, A_snap, DC_max, DC_min = n.loadtxt(
    os.path.join(git_dir, 'snap_list_with_border.txt'), unpack=True)

# regular pavement
pts_i = n.arange(-8, 8, 1)
bins = n.arange(-9, 9, 1)
ix_i, iy_i, iz_i = n.meshgrid(pts_i, pts_i, pts_i)
ix, iy, iz = n.ravel(ix_i), n.ravel(iy_i), n.ravel(iz_i)
# print(ix,iy,iz)
pts = pts_i * L_box
x0_i, y0_i, z0_i = n.meshgrid(pts, pts, pts)
x0, y0, z0 = n.ravel(x0_i), n.ravel(y0_i), n.ravel(z0_i)

XT = n.transpose([x0, y0, z0])
iXT = n.transpose([ix, iy, iz])

# Eight summits of the box
X0 = n.array([0., 0., 0.]) * L_box
X1 = n.array([1., 0., 0.]) * L_box
X2 = n.array([0., 1., 0.]) * L_box
X3 = n.array([0., 0., 1.]) * L_box
X4 = n.array([1., 1., 0.]) * L_box
X5 = n.array([1., 0., 1.]) * L_box
X6 = n.array([0., 1., 1.]) * L_box
X7 = n.array([1., 1., 1.]) * L_box


def d_X(Xi): return n.sum((Xi + XT)**2, axis=1)


dist_2_box_summits = n.array([d_X(X0), d_X(X1), d_X(
    X2), d_X(X3), d_X(X4), d_X(X5), d_X(X6), d_X(X7)])

# for each box replication, compute the furthest point to 0,0,0
arr_max = n.max(dist_2_box_summits, axis=0)
# for each box replication, compute the closest point to 0,0,0
arr_min = n.min(dist_2_box_summits, axis=0)

for ii in range(len(DC_min)):
    print('=================================================')
    #selection = (DC_min[ii] >= arr_min**0.5 ) & ( DC_max[ii] < arr_max**0.5 )
    selection1 = (n.floor(DC_min[ii] / L_box) * L_box <= arr_min **
                  0.5) & (arr_min**0.5 < n.ceil(DC_max[ii] / L_box) * L_box)
    selection2 = (n.floor(DC_min[ii] / L_box) * L_box <= arr_max **
                  0.5) & (arr_max**0.5 < n.ceil(DC_max[ii] / L_box) * L_box)
    selection = (selection1) | (selection2)
    print(DC_min[ii], DC_max[ii], DC_min[ii] / L_box, DC_max[ii] / L_box)
    print(n.floor(DC_min[ii] / L_box) * L_box)
    print(n.ceil(DC_max[ii] / L_box) * L_box)
    print(iXT[selection])
    print(arr_min[selection]**0.5)
    fig_nn = str(n.round(DC_min[ii], 1)) + '<DC<' + str(n.round(DC_max[ii], 1))
    path_2_figure = os.path.join(fig_dir, 'replication_' + fig_nn + '.png')
    #path_2_figure = os.path.join(  fig_dir, 'replication_'+str(jx)+'_'+str(jy)+'_'+str(jz)+'.png')
    # for jj in range(len(iXT[selection])):
    # jx,jy,jz=iXT[selection][jj]
    print(path_2_figure)
    p.figure(1, (6., 5.5))
    p.tight_layout()
    p.hist(
        iXT[selection].T[0],
        histtype='step',
        rasterized=True,
        lw=4,
        label='jx',
        bins=bins)
    p.hist(
        iXT[selection].T[1],
        histtype='step',
        rasterized=True,
        lw=3,
        label='jy',
        bins=bins)
    p.hist(
        iXT[selection].T[2],
        histtype='step',
        rasterized=True,
        lw=2,
        label='jz',
        bins=bins)
    p.legend(frameon=False, loc=0)
    p.title(fig_nn)
    p.xlabel('ji')
    p.ylabel('Counts')
    p.grid()
    # p.yscale('log')
    p.savefig(path_2_figure)
    p.clf()

# sys.exit()
