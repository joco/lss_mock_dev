#!/bin/bash

python 000_process_hlists.py  /data44s/eroAGN_WG_DATA/DATA/simulation/MD/MD_1.0Gpc/hlist/hlist_0.85640.list
python 001_process_hlists_sat.py /data44s/eroAGN_WG_DATA/DATA/simulation/MD/MD_1.0Gpc/hlist/hlist_0.85640.list

python 000_process_hlists.py  /data44s/eroAGN_WG_DATA/DATA/simulation/MD/MD_1.0Gpc/hlist/hlist_0.80130.list
python 001_process_hlists_sat.py /data44s/eroAGN_WG_DATA/DATA/simulation/MD/MD_1.0Gpc/hlist/hlist_0.80130.list


python 000_process_hlists.py  /data44s/eroAGN_WG_DATA/DATA/simulation/MD/MD_1.0Gpc/hlist/hlist_0.65650.list.bz2
python 001_process_hlists_sat.py /data44s/eroAGN_WG_DATA/DATA/simulation/MD/MD_1.0Gpc/hlist/hlist_0.65650.list

python 000_process_hlists.py  /data44s/eroAGN_WG_DATA/DATA/simulation/MD/MD_1.0Gpc/hlist/hlist_0.61420.list.bz2
python 001_process_hlists_sat.py /data44s/eroAGN_WG_DATA/DATA/simulation/MD/MD_1.0Gpc/hlist/hlist_0.61420.list

python 000_process_hlists.py  /data44s/eroAGN_WG_DATA/DATA/simulation/MD/MD_1.0Gpc/hlist/hlist_0.57470.list.bz2
python 001_process_hlists_sat.py /data44s/eroAGN_WG_DATA/DATA/simulation/MD/MD_1.0Gpc/hlist/hlist_0.57470.list
