import numpy as n
import os
import sys
import glob

# "/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001/hlists"
top_dir = sys.argv[1]

fits_dir = os.path.join(top_dir, 'fits')

out_dir = os.path.join(top_dir, 'mini_fits')
if os.path.isdir(out_dir) == False:
    os.mkdir(out_dir)

fits_list = sorted(
    n.array(
        glob.glob(
            os.path.join(
                fits_dir,
                'all_?.?????.fits'))))

for in_file in fits_list[::-1]:
    out_file = os.path.join(out_dir, os.path.basename(in_file))
    if os.path.isfile(out_file):
        print(out_file, 'done')
    else:
        stilts_command = "stilts tpipe in=" + in_file + \
            """ ifmt=fits cmd='select "z>-5 && z<5"' omode=out ofmt=fits out=""" + out_file
        print(stilts_command)
        os.system(stilts_command)

fits_list = n.array(glob.glob(os.path.join(fits_dir, 'sat_?.?????.fits')))
fits_list.sort()

for in_file in fits_list[::-1]:
    out_file = os.path.join(out_dir, os.path.basename(in_file))
    if os.path.isfile(out_file):
        print(out_file, 'done')
    else:
        stilts_command = "stilts tpipe in=" + in_file + \
            """ ifmt=fits cmd='select "z>-5 && z<5"' omode=out ofmt=fits out=""" + out_file
        print(stilts_command)
        os.system(stilts_command)
