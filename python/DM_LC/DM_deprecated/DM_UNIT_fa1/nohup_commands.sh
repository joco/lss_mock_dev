#!/bin/bash

nohup sh run_000_001_fA1.sh > run_000_001_fA1.log & 
nohup sh run_000_001_fA1i.sh > run_000_001_fA1i.log & 
nohup sh run_000_001_fA2.sh > run_000_001_fA2.log & 

nohup sh run_002_shell4_fA1i.sh > run_002_shell4_fA1i.log &
nohup sh  run_002_shell4_fA1.sh >  run_002_shell4_fA1.log &
nohup sh  run_002_shell4_fA2.sh >  run_002_shell4_fA2.log &
nohup sh run_002_shell5_fA1i.sh > run_002_shell5_fA1i.log &
nohup sh  run_002_shell5_fA1.sh >  run_002_shell5_fA1.log &
nohup sh  run_002_shell5_fA2.sh >  run_002_shell5_fA2.log &
nohup sh run_002_shell6_fA1i.sh > run_002_shell6_fA1i.log &
nohup sh  run_002_shell6_fA1.sh >  run_002_shell6_fA1.log &
nohup sh  run_002_shell6_fA2.sh >  run_002_shell6_fA2.log &


# nohup sh run_000_001.sh > run_000_001.log &
# nohup sh run_003.sh > run_003.log &

python 003_small_fits_for_plotting.py /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001/hlists
python 003_small_fits_for_plotting.py /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_002/hlists
python 003_small_fits_for_plotting.py /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001/hlists
python 003_small_fits_for_plotting.py /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_002/hlists

ll /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001/hlists/mini_fits/*
ll /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_002/hlists/mini_fits/*
ll /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001/hlists/mini_fits/*
ll /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_002/hlists/mini_fits/*

ll /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001/hlists/fits/*
ll /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_002/hlists/fits/*
ll /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001/hlists/fits/*
ll /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_002/hlists/fits/*

export UNIT_fA1_DIR='/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001/'
export UNIT_fA2_DIR='/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_002/'
export UNIT_fA1i_DIR='/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001/'
export UNIT_fA2i_DIR='/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_002/'



# nohup sh hlist_1.00000.sh > hlist_1.00000.log &
# nohup sh hlist_0.97810.sh > hlist_0.97810.log &
# nohup sh hlist_0.95670.sh > hlist_0.95670.log &
# nohup sh hlist_0.93570.sh > hlist_0.93570.log &
# nohup sh hlist_0.91520.sh > hlist_0.91520.log &
# nohup sh hlist_0.89510.sh > hlist_0.89510.log &
# nohup sh hlist_0.87550.sh > hlist_0.87550.log &
# nohup sh hlist_0.85640.sh > hlist_0.85640.log &
# nohup sh hlist_0.83760.sh > hlist_0.83760.log &
# nohup sh hlist_0.81920.sh > hlist_0.81920.log &
# nohup sh hlist_0.80130.sh > hlist_0.80130.log &
nohup sh hlist_0.78370.sh > hlist_0.78370.log &
nohup sh hlist_0.76660.sh > hlist_0.76660.log &
nohup sh hlist_0.74980.sh > hlist_0.74980.log &
nohup sh hlist_0.73330.sh > hlist_0.73330.log &
nohup sh hlist_0.71730.sh > hlist_0.71730.log &
nohup sh hlist_0.70160.sh > hlist_0.70160.log &
nohup sh hlist_0.68620.sh > hlist_0.68620.log &
nohup sh hlist_0.67120.sh > hlist_0.67120.log &
nohup sh hlist_0.65650.sh > hlist_0.65650.log &
nohup sh hlist_0.64210.sh > hlist_0.64210.log &
nohup sh hlist_0.62800.sh > hlist_0.62800.log &
nohup sh hlist_0.61420.sh > hlist_0.61420.log &
nohup sh hlist_0.60080.sh > hlist_0.60080.log &
nohup sh hlist_0.58760.sh > hlist_0.58760.log &
nohup sh hlist_0.57470.sh > hlist_0.57470.log &
nohup sh hlist_0.56220.sh > hlist_0.56220.log &
nohup sh hlist_0.54980.sh > hlist_0.54980.log &
nohup sh hlist_0.53780.sh > hlist_0.53780.log &
nohup sh hlist_0.52600.sh > hlist_0.52600.log &
nohup sh hlist_0.51450.sh > hlist_0.51450.log &
nohup sh hlist_0.50320.sh > hlist_0.50320.log &
nohup sh hlist_0.49220.sh > hlist_0.49220.log &
nohup sh hlist_0.48140.sh > hlist_0.48140.log &
nohup sh hlist_0.47090.sh > hlist_0.47090.log &
nohup sh hlist_0.46050.sh > hlist_0.46050.log &
nohup sh hlist_0.45050.sh > hlist_0.45050.log &
nohup sh hlist_0.44060.sh > hlist_0.44060.log &
nohup sh hlist_0.43090.sh > hlist_0.43090.log &
nohup sh hlist_0.42150.sh > hlist_0.42150.log &
nohup sh hlist_0.41230.sh > hlist_0.41230.log &
nohup sh hlist_0.40320.sh > hlist_0.40320.log &
nohup sh hlist_0.39440.sh > hlist_0.39440.log &
nohup sh hlist_0.38570.sh > hlist_0.38570.log &
nohup sh hlist_0.37730.sh > hlist_0.37730.log &
nohup sh hlist_0.36900.sh > hlist_0.36900.log &
nohup sh hlist_0.36090.sh > hlist_0.36090.log &
nohup sh hlist_0.35300.sh > hlist_0.35300.log &
nohup sh hlist_0.34530.sh > hlist_0.34530.log &
nohup sh hlist_0.33770.sh > hlist_0.33770.log &
nohup sh hlist_0.33030.sh > hlist_0.33030.log &
nohup sh hlist_0.32310.sh > hlist_0.32310.log &
nohup sh hlist_0.31600.sh > hlist_0.31600.log &
nohup sh hlist_0.30910.sh > hlist_0.30910.log &
nohup sh hlist_0.30230.sh > hlist_0.30230.log &
nohup sh hlist_0.29570.sh > hlist_0.29570.log &
nohup sh hlist_0.28920.sh > hlist_0.28920.log &
nohup sh hlist_0.28290.sh > hlist_0.28290.log &
nohup sh hlist_0.27670.sh > hlist_0.27670.log &
nohup sh hlist_0.27060.sh > hlist_0.27060.log &
nohup sh hlist_0.26470.sh > hlist_0.26470.log &
nohup sh hlist_0.25890.sh > hlist_0.25890.log &
nohup sh hlist_0.25320.sh > hlist_0.25320.log &
nohup sh hlist_0.24770.sh > hlist_0.24770.log &
nohup sh hlist_0.24230.sh > hlist_0.24230.log &
nohup sh hlist_0.23690.sh > hlist_0.23690.log &
nohup sh hlist_0.23180.sh > hlist_0.23180.log &
nohup sh hlist_0.22670.sh > hlist_0.22670.log &
nohup sh hlist_0.22170.sh > hlist_0.22170.log &
nohup sh hlist_0.21690.sh > hlist_0.21690.log &
nohup sh hlist_0.21210.sh > hlist_0.21210.log &
nohup sh hlist_0.20750.sh > hlist_0.20750.log &
nohup sh hlist_0.20290.sh > hlist_0.20290.log &
nohup sh hlist_0.19850.sh > hlist_0.19850.log &
nohup sh hlist_0.19410.sh > hlist_0.19410.log &
nohup sh hlist_0.18990.sh > hlist_0.18990.log &
nohup sh hlist_0.18570.sh > hlist_0.18570.log &
nohup sh hlist_0.18160.sh > hlist_0.18160.log &
nohup sh hlist_0.17770.sh > hlist_0.17770.log &
nohup sh hlist_0.17380.sh > hlist_0.17380.log &
nohup sh hlist_0.17000.sh > hlist_0.17000.log &
nohup sh hlist_0.16620.sh > hlist_0.16620.log &
nohup sh hlist_0.16260.sh > hlist_0.16260.log &
nohup sh hlist_0.15900.sh > hlist_0.15900.log &
nohup sh hlist_0.15550.sh > hlist_0.15550.log &
nohup sh hlist_0.15210.sh > hlist_0.15210.log &
nohup sh hlist_0.14880.sh > hlist_0.14880.log &
nohup sh hlist_0.14550.sh > hlist_0.14550.log &
nohup sh hlist_0.14240.sh > hlist_0.14240.log &
nohup sh hlist_0.13920.sh > hlist_0.13920.log &
nohup sh hlist_0.13620.sh > hlist_0.13620.log &
nohup sh hlist_0.13320.sh > hlist_0.13320.log &
nohup sh hlist_0.13030.sh > hlist_0.13030.log &

