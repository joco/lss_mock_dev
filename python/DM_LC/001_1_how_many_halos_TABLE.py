"""
What it does
------------

counts the number of halos 

python 001_1_how_many_halos_TABLE.py MD10 
python 001_0_how_many_halos.py MD04 
python 001_0_how_many_halos.py MD40 
python 001_0_how_many_halos.py UNIT_fA1_DIR 
python 001_0_how_many_halos.py UNIT_fA2_DIR 
python 001_0_how_many_halos.py UNIT_fA1i_DIR 
"""
# import python packages

import sys, os, glob
import astropy.io.fits as fits
from astropy.table import Table, Column
import numpy as n
import time
#print('count halos')

env = sys.argv[1]

dict_sim = {
	'MD04': '\\textsc{SMDPL}',
	'MD10': '\\textsc{MDPL2}',
	'MD40': '\\textsc{HMDPL}',
	'UNIT_fA1_DIR':  '\\textsc{UNIT\_1}'  ,
	'UNIT_fA2_DIR':  '\\textsc{UNIT\_2}'  ,
	'UNIT_fA1i_DIR': '\\textsc{UNIT\_1i}' ,
	'UNIT_fA2i_DIR': '\\textsc{UNIT\_2i}'
	}

output_file_all = os.path.join( 
	os.environ['GIT_AGN_MOCK'], 'python', 'DM_LC', 'halo_counts', env+ '_halo_count_all.ascii')
output_file_5e13 = os.path.join( 
	os.environ['GIT_AGN_MOCK'], 'python', 'DM_LC', 'halo_counts', env+'_halo_count_5e13.ascii')
output_file_7e13 = os.path.join( 
	os.environ['GIT_AGN_MOCK'], 'python', 'DM_LC', 'halo_counts', env+'_halo_count_7e13.ascii')
output_file_1e14 = os.path.join( 
	os.environ['GIT_AGN_MOCK'], 'python', 'DM_LC', 'halo_counts', env+'_halo_count_1e14.ascii')
output_file_5e14 = os.path.join( 
	os.environ['GIT_AGN_MOCK'], 'python', 'DM_LC', 'halo_counts', env+'_halo_count_5e14.ascii')

# import all pathes
#envs = n.array(['MD10', 'MD04', 'MD40', 'UNIT_fA1i_DIR', 'UNIT_fA1_DIR', 'UNIT_fA2_DIR'])
f_all_aexp,  f_all_N_distinct,  f_all_N_sat = n.loadtxt( output_file_all,  unpack = True )
f_5e13_aexp, f_5e13_N_distinct, f_5e13_N_sat = n.loadtxt( output_file_5e13, unpack = True )
f_7e13_aexp, f_7e13_N_distinct, f_7e13_N_sat = n.loadtxt( output_file_7e13, unpack = True )
f_1e14_aexp, f_1e14_N_distinct, f_1e14_N_sat = n.loadtxt( output_file_1e14, unpack = True )
f_5e14_aexp, f_5e14_N_distinct, f_5e14_N_sat = n.loadtxt( output_file_5e14, unpack = True )

def get_line(N_distinct, N_sat):
	str_1 = str( int( n.sum( N_distinct) ) )
	str_2 = str( int( n.sum( N_sat     ) ) )
	str_3 = str( n.round( 100. * n.sum( N_sat ) / n.sum( N_distinct ), 1 ) )
	line = str_1 + " & " + str_2 + " ("+str_3+") "
	return line

str_all  = get_line(f_all_N_distinct,  f_all_N_sat )
str_5e13 = get_line(f_5e13_N_distinct, f_5e13_N_sat)
str_7e13 = get_line(f_7e13_N_distinct, f_7e13_N_sat)
str_1e14 = get_line(f_1e14_N_distinct, f_1e14_N_sat)
str_5e14 = get_line(f_5e14_N_distinct, f_5e14_N_sat)
z_max =  str(n.round(1/f_all_aexp.min()-1,2)  )
counts = str_all + " & " + str_5e13 + " & " + str_7e13 + " & " + str_1e14 + " & " + str_5e14
#counts = str_all + " & " + str_7e13 + " & " + str_1e14 + " & " + str_5e13
env_line = dict_sim[env] +" & " + z_max +" & " + counts + " \\\\"

print(env_line)
