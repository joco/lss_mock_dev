#!/bin/bash

cd $GIT_AGN_MOCK/python/DM_LC
pyCONDA
# new files for gap at z=0.55
# screens on ds43
python 000_process_hlists.py 4000.0 0.6777 64 MD40 10000000000000.0 /data39s/simulation_2/MD/MD_4.0Gpc/hlists/hlist_0.65650.list.bz2
python 000_process_hlists.py 4000.0 0.6777 64 MD40 10000000000000.0 /data39s/simulation_2/MD/MD_4.0Gpc/hlists/hlist_0.67120.list.bz2
python 000_process_hlists.py 4000.0 0.6777 64 MD40 10000000000000.0 /data39s/simulation_2/MD/MD_4.0Gpc/hlists/hlist_0.68620.list.bz2

# border files 
python 000_process_hlists.py 4000.0 0.6777 64 MD40 10000000000000.0 /data39s/simulation_2/MD/MD_4.0Gpc/hlists/hlist_0.64210.list.bz2
python 000_process_hlists.py 4000.0 0.6777 64 MD40 10000000000000.0 /data39s/simulation_2/MD/MD_4.0Gpc/hlists/hlist_0.70160.list.bz2

# screens on ds23

cd $GIT_AGN_MOCK/python/DM_LC
pyCONDA
python 000_process_hlists.py 4000.0 0.6777 64 MD40 10000000000000.0 /data39s/simulation_2/MD/MD_4.0Gpc/hlists/hlist_0.49220.list.bz2
python 000_process_hlists.py 4000.0 0.6777 64 MD40 10000000000000.0 /data39s/simulation_2/MD/MD_4.0Gpc/hlists/hlist_0.50320.list.bz2
python 000_process_hlists.py 4000.0 0.6777 64 MD40 10000000000000.0 /data39s/simulation_2/MD/MD_4.0Gpc/hlists/hlist_0.51450.list.bz2

cd $GIT_AGN_MOCK/python/DM_LC
pyCONDA

python 000_process_hlists.py 4000.0 0.6777 64 MD40 10000000000000.0 /data39s/simulation_2/MD/MD_4.0Gpc/hlists/hlist_0.41230.list.bz2
python 000_process_hlists.py 4000.0 0.6777 64 MD40 10000000000000.0 /data39s/simulation_2/MD/MD_4.0Gpc/hlists/hlist_0.42150.list.bz2
python 000_process_hlists.py 4000.0 0.6777 64 MD40 10000000000000.0 /data39s/simulation_2/MD/MD_4.0Gpc/hlists/hlist_0.43090.list.bz2
