from astropy.io import fits
from colossus.cosmology import cosmology
from colossus.lss import peaks
from colossus.lss import mass_function
from colossus.lss import bias
import numpy as n
import os
import sys
from scipy.interpolate import interp1d
from scipy.integrate import quad

import matplotlib.pyplot as p

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
cosmo = FlatLambdaCDM(
    H0=67.77 *
    u.km /
    u.s /
    u.Mpc,
    Om0=0.307115,
    Ob0=0.048206)
#delta_c = 1.68647
#A2, a2, p2, q2 = 0.324, 0.897, 0.624, 1.589
#f_BH11_NB = lambda sigma, A, a, p, q: A * (2./n.pi)**(0.5) * ( 1. + (sigma**2./(a*delta_c**2.))**(p) )*(delta_c*a**0.5/sigma)**(q)*n.e**(-a*delta_c**2./(2.*sigma**2.))
#fsigma_Comparat_17_2 = lambda sigma : f_BH11_NB(sigma, A2, a2, p2, q2)
cosmology.setCosmology('planck15')
cosmoC = cosmology.getCurrent()


# setup of the simulation
# name of the shell
path_2_in = '1.00000'  # sys.argv[1]
#top_dir = '/data17s/darksim/MD/MD_0.4Gpc/hlists/fits/'
top_dir = '/home/comparat/data/MultiDark/MD_1.0Gpc/hlists/fits'
cen_filename = os.path.join(top_dir, 'all_' + path_2_in + '.fits')

# retrieves the data
hdus = fits.open(cen_filename)[1].data

z_array = n.arange(0, 20., 0.001)
dcs = cosmo.comoving_distance(z_array)
dc_to_z = interp1d(dcs, z_array)
z_to_dc = interp1d(z_array, dcs)

git_dir = os.path.join(
    os.environ['GIT_VS'],
    'bin',
    'bin_nbody_data',
    'DM_SMDPL')
N_snap, Z_snap, A_snap, DC_max, DC_min = n.loadtxt(
    os.path.join(git_dir, 'snap_list_with_border.txt'), unpack=True)

# retrieve the shell parameters
snap_sel = (float(path_2_in) == A_snap)
V_shell = (
    cosmo.comoving_volume(
        dc_to_z(
            DC_max[snap_sel])) -
    cosmo.comoving_volume(
        dc_to_z(
            DC_min[snap_sel])))
print(
    'V_shell', V_shell, 'in', dc_to_z(
        DC_min[snap_sel]), '<z<', dc_to_z(
            DC_max[snap_sel]))

z_shell = Z_snap[snap_sel]
# cosmic density to normalize the substructure function
# background density
rhom_w_units = cosmo.Om(
    z_shell) * cosmo.critical_density(z_shell).to(u.solMass / (u.Mpc)**3.)

print('rho mean', rhom_w_units)
print('mass in shell', rhom_w_units * V_shell)
rhom = rhom_w_units.value


# minimum satellite mass parameter
M_min_desired = 1.0e11
print(M_min_desired)


# substructure mass function, (comparat et al. 2017)
logN0 = -2.25
N0 = 10**logN0
a = -1.8
b = 5.8
exponent = 2.54


def fsat(xi): return N0 * xi**a * n.e**(-b * xi**exponent)


# setup of halo mass function for distinct haloes (comparat et al. 2017)
# conversion mass => nu => sigma
M = 10**n.arange(5.0, 16.1, 0.1)
nu = n.array([peaks.peakHeight(M_i, z_shell[0])
              for M_i in M]) / peaks.collapseOverdensity(z=z_shell[0])
m_2_s = interp1d(M, nu**-1)

# mass function
mfunc = mass_function.massFunction(
    M, 0., mdef='vir', model='comparat17', q_out='M2dndM')
itp_hmf = interp1d(M, mfunc / M**2)

int_MF_all = n.array([
    quad(itp_hmf, 10**M1, 10**M2)[0]
    for M1, M2 in zip(n.arange(6.0, 15.1, 0.5), n.arange(7.0, 16.1, 0.5))
]).sum()

int_MF_min_desired = n.array(
    [
        quad(
            itp_hmf,
            10**M1,
            10**M2)[0] for M1,
        M2 in zip(
                n.arange(
                    n.log10(M_min_desired),
                    15.1,
                    0.5),
            n.arange(
                    n.log10(M_min_desired) +
                    1,
                    16.1,
                    0.5))]).sum()

int_MF_min_simulated = n.array([
    quad(itp_hmf, 10**M1, 10**M2)[0]
    for M1, M2 in zip(n.arange(11., 15.1, 0.5), n.arange(11. + 1, 16.1, 0.5))
]).sum()

print('HMF integral ', int_MF_all, int_MF_min_desired, int_MF_min_simulated)
probed_total_mass_fraction = int_MF_min_desired / int_MF_all
simulated_total_mass_fraction = int_MF_min_simulated / int_MF_all

print(
    'fraction of the mass considered, desired',
    probed_total_mass_fraction,
    'simulated',
    simulated_total_mass_fraction)

total_mass = n.sum(hdus['Mvir'])  # *u.Msun/0.6777
print('total mass in catalog', n.log10(total_mass))
print('theoretical total mass', rhom_w_units *
      simulated_total_mass_fraction * V_shell)


# host halo selection bin
M_host_min = 10**13.5
M_host_max = 10**15.
M_host = 0.5 * (M_host_max + M_host_min)
sel = (hdus['Mvir'] > M_host_min) & (hdus['Mvir'] <= M_host_max)
Mvir_all_host = hdus['Mvir'][sel]
N_host = len(hdus['Mvir'][sel])
print('N_host', N_host, 'in', n.log10(M_host_min), '<m<', n.log10(M_host_max))

# xi is the ratio between the sat end the host mass
xi_min = n.log10(M_min_desired / M_host_min)
xi = 10**n.arange(xi_min - 0.1, 0, 0.1)

fun_1 = interp1d(
    xi,
    fsat(xi) *
    V_shell *
    rhom *
    probed_total_mass_fraction /
    M_host)
norm_1 = quad(fun_1, xi[1], xi[-2])[0]

print(norm_1)


# output file name
sat_filename = os.path.join(top_dir, 'sat_NFW_' + path_2_in + '.fits')
