import numpy as n
import os
import sys
from scipy.interpolate import interp1d
from astropy.io import fits
M_min_desired = 1.0e9

path_2_in = '1.00000'  # sys.argv[1]

top_dir = '/data17s/darksim/MD/MD_0.4Gpc/hlists/fits/'

sat_filename = os.path.join(top_dir, 'satellites_' + path_2_in + '.fits')

hdus = fits.open(sat_filename)[1].data

bins = n.arange(-5, 0.1, 0.1)
x_bins = n.arange(-5, 0.1, 0.1)[:-1] + 0.05

M_mean = 10**13.5
M_min = 10**13.0
M_max = 10**14.0
sel = (hdus['Mvir_host'] > M_min) & (hdus['Mvir_host'] < M_max)

sat_mvir = hdus['Mvir'][sel]
N_sat = len(sat_mvir)
M_sat_min = n.min(sat_mvir)
N_hosts = len(n.unique(hdus['id_host'][sel]))

Mratio = sat_mvir / M_mean
out = n.cumsum(n.histogram(n.log10(Mratio), bins=bins)[0][::-1])
# divide by total number of hosts to obtain the average number per host
# Big problem, all sat do not have a match among the distinct, some will
# have among the sat.

itp = interp1d(x_bins[::-1], out)
# itp will have a maximum, before that, we need to measure the slope and
# extrapolate to M_min_desired.

# finds maximum

# takes derivative

# extrapolates to M_min_desired

#

# what I need : a function that for a host halo mass selection 13-13.2
# - the total number of satelittes to distribute
# - the mass of each satellite
