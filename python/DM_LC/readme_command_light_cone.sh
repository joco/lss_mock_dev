#!/bin/bash

# once all light cones are finished

"""
dict_sim = {
	'MD04': 'SMDPL'
	'MD10': 'MDPL2'
	'MD40': 'HMDPL'
	'UNIT_fA1_DIR':  'UNIT\_1'
	'UNIT_fA2_DIR':  'UNIT\_2'
	'UNIT_fA1i_DIR': 'UNIT\_1i'
	'UNIT_fA2i_DIR': 'UNIT\_2i'
	}
"""
cd $GIT_AGN_MOCK/python/DM_LC/

# counts halos distinct and satelittes for Table 1 of the cluster mock paper
# script: 001_0_how_many_halos.py
# inputs:    ${env}/fits/all_?.?????.fits
# outputs:   files containing the counts for various mass threshold:
#            $GIT_AGN_MOCK/python/DM_LC/halo_counts/${env}_halo_count_*.ascii
#           
nohup python 001_0_how_many_halos.py MD10 > logs/001_0_how_many_halos_MD10.log &
nohup python 001_0_how_many_halos.py MD04 > logs/001_0_how_many_halos_MD04.log &
nohup python 001_0_how_many_halos.py MD40 > logs/001_0_how_many_halos_MD40.log &
nohup python 001_0_how_many_halos.py UNIT_fA1_DIR > logs/001_0_how_many_halos_UNIT_fA1_DIR.log &
nohup python 001_0_how_many_halos.py UNIT_fA2_DIR > logs/001_0_how_many_halos_UNIT_fA2_DIR.log &
nohup python 001_0_how_many_halos.py UNIT_fA1i_DIR > logs/001_0_how_many_halos_UNIT_fA1i_DIR.log & 
nohup python 001_0_how_many_halos.py UNIT_fA2i_DIR > logs/001_0_how_many_halos_UNIT_fA2i_DIR.log & 

sh 001_1_how_many_halos_TABLE.sh
"""
python 001_1_how_many_halos_TABLE.py MD04 
python 001_1_how_many_halos_TABLE.py MD10 
python 001_1_how_many_halos_TABLE.py MD40 
python 001_1_how_many_halos_TABLE.py UNIT_fA1_DIR 
python 001_1_how_many_halos_TABLE.py UNIT_fA2_DIR 
python 001_1_how_many_halos_TABLE.py UNIT_fA1i_DIR 
python 001_1_how_many_halos_TABLE.py UNIT_fA2i_DIR 
"""

# creates small fits files for each shell
# in the z coordinate select a shell of 10Mpc width : z>-5 && z<5
# topcat commands
# script: Create_small_fits_for_plotting.py
# inputs: ${env}/fits/all_?.?????.fits
# outputs: ${env}/mini_fits/all_?.?????.fits
nohup python Create_small_fits_for_plotting.py  MD04           > logs/small_fits_MD04.log           &
nohup python Create_small_fits_for_plotting.py  MD10           > logs/small_fits_MD10.log           &
nohup python Create_small_fits_for_plotting.py  MD40           > logs/small_fits_MD40.log           &
nohup python Create_small_fits_for_plotting.py  UNIT_fA1_DIR   > logs/small_fits_UNIT_fA1_DIR.log   & 
nohup python Create_small_fits_for_plotting.py  UNIT_fA2_DIR   > logs/small_fits_UNIT_fA2_DIR.log   &
nohup python Create_small_fits_for_plotting.py  UNIT_fA1i_DIR  > logs/small_fits_UNIT_fA1i_DIR.log  &
nohup python Create_small_fits_for_plotting.py  UNIT_fA2i_DIR  > logs/small_fits_UNIT_fA2i_DIR.log   &

tar -czf /home/comparat/data2/firefly/mocks/2020-03/HMD_mini_LC.tar.gz   $MD40/mini_fits/all_*.fits
tar -czf /home/comparat/data2/firefly/mocks/2020-03/SMDPL_mini_LC.tar.gz $MD04/mini_fits/all_*.fits
tar -czf /home/comparat/data2/firefly/mocks/2020-03/MDPL2_mini_LC.tar.gz $MD10/mini_fits/all_*.fits
tar -czf /home/comparat/data2/firefly/mocks/2020-03/U_fA1_mini_LC.tar.gz $UNIT_fA1_DIR/mini_fits/all_*.fits
tar -czf /home/comparat/data2/firefly/mocks/2020-03/U_fA2_mini_LC.tar.gz $UNIT_fA2_DIR/mini_fits/all_*.fits
tar -czf /home/comparat/data2/firefly/mocks/2020-03/U_fA1i_mini_LC.tar.gz $UNIT_fA1i_DIR/mini_fits/all_*.fits
tar -czf /home/comparat/data2/firefly/mocks/2020-03/U_fA2i_mini_LC.tar.gz $UNIT_fA2i_DIR/mini_fits/all_*.fits

chmod ugo+rX /home/comparat/data2/firefly/mocks/2020-03/*

cd $MD40
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/HMD_mini_LC.tar.gz
cd $MD04
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/SMDPL_mini_LC.tar.gz
cd $MD10
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/MDPL2_mini_LC.tar.gz
cd $UNIT_fA1_DIR
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/U_fA1_mini_LC.tar.gz
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/U_fA2_mini_LC.tar.gz
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/U_fA1i_mini_LC.tar.gz
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/U_fA2i_mini_LC.tar.gz

wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/spectra.tar.gz
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/spectraAGN.tar.gz

###########################
#
# Plotting 
#
###########################
cd $GIT_AGN_MOCK/python/figures/

# topcat commands to show the light cone
# script: plot_light_cone.py
# inputs: ${env}/mini_fits/all_?.?????.fits
# outputs: $GIT_AGN_MOCK/figures/${env}/nbody_mini_fits/light_cone_xy_slice_all.png
nohup nice -n 19  python plot_light_cone.py MD04          > plot_light_cone_MD04.log &
nohup nice -n 19  python plot_light_cone.py MD10          > plot_light_cone_MD10.log &
nohup nice -n 19  python plot_light_cone.py MD40          > plot_light_cone_MD40.log &
nohup nice -n 19  python plot_light_cone.py UNIT_fA1_DIR  > plot_light_cone_UNIT_fA1_DIR.log &
nohup nice -n 19  python plot_light_cone.py UNIT_fA2_DIR  > plot_light_cone_UNIT_fA2_DIR.log &
nohup nice -n 19  python plot_light_cone.py UNIT_fA1i_DIR > plot_light_cone_UNIT_fA1i_DIR.log &
nohup nice -n 19  python plot_light_cone.py UNIT_fA2i_DIR > plot_light_cone_UNIT_fA2i_DIR.log &

# topcat commands to create histograms, 2D and 3D plots
# all columns can be passed, used for finding bugs
# script: plot_Nbody_RUN.py that runs : plot_Nbody.py for each shell 
# inputs: ${env}/fits/all_?.?????.fits or ${env}/mini_fits/all_?.?????.fits
# outputs: $GIT_AGN_MOCK/figures/${env}/ then in nbody_mini_fits/ or nbody_fits/ *.png
# nohup nice -n 19  python plot_Nbody_RUN.py  MD04          > plot_Nbody_RUN_MD04.log &          
# nohup nice -n 19  python plot_Nbody_RUN.py  MD10          > plot_Nbody_RUN_MD10.log &           
# nohup nice -n 19  python plot_Nbody_RUN.py  MD40          > plot_Nbody_RUN_MD40.log &           
# nohup nice -n 19  python plot_Nbody_RUN.py  UNIT_fA1_DIR  > plot_Nbody_RUN_UNIT_fA1_DIR.log &          
# nohup nice -n 19  python plot_Nbody_RUN.py  UNIT_fA2_DIR  > plot_Nbody_RUN_UNIT_fA2_DIR.log &          
# nohup nice -n 19  python plot_Nbody_RUN.py  UNIT_fA1i_DIR > plot_Nbody_RUN_UNIT_fA1i_DIR.log &          



