
cd $GIT_AGN_MOCK/python/DM_LC

!conda

# processing with 000_process_uchuu_pixel_i.py

cd pixel_batches_uchuu
nohup sh batch_00.sh > log_batch_00.log &
nohup sh batch_01.sh > log_batch_01.log &
nohup sh batch_02.sh > log_batch_02.log &
nohup sh batch_03.sh > log_batch_03.log &
nohup sh batch_04.sh > log_batch_04.log &
nohup sh batch_05.sh > log_batch_05.log &
nohup sh batch_06.sh > log_batch_06.log &
nohup sh batch_07.sh > log_batch_07.log &
nohup sh batch_08.sh > log_batch_08.log &
nohup sh batch_09.sh > log_batch_09.log &
nohup sh batch_10.sh > log_batch_10.log &
nohup sh batch_11.sh > log_batch_11.log &
nohup sh batch_12.sh > log_batch_12.log &
nohup sh batch_13.sh > log_batch_13.log &
nohup sh batch_14.sh > log_batch_14.log &
nohup sh batch_15.sh > log_batch_15.log &
nohup sh batch_16.sh > log_batch_16.log &
nohup sh batch_17.sh > log_batch_17.log &
nohup sh batch_18.sh > log_batch_18.log &
nohup sh batch_19.sh > log_batch_19.log &
nohup sh batch_20.sh > log_batch_20.log &
nohup sh batch_21.sh > log_batch_21.log &
nohup sh batch_22.sh > log_batch_22.log &
nohup sh batch_23.sh > log_batch_23.log &
nohup sh batch_24.sh > log_batch_24.log &
nohup sh batch_25.sh > log_batch_25.log &
nohup sh batch_26.sh > log_batch_26.log &
nohup sh batch_27.sh > log_batch_27.log &
nohup sh batch_28.sh > log_batch_28.log &
nohup sh batch_29.sh > log_batch_29.log &
nohup sh batch_30.sh > log_batch_30.log &

cd $GIT_AGN_MOCK/python/DM_LC


nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_010 > 001_pixelize_halodir_010.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_011 > 001_pixelize_halodir_011.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_012 > 001_pixelize_halodir_012.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_013 > 001_pixelize_halodir_013.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_014 > 001_pixelize_halodir_014.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_015 > 001_pixelize_halodir_015.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_016 > 001_pixelize_halodir_016.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_017 > 001_pixelize_halodir_017.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_018 > 001_pixelize_halodir_018.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_019 > 001_pixelize_halodir_019.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_020 > 001_pixelize_halodir_020.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_021 > 001_pixelize_halodir_021.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_022 > 001_pixelize_halodir_022.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_023 > 001_pixelize_halodir_023.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_024 > 001_pixelize_halodir_024.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_025 > 001_pixelize_halodir_025.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_026 > 001_pixelize_halodir_026.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_027 > 001_pixelize_halodir_027.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_028 > 001_pixelize_halodir_028.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_029 > 001_pixelize_halodir_029.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_030 > 001_pixelize_halodir_030.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_031 > 001_pixelize_halodir_031.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_032 > 001_pixelize_halodir_032.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_033 > 001_pixelize_halodir_033.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_034 > 001_pixelize_halodir_034.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_035 > 001_pixelize_halodir_035.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_036 > 001_pixelize_halodir_036.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_037 > 001_pixelize_halodir_037.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_038 > 001_pixelize_halodir_038.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_039 > 001_pixelize_halodir_039.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_040 > 001_pixelize_halodir_040.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_041 > 001_pixelize_halodir_041.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_042 > 001_pixelize_halodir_042.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_043 > 001_pixelize_halodir_043.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_044 > 001_pixelize_halodir_044.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_045 > 001_pixelize_halodir_045.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_046 > 001_pixelize_halodir_046.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_047 > 001_pixelize_halodir_047.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_048 > 001_pixelize_halodir_048.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_049 > 001_pixelize_halodir_049.log &
nohup python 001_pixelize_uchuu.py /home/users/dae/comparat/Uchuu/LC/halodir_050 > 001_pixelize_halodir_050.log &

