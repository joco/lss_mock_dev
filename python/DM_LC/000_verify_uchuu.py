"""

Replicates the snapshot over the sky and extracts halos in a given shell of comoving distance
Does full sky replication

Parameters
==========

 * l_box: length of the box in Mpc/h, example 400
 * h: unitless hubble parameter h=H0/100mk/s/Mpc, example 0.6777
 * N_skip: how many lines of header should be skipped while reading the halo file
 * env: environment variable linking to the simulation directory, example MD04 MD10, UNIT_fA1_DIR ...
 * path_2_header: path to the header of the output file
 * path_2_in_0: path to the rockstar hlist file, example /path/2/simulation/hlist_1.00000.h5

/home/users/dae/comparat/Uchuu/hlists/halodir_050/halolist_z0p00_0.h5
/home/users/dae/comparat/Uchuu/LC/halodir_050/halolist_z0p00_0.h5

Processing
==========

Generates and executes commands to replicate and filter the halo files. Lines 120 - 129 are the most important lines of the script.

outputs
=======

 * directory(path_2_in_0)/replication_$expansionParameter/all the files: contains a single file per replication. Deleted after the computation finishes.
 * directory(path_2_in_0)/replicated_$expansionParameter/one summary file: contains one file with all replication concatenated. Could be deleted after the computation is done.
 * $env/fits/all_'+$expansionParameter+'.fits') the former file converted to fits staged into the final directory. Keep this one ! It is the shell containing all the halos

python 000_process_uchuu.py /home/users/dae/comparat/Uchuu/hlists/halodir_050/halolist_z0p00_0.h5 

nohup sh 000_process_uchuu_pixel_i.sh 1 >  000_process_uchuu_pixel_1.log &
nohup sh 000_process_uchuu_pixel_i.sh 2 >  000_process_uchuu_pixel_2.log &
nohup sh 000_process_uchuu_pixel_i.sh 3 >  000_process_uchuu_pixel_3.log &
nohup sh 000_process_uchuu_pixel_i.sh 4 >  000_process_uchuu_pixel_4.log &
nohup sh 000_process_uchuu_pixel_i.sh 5 >  000_process_uchuu_pixel_5.log &
nohup sh 000_process_uchuu_pixel_i.sh 6 >  000_process_uchuu_pixel_6.log &
nohup sh 000_process_uchuu_pixel_i.sh 7 >  000_process_uchuu_pixel_7.log &
nohup sh 000_process_uchuu_pixel_i.sh 8 >  000_process_uchuu_pixel_8.log &
nohup sh 000_process_uchuu_pixel_i.sh 9 >  000_process_uchuu_pixel_9.log &


import numpy as n
import os, sys, glob
import healpy

NSIDE = 16
for ii, pixels in zip( n.arange(51), n.arange(51*60).reshape((51,60)) ):
	f=open('pixels_batch_'+str(ii)+".sh", 'w')
	f.write("#!/bin/bash")
	f.write("\n")
	for pix in pixels:
		f.write("python 000_process_uchuu_pixel_i_list.py "+str(pix)+" \n")
	f.close()

f=open("pixels_batch_51.sh", 'w')
f.write("#!/bin/bash")
f.write("\n")
all_npix = n.arange( 3060, healpy.nside2npix( NSIDE ), 1 )
for pix in all_npix:
	f.write("python 000_process_uchuu_pixel_i_list.py "+str(pix)+" \n")
f.close()

nohup sh  pixels_batch_0.sh   > log_pixels_batch_0.sh     &
nohup sh  pixels_batch_10.sh  > log_pixels_batch_10.sh    &
nohup sh  pixels_batch_11.sh  > log_pixels_batch_11.sh    &
nohup sh  pixels_batch_12.sh  > log_pixels_batch_12.sh    &
nohup sh  pixels_batch_13.sh  > log_pixels_batch_13.sh    &
nohup sh  pixels_batch_14.sh  > log_pixels_batch_14.sh    &
nohup sh  pixels_batch_15.sh  > log_pixels_batch_15.sh    &
nohup sh  pixels_batch_16.sh  > log_pixels_batch_16.sh    &
nohup sh  pixels_batch_17.sh  > log_pixels_batch_17.sh    &
nohup sh  pixels_batch_18.sh  > log_pixels_batch_18.sh    &
nohup sh  pixels_batch_19.sh  > log_pixels_batch_19.sh    &
nohup sh  pixels_batch_1.sh   > log_pixels_batch_1.sh     &
nohup sh  pixels_batch_20.sh  > log_pixels_batch_20.sh    &
nohup sh  pixels_batch_21.sh  > log_pixels_batch_21.sh    &
nohup sh  pixels_batch_22.sh  > log_pixels_batch_22.sh    &
nohup sh  pixels_batch_23.sh  > log_pixels_batch_23.sh    &
nohup sh  pixels_batch_24.sh  > log_pixels_batch_24.sh    &
nohup sh  pixels_batch_25.sh  > log_pixels_batch_25.sh    &
nohup sh  pixels_batch_26.sh  > log_pixels_batch_26.sh    &
nohup sh  pixels_batch_27.sh  > log_pixels_batch_27.sh    &
nohup sh  pixels_batch_28.sh  > log_pixels_batch_28.sh    &
nohup sh  pixels_batch_29.sh  > log_pixels_batch_29.sh    &
nohup sh  pixels_batch_2.sh   > log_pixels_batch_2.sh     &
nohup sh  pixels_batch_30.sh  > log_pixels_batch_30.sh    &
nohup sh  pixels_batch_31.sh  > log_pixels_batch_31.sh    &
nohup sh  pixels_batch_32.sh  > log_pixels_batch_32.sh    &
nohup sh  pixels_batch_33.sh  > log_pixels_batch_33.sh    &
nohup sh  pixels_batch_34.sh  > log_pixels_batch_34.sh    &
nohup sh  pixels_batch_35.sh  > log_pixels_batch_35.sh    &
nohup sh  pixels_batch_36.sh  > log_pixels_batch_36.sh    &
nohup sh  pixels_batch_37.sh  > log_pixels_batch_37.sh    &
nohup sh  pixels_batch_38.sh  > log_pixels_batch_38.sh    &
nohup sh  pixels_batch_39.sh  > log_pixels_batch_39.sh    &
nohup sh  pixels_batch_3.sh   > log_pixels_batch_3.sh     &
nohup sh  pixels_batch_40.sh  > log_pixels_batch_40.sh    &
nohup sh  pixels_batch_41.sh  > log_pixels_batch_41.sh    &
nohup sh  pixels_batch_42.sh  > log_pixels_batch_42.sh    &
nohup sh  pixels_batch_43.sh  > log_pixels_batch_43.sh    &
nohup sh  pixels_batch_44.sh  > log_pixels_batch_44.sh    &
nohup sh  pixels_batch_45.sh  > log_pixels_batch_45.sh    &
nohup sh  pixels_batch_46.sh  > log_pixels_batch_46.sh    &
nohup sh  pixels_batch_47.sh  > log_pixels_batch_47.sh    &
nohup sh  pixels_batch_48.sh  > log_pixels_batch_48.sh    &
nohup sh  pixels_batch_49.sh  > log_pixels_batch_49.sh    &
nohup sh  pixels_batch_4.sh   > log_pixels_batch_4.sh     &
nohup sh  pixels_batch_50.sh  > log_pixels_batch_50.sh    &
nohup sh  pixels_batch_51.sh  > log_pixels_batch_51.sh    &
nohup sh  pixels_batch_5.sh   > log_pixels_batch_5.sh     &
nohup sh  pixels_batch_6.sh   > log_pixels_batch_6.sh     &
nohup sh  pixels_batch_7.sh   > log_pixels_batch_7.sh     &
nohup sh  pixels_batch_8.sh   > log_pixels_batch_8.sh     &
nohup sh  pixels_batch_9.sh   > log_pixels_batch_9.sh     &
 
"""
print('runs 001_process_hlists.py with arguments' )
import time
#t0=time.time()
import numpy as n
import os, sys, glob
#print(sys.argv)
#import h5py
#from astropy.table import Table, Column
#from astropy.coordinates import SkyCoord
#import healpy
#from scipy.interpolate import interp1d
import astropy.io.fits as fits
#from scipy.special import erf
#from scipy.stats import norm
#import scipy
#from astropy.cosmology import FlatLambdaCDM
#import astropy.units as u

#from dustmaps.planck import PlanckQuery
#planck = PlanckQuery()

#path_2_NH_map = '/home/users/dae/comparat/h1_maps/H14PI/asu.fit'
#NH_DATA2 = fits.open(path_2_NH_map)[1].data
#nh_val = NH_DATA2['NHI']

#l_box = float(sys.argv[1]) # = 1000.0 
#h_str = sys.argv[2] # = 0.6777
#env = sys.argv[4]
#Mmin_str = sys.argv[5] # '9.0e9'
#path_2_in_0 = sys.argv[6]

#cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
h = 0.6774
#cosmo = cosmoUNIT

#z_array = n.arange(0, 7.5, 0.001)
#dc_to_z = interp1d(cosmo.comoving_distance(z_array), z_array)

#d_L = cosmo.luminosity_distance(z_array)
#dl_cm = (d_L.to(u.cm)).value
#dL_interpolation = interp1d(z_array, dl_cm)

l_box = 2000.0
L_box = l_box/h

N_snap, Z_snap, A_snap, DC_max, DC_min, NN_replicas = n.loadtxt(os.path.join(os.environ[env], 'snap_list_with_border.txt'), unpack=True)

env = "UCHUU"
#Mmin = float(7.0e9) # 20 times the resolution
all_in = n.array(glob.glob("/home/users/dae/comparat/Uchuu/hlists/halodir_*/halolist_*.h5" ))
all_in.sort()
for path_2_in_0 in all_in :
	print(path_2_in_0)
	N_snap_str=path_2_in_0.split('/')[-2][-3:]
	N_snap_i = float(N_snap_str)
	halolist = path_2_in_0.split('/')[-1][:-3]
	f = open("uchuu_verification/"+N_snap_str+"_"+halolist+"_todo.txt", 'w')
	g = open("uchuu_verification/"+N_snap_str+"_"+halolist+"_done.txt", 'w')
	# reads the list of snapshot available and the boundaries to be applied for each
	# previously computed with 000_geometry.py
	# Retrieve the snapshot and gets the boundaries
	sel = (N_snap == N_snap_i)
	# Get the NN parameter: very important
	# NN: number of times the box is replicated by 1 Lbox positively and negatively. Example: NN=3 , the box gets replicated by -3xL_box, -2xL_box, -1xL_box, 0xL_box, 1xL_box, 2xL_box in all X,Y,Z directions, (2*NN)^3 = 6^3 = 216 replications.
	NN = NN_replicas[sel][0]
	D2_min = DC_min[sel][0]**2
	D2_max = DC_max[sel][0]**2
	# specifyng the output directory 
	LC_dir = os.path.join( os.environ[env], "LC" )
	out_dir = os.path.join(LC_dir, path_2_in_0.split('/')[-2], 'replication_' + path_2_in_0.split('/')[-1][:-3] )
	#out_dir_2 = os.path.join(LC_dir, path_2_in_0.split('/')[-2], 'replicated_' + path_2_in_0.split('/')[-1][:-3] )
	if os.path.isdir(out_dir) == False:
		os.system("mkdir -p " + out_dir)
	#if os.path.isdir(out_dir_2) == False:
		#os.system("mkdir -p " + out_dir_2)
	fits_list = n.array(glob.glob(os.path.join( out_dir, 'replication_*', 'hlist_*.fits')))
	fits_list.sort()
	for el in fits_list:
		out = fits.open(el)

# Creates the regular pavement for the replication
pts_i = n.arange(-1 * NN, NN, 1)
ix_i, iy_i, iz_i = n.meshgrid(pts_i, pts_i, pts_i)
ix, iy, iz = n.ravel(ix_i), n.ravel(iy_i), n.ravel(iz_i)
# print(ix,iy,iz)
pts = pts_i * L_box
x0_i, y0_i, z0_i = n.meshgrid(pts, pts, pts)
x0, y0, z0 = n.ravel(x0_i), n.ravel(y0_i), n.ravel(z0_i)
XT = n.transpose([x0, y0, z0])
iXT = n.transpose([ix, iy, iz])

######################
# REPLICATE over the sky
######################
#mvir = n.array( hf['Mvir'] )
#x = n.array( hf['x'] )/h
#y = n.array( hf['y'] )/h
#z = n.array( hf['z'] )/h
#m_selection = ( mvir > Mmin )

# Loop over all (2*NN)^3 configuration to do all the replications
for jj in range(len(iXT)):
	jx,jy,jz=iXT[jj]
	out_dir2 = os.path.join(  out_dir, 'replication_'+str(jx)+'_'+str(jy)+'_'+str(jz))
	if os.path.isdir(out_dir2) == False:
		f.write("mkdir -p " + out_dir2+" \n")
	else:
		g.write("mkdir -p " + out_dir2+" \n")
	base_2_out = os.path.join(  out_dir2, 'hlist_')
	for pix_id in n.arange(3072):
		path_2_out = base_2_out + str(pix_id).zfill(4) + '.fits'
		if os.path.isfile(path_2_out)==False :
			f.write(path_2_out + "\n")
		else:
			g.write(path_2_out + "\n")

f.close()
g.close()

