"""

Merges the light cone

ls halodir_010 | wc -l 
ls halodir_011 | wc -l 
ls halodir_012 | wc -l 
ls halodir_013 | wc -l 
ls halodir_014 | wc -l 
ls halodir_015 | wc -l 
ls halodir_016 | wc -l 
ls halodir_017 | wc -l 
ls halodir_018 | wc -l 
ls halodir_019 | wc -l 
ls halodir_020 | wc -l 
ls halodir_021 | wc -l 
ls halodir_022 | wc -l 
ls halodir_023 | wc -l 
ls halodir_024 | wc -l 
ls halodir_025 | wc -l 
ls halodir_026 | wc -l 
ls halodir_027 | wc -l 
ls halodir_028 | wc -l 
ls halodir_029 | wc -l 
ls halodir_030 | wc -l 
ls halodir_031 | wc -l 
ls halodir_032 | wc -l 
ls halodir_033 | wc -l 
ls halodir_034 | wc -l 
ls halodir_035 | wc -l 
ls halodir_036 | wc -l 
ls halodir_037 | wc -l 
ls halodir_038 | wc -l 
ls halodir_039 | wc -l 
ls halodir_040 | wc -l 
ls halodir_041 | wc -l 
ls halodir_042 | wc -l 
ls halodir_043 | wc -l 
ls halodir_044 | wc -l 
ls halodir_045 | wc -l 
ls halodir_046 | wc -l 
ls halodir_047 | wc -l 
ls halodir_048 | wc -l 
ls halodir_049 | wc -l 
ls halodir_050 | wc -l 


ls halodir_010/*/repl* | wc -l 
ls halodir_011/*/repl* | wc -l 
ls halodir_012/*/repl* | wc -l 
ls halodir_013/*/repl* | wc -l 
ls halodir_014/*/repl* | wc -l 
ls halodir_015/*/repl* | wc -l 
ls halodir_016/*/repl* | wc -l 
ls halodir_017/*/repl* | wc -l 
ls halodir_018/*/repl* | wc -l 
ls halodir_019/*/repl* | wc -l 
ls halodir_020/*/repl* | wc -l 
ls halodir_021/*/repl* | wc -l 
ls halodir_022/*/repl* | wc -l 
ls halodir_023/*/repl* | wc -l 
ls halodir_024/*/repl* | wc -l 
ls halodir_025/*/repl* | wc -l 
ls halodir_026/*/repl* | wc -l 
ls halodir_027/*/repl* | wc -l 
ls halodir_028/*/repl* | wc -l 
ls halodir_029/*/repl* | wc -l 
ls halodir_030/*/repl* | wc -l 
ls halodir_031/*/repl* | wc -l 
ls halodir_032/*/repl* | wc -l 
ls halodir_033/*/repl* | wc -l 
ls halodir_034/*/repl* | wc -l 
ls halodir_035/*/repl* | wc -l 
ls halodir_036/*/repl* | wc -l 
ls halodir_037/*/repl* | wc -l 
ls halodir_038/*/repl* | wc -l 
ls halodir_039/*/repl* | wc -l 
ls halodir_040/*/repl* | wc -l 
ls halodir_041/*/repl* | wc -l 
ls halodir_042/*/repl* | wc -l 
ls halodir_043/*/repl* | wc -l 
ls halodir_044/*/repl* | wc -l 
ls halodir_045/*/repl* | wc -l 
ls halodir_046/*/repl* | wc -l 
ls halodir_047/*/repl* | wc -l 
ls halodir_048/*/repl* | wc -l 
ls halodir_049/*/repl* | wc -l 
ls halodir_050/*/repl* | wc -l 

nohup python 001_merge_uchuu.py  > 001_merge.log &

"""
import numpy as n
import os, sys, glob
import healpy

NSIDE = 16
all_npix = n.arange( healpy.nside2npix( NSIDE ) )

env = "UCHUU"

stilts_tcat_cmd = "/home/users/dae/comparat/software/stilts/stilts tcat "

in_dir = os.path.join( os.environ[env], "HPX8" )
out_dir = os.path.join( os.environ[env], "final" )
if os.path.isdir(out_dir) == False:
	os.system("mkdir -p " + out_dir)

for pix_id in all_npix:
	path_2_out = n.array(glob.glob(os.path.join( in_dir , 'halodir_*', str(pix_id).zfill(4) + '.fits')))
	path_2_out.sort()
	out_file = os.path.join(out_dir, str(pix_id).zfill(4)+'.fits')
	print(path_2_out)
	if len(path_2_out)==0:
		pass
	elif len(path_2_out)==1:
		cmd = "cp " +path_2_out[0] +" " + out_file
		print(cmd)
		os.system(cmd)
	elif len(path_2_out)>1:
		list_file = os.path.join(out_dir, str(pix_id).zfill(4)+'.list')
		print('saving list', list_file)
		n.savetxt(list_file, path_2_out, fmt = '%s')
		tcat_command = stilts_tcat_cmd + "in=@" + list_file + """ ifmt=fits omode=out ofmt=fits out="""+ out_file
		print('tcat_command', tcat_command)
		os.system(tcat_command)
		os.system('gzip '+out_file)
