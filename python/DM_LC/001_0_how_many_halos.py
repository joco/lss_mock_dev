"""
What it does
------------

counts the number of halos 

nohup python 001_0_how_many_halos.py MD10 > logs/001_0_how_many_halos_MD10.log &
nohup python 001_0_how_many_halos.py MD04 > logs/001_0_how_many_halos_MD04.log &
nohup python 001_0_how_many_halos.py MD40 > logs/001_0_how_many_halos_MD40.log &
nohup python 001_0_how_many_halos.py UNIT_fA1_DIR > logs/001_0_how_many_halos_UNIT_fA1_DIR.log &
nohup python 001_0_how_many_halos.py UNIT_fA2_DIR > logs/001_0_how_many_halos_UNIT_fA2_DIR.log &
nohup python 001_0_how_many_halos.py UNIT_fA1i_DIR > logs/001_0_how_many_halos_UNIT_fA1i_DIR.log & 
"""
# import python packages

import sys, os, glob
import astropy.io.fits as fits
from astropy.table import Table, Column
import numpy as n
import time
print('count halos')

env = sys.argv[1]

test_dir = os.path.join(os.environ[env], 'fits')
path_2_light_cones = glob.glob(os.path.join(test_dir, 'all_?.?????.fits'))
path_2_light_cones.sort()

output_file_5e14 = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'DM_LC', 'halo_counts', env+'_halo_count_5e14.ascii')
output_file_1e14 = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'DM_LC', 'halo_counts', env+'_halo_count_1e14.ascii')
output_file_5e13 = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'DM_LC', 'halo_counts', env+'_halo_count_5e13.ascii')
output_file_7e13 = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'DM_LC', 'halo_counts', env+'_halo_count_7e13.ascii')
output_file_all = os.path.join(os.environ['GIT_AGN_MOCK'], 'python', 'DM_LC', 'halo_counts', env+ '_halo_count_all.ascii')
# import all pathes
#envs = n.array(['MD10', 'MD04', 'MD40', 'UNIT_fA1i_DIR', 'UNIT_fA1_DIR', 'UNIT_fA2_DIR'])
f_all  = open(output_file_all, 'w')
f_5e14 = open(output_file_5e14, 'w')
f_1e14 = open(output_file_1e14, 'w')
f_5e13 = open(output_file_5e13, 'w')
f_7e13 = open(output_file_7e13, 'w')

t0 = time.time()
f_all.write('# aexp N_distinct N_sat \n ')
f_5e13.write('# aexp N_distinct N_sat \n ')
f_7e13.write('# aexp N_distinct N_sat \n ')
f_1e14.write('# aexp N_distinct N_sat \n ')
f_5e14.write('# aexp N_distinct N_sat \n ')

#NN = []
for path_2_light_cone in path_2_light_cones[::-1]:
	f1 = fits.open(path_2_light_cone)
	mass = f1[1].data['Mvir']
	PIDS = f1[1].data['pid']
	distinct = (PIDS==-1)

	# first selection
	msel = (mass>1)
	# counts halos
	N_all = len(PIDS[msel])
	N_d = len(PIDS[msel & distinct])
	# writes the output
	f_all.write( os.path.basename(path_2_light_cone)[4:-5]+' '+str( N_d)+' '+str( N_all-N_d)+' \n ' )

	# second selection
	msel = (mass>5e13)
	# counts halos
	N_all = len(PIDS[msel])
	N_d = len(PIDS[msel & distinct])
	# writes the output
	f_5e13.write( os.path.basename(path_2_light_cone)[4:-5]+' '+str( N_d)+' '+str( N_all-N_d)+' \n ' )

	# second selection
	msel = (mass>7e13)
	# counts halos
	N_all = len(PIDS[msel])
	N_d = len(PIDS[msel & distinct])
	# writes the output
	f_7e13.write( os.path.basename(path_2_light_cone)[4:-5]+' '+str( N_d)+' '+str( N_all-N_d)+' \n ' )

	# second selection
	msel = (mass>1e14)
	# counts halos
	N_all = len(PIDS[msel])
	N_d = len(PIDS[msel & distinct])
	# writes the output
	f_1e14.write( os.path.basename(path_2_light_cone)[4:-5]+' '+str( N_d)+' '+str( N_all-N_d)+' \n ' )

	# second selection
	msel = (mass>5e14)
	# counts halos
	N_all = len(PIDS[msel])
	N_d = len(PIDS[msel & distinct])
	# writes the output
	f_5e14.write( os.path.basename(path_2_light_cone)[4:-5]+' '+str( N_d)+' '+str( N_all-N_d)+' \n ' )

	f1.close()

f_all.close() 
f_5e13.close() 
f_7e13.close() 
f_1e14.close() 
f_5e14.close() 

