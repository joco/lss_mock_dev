"""

Replicates the snapshot over the sky and extracts halos in a given shell of comoving distance
Does full sky replication

Parameters
==========

 * l_box: length of the box in Mpc/h, example 400
 * h: unitless hubble parameter h=H0/100mk/s/Mpc, example 0.6777
 * N_skip: how many lines of header should be skipped while reading the halo file
 * env: environment variable linking to the simulation directory, example MD04 MD10, UNIT_fA1_DIR ...
 * path_2_header: path to the header of the output file
 * path_2_in_0: path to the rockstar hlist file, example /path/2/simulation/hlist_1.00000.h5

/home/users/dae/comparat/Uchuu/hlists/halodir_050/halolist_z0p00_0.h5
/home/users/dae/comparat/Uchuu/LC/halodir_050/halolist_z0p00_0.h5

Processing
==========

Generates and executes commands to replicate and filter the halo files. Lines 120 - 129 are the most important lines of the script.

outputs
=======

 * directory(path_2_in_0)/replication_$expansionParameter/all the files: contains a single file per replication. Deleted after the computation finishes.
 * directory(path_2_in_0)/replicated_$expansionParameter/one summary file: contains one file with all replication concatenated. Could be deleted after the computation is done.
 * $env/fits/all_'+$expansionParameter+'.fits') the former file converted to fits staged into the final directory. Keep this one ! It is the shell containing all the halos

python 000_process_uchuu.py /home/users/dae/comparat/Uchuu/hlists/halodir_050/halolist_z0p00_0.h5 

"""
print('runs 001_process_hlists.py with arguments' )
import time
t0=time.time()
import numpy as n
import os, sys, subprocess
print(sys.argv)
import h5py
from astropy.table import Table, Column
from astropy.coordinates import SkyCoord
import healpy
from scipy.interpolate import interp1d
import astropy.io.fits as fits
from scipy.special import erf
from scipy.stats import norm
import scipy
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u

from dustmaps.planck import PlanckQuery
planck = PlanckQuery()

path_2_NH_map = '/home/users/dae/comparat/h1_maps/H14PI/asu.fit'
NH_DATA2 = fits.open(path_2_NH_map)[1].data
nh_val = NH_DATA2['NHI']

#l_box = float(sys.argv[1]) # = 1000.0 
#h_str = sys.argv[2] # = 0.6777
#env = sys.argv[4]
#Mmin_str = sys.argv[5] # '9.0e9'
#path_2_in_0 = sys.argv[6]

cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
h = 0.6774
cosmo = cosmoUNIT

z_array = n.arange(0, 7.5, 0.001)
dc_to_z = interp1d(cosmo.comoving_distance(z_array), z_array)

d_L = cosmo.luminosity_distance(z_array)
dl_cm = (d_L.to(u.cm)).value
dL_interpolation = interp1d(z_array, dl_cm)

l_box = 2000.0
L_box = l_box/h

env = "UCHUU"
Mmin = float(7.0e9) # 20 times the resolution
path_2_in_0 = sys.argv[1] #"/home/users/dae/comparat/Uchuu/hlists/halodir_050/halolist_z0p00_0.h5"
#path_2_in_0 = "/home/users/dae/comparat/Uchuu/hlists/halodir_050/halolist_z0p00_0.h5"


N_snap_i = float(path_2_in_0.split('/')[-2][-3:])

# reads the list of snapshot available and the boundaries to be applied for each
# previously computed with 000_geometry.py
N_snap, Z_snap, A_snap, DC_max, DC_min, NN_replicas = n.loadtxt(os.path.join(os.environ[env], 'snap_list_with_border.txt'), unpack=True)

# Retrieve the snapshot and gets the boundaries
sel = (N_snap == N_snap_i)
# Get the NN parameter: very important
# NN: number of times the box is replicated by 1 Lbox positively and negatively. Example: NN=3 , the box gets replicated by -3xL_box, -2xL_box, -1xL_box, 0xL_box, 1xL_box, 2xL_box in all X,Y,Z directions, (2*NN)^3 = 6^3 = 216 replications.
NN = NN_replicas[sel][0]
D2_min = DC_min[sel][0]**2
D2_max = DC_max[sel][0]**2

# specifyng the output directory 
LC_dir = os.path.join( os.environ[env], "LC" )
out_dir = os.path.join(LC_dir, path_2_in_0.split('/')[-2], 'replication_' + path_2_in_0.split('/')[-1][:-3] )
#out_dir_2 = os.path.join(LC_dir, path_2_in_0.split('/')[-2], 'replicated_' + path_2_in_0.split('/')[-1][:-3] )
if os.path.isdir(out_dir) == False:
	os.system("mkdir -p " + out_dir)
#if os.path.isdir(out_dir_2) == False:
	#os.system("mkdir -p " + out_dir_2)


# STELLAR MASS
# Equations 1 of Comparat et al. 2019
def meanSM(Mh, z): return n.log10(Mh * 2. * (0.0351 - 0.0247 * z / (1. + z)) / ((Mh / (10**(11.79 + 1.5 * z / (1. + z))))** (- 0.9 + 0.5 * z / (1. + z)) + (Mh / (10**(11.79 + 1.5 * z / (1. + z))))**(0.67 + 0.2 * z / (1. + z))))

# QUENCHED fraction
def scatter_Qf(z): return - 0.45 * (1 + z) + 1.54

def log10_M0_Qf(z): return 9.71 + 0.78 * (1 + z)

def fraction_Qf(mass, z): return 0.5 + 0.5 * erf((mass - log10_M0_Qf(z)) / scatter_Qf(z))

def beta_z(z): return -0.57 * z + 1.43

def alpha_z(z): return 6.32 * z - 16.26

# SFR
def mean_SFR_Q(mass, z): return mass * beta_z(z) + alpha_z(z)

def scale_z(z): return -0.34 * z + 0.99

# Hard X-ray emission, after Aird et al. 2018
def galaxy_lx(redshift, mass, sfr):
	return 10**(28.81) * (1 + redshift)**(3.9) * mass + \
        10**(39.5) * (1 + redshift)**(0.67) * sfr**(0.86)


# reading input file
hf = h5py.File( path_2_in_0, 'r')
# hf.keys()
# <KeysViewHDF5 ['A_x', 'A_x_500c', 'A_y', 'A_y_500c', 'A_z', 'A_z_500c', 'Acc_Log_Vmax_1_Tdyn', 'Acc_Log_Vmax_Inst', 'Acc_Rate_100Myr', 'Acc_Rate_1_Tdyn', 'Acc_Rate_2_Tdyn', 'Acc_Rate_Inst', 'Acc_Rate_Mpeak', 'Acc_Scale', 'Breadth_first_ID', 'Depth_first_ID', 'First_Acc_Mvir', 'First_Acc_Scale', 'First_Acc_Vmax', 'Future_merger_MMP_ID', 'Halfmass_Radius', 'Halfmass_Scale', 'Jx', 'Jy', 'Jz', 'Last_mainleaf_depthfirst_ID', 'Last_progenitor_depthfirst_ID', 'Log_Vmax_Vmax_max_Tdyn_Tmpeak_', 'M200b', 'M200c', 'M2500c', 'M500c', 'M_pe_Behroozi', 'M_pe_Diemer', 'Macc', 'Mpeak', 'Mpeak_Scale', 'Mvir', 'Mvir_all', 'Next_coprogenitor_depthfirst_ID', 'Orig_halo_ID', 'Rs_Klypin', 'Rvir', 'Snap_idx', 'Spin', 'Spin_Bullock', 'T_U', 'Tidal_Force', 'Tidal_Force_Tdyn', 'Tidal_ID', 'Time_to_future_merger', 'Tree_root_ID', 'Vacc', 'Vmax_Mpeak', 'Voff', 'Vpeak', 'Xoff', 'b_to_a', 'b_to_a_500c', 'c_to_a', 'c_to_a_500c', 'desc_id', 'desc_pid', 'desc_scale', 'id', 'mmp', 'num_prog', 'phantom', 'pid', 'rs', 'rvmax', 'sam_Mvir', 'scale', 'scale_of_last_MM', 'upid', 'vmax', 'vrms', 'vx', 'vy', 'vz', 'x', 'y', 'z']>

#pid  = np.array( hf['pid']  )
#hf.close()

# Creates the regular pavement for the replication
pts_i = n.arange(-1 * NN, NN, 1)
ix_i, iy_i, iz_i = n.meshgrid(pts_i, pts_i, pts_i)
ix, iy, iz = n.ravel(ix_i), n.ravel(iy_i), n.ravel(iz_i)
# print(ix,iy,iz)
pts = pts_i * L_box
x0_i, y0_i, z0_i = n.meshgrid(pts, pts, pts)
x0, y0, z0 = n.ravel(x0_i), n.ravel(y0_i), n.ravel(z0_i)
XT = n.transpose([x0, y0, z0])
iXT = n.transpose([ix, iy, iz])

######################
# REPLICATE over the sky
######################
mvir = n.array( hf['Mvir'] )
x = n.array( hf['x'] )/h
y = n.array( hf['y'] )/h
z = n.array( hf['z'] )/h
m_selection = ( mvir > Mmin )

# Loop over all (2*NN)^3 configuration to do all the replications
for jj in range(len(iXT)):
	jx,jy,jz=iXT[jj]
	xt = XT[jj]
	x0, y0, z0 = xt
	d2 = ( x + x0 )**2 + ( y + y0 )**2 + ( z + z0 )**2
	d_selection = ( d2 > D2_min ) & ( d2 < D2_max ) & ( m_selection )
	N_selected = len(x[d_selection])
	if N_selected>0:
		out_dir2 = os.path.join(  out_dir, 'replication_'+str(jx)+'_'+str(jy)+'_'+str(jz))
		if os.path.isdir(out_dir2) == False:
			os.system("mkdir -p " + out_dir2)
		base_2_out = os.path.join(  out_dir2, 'hlist_')
		print(base_2_out, N_selected)
		xn = x[d_selection] + x0
		yn = y[d_selection] + y0
		zn = z[d_selection] + z0
		rr = d2[d_selection]**0.5
		# now computing coordinates 
		vxn = hf['vx'][d_selection]
		vyn = hf['vy'][d_selection]
		vzn = hf['vz'][d_selection]
		# angular coordinates
		theta = n.arccos(zn / rr) * 180 / n.pi
		phi = n.arctan2(yn, xn) * 180 / n.pi
		ra = phi + 180.
		dec = theta - 90.
		# galactic and ecliptic coordinates
		coords = SkyCoord(ra, dec, unit='deg', frame='icrs')
		bb_gal = coords.galactic.b.value
		ll_gal = coords.galactic.l.value
		bb_ecl = coords.barycentrictrueecliptic.lat
		ll_ecl = coords.barycentrictrueecliptic.lon
		# extinction
		ebv = planck(coords)
		# line of sight, redshift
		redshift_R = dc_to_z(rr)
		vPara = (vxn * xn + vyn * yn + vzn * zn) / rr
		rr_s = rr + vPara / cosmo.H(redshift_R).value
		rr_s[rr_s <= 0] = rr[rr_s <= 0]
		redshift_S = dc_to_z(rr_s)
		# Galaxy properties
		N_halos = len(redshift_R)
		Mvir = mvir[d_selection] / h
		mean_SM = meanSM(Mvir, redshift_R)
		rds = norm.rvs(loc=0, scale=0.15, size=N_halos)
		mass = mean_SM + rds  # fun(mean_SM)
		# STAR FORMATION RATE (valid only for star forming galaxies !)
		sfr = n.zeros_like(redshift_R)
		# whitaker et al 2012, Eq. 1,2,3.
		mean_SFR = (0.70 - 0.13 * redshift_R) * (mass - 10.5) + 0.38 + 1.14 * redshift_R - 0.19 * redshift_R**2.
		rds2 = norm.rvs(loc=0, scale=0.34, size=N_halos)
		log_sfr = mean_SFR + rds2
		# quiescent fraction fitted on COSMOS, Ilbert et al. 2013 v2 catalogue
		rds_Qf = n.random.random(N_halos)
		frac = fraction_Qf(mass, redshift_R)
		frac[ redshift_R > 2 ] = 0.
		SF = (rds_Qf > frac)
		QU = (SF == False)
		# mass-SFR sequence for the quiescent
		sfr_Q = n.zeros_like(redshift_R)
		rds2 = norm.rvs(loc=0, scale=1., size=len(redshift_R[QU])) * scale_z(redshift_R[QU])
		log_sfr_Q = mean_SFR_Q(mass[QU], redshift_R[QU]) + rds2
		# change SFR for the quiesent selection
		log_sfr[QU] = log_sfr_Q
		# galaxy LX
		gal_LX = galaxy_lx(redshift_R, 10**mass, 10**log_sfr)
		# healpix ids
		HEALPIX = healpy.ang2pix(1024, n.pi / 2. - bb_gal * n.pi / 180., 2 * n.pi - ll_gal * n.pi / 180.)
		NH = nh_val[HEALPIX]
		# healpix index
		NSIDE = 16
		HEALPIX_val = healpy.ang2pix(NSIDE, n.pi/2. - dec*n.pi/180. , ra*n.pi/180. , nest=True)
		sampled_idx = n.unique(HEALPIX_val)
		print('N pixels', len(sampled_idx))
		for pix_id in sampled_idx: #n.arange(healpy.nside2npix):
			pix_selection = ( HEALPIX_val == pix_id )
			path_2_out = base_2_out + str(pix_id).zfill(4) + '.fits'
			if os.path.isfile(path_2_out)==False :
				t = []
				for kk in hf.keys():
					if kk=='x':
						t.append(fits.Column(name=kk, array = xn[pix_selection], unit='Mpc', format=hf[kk].dtype ))
					elif kk=='y':
						t.append(fits.Column(name=kk, array = yn[pix_selection], unit='Mpc', format=hf[kk].dtype))
					elif kk=='z':
						t.append(fits.Column(name=kk, array = zn[pix_selection], unit='Mpc', format=hf[kk].dtype))
					else:
						t.append(fits.Column(name=kk, array=hf[kk][d_selection][pix_selection], unit='', format=hf[kk].dtype))

				t.append(fits.Column(name='HEALPIX_'+str(NSIDE), array=HEALPIX_val[pix_selection], unit='', format='K'))
				# luminosity distance
				dL_cm = dL_interpolation(redshift_R)
				# coordinate columns
				t.append(fits.Column(name='RA', array=ra[pix_selection], unit='deg', format = 'D'))
				t.append(fits.Column(name='DEC', array=dec[pix_selection], unit='deg', format = 'D'))
				#
				t.append(fits.Column(name='g_lat', array=bb_gal[pix_selection], unit='deg', format = 'D'))
				t.append(fits.Column(name='g_lon', array=ll_gal[pix_selection], unit='deg', format = 'D'))
				#
				t.append(fits.Column(name='ecl_lat', array=bb_ecl[pix_selection], unit='deg', format = 'D'))
				t.append(fits.Column(name='ecl_lon', array=ll_ecl[pix_selection], unit='deg', format = 'D'))
				#
				t.append(fits.Column(name='redshift_R', array=redshift_R[pix_selection], unit='', format = 'D'))
				t.append(fits.Column(name='redshift_S', array=redshift_S[pix_selection], unit='', format = 'D'))
				#
				t.append(fits.Column(name='dL', array=dL_cm[pix_selection], unit='cm', format = 'D'))
				#
				t.append(fits.Column(name='nH', array=NH[pix_selection], unit='cm**(-2)', format = 'D'))
				t.append(fits.Column(name='ebv', array=ebv[pix_selection], unit='mag', format = 'D'))
				# columns added
				t.append(fits.Column(name='SMHMR_mass', array=mass[pix_selection], unit='log10(Msun)', format = 'D'))
				t.append(fits.Column(name='star_formation_rate', array=log_sfr[pix_selection], unit='log10(Msun/yr)', format = 'D'))
				t.append(fits.Column(name='is_quiescent', array=QU[pix_selection], unit='', format = 'L'))
				t.append(fits.Column(name='LX_hard', array=n.log10(gal_LX)[pix_selection], unit='log10(erg/s)', format = 'D'))
				t.append(fits.Column(name='mag_abs_r', array=n.zeros_like(mass)[pix_selection], unit='mag', format = 'D'))
				t.append(fits.Column(name='mag_r', array=n.zeros_like(mass)[pix_selection], unit='mag', format = 'D'))
				# writing
				print('writing', path_2_out)
				tbhdu = fits.BinTableHDU.from_columns(fits.ColDefs(t))
				tbhdu.header['author'] = 'JC'
				if os.path.isfile(path_2_out):
					os.remove(path_2_out)
				tbhdu.writeto(path_2_out)

