"""
output columns :
    name = 'M200c'; format = 'D'
    name = 'M500c'; format = 'D'
    name = 'Mpeak'; format = 'D'
    name = 'Mpeak_Scale'; format = 'D'
    name = 'Mvir'; format = 'D'
    name = 'Mvir_all'; format = 'D'
    name = 'Rs_Klypin'; format = 'D'
    name = 'Rvir'; format = 'D'
    name = 'Spin'; format = 'D'
    name = 'T_U'; format = 'D'
    name = 'Vmax_Mpeak'; format = 'D'
    name = 'Voff'; format = 'D'
    name = 'Vpeak'; format = 'D'
    name = 'Xoff'; format = 'D'
    name = 'b_to_a'; format = 'D'
    name = 'b_to_a_500c'; format = 'D'
    name = 'c_to_a'; format = 'D'
    name = 'c_to_a_500c'; format = 'D'
    name = 'id'; format = 'K'
    name = 'pid'; format = 'K'
    name = 'rs'; format = 'D'
    name = 'scale_of_last_MM'; format = 'D'
    name = 'vmax'; format = 'D'
    name = 'vrms'; format = 'D'
    name = 'vx'; format = 'D'
    name = 'vy'; format = 'D'
    name = 'vz'; format = 'D'
    name = 'x'; format = 'D'; unit = 'Mpc'
    name = 'y'; format = 'D'; unit = 'Mpc'
    name = 'z'; format = 'D'; unit = 'Mpc'
    name = 'RA'; format = 'D'; unit = 'deg'
    name = 'DEC'; format = 'D'; unit = 'deg'
    name = 'g_lat'; format = 'D'; unit = 'deg'
    name = 'g_lon'; format = 'D'; unit = 'deg'
    name = 'ecl_lat'; format = 'D'; unit = 'deg'
    name = 'ecl_lon'; format = 'D'; unit = 'deg'
    name = 'redshift_R'; format = 'D'
    name = 'redshift_S'; format = 'D'
    name = 'dL'; format = 'D'; unit = 'cm'
    name = 'nH'; format = 'D'; unit = 'cm-2'
    name = 'ebv'; format = 'D'; unit = 'mag'
    name = 'SMHMR_mass'; format = 'D'; unit = 'log10(Msun)'
    name = 'star_formation_rate'; format = 'D'; unit = 'log10(Msun/yr)'
    name = 'is_quiescent'; format = 'L'
    name = 'LX_hard'; format = 'D'; unit = 'log10(erg/s)'
    name = 'mag_abs_r'; format = 'D'; unit = 'mag'
    name = 'mag_r'; format = 'D'; unit = 'mag'

"""
import numpy as n
import os, sys, glob
import healpy
from astropy.table import Table, Column, vstack

NSIDE = 16
all_npix = n.arange( healpy.nside2npix( NSIDE ) )
path_2_in_0 = sys.argv[1]
pix_id = int(sys.argv[2])
env = "UCHUU"

unwanted_columns = "A_x A_x_500c A_y A_y_500c A_z A_z_500c Acc_Log_Vmax_1_Tdyn Acc_Log_Vmax_Inst Acc_Rate_100Myr Acc_Rate_1_Tdyn Acc_Rate_2_Tdyn Acc_Rate_Inst Acc_Rate_Mpeak Acc_Scale Breadth_first_ID Depth_first_ID First_Acc_Mvir First_Acc_Scale First_Acc_Vmax Future_merger_MMP_ID Halfmass_Radius Halfmass_Scale Jx Jy Jz Last_mainleaf_depthfirst_ID Last_progenitor_depthfirst_ID Log_Vmax_Vmax_max_Tdyn_Tmpeak_ M200b M2500c Macc Next_coprogenitor_depthfirst_ID Orig_halo_ID Snap_idx Spin_Bullock Tidal_Force Tidal_Force_Tdyn Tidal_ID Time_to_future_merger Tree_root_ID Vacc desc_id desc_pid desc_scale mmp num_prog phantom rvmax sam_Mvir scale upid HEALPIX_16 M_pe_Behroozi M_pe_Diemer "
to_del = unwanted_columns.split()

LC_dir = os.path.join( os.environ[env], "HPX8" )
out_dir = os.path.join(LC_dir, path_2_in_0.split('/')[-1] )
if os.path.isdir(out_dir) == False:
	os.system("mkdir -p " + out_dir)

out_dir2 = os.path.join(  path_2_in_0, '*', '*', 'hlist_')
path_2_out = n.array(glob.glob(out_dir2 + str(pix_id).zfill(4) + '.fits'))
path_2_out.sort()
out_file = os.path.join(out_dir, str(pix_id).zfill(4)+'.fits.gz')
print(path_2_out)
if len(path_2_out)==0:
	pass
elif len(path_2_out)>=1:
	tx = []
	for el in path_2_out:
		t1 = Table.read(el)
		t1.remove_columns(to_del)
		tx.append(t1)
	t_out = vstack(tx)
	t_out.write(out_file, overwrite=True)
