"""
What it does
------------

Plots the cluster model

References
----------

Command to run
--------------

python3 004_5_plot_clusters.py environmentVAR

arguments
---------

environmentVAR: environment variable linking to the directory where files are e.g. "MD10"
It will then	 work in the directory : $environmentVAR/hlists/fits/

Dependencies
------------

import time, os, sys, glob, numpy, astropy, scipy, matplotlib

"""
from astropy.table import Table, Column
from scipy.stats import scoreatpercentile
import glob
import sys
from astropy_healpix import healpy
import os
import time
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p

import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
from scipy.stats import norm
from scipy.interpolate import interp1d
import astropy.io.fits as fits
import numpy as n
print('Create file with galaxies around clusters')
print('=> Abundance matching for magnitudes')
print('=> Red sequence colors')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()


env =  sys.argv[1]
path_2_CLU_SAT_catalog = os.path.join(os.environ[env], env+'_eRO_CLU_b8_CM_0_pixS_20.0.fits')
clu_MD = fits.open(path_2_CLU_SAT_catalog)


p_2_training_sample = os.path.join(os.environ['GIT_AGN_MOCK'], 'data/training_CBP')
z_HI, m_HI = n.loadtxt( os.path.join( p_2_training_sample, 'M-z-HIFLUGCS.txt')  , unpack = True)
z_XC, m_XC = n.loadtxt( os.path.join( p_2_training_sample, 'M-z-XCOP.txt')      , unpack = True)
z_XX, m_XX = n.loadtxt( os.path.join( p_2_training_sample, 'M-z-XXL.txt')       , unpack = True)
spt = fits.open( os.path.join( p_2_training_sample, 'bleem_2015.fit'))
cdfs = fits.open( os.path.join( os.environ['HOME'], 'data/CDFS/finoguenov2015', 'J_A+A_576_A130_table4.dat.fits'))[1].data
cosmos = fits.open( os.path.join( os.environ['HOME'], 'data/COSMOS/george2011', 'xgroups.fits'))[1].data

fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters' )
if os.path.isdir(fig_dir) == False:
	os.system('mkdir -p ' + fig_dir)

# simulation setup
if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

nl = lambda sel : len(sel.nonzero()[0])


def get_x_y(clu_MD, NNN):
	sort_ids_FX_MD10 = n.argsort(clu_MD[1].data['CLUSTER_FX_soft'])
	FX_min = clu_MD[1].data['CLUSTER_FX_soft'][sort_ids_FX_MD10[-NNN-1]]
	s1 = (clu_MD[1].data['CLUSTER_FX_soft'] > FX_min )
	x_MD10, y_MD10 = clu_MD[1].data['redshift_R'][s1], n.log10(clu_MD[1].data['HALO_M500c'][s1])
	print('N', nl(s1))
	return x_MD10, y_MD10, FX_min

x_MD10, y_MD10, FX_min_MD10 = get_x_y(clu_MD, 100000)

fig_out = os.path.join(fig_dir, 'M500c-zpng')

p.figure(1, (7., 5.))
p.axes([0.15, 0.15, 0.75, 0.75])
#p.plot(x_MD10, y_MD10, 'm+', label='MDPL2, 100,000 clusters with F_X>$10^{'+str(n.round(n.log10(FX_min_MD10),2))+'}$', rasterized=True, alpha=0.1)
p.plot(x_MD10, y_MD10, 'mo', label='MDPL2', rasterized=True, alpha=0.01)
p.plot(spt[1].data['z'], n.log10(spt[1].data['M500c']*1e14),  'kx', label='SPT', rasterized=True)
p.plot(z_HI, m_HI, 'gs', label='HIFLUGCS', rasterized=True)
p.plot(z_XC, m_XC, 'b*', label='X-COP', rasterized=True)
p.plot(z_XX, m_XX, 'ro', label='XXL', rasterized=True)
p.plot(cdfs['z'], n.log10(cdfs['M200']*1e13), 'k*', label='CDFS M200c', rasterized=True)
p.plot(cosmos['REDSHIFT'], cosmos['M200c'], 'b+', label='COSMOS M200c', rasterized=True)
p.legend(fontsize=12, loc=2)#frameon=False)
p.xlabel('redshift')
p.ylabel(r'$\log_{10}(M_{500c}/M_\odot)$')
#p.ylabel('probability distribution function')
p.grid()
p.xlim((0.01,2.1))
p.ylim((12.9,15.5))
# p.yscale('log')
p.xscale('log')
p.savefig(fig_out)
p.clf()

x_MD10, y_MD10, FX_min_MD10 = get_x_y(clu_MD, 600000)

fig_out = os.path.join(fig_dir, 'M500c-z-1em14.png')

p.figure(1, (7., 5.))
p.axes([0.15, 0.15, 0.75, 0.75])
p.plot(x_MD10, y_MD10, 'mo', label='MDPL2', rasterized=True, alpha=0.01)#, 600,000 clusters with F_X>$10^{'+str(n.round(n.log10(FX_min_MD10),2))+'}$', rasterized=True, alpha=0.1)
p.plot(spt[1].data['z'], n.log10(spt[1].data['M500c']*1e14),  'kx', label='SPT', rasterized=True)
p.plot(z_HI, m_HI, 'gs', label='HIFLUGCS', rasterized=True)
p.plot(z_XC, m_XC, 'b*', label='X-COP', rasterized=True)
p.plot(z_XX, m_XX, 'ro', label='XXL', rasterized=True)
p.plot(cdfs['z'], n.log10(cdfs['M200']*1e13), 'k*', label='CDFS M200c', rasterized=True)
p.plot(cosmos['REDSHIFT'], cosmos['M200c'], 'b+', label='COSMOS M200c', rasterized=True)
p.legend(fontsize=12, loc=2)
p.xlabel('redshift')
p.ylabel(r'$\log_{10}(M_{500c}/M_\odot)$')
#p.ylabel('probability distribution function')
p.grid()
p.xlim((0.01,2.1))
p.ylim((12.9,15.5))
# p.yscale('log')
p.xscale('log')
p.savefig(fig_out)
p.clf()

sys.exit()

nl = lambda sel : len(sel.nonzero()[0])
s1 = clu[1].data['CLUSTER_FX_soft']>2e-14
print(nl(s1))
x_MD10, y_MD10 = clu_MD[1].data['redshift_R'][s1], n.log10(clu_MD[1].data['HALO_M500c'][s1])

# opti0nal red sequence figure
fig_out = os.path.join(fig_dir, 'M500c-z-1em14.png')

p.figure(1, (6., 6.))

p.plot(x_MD10, y_MD10, 'm+', label='FX>2e-14, 6e5', rasterized=True, alpha=0.1)

p.plot(spt[1].data['z'], n.log10(spt[1].data['M500c']*1e14),  'kx', label='SPT', rasterized=True)
p.plot(z_HI, m_HI, 'gs', label='HIFLUGCS', rasterized=True)
p.plot(z_XC, m_XC, 'b*', label='X-COP', rasterized=True)
p.plot(z_XX, m_XX, 'ro', label='XXL', rasterized=True)


p.legend(frameon=False)
p.xlabel('redshift')
p.ylabel(r'$\log_{10}(M_{500c}/M_\odot)$')
# p.ylabel('probability distribution function')
p.grid()
# p.ylim((0,1.1))
# p.yscale('log')
p.xscale('log')
p.savefig(fig_out)
p.clf()

