import sys
import os
import matplotlib.pyplot as p
import matplotlib
import numpy as n
import h5py
import time
import astropy.io.fits as fits
from scipy.stats import norm
from scipy.interpolate import interp2d
from scipy.interpolate import interp1d
	
print('Plots AGN Hubble diagram	')
t0 = time.time()

matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})

stilts_cmd = 'stilts'

env = 'MD10' # sys.argv[1]  # 'MD04'
ftyp = 'all' # sys.argv[2]  # "sat"
print(env, ftyp)


from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
cosmoMD = FlatLambdaCDM(
	H0=67.77 * u.km / u.s / u.Mpc,
	Om0=0.307115)  # , Ob0=0.048206)
h = 0.6777
L_box = 1000.0 / h
cosmo = cosmoMD

zzs = n.arange(0., 3., 0.01)
DMs = cosmo.distmod(zzs).value 


test_dir = os.path.join(os.environ[env], 'hlists', 'fits')

path_2_ero_agn_file = os.path.join(test_dir, env + "_" + ftyp + "_eRO_AGN.fit")

fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'eRO_AGN', )
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)



hd = fits.open(path_2_ero_agn_file)[1].data	

# eROSITA selection

detected = (hd['AGN_FX_soft']>1e-14) & (hd['redshift_R']>0.6) & (hd['AGN_type']==11) & (hd['AGN_SDSS_r_magnitude']<23.)

data = hd[detected]

N_total = len(data['redshift_R'])
QSO_scatter = norm.rvs(loc=0, scale=0.1, size=N_total)
DM = cosmo.distmod(data['redshift_R']).value + QSO_scatter

# SN1a

SN = fits.open(os.path.join(os.environ['HOME'],'data/sn1a/jones_SN.fits'))[1].data
SNok = (SN['zSN']>0) 
N_sn1a = len(SN['zSN'][SNok])
rdsSN = norm.rvs(loc=0, scale=1, size=N_sn1a)
SN_scatter=SN['e_mB'][SNok]*rdsSN
DMSN = cosmo.distmod(SN['zSN'][SNok]).value + SN_scatter

fig_out = os.path.join(fig_dir, 'HZ_z_' + env + "_" + ftyp + '.png')

p.figure(0,(5.5,5.5))
p.plot(data['redshift_R'], DM, 'b,', label='QSO')
p.plot(SN['zSN'][SNok], DMSN, 'k+', label='SN1A')
p.xlabel('redshift')
p.ylabel('Distance modulus')
p.grid()
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'HZERR_z_' + env + "_" + ftyp + '.png')

p.figure(0,(6.0,5.5))
p.plot(data['redshift_R'], abs(QSO_scatter), 'b,', label='QSO')
p.plot(SN['zSN'][SNok], abs(SN_scatter), 'k+', label='SN1A')
p.xlabel('redshift')
p.yscale('log')
p.ylabel('Distance modulus uncertainty')
p.grid()
p.savefig(fig_out)
p.clf()

