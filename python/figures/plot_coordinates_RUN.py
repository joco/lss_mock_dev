import os
import sys
import glob
import numpy as n


def plot_coordinates(env='MD04'):
    test_dir = os.path.join(os.environ[env], 'hlists', 'fits')

    fig_dir = os.path.join(
        os.environ['GIT_AGN_MOCK'],
        'figures',
        env,
        'coordinates',
    )
    if os.path.isdir(fig_dir) == False:
        os.system('mkdir -p ' + fig_dir)

    all_files = sorted(
        n.array(
            glob.glob(
                os.path.join(
                    test_dir,
                    '???_?.?????.fits'))))

    for fff in all_files[::-1]:
        print(fff)
        cmd = 'python3 plot_coordinates.py ' + \
            env + ' ' + os.path.basename(fff)[:-5]
        print(cmd)
        try:
            os.system(cmd)
        except(OSError):
            print('os error')


plot_coordinates(env='MD04')
plot_coordinates(env='MD10')
plot_coordinates(env='UNIT_fA1_DIR')
plot_coordinates(env='UNIT_fA1i_DIR')
plot_coordinates(env='UNIT_fA2_DIR')
