import sys
import os
import astropy.io.fits as fits
import time
print('Plots Nbody data')
t0 = time.time()
stilts_cmd = 'stilts'

env = sys.argv[1]  # 'MD04'
baseName = sys.argv[2]  # "sat_0.62840"
mini_fits = sys.argv[3]
print(env, baseName)

if mini_fits:
    test_dir = os.path.join(os.environ[env], 'mini_fits')
else:
    test_dir = os.path.join(os.environ[env], 'fits')

if mini_fits:
    fig_dir = os.path.join(
        os.environ['GIT_AGN_MOCK'],
        'figures',
        env,
        'nbody_mini_fits',
    )
else:
    fig_dir = os.path.join(
        os.environ['GIT_AGN_MOCK'],
        'figures',
        env,
        'nbody',
    )

path_2_light_cone = os.path.join(test_dir, baseName + '.fits')

# def threeD_plot():
#fig_out = os.path.join(fig_dir, 'xyz_'+baseName+'.png')
# cmd = stilts_cmd+""" plot2cube \
# xpix=595 ypix=530 \
# xlabel=x ylabel=y zlabel=z xcrowd=3.3 ycrowd=3.3 zcrowd=3.6 gridaa=true fontsize=18 \
# legend=false title="""+baseName+""" \
#layer=Mark \
# in="""+path_2_light_cone+""" \
# x=x y=y z=z \
#shading=transparent \
# omode=out out="""+fig_out
# print(cmd)
# os.system(cmd)

# threeD_plot()


def twoD_plot():
    fig_out = os.path.join(fig_dir, 'xy_' + baseName + '.png')
    cmd = stilts_cmd + """ plot2plane \
   xpix=600 ypix=600 fontsize=18 \
   xlabel=x ylabel=y \
   legend=false title=""" + baseName + """ \
   layer=Mark \
      in=""" + path_2_light_cone + """ \
      x=x y=y \
      shading=auto \
	  omode=out out=""" + fig_out
    print(cmd)
    os.system(cmd)
    fig_out = os.path.join(fig_dir, 'yz_' + baseName + '.png')
    cmd = stilts_cmd + """ plot2plane \
   xpix=600 ypix=600 fontsize=18 \
   xlabel=y ylabel=z \
   legend=false title=""" + baseName + """ \
   layer=Mark \
      in=""" + path_2_light_cone + """ \
      x=y y=z \
      shading=auto \
	  omode=out out=""" + fig_out
    print(cmd)
    os.system(cmd)
    fig_out = os.path.join(fig_dir, 'xz_' + baseName + '.png')
    cmd = stilts_cmd + """ plot2plane \
   xpix=600 ypix=600 fontsize=18 \
   xlabel=x ylabel=z \
   legend=false title=""" + baseName + """ \
   layer=Mark \
      in=""" + path_2_light_cone + """ \
      x=x y=z \
      shading=auto \
	  omode=out out=""" + fig_out
    print(cmd)
    os.system(cmd)


twoD_plot()


def loglog_hist(col_name='Mvir'):
    fig_out = os.path.join(fig_dir, col_name + '_hist_' + baseName + '.png')
    cmd = stilts_cmd + """ plot2plane \
	xpix=900 ypix=500 \
	xlog=true ylog=true xlabel=""" + col_name + """ ylabel=Counts grid=true fontsize=18 \
	legend=false title=""" + baseName + """ \
	layer=Histogram \
		in=""" + path_2_light_cone + """ \
		x=""" + col_name + """ \
		barform=steps thick=4 \
		omode=out out=""" + fig_out

    print(cmd)
    os.system(cmd)


def linlog_hist(col_name='Mvir'):
    fig_out = os.path.join(fig_dir, col_name + '_hist_' + baseName + '.png')
    cmd = stilts_cmd + """ plot2plane \
	xpix=900 ypix=500 \
	ylog=true xlabel=""" + col_name + """ ylabel=Counts grid=true fontsize=18 \
	legend=false title=""" + baseName + """ \
	layer=Histogram \
		in=""" + path_2_light_cone + """ \
		x=""" + col_name + """ \
		barform=steps thick=4 \
		omode=out out=""" + fig_out

    print(cmd)
    os.system(cmd)


# loglog_hist(col_name='Mvir')
# loglog_hist(col_name='M200c')
# loglog_hist(col_name='M500c')
# loglog_hist(col_name='Rvir')
# loglog_hist(col_name='rs')
# loglog_hist(col_name='scale_of_last_MM')
# loglog_hist(col_name='vmax')
# linlog_hist(col_name='Acc_Rate_1_Tdyn')
# linlog_hist(col_name='b_to_a_500c')
# linlog_hist(col_name='c_to_a_500c')
#linlog_hist(col_name='x')
#linlog_hist(col_name='y')
#linlog_hist(col_name='z')
# linlog_hist(col_name='vx')
# linlog_hist(col_name='vy')
# linlog_hist(col_name='vz')
