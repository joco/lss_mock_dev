import glob
import sys
import os
import astropy.io.fits as fits
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
import matplotlib.pyplot as p
import matplotlib
import numpy as n
import time
t0 = time.time()


matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})

# simulation setup
cosmoMD = FlatLambdaCDM(
    H0=67.77 * u.km / u.s / u.Mpc,
    Om0=0.307115)  # , Ob0=0.048206)
h = 0.6777
L_box = 1000.0 / h
cosmo = cosmoMD

DZ = 0.1
z_bins = n.arange(0., 0.71, DZ)
m_bins = n.arange(13.5, 15.7, 0.1)
x_m = (m_bins[1:] + m_bins[:-1]) / 2.

env = 'MD10'  # sys.argv[1]
test_dir = os.path.join(os.environ[env], 'hlists', 'mocks')

file_list = sorted(
    n.array(
        glob.glob(
            os.path.join(
                test_dir,
                "MD10_all_eRO_CLU_dz*.fit"))))

Nc = {}

for file_name in file_list:
    hd = fits.open(file_name)
    m200c = hd[1].data['HALO_M200c']
    z = hd[1].data['redshift_R']
    NN = n.zeros((len(z_bins), len(m_bins) - 1))
    for jj, (zmin, zmax) in enumerate(zip(z_bins, z_bins + DZ)):
        ok = (z >= zmin) & (z < zmax)
        NN[jj] = n.histogram(m200c[ok], bins=m_bins)[0]
    Nc[os.path.basename(file_name)] = NN


ref = Nc['MD10_all_eRO_CLU_dz100_dm100.fit']

frac_diff = {}
for kk in Nc.keys():
    val = abs(Nc[kk] / ref - 1)
    val[n.isnan(val)] = 0
    val[n.isinf(val)] = 0
    frac_diff[kk] = val

ls = {}
ls['dm5'] = 'solid'
ls['dm10'] = 'dashed'
ls['dm20'] = 'dotted'
ls['dm50'] = 'dashdot'
ls['dm100'] = 'solid'

markers = {}
markers['dm5'] = 'o'
markers['dm10'] = 'v'
markers['dm20'] = '+'
markers['dm50'] = 'D'
markers['dm100'] = '|'


#n.array([ abs(Nc[kk]/ref - 1) for kk in Nc.keys() ])
for jj, (zmin, zmax) in enumerate(zip(z_bins, z_bins + DZ)):
    p.figure(0, (6, 6))
    for kk in Nc.keys():
        if 'dz100' in kk:
            lab = kk[:-4].split('_')
            p.plot(x_m,
                   100 * frac_diff[kk][jj],
                   label=lab[-1] + ' ' + lab[-2],
                   ls=ls[lab[-1]],
                   marker=markers[lab[-1]])

    p.ylabel(r'$\Delta N/N$ [%]')
    p.xlabel(r'$\log_{10}(M_{200c})$')
    p.yscale('log')
    p.tight_layout()
    p.title(str(n.round(zmin, 1)) + '<z<' + str(n.round(zmax, 1)))
    p.legend(loc=0, frameon=False)
    p.savefig(os.path.join(os.environ['GIT_AGN_MOCK'],
                           'figures',
                           env,
                           'mocks',
                           'HMF_ratios_dz100_' + str(n.round(zmin,
                                                             1)) + '_z_' + str(n.round(zmax,
                                                                                       1)) + '.png'))
    p.clf()

for jj, (zmin, zmax) in enumerate(zip(z_bins, z_bins + DZ)):
    p.figure(0, (6, 6))
    for kk in Nc.keys():
        if 'dm100' in kk:
            lab = kk[:-4].split('_')
            p.plot(x_m,
                   100 * frac_diff[kk][jj],
                   label=lab[-1] + ' ' + lab[-2],
                   ls=ls[lab[-1]],
                   marker=markers[lab[-1]])

    p.ylabel(r'$\Delta N/N$ [%]')
    p.xlabel(r'$\log_{10}(M_{200c})$')
    p.yscale('log')
    p.tight_layout()
    p.title(str(n.round(zmin, 1)) + '<z<' + str(n.round(zmax, 1)))
    p.legend(loc=0, frameon=False)
    p.savefig(os.path.join(os.environ['GIT_AGN_MOCK'],
                           'figures',
                           env,
                           'mocks',
                           'HMF_ratios_dm100_' + str(n.round(zmin,
                                                             1)) + '_z_' + str(n.round(zmax,
                                                                                       1)) + '.png'))
    p.clf()
