import sys
import os
import matplotlib.pyplot as p
import matplotlib
import numpy as n
import h5py
import time
print('Plots galaxy data')
t0 = time.time()


matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})


env = sys.argv[1]  # 'MD04'
baseName = sys.argv[2]  # "sat_0.62840"
print(env, baseName)

test_dir = os.path.join(os.environ[env], 'hlist', 'fits')

path_2_light_cone = os.path.join(test_dir, baseName + '_coordinates.h5')
path_2_galaxy_file = os.path.join(test_dir, baseName + '_galaxy.h5')

fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'galaxy', )
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)


h5_file = os.path.join(path_2_light_cone)
f = h5py.File(h5_file, "r")
zr = f['/coordinates/redshift_R'].value
zs = f['/coordinates/redshift_S'].value

g_lat = f['/coordinates/g_lat'].value
g_lon = f['/coordinates/g_lon'].value

ecl_lat = f['/coordinates/ecl_lat'].value
ecl_lon = f['/coordinates/ecl_lon'].value

ra = f['/coordinates/ra'].value
dec = f['/coordinates/dec'].value

dL = f['/coordinates/dL'].value
NH = f['/coordinates/NH'].value
ebv = f['/coordinates/ebv'].value
f.close()


h5_file = os.path.join(path_2_galaxy_file)
f = h5py.File(h5_file, "r")
galaxy_SMHMR_mass = f['/galaxy/SMHMR_mass'].value
galaxy_star_formation_rate = f['/galaxy/star_formation_rate'].value
galaxy_LX_hard = f['/galaxy/LX_hard'].value
galaxy_mag_abs_r = f['/galaxy/mag_abs_r'].value
galaxy_mag_r = f['/galaxy/mag_r'].value
f.close()

N_obj = len(galaxy_SMHMR_mass)
rds = n.random.random(N_obj)
sel = (rds < 1e6 / N_obj)

# histograms

fig_out = os.path.join(fig_dir, 'SMHMR_mass_hist_' + baseName + '.png')

X = galaxy_SMHMR_mass

p.figure(1, (6., 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('SMHMR_mass')
p.ylabel('Counts')
p.grid()
p.yscale('log')
p.savefig(fig_out)
p.clf()


fig_out = os.path.join(
    fig_dir,
    'star_formation_rate_hist_' +
    baseName +
    '.png')

X = galaxy_star_formation_rate

p.figure(1, (6., 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('galaxy_star_formation_rate')
p.ylabel('Counts')
p.grid()
p.yscale('log')
p.savefig(fig_out)
p.clf()


fig_out = os.path.join(fig_dir, 'LX_hard_hist_' + baseName + '.png')

X = galaxy_LX_hard

p.figure(1, (6., 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('galaxy_LX_hard')
p.ylabel('Counts')
p.grid()
p.yscale('log')
p.savefig(fig_out)
p.clf()


fig_out = os.path.join(fig_dir, 'mag_abs_r_hist_' + baseName + '.png')

X = galaxy_mag_abs_r

p.figure(1, (6., 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('galaxy_mag_abs_r')
p.ylabel('Counts')
p.grid()
p.yscale('log')
p.savefig(fig_out)
p.clf()


fig_out = os.path.join(fig_dir, 'mag_r_hist_' + baseName + '.png')

X = galaxy_mag_r

p.figure(1, (6., 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('galaxy_mag_r')
p.ylabel('Counts')
p.grid()
p.yscale('log')
p.savefig(fig_out)
p.clf()

# 2D plots

fig_out = os.path.join(fig_dir, 'mag_r_vs_zr_' + baseName + '.png')

X = zr[sel]
Y = galaxy_mag_r[sel]
p.figure(1, (6., 5.5))
p.tight_layout()
p.plot(X, Y, 'k,', rasterized=True)
p.title(baseName)
p.xlabel('redshift R')
p.ylabel('mag_r')
p.grid()
p.savefig(fig_out)
p.clf()


fig_out = os.path.join(fig_dir, 'mag_abs_r_vs_zr_' + baseName + '.png')

X = zr[sel]
Y = galaxy_mag_abs_r[sel]
p.figure(1, (6., 5.5))
p.tight_layout()
p.plot(X, Y, 'k,', rasterized=True)
p.title(baseName)
p.xlabel('redshift R')
p.ylabel('mag_abs_r')
p.grid()
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'SMHMR_mass_vs_zr_' + baseName + '.png')

X = zr[sel]
Y = galaxy_SMHMR_mass[sel]
p.figure(1, (6., 5.5))
p.tight_layout()
p.plot(X, Y, 'k,', rasterized=True)
p.title(baseName)
p.xlabel('redshift R')
p.ylabel('SMHMR_mass')
p.grid()
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(
    fig_dir,
    'star_formation_rate_vs_zr_' +
    baseName +
    '.png')

X = zr[sel]
Y = galaxy_star_formation_rate[sel]
p.figure(1, (6., 5.5))
p.tight_layout()
p.plot(X, Y, 'k,', rasterized=True)
p.title(baseName)
p.xlabel('redshift R')
p.ylabel('star_formation_rate')
p.grid()
p.savefig(fig_out)
p.clf()
