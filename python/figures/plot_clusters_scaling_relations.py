"""
python plot_clusters_scaling_relations.py MD10
python plot_clusters_scaling_relations.py MD04
python plot_clusters_scaling_relations.py MD40
python plot_clusters_scaling_relations.py UNIT_fA1i_DIR
python plot_clusters_scaling_relations.py UNIT_fA1_DIR
python plot_clusters_scaling_relations.py UNIT_fA2_DIR

"""

import sys
import os
from scipy.interpolate import interp1d
import astropy.io.fits as fits
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
from scipy.stats import norm
from scipy.optimize import curve_fit
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})

import matplotlib.pyplot as p

import numpy as n
#import h5py
import time
print('Plots clusters catalog, scaling relATIONS')
t0 = time.time()

#hdu_0 = get_cat(env='MD04')
#hdu_1 = get_cat(env='MD40')
env = 'MD10'

SR_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'scaling_relations')

kT_kev, kT_kev_err, Rspec, R500, R500_err, M500, M500_err, Mgas500, Mgas500_err, R2500,  R2500_err, M2500, M2500_err, Mgas2500, Mgas2500_err, tcool, tcool_err, LXxmm, LXxmm_err = n.loadtxt(os.path.join(SR_dir, 'lovisari_2015_table2.ascii'), unpack=True)

redshift_s18, nh, LX_s18, Rktmax, M500NFWFreeze, M500kTextrp, M500NFWHudson, M500NFWAll, M200NFWFreeze, M200kTextrp, M500PlanckSZ = n.loadtxt(os.path.join(SR_dir, 'schllenberger_2018_tableB2B3.ascii'), unpack=True)

s_mi20, l_mi20, b_mi20, T_mi20, LX_mi20, sig_LX_mi20, f_mi20, NH_mi20, Z_mi20 = n.loadtxt(os.path.join(SR_dir, 'migkas_2020_tableC1.ascii'), unpack=True)

B18_id,    B18_z, B18_R500, B18_LXcin, B18_LXcinbol, B18_TXcin, B18_ZXcin, B18_LXcexbol, B18_LXcex, B18_TXcex, B18_ZXcex, B18_MICM, B18_YXcin, B18_M500 = n.loadtxt(os.path.join(SR_dir, 'bulbul_2018_table1_2.ascii'), unpack=True)

Lo20_planckName, Lo20_z, Lo20_M500, Lo20_Mg500, Lo20_kT, Lo20_kTexc, Lo20_LX, Lo20_LXexc, Lo20_Lbol, Lo20_Lbolexc, Lo20_NT, Lo20_fT, Lo20_Nsb, Lo20_fsb = n.loadtxt(os.path.join(SR_dir, 'lovisari_2020_tableA1.ascii'), unpack=True)

XXL = fits.open(os.path.join('/home/comparat/data/XMM/XXL','xxl365gc.fits'))[1].data

fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters')
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)

# simulation setup
if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

area = 30575 / 2.
area = 34089. 

def get_cat(env='MD04', name='fn'):
    path_2_CLU_catalog = os.path.join(os.environ[env], name) 
    hdu_1 = fits.open(path_2_CLU_catalog)
    ra = hdu_1[1].data['RA']
    dec = hdu_1[1].data['DEC']
    bb = hdu_1[1].data['g_lat']
    sel = (abs(bb) > 10) & (hdu_1[1].data['HALO_M500c'] > 0)
    return hdu_1[1].data[sel]

hdu_MD04 = get_cat('MD04', 'MD04_eRO_CLU.fit.2')
hdu_MD10 = get_cat('MD10', 'MD10_eRO_CLU_March2020.fit')
hdu_MD40 = get_cat('MD40', 'MD40_eRO_CLU.fit')

class ScalingRelation:
    pass

def get_mean_scaling_relation(mass_array_log10, mass_proxy_array, redshift_array, DZ = 0.1, Dlog10M = 0.1, z_max_SR = 1.2 ):
	SR = ScalingRelation()
	# define the binning scheme
	z_array = n.arange(n.min(redshift_array), n.min([z_max_SR, n.max(redshift_array)]), DZ)
	m_array = n.arange(n.min(mass_array_log10), n.max(mass_array_log10), Dlog10M)
	z_mins, m_mins = n.meshgrid(z_array , m_array)
	z_maxs, m_maxs = n.meshgrid(z_array + DZ , m_array + Dlog10M)
	redshift_mins, redshift_maxs = n.ravel(z_mins), n.ravel(z_maxs)
	mass_mins, mass_maxs = n.ravel(m_mins), n.ravel(m_maxs)
	# define empty columns
	mean_mass_proxy = n.zeros_like(mass_mins)
	std_mass_proxy  = n.zeros_like(mass_mins)
	mean_mass       = n.zeros_like(mass_mins)
	mean_redshift   = n.zeros_like(mass_mins)
	# in each bin compute the mean and std of the mass proxy, mass and redshift
	for jj, (m_min, m_max, z_min, z_max) in enumerate(zip(mass_mins, mass_maxs, redshift_mins, redshift_maxs)):
		selection = ( redshift_array >= z_min ) & ( redshift_array < z_max ) & ( mass_array_log10 >= m_min ) & ( mass_array_log10 < m_max )
		mproxy_values = mass_proxy_array[ selection ]
		mm_values = mass_array_log10[ selection ]
		zz_values = redshift_array[ selection ]
		mean_mass_proxy[jj] = n.mean( mproxy_values )
		std_mass_proxy [jj] = n.std( mproxy_values )
		mean_mass      [jj] = n.mean(mm_values )
		mean_redshift  [jj] = n.mean(zz_values )
	
	# fit scaling relation for z<1.5
	good = (mean_mass>0)&(mean_mass_proxy>0)&(std_mass_proxy>0.001)&(redshift_mins < z_max_SR)
	xdata = mean_mass[good]
	ydata = mean_mass_proxy[good]
	yerr = std_mass_proxy[good]/2.
	xerr = Dlog10M/2.
	out_m = n.polyfit(xdata, ydata, deg=1, full=True)#, w=1/yerr)
	x_model = n.arange(13, 16, 0.1)
	y_model = n.polyval(out_m[0], x_model)
	SR.mass_mins  = mass_mins 
	SR.mass_maxs, redshift_mins = mass_maxs, redshift_mins
	SR.redshift_maxs = redshift_maxs
	SR.mean_mass_proxy = mean_mass_proxy
	SR.std_mass_proxy = std_mass_proxy
	SR.mean_mass = mean_mass
	SR.mean_redshift = mean_redshift
	SR.x_model = x_model
	SR.y_model = y_model
	SR.out_m = out_m
	# output values and boundaries or std
	return SR  

 
hdu_data = hdu_MD04

redshift_array = hdu_data['redshift_R']
log_EZ =  n.log10(cosmo.efunc(redshift_array) )
mass_array_log10 = n.log10(hdu_data['HALO_M500c'] ) + log_EZ
mass_proxy_array = hdu_data['CLUSTER_LX_soft_RF'] - log_EZ
DZ = 0.1
Dlog10M=0.2 
rds = n.random.random(len(mass_array_log10))
plotting_sample = (rds<0.1)

MD04 = get_mean_scaling_relation(mass_array_log10, mass_proxy_array, redshift_array, DZ = DZ, Dlog10M= Dlog10M)
MD04.mass_array_log10 = mass_array_log10
MD04.mass_proxy_array = mass_proxy_array

hdu_data = hdu_MD10

redshift_array = hdu_data['redshift_R']
log_EZ =  n.log10(cosmo.efunc(redshift_array) )
mass_array_log10 = n.log10(hdu_data['HALO_M500c'] ) + log_EZ
mass_proxy_array = hdu_data['CLUSTER_LX_soft_RF'] - log_EZ
DZ = 0.1
Dlog10M=0.2 
rds = n.random.random(len(mass_array_log10))
plotting_sample = (rds<0.1)

MD10 = get_mean_scaling_relation(mass_array_log10, mass_proxy_array, redshift_array, DZ = DZ, Dlog10M= Dlog10M)
MD10.mass_array_log10 = mass_array_log10
MD10.mass_proxy_array = mass_proxy_array

hdu_data = hdu_MD40

redshift_array = hdu_data['redshift_R']
log_EZ =  n.log10(cosmo.efunc(redshift_array) )
mass_array_log10 = n.log10(hdu_data['HALO_M500c'] ) + log_EZ
mass_proxy_array = hdu_data['CLUSTER_LX_soft_RF'] - log_EZ
DZ = 0.1
Dlog10M=0.2 
rds = n.random.random(len(mass_array_log10))
plotting_sample = (rds<0.1)

MD40 = get_mean_scaling_relation(mass_array_log10, mass_proxy_array, redshift_array, DZ = DZ, Dlog10M= Dlog10M)
MD40.mass_array_log10 = mass_array_log10
MD40.mass_proxy_array = mass_proxy_array

MD40.name = 'HMDPL'
MD10.name = 'MDPL2'
MD04.name = 'SMDPL'
######
#
# LX - M500c 
#
######

fig_out = os.path.join(fig_dir, 'M500c-LX-z.png')
#title_str=r'$M_{500c}>1\times10^{13}M_\odot$'

p.figure(0, (6., 5.))
#p.plot(mass_array_log10[plotting_sample], mass_proxy_array[plotting_sample], 'y,', rasterized=True, alpha=0.1)

MD = MD10
p.plot(MD.mass_array_log10, MD.mass_proxy_array, 'cx', rasterized=True, alpha=0.01, label=MD.name)

#MD = MD40
#p.plot(MD.mass_array_log10, MD.mass_proxy_array, 'm+', rasterized=True, alpha=0.2, label=MD.name +', y='+str(n.round(MD.out_m[0][0],2))+' x+'+str(n.round(MD.out_m[0][1],2)))

#MD = MD04
#p.plot(MD.mass_array_log10, MD.mass_proxy_array, 'c+', rasterized=True, alpha=0.2, label=MD.name +', y='+str(n.round(MD.out_m[0][0],2))+' x+'+str(n.round(MD.out_m[0][1],2)))

#MD = MD40
#p.plot(MD.x_model, MD.y_model, ls='dashed', color='m')

#MD = MD04
#p.plot(MD.x_model, MD.y_model, ls='dashed', color='c')


#for zz in n.unique(redshift_maxs)[:10]:
	#s1 = (redshift_maxs == zz) & (good)
	#label = str(n.round(zz+DZ/2,2))
	#xdata = mean_mass[s1]
	#ydata = mean_mass_proxy[s1]
	#out = n.polyfit(xdata, ydata, deg=1, full=True)#, w=1/yerr)
	#p.fill_between(mean_mass[s1], y1=mean_mass_proxy[s1]-std_mass_proxy[s1]/2., y2=mean_mass_proxy[s1]+std_mass_proxy[s1]/2.,  rasterized=True, label=str(n.round(out[0][0],2))+' z='+label, alpha=0.3)

p.plot(n.log10( Lo20_M500*1e14*cosmo.efunc(Lo20_z) ), n.log10(Lo20_LX*1e44 / cosmo.efunc(Lo20_z)), marker='o', ls='', mfc='none', label='Lo20')

p.plot( n.log10(XXL['Mgas500kpc']*1e11*10**(1.1) * cosmo.efunc(XXL['z'])), n.log10(XXL['LXXL500MT'] * 1e42/cosmo.efunc(XXL['z']) ), marker='o', ls='', label='Ad18+Se19', mfc='none')

p.plot(n.log10(M500*1e13/0.7), n.log10(LXxmm*1e43), marker='s', ls='', mfc='none', label='Lo15')

p.plot(n.log10(B18_M500*1e14*cosmo.efunc(B18_z)), n.log10(B18_LXcin * 1e44 / cosmo.efunc(B18_z)), marker='*', ls='', mfc='none', label='Bu18')

p.plot(n.log10(M500NFWFreeze*1e14*cosmo.efunc(redshift_s18)), n.log10(LX_s18*1e43/cosmo.efunc(redshift_s18)), marker='o', ls='', label='Sc18', mfc='none')

MD = MD10
p.plot(MD.x_model, MD.y_model, ls='dashed', color='y', label='y='+str(n.round(MD.out_m[0][0],1))+' x+'+str(n.round(MD.out_m[0][1],1)))

p.xlabel(r'$\log_{10}(E(z) M_{500c}/M_\odot)$')
p.ylabel(r'$\log_{10}(L_X/E(z)$ [erg/s])')
p.legend(frameon=True, loc=0, fontsize=11)
#p.xscale('log')
#p.yscale('log')
p.xlim((13, 15.5))
#p.ylim((-3, 1.5))
#p.title(title_str)
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()


sys.exit()

######
#
# LX - kT 
#
######


# scaling relations from Giles 2015
fun = lambda T, z, A, B, gama, T_0=3, L_0=3e43 : L_0 * cosmo.efunc(z)**gama * A * (T/T_0)**B
LT_Giles15 = lambda T : fun(T, 0.3, 0.71, 2.63, 1.64)/cosmo.efunc(0.3)
LT_Clerc14 = lambda T : fun(T, 0.3, 1.26, 3.12, 1.0) /cosmo.efunc(0.3)
LT_Pratt09 = lambda T : fun(T, 0.3, 1.54, 2.97, 1.0) /cosmo.efunc(0.3)

LX_K15 = lambda T, z, na, nb, T_0=5, L_0=1e44 : L_0 * 10**(0.23 + 2.65 * n.log10(T * cosmo.efunc(z)**nb / T_0)) / cosmo.efunc(z)**na

LT_Kettula15 = lambda T : LX_K15(T, 0.3, 0., -1.0) /cosmo.efunc(0.3)


all_ts = n.arange(0.5,10.,0.1)

fig_out = os.path.join(fig_dir, 'LX-KT-Z.png')
title_str=r'$M_{500c}>1\times10^{13}M_\odot$'

LX = hdu_2['CLUSTER_LX_soft_RF'] # n.log10(hdu_2['CLUSTER_FX_soft'])+2*n.log10(hdu_2['Dl'])+n.log10(4*n.pi)
zz = hdu_2['redshift_R']
z_data = hdu_2['redshift_R'] # n.log10(hdu_2['HALO_M500c'] )
y_data = LX - n.log10(cosmo.efunc(zz))
x_data = hdu_2['CLUSTER_kT']

rds = n.random.random(len(x_data))
Nmax = 500.
frac_plot = len(x_data)/Nmax

XX,YY,ZZ = x_data[rds<frac_plot], y_data[rds<frac_plot], z_data[rds<frac_plot]

p.figure(0, (6., 5.))
p.scatter(XX, YY, c=ZZ, s=2, marker='o', edgecolors='face', rasterized=True, cmap='tab20')

p.plot( all_ts, n.log10(LT_Giles15(all_ts)) , label='G16', ls='dotted',  lw=2, color='k')
p.plot( all_ts, n.log10(LT_Clerc14(all_ts)) , label='C14', ls='dashed',  lw=2, color='k')
p.plot( all_ts, n.log10(LT_Pratt09(all_ts)) , label='P09', ls='dashdot', lw=2, color='k')
p.plot( all_ts, n.log10(LT_Kettula15(all_ts)) , label='K15', ls='solid', lw=2, color='k')

p.colorbar(label=r'$\log_{10}({M_{500c}/M_\odot})$')
p.xlabel('kT [keV]')
p.ylabel(r'$L_X/E(z)$ [erg/s]')
p.legend(frameon=False, loc=0)
p.xscale('log')
#p.yscale('log')
#p.xlim((-14.2, -10.5))
#p.ylim((-3, 1.5))
#p.title(title_str)
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()


# MANTZ 2016
N_Mgas = 31.98
N_kT 	= 2.18
N_L 	= 103.7
N_Lce 	= 102.66

slope_E_Mgas 	= -0.11
slope_E_kT 	= 0.61
slope_E_L 	= 1.20
slope_E_Lce 	= 1.82

slope_M500_Mgas= 1.04
slope_M500_kT 	= 0.66
slope_M500_L 	= 1.26
slope_M500_Lce = 1.06

scatter_Mgas = 0.086
scatter_kT = 0.18
scatter_L = 0.24
scatter_Lce = 0.17

E035 = cosmo.efunc(0.35)

# converts logM500 to clusters observables

def m500_to_qty(logM500, z, slope_efunc, slope_m500, normalization): return n.e**normalization * \
    (cosmo.efunc(z) / E035)**(slope_efunc) * (10**(logM500 - n.log10(6) - 14))**(slope_m500)

def logM500_to_kT(
    logM500,
    z): return m500_to_qty(
        logM500,
        z,
        slope_E_kT,
        slope_M500_kT,
    N_kT)

all_m500c = n.arange(13,16,0.1)
mantz16_kts = logM500_to_kT(all_m500c, n.ones_like(all_m500c)*0.35)*E035**(2./3.)

# Arnaud 2005
all_kts = n.arange(0.1, 30, 0.1)
arnaud05_kts = lambda kt, z : 3.84*10**(14) * (kt/5)**1.71 / (cosmo.H(z)/100) 
arnaud05_m500c = arnaud05_kts(all_kts, n.ones_like(all_kts)*0.1 ).value
arnaud05_KT = all_kts*cosmo.efunc(0.1)**(2./3.)

# Bulbul et al. 2018 scaling relations
z_pivot = 0.45
M_pivot = 6.35 * 10**(14)
E_pivot = cosmo.efunc(z_pivot)
# Eq 17 T X cin
def SR_0(m500, z): return 6.41 * (m500 / M_pivot) ** 0.799 * \
    (cosmo.efunc(z) / E_pivot)**(2. / 3.) * ((1 + z) / (1 + z_pivot))**(-0.36)

bulbul18_kts = SR_0(10**all_m500c, n.ones_like(all_m500c)*0.45)*E_pivot**(2./3.)

fig_out = os.path.join(fig_dir, 'M500-KT-Z.png')
title_str=r'$M_{500c}>1\times10^{13}M_\odot$'

z_data = hdu_2['redshift_R']
y_data = hdu_2['CLUSTER_kT']/cosmo.efunc(z_data)**(2./3.)
x_data = hdu_2['HALO_M500c'] 

rds = n.random.random(len(x_data))
Nmax = 500.
frac_plot = len(x_data)/Nmax

X,Y,Z=x_data[rds<frac_plot], y_data[rds<frac_plot], z_data[rds<frac_plot]
p.figure(1, (6., 5.))
p.scatter(X,Y, c=Z, s=2, marker='o', edgecolors='face', rasterized=True, cmap='tab20')

ok = (arnaud05_m500c>7e13)&(arnaud05_m500c<1e15)
p.plot( arnaud05_m500c[ok], arnaud05_KT[ok] , label='A05 z=0.1', ls='dashed', lw=1, color='k')

ok = (10**all_m500c>2e14)&(10**all_m500c<4e15)
p.plot( 10**all_m500c[ok], mantz16_kts[ok] , label='M16 z=0.35', ls='dotted', lw=2, color='k')

ok = (10**all_m500c>2e14)&(10**all_m500c<1.5e15)
p.plot( 10**all_m500c[ok], bulbul18_kts[ok], label='B18 z=0.45', ls='dashdot', lw=2, color='k')

p.colorbar(label='z')

p.xlabel(r'$M_{500c}\; [M_\odot]$')
p.ylabel(r'$kT / E(z)^{2/3}$ [keV]')
p.legend(frameon=False, loc=0)
p.xscale('log')
p.yscale('log')
#p.xlim((-14.2, -10.5))
#p.ylim((-3, 1.5))
#p.title(title_str)
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()


