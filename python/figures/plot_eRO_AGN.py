import sys
import os
import matplotlib.pyplot as p
import matplotlib
import numpy as n
import h5py
import time
print('Plots AGN data')
t0 = time.time()


matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})


laptop = True
laptop = False
if laptop:
    stilts_cmd = 'java -jar /home/comparat/software/stilts.jar'
else:
    stilts_cmd = 'stilts'

env = sys.argv[1]  # 'MD04'
ftyp = sys.argv[2]  # "sat"
print(env, ftyp)

test_dir = os.path.join(os.environ[env], 'hlists', 'fits')

path_2_ero_agn_file = os.path.join(test_dir, env + "_" + ftyp + "_eRO_AGN.fit")

fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'eRO_AGN', )
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)


fig_out = os.path.join(fig_dir, 'redshift_hist_' + env + "_" + ftyp + '.png')
cmd = stilts_cmd + """ plot2plane \
   xpix=629 ypix=613 \
   ylog=true xlabel='redshift_R / real space' ylabel= grid=true \
   legend=false \
   layer=Histogram \
      in=""" + path_2_ero_agn_file + """ \
      x=redshift_R \
      barform=steps thick=5 \
	  omode=out out=""" + fig_out
print(cmd)
os.system(cmd)


fig_out = os.path.join(fig_dir, 'redshift_vs_dec_' + env + "_" + ftyp + '.png')
cmd = stilts_cmd + """ plot2plane \
   xlabel='redshift_R / real space' \
    ylabel='dec / degree' \
   legend=false \
   layer=Mark \
      in=""" + path_2_ero_agn_file + """ \
      x=redshift_R y=dec \
      shading=auto \
	  omode=out out=""" + fig_out
print(cmd)
os.system(cmd)


fig_out = os.path.join(fig_dir, 'redshift_vs_ra_' + env + "_" + ftyp + '.png')
cmd = stilts_cmd + """ plot2plane \
   xlabel='redshift_R / real space' \
    ylabel='ra / degree' \
   legend=false \
   layer=Mark \
      in=""" + path_2_ero_agn_file + """ \
      x=redshift_R y=ra \
      shading=auto \
	  omode=out out=""" + fig_out
print(cmd)
os.system(cmd)

fig_out = os.path.join(fig_dir, 'dec_vs_ra_' + env + "_" + ftyp + '.png')
cmd = stilts_cmd + """ plot2sky \
   xpix=912 ypix=586 \
   projection=car fontsize=18 \
   auxmap=colour auxfunc=log auxmin=0.1 auxmax=42.8 \
   auxvisible=true auxlabel='density / 1/deg**2' \
   legend=false \
   layer=SkyDensity \
      in=""" + path_2_ero_agn_file + """ \
      lon=ra lat=dec \
      level=-4 combine=count-per-unit \
	  omode=out out=""" + fig_out

print(cmd)
os.system(cmd)
