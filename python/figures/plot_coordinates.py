import sys
import os
import matplotlib.pyplot as p
import matplotlib
import numpy as n
import h5py
import time
print('Plots coordinate data')
t0 = time.time()


matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})


env = sys.argv[1]  # 'MD04'
baseName = sys.argv[2]  # "sat_0.62840"
print(env, baseName)

test_dir = os.path.join(os.environ[env], 'hlists', 'fits')

path_2_light_cone = os.path.join(test_dir, baseName + '_coordinates.h5')

fig_dir = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    'figures',
    env,
    'coordinates',
)
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)


h5_file = os.path.join(path_2_light_cone)
f = h5py.File(h5_file, "r")
zr = f['/coordinates/redshift_R'].value
zs = f['/coordinates/redshift_S'].value

g_lat = f['/coordinates/g_lat'].value
g_lon = f['/coordinates/g_lon'].value

ecl_lat = f['/coordinates/ecl_lat'].value
ecl_lon = f['/coordinates/ecl_lon'].value

ra = f['/coordinates/ra'].value
dec = f['/coordinates/dec'].value

dL = f['/coordinates/dL'].value
NH = f['/coordinates/NH'].value
ebv = f['/coordinates/ebv'].value
f.close()

N_obj = len(zr)
rds = n.random.random(N_obj)
sel = (rds < 1e6 / N_obj)


fig_out = os.path.join(fig_dir, 'redshift_declination_' + baseName + '.png')

XX = zr[sel]
YY = dec[sel]

p.figure(0, (8., 5.5))
p.tight_layout()
p.plot(XX, YY, 'k,')
p.title(baseName)
p.xlabel('redshift')
p.ylabel('Dec.')
p.grid()
p.savefig(fig_out)
p.clf()

# redshift distributions

fig_out = os.path.join(fig_dir, 'zr-minus-zs_hist_' + baseName + '.png')

dz = zr - zs

X = n.log10(abs(dz[dz > 0]))

p.figure(1, (5.5, 5.5))
p.tight_layout()
p.hist(X[X > -6], histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('log10(|redshift R-redshiftS|)')
p.ylabel('Counts')
p.grid()
p.yscale('log')
#cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(E(B-V))$')
#ax = p.gca()
# ax.invert_xaxis()
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'zr_hist_' + baseName + '.png')

X = zr

p.figure(1, (5.5, 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('redshift R')
p.ylabel('Counts')
p.yscale('log')
p.grid()
#cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(E(B-V))$')
#ax = p.gca()
# ax.invert_xaxis()
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'zs_hist_' + baseName + '.png')

X = zs

p.figure(1, (5.5, 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('redshift S')
p.ylabel('Counts')
p.yscale('log')
p.grid()
#cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(E(B-V))$')
#ax = p.gca()
# ax.invert_xaxis()
p.savefig(fig_out)
p.clf()


fig_out = os.path.join(fig_dir, 'dL_hist_' + baseName + '.png')

X = n.log10(dL)

p.figure(1, (5.5, 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('dL [cm]')
p.ylabel('Counts')
p.yscale('log')
p.grid()
#cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(E(B-V))$')
#ax = p.gca()
# ax.invert_xaxis()
p.savefig(fig_out)
p.clf()

# ebv plot

fig_out = os.path.join(fig_dir, 'ra_dec_ebv_' + baseName + '.png')

XX = ra[sel]
YY = dec[sel]
cc = n.log10(ebv[sel])

p.figure(0, (8., 5.5))
p.tight_layout()
p.scatter(
    XX,
    YY,
    c=cc,
    s=3,
    marker='s',
    edgecolor='face',
    cmap='Paired',
    vmin=-3,
    vmax=2,
    rasterized=True)
p.title(baseName)
p.xlabel('R.A.')
p.ylabel('Dec.')
p.yscale('log')
p.grid()
cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(E(B-V))$')
ax = p.gca()
# ax.invert_xaxis()
# p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'g_lat_g_lon_ebv_' + baseName + '.png')

XX = g_lon[sel]
YY = g_lat[sel]
cc = n.log10(ebv[sel])

p.figure(0, (8., 5.5))
p.tight_layout()
p.scatter(
    XX,
    YY,
    c=cc,
    s=3,
    marker='s',
    edgecolor='face',
    cmap='Paired',
    vmin=-3,
    vmax=2,
    rasterized=True)
p.title(baseName)
p.xlabel('galactic lon.')
p.ylabel('galactic lat.')
p.grid()
cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(E(B-V))$')
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'ecl_lat_ecl_lon_ebv_' + baseName + '.png')

XX = ecl_lon[sel]
YY = ecl_lat[sel]
cc = n.log10(ebv[sel])

p.figure(0, (8., 5.5))
p.tight_layout()
p.scatter(
    XX,
    YY,
    c=cc,
    s=3,
    marker='s',
    edgecolor='face',
    cmap='Paired',
    vmin=-3,
    vmax=2,
    rasterized=True)
p.title(baseName)
p.xlabel('ecliptic lon.')
p.ylabel('ecliptic lat.')
p.grid()
cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(E(B-V))$')
p.savefig(fig_out)
p.clf()

# NH plot

fig_out = os.path.join(fig_dir, 'ra_dec_nh_' + baseName + '.png')

XX = ra[sel]
YY = dec[sel]
cc = n.log10(NH[sel])

p.figure(0, (8., 5.5))
p.tight_layout()
p.scatter(
    XX,
    YY,
    c=cc,
    s=3,
    marker='s',
    edgecolor='face',
    cmap='Paired',
    vmin=19.5,
    vmax=22,
    rasterized=True)
p.title(baseName)
p.xlabel('R.A.')
p.ylabel('Dec.')
p.grid()
cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(n_H/cm^{-2})$')
p.savefig(fig_out)
p.clf()


fig_out = os.path.join(fig_dir, 'g_lat_g_lon_nh_' + baseName + '.png')

XX = g_lon[sel]
YY = g_lat[sel]
cc = n.log10(NH[sel])

p.figure(0, (8., 5.5))
p.tight_layout()
p.scatter(
    XX,
    YY,
    c=cc,
    s=3,
    marker='s',
    edgecolor='face',
    cmap='Paired',
    vmin=19.5,
    vmax=22,
    rasterized=True)
p.title(baseName)
p.xlabel('galactic lon.')
p.ylabel('galactic lat.')
p.grid()
cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(n_H/cm^{-2})$')
p.savefig(fig_out)
p.clf()


fig_out = os.path.join(fig_dir, 'ecl_lat_ecl_lon_nh_' + baseName + '.png')

XX = ecl_lon[sel]
YY = ecl_lat[sel]
cc = n.log10(NH[sel])

p.figure(0, (8., 5.5))
p.tight_layout()
p.scatter(
    XX,
    YY,
    c=cc,
    s=3,
    marker='s',
    edgecolor='face',
    cmap='Paired',
    vmin=19.5,
    vmax=22,
    rasterized=True)
p.title(baseName)
p.xlabel('ecliptic lon.')
p.ylabel('ecliptic lat.')
p.grid()
cb = p.colorbar(shrink=0.7, label=r'$\log_{10}(n_H/cm^{-2})$')
p.savefig(fig_out)
p.clf()
