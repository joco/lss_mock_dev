"""

python plot_cluster_dz_z.py 

"""
import sys, os
import astropy.io.fits as fits
import astropy.units as u
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p
import numpy as n
import time
from astropy.table import Table, Column
from astropy.coordinates import SkyCoord
from sklearn.neighbors import BallTree

fig_dir = os.path.join(os.environ['HOME'], 'data/erosita/comparison-photoz')

N_MEM_MIN = 5
# eFEDs data

#d_a = fits.open('/home/comparat/data/erosita/eFEDS/redmapper/b0_Sc1Cat_V13c_ext_gt_0.fits')[1].data
#d_b = fits.open('/home/comparat/data/erosita/eFEDS/redmapper/decals_dr8_run_redmapper_v0.6.6_lgt05_catalog.fit')[1].data
#d_c = fits.open('/home/comparat/data/erosita/eFEDS/MCMF/eFEDS02_22_v940_valadd.fits')[1].data

#deg_to_rad = n.pi/180.
#coord_a = deg_to_rad * n.transpose([d_a['DEC'], d_a['RA']])
#coord_b = deg_to_rad * n.transpose([d_b['DEC'], d_b['RA']])
#coord_c = deg_to_rad * n.transpose([d_c['DEC'], d_c['RA']])

#Tree_a = BallTree(coord_a, metric='haversine')
#Tree_b = BallTree(coord_b, metric='haversine')
#Tree_c = BallTree(coord_c, metric='haversine')

#radius = 1./60. # 1 arc minute in degrees
#distance_b, index_b = Tree_b.query(coord_a, k=1, return_distance=True) 
#distance_c, index_c = Tree_c.query(coord_a, k=1, return_distance=True) 

#t_b = Table( d_b[ n.hstack(( index_b )) ] )
#t_c = Table( d_c[ n.hstack(( index_c )) ] )

#merged_table = Table(d_a)
#for el in t_b.columns.keys(): 
	#merged_table.add_column( Column( data = t_b[el], name = 'RM_'+el ) ) 

#merged_table.add_column( Column( data = distance_b * 60. / deg_to_rad, name = 'RM_distance_arcmin' ) )

#for el in t_c.columns.keys(): 
	#merged_table.add_column( Column( data = t_c[el], name = 'MC_'+el ) ) 

#merged_table.add_column( Column( data = distance_c * 60. / deg_to_rad, name = 'MC_distance_arcmin' ) )

#merged_table.write('/home/comparat/data/erosita/eFEDS/merged-X-RM-MCMF.fits', overwrite = True)

d1 = Table.read('/home/comparat/data/erosita/eFEDS/merged-X-RM-MCMF.fits')

eFEDs_spec_z_selection = ( d1['RM_spec_z_boot'] > 0.1 ) & ( d1['RM_spec_z_boot'] < 0.7 ) & ( d1['RM_distance_arcmin'] < 0.5 ) & ( d1['MC_distance_arcmin'] < 0.5 ) & ( d1['MC_Z_LFC'] > 0. ) & ( d1['RM_n_members'] > N_MEM_MIN )
eFEDs_zphot_mcmf       = d1['MC_Z_LFC'][eFEDs_spec_z_selection]
eFEDs_zphot_redmapper  = d1['RM_photo_z'][eFEDs_spec_z_selection]
eFEDs_zspec            = d1['RM_spec_z_boot'][eFEDs_spec_z_selection]


# eRASS data

#d_a = fits.open('/home/comparat/data/erosita/eRASS/all_e1_200602_poscorr_mpe_clean.fits')[1].data
##d_b = fits.open('/home/comparat/data/erosita/eRASS/redmapper/decals_dr8_0.05_0.72_v0.0_all_e1_200602_poscorr_mpe_clean/decals_dr8_run_redmapper_v0.6.6_lgt05_catalog.fit')[1].data
##d_b = fits.open('/home/comparat/data/erosita/eRASS/redmapper/ext_ge_0/decals_dr8_run_redmapper_v0.6.6_lgt05_vl02_catalog.fit')[1].data
#d_b = fits.open('/home/comparat/data/erosita/eRASS/redmapper/legacy_wise_dr8_0.05_0.90_v0.0_all_e1_200602_poscorr_mpe_clean/legacy_wise_dr8_0.05_0.90_v0.0_all_e1_200602_poscorr_mpe_clean/legacy_wise_dr8_run_redmapper_v0.6.6_lgt05_catalog.fit')[1].data
#d_c = fits.open('/home/comparat/data/erosita/eRASS/MCMF/eRASS1_ext_prelim_DECALS_version_4.fits')[1].data

#deg_to_rad = n.pi/180.
#coord_a = deg_to_rad * n.transpose([d_a['DEC'], d_a['RA']])
#coord_b = deg_to_rad * n.transpose([d_b['DEC'], d_b['RA']])
#coord_c = deg_to_rad * n.transpose([d_c['DEC'], d_c['RA']])

#Tree_a = BallTree(coord_a, metric='haversine')
#Tree_b = BallTree(coord_b, metric='haversine')
#Tree_c = BallTree(coord_c, metric='haversine')

#radius = 1./60. # 1 arc minute in degrees
#distance_b, index_b = Tree_b.query(coord_a, k=1, return_distance=True) 
#distance_c, index_c = Tree_c.query(coord_a, k=1, return_distance=True) 

#t_b = Table( d_b[ n.hstack(( index_b )) ] )
#t_c = Table( d_c[ n.hstack(( index_c )) ] )

#merged_table = Table(d_a)
#for el in t_b.columns.keys(): 
	#merged_table.add_column( Column( data = t_b[el], name = 'RM_'+el ) ) 

#merged_table.add_column( Column( data = distance_b * 60. / deg_to_rad, name = 'RM_distance_arcmin' ) )

#for el in t_c.columns.keys(): 
	#merged_table.add_column( Column( data = t_c[el], name = 'MC_'+el ) ) 

#merged_table.add_column( Column( data = distance_c * 60. / deg_to_rad, name = 'MC_distance_arcmin' ) )

#merged_table.write('/home/comparat/data/erosita/eRASS/merged-X-RM-MCMF-June26-wise.fits', overwrite = True)

d3 = Table.read('/home/comparat/data/erosita/eRASS/merged-X-RM-MCMF-June26-wise.fits')

eRASS_spec_z_selection = ( d3['RM_spec_z_boot'] > 0.1 ) & ( d3['RM_spec_z_boot'] < 0.7 ) & ( d3['RM_distance_arcmin'] < 0.5 ) & ( d3['MC_distance_arcmin'] < 0.5 ) & ( d3['MC_Z_LFC'] > 0. ) & ( d3['RM_n_members'] > N_MEM_MIN )
eRASS_zphot_mcmf       = d3['MC_Z_LFC'][eRASS_spec_z_selection]
eRASS_zphot_redmapper  = d3['RM_z_lambda'][eRASS_spec_z_selection]
eRASS_zspec            = d3['RM_spec_z_boot'][eRASS_spec_z_selection]

zphot_mcmf       = n.hstack(( eFEDs_zphot_mcmf      , eRASS_zphot_mcmf      )) 
zphot_redmapper  = n.hstack(( eFEDs_zphot_redmapper , eRASS_zphot_redmapper )) 
zspec            = n.hstack(( eFEDs_zspec           , eRASS_zspec           )) 

print('eROSITA has', len(zspec), 'clusters over eRASS + eFEDs')


# SPIDERS data
# SDSS
d4 = Table.read('/home/comparat/data/spiders/cluster/mastercatalogue_FINAL_CODEXID.fits')
sdss_zspec_selection = (d4['SCREEN_CLUZSPEC']>0.1) & (d4['SCREEN_CLUZSPEC']<0.6)  & (d4['SCREEN_NMEMBERS_W'] > N_MEM_MIN)
sdss_zphot_redmapper = d4['Z_LAMBDA']       [sdss_zspec_selection]
sdss_zspec           = d4['SCREEN_CLUZSPEC'][sdss_zspec_selection]

# DECALS
d5 = Table.read('/home/comparat/data/spiders/redmapper/decals_dr8_0.05_0.72_v0.1_spiders_dr16/decals_dr8_run_redmapper_v0.6.6_lgt05_catalog.fit')

decals_zspec_selection = ( d5['spec_z'] > 0.1 ) & ( d5['spec_z'] < 0.6 ) & ( d5['n_members'] > N_MEM_MIN )
decals_zphot_redmapper  = d5['photo_z'][decals_zspec_selection]
decals_zspec            = d5['spec_z'][decals_zspec_selection]

print('SDSS SPIDERS has', len(sdss_zspec), 'clusters over DR16')
print('DECALS SPIDERS has', len(decals_zspec), 'clusters over DR16')

# comparison MCMF redmapper

fig_out = os.path.join(fig_dir, 'dz-z.png')

p.figure(1, (6., 6.))
#
z_diff_rm = abs(zphot_redmapper-zspec)/(1+zspec)
sig_rm = 1.48 * n.median( z_diff_rm )
outlier_2 = ( z_diff_rm > 2 * sig_rm )
outlier = ( z_diff_rm > 3 * sig_rm )
frac_outlier_2 = len(z_diff_rm[outlier_2])*1./len(z_diff_rm)
frac_outlier = len(z_diff_rm[outlier])*1./len(z_diff_rm)
N_cata_rm01 = len( (z_diff_rm>0.02).nonzero()[0] )
N_cata_rm05 = len( (z_diff_rm>0.1).nonzero()[0] )
p.axhline(sig_rm, c='k', ls='dashed', label=r'RM $\sigma_{NMAD}$='+str(n.round(sig_rm,4)))
p.plot(zspec, z_diff_rm, 'kx', 
	   label=r'RM $f_{out}(0.02)=$'+str(n.round(100.*N_cata_rm01/len(z_diff_rm),1)) 
		 + r'$\%, f_{out}(0.1)=$'+str(n.round(100.*N_cata_rm05/len(z_diff_rm),1)) + r'$\%$')
#
z_diff_mcmf = abs(zphot_mcmf-zspec)/(1+zspec)
sig_mcmf = 1.48 *n.median( z_diff_mcmf )
outlier = ( z_diff_mcmf > 3 * sig_mcmf )
outlier_2 = ( z_diff_mcmf > 2 * sig_mcmf )
frac_outlier = len(z_diff_mcmf[outlier])*1./len(z_diff_mcmf)
frac_outlier_2 = len(z_diff_mcmf[outlier_2])*1./len(z_diff_mcmf)
N_cata_mcmf01 = len( (z_diff_mcmf>0.02).nonzero()[0] )
N_cata_mcmf05 = len( (z_diff_mcmf>0.1).nonzero()[0] )
p.axhline(sig_mcmf, c='b', ls='dashed', label=r'MC $\sigma_{NMAD}$='+str(n.round(sig_mcmf,4)))
p.plot(zspec, z_diff_mcmf, 'b+', 
	   label=r'MC $f_{out}(0.02)=$'+str(n.round(100.*N_cata_mcmf01/len(z_diff_mcmf),1)) 
		 + r'$\%, f_{out}(0.1)=$'+str(n.round(100.*N_cata_mcmf05/len(z_diff_mcmf),1)) + r'$\%$')
#
p.ylabel(r'$|z_{phot}-z_{spec}|/(1+z_{zspec})$')
p.xlabel(r'$z_{zspec}$ ')
p.legend(frameon=True, loc=4, fontsize=11)
#p.xscale('log')
p.yscale('log')
#p.xlim((-0.75, +0.75))
#p.ylim((1e-4, 0.5))
p.title(str(len(z_diff_rm))+' clusters in eFEDs + eRASS')
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'dz-hist.png')

bins = 10**n.arange(-5,0.5,0.25)
p.figure(1, (6., 6.))

z_diff_rm = abs(zphot_redmapper-zspec)/(1+zspec)
sig_rm = 1.48 * n.median( z_diff_rm )
outlier_2 = ( z_diff_rm > 2 * sig_rm )
outlier = ( z_diff_rm > 3 * sig_rm )
frac_outlier_2 = len(z_diff_rm[outlier_2])*1./len(z_diff_rm)
frac_outlier = len(z_diff_rm[outlier])*1./len(z_diff_rm)
p.hist(z_diff_rm, bins=bins, histtype='step', lw=5, label=r'RM')# f $>2\sigma_{NMAD}$='+str(n.round(frac_outlier_2*100,1))+r'$\%$ f $>3\sigma_{NMAD}$='+str(n.round(frac_outlier*100,1))+'%')
p.axvline(sig_rm, c='k', ls='dashed', label=r'RM $\sigma_{NMAD}$='+str(n.round(sig_rm,4)) )#+', N='+str(len(z_diff_rm)))

z_diff_mcmf = abs(zphot_mcmf-zspec)/(1+zspec)
sig_mcmf = 1.48 * n.median( z_diff_mcmf )
outlier = ( z_diff_mcmf > 3 * sig_mcmf )
outlier_2 = ( z_diff_mcmf > 2 * sig_mcmf )
frac_outlier = len(z_diff_mcmf[outlier])*1./len(z_diff_mcmf)
frac_outlier_2 = len(z_diff_mcmf[outlier_2])*1./len(z_diff_mcmf)
p.hist(z_diff_mcmf, bins=bins, histtype='step', lw=3, label=r'MCMF')# f $>2\sigma_{NMAD}$='+str(n.round(frac_outlier_2*100,1))+r'$\%$ f $>3\sigma_{NMAD}$='+str(n.round(frac_outlier*100,1))+'%')
p.axvline(sig_mcmf, c='b', ls='dashed', label=r'MCMF $\sigma_{NMAD}$='+str(n.round(sig_mcmf,4)) )#+', N='+str(len(z_diff_mcmf)))
p.xlabel(r'$|z_{i}-z_{spec}|/(1+z_{zspec})$')
p.ylabel(r'count')
p.legend(frameon=False, loc=2, fontsize=11)
p.xscale('log')
#p.yscale('log')
#p.xlim((-0.75, +0.75))
#p.xlim((1e-4, 0.5))
p.title(str(len(z_diff_rm))+' clusters in eFEDs + eRASS')
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()

# comparison internal to SPIDERS SDSS - DECALS

fig_out = os.path.join(fig_dir, 'spiders-dz-z.png')

p.figure(0, (6., 6.))

z_diff = abs(sdss_zphot_redmapper-sdss_zspec)/(1+sdss_zspec)
sig_sdss = 1.48 * n.median( z_diff )
outlier_2 = ( z_diff > 2 * sig_sdss )
outlier = ( z_diff > 3 * sig_sdss )
frac_outlier_2 = len(z_diff[outlier_2])*1./len(z_diff)
frac_outlier = len(z_diff[outlier])*1./len(z_diff)
p.plot(sdss_zspec, z_diff, 'k+', label=r'sdss f $>2\sigma_{NMAD}$='+str(n.round(frac_outlier_2*100,1))+r'$\%$ f $>3\sigma_{NMAD}$='+str(n.round(frac_outlier*100,1))+'%')
p.axhline(sig_sdss, c='k', ls='dashed', label=r'sdss $\sigma_{NMAD}$='+str(n.round(sig_sdss,4)))

z_diff = abs(decals_zphot_redmapper-decals_zspec)/(1+decals_zspec)
sig_sdss = 1.48 * n.median( z_diff )
outlier = ( z_diff > 3 * sig_sdss )
outlier_2 = ( z_diff > 2 * sig_sdss )
frac_outlier = len(z_diff[outlier])*1./len(z_diff)
frac_outlier_2 = len(z_diff[outlier_2])*1./len(z_diff)
p.plot(decals_zspec, z_diff, 'b+', label=r'decals f $>2\sigma_{NMAD}$='+str(n.round(frac_outlier_2*100,1))+r'$\%$ f $>3\sigma_{NMAD}$='+str(n.round(frac_outlier*100,1))+'%')
p.axhline(sig_sdss, c='b', ls='dashed', label=r'decals $\sigma_{NMAD}$='+str(n.round(sig_sdss,4)))

p.ylabel(r'$|z_{i}-z_{spec}|/(1+z_{zspec})$')
p.xlabel(r'$z_{zspec}$ ')
p.legend(frameon=True, loc=0, fontsize=11)
#p.xscale('log')
p.yscale('log')
#p.xlim((-0.75, +0.75))
p.ylim((1e-4, 0.5))
p.title('SPIDERS SDSS-DECALS')
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'spiders-dz-hist.png')

bins = 10**n.arange(-5,-1,0.1)
p.figure(1, (6., 6.))

z_diff = abs(sdss_zphot_redmapper-sdss_zspec)/(1+sdss_zspec)
sig_sdss = 1.48 * n.median( z_diff )
outlier = ( z_diff > 3 * sig_sdss )
outlier_2 = ( z_diff > 2 * sig_sdss )
frac_outlier = len(z_diff[outlier])*1./len(z_diff)
frac_outlier_2 = len(z_diff[outlier_2])*1./len(z_diff)
p.hist(z_diff, bins=bins, histtype='step', lw=2)#, label=r'sdss f $>2\sigma_{NMAD}$='+str(n.round(frac_outlier_2*100,1))+r'$\%$ f $>3\sigma_{NMAD}$='+str(n.round(frac_outlier*100,1))+'%')
p.axvline(sig_sdss, c='k', ls='dashed')#, label=r'sdss $\sigma_{NMAD}$='+str(n.round(sig_sdss,4))+', N='+str(len(z_diff)))

z_diff = abs(decals_zphot_redmapper-decals_zspec)/(1+decals_zspec)
sig_sdss = 1.48 * n.median( z_diff )
outlier = ( z_diff > 3 * sig_sdss )
outlier_2 = ( z_diff > 2 * sig_sdss )
frac_outlier = len(z_diff[outlier])*1./len(z_diff)
frac_outlier_2 = len(z_diff[outlier_2])*1./len(z_diff)
p.hist(z_diff, bins=bins, histtype='step', lw=2 )#, label=r'decals f $>2\sigma_{NMAD}$='+str(n.round(frac_outlier_2*100,1))+r'$\%$ f $>3\sigma_{NMAD}$='+str(n.round(frac_outlier*100,1))+'%')
p.axvline(sig_sdss, c='b', ls='dashed')#, label=r'decals $\sigma_{NMAD}$='+str(n.round(sig_sdss,4))+', N='+str(len(z_diff)))

p.xlabel(r'$|z_{i}-z_{spec}|/(1+z_{zspec})$')
p.ylabel(r'count')
#p.legend(frameon=True, loc=0, fontsize=11)
p.xscale('log')
#p.yscale('log')
#p.xlim((-0.75, +0.75))
p.xlim((1e-4, 0.5))
p.title('SPIDERS SDSS-DECALS')
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()

