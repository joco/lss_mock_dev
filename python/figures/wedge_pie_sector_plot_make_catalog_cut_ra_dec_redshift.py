"""
Creates a fits catalog with all redshifts shells concatenated.

python3 004_eRo_AGN_catalog.py
"""
import glob
import sys
import os
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
import matplotlib.pyplot as p
import matplotlib
from astropy_healpix import healpy
import astropy.io.fits as fits
import h5py
import numpy as n
import time
from typing import Union

print('CREATES FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')

t0 = time.time()


matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})

# import all pathes

env = sys.argv[1]  # 'MD04'
ftyp = sys.argv[2]  # 'all'

print(env)

ra_min = 170.
ra_max = 230.
dec_min = -5.
dec_max = 5.

output_dir_name = 'wedge'

laptop = False
#laptop = True
if laptop:
    stilts_cmd = 'java -jar /home/comparat/software/stilts.jar'
else:
    stilts_cmd = 'stilts'

# simulation setup

if env == "MD10" or env == "MD04":
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

test_dir = os.path.join(os.environ[env], 'hlists', 'fits')
output_dir = os.path.join(os.environ[env], 'hlists', 'fits', output_dir_name)
if os.path.isdir(output_dir) == False:
    os.system('mkdir -p ' + output_dir)


def get_pathes(baseName='all_1.00000'):
    pathes = {}
    path_2_light_cone = os.path.join(test_dir, baseName + '.fits')
    path_2_coordinate_file = os.path.join(
        test_dir, baseName + '_coordinates.h5')
    path_2_galaxy_file = os.path.join(test_dir, baseName + '_galaxy.h5')
    path_2_agn_file = os.path.join(test_dir, baseName + '_agn.h5')
    pathes['LC'] = path_2_light_cone
    pathes['coord'] = path_2_coordinate_file
    pathes['gal'] = path_2_galaxy_file
    pathes['agn'] = path_2_agn_file
    if baseName.split('_')[0] == 'all':
        path_2_clu_file = os.path.join(test_dir, baseName + '_CLU.h5')
        pathes['clu'] = path_2_clu_file
    return pathes


baseNames_i = sorted(
    n.array(
        glob.glob(
            os.path.join(
                test_dir,
                ftyp +
                '_*.fits'))))
baseNames = n.array([os.path.basename(bn)[:-5] for bn in baseNames_i])

pathes = {}
for bn in baseNames:
    pathes[bn] = get_pathes(bn)


def get_selection_ra_dec(
        path_dict,
        ra_min=ra_min,
        ra_max=ra_max,
        dec_min=dec_min,
        dec_max=dec_max):
    f2 = h5py.File(path_dict['coord'], 'r')
    ra = f2['/coordinates/ra'].value
    dec = f2['/coordinates/dec'].value
    f2.close()
    selection = (
        ra > ra_min) & (
        ra < ra_max) & (
            dec > dec_min) & (
                dec < dec_max)
    return n.arange(len(ra))[selection]


def get_ra_dec_z(path_dict, id_2_select):
    coordinates_i = {}
    f2 = h5py.File(path_dict['coord'], 'r')
    coordinates_i['ra'] = f2['/coordinates/ra'].value[id_2_select]
    coordinates_i['dec'] = f2['/coordinates/dec'].value[id_2_select]
    coordinates_i['redshift'] = f2['/coordinates/redshift_R'].value[id_2_select]
    f2.close()
    return coordinates_i


def get_mass_sfr(path_dict, id_2_select):
    galaxy_i = {}
    f2 = h5py.File(path_dict['gal'], 'r')
    galaxy_i['log_stellar_mass'] = f2['/galaxy/SMHMR_mass'].value[id_2_select]
    galaxy_i['log_star_formation_rate'] = f2['/galaxy/star_formation_rate'].value[id_2_select]
    galaxy_i['is_quiescent'] = f2['/galaxy/is_quiescent'].value[id_2_select]
    f2.close()
    return galaxy_i


def get_halo_properties(path_dict, id_2_select):
    halo_i = {}
    f1 = fits.open(path_dict['LC'])
    halo_i['M200c'] = f1[1].data['M200c'][id_2_select] / h
    halo_i['halo_id'] = f1[1].data['id'][id_2_select]
    halo_i['halo_x'] = f1[1].data['x'][id_2_select]
    halo_i['halo_y'] = f1[1].data['y'][id_2_select]
    halo_i['halo_z'] = f1[1].data['z'][id_2_select]
    f1.close()
    return halo_i


def get_agn(path_dict, id_2_select):
    agn_i = {}
    f3 = h5py.File(path_dict['agn'], 'r')
    # FX_hard              = f3["AGN/FX_hard"].value
    FX_soft_attenuated = f3["AGN/FX_soft_attenuated"].value
    ids_active = f3["AGN/ids_active"].value[FX_soft_attenuated > 1e-14]
    f3.close()
    agn_i['is_agn'] = n.zeros_like(id_2_select)
    agn_i['is_agn'][n.in1d(id_2_select, ids_active)] = 1
    return agn_i


coordinates = {}
galaxy = {}
halo = {}
ra_dec_selections = {}
agn = {}
for bn in baseNames[::-1]:
    try:
        path_2_output = os.path.join(output_dir, bn + '.ascii')
        print(bn)
        id_2_select = get_selection_ra_dec(pathes[bn])
        coordinates = get_ra_dec_z(pathes[bn], id_2_select)
        galaxy = get_mass_sfr(pathes[bn], id_2_select)
        halo = get_halo_properties(pathes[bn], id_2_select)
        agn = get_agn(pathes[bn], id_2_select)
        n.savetxt(path_2_output, n.transpose([
            coordinates['ra'],
            coordinates['dec'],
            coordinates['redshift'],
            galaxy['log_stellar_mass'],
            halo['M200c'],
            halo['halo_x'],
            halo['halo_y'],
            halo['halo_z'],
            agn['is_agn']
        ]), header=" ra dec redshift log_stellar_mass M200c x y z is_agn")
    except(OSError):
        print('OSError', bn)

path_2_summary = os.path.join(output_dir, ftyp + '_summary.fits')
c1 = 'ls ' + output_dir + '/' + ftyp + '*.ascii > ' + \
    output_dir + '/fit_list_' + env + "_" + ftyp + '.list'
print(c1)
os.system(c1)
c2 = stilts_cmd + """ tcat in=@""" + output_dir + """/fit_list_""" + env + \
    "_" + ftyp + """.list ifmt=ascii omode=out ofmt=fits out=""" + path_2_summary
print(c2)
os.system(c2)
