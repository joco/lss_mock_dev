import sys
import os
from scipy.interpolate import interp1d
from scipy.integrate import quad
import astropy.io.fits as fits
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
import numpy as n
import time
print('Plots clusters catalog')
t0 = time.time()
from scipy.special import erf
from astropy.table import Table, Column

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p

# simulation setup
cosmoMD = FlatLambdaCDM( H0 = 67.77 * u.km / u.s / u.Mpc, Om0 = 0.307115)
cosmo = cosmoMD

cosmoMG = FlatLambdaCDM(H0=70.4 * u.km / u.s / u.Mpc, Om0=0.272) 

zzs = n.hstack((0, 10**n.arange(-3,1,0.1) ))
EE = interp1d(zzs, cosmo.efunc(zzs))
#EE = lambda zz : 

dC = interp1d(zzs, cosmoMG.comoving_distance(zzs))

omega = lambda zz: cosmo.Om0*(1+zz)**3. / EE(zz)**2
DeltaVir_bn98 = lambda zz : (18.*n.pi**2. + 82.*(omega(zz)-1)- 39.*(omega(zz)-1)**2.)/omega(zz)

env = 'MD10'
test_dir = os.path.join(os.environ[env])

fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters')
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)

#path_2_magneticum = os.path.join(os.environ['HOME'], 'data', 'magneticum', 'all_cluster_members_magnetium_z0to1.3.fits')
#MG = Table.read(path_2_magneticum)
#r_gal = dC(MG['z_true'])
#r_cl = dC(MG['z_true_cl'])

#x_gal = r_gal * n.sin(MG['b']*n.pi/180.)*n.sin(MG['l']*n.pi/180.)
#y_gal = r_gal * n.sin(MG['b']*n.pi/180.)*n.cos(MG['l']*n.pi/180.)
#z_gal = r_gal * n.cos(MG['b']*n.pi/180.)

#x_cl = r_cl * n.sin(MG['b_cl']*n.pi/180.)*n.sin(MG['l_cl']*n.pi/180.)
#y_cl = r_cl * n.sin(MG['b_cl']*n.pi/180.)*n.cos(MG['l_cl']*n.pi/180.)
#z_cl = r_cl * n.cos(MG['b_cl']*n.pi/180.)

#d_to_cl_rvir = n.sqrt( (x_gal - x_cl)**2. + (y_gal - y_cl)**2. +  (z_gal - z_cl)**2. ) /(MG['Rvir_cl']/2.)

#mg_sf = (MG['sfr']>0)
#mg_qu = (MG['sfr']==0)


path_2_CLU_SAT_catalog = os.path.join(test_dir, 'MD10_eRO_CLU_b8_CM_0_pixS_20.0.fits')
clu = fits.open(path_2_CLU_SAT_catalog)
z_R = clu[1].data['redshift_R']
M200c = clu[1].data['HALO_M200c']
Mvir = clu[1].data['HALO_Mvir']
Rvir = clu[1].data['HALO_rvir']
R200c = (DeltaVir_bn98(z_R)/200. * M200c / Mvir)**(1./3.)*Rvir

m2e14 = (M200c>2e14)&(M200c<3e14)
print(n.mean(R200c[m2e14]))
Ni18_r200c = n.mean(R200c[m2e14]) /1000. # kpc

m5e14 = (M200c>5e14)&(M200c<6e14)
print(n.mean(R200c[m5e14]))
He17_r200c = n.mean(R200c[m5e14]) /1000. # kpc

path_2_CLU_SAT_catalog = os.path.join(test_dir, 'MD10_eRO_CLU_galaxies_b8_CM_0_pixS_20.0.fit')
hdu = fits.open(path_2_CLU_SAT_catalog)

dx2 = (hdu[1].data['x']-hdu[1].data['HOST_HALO_x'])**2
dy2 = (hdu[1].data['y']-hdu[1].data['HOST_HALO_y'])**2
dz2 = (hdu[1].data['z']-hdu[1].data['HOST_HALO_z'])**2
rr = (dx2 + dy2 + dz2)**0.5
r_2_c = rr * 1000. / hdu[1].data['HOST_HALO_Rvir']

is_QU = hdu[1].data['is_quiescent']
#z_R = hdu[1].data['redshift_R']
#M200c = hdu[1].data['HALO_M200c']
#Mvir = hdu[1].data['HALO_Mvir']
#Rvir = hdu[1].data['HALO_rvir']
#R200c = (DeltaVir_bn98(z_R)/200. * M200c / Mvir)**(1./3.)*Rvir
#r_2_c = r_2_c_i * Rvir / R200c

#def frac_old(x, z_cluster): return ((x + 0.01)**(-0.25) - x / 100. - 0.47) * (1. + z_cluster)**2. / 3.2

data_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'Hennig_17')
dat = n.loadtxt(os.path.join(data_dir, 'all_profile_038.txt'), unpack=True)
itp_all = interp1d(dat[0], dat[1])

dat = n.loadtxt(os.path.join(data_dir, 'RS_profile_038.txt'), unpack=True)
itp_RS = interp1d(dat[0], dat[1])

dat = n.loadtxt(os.path.join(data_dir, 'SF_profile_038.txt'), unpack=True)
itp_SF = interp1d(dat[0], dat[1])

common_x_038 = 10**n.arange(n.log10(n.max([n.min(itp_RS.x), n.min(itp_all.x)])),
                        n.log10(n.min([n.max(itp_RS.x), n.max(itp_all.x)])), 0.1)
Hennig_red_038 = itp_RS(common_x_038) / itp_all(common_x_038)
Hennig_blu_038 = itp_SF(common_x_038) / itp_all(common_x_038)


data_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'Hennig_17')
dat = n.loadtxt(os.path.join(data_dir, 'all_profile_015.txt'), unpack=True)
itp_all = interp1d(dat[0], dat[1])

dat = n.loadtxt(os.path.join(data_dir, 'RS_profile_015.txt'), unpack=True)
itp_RS = interp1d(dat[0], dat[1])

dat = n.loadtxt(os.path.join(data_dir, 'SF_profile_015.txt'), unpack=True)
itp_SF = interp1d(dat[0], dat[1])

common_x_015 = 10**n.arange(n.log10(n.max([n.min(itp_RS.x), n.min(itp_all.x)])),
                        n.log10(n.min([n.max(itp_RS.x), n.max(itp_all.x)])), 0.1)
Hennig_red_015 = itp_RS(common_x_015) / itp_all(common_x_015)
Hennig_blu_015 = itp_SF(common_x_015) / itp_all(common_x_015)

#nfw_func = lambda x, conc : 1./( conc*x * (1.+conc*x)**2. )
# Table 3, redshift 0.38
#c_all = 2.52
#c_red = 4.48
#c_blu = 0.79
#Hennig_red = lambda x : nfw_func(x, c_red) / nfw_func(x, c_all)
#Hennig_blu = lambda x : nfw_func(x, c_blu) / nfw_func(x, c_all)

data_dir2 = os.path.join(os.environ['GIT_AGN_MOCK'], 'data/Nishizawa_2018')
data_Ni18_018 = n.loadtxt(
    os.path.join(
        data_dir2,
        'red_fraction_Fig8_z_0.18.txt'),
    unpack=True)
data_Ni18_032 = n.loadtxt(
    os.path.join(
        data_dir2,
        'red_fraction_Fig8_z_0.32.txt'),
    unpack=True)
data_Ni18_055 = n.loadtxt(
    os.path.join(
        data_dir2,
        'red_fraction_Fig8_z_0.55.txt'),
    unpack=True)
data_Ni18_085 = n.loadtxt(
    os.path.join(
        data_dir2,
        'red_fraction_Fig8_z_0.85.txt'),
    unpack=True)


path_2_murata2020 = os.path.join(os.environ['GIT_AGN_MOCK'], 'data/Murata_2020', 'fig5_rho_01-z-04.txt')
Radius_M20, red_up_M20, red_low_M20, blue_up_M20, blue_low_M20 = n.loadtxt(path_2_murata2020, unpack=True)

red_mid_M20 = (red_up_M20+ red_low_M20)/2. 
blue_mid_M20 = (blue_up_M20+ blue_low_M20)/2.

R_S09, redSF_S09, redQU_S09, blueQU_S09 = n.loadtxt(os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'Smriti_09', 'smriti_09.txt'), unpack=True)

# MD04
bins = n.hstack((0., 10**n.arange(-2, 1, 0.01)))
all_QU = n.histogram(r_2_c[is_QU], bins=bins)[0]
all_GAL = n.histogram(r_2_c, bins=bins)[0]


fig_out = os.path.join(fig_dir, 'frac_QU_radius.png')

log_xx = n.arange(-3,3,0.1)
def frac_RS(log_xxx, z_cluster): return (erf((-log_xxx+0.1)/0.6)+0.9)*0.38 * (1+z_cluster)**(-0.65)+0.22 


p.figure(1, (6., 6.))

#p.plot(R_S09, redSF_S09 + redQU_S09 + blueQU_S09, label='S09', lw=2)

#p.fill_between(
	#Radius_M20*0.7**(-1),
	#y2=red_up_M20/(red_mid_M20+blue_mid_M20),
	#y1=red_low_M20/(red_mid_M20+blue_mid_M20),
	#label='Mu20, z=0.25', alpha=0.2)

p.fill_between(
	data_Ni18_018[0]*0.7**(-1)/Ni18_r200c,
	y2=data_Ni18_018[2],
	y1=data_Ni18_018[1],
	label='Ni18, z=0.18', alpha=0.2)
p.fill_between(
	data_Ni18_032[0]*0.7**(-1)/Ni18_r200c,
	y2=data_Ni18_032[2],
	y1=data_Ni18_032[1],
	label='Ni18, z=0.32', alpha=0.2)
p.fill_between(
	data_Ni18_055[0]*0.7**(-1)/Ni18_r200c,
	y2=data_Ni18_055[2],
	y1=data_Ni18_055[1],
	label='Ni18, z=0.55', alpha=0.2)
p.fill_between(
	data_Ni18_085[0]*0.7**(-1)/Ni18_r200c,
	y2=data_Ni18_085[2],
	y1=data_Ni18_085[1],
	label='Ni18, z=0.85', alpha=0.2)

#p.plot(common_x_038*He17_r200c, Hennig_red_038, ls='dashed', label='He17 RS z=0.38')
#p.plot(common_x_015*He17_r200c, Hennig_red_015, ls='dashed', label='He17 RS z=0.15')

#p.plot(0.5 * (bins[:-1] + bins[1:]), all_QU * 1. / all_GAL, label=env)

yy = frac_RS(log_xx, n.ones_like(log_xx)*0.18)
itp = interp1d(10**log_xx, yy)
mean_frac = quad(itp, 0.01,1)[0]
p.plot(10**log_xx, yy, ls='dashed', lw=3, label='Model z=0.18 ')#+str(n.round(mean_frac,2)))

yy = frac_RS(log_xx, n.ones_like(log_xx)*0.32)
itp = interp1d(10**log_xx, yy)
mean_frac = quad(itp, 0.01,1)[0]
p.plot(10**log_xx, yy, ls='dashed', lw=3, label='Model z=0.32 ')#+str(n.round(mean_frac,2)))

yy = frac_RS(log_xx, n.ones_like(log_xx)*0.55)
itp = interp1d(10**log_xx, yy)
mean_frac = quad(itp, 0.01,1)[0]
p.plot(10**log_xx, yy, ls='dashed', lw=3, label='Model z=0.55 ')#+str(n.round(mean_frac,2)))

yy = frac_RS(log_xx, n.ones_like(log_xx)*0.85)
itp = interp1d(10**log_xx, yy)
mean_frac = quad(itp, 0.01,1)[0]
p.plot(10**log_xx, yy, ls='dashed', lw=3, label='Model z=0.85 ')#+str(n.round(mean_frac,2)))

p.legend(frameon=False, loc=3)
p.xlabel(r'R / R$_{200c}$')
p.ylabel('red fraction')
p.grid()
p.ylim((0, 1.1))
p.xlim((0.09, 5))
# p.yscale('log')
p.xscale('log')
p.savefig(fig_out)
p.clf()

