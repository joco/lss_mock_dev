
from scipy.interpolate import interp1d
from astropy.cosmology import FlatLambdaCDM, z_at_value
import astropy.units as u
from matplotlib.patches import Polygon
from astropy import constants
import astropy.io.fits as fits
from matplotlib import gridspec
import subprocess
import numpy as np
import matplotlib.pyplot as plt
import os
import sys

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
#
cosmo = FlatLambdaCDM(H0=70, Om0=0.3, Ob0=0.05)


# DOSDSS = 1#int(sys.argv[1]) # 0 or 1
# DOBOSS = 1#int(sys.argv[2]) # 0 or 1
# DOEBOSS= 1#int(sys.argv[3]) # 0 or 1
# DOLARGE= 1#int(sys.argv[4]) # 0 or 1 -> large font or not

env = sys.argv[1]
md04_all = fits.open(
    os.path.join(
        os.environ[env],
        'hlists',
        'fits',
        'wedge',
        'all_summary.fits'))[1].data
md04_sat = fits.open(
    os.path.join(
        os.environ[env],
        'hlists',
        'fits',
        'wedge',
        'sat_summary.fits'))[1].data

if env == "MD04":
    zmax = 0.28
    height_max = 20
if env == "MD10":
    zmax = 0.9
    height_max = 20


def pol2cart(r, t):
    x = r * np.cos(t - tref)
    y = r * np.sin(t - tref)
    return(x, y)


def create_poly(rr, tt, nn):
    # rr = np.array(rmin,rmax)
    # tt = np.array(tmin,tmax)
    # nn = number of points per side
    tmpr = np.zeros(4 * nn + 1)
    tmpr[0] = rr[0]
    tmpr[1:1 + nn] = np.linspace(rr[0], rr[1], nn)
    tmpr[1 + 1 * nn:1 + 2 * nn] = rr[1] + np.zeros(nn)
    tmpr[1 + 2 * nn:1 + 3 * nn] = np.linspace(rr[1], rr[0], nn)
    tmpr[1 + 3 * nn:1 + 4 * nn] = rr[0] + np.zeros(nn)
    tmpt = np.zeros(4 * nn + 1)
    tmpt[0] = tt[0]
    tmpt[1:1 + nn] = tt[0] + np.zeros(nn)
    tmpt[1 + 1 * nn:1 + 2 * nn] = np.linspace(tt[0], tt[1], nn)
    tmpt[1 + 2 * nn:1 + 3 * nn] = tt[1] + np.zeros(nn)
    tmpt[1 + 3 * nn:1 + 4 * nn] = np.linspace(tt[1], tt[0], nn)
    return tmpr, tmpt


def create_blank(ax, lab):
    tmpr, tmpt = create_poly([1e4, 1e5], [0, 0], 2)
    a = pol2cart(tmpr, tmpt)
    ax.plot(a[0], a[1], c='none', linewidth=0, zorder=0, label=lab)
    return True


def do_frame(ax, zmin, zmax, rlim, tlim, ylim,
             tticks, tlabel, tlinew,
             rticks, rlabel, agelabel, rlinew,
             tmpnt, tmpdy):
    tmpz = np.linspace(zmin, zmax, tmpnt)
    # R.A.
    for t, tlab, tlw in zip(tticks, tlabel, tlinew):
        tmpr = np.linspace(rlim[0], rlim[1], tmpnt)
        tmpt = t + np.zeros(tmpnt)
        a = pol2cart(tmpr, tmpt)
        ax.plot(a[0], a[1], linewidth=tlw, color='k', zorder=20)
        if (t == tref):
            ax.text(a[0][-1] + 0.0 * tmpdy, a[1][-1], tlab,
                    color='k', va='center', fontsize=fsticklab)
        else:
            ax.text(a[0][-1] + 0.3 * tmpdy, a[1][-1], tlab,
                    color='k', va='center', fontsize=fsticklab)
    # z and age
    # labels
    r = np.mean(rlim)
    t = tlim[1]
    a = pol2cart(r, t)
    ax.text(
        a[0] - 2 * tmpdy,
        a[1] + 2 * tmpdy,
        'Redshift',
        ha='center',
        va='center',
        fontsize=fslab)  # ,rotation=np.degrees(tlim[1]))
    t = tlim[0]
    a = pol2cart(r, t)
    ax.text(
        a[0] - 2 * tmpdy,
        a[1] - 2.5 * tmpdy,
        'Lookback time\n [Gyr]',
        ha='center',
        va='center',
        fontsize=fslab)  # ,rotation=np.degrees(tlim[0]))
    # grid
    for r, rlab, alab, rlw in zip(rticks, rlabel, agelabel, rlinew):
        # r=0
        if (r == 0):
            a = pol2cart(0, 0)
            ax.text(a[0], a[1] + tmpdy, rlab, color='k',
                    ha='center', va='center', fontsize=fsticklab)
            ax.text(a[0], a[1] - tmpdy, alab, color='k',
                    ha='center', va='center', fontsize=fsticklab)
        else:
            tmpr = np.zeros(tmpnt) + r
            tmpt = np.linspace(tlim[0], tlim[1], tmpnt)
            a = pol2cart(tmpr, tmpt)
            ax.plot(a[0], a[1], linewidth=rlw, color='k')
            a = pol2cart(r, tlim[1])
            ax.text(a[0], a[1] + tmpdy, rlab, color='k',
                    ha='center', va='center', fontsize=fsticklab,
                    zorder=20)
            a = pol2cart(r, tlim[0])
            ax.text(a[0], a[1] - tmpdy, alab, color='k',
                    ha='center', va='center', fontsize=fsticklab,
                    zorder=20)
    return True


# outpng
outroot = os.getenv('GIT_AGN_MOCK') + '/figures'
"""
if (DOSDSS==1):
	outroot += '.SDSS'
if (DOBOSS==1):
	outroot += '.BOSS'
if (DOEBOSS==1):
	outroot += '.eBOSS'
if (DOLARGE==1):
	outroot += '.pressrelease.largefont'
else:
	outroot += '.pressrelease'
"""


# settings
tlim = np.radians([170, 230])
tticks = np.radians([170, 180, 190, 200, 210, 220, 230])
tlinew = tticks * 0 + 0.1  # np.array([2,0.1,0.1,0.1,0.1,0.1,2.0])
tlinew[0] = 2
tlinew[-1] = 2
tref = np.mean(tlim)
tcoeff = 1.
tplate = tref + tcoeff * \
    np.radians(1.49) * np.array([-1, 1])  # SDSS plate size
zmin = 0
#zmax   = 0.28
itp = interp1d(
    np.arange(
        0., 8, 0.01), (cosmo.kpc_comoving_per_arcmin(
            np.arange(
                0., 8, 0.01)).to(
                    u.Mpc / u.deg)).value)

rlim = np.log10(1 + np.array([zmin, zmax]))
tlabel = np.array([str(np.degrees(x).astype('int')) +
                   r'$^\circ$' for x in tticks], dtype='object')
tmp = (tticks == 0)
tlabel[tmp] = 'RA=' + tlabel[tmp]
# redshift ticks and labels
zticks = np.arange(0, zmax, (zmax - zmin) / 10.)
#zticks = np.array([0,0.5,1,1.5,2,2.5,3,3.5])
rticks = np.log10(1 + zticks)
rlabel = np.array([str(np.round(z, 2)) for z in zticks], dtype='object')
rlinew = rticks * 0 + 0.1
rlinew[-1] = 2
# Universe age at zticks
ageticks = cosmo.age(0).value - cosmo.age(zticks).value  # in Gyr
agelabel = np.array(['%.1f' % age for age in ageticks])
tmpnt = 1000
# fontsize
# if (DOLARGE==0):
fstxt = 14
fslab = 14
fsticklab = 14
fsleg = 14
#fstxt     = 17
#fslab     = 25
#fsticklab = 22
#fsleg     = 17

# polar plot with data
# building the frame
#fig     = plt.figure(figsize=(10,20))
#gs      = gridspec.GridSpec(2,1,hspace=0.6,height_ratios=[1,0.2])

fig, ax = plt.subplots(figsize=(7.5, 10))
xlim = rlim
ylim = np.zeros(2)
ylim[0] = rlim[1] * np.sin(1.0 * (tlim[0] - tref))
ylim[1] = rlim[1] * np.sin(1.0 * (tlim[1] - tref))
ax.set_xlim(xlim)
ax.set_ylim(ylim)
ax.grid(True)
ax.axis('off')
# polar grid  & axis labelling
x = do_frame(ax, zmin, zmax, rlim, tlim, ylim,
             tticks, tlabel, tlinew,
             rticks, rlabel, agelabel, rlinew,
             tmpnt, 0.05 * (ylim[1] - ylim[0]))


# data
# all satellite galaxies
gal_sat = (abs(itp(md04_sat['redshift']) * md04_sat['dec'])
           < height_max) & (md04_sat['redshift'] < zmax)
r = np.log10(1 + md04_sat['redshift'][gal_sat])
#alpha = 0.1/(1+100*md04_sat['redshift'][gal_sat])
tmpra = md04_sat['ra'][gal_sat]
print(len(tmpra))
t = np.radians(tmpra)
a = pol2cart(r, t)
ax.scatter(
    a[0],
    a[1],
    edgecolor='none',
    alpha=0.1,
    color='grey',
    s=0.5,
    marker=',',
    rasterized=True)

# all distinct galaxies
gal_all = (abs(itp(md04_all['redshift']) * md04_all['dec'])
           < height_max) & (md04_all['redshift'] < zmax)
r = np.log10(1 + md04_all['redshift'][gal_all])
#alpha = 0.1/(1+3*md04_all['redshift'][gal])
tmpra = md04_all['ra'][gal_all]
print(len(tmpra))
t = np.radians(tmpra)
a = pol2cart(r, t)
ax.scatter(
    a[0],
    a[1],
    edgecolor='none',
    alpha=0.1,
    color='grey',
    s=0.5,
    marker=',',
    rasterized=True)


# MD04 sat AGN
rds = np.random.random(len(md04_sat['is_agn']))
agn_sat = (md04_sat['is_agn'] == 1) & (rds > 0.9) & (gal_sat)
print(len(md04_sat['redshift'][agn_sat]))
r = np.log10(1 + md04_sat['redshift'][agn_sat])
tmpra = md04_sat['ra'][agn_sat]
t = np.radians(tmpra)
a = pol2cart(r, t)
ax.scatter(
    a[0],
    a[1],
    edgecolor='none',
    alpha=1,
    color='b',
    s=3,
    marker='o',
    rasterized=True)


# MD04 all AGN
rds = np.random.random(len(md04_all['is_agn']))
agn_all = (md04_all['is_agn'] == 1) & (rds < 0.9) & (gal_all)
print(len(md04_all['redshift'][agn_all]))
r = np.log10(1 + md04_all['redshift'][agn_all])
tmpra = md04_all['ra'][agn_all]
t = np.radians(tmpra)
a = pol2cart(r, t)
ax.scatter(
    a[0],
    a[1],
    edgecolor='none',
    alpha=1,
    color='b',
    s=3,
    marker='o',
    rasterized=True)

# MD04 all CLUSTERS
cluster = (md04_all['M200c'] > 2e14) & (gal_all)
print(len(md04_all['redshift'][cluster]))
s = md04_all['M200c'][cluster] / 1e13
r = np.log10(1 + md04_all['redshift'][cluster])
tmpra = md04_all['ra'][cluster]
t = np.radians(tmpra)
a = pol2cart(r, t)
ax.scatter(
    a[0],
    a[1],
    edgecolor='none',
    alpha=0.5,
    color='r',
    s=s,
    marker='o',
    rasterized=True)

plt.savefig(os.path.join(outroot , 'LSS_'+env+'_zmax_' + str(zmax) + '_sector.png'), bbox_tight=True)
plt.clf()

sys.exit()
# radec plot
'''fig, ax = plt.subplots(figsize=(10,10))
ax.set_xlim([-50,50])
ax.set_ylim([-7,7])
ax.set_xlabel('R.A. [deg.]',fontsize=20)
ax.set_ylabel('DEC. [deg.]',fontsize=20)
ax.tick_params(axis='both', which='major', labelsize=20)
ax.grid(True)
ax.set_title('Fat Stripe 82')
## FS82 footprint
footprint_ra  = np.array([-43.5,0,0,45,45,0,0,-43.5,-43.5])
footprint_dec = np.array([-2.0,-2.0,-5,-5,5,5,2.0,2.0,-2.0])
ax.plot(footprint_ra,footprint_dec,c='k',zorder=10,linewidth=3)
##
for i in xrange(len(tracer)):
	if (survplot[i]==1):
		exec ('ax.scatter('+tracer[i]+'ra,'+tracer[i]+'dec,'+
				'c=traccol[i],edgecolor="none",s=1,alpha=tracalph[i],zorder=traczord[i])')
##
plt.savefig(outroot+'.radec.png',bbox_tight=True)
plt.close()
'''
"""

# polar plot - all R.A.s
#fig = plt.figure(figsize=(8,8))
#ax  = fig.add_axes(polar=True)
ax = plt.subplot(111, projection='polar')
ax.set_rlim(rlim)
ax.set_rgrids(rticks[1:], angle=90)
for r,l in zip(rticks[1:],agelabel[1:]):
	ax.text(-np.pi/2.,r,' '+l,fontsize=12)
ax.yaxis.set_ticklabels(' '+rlabel[1:])
ax.text(0.47,0.8,'Redshift',va='center',ha='center',rotation=90,fontsize=12,transform=ax.transAxes)

# data
for i in xrange(len(tracer)):
	if (survplot[i]==1):
		exec 'r = np.log10(1+'+tracer[i]+'z)'
		exec 'tmpra = '+tracer[i]+'ra'
		t = np.radians(tmpra)
		ax.scatter(t,r,c=traccol[i],
			edgecolor='none',s=tracsize[i],
			alpha=tracalph[i],zorder=traczord[i])
plt.savefig(outroot+'.circle.png',bbox_tight=True)
plt.close()


# radec plot
fig, ax = plt.subplots(figsize=(10,10))
ax.set_xlim([-50,50])
ax.set_ylim([-7,7])
ax.set_xlabel('R.A. [deg.]',fontsize=20)
ax.set_ylabel('DEC. [deg.]',fontsize=20)
ax.tick_params(axis='both', which='major', labelsize=20)
ax.grid(True)
ax.set_title('Fat Stripe 82')
## FS82 footprint
footprint_ra  = np.array([-43.5,0,0,45,45,0,0,-43.5,-43.5])
footprint_dec = np.array([-2.0,-2.0,-5,-5,5,5,2.0,2.0,-2.0])
ax.plot(footprint_ra,footprint_dec,c='k',zorder=10,linewidth=3)
##
for i in xrange(len(tracer)):
	if (survplot[i]==1):
		exec ('ax.scatter('+tracer[i]+'ra,'+tracer[i]+'dec,'+
				'c=traccol[i],edgecolor="none",s=1,alpha=tracalph[i],zorder=traczord[i])')
##
plt.savefig(outroot+'.radec.png',bbox_tight=True)
plt.close()

"""
