import sys
import os
import glob
import numpy as np
import astropy.io.fits as fits
import time
print('Plots Nbody data')
t0 = time.time()

env = sys.argv[1]


def exec_cmd(env='MD10', mini_fits=True):
	stilts_cmd = 'stilts'
	if mini_fits:
		test_dir = os.path.join(os.environ[env], 'mini_fits')
		fig_dir = os.path.join(
			os.environ['GIT_AGN_MOCK'],
			'figures',
			env,
			'nbody_mini_fits',
		)
	else:
		test_dir = os.path.join(os.environ[env], 'fits')
		fig_dir = os.path.join(
			os.environ['GIT_AGN_MOCK'],
			'figures',
			env,
			'nbody',
		)

	if os.path.isdir(fig_dir) == False:
		os.system('mkdir -p ' + fig_dir)

	all_files = sorted(
		np.array(
			glob.glob(
				os.path.join(
					test_dir,
					'all_?.?????.fits'))))

	cmd = stilts_cmd + """ plot2plane xlabel=x ylabel=y xpix=1200 ypix=1200 fontsize=18 legend=true x=x y=y shading=auto """
	end_cmd = """  omode=out out=""" + \
		os.path.join(fig_dir, 'light_cone_xy_slice_all.png')

	def layer(ii, all_files): return """ layer_""" + str(ii + 1) + """=Mark  in_""" + str(ii + 1) + """=""" + \
		all_files[ii] + """ leglabel_""" + str(ii + 1) + """='""" + os.path.basename(all_files[ii])[4:-5] + """' """

	command = cmd
	for ii in range(len(all_files)):
		command += layer(ii, all_files)
		# print(command)

	command += end_cmd
	print("____________________________________________________________")
	print("____________________________________________________________")
	print("____________________________________________________________")
	print(command)
	print("____________________________________________________________")
	print("____________________________________________________________")
	print("____________________________________________________________")

	os.system(command)

	#all_files = np.array(glob.glob(os.path.join(test_dir, 'sat_?.?????.fits')))
	#all_files.sort()

	#cmd = stilts_cmd + """ plot2plane xlabel=x ylabel=y xpix=1200 ypix=1200 fontsize=18 legend=true x=x y=y shading=auto """
	#end_cmd = """  omode=out out=""" + \
		#os.path.join(fig_dir, 'light_cone_xy_slice_sat.png')

	#def layer(ii, all_files): return """ layer_""" + str(ii + 1) + """=Mark  in_""" + str(ii + 1) + """=""" + \
		#all_files[ii] + """ leglabel_""" + str(ii + 1) + """='""" + os.path.basename(all_files[ii])[4:-5] + """' """

	#command = cmd
	#for ii in range(len(all_files)):
		#command += layer(ii, all_files)
		## print(command)

	#command += end_cmd
	#print("____________________________________________________________")
	#print("____________________________________________________________")
	#print("____________________________________________________________")
	#print(command)
	#print("____________________________________________________________")
	#print("____________________________________________________________")
	#print("____________________________________________________________")

	#os.system(command)


exec_cmd(env, mini_fits=True)

# = 'MD04'
# = 'MD10'
# = 'UNIT_fA1_DIR'
# = 'UNIT_fA2_DIR'
# = 'UNIT_fA1i_DIR'
# = 'UNIT_fA2i_DIR'
