"""
Plots the stellar masses obtained
"""
print('PLOT SMF for all galaxies')
print('------------------------------------------------')
print('------------------------------------------------')
#from lib_model_agn import *
import os
import glob
import numpy as n
from scipy.interpolate import interp1d
import sys
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
cosmoMD = FlatLambdaCDM(H0=67.77*u.km/u.s/u.Mpc, Om0=0.307115)#, Ob0=0.048206)
import astropy.io.fits as fits

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p

env =  sys.argv[1]  # 'MD04'
baseName =  sys.argv[2]  # "sat_0.62840"
print(env, baseName)

plotDir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'galaxy')

ll_dir = os.path.join(os.environ['GIT_VS'], 'data/LF_SMF')

L15_ngal,   L15_M,     L15_phi,    L15_Err = n.loadtxt(os.path.join(ll_dir, 'loveday_2015/smfs.txt'), unpack=True)

smf_ilbert13 = lambda M, M_star, phi_1s, alpha_1s, phi_2s, alpha_2s : ( phi_1s * (M/M_star) ** alpha_1s + phi_2s * (M/M_star) ** alpha_2s ) * n.e ** (-M/M_star) * (M/ M_star)
path_ilbert13_SMF = os.path.join(ll_dir, "ilbert_2013_mass_function_params.txt")
zmin, zmax, N, M_comp, M_star, phi_1s, alpha_1s, phi_2s, alpha_2s, log_rho_s = n.loadtxt(path_ilbert13_SMF, unpack=True)

smf_ilbert_fun = n.array([
lambda mass : smf_ilbert13( mass , 10**M_star[0], phi_1s[0]*10**(-3), alpha_1s[0], phi_2s[0]*10**(-3), alpha_2s[0] )
, lambda mass : smf_ilbert13( mass , 10**M_star[1], phi_1s[1]*10**(-3), alpha_1s[1], phi_2s[1]*10**(-3), alpha_2s[1] )
, lambda mass : smf_ilbert13( mass , 10**M_star[2], phi_1s[2]*10**(-3), alpha_1s[2], phi_2s[2]*10**(-3), alpha_2s[2] )
, lambda mass : smf_ilbert13( mass , 10**M_star[3], phi_1s[3]*10**(-3), alpha_1s[3], phi_2s[3]*10**(-3), alpha_2s[3] )
, lambda mass : smf_ilbert13( mass , 10**M_star[4], phi_1s[4]*10**(-3), alpha_1s[4], phi_2s[4]*10**(-3), alpha_2s[4] )
, lambda mass : smf_ilbert13( mass , 10**M_star[5], phi_1s[5]*10**(-3), alpha_1s[5], phi_2s[5]*10**(-3), alpha_2s[5] )
, lambda mass : smf_ilbert13( mass , 10**M_star[6], phi_1s[6]*10**(-3), alpha_1s[6], phi_2s[6]*10**(-3), alpha_2s[6] )
, lambda mass : smf_ilbert13( mass , 10**M_star[7], phi_1s[7]*10**(-3), alpha_1s[7], phi_2s[7]*10**(-3), alpha_2s[7] )
])

mbins = n.arange(8,12.5,0.25)

smf_ilbert_zmin = n.array([ 
0.2
, 0.5
, 0.8
, 1.1
, 1.5
, 2.0
, 2.5
, 3.0 ])

smf_ilbert_zmax = n.array([ 
0.5
, 0.8
, 1.1
, 1.5
, 2.0
, 2.5
, 3.0
, 4.0 ])

smf_ilbert_name = n.array([ "Il13 "+str(zmin)+"<z<"+str(zmax) for zmin, zmax in zip(smf_ilbert_zmin,smf_ilbert_zmax) ])


test_dir = os.path.join(os.environ[env], 'fits')

path_2_light_cone = os.path.join(test_dir, baseName + '.fits')
path_2_coordinate_file = os.path.join(test_dir, baseName + '_coordinates.fits')
path_2_galaxy_file = os.path.join(test_dir, baseName + '_galaxy.fits')
path_2_figure = os.path.join(plotDir, "SMF_galaxies_"+baseName+".png")

dlogM=0.2
bins = n.arange(7,13.1,dlogM)
x_SMF=(bins[1:]+bins[:-1])*0.5

def get_M(path_2_coordinate_file, path_2_galaxy_file):	
	f2 = fits.open(path_2_coordinate_file)
	z = f2[1].data['redshift_R']
	N_galaxies = len(z)
	f2.close()
	f3 = fits.open(path_2_galaxy_file)
	masses = f3[1].data['SMHMR_mass'] 
	f3.close()
	print(masses, z)
	volumes=(cosmoMD.comoving_volume(z.max())-cosmoMD.comoving_volume(z.min()))
	print(volumes)
	vol = volumes.value
	NN = n.histogram(masses, bins=bins)[0]/(dlogM*vol) 
	return NN, z.max(), z.min()

def plot_SMF(path_2_coordinate_file, path_2_galaxy_file, path_2_figure):
	NN, zMAX, zMIN = get_M(path_2_coordinate_file, path_2_galaxy_file)
	# SMF plot 
	p.figure(1,(6,6))
	p.axes([0.16,0.15,0.82,0.8])
	for fun, name in zip(smf_ilbert_fun, smf_ilbert_name):
		p.plot(mbins, fun(10**mbins)/(0.7**3), label=name, ls='dashed', lw=0.5)
	#p.errorbar( L15_M, y =  L15_phi*0.7**3, yerr=   L15_Err*0.7**3, color='blue' , label='Loveday 15, z<0.65')
	#p.errorbar( L15_M+2*n.log10(0.7),   y =  L15_phi*0.7**(3), yerr=   L15_Err*0.7**(3) , label='Loveday 15, z<0.65')
	p.plot(x_SMF, NN, label=str(n.round((zMAX + zMIN)/2.,2)))
	#p.colorbar(label='redshift')
	#p.tight_layout()
	p.xlabel(r'$\log_{10}(M_*/M_\odot)$')
	p.ylabel(r'$\Phi(M_*)d\log_{10}(M_*)$ [Mpc$^{-3}$]')
	p.yscale('log')
	p.ylim((1e-8,1e-1))
	p.xlim((9., 12.5))
	p.grid()
	p.legend(frameon=False, loc=0)
	p.savefig(path_2_figure)
	p.clf()

plot_SMF(path_2_coordinate_file, path_2_galaxy_file, path_2_figure)

sys.exit()

p.figure(1,(6,6))
p.axes([0.16,0.15,0.82,0.8])

p.scatter(x_smfs-n.log10(0.7), a_smfs, s=8, c=z_smfs, marker='s', edgecolor='face', label='AGN HSMF')
#p.plot(x_SMF-n.log10(0.7), a_smf, 'k--')

p.colorbar(label='redshift')
#p.tight_layout()
p.xlabel(r'$\log_{10}(M_*/M_\odot)$')
p.ylabel(r'$\Phi_{HGMF}(M_*,z)$ [Mpc$^{-3}$]')
p.yscale('log')
p.ylim((1e-7,1e-1))
p.xlim((8.5, 13))
p.grid()
p.legend(frameon=False, loc=0)
p.savefig(os.path.join(plotDir, "SMF_AGN.png"))
p.clf()


p.figure(1,(6,6))
p.axes([0.16,0.15,0.82,0.8])

p.scatter(data[2], a_smfs/data[3], s=8, c=(data[0]+data[1])*0.5, marker='s', edgecolor='face', label='duty cycle')
p.colorbar(label='redshift')

p.xlabel(r'$\log_{10}(M_*/M_\odot)$')
p.ylabel(r'$f_{AGN}(M_*,z)$')
p.yscale('log')
p.ylim((1e-5,1))
p.xlim((9, 12))
p.grid()
p.legend(frameon=False, loc=0)
p.savefig(os.path.join(plotDir, "SMF_AGN_DC.png"))
p.clf()


