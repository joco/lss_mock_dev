import os
import sys
import glob
import numpy as n

mini_fits = False
mini_fits = True

env = sys.argv[1]  # 'MD04'


def exec_cmds(env='MD04', mini_fits=mini_fits):
    if mini_fits:
        test_dir = os.path.join(os.environ[env], 'mini_fits')
    else:
        test_dir = os.path.join(os.environ[env], 'fits')

    if mini_fits:
        fig_dir = os.path.join(
            os.environ['GIT_AGN_MOCK'],
            'figures',
            env,
            'nbody_mini_fits',
        )
    else:
        fig_dir = os.path.join(
            os.environ['GIT_AGN_MOCK'],
            'figures',
            env,
            'nbody',
        )

    if os.path.isdir(fig_dir) == False:
        os.system('mkdir -p ' + fig_dir)

    all_files = sorted(
        n.array(
            glob.glob(
                os.path.join(
                    test_dir,
                    'all_?.?????.fits'))))

    for fff in all_files[::-1]:
        print(fff)
        cmd = 'python3 plot_Nbody.py ' + env + ' ' + \
            os.path.basename(fff)[:-5] + ' ' + str(mini_fits)
        print(cmd)
        try:
            os.system(cmd)
        except(OSError):
            print('os error')

    all_files = n.array(glob.glob(os.path.join(test_dir, 'sat_?.?????.fits')))
    all_files.sort()

    for fff in all_files[::-1]:
        print(fff)
        cmd = 'python3 plot_Nbody.py ' + env + ' ' + \
            os.path.basename(fff)[:-5] + ' ' + str(mini_fits)
        print(cmd)
        try:
            os.system(cmd)
        except(OSError):
            print('os error')


exec_cmds(env) 