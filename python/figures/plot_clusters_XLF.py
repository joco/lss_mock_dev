"""
python plot_clusters_XLF.py MD10
python plot_clusters_XLF.py MD04
python plot_clusters_XLF.py MD40
python plot_clusters_XLF.py UNIT_fA1i_DIR
python plot_clusters_XLF.py UNIT_fA1_DIR
python plot_clusters_XLF.py UNIT_fA2_DIR

"""

import sys
import os
from scipy.interpolate import interp1d
import astropy.io.fits as fits
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
from scipy.stats import norm

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})

import matplotlib.pyplot as p

import numpy as n
#import h5py
import time
print('Plots clusters catalog, X-ray luminosity function')
t0 = time.time()

env = 'MD10'
MMIN_VAL = 1e14
MMIN_VAL_STR = '1e14'
MMstr = r', $M_{500c}>1\times10^{14}M_\odot$'

MMIN_VAL = 7e13
MMIN_VAL_STR = '7e13'
MMstr = r', $M_{500c}>7\times10^{13}M_\odot$'

#env = 'MD10'
fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters')
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)

# simulation setup
if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

area = 30575 / 2. # |b|>15
area = 34089.  # |b|>10
#area = 129600 / n.pi # full sky
area = 27143.

dlog10lx = 0.25
lx_bins = n.arange(41.,50.,dlog10lx)
#if env=="MD04":
	#z_bins  = n.arange(0.,0.41, 0.2)
#else:
z_bins  = n.arange(0.0, 1.2, 0.2)

lx_val = ( lx_bins[1:] + lx_bins[:-1] )/2.
z_val  = ( z_bins [1:] + z_bins [:-1] )/2.

shell_volumes = (cosmo.comoving_volume(z_bins[1:])-cosmo.comoving_volume(z_bins[:-1]))*area*n.pi/129600

def get_XLF(env='MD10', fname = 'MD10_eRO_CLU_March2020.fit', mmin = MMIN_VAL, LX_name = 'CLUSTER_LX_soft_RF'):
	hdu_1 = fits.open( os.path.join(os.environ[env], fname) )
	#ra = hdu_1[1].data['RA']
	#dec = hdu_1[1].data['DEC']
	bb = hdu_1[1].data['g_lat']
	sel = (abs(bb) > 20)  & (hdu_1[1].data['HALO_M500c'] > mmin) # & (ll > 180)
	LX = hdu_1[1].data[LX_name]     [sel] 
	zz = hdu_1[1].data['redshift_R']        [sel]
	NN_LX_Z_1e13 = n.histogram2d(LX, zz, bins=[lx_bins,z_bins])[0].T
	return  NN_LX_Z_1e13 # hdu_1[1].data[sel]








NN_BP      = get_XLF(fname = 'MD10_eRO_CLU_bpaste.fit', LX_name = 'BPaste_LX_soft' )
#NN_Mar     = get_XLF(fname = 'MD10_eRO_CLU_March2020.fit' )
NN_b06     = get_XLF(fname = 'MD10_eRO_CLU_b6_CM_0_pixS_20.0.fits' )
NN_b07     = get_XLF(fname = 'MD10_eRO_CLU_b7_CM_0_pixS_20.0.fits' )
NN_b08     = get_XLF(fname = 'MD10_eRO_CLU_b8_CM_0_pixS_20.0.fits' )
NN_b09     = get_XLF(fname = 'MD10_eRO_CLU_b9_CM_0_pixS_20.0.fits' )
#NN_b11     = get_XLF(fname = 'MD10_eRO_CLU_b11.fit' )
NN_regular     = get_XLF(fname = 'MD10_eRO_CLU_b10_CM_0_pixS_20.0.fits' )
#NN_b10_m20 = get_XLF(fname = 'MD10_eRO_CLU_b10_m20.fit' )
#NN_b10_m40 = get_XLF(fname = 'MD10_eRO_CLU_b10_m40.fit' )
#NN_b10_p20 = get_XLF(fname = 'MD10_eRO_CLU_b10_p20.fit' )
#NN_b10_p40 = get_XLF(fname = 'MD10_eRO_CLU_b10_p40.fit' )
#NN_regular = get_XLF(fname = 'MD10_eRO_CLU_b10_0.fit' )

p_2_Z18 = os.path.join(os.environ['MD10'], 'Zandanel_2018')
Z18_BigMDPL = n.loadtxt(os.path.join(p_2_Z18, 'BigMDPLANCK_lightcone_5e13_Mantz2010.dat.gz'), unpack = True)
Z18 = Z18_BigMDPL
ok = (abs(Z18[3])>20)&(Z18[5]/0.6777>MMIN_VAL)
zz = Z18[1][ok]
LX = n.log10(Z18[19][ok])
NN_Z18_b10 = n.histogram2d(LX, zz, bins=[lx_bins,z_bins])[0].T

Z18_BigMDPL_b06 = n.loadtxt(os.path.join(p_2_Z18, 'BigMDPLANCK_lightcone_5e13_Mantz2010_bias0.6.dat.gz'), unpack = True)
Z18 = Z18_BigMDPL_b06
ok = (abs(Z18[3])>20)&(Z18[5]/0.6777>MMIN_VAL)
zz = Z18[1][ok]
LX = n.log10(Z18[19][ok])
NN_Z18_b06 = n.histogram2d(LX, zz, bins=[lx_bins,z_bins])[0].T


# CODEX Finoguenov 2019
f19_LX, f19_n_up, f19_n_low = n.loadtxt(os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'XLF_clusters', 'finoguenov_2019.txt'), unpack=True)
L_1e42, dndl, Err_percent = n.loadtxt(os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'XLF_clusters', 'pacaud_2015.txt'), unpack=True)
# REFLEX Boehringer 2002
schechter_fun = lambda L, Lstar, alpha, n0 : n0 * n.e**(-L/Lstar) * (L/Lstar)**(-alpha) #/ Lstar
schechter_mod = lambda L, Lstar, alpha, n0 : n0 * n.e**(-L/Lstar) * (L/Lstar)**(-alpha) * (1-(1+L/0.25)**(-1.7))#/ Lstar

boehringer2002 = lambda L : schechter_fun(L, 8e44, 1.3, 8e-8)
boehringer2014 = lambda L : schechter_mod(L, (5*0.7**(-2))*10**44, 2.13, 1.7e-7 * 0.7**3)
# REFLEX SOUTH
boehringer2002s = lambda L : schechter_fun(L, (5*0.5**(-2))*10**44, 1.55, 2.6e-7 * 0.5**3)/n.log(10)
# REFLEX norTH
boehringer2002n = lambda L : schechter_fun(L, (9.4*0.5**(-2))*10**44, 1.79, 0.9e-7 * 0.5**3)/n.log(10)


nBH02S = boehringer2002s(10**lx_val)
nBH02N = boehringer2002n(10**lx_val)
nBH14 = boehringer2014(10**lx_val)
#boehringer2014 = lambda L : schechter_fun(L, 1.08e44*0.7**-2, 1.7, 1.8e-6)
quickFit = boehringer2002(10**lx_val)

for NN_BP_i, NN_b06_i, NN_b08_i, NN_regular_i, NN_Z18_b06_i, NN_Z18_b10_i, z_v, Vol in zip(NN_BP, NN_b06, NN_b08, NN_regular, NN_Z18_b06, NN_Z18_b10, z_val, shell_volumes.value):
	fig_out = os.path.join(fig_dir, 'XLF_z'+str(n.round(z_v,1))+'_M_'+MMIN_VAL_STR+'.png')
	title_str='z='+str(n.round(z_v,1))+MMstr
	p.figure(2, (6.,6.))
	#p.fill_between(n.log10(f19_LX), f19_n_low, f19_n_up, alpha=0.4, label='F19 '+r'$0.1<z<0.2$')
	#p.fill_between(n.log10(L_1e42*1e42*0.697**(-2)), dndl*0.697**(5)*(1-Err_percent/100.), dndl*0.697**(5)*(1+Err_percent/100.), alpha=0.4, label=r'P15 $z<1.2$')
	p.plot(lx_val+n.log10(0.67), nBH02S, 'k--', label='BH02 S')
	p.plot(lx_val+n.log10(0.67), nBH02N, 'g--', label='BH02 N')
	p.plot(lx_val+n.log10(0.67), nBH14, 'b--', label='BH14')
	ok_BP_i       = (NN_BP_i      > 1 )
	ok_b06_i      = (NN_b06_i     > 1 )
	ok_b08_i      = (NN_b08_i     > 1 )
	ok_regular_i  = (NN_regular_i > 1 )
	ok_Z18_b06_i  = (NN_Z18_b06_i > 1 )
	ok_Z18_b10_i  = (NN_Z18_b10_i > 1 )
	p.plot(lx_val, quickFit, ls='dotted', label=r'$\alpha=-1.3$')
	p.plot(lx_val[ok_regular_i ], NN_regular_i [ok_regular_i ]/dlog10lx/Vol/n.log(10), lw=3, label='1')
	p.plot(lx_val[ok_b08_i     ], NN_b08_i     [ok_b08_i     ]/dlog10lx/Vol/n.log(10), lw=3, label='0.8')
	p.plot(lx_val[ok_b06_i     ], NN_b06_i     [ok_b06_i     ]/dlog10lx/Vol/n.log(10), lw=3, label='0.6')
	p.plot(lx_val[ok_BP_i      ], NN_BP_i      [ok_BP_i      ]/dlog10lx/Vol/n.log(10), lw=2, ls='dashed', label='BP ')
	p.plot(lx_val[ok_Z18_b06_i ], NN_Z18_b06_i [ok_Z18_b06_i ]/dlog10lx/Vol/n.log(10), lw=2, ls='dashed', label='Z18 0.6')
	p.plot(lx_val[ok_Z18_b10_i ], NN_Z18_b10_i [ok_Z18_b10_i ]/dlog10lx/Vol/n.log(10), lw=2, ls='dashed', label='Z18 1.0')
	p.xlabel(r'$\log_{10}(L_X/[erg/s])$')
	p.ylabel(r'$dn/d\log_{10}(L_X)/dV$ [Mpc$^{-3}$ dex$^{-1}$]')
	p.legend(frameon=True, loc=3)
	#p.xscale('log')
	p.yscale('log')
	p.xlim((42, 45.25))
	p.ylim((2e-9, 2e-5))
	p.title(title_str)
	p.tight_layout()
	p.grid()
	p.savefig(fig_out)
	p.clf()

