#!/bin/bash

topcat -stilts plot2sky \
   xpix=912 ypix=586 \
   projection=car \
   clon=54.52 clat=-11.29 radius=3.913 \
   legend=true \
   in=/data42s/comparat/firefly/stuff/RS_mock/MD04_eRO_CLU_SAT_RS.fit lon=ra lat=dec shading=auto \
   layer_1=Mark \
      color_1=blue \
      leglabel_1='2: All' \
   layer_2=Mark \
      icmd_2='select is_quiescent' \
      leglabel_2='2: is_quiescent' 
      

topcat -stilts plot2sky \
   xpix=912 ypix=586 \
   projection=car \
   legend=true \
   in=/data42s/comparat/firefly/stuff/RS_mock/MD04_eRO_CLU_SAT_RS.fit lon=ra lat=dec shading=auto \
   layer_1=Mark \
      color_1=blue \
      leglabel_1='2: All' \
   layer_2=Mark \
      icmd_2='select sdss_r_err>0' \
      color_2=green \
      leglabel_2='2: sdss' 
      
topcat -stilts plot2plane \
   xlabel='redshift_R / real space' ylabel= \
   xmin=-8.0E-8 xmax=0.816 ymin=0 \
    ymax=112348 \
   title=clusters legend=true \
   x=redshift_R \
   layer_1=Histogram \
      in_1=/data42s/comparat/firefly/stuff/RS_mock/MD10_eRO_CLU.fit \
      leglabel_1=MD10 \
   layer_2=Histogram \
      in_2=/data42s/comparat/firefly/stuff/RS_mock/MD04_eRO_CLU.fit \
      color_2=blue \
      leglabel_2=MD04
      
topcat -stilts plot2plane \
   ylog=true \
    xlabel='redshift_R / real space' \
    ylabel='HALO_M500c / log10(M_sun)' \
   xmin=0.005 xmax=0.815 ymin=2.7E13 \
    ymax=2.644E15 \
   legend=true \
   x=redshift_R y=HALO_M500c shading=auto \
   layer_1=Mark \
      in_1=/data42s/comparat/firefly/stuff/RS_mock/MD10_eRO_CLU.fit \
      color_1=green \
      leglabel_1=MD10 \
   layer_2=Mark \
      in_2=/data42s/comparat/firefly/stuff/RS_mock/MD04_eRO_CLU.fit \
       icmd_2='select redshift_R<0.28' \
      color_2=blue \
      leglabel_2='1: 0.28' 
      
      
topcat -stilts plot2plane \
   xpix=839 ypix=583 \
   xlabel='redshift_R / real space' ylabel='sdss_i / mag' \
   xmin=0.009 xmax=0.814 ymin=9.03 ymax=25.81 \
   legend=true \
   x=redshift_R y=sdss_i shading=auto \
   layer_1=Mark \
      in_1=/data42s/comparat/firefly/stuff/RS_mock/MD04_eRO_CLU_SAT_RS_sdss_only.fit \
      leglabel_1=MD04 \
   layer_2=Mark \
      in_2=/data42s/comparat/firefly/stuff/RS_mock/MD10_eRO_CLU_SAT_RS_sdss_only.fit \
      color_2=blue \
      leglabel_2=MD10 
      
topcat -stilts plot2plane \
   xpix=839 ypix=583 \
   xlog=true xlabel='HALO_vmax / km/s' ylabel='galaxy_mag_abs_r / mag' \
   xmin=15 xmax=2546 ymin=-25.04 ymax=-16.62 \
   legend=true \
   x=HALO_vmax y=galaxy_mag_abs_r shading=auto \
   layer_1=Mark \
      in_1=/data42s/comparat/firefly/stuff/RS_mock/MD04_eRO_CLU_SAT_RS_sdss_only.fit \
      leglabel_1=MD04 \
   layer_2=Mark \
      in_2=/data42s/comparat/firefly/stuff/RS_mock/MD10_eRO_CLU_SAT_RS_sdss_only.fit \
      color_2=blue \
      leglabel_2=MD10 
      
topcat -stilts plot2plane \
   xpix=839 ypix=583 \
   xlog=true xlabel='redshift_R / real space' ylabel='galaxy_gr / mag' \
   xmin=0.008 xmax=0.75 ymin=0.58 ymax=2.4 \
   legend=true \
   x=redshift_R y=galaxy_gr shading=auto \
   layer_1=Mark \
      in_1=/data42s/comparat/firefly/stuff/RS_mock/MD04_eRO_CLU_SAT_RS_sdss_only.fit \
      leglabel_1=MD04 \
   layer_2=Mark \
      in_2=/data42s/comparat/firefly/stuff/RS_mock/MD10_eRO_CLU_SAT_RS_sdss_only.fit \
      color_2=blue \
      leglabel_2=MD10 
      
topcat -stilts plot2plane \
   xpix=839 ypix=583 \
   xlog=true xlabel='redshift_R / real space' ylabel='galaxy_ri / mag' \
   xmin=0.008 xmax=0.75 ymin=0.26 ymax=1.61 \
   legend=true \
   x=redshift_R y=galaxy_ri shading=auto \
   layer_1=Mark \
      in_1=/data42s/comparat/firefly/stuff/RS_mock/MD04_eRO_CLU_SAT_RS_sdss_only.fit \
      leglabel_1=MD04 \
   layer_2=Mark \
      in_2=/data42s/comparat/firefly/stuff/RS_mock/MD10_eRO_CLU_SAT_RS_sdss_only.fit \
      color_2=blue \
      leglabel_2=MD10 
      
topcat -stilts plot2plane \
   xpix=839 ypix=583 \
   xlog=true xlabel='redshift_R / real space' ylabel='galaxy_iz / mag' \
   xmin=0.008 xmax=0.75 ymin=0.142 ymax=0.884 \
   legend=true \
   x=redshift_R y=galaxy_iz shading=auto \
   layer_1=Mark \
      in_1=/data42s/comparat/firefly/stuff/RS_mock/MD04_eRO_CLU_SAT_RS_sdss_only.fit \
      leglabel_1=MD04 \
   layer_2=Mark \
      in_2=/data42s/comparat/firefly/stuff/RS_mock/MD10_eRO_CLU_SAT_RS_sdss_only.fit \
      color_2=blue \
      leglabel_2=MD10 