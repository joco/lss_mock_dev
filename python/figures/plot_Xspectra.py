"""
python plot_Xspectra.py MD10

"""

import sys, glob, os
from scipy.interpolate import interp1d
import astropy.io.fits as fits
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
from scipy.stats import norm

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})

import matplotlib.pyplot as p

import numpy as n
#import h5py
import time
print('Plots clusters catalog, scaling relATIONS')
t0 = time.time()

env = 'MD10'
#sys.argv[1] # 'MD10'

fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'Xspectra')
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)

spec_list_clu = n.array( glob.glob( os.path.join(os.environ[env], 'cat_CLU_SIMPUT', 'cluster_Xspectra', '*.fits' ) ) )
spec_list_clu.sort()

spec_list_agn = n.array( glob.glob( os.path.join(os.environ[env], 'cat_AGN_SIMPUT', 'agn_Xspectra', '*.fits' ) ) )
spec_list_agn.sort()

fig_out = os.path.join(fig_dir, 'test.png')

spec_file = spec_list_agn[0]
hd_agn = fits.open(spec_file)
x_agn , y_agn = hd_agn[1].data['ENERGY'][0], hd_agn[1].data['FLUXDENSITY'][0]

bns_10kt = n.array([os.path.basename(spec_file)[17:-5].split('_')[1] for spec_file in spec_list_clu ]) 
bns_100z = n.array([os.path.basename(spec_file)[17:-5].split('_')[3] for spec_file in spec_list_clu ]) 

U_kt = n.unique(bns_10kt)
U_z = n.unique(bns_100z)

for U_z_i in U_z:
	fig_out = os.path.join(fig_dir, 'z_'+U_z_i+'.png')
	selection = (bns_100z==U_z_i)
	p.figure(1, (12., 12.))
	for jj, spec_file in enumerate(spec_list_clu[selection]):
		hd_clu = fits.open(spec_file)
		label = os.path.basename(spec_file)[17:-5]
		x_clu , y_clu = hd_clu[1].data['ENERGY'][0], hd_clu[1].data['FLUXDENSITY'][0]
		p.plot( x_clu , y_clu/2**jj , label=label, ls='solid',  lw=2, rasterized=True)

	#p.plot( x_agn , y_agn , label='agn', ls='solid',  lw=2, color='b')
	p.xlabel('kT [keV]')
	p.ylabel(r'flux density')
	p.legend(frameon=False, loc=3)
	p.xscale('log')
	p.yscale('log')
	#p.xlim((-14.2, -10.5))
	#p.ylim((-3, 1.5))
	#p.title(title_str)
	p.tight_layout()
	p.grid()
	p.savefig(fig_out)
	p.clf()


for U_kt_i in U_kt:
	fig_out = os.path.join(fig_dir, 'kt_'+U_kt_i+'.png')
	selection = (bns_10kt==U_kt_i)
	p.figure(1, (12., 12.))
	for jj, spec_file in enumerate(spec_list_clu[selection]):
		hd_clu = fits.open(spec_file)
		label = os.path.basename(spec_file)[17:-5]
		x_clu , y_clu = hd_clu[1].data['ENERGY'][0], hd_clu[1].data['FLUXDENSITY'][0]
		p.plot( x_clu , y_clu/2**jj , label=label, ls='solid',  lw=2, rasterized=True)

	#p.plot( x_agn , y_agn , label='agn', ls='solid',  lw=2, color='b')
	p.xlabel('kT [keV]')
	p.ylabel(r'flux density')
	p.legend(frameon=False, loc=3)
	p.xscale('log')
	p.yscale('log')
	#p.xlim((-14.2, -10.5))
	#p.ylim((-3, 1.5))
	#p.title(title_str)
	p.tight_layout()
	p.grid()
	p.savefig(fig_out)
	p.clf()

