"""
python plot_clusters_scaling_relations_M500c_LX.py MD10
python plot_clusters_scaling_relations_M500c_LX.py MD04
python plot_clusters_scaling_relations_M500c_LX.py MD40
python plot_clusters_scaling_relations_M500c_LX.py UNIT_fA1i_DIR
python plot_clusters_scaling_relations_M500c_LX.py UNIT_fA1_DIR
python plot_clusters_scaling_relations_M500c_LX.py UNIT_fA2_DIR

"""
from scipy.stats import norm 
from scipy.special import erf
import sys
import os
from scipy.interpolate import interp1d
import astropy.io.fits as fits
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
from scipy.stats import norm
from scipy.optimize import curve_fit
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
from scipy.stats import scoreatpercentile
import matplotlib.pyplot as p

import numpy as n
#import h5py
import time
print('Plots clusters catalog, scaling relATIONS')
t0 = time.time()

#hdu_0 = get_cat(env='MD04')
#hdu_1 = get_cat(env='MD40')
#env = 'MD10'

SR_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'scaling_relations')

itp_z, itp_kt, itp_frac_obs = n.loadtxt( os.path.join( os.environ['GIT_AGN_MOCK'], "data", "xray_k_correction", "fraction_05_20_01_24_no_nH.txt"), unpack=True )

nh_vals = 10**n.arange(-2,4+0.01,0.5)#0.05)
z_vals = n.hstack(( n.arange(0.,0.7,0.05), n.arange(0.8, 4.5, 0.1)))#[0.8, 0.9, 1, 1.1, 1.2, 1.4, 1.6] ))
#kT_vals = n.hstack(( n.arange(0.5,8,0.5), [10, 20, 30, 40, 50] ))
kT_vals = n.hstack(([0.1, 0.2], n.arange(0.5,8,0.5), [10, 20, 30, 40, 50] ))

XX_nh, YY_z, ZZ_kt = n.meshgrid(nh_vals, z_vals, kT_vals)

shape_i = XX_nh.shape

matrix_z_nh_kt = itp_frac_obs.reshape(shape_i)

from scipy.interpolate import RegularGridInterpolator
attenuation_3d = RegularGridInterpolator((z_vals, n.log10(nh_vals*1e22), kT_vals), matrix_z_nh_kt)

kT_kev, kT_kev_err, Rspec, R500, R500_err, M500, M500_err, Mgas500, Mgas500_err, R2500,  R2500_err, M2500, M2500_err, Mgas2500, Mgas2500_err, tcool, tcool_err, LXxmm, LXxmm_err = n.loadtxt(os.path.join(SR_dir, 'lovisari_2015_table2.ascii'), unpack=True)

redshift_s18, nh, LX_s18, Rktmax, M500NFWFreeze, M500kTextrp, M500NFWHudson, M500NFWAll, M200NFWFreeze, M200kTextrp, M500PlanckSZ = n.loadtxt(os.path.join(SR_dir, 'schllenberger_2018_tableB2B3.ascii'), unpack=True)

s_mi20, l_mi20, b_mi20, T_mi20, LX_mi20, sig_LX_mi20, f_mi20, NH_mi20, Z_mi20 = n.loadtxt(os.path.join(SR_dir, 'migkas_2020_tableC1.ascii'), unpack=True)

B18_id,    B18_z, B18_R500, B18_LXcin, B18_LXcinbol, B18_TXcin, B18_ZXcin, B18_LXcexbol, B18_LXcex, B18_TXcex, B18_ZXcex, B18_MICM, B18_YXcin, B18_M500 = n.loadtxt(os.path.join(SR_dir, 'bulbul_2018_table1_2.ascii'), unpack=True)

Lo20_planckName, Lo20_z, Lo20_M500, Lo20_Mg500, Lo20_kT, Lo20_kTexc, Lo20_LX, Lo20_LXexc, Lo20_Lbol, Lo20_Lbolexc, Lo20_NT, Lo20_fT, Lo20_Nsb, Lo20_fsb = n.loadtxt(os.path.join(SR_dir, 'lovisari_2020_tableA1.ascii'), unpack=True)

XXL_i = fits.open(os.path.join('/home/comparat/data/XMM/XXL','xxl365gc.fits'))[1].data
XXL = XXL_i[(XXL_i['Class']==1)]

WtG = fits.open(os.path.join(os.environ['GIT_AGN_MOCK'], 'data','WtG','Mantz16_Table2.fits'))[1].data

LB13_M5, LB13_LX = n.loadtxt(os.path.join(os.environ['GIT_AGN_MOCK'],'data/scaling_relations/lebrun_2013_fig1.ascii'), unpack=True )

env = 'MD10'
fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', 'SR_test')
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)

# simulation setup
if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

area = 30575 / 2.
area = 34089. 

def get_cat(env='MD04', name='fn'):
    path_2_CLU_catalog = os.path.join(os.environ[env], name) 
    hdu_1 = fits.open(path_2_CLU_catalog)
    ra = hdu_1[1].data['RA']
    dec = hdu_1[1].data['DEC']
    bb = hdu_1[1].data['g_lat']
    sel = (abs(bb) > 10) & (hdu_1[1].data['HALO_M500c'] > 0)
    return hdu_1[1].data[sel]

class ScalingRelation:
    pass

percents = n.arange(0,101,1)

def get_mean_scaling_relation(mass_array_log10, mass_proxy_array, redshift_array, DZ = 0.1, Dlog10M = 0.1, z_max_SR = 1.2, percents = percents ):
	SR = ScalingRelation()
	# define the binning scheme
	z_array = n.arange(n.min(redshift_array), n.min([z_max_SR, n.max(redshift_array)]), DZ)
	m_array = n.arange(n.min(mass_array_log10), n.max(mass_array_log10), Dlog10M)
	z_mins, m_mins = n.meshgrid(z_array , m_array)
	z_maxs, m_maxs = n.meshgrid(z_array + DZ , m_array + Dlog10M)
	redshift_mins, redshift_maxs = n.ravel(z_mins), n.ravel(z_maxs)
	mass_mins, mass_maxs = n.ravel(m_mins), n.ravel(m_maxs)
	# define empty columns
	mean_mass_proxy = n.zeros_like(mass_mins)
	std_mass_proxy  = n.zeros_like(mass_mins)
	mean_mass       = n.zeros_like(mass_mins)
	mean_redshift   = n.zeros_like(mass_mins)
	#n.array([5, 10, 20, 30, 32, 40, 50, 60, 68, 70, 80, 90, 95])
	percentile_values = n.zeros((len(mass_mins), len(percents)))
	# in each bin compute the mean and std of the mass proxy, mass and redshift
	for jj, (m_min, m_max, z_min, z_max) in enumerate(zip(mass_mins, mass_maxs, redshift_mins, redshift_maxs)):
		selection = ( redshift_array >= z_min ) & ( redshift_array < z_max ) & ( mass_array_log10 >= m_min ) & ( mass_array_log10 < m_max )
		mproxy_values = mass_proxy_array[ selection ]
		mm_values = mass_array_log10[ selection ]
		zz_values = redshift_array[ selection ]
		mean_mass_proxy[jj] = n.mean( mproxy_values )
		std_mass_proxy [jj] = n.std( mproxy_values )
		percentile_values[jj] = scoreatpercentile(mproxy_values, percents) 
		mean_mass      [jj] = n.mean(mm_values )
		mean_redshift  [jj] = n.mean(zz_values )
	
	# fit scaling relation for z<1.5
	good = (mean_mass>0)&(mean_mass_proxy>0)&(std_mass_proxy>0.001)&(redshift_mins < z_max_SR)
	xdata = mean_mass[good]
	ydata = mean_mass_proxy[good]
	yerr = std_mass_proxy[good]/2.
	xerr = Dlog10M/2.
	out_m = n.polyfit(xdata, ydata, deg=1, cov=True)#, w=1/yerr)
	x_model = n.arange(13, 16, 0.1)
	y_model = n.polyval(out_m[0], x_model)
	SR.mass_mins  = mass_mins 
	SR.mass_maxs, redshift_mins = mass_maxs, redshift_mins
	SR.redshift_maxs = redshift_maxs
	SR.mean_mass_proxy = mean_mass_proxy
	SR.std_mass_proxy = std_mass_proxy
	SR.mean_mass = mean_mass
	SR.mean_redshift = mean_redshift
	SR.x_model = x_model
	SR.y_model = y_model
	SR.out_m = out_m
	SR.percentile_values = percentile_values
	# output values and boundaries or std
	return SR  

######
#
# LX - M500c 
#
######
DZ = 1.1
def get_data_FX_mass(fname, env='MD10', FX_name = 'CLUSTER_LX_soft_RF',	DZ = 1.1,Dlog10M=0.2):
	hdu_MD10 = get_cat(env, fname) 
	hdu_data = hdu_MD10
	redshift_array = hdu_data['redshift_R']
	log_EZ =  n.log10(cosmo.efunc(redshift_array) )
	mass_array_log10 = n.log10(hdu_data['HALO_M500c'] ) + log_EZ
	mass_proxy_array = hdu_data[FX_name] - log_EZ
	rds = n.random.random(len(mass_array_log10))
	plotting_sample = (rds<0.1)
	MD10March = get_mean_scaling_relation(mass_array_log10, mass_proxy_array, redshift_array, DZ = DZ, Dlog10M= Dlog10M)
	MD10March.mass_array_log10 = mass_array_log10
	MD10March.mass_proxy_array = mass_proxy_array
	return MD10March


tt = fits.open('/home/comparat/data/erosita/erosim/t000.fits')[1].data
redshift_array = tt['redshift']
def f_LX_att(log10KT): return (0.51 + 0.5 * erf((log10KT - 0) / 0.2))
correction = n.min([f_LX_att(n.log10(tt['CLUSTER_kT'] ) ), n.ones_like(tt['CLUSTER_kT'])], axis=0)
log_EZ =  n.log10(cosmo.efunc(redshift_array) )
mass_array_log10 = tt['M500c'] + log_EZ
mass_proxy_array = tt['CLUSTER_LX_soft_RF'] - log_EZ  + n.log10(correction)
Dlog10M=0.2 
rds = n.random.random(len(mass_array_log10))
plotting_sample = (rds<0.1)
TT = get_mean_scaling_relation(mass_array_log10, mass_proxy_array, redshift_array, DZ = DZ, Dlog10M= Dlog10M)
TT.mass_array_log10 = mass_array_log10
TT.mass_proxy_array = mass_proxy_array


MD10_BP      = get_data_FX_mass('MD10_eRO_CLU_bpaste.fit', FX_name = 'BPaste_LX_soft' )
MD10_Mar     = get_data_FX_mass('MD10_eRO_CLU_March2020.fit' )
MD10_b06     = get_data_FX_mass('MD10_eRO_CLU_b6_CM_0_pixS_20.0.fits' )
MD10_b07     = get_data_FX_mass('MD10_eRO_CLU_b7_CM_0_pixS_20.0.fits' )
MD10_b08     = get_data_FX_mass('MD10_eRO_CLU_b8_CM_0_pixS_20.0.fits' )
MD10_b09     = get_data_FX_mass('MD10_eRO_CLU_b9_CM_0_pixS_20.0.fits' )
#MD10_b11     = get_data_FX_mass('MD10_eRO_CLU_b11.fit' )
MD10_b10_m20 = get_data_FX_mass('MD10_eRO_CLU_b10_m20.fit' )
MD10_b10_m40 = get_data_FX_mass('MD10_eRO_CLU_b10_m40.fit' )
MD10_b10_p20 = get_data_FX_mass('MD10_eRO_CLU_b10_p20.fit' )
MD10_b10_p40 = get_data_FX_mass('MD10_eRO_CLU_b10_p40.fit' )
MD10_b10 = get_data_FX_mass('MD10_eRO_CLU_b10_CM_0_pixS_20.0.fits' )
MD10_regular = get_data_FX_mass('MD10_eRO_CLU_b10_CM_0_pixS_20.0.fits' )


fig_out = os.path.join(fig_dir, 'M500c-LXscatter-distributionCDF-z.png')
#title_str=r'$M_{500c}>1\times10^{13}M_\odot$'

p.figure(0, (6., 5.))
x_norm = n.arange(-2,2,0.01)
p.plot(x_norm, 100*norm.cdf(x_norm, loc=0, scale=0.21), label='N(0,0.21)', lw=2, ls='dashed') 
for zz in n.unique(MD10_regular.redshift_maxs)[:1]:
	#print(zz)
	s1 = (MD10_regular.redshift_maxs == zz) 
	mmm = MD10_regular.mean_mass[s1]
	label = str(n.round(zz+DZ/2,2))
	pcs = MD10_regular.percentile_values[s1]
	for pc, m_i in zip( pcs[1:-2], mmm[1:-2] ) :
		p.plot(pc-pc[50], percents, label=str(n.round(m_i,1)), lw=0.5)

p.ylabel('per cent')
p.xlabel(r'$\sigma(\log_{10}(L_X/E(z)\; [erg\; s^{-1}])$ ')
p.legend(frameon=True, loc=0, fontsize=11)
#p.xscale('log')
#p.yscale('log')
p.xlim((-0.75, +0.75))
#p.ylim((-3, 1.5))
#p.title(title_str)
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()


fig_out = os.path.join(fig_dir, 'M500c-LXscatter-z.png')
#title_str=r'$M_{500c}>1\times10^{13}M_\odot$'

p.figure(0, (6., 5.))

for zz in n.unique(MD10_regular.redshift_maxs)[:1]:
	#print(zz)
	s1 = (MD10_regular.redshift_maxs == zz) 
	label = str(n.round(zz+DZ/2,2))
	pcs = MD10_regular.percentile_values[s1]
	#delta_90_10 = pcs.T[-1]-pcs.T[0]
	delta_80_20 = pcs.T[-2]-pcs.T[1]
	delta_70_30 = pcs.T[70]-pcs.T[30]
	delta_68_32 = pcs.T[68]-pcs.T[32]
	delta_60_40 = pcs.T[60]-pcs.T[40]
	#p.plot(MD10_regular.mean_mass[s1][:-1], delta_80_20[:-1], rasterized=True, label='20-80' )
	#p.plot(MD10_regular.mean_mass[s1][:-1], delta_70_30[:-1], rasterized=True, label='30-70' )
	p.plot(MD10_regular.mean_mass[s1][:-1], delta_68_32[:-1], rasterized=True, label='MDPL2')# 32%-68%' )
	#p.plot(MD10_regular.mean_mass[s1][:-1], delta_60_40[:-1], rasterized=True, label='40-60' )

for zz in n.unique(MD10_BP.redshift_maxs)[:1]:
	print(zz)
	s1 = (MD10_BP.redshift_maxs == zz) 
	label = str(n.round(zz+DZ/2,2))
	pcs = MD10_BP.percentile_values[s1]
	#delta_90_10 = pcs.T[-1]-pcs.T[0]
	delta_80_20 = pcs.T[-2]-pcs.T[1]
	delta_70_30 = pcs.T[70]-pcs.T[30]
	delta_68_32 = pcs.T[68]-pcs.T[32]
	delta_60_40 = pcs.T[60]-pcs.T[40]
	#p.plot(MD10_BP.mean_mass[s1][:-1], delta_80_20[:-1], rasterized=True, label='20-80' )
	#p.plot(MD10_BP.mean_mass[s1][:-1], delta_70_30[:-1], rasterized=True, label='30-70' )
	p.plot(MD10_BP.mean_mass[s1][:-1], delta_68_32[:-1], rasterized=True, label='Sh10' )
	#p.plot(MD10_BP.mean_mass[s1][:-1], delta_60_40[:-1], rasterized=True, label='40-60' )



p.axhline(0.2 , label='Lo20', ls='dashed', color='r')
p.axhline(0.24, label='Lo15', ls='dashed', color='b')
p.axhline(0.25, label='Bu19', ls='dashed', color='k')
#p.axvline(, label='Ad18')

p.xlabel(r'$\log_{10}(E(z) M_{500c}\; [M_\odot])$')
p.ylabel(r'$\sigma(\log_{10}(L_X/E(z)\; [erg\; s^{-1}])$ ')
p.legend(frameon=True, loc=0, fontsize=11)
#p.xscale('log')
#p.yscale('log')
p.xlim((13, 15.5))
#p.ylim((-3, 1.5))
#p.title(title_str)
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()


fig_out = os.path.join(fig_dir, 'M500c-LX-z.png')
#title_str=r'$M_{500c}>1\times10^{13}M_\odot$'

p.figure(0, (6., 5.))

#for zz in n.unique(MD10_regular.redshift_maxs)[:1]:
	#s1 = (MD10_regular.redshift_maxs == zz) 
	#pcs = MD10_regular.percentile_values[s1]
	#pcs_68 = pcs.T[68]
	#pcs_32 = pcs.T[32]
	#pcs_05 = pcs.T[5]
	#pcs_95 = pcs.T[95]
	#label = str(n.round(zz+DZ/2,2))
	#p.fill_between(10**MD10_regular.mean_mass[s1], 
				#y1=10**pcs_05, #MD10_regular.mean_mass_proxy[s1]-2*MD10_regular.std_mass_proxy[s1], 
				#y2=10**pcs_95, #MD10_regular.mean_mass_proxy[s1]+2*MD10_regular.std_mass_proxy[s1],  
				#rasterized=True, alpha=0.2, label='MDPL2 2$\sigma$')
	#p.fill_between(10**MD10_regular.mean_mass[s1], 
				#y1=10**pcs_32, #MD10_regular.mean_mass_proxy[s1]-3*MD10_regular.std_mass_proxy[s1], 
				#y2=10**pcs_68, #MD10_regular.mean_mass_proxy[s1]+3*MD10_regular.std_mass_proxy[s1],  
				#rasterized=True, alpha=0.4, label='MDPL2 1$\sigma$')
	##p.fill_between(MD10_regular.mean_mass[s1], 
				##y1=MD10_regular.mean_mass_proxy[s1]-MD10_regular.std_mass_proxy[s1], 
				##y2=MD10_regular.mean_mass_proxy[s1]+MD10_regular.std_mass_proxy[s1],  
				##rasterized=True, alpha=0.3)

for zz in n.unique(MD10_BP.redshift_maxs)[:1]:
	s1 = (MD10_BP.redshift_maxs == zz) 
	pcs = MD10_BP.percentile_values[s1]
	pcs_68 = pcs.T[68]
	pcs_32 = pcs.T[32]
	pcs_05 = pcs.T[5]
	pcs_95 = pcs.T[95]
	label = str(n.round(zz+DZ/2,2))
	p.fill_between(10**MD10_BP.mean_mass[s1], 
				y1=10**pcs_05, #MD10_BP.mean_mass_proxy[s1]-2*MD10_BP.std_mass_proxy[s1], 
				y2=10**pcs_95, #MD10_BP.mean_mass_proxy[s1]+2*MD10_BP.std_mass_proxy[s1],  
				rasterized=True, alpha=0.2, label='Sh10 2$\sigma$')
	p.fill_between(10**MD10_BP.mean_mass[s1], 
				y1=10**pcs_32, #MD10_BP.mean_mass_proxy[s1]-3*MD10_BP.std_mass_proxy[s1], 
				y2=10**pcs_68, #MD10_BP.mean_mass_proxy[s1]+3*MD10_BP.std_mass_proxy[s1],  
				rasterized=True, alpha=0.4, label='Sh10 1$\sigma$')
	#p.fill_between(MD10_BP.mean_mass[s1], 
				#y1=MD10_BP.mean_mass_proxy[s1]-MD10_BP.std_mass_proxy[s1], 
				#y2=MD10_BP.mean_mass_proxy[s1]+MD10_BP.std_mass_proxy[s1],  
				#rasterized=True, alpha=0.3)

for zz in n.unique(TT.redshift_maxs)[:1]:
	s1 = (TT.redshift_maxs == zz) 
	pcs = TT.percentile_values[s1]
	pcs_68 = pcs.T[68]
	pcs_32 = pcs.T[32]
	pcs_05 = pcs.T[5]
	pcs_95 = pcs.T[95]
	label = str(n.round(zz+DZ/2,2))
	p.fill_between(10**TT.mean_mass[s1], 
				y1=10**pcs_05, #TT.mean_mass_proxy[s1]-2*TT.std_mass_proxy[s1], 
				y2=10**pcs_95, #TT.mean_mass_proxy[s1]+2*TT.std_mass_proxy[s1],  
				rasterized=True, alpha=0.2, label='TT 2$\sigma$')
	p.fill_between(10**TT.mean_mass[s1], 
				y1=10**pcs_32, #TT.mean_mass_proxy[s1]-3*TT.std_mass_proxy[s1], 
				y2=10**pcs_68, #TT.mean_mass_proxy[s1]+3*TT.std_mass_proxy[s1],  
				rasterized=True, alpha=0.4, label='TT 1$\sigma$')
	#p.fill_between(TT.mean_mass[s1], 
				#y1=TT.mean_mass_proxy[s1]-TT.std_mass_proxy[s1], 
				#y2=TT.mean_mass_proxy[s1]+TT.std_mass_proxy[s1],  
				#rasterized=True, alpha=0.3)

# Lovsari 2020
k_correction_3d_Lo20 = attenuation_3d( n.transpose([Lo20_z, n.ones_like(Lo20_z)*20.1, Lo20_kT]))
p.plot( Lo20_M500*1e14*cosmo.efunc(Lo20_z) ,  Lo20_LX * k_correction_3d_Lo20 * 1e44 / cosmo.efunc(Lo20_z)  , marker='o', ls='', mfc='none', label='Lo20')

# Adami 18, XXL
p.plot( XXL['Mgas500kpc']*1e11*10**(1.1) * cosmo.efunc(XXL['z']), XXL['LXXL500MT'] * 1e42/cosmo.efunc(XXL['z']) , marker='o', ls='', label='Ad18', mfc='none')

# Lovisari 2015, groups
k_correction_3d_Lo15 = attenuation_3d( n.transpose([n.ones_like(kT_kev)*0.02, n.ones_like(kT_kev)*20.1, kT_kev]))
p.plot( M500 * 1e13 / 0.7 , LXxmm * k_correction_3d_Lo15 * 1e43, marker='s', ls='', mfc='none', label='Lo15')

# Schellenberger 18
k_correction_3d_Sc17 = attenuation_3d( n.transpose([redshift_s18, n.ones_like(redshift_s18)*20.1, n.ones_like(redshift_s18)*2]))
p.plot(M500NFWFreeze*1e14*cosmo.efunc(redshift_s18), LX_s18 * k_correction_3d_Sc17 * 1e43 / cosmo.efunc(redshift_s18), marker='o', ls='', label='Sc17', mfc='none')

# Bulbul 18
p.plot(B18_M500*1e14*cosmo.efunc(B18_z), B18_LXcin * 1e44 / cosmo.efunc(B18_z), marker='*', ls='', mfc='none', label='Bu19')

# Mantz 16
k_correction_3d_Ma16 = attenuation_3d( n.transpose([WtG['Ma16_z'], n.ones_like(WtG['Ma16_z'])*20.1, WtG['Ma16_kT_keV']]))
p.plot(  WtG['Ma16_Mlen_1e15'] * 1e15 ,  WtG['Ma16_LX_1e44'] * k_correction_3d_Ma16 * 1e44 / cosmo.efunc(WtG['Ma16_z'] ) , marker='s', ls='', mfc='none', label='Ma16')

p.plot(10**LB13_M5, 10**LB13_LX, 'b--', label='LB13 AGN8')

MD = MD10_regular
p.plot(10**MD.x_model, 10**MD.y_model, ls='dashed', color='k', label='y='+str(n.round(MD.out_m[0][0],1))+' x+'+str(n.round(MD.out_m[0][1],1)))
print('slope regular',MD.out_m[0][0],MD.out_m[1][0][0]**0.5)

p.errorbar(10**13.75, 10**45.2, xerr=0.15*10**13.75, yerr=0.1*10**45.2, color='k')

p.xlabel(r'$E(z) M_{500c}\; [M_\odot]$')
p.ylabel(r'$L_X/E(z)$ [erg s$^{-1}$]')
p.legend(frameon=True, loc=0, fontsize=10, ncol=2)
p.xscale('log')
p.yscale('log')
p.xlim((10**12.5, 10**15.5))
p.ylim((1e41, 10**45.5))
#p.title(title_str)
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()




fig_out = os.path.join(fig_dir, 'M500c-LX-z-noDATA.png')

p.figure(0, (6., 5.))

for zz in n.unique(MD10_BP.redshift_maxs)[:1]:
	s1 = (MD10_BP.redshift_maxs == zz) 
	pcs = MD10_BP.percentile_values[s1]
	pcs_68 = pcs.T[68]
	pcs_32 = pcs.T[32]
	pcs_05 = pcs.T[5]
	pcs_95 = pcs.T[95]
	label = str(n.round(zz+DZ/2,2))
	p.fill_between(MD10_BP.mean_mass[s1], 
				y1=pcs_05, #MD10_BP.mean_mass_proxy[s1]-2*MD10_BP.std_mass_proxy[s1], 
				y2=pcs_95, #MD10_BP.mean_mass_proxy[s1]+2*MD10_BP.std_mass_proxy[s1],  
				rasterized=True, alpha=0.2, label='Sh10 2$\sigma$')
	p.fill_between(MD10_BP.mean_mass[s1], 
				y1=pcs_32, #MD10_BP.mean_mass_proxy[s1]-3*MD10_BP.std_mass_proxy[s1], 
				y2=pcs_68, #MD10_BP.mean_mass_proxy[s1]+3*MD10_BP.std_mass_proxy[s1],  
				rasterized=True, alpha=0.4, label='Sh10 1$\sigma$')
	#p.fill_between(MD10_BP.mean_mass[s1], 
				#y1=MD10_BP.mean_mass_proxy[s1]-MD10_BP.std_mass_proxy[s1], 
				#y2=MD10_BP.mean_mass_proxy[s1]+MD10_BP.std_mass_proxy[s1],  
				#rasterized=True, alpha=0.3)

for zz in n.unique(MD10_regular.redshift_maxs)[:1]:
	s1 = (MD10_regular.redshift_maxs == zz) 
	pcs = MD10_regular.percentile_values[s1]
	pcs_68 = pcs.T[68]
	pcs_32 = pcs.T[32]
	pcs_05 = pcs.T[5]
	pcs_95 = pcs.T[95]
	label = str(n.round(zz+DZ/2,2))
	p.fill_between(MD10_regular.mean_mass[s1], 
				y1=pcs_05, #MD10_regular.mean_mass_proxy[s1]-2*MD10_regular.std_mass_proxy[s1], 
				y2=pcs_95, #MD10_regular.mean_mass_proxy[s1]+2*MD10_regular.std_mass_proxy[s1],  
				rasterized=True, alpha=0.2, label='MDPL2 2$\sigma$')
	p.fill_between(MD10_regular.mean_mass[s1], 
				y1=pcs_32, #MD10_regular.mean_mass_proxy[s1]-3*MD10_regular.std_mass_proxy[s1], 
				y2=pcs_68, #MD10_regular.mean_mass_proxy[s1]+3*MD10_regular.std_mass_proxy[s1],  
				rasterized=True, alpha=0.4, label='MDPL2 1$\sigma$')
	#p.fill_between(MD10_regular.mean_mass[s1], 
				#y1=MD10_regular.mean_mass_proxy[s1]-MD10_regular.std_mass_proxy[s1], 
				#y2=MD10_regular.mean_mass_proxy[s1]+MD10_regular.std_mass_proxy[s1],  
				#rasterized=True, alpha=0.3)


MD = MD10_regular
p.plot(MD.x_model, MD.y_model, ls='dashed', color='k', label='MDPL2 y='+str(n.round(MD.out_m[0][0],1))+' x+'+str(n.round(MD.out_m[0][1],1)))

MD = MD10_BP
p.plot(MD.x_model, MD.y_model, ls='dashed', color='r', label='Sh10 y='+str(n.round(MD.out_m[0][0],1))+' x+'+str(n.round(MD.out_m[0][1],1)))


p.xlabel(r'$\log_{10}(E(z) M_{500c}\; [M_\odot])$')
p.ylabel(r'$\log_{10}(L_X/E(z)$ [erg s$^{-1}$])')
p.legend(frameon=True, loc=0, fontsize=12)
#p.xscale('log')
#p.yscale('log')
p.xlim((12.5, 15.8))
p.ylim((1e41, 10**45.5))
#p.xlim((13, 15.5))
#p.ylim((-3, 1.5))
#p.title(title_str)
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()

MD10all = n.array([
	MD10_BP     
	,TT    
	,MD10_b06    
	,MD10_regular ])

MD10names = n.array([
	'Sh10'     
	,'TT'    
	,'MDPL2 b=0.6'    
	,'MDPL2 b=1.0' ])

hatches = n.array([
	".", 
	"x", 
	"+", 
	"+" ]) 

fig_out = os.path.join(fig_dir, 'M500c-LX-z-manyMD10.png')

p.figure(0, (6., 6.))
for MD10, name_i, hct in zip( MD10all[::-1], MD10names[::-1], hatches[::-1] ) : 
	for zz in n.unique(MD10.redshift_maxs)[:1]:
		s1 = (MD10.redshift_maxs == zz) 
		pcs = MD10.percentile_values[s1]
		pcs_68 = pcs.T[68]
		pcs_32 = pcs.T[32]
		pcs_05 = pcs.T[5]
		pcs_95 = pcs.T[95]
		label = str(n.round(zz+DZ/2,2))
		p.fill_between(MD10.mean_mass[s1], y1=pcs_32, y2=pcs_68, rasterized=True, alpha=0.3, label=name_i, hatch=hct)#+' 1$\sigma$')

p.xlabel(r'$\log_{10}(E(z) M_{500c}\; [M_\odot])$')
p.ylabel(r'$\log_{10}(L_X/E(z)$ [erg s$^{-1}$])')
p.legend(frameon=True, loc=2, fontsize=14)
p.xlim((12.5, 15.8))
p.ylim((41., 45.5))
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()

#sys.exit()

MD10all = n.array([
	 MD10_b10_m20
	,MD10_b10_m40
	,MD10_b10_p20
	,MD10_b10_p40, 
	MD10_regular])
MD10names = n.array([
	 'b10_m20'
	,'b10_m40'
	,'b10_p20'
	,'b10_p40', 'b10'])

fig_out = os.path.join(fig_dir, 'M500c-LX-z-manyMD10-2.png')

p.figure(0, (6., 5.))

for MD10, name_i in zip( MD10all, MD10names ) : 
	for zz in n.unique(MD10.redshift_maxs)[:1]:
		s1 = (MD10.redshift_maxs == zz) 
		pcs = MD10.percentile_values[s1]
		pcs_68 = pcs.T[68]
		pcs_32 = pcs.T[32]
		pcs_05 = pcs.T[5]
		pcs_95 = pcs.T[95]
		label = str(n.round(zz+DZ/2,2))
		#p.fill_between(MD10.mean_mass[s1], 
					#y1=pcs_05, #MD10.mean_mass_proxy[s1]-2*MD10.std_mass_proxy[s1], 
					#y2=pcs_95, #MD10.mean_mass_proxy[s1]+2*MD10.std_mass_proxy[s1],  
					#rasterized=True, alpha=0.2, label=name_i+' 2$\sigma$')
		#p.fill_between(MD10.mean_mass[s1], 
					#y1=pcs_32, #MD10.mean_mass_proxy[s1]-3*MD10.std_mass_proxy[s1], 
					#y2=pcs_68, #MD10.mean_mass_proxy[s1]+3*MD10.std_mass_proxy[s1],  
					#rasterized=True, alpha=0.4, label=name_i+' 1$\sigma$')
		#p.fill_between(MD10.mean_mass[s1], 
					#y1=MD10.mean_mass_proxy[s1]-MD10.std_mass_proxy[s1], 
					#y2=MD10.mean_mass_proxy[s1]+MD10.std_mass_proxy[s1],  
					#rasterized=True, alpha=0.3)

for MD, name_i in zip( MD10all, MD10names ) : 
	p.plot(MD.x_model, MD.y_model, ls='dashed', label=name_i)

p.xlabel(r'$\log_{10}(E(z) M_{500c}\; [M_\odot])$')
p.ylabel(r'$\log_{10}(L_X/E(z)$ [erg s$^{-1}$])')
p.legend(frameon=True, loc=2, fontsize=11)
#p.xscale('log')
#p.yscale('log')
p.xlim((13.7, 15.5))
p.ylim((42.5, 45.5))
#p.title(title_str)
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'M500c-LX-z-manyMD10-2contour.png')

p.figure(0, (6., 5.))

for MD10, name_i in zip( MD10all, MD10names ) : 
	for zz in n.unique(MD10.redshift_maxs)[:1]:
		s1 = (MD10.redshift_maxs == zz) 
		pcs = MD10.percentile_values[s1]
		pcs_68 = pcs.T[68]
		pcs_32 = pcs.T[32]
		pcs_05 = pcs.T[5]
		pcs_95 = pcs.T[95]
		label = str(n.round(zz+DZ/2,2))
		p.fill_between(MD10.mean_mass[s1], 
					y1=pcs_05, #MD10.mean_mass_proxy[s1]-2*MD10.std_mass_proxy[s1], 
					y2=pcs_95, #MD10.mean_mass_proxy[s1]+2*MD10.std_mass_proxy[s1],  
					rasterized=True, alpha=0.2, label=name_i+' 2$\sigma$')
		#p.fill_between(MD10.mean_mass[s1], 
					#y1=pcs_32, #MD10.mean_mass_proxy[s1]-3*MD10.std_mass_proxy[s1], 
					#y2=pcs_68, #MD10.mean_mass_proxy[s1]+3*MD10.std_mass_proxy[s1],  
					#rasterized=True, alpha=0.4, label=name_i+' 1$\sigma$')
		#p.fill_between(MD10.mean_mass[s1], 
					#y1=MD10.mean_mass_proxy[s1]-MD10.std_mass_proxy[s1], 
					#y2=MD10.mean_mass_proxy[s1]+MD10.std_mass_proxy[s1],  
					#rasterized=True, alpha=0.3)

for MD, name_i in zip( MD10all, MD10names ) : 
	p.plot(MD.x_model, MD.y_model, ls='dashed', label=name_i)

p.xlabel(r'$\log_{10}(E(z) M_{500c}\; [M_\odot])$')
p.ylabel(r'$\log_{10}(L_X/E(z)$ [erg s$^{-1}$])')
p.legend(frameon=True, loc=2, fontsize=11)
#p.xscale('log')
#p.yscale('log')
p.xlim((13.7, 15.5))
p.ylim((42.5, 45.5))
#p.title(title_str)
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()


MD10all = n.array([
	 MD10_b10_m20
	,MD10_b10_m40
	,MD10_b10_p20
	,MD10_b10_p40, MD10_regular])
MD10names = n.array([
	 'b10_m20'
	,'b10_m40'
	,'b10_p20'
	,'b10_p40', 'b10'])

fig_out = os.path.join(fig_dir, 'M500c-LX-z-manyMD10-2contour-ratio.png')

p.figure(0, (6., 5.))

for MD, name_i in zip( MD10all[:-1], MD10names[:-1] ) : 
	p.plot(MD.x_model, MD.y_model/MD10_regular.y_model, ls='dashed', label=name_i)

p.xlabel(r'$\log_{10}(E(z) M_{500c}\; [M_\odot])$')
p.ylabel(r'LX scaling relation ratio')
p.legend(frameon=True, loc=2, fontsize=11)
#p.xscale('log')
#p.yscale('log')
p.xlim((13.7, 15.5))
#p.ylim((42.5, 45.5))
#p.title(title_str)
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()
