"""
python plot_clusters_logNlogS.py MD10

"""

import sys
import os
from scipy.interpolate import interp1d
import astropy.io.fits as fits
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p
import numpy as n
import h5py
import time
print('Plots clusters catalog, logN logS')
t0 = time.time()


matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})

# simulation setup
cosmoMD = FlatLambdaCDM(
    H0=67.77 * u.km / u.s / u.Mpc,
    Om0=0.307115)  # , Ob0=0.048206)
h = 0.6777
L_box = 1000.0 / h
cosmo = cosmoMD

z_max = 2.

area = 30575. 
area = 34089. 
area = 27143.
def get_cat(env='MD04', cat_name = '_eRO_CLU.fit_w_SPT'):
    path_2_CLU_catalog = os.path.join(os.environ[env], env + cat_name ) #'MD10_eRO_CLU_Eckert19.fit')
    hdu_1 = fits.open(path_2_CLU_catalog)
    ra = hdu_1[1].data['RA']
    dec = hdu_1[1].data['DEC']
    bb = hdu_1[1].data['g_lat']
    ll = hdu_1[1].data['ecl_lon']
    sel = (abs(bb) > 20) & (hdu_1[1].data['HALO_M500c'] > 1e13) & (hdu_1[1].data['redshift_R']>=0.0) & (hdu_1[1].data['redshift_R'] < 2.0)
    return hdu_1[1].data[sel]


#hdu_0 = get_cat(env='MD04')
#hdu_1 = get_cat(env='MD40')
env = sys.argv[1] # 'MD10'
print(env, area)

#p_2_Z18 = '/data/data/MultiDark/Zandanel_2018/'
p_2_Z18 = os.path.join(os.environ['MD10'], 'Zandanel_2018')
# '/data39s/simulation_2/MD/Zandanel_2018/'
Z18_BigMD27 = n.loadtxt(os.path.join(p_2_Z18, 'BigMD27_lightcone_5e13_Mantz2010.dat.gz'), unpack = True)
Z18_BigMDPL = n.loadtxt(os.path.join(p_2_Z18, 'BigMDPLANCK_lightcone_5e13_Mantz2010.dat.gz'), unpack = True)
Z18_BigMD27_b06 = n.loadtxt(os.path.join(p_2_Z18, 'BigMD27_lightcone_5e13_Mantz2010_bias0.6.dat.gz'), unpack = True)
Z18_BigMDPL_b06 = n.loadtxt(os.path.join(p_2_Z18, 'BigMDPLANCK_lightcone_5e13_Mantz2010_bias0.6.dat.gz'), unpack = True)

hdu_10  = get_cat(env='MD10', cat_name = '_eRO_CLU_b10_CM_0_pixS_20.0.fits')
hdu_06 = get_cat(env='MD10', cat_name = '_eRO_CLU_b6_CM_0_pixS_20.0.fits')
hdu_07 = get_cat(env='MD10', cat_name = '_eRO_CLU_b7_CM_0_pixS_20.0.fits')
hdu_08 = get_cat(env='MD10', cat_name = '_eRO_CLU_b8_CM_0_pixS_20.0.fits')
hdu_09 = get_cat(env='MD10', cat_name = '_eRO_CLU_b9_CM_0_pixS_20.0.fits')
#hdu_11 = get_cat(env='MD10', cat_name = '_eRO_CLU_b11.fit')

if env=="MD04" :
	z0,z1,z2,z3 = 0.1, 0.2, 0.3, 0.45
if env=="MD10" or env=="MD40" or env[:4]=="UNIT" :
	z0,z1,z2,z3 = 0.3, 0.6, 0.9, 1.2

fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters')
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)

def get_lognlogs_replicas(fx, area=area):
    log_f_05_20 = n.log10(fx[fx > 0])
    out = n.histogram(log_f_05_20, bins=n.arange(-18, -8., 0.2),
                      weights=n.ones_like(log_f_05_20))
    # cumulative number density per square degrees
    x_out = 0.5 * (out[1][1:] + out[1][:-1])
    N_out = n.array([n.sum(out[0][ii:]) for ii in range(len(out[0]))])
    c_out = n.array([n.sum(out[0][ii:]) for ii in range(len(out[0]))]) / area
    c_out_up = (1 + N_out**(-0.5)) * c_out
    c_out_low = (1 - N_out**(-0.5)) * c_out
    c_err = (n.log10(c_out_up) - n.log10(c_out_low)) / 2.
    return x_out, c_out, c_err


Z18 = Z18_BigMD27
FX_Z18 = Z18[17][(abs(Z18[3])>20)&(Z18[5]/0.7>7e13)]
Z18_BigMD27_x_out, Z18_BigMD27_c_out, Z18_BigMD27_c_err = get_lognlogs_replicas(FX_Z18, area=area)

Z18 = Z18_BigMDPL
FX_Z18 = Z18[17][(abs(Z18[3])>20)&(Z18[5]/0.6777>7e13)]
Z18_BigMDPL_x_out, Z18_BigMDPL_c_out, Z18_BigMDPL_c_err = get_lognlogs_replicas(FX_Z18, area=area)

Z18 = Z18_BigMDPL_b06
FX_Z18 = Z18[17][(abs(Z18[3])>20)&(Z18[5]/0.6777>7e13)]
Z18_BigMDPL_b06_x_out, Z18_BigMDPL_b06_c_out, Z18_BigMDPL_b06_c_err = get_lognlogs_replicas(FX_Z18, area=area)

Z18 = Z18_BigMD27_b06
FX_Z18 = Z18[17][(abs(Z18[3])>20)&(Z18[5]/0.7>7e13)]
Z18_BigMD27_b06_x_out, Z18_BigMD27_b06_c_out, Z18_BigMD27_b06_c_err = get_lognlogs_replicas(FX_Z18, area=area)

spiders = fits.open(os.path.join(os.environ['HOME'], 'data/spiders/cluster', 'mastercatalogue_FINAL_CODEXID.fits'))[1].data
FX_bins = n.arange(8, 18., 0.25)
out = n.cumsum(n.histogram(-n.log10(spiders['FLUX052']), bins=FX_bins)[0])
c_out_spiders = out/5128.
c_err_spiders = c_out_spiders * out**(-0.5)
x_out_spiders = - 0.5 * (FX_bins[1:] + FX_bins[:-1])
ok_spiders = (c_err_spiders>0)&(c_out_spiders>0)&(c_out_spiders>2*c_err_spiders)&(x_out_spiders>-14) 


#p_2_header = os.path.join(os.environ['GIT_AGN_MOCK'],'logNlogS/spidersLogNLogS','lgnlgs_header.tbl')
#  (1)              (2)        (3)      (4)
#lg(flux) N>flux: 0.1<z<0.3 0.3<z<0.5 0.5<z<0.7
spiders_om205s860 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data','logNlogS/spidersLogNLogS','lgnlgs_om205s860.tbl'), unpack = True )
spiders_om270s792 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data','logNlogS/spidersLogNLogS','lgnlgs_om270s792.tbl'), unpack = True )
spiders_om342s730 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data','logNlogS/spidersLogNLogS','lgnlgs_om342s730.tbl'), unpack = True )
spiders_om307s823_i = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data','logNlogS/spidersLogNLogS','lgnlgs_om307s823h67forecast_logz_con.tbl'), unpack = True )
spiders_om307s823 = spiders_om307s823_i.T[4:].T 

#XXL = fits.open(os.path.join('/home/comparat/data/XMM/XXL','xxl365gc.fits'))[1].data
#out = n.cumsum(n.histogram(-n.log10(XXL['F60'] * 1e-15), bins=FX_bins)[0])
#c_out_xxl = out/20.
#c_err_xxl = c_out_xxl * out**(-0.5)
#x_out_xxl = - 0.5 * (FX_bins[1:] + FX_bins[:-1])
#ok_xxl = (c_err_xxl>0)&(c_out_xxl>0)&(c_out_xxl>2*c_err_xxl)&(x_out_xxl>-16) 

fig_out = os.path.join(fig_dir, 'logNlogS_2e13.png')
data_out = os.path.join(fig_dir, 'logNlogS_2e13.data')
M_min = 2e13
title_str=r'$M_{500c}>2\times10^{13}M_\odot$'
z_array = n.arange(0.3, 1.2, 0.3)
def plot_write_logNlogS(fig_out, data_out, M_min, title_str):
	#p.figure(2, (6., 6.))
	p.figure(3, (6., 6.))
	#ys = []
	#for z_max in z_array:
		#x, y, ye = get_lognlogs_replicas(hdu_2['CLUSTER_FX_soft'][ (hdu_2['redshift_R']<z_max) & (hdu_2['HALO_M500c']>M_min) ] )
		#p.errorbar(x, n.log10(y), yerr=ye, rasterized=True, label=r"$z<$"+str(z_max), lw=1)
		#ys.append(y)
	#y_z0 = y
	#x, y, ye = get_lognlogs_replicas(hdu_2['CLUSTER_FX_soft'][ (hdu_2['redshift_R']<z1) & (hdu_2['HALO_M500c']>M_min) ] )
	#p.errorbar(x,    n.log10(y),    yerr=ye,    rasterized=True,    label=r"$z<$"+str(z1),    lw=1)
	#y_z1 = y
	#x, y, ye = get_lognlogs_replicas(hdu_2['CLUSTER_FX_soft'][ (hdu_2['redshift_R']<z2) & (hdu_2['HALO_M500c']>M_min) ] )
	#p.errorbar(    x,    n.log10(y),    yerr=ye,    rasterized=True,    label=r"$z<$"+str(z2),    lw=1)
	#y_z2 = y
	#x, y, ye = get_lognlogs_replicas(hdu_2['CLUSTER_FX_soft'][ (hdu_2['redshift_R']<z3) & (hdu_2['HALO_M500c']>M_min) ] )
	#p.errorbar(    x,    n.log10(y),    yerr=ye,    rasterized=True,    label=r"$z<$"+str(z3),    lw=1)
	#y_z3 = y
	#x, y, ye = get_lognlogs_replicas(hdu_2['CLUSTER_FX_soft'])
	##p.errorbar(    x,    n.log10(y),    yerr=ye,    rasterized=True,    label=r"all",    lw=4)
	path_2_logNlogS_data = os.path.join(    os.environ["GIT_AGN_MOCK"],    'data',    'logNlogS',
		'logNlogS_Finoguenov_cosmos_2007_clusters.data')
	x_data, y_data, y_data_min, y_data_max = n.loadtxt(    path_2_logNlogS_data, unpack=True)
	p.fill_between(
		n.log10(x_data),
		y1= y_data_min,
		y2= y_data_max,
		rasterized=True,
		alpha=0.5,
		label='Fi07 COSMOS')
	path_2_logNlogS_data = os.path.join(    os.environ["GIT_AGN_MOCK"],    'data',    'logNlogS',
		'logNlogS_Finoguenov_ecdfs_2015_clusters.data')
	x_data, y_data, y_data_min, y_data_max = n.loadtxt(    path_2_logNlogS_data, unpack=True)
	p.fill_between(
		n.log10(x_data),
		y1=y_data_min,
		y2=y_data_max,
		rasterized=True,
		alpha=0.5,
		label='Fi15 CDFS')
	path_2_logNlogS_data = os.path.join(os.environ["GIT_AGN_MOCK"],'data','logNlogS','Boehringer_noras2_2017.data')
	FX_01_24, N_per_str_up, N_per_str_low = n.loadtxt(    path_2_logNlogS_data, unpack=True)
	N_per_str = (N_per_str_up + N_per_str_low)/2.
	x_data = FX_01_24 * 1e-12 * 0.67 / 1
	y_data = N_per_str / (180/n.pi)**2
	y_data_up = N_per_str_up / (180/n.pi)**2
	y_data_lo = N_per_str_low / (180/n.pi)**2
	#p.plot(n.log10(x_data)[3:-2], y_data[3:-2], rasterized=True, ls='dashed', label='B17 NORAS 2', lw=2)
	#p.fill_between(n.log10(x_data), y1=y_data_lo, y2=y_data_up, rasterized=True, label='Bo17 NORAS 2', color='c')
	#
	#
	# SPIDERS
	#
	#
	#p.fill_between(x_out_spiders[ok_spiders], y1=c_out_spiders[ok_spiders]-c_err_spiders[ok_spiders], y2=c_out_spiders[ok_spiders]+c_err_spiders[ok_spiders], label='JC SPIDERS', alpha=0.3, color='m')
	#
	x_spiders = spiders_om205s860[0]
	y1_spiders = n.min([n.sum(spiders_om205s860[1:,:] , axis=0), n.sum(spiders_om270s792[1:,:] , axis=0), n.sum(spiders_om342s730[1:,:] , axis=0), n.sum(spiders_om307s823[1:,:], axis=0)], axis=0)
	y2_spiders = n.max([n.sum(spiders_om205s860[1:,:] , axis=0), n.sum(spiders_om270s792[1:,:] , axis=0), n.sum(spiders_om342s730[1:,:] , axis=0), n.sum(spiders_om307s823[1:,:], axis=0)], axis=0)
	y_mean_count = (5100 * (y1_spiders + y2_spiders)/2.).astype('int')
	y_err_count = y_mean_count**(-0.5)
	#p.fill_between(x_spiders, y1 = y1_spiders*(1-y_err_count), y2 = y2_spiders*(1+y_err_count), label='Fi20 SPIDERS', alpha=0.3, color='c')
	#
	#p.plot(x_spiders,n.sum(spiders_om270s792[1:,:], axis=0), label='om270s792')
	#y_spiders = n.sum(spiders_om307s823[1:,:], axis=0)
	#y_mean_count = ( 5100 * y_spiders ).astype('int')
	#y_err_count = y_mean_count**(-0.5) * 2
	#p.plot(spiders_om307s823[0], y_spiders, label='SPIDERS om307s823')
	p.fill_between(x_spiders, y1 = y1_spiders*(1-y_err_count), y2 = y2_spiders*(1+y_err_count), label='Fi20 SPIDERS', alpha=0.3, color='r')
	#
	#
	# Zandanel 2018
	#
	#
	#p.plot(Z18_BigMD27_x_out,     Z18_BigMD27_c_out,     rasterized=True, lw=3, label="Z18 BigMD27", ls = 'dashed')
	p.plot(Z18_BigMDPL_x_out,     Z18_BigMDPL_c_out,     rasterized=True, lw=2, label="Z18 b=1.0", ls = 'dashed')
	#p.plot(Z18_BigMD27_b06_x_out, Z18_BigMD27_b06_c_out, rasterized=True, lw=3, label="Z18 BigMD27_b06", ls = 'dotted')
	p.plot(Z18_BigMDPL_b06_x_out, Z18_BigMDPL_b06_c_out, rasterized=True, lw=2, label="Z18 b=0.6", ls = 'dotted')
		
	#x, y, ye = get_lognlogs_replicas(hdu_06['CLUSTER_FX_soft'][ (hdu_06['redshift_R']>0.) &(hdu_06['redshift_R']<1.2) & (hdu_06['HALO_M500c']>M_min) ] )
	#p.plot(x, y, rasterized=True, lw=3, label=r"MDPL2 b0.6")
	x, y, ye = get_lognlogs_replicas(hdu_07['CLUSTER_FX_soft'][ (hdu_07['redshift_R']>0.) &(hdu_07['redshift_R']<1.2) & (hdu_07['HALO_M500c']>M_min) ] )
	p.errorbar(x, y, yerr = ye, rasterized=True, lw=3, label=r"MDPL2 b0.7")
	x, y, ye = get_lognlogs_replicas(hdu_08['CLUSTER_FX_soft'][ (hdu_08['redshift_R']>0.) &(hdu_08['redshift_R']<1.2) & (hdu_08['HALO_M500c']>M_min) ] )
	p.errorbar(x, y, yerr = ye, rasterized=True, lw=3, label=r"MDPL2 b0.8")
	x, y, ye = get_lognlogs_replicas(hdu_09['CLUSTER_FX_soft'][ (hdu_09['redshift_R']>0.) &(hdu_09['redshift_R']<1.2) & (hdu_09['HALO_M500c']>M_min) ] )
	p.errorbar(x, y, yerr = ye, rasterized=True, lw=3, label=r"MDPL2 b0.9")
	#x, y, ye = get_lognlogs_replicas(hdu_11['CLUSTER_FX_soft'][ (hdu_11['redshift_R']>0.) &(hdu_11['redshift_R']<1.2) & (hdu_11['HALO_M500c']>M_min) ] )
	#p.plot(x, y, rasterized=True, lw=3, label=r"MDPL2 b1.1")
	#x, y, ye = get_lognlogs_replicas(hdu_2['CLUSTER_FX_soft'] [ (hdu_2['redshift_R']>0.) & (hdu_2['redshift_R']<1.2) & (hdu_2['HALO_M500c']>M_min) ] )
	#p.plot(x, y, rasterized=True, lw=3, label=r"MDPL2 b1")

	# XXL
	#p.fill_between(x_out_xxl[ok_xxl], y1=c_out_xxl[ok_xxl]-c_err_xxl[ok_xxl], y2=c_out_xxl[ok_xxl]+c_err_xxl[ok_xxl], label='Ad18 XXL', alpha=0.1, color='black')
	#xs = []
	#ys = []
	#zs = []
	#yes = []
	#mmins = []
	#for z_max in z_array:
		#x, y, ye = get_lognlogs_replicas(hdu_2['CLUSTER_FX_soft'][ (hdu_2['redshift_R']<z_max) & (hdu_2['HALO_M500c']>M_min) ] )
		##p.plot(x, y, rasterized=True, lw=1, label=r"MDPL2 $z<$"+str(n.round(z_max,1)))
		#xs.append(x)
		#ys.append(y)
		#zs.append(n.ones_like(x)*z_max)
		#yes.append(ye)
		#mmins.append(n.ones_like(x)*M_min)
	#p.axhline(4, ls='dotted', color='k')
	p.xlabel('log(F[0.5-2.0 keV])')
	p.ylabel('log(>F) [/deg2]')
	p.legend(frameon=False, loc=3)
	p.yscale('log')
	p.xlim((-14.5, -10.))
	p.ylim((1e-4, 20))
	p.title(title_str)
	p.tight_layout()
	p.grid()
	p.savefig(fig_out)
	p.clf()
	#xs    = n.hstack(( xs    ))
	#ys    = n.hstack(( ys    ))
	#zs    = n.hstack(( zs    ))
	#yes   = n.hstack(( yes   ))
	#mmins = n.hstack(( mmins ))
	#n.savetxt(data_out, n.transpose([xs, ys, zs, yes, mmins ]) )
	#n.savetxt(data_out, n.transpose([ x, y, ye, y_z0, y_z1, y_z2, y_z3 ]) )

fig_out = os.path.join(fig_dir, 'logNlogS_2e13.png')
data_out = os.path.join(fig_dir, 'logNlogS_2e13.data')
M_min = 2e13
title_str=r'$M_{500c}>2\times10^{13}M_\odot$ $z<2$'
plot_write_logNlogS(fig_out, data_out, M_min, title_str)

fig_out = os.path.join(fig_dir, 'logNlogS_5e13.png')
data_out = os.path.join(fig_dir, 'logNlogS_5e13.data')
M_min = 5e13
title_str=r'$M_{500c}>5\times10^{13}M_\odot$ $z<2$'
plot_write_logNlogS(fig_out, data_out, M_min, title_str)

fig_out = os.path.join(fig_dir, 'logNlogS_7e13.png')
data_out = os.path.join(fig_dir, 'logNlogS_7e13.data')
M_min = 7e13
title_str=r'$M_{500c}>7\times10^{13}M_\odot$ $z<2$'
plot_write_logNlogS(fig_out, data_out, M_min, title_str)

fig_out = os.path.join(fig_dir, 'logNlogS_1e14.png')
data_out = os.path.join(fig_dir, 'logNlogS_1e14.data')
M_min = 1e14
title_str=r'$M_{500c}>1\times10^{14}M_\odot$ $z<2$'
plot_write_logNlogS(fig_out, data_out, M_min, title_str)

fig_out = os.path.join(fig_dir, 'logNlogS_2e14.png')
data_out = os.path.join(fig_dir, 'logNlogS_2e14.data')
M_min = 2e14
title_str=r'$M_{500c}>2\times10^{14}M_\odot$ $z<2$'
plot_write_logNlogS(fig_out, data_out, M_min, title_str)

fig_out = os.path.join(fig_dir, 'logNlogS_5e14.png')
data_out = os.path.join(fig_dir, 'logNlogS_5e14.data')
M_min = 5e14
title_str=r'$M_{500c}>5\times10^{14}M_\odot$ $z<2$'
plot_write_logNlogS(fig_out, data_out, M_min, title_str)

sys.exit()

# summary figure
fig_out = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', 'logNlogS_cluster_all.png')

#p.figure(2, (6., 5.))
p.figure(3, (12., 10.))

path_2_logNlogS_data = os.path.join(    os.environ["GIT_AGN_MOCK"],    'data',    'logNlogS',
	'logNlogS_Finoguenov_cosmos_2007_clusters.data')
x_data, y_data, y_data_min, y_data_max = n.loadtxt(    path_2_logNlogS_data, unpack=True)
p.fill_between(
	n.log10(x_data),
	y1=n.log10(y_data_min),
	y2=n.log10(y_data_max),
	rasterized=True,
	alpha=0.5,
	label='F07 COSMOS')

path_2_logNlogS_data = os.path.join(    os.environ["GIT_AGN_MOCK"],    'data',    'logNlogS',
	'logNlogS_Finoguenov_ecdfs_2015_clusters.data')
x_data, y_data, y_data_min, y_data_max = n.loadtxt(    path_2_logNlogS_data, unpack=True)
p.fill_between(
	n.log10(x_data),
	y1=n.log10(y_data_min),
	y2=n.log10(y_data_max),
	rasterized=True,
	alpha=0.5,
	label='F15 CDFS')

path_2_logNlogS_data = os.path.join(os.environ["GIT_AGN_MOCK"],'data','logNlogS','Boehringer_noras2_2017.data')
FX_01_24, N_per_str_up, N_per_str_low = n.loadtxt(    path_2_logNlogS_data, unpack=True)
x_data = FX_01_24 * 1e-12 / 1
p.fill_between(
	n.log10(x_data[1:]),
	y1=n.log10(N_per_str_low[1:]/ (180/n.pi)**2),
	y2=n.log10(N_per_str_up [1:]/ (180/n.pi)**2),
	rasterized=True,
	alpha=0.5,
	label='B17 NORAS 2')

env='MD10'
fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters')

path_2_logNlogS_data = os.path.join(fig_dir, 'logNlogS_7e13.data')
xs, ys, zs, yes, mmins = n.loadtxt(    path_2_logNlogS_data, unpack=True)
p.scatter(xs[zs<1.], n.log10(ys[zs<1.]), c=zs[zs<1.], s=6, marker='o', linewidths=2, edgecolors='face', rasterized=True, cmap='seismic',label= r'$M_{500c}>7\times10^{13}M_\odot$')
p.colorbar(label='z')
p.axhline(7, ls='dashed')
p.xlabel('log(F[0.5-2.0 keV])')
p.ylabel('log(>F) [/deg2]')
p.legend(frameon=False, loc=3)
#p.yscale('log')
p.xlim((-14.2, -10.))
p.ylim((-4, 1.5))
#p.title(title_str)
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()



fig_out = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', 'logNlogS_cluster_1e14.png')
#title_str=r'$M_{500c}>1\times10^{14}M_\odot$'

p.figure(2, (6., 5.))

path_2_logNlogS_data = os.path.join(    os.environ["GIT_AGN_MOCK"],    'data',    'logNlogS',
	'logNlogS_Finoguenov_cosmos_2007_clusters.data')
x_data, y_data, y_data_min, y_data_max = n.loadtxt(    path_2_logNlogS_data, unpack=True)
p.fill_between(
	n.log10(x_data),
	y1=n.log10(y_data_min),
	y2=n.log10(y_data_max),
	rasterized=True,
	alpha=0.5,
	label='F07 COSMOS')

path_2_logNlogS_data = os.path.join(    os.environ["GIT_AGN_MOCK"],    'data',    'logNlogS',
	'logNlogS_Finoguenov_ecdfs_2015_clusters.data')
x_data, y_data, y_data_min, y_data_max = n.loadtxt(    path_2_logNlogS_data, unpack=True)
p.fill_between(
	n.log10(x_data),
	y1=n.log10(y_data_min),
	y2=n.log10(y_data_max),
	rasterized=True,
	alpha=0.5,
	label='F15 CDFS')

path_2_logNlogS_data = os.path.join(os.environ["GIT_AGN_MOCK"],'data','logNlogS','Boehringer_noras2_2017.data')
FX_01_24, N_per_str_up, N_per_str_low = n.loadtxt(    path_2_logNlogS_data, unpack=True)
x_data = FX_01_24 * 1e-12 / 1
p.fill_between(
	n.log10(x_data[1:]),
	y1=n.log10(N_per_str_low[1:]/ (180/n.pi)**2),
	y2=n.log10(N_per_str_up [1:]/ (180/n.pi)**2),
	rasterized=True,
	alpha=0.5,
	label='B17 NORAS 2')

env='MD10'
fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters')

path_2_logNlogS_data = os.path.join(fig_dir, 'logNlogS_1e14.data')
xs, ys, zs, yes, mmins = n.loadtxt(    path_2_logNlogS_data, unpack=True)
p.scatter(xs[zs<1.], n.log10(ys[zs<1.]), c=zs[zs<1.], s=6, marker='o', linewidths=2, edgecolors='face', rasterized=True, cmap='seismic',label= r'$M_{500c}>1\times10^{14}M_\odot$')
p.colorbar(label='z')
p.axhline(7, ls='dashed')
p.xlabel('log(F[0.5-2.0 keV])')
p.ylabel('log(>F) [/deg2]')
p.legend(frameon=False, loc=3)
#p.yscale('log')
p.xlim((-14.2, -10.))
p.ylim((-4, 1.5))
#p.title(title_str)
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()
