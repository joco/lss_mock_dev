"""

python plot_cluster_dlambda_lambda.py 

"""
import sys, os
import astropy.io.fits as fits
import astropy.units as u
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p
import numpy as n
import time
from astropy.table import Table, Column
from astropy.coordinates import SkyCoord
from sklearn.neighbors import BallTree

fig_dir = os.path.join(os.environ['HOME'], 'data/erosita/comparison-photoz')

N_MEM_MIN = 5
# eFEDs data

#d_a = fits.open('/home/comparat/data/erosita/eFEDS/redmapper/b0_Sc1Cat_V13c_ext_gt_0.fits')[1].data
#d_b = fits.open('/home/comparat/data/erosita/eFEDS/redmapper/decals_dr8_run_redmapper_v0.6.6_lgt05_catalog.fit')[1].data
#d_c = fits.open('/home/comparat/data/erosita/eFEDS/MCMF/eFEDS02_22_v940_valadd.fits')[1].data

#deg_to_rad = n.pi/180.
#coord_a = deg_to_rad * n.transpose([d_a['DEC'], d_a['RA']])
#coord_b = deg_to_rad * n.transpose([d_b['DEC'], d_b['RA']])
#coord_c = deg_to_rad * n.transpose([d_c['DEC'], d_c['RA']])

#Tree_a = BallTree(coord_a, metric='haversine')
#Tree_b = BallTree(coord_b, metric='haversine')
#Tree_c = BallTree(coord_c, metric='haversine')

#radius = 1./60. # 1 arc minute in degrees
#distance_b, index_b = Tree_b.query(coord_a, k=1, return_distance=True) 
#distance_c, index_c = Tree_c.query(coord_a, k=1, return_distance=True) 

#t_b = Table( d_b[ n.hstack(( index_b )) ] )
#t_c = Table( d_c[ n.hstack(( index_c )) ] )

#merged_table = Table(d_a)
#for el in t_b.columns.keys(): 
	#merged_table.add_column( Column( data = t_b[el], name = 'RM_'+el ) ) 

#merged_table.add_column( Column( data = distance_b * 60. / deg_to_rad, name = 'RM_distance_arcmin' ) )

#for el in t_c.columns.keys(): 
	#merged_table.add_column( Column( data = t_c[el], name = 'MC_'+el ) ) 

#merged_table.add_column( Column( data = distance_c * 60. / deg_to_rad, name = 'MC_distance_arcmin' ) )

#merged_table.write('/home/comparat/data/erosita/eFEDS/merged-X-RM-MCMF.fits', overwrite = True)

d1 = Table.read('/home/comparat/data/erosita/eFEDS/merged-X-RM-MCMF.fits')

eFEDs_spec_z_selection = ( d1['RM_spec_z'] > 0.1 ) & ( d1['RM_spec_z'] < 0.7 ) & ( d1['RM_distance_arcmin'] < 0.5 ) & ( d1['MC_distance_arcmin'] < 0.5 ) & ( d1['MC_Z_LFC'] > 0. ) & ( d1['RM_n_members'] > N_MEM_MIN )
eFEDs_zphot_mcmf       = d1['MC_Z_LFC'][eFEDs_spec_z_selection]
eFEDs_zphot_redmapper  = d1['RM_photo_z'][eFEDs_spec_z_selection]
eFEDs_zspec            = d1['RM_spec_z'][eFEDs_spec_z_selection]
eFEDs_lambda_mcmf      = d1['MC_LAMBDA_P1'][eFEDs_spec_z_selection]
eFEDs_lambda_redmapper = d1['RM_lambda'   ][eFEDs_spec_z_selection]

d3 = Table.read('/home/comparat/data/erosita/eRASS/merged-X-RM-MCMF-June26.fits')

eRASS_spec_z_selection = ( d3['RM_spec_z_boot'] > 0.1 ) & ( d3['RM_spec_z_boot'] < 0.7 ) & ( d3['RM_distance_arcmin'] < 0.5 ) & ( d3['MC_distance_arcmin'] < 0.5 ) & ( d3['MC_Z_LFC'] > 0. ) & ( d3['RM_n_members'] > N_MEM_MIN )
eRASS_zphot_mcmf       = d3['MC_Z_LFC'][eRASS_spec_z_selection]
eRASS_zphot_redmapper  = d3['RM_photo_z'][eRASS_spec_z_selection]
eRASS_zspec            = d3['RM_spec_z_boot'][eRASS_spec_z_selection]
eRASS_lambda_mcmf      = d3['MC_LAMBDA_P1'][eRASS_spec_z_selection]
eRASS_lambda_redmapper = d3['RM_lambda'   ][eRASS_spec_z_selection]

zphot_mcmf       = n.hstack(( eFEDs_zphot_mcmf      , eRASS_zphot_mcmf      )) 
zphot_redmapper  = n.hstack(( eFEDs_zphot_redmapper , eRASS_zphot_redmapper )) 
zspec            = n.hstack(( eFEDs_zspec           , eRASS_zspec           )) 
lambda_mcmf      = n.hstack(( eFEDs_lambda_mcmf      , eRASS_lambda_mcmf      ))
lambda_redmapper = n.hstack(( eFEDs_lambda_redmapper , eRASS_lambda_redmapper ))

print('eROSITA has', len(zspec), 'clusters over eRASS + eFEDs')

# comparison MCMF redmapper

fig_out = os.path.join(fig_dir, 'specz002-dlambda-lambda.png')

p.figure(1, (6., 6.))
#
z_diff_rm = abs(zphot_redmapper-zspec)/(1+zspec)
z_diff_mcmf = abs(zphot_mcmf-zspec)/(1+zspec)
good_photoz_rm = ( z_diff_rm < 0.02 ) & ( z_diff_mcmf < 0.02 )
#l_arr = n.arange(5, 500, 1)
#snr = l_arr*0.1
#l_error = l_arr
p.axhline(10, c='b', ls='dashed')
p.plot(lambda_redmapper[good_photoz_rm], abs(lambda_mcmf[good_photoz_rm]-lambda_redmapper[good_photoz_rm]), 'k+')

p.xlabel(r'$\lambda_{RM}$')
p.ylabel(r'$|\lambda_{MCMF}-\lambda_{RM}|$')

#p.legend(frameon=True, loc=4, fontsize=14)
#p.xscale('log')
p.yscale('log')
#p.xlim((-0.75, +0.75))
#p.ylim((1e-4, 0.5))
p.title(str(len(z_diff_rm[good_photoz_rm]))+' specz clusters in eFEDs + eRASS $\Delta_z<0.02$')
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'specz002-dlambda-lambda-2.png')

p.figure(1, (6., 6.))
#
z_diff_rm = abs(zphot_redmapper-zspec)/(1+zspec)
z_diff_mcmf = abs(zphot_mcmf-zspec)/(1+zspec)
good_photoz_rm = ( z_diff_rm < 0.02 ) & ( z_diff_mcmf < 0.02 )
mean_ratio = n.mean(lambda_mcmf[good_photoz_rm]/lambda_redmapper[good_photoz_rm])
l_arr = n.arange(0, 200, 1)
p.plot(l_arr, l_arr, 'b--', label='y=x')
p.plot(l_arr, mean_ratio*l_arr, 'b--', label='y='+str(n.round(mean_ratio,2))+'x')
#snr = l_arr*0.1
#l_error = l_arr
#p.axhline(10, c='b', ls='dashed')
p.plot(lambda_redmapper[good_photoz_rm], lambda_mcmf[good_photoz_rm], 'k+')

p.xlabel(r'$\lambda_{RM}$')
p.ylabel(r'$\lambda_{MCMF}$')

p.legend(frameon=True, loc=4, fontsize=14)
#p.xscale('log')
#p.yscale('log')
#p.xlim((-0.75, +0.75))
#p.ylim((1e-4, 0.5))
p.title(str(len(z_diff_rm[good_photoz_rm]))+' specz clusters in eFEDs + eRASS $\Delta_z<0.02$')
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()



d1 = Table.read('/home/comparat/data/erosita/eFEDS/merged-X-RM-MCMF.fits')

eFEDs_spec_z_selection = ( d1['RM_photo_z'] > 0.1 ) & ( d1['RM_photo_z'] < 0.7 ) & ( d1['RM_distance_arcmin'] < 0.5 ) & ( d1['MC_distance_arcmin'] < 0.5 ) & ( d1['MC_Z_LFC'] > 0.1 ) & ( d1['MC_Z_LFC'] < 0.7 )
eFEDs_zphot_mcmf       = d1['MC_Z_LFC'][eFEDs_spec_z_selection]
eFEDs_zphot_redmapper  = d1['RM_photo_z'][eFEDs_spec_z_selection]
eFEDs_zspec            = d1['RM_spec_z'][eFEDs_spec_z_selection]
eFEDs_lambda_mcmf      = d1['MC_LAMBDA_P1'][eFEDs_spec_z_selection]
eFEDs_lambda_redmapper = d1['RM_lambda'   ][eFEDs_spec_z_selection]
eFEDs_lambda_redmapperE= d1['RM_lambda_e' ][eFEDs_spec_z_selection]

d3 = Table.read('/home/comparat/data/erosita/eRASS/merged-X-RM-MCMF.fits')

eRASS_spec_z_selection = ( d3['RM_photo_z'] > 0.1 ) & ( d3['RM_photo_z'] < 0.7 ) & ( d3['RM_distance_arcmin'] < 0.5 ) & ( d3['MC_distance_arcmin'] < 0.5 ) & ( d3['MC_Z_LFC'] > 0.1 ) & ( d3['MC_Z_LFC'] < 0.7 )
eRASS_zphot_mcmf       = d3['MC_Z_LFC'][eRASS_spec_z_selection]
eRASS_zphot_redmapper  = d3['RM_photo_z'][eRASS_spec_z_selection]
eRASS_zspec            = d3['RM_spec_z_boot'][eRASS_spec_z_selection]
eRASS_lambda_mcmf      = d3['MC_LAMBDA_P1'][eRASS_spec_z_selection]
eRASS_lambda_redmapper = d3['RM_lambda'   ][eRASS_spec_z_selection]
eRASS_lambda_redmapperE= d3['RM_lambda_e' ][eRASS_spec_z_selection]

zphot_mcmf       = n.hstack(( eFEDs_zphot_mcmf      , eRASS_zphot_mcmf      )) 
zphot_redmapper  = n.hstack(( eFEDs_zphot_redmapper , eRASS_zphot_redmapper )) 
zspec            = n.hstack(( eFEDs_zspec           , eRASS_zspec           )) 
lambda_mcmf      = n.hstack(( eFEDs_lambda_mcmf      , eRASS_lambda_mcmf      ))
lambda_redmapper = n.hstack(( eFEDs_lambda_redmapper , eRASS_lambda_redmapper ))
lambda_redmapperE= n.hstack(( eFEDs_lambda_redmapperE , eRASS_lambda_redmapperE ))

print('eROSITA has', len(zspec), ' specz clusters over eRASS + eFEDs')

# comparison MCMF redmapper

fig_out = os.path.join(fig_dir, 'dlambda-lambda-frac.png')

p.figure(1, (6., 6.))
#
z_diff = abs(zphot_redmapper-zphot_mcmf)
good_photoz_rm = ( z_diff < 0.02 ) 
l_arr = n.arange(2, 300, 1)
#snr = l_arr*0.1
#l_error = l_arr
#p.axhline(10, c='b', ls='dashed')
x_data, y_data = lambda_redmapper[good_photoz_rm], lambda_redmapperE[good_photoz_rm]/lambda_redmapper[good_photoz_rm]
pols = n.polyfit(n.log10(x_data), n.log10(y_data), deg=1)
y_err = n.polyval(pols, n.log10(l_arr))
p.plot(x_data, y_data, 'mx', label=r'$\lambda_{RM}$ fractional error')
p.plot(l_arr, 10**y_err, 'r--', lw=3, label=r'$y=10**($'+str(n.round(pols[0],2)) + r'$\log_{10}(\lambda)+$'+str(n.round(pols[1],2)) + ')')

#x_data, y_data = lambda_redmapper[good_photoz_rm], abs(lambda_mcmf[good_photoz_rm]-lambda_redmapper[good_photoz_rm]) / lambda_redmapper[good_photoz_rm]
#pols = n.polyfit(n.log10(x_data), n.log10(y_data), deg=1)
y_err = n.polyval([pols[0], pols[1]+0.7], n.log10(l_arr))
p.plot(l_arr, 10**y_err, 'b--', lw=3, label=r'$y=10**($'+str(n.round(pols[0],2)) + r'$\log_{10}(\lambda)+$'+str(n.round(pols[1]+0.7,2))+ ')' )

p.plot(lambda_redmapper[good_photoz_rm], abs(lambda_mcmf[good_photoz_rm]-lambda_redmapper[good_photoz_rm]) / lambda_redmapper[good_photoz_rm], 'k+', label = r'$|\lambda_{MCMF}-\lambda_{RM}|/\lambda_{RM}$')
p.xlabel(r'$\lambda_{RM}$')
#p.ylabel(r'$|\lambda_{MCMF}-\lambda_{RM}|/\lambda_{RM}, \sigma(\lambda_{RM})$')
p.ylabel('fraction')
p.legend(frameon=True, loc=3, fontsize=14)
p.xscale('log')
p.yscale('log')
p.xlim((5, 200))
p.ylim((1e-2, 10))
p.title(str(len(z_diff[good_photoz_rm]))+' eFEDs + eRASS $\Delta_z<0.02$')
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'dlambda-lambda.png')

p.figure(1, (6., 6.))
#
z_diff = abs(zphot_redmapper-zphot_mcmf)
good_photoz_rm = ( z_diff < 0.02 ) 
#l_arr = n.arange(5, 500, 1)
#snr = l_arr*0.1
#l_error = l_arr
p.axhline(10, c='b', ls='dashed')
p.plot(lambda_redmapper[good_photoz_rm], lambda_redmapperE[good_photoz_rm], 'rx', label=r'error on RM $\lambda$')
p.plot(lambda_redmapper[good_photoz_rm], abs(lambda_mcmf[good_photoz_rm]-lambda_redmapper[good_photoz_rm]), 'k+', label = 'difference')
p.xlabel(r'$\lambda_{RM}$')
p.ylabel(r'$|\lambda_{MCMF}-\lambda_{RM}|$')

p.legend(frameon=True, loc=4, fontsize=14)
p.xscale('log')
p.yscale('log')
p.xlim((5, 200))
p.ylim((1e-2, 200))
p.title(str(len(z_diff[good_photoz_rm]))+' eFEDs + eRASS $\Delta_z<0.02$')
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'dlambda-lambda-2.png')

p.figure(1, (6., 6.))
#
z_diff = abs(zphot_redmapper-zphot_mcmf)
good_photoz_rm = ( z_diff < 0.02 ) 
mean_ratio = n.mean(lambda_mcmf[good_photoz_rm]/lambda_redmapper[good_photoz_rm])
l_arr = n.arange(0, 200, 1)
p.plot(l_arr, l_arr, 'b--', label='y=x')
p.plot(l_arr, mean_ratio*l_arr, 'b--', label='y='+str(n.round(mean_ratio,2))+'x')
#snr = l_arr*0.1
#l_error = l_arr
#p.axhline(10, c='b', ls='dashed')
p.plot(lambda_redmapper[good_photoz_rm], lambda_mcmf[good_photoz_rm], 'k+')
#p.plot(lambda_redmapper[good_photoz_rm], lambda_redmapperE[good_photoz_rm], 'r,', label=r'error on RM $\lambda$')

p.xlabel(r'$\lambda_{RM}$')
p.ylabel(r'$\lambda_{MCMF}$')

p.legend(frameon=True, loc=4, fontsize=14)
#p.xscale('log')
#p.yscale('log')
#p.xlim((-0.75, +0.75))
#p.ylim((1e-4, 0.5))
p.title(str(len(z_diff[good_photoz_rm]))+' eFEDs + eRASS $\Delta_z<0.02$')
p.tight_layout()
p.grid()
p.savefig(fig_out)
p.clf()


