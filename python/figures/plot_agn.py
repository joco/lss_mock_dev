import sys
import os
import matplotlib.pyplot as p
import matplotlib
import numpy as n
import h5py
import time
print('Plots AGN data')
t0 = time.time()


matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})


env = sys.argv[1]  # 'MD04'
baseName = sys.argv[2]  # "sat_0.62840"
print(env, baseName)

test_dir = os.path.join(os.environ[env], 'hlist', 'fits')

path_2_light_cone = os.path.join(test_dir, baseName + '_coordinates.h5')
path_2_galaxy_file = os.path.join(test_dir, baseName + '_galaxy.h5')
path_2_agn_file = os.path.join(test_dir, baseName + '_agn.h5')

fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'agn', )
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)


h5_file = os.path.join(path_2_light_cone)
f = h5py.File(h5_file, "r")
zr = f['/coordinates/redshift_R'].value
zs = f['/coordinates/redshift_S'].value

g_lat = f['/coordinates/g_lat'].value
g_lon = f['/coordinates/g_lon'].value

ecl_lat = f['/coordinates/ecl_lat'].value
ecl_lon = f['/coordinates/ecl_lon'].value

ra = f['/coordinates/ra'].value
dec = f['/coordinates/dec'].value

dL = f['/coordinates/dL'].value
NH = f['/coordinates/NH'].value
ebv = f['/coordinates/ebv'].value
f.close()

# GALAXY file
h5_file = os.path.join(path_2_galaxy_file)
f = h5py.File(h5_file, "r")
galaxy_SMHMR_mass = f['/galaxy/SMHMR_mass'].value
galaxy_star_formation_rate = f['/galaxy/star_formation_rate'].value
galaxy_LX_hard = f['/galaxy/LX_hard'].value
galaxy_mag_abs_r = f['/galaxy/mag_abs_r'].value
galaxy_mag_r = f['/galaxy/mag_r'].value
f.close()

# AGN file
h5_file = os.path.join(path_2_agn_file)
f = h5py.File(h5_file, "r")
AGN_ids_active = f['/AGN/ids_active'].value
AGN_LX_hard = f['/AGN/LX_hard'].value
AGN_LX_soft = f['/AGN/LX_soft'].value
AGN_FX_soft = f['/AGN/FX_soft'].value
AGN_FX_soft_attenuated = f['/AGN/FX_soft_attenuated'].value
AGN_FX_hard = f['/AGN/FX_hard'].value
AGN_logNH = f['/AGN/logNH'].value
AGN_agn_type = f['/AGN/agn_type'].value
AGN_random = f['/AGN/random'].value
AGN_SDSS_r_AB = f['/AGN/SDSS_r_AB'].value
AGN_SDSS_r_AB_attenuated = f['/AGN/SDSS_r_AB_attenuated'].value

N_obj = len(AGN_ids_active)
rds = n.random.random(N_obj)
sel = (rds < 1e6 / N_obj)


# 2D plots

fig_out = os.path.join(fig_dir, 'AGN_LX_soft_vs_zr_' + baseName + '.png')

X = zr[AGN_ids_active][sel]
Y = AGN_LX_soft[sel]
p.figure(1, (6., 5.5))
p.tight_layout()
p.plot(X, Y, 'k,', rasterized=True)
p.title(baseName)
p.yscale('log')
p.xlabel('redshift R')
p.ylabel('LX_soft')
p.grid()
p.savefig(fig_out)
p.clf()


# histograms

fig_out = os.path.join(
    fig_dir,
    'AGN_SDSS_r_AB_attenuated_hist_' +
    baseName +
    '.png')
X = AGN_SDSS_r_AB_attenuated

p.figure(1, (6., 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('AGN_SDSS_r_AB_attenuated')
p.ylabel('Counts')
p.grid()
p.yscale('log')
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'AGN_SDSS_r_AB_hist_' + baseName + '.png')
X = AGN_SDSS_r_AB

p.figure(1, (6., 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('AGN_SDSS_r_AB')
p.ylabel('Counts')
p.grid()
p.yscale('log')
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'agn_type_hist_' + baseName + '.png')
X = AGN_agn_type

p.figure(1, (6., 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('agn_type')
p.ylabel('Counts')
p.grid()
p.yscale('log')
p.savefig(fig_out)
p.clf()


fig_out = os.path.join(fig_dir, 'logNH_hist_' + baseName + '.png')
X = AGN_logNH

p.figure(1, (6., 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('logNH')
p.ylabel('Counts')
p.grid()
p.yscale('log')
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'FX_hard_hist_' + baseName + '.png')
X = n.log10(AGN_FX_hard)

p.figure(1, (6., 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('FX_hard')
p.ylabel('Counts')
p.grid()
p.yscale('log')
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'FX_soft_attenuated_hist_' + baseName + '.png')
X = n.log10(AGN_FX_soft_attenuated)

p.figure(1, (6., 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('FX_soft_attenuated')
p.ylabel('Counts')
p.grid()
p.yscale('log')
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'FX_soft_hist_' + baseName + '.png')
X = n.log10(AGN_FX_soft)

p.figure(1, (6., 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('FX_soft')
p.ylabel('Counts')
p.grid()
p.yscale('log')
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'LX_soft_hist_' + baseName + '.png')
X = AGN_LX_soft

p.figure(1, (6., 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('LX_soft')
p.ylabel('Counts')
p.grid()
p.yscale('log')
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'LX_hard_hist_' + baseName + '.png')

X = AGN_LX_hard

p.figure(1, (6., 5.5))
p.tight_layout()
p.hist(X, histtype='step', rasterized=True, lw=4)
p.title(baseName)
p.xlabel('LX_hard')
p.ylabel('Counts')
p.grid()
p.yscale('log')
p.savefig(fig_out)
p.clf()


# sys.exit()
