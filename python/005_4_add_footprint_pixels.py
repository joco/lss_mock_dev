"""
Creates a fits catalog containing the 4FS input columns.

Create the ID array
remove unwanted column
check rulesets and templates names are correct

005_4_add_footprint.py


python 005_4_add_footprint_pixels.py MD10 000 
      x_01=REDSHIFT_ESTIMATE y_01=MAG \
      zero_01=1.007 \
      leglabel_01='AGN eROSITA' \
   layer_02=Contour \
      in_02=/data/data/MultiDark/MD_1.0Gpc/QSO_4MOST.fits \
      x_02=REDSHIFT_ESTIMATE y_02=MAG \
      color_02=orange \
      leglabel_02='QSO S8' \
   layer_03=Contour \
      in_03=/data/data/MultiDark/MD_1.0Gpc/AGN_IR_4MOST.fits \

"""
import glob
import sys
from astropy_healpix import healpy
import os
from scipy.stats import scoreatpercentile
import pandas as pd  # external package
from scipy.special import erf
from astropy.coordinates import SkyCoord
import astropy.constants as cc
import astropy.io.fits as fits
from astropy.table import Table, Column
import astropy.units as u
import numpy as n
import extinction
from matplotlib.path import Path 
from astropy import units
import pymangle
nl = lambda selection : len(selection.nonzero()[0])

print('adds survey bits')
print('------------------------------------------------')
print('------------------------------------------------')

MASKDIR = os.path.join(os.environ['GIT_AGN_MOCK'],'data', 'masks')

env = sys.argv[1]  
HEALPIX_id = int(sys.argv[2])
print(env, HEALPIX_id)

root_dir = os.path.join(os.environ[env])
dir_2_OUT = os.path.join(root_dir, "cat_SHAM_COSMO")

# get survey footprint bitlist
def fourmost_get_survbitlist():
	mydict                = {}
	mydict['des']         = 0
	mydict['kidss']       = 1
	mydict['kidsn']       = 2
	mydict['atlassgcnotdes'] = 3
	mydict['atlasngc']    = 4
	mydict['kabs']        = 5
	mydict['vhsb10']      = 6
	mydict['vhsb15']      = 7
	mydict['vhsb20']      = 8
	mydict['vhsb20clean'] = 9
	mydict['desi']        = 10
	mydict['erosita']     = 11
	mydict['waveswide']   = 12
	mydict['euclid']      = 13
	mydict['s8elg']       = 14
	mydict['s8']          = 15
	return mydict


# 4most s8 footprint
def fourmost_get_s8foot(ra_deg, dec_deg):
	ra = ra_deg.value
	dec = dec_deg.value
	svbdict = fourmost_get_survbitlist()
	survbit = n.zeros(len(ra),dtype=int)
	for bname in ['des','desi','kidss','kidsn','atlassgcnotdes','atlasngc']:
		isb           = fourmost_get_survbit_indiv(ra_deg,dec_deg,bname)
		survbit[isb] += 2**svbdict[bname]
	# bg/lrgi/qso/lya
	iss8  = ((((survbit & 2**svbdict['des'])>0) | ((survbit & 2**svbdict['atlassgcnotdes'])>0)) & ((survbit & 2**svbdict['desi'])==0)) | ((((survbit & 2**svbdict['kidsn'])>0) & (ra>154)) | ((survbit & 2**svbdict['atlasngc'])>0)) 
	# elg
	iss8elg = (((ra>330) | (ra<90)) & (dec>-35.5) & (dec<-26)) | ((ra>50) & (ra<90) & (dec>-40) & (dec<-26))
	return iss8,iss8elg



# get survey footprint bit
def fourmost_get_survbit_indiv(ra_deg,dec_deg,bname):
	#
	# Galactic l,b
	c       = SkyCoord(ra=ra_deg, dec=dec_deg, frame='fk5')
	l,b     = c.galactic.l.value,c.galactic.b.value
	lon,lat = c.barycentrictrueecliptic.lon.degree,c.barycentrictrueecliptic.lat.degree
	ra = ra_deg.value
	dec = dec_deg.value
	# 4most/s8 , s8elg
	if (bname in ['s8elg','s8']):
		iss8,iss8elg = fourmost_get_s8foot(ra_deg, dec_deg)
		if (bname=='s8elg'):
			keep = iss8elg
		else: # s8
			keep = iss8
	# desi: no ply file for the moment...
	elif (bname=='desi'):
		# desi sgc
		polyra = n.array([0 ,-25,-35,-50,-54,-45,10,10,  60, 70, 70,53,42,42,38,0])
		polydec= n.array([33,33, 25,  8,  -8, -15,-15,-20,-20,-15,0, 0, 10,20,33,33])
		sgcpoly = Path(n.concatenate(
							(polyra. reshape((len(polyra),1)),
							 polydec.reshape((len(polyra),1))),
							axis=1))
		# desi ngc
		polyra = n.array([275,107,115,130,230,230,230,255,265,275])
		polydec= n.array([33, 33, 12, -10,-10, -2, -2, -2,  13, 33])
		ngcpoly = Path(n.concatenate(
							(polyra. reshape((len(polyra),1)),
							 polydec.reshape((len(polyra),1))),
							axis=1))
		#
		tmpradec         = n.transpose(n.array([ra,dec]))
		tmp              = (ra>300)
		tmpradec[tmp,0] -= 360.
		keep = n.zeros(len(ra),dtype=bool)
		for poly in [sgcpoly,ngcpoly]:
			keep[poly.contains_points(tmpradec)] = True
	elif (bname=='erosita'):
		# johan email 30/07/2018 14:12
		keep = (abs(b)>15) & (l>180)
	elif (bname=='waveswide'):
		# https://wavesurvey.org/project/survey-design/
		keep = (((ra>155) & (ra<240) & (dec>-5) & (dec<5))
				|
				(((ra>330) | (ra<50)) & (dec>-36) & (dec<-26)))
	elif (bname=='euclid'):
		keep = (n.abs(b)>=30) & (n.abs(lat)>5.)
	else:
		if (bname[:3]=='vhs'):
			## -70<dec<0
			mng    = pymangle.Mangle(MASKDIR+'/vhsdec.ply')
			polyid = mng.polyid(ra,dec)
			keepdec= (polyid!=-1)
			# |b|>bmin
			mng    = pymangle.Mangle(MASKDIR+'/'+bname[3:6]+'.ply')
			polyid = mng.polyid(l,b)
			keepb  = (polyid!=-1)
			##
			keep   = (keepdec) & (keepb)
		else:
			mng    = pymangle.Mangle(MASKDIR+'/'+bname+'.ply')
			polyid = mng.polyid(ra,dec)
			keep   = (polyid!=-1)
		if (bname=='vhsb20clean'):
			## Jext<0.1 and low nstar selection [both VHS and DES]
			ra60    = (ra>55)  & (ra<65)  & (dec>-5)  & (dec<0)
			ra70    = (ra>67)  & (ra<72)  & (dec>-16) & (dec<-13)
			ra100   = (ra>50)  & (ra<180) & (dec>-20) & (dec<0)   & (b>-23) & (b<0)
			ra120   = (ra>100) & (ra<180)                         & (b>-15) & (b<0)
			ra230   = (ra>228) & (ra<270) & (dec>-40) & (dec<-20) & (b>0) 
			ra250   = (ra>235) & (ra<270) & (dec>-20) & (dec<0)   & (b>0)
			ra300   = (ra>180) & (ra<360) & (dec>-70) & (dec<0)   & (b>-25) & (b<0)
			LMC     = (ra>70)  & (ra<90)  & (dec>-70) & (dec<-65)
			keep    = ((keep) & 
						(~ra60)  & (~ra70)  & 
						(~ra100) & (~ra120) & 
						(~ra230) & (~ra250) & (~ra300) &
						(~LMC))
	#print( bname, len(ra[keep]))
	return keep


def fourmost_get_survbit(ra,dec):
	bdict   = fourmost_get_survbitlist()
	survbit = n.zeros(len(ra),dtype='int')
	for bname in bdict.keys():
		#print(bname)
		isb           = fourmost_get_survbit_indiv(ra,dec,bname)
		survbit[isb] += 2**bdict[bname]
	return survbit


def make_4most_Catalogue(HEALPIX_id):
	print(HEALPIX_id)
	path_2_BG    = os.path.join(dir_2_OUT, 'BG_'  + str(HEALPIX_id).zfill(6) + '.fit')
	path_2_BG_S5 = os.path.join(dir_2_OUT, 'S5GAL_'  + str(HEALPIX_id).zfill(6) + '.fit')
	path_2_LRG   = os.path.join(dir_2_OUT, 'LRG_' + str(HEALPIX_id).zfill(6) + '.fit')
	#path_2_ELG   = os.path.join(dir_2_OUT, 'ELG_' + str(HEALPIX_id).zfill(6) + '.fit')

	def write_sbits(dir_2_MOCK):
		print(dir_2_MOCK)
		t_survey  = Table.read(dir_2_MOCK  )
		if t_survey['RA'].unit==u.deg:
			mask_bit = fourmost_get_survbit(t_survey['RA'].data*u.deg, t_survey['DEC'].data*u.deg)
		else:
			mask_bit = fourmost_get_survbit(t_survey['RA']*u.deg, t_survey['DEC']*u.deg)
		#print(t_survey.columns.keys())
		if 'MASK_BIT' in t_survey.columns.keys():
			t_survey['MASK_BIT'] = mask_bit
		else:
			t_survey.add_column(Column(name='MASK_BIT'  ,data=mask_bit, unit=''))
			
		t_survey.write (dir_2_MOCK  , overwrite=True)
		
	write_sbits(path_2_BG    )
	write_sbits(path_2_BG_S5 )
	write_sbits(path_2_LRG   )
	#write_sbits(path_2_ELG   )

make_4most_Catalogue(HEALPIX_id)
