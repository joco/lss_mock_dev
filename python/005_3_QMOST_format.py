"""
What it does
------------

Add realistic magnitudes to BG, LRG and FIL (ELG deprecated)

Command to run
--------------

python3 005_2_all_magnitudes.py environmentVAR 


RULESET_NAME;REQ_VALUE;EXPRESSION
Red_SN02_Nov21;1.0;(Elliptical_Nov21 >= 2.)
Red_SN03_Nov21;1.0;(Elliptical_Nov21 >= 3.)
Red_SN04_Nov21;1.0;(Elliptical_Nov21 >= 4.)
Red_SN05_Nov21;1.0;(Elliptical_Nov21 >= 5.)
Red_SN10_Nov21;1.0;(Elliptical_Nov21 >= 10.)
(eroconda) comparat@he07lt:~/software/lss_mock_dev/python/qmost/S5$ cat S5_20211109T1657Z_rules.csv
RULE_NAME;VARIABLE;METRIC;OPERATOR;VALUE;L_MIN;L_MAX;L_UNIT;DELTA_L;DELTA_L_UNIT;RES_MODE;SPEC_ARM
Elliptical_Nov21;SNR;MEDIAN;DIV;1.0;7200.0;9000.0;AA;1.0;AA;LRS;RED


"""

#import matplotlib
#matplotlib.use('Agg')
#matplotlib.rcParams.update({'font.size': 14})
#import matplotlib.pyplot as p
from sklearn.neighbors import BallTree

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import extinction

from astropy_healpix import healpy
import sys
import os
import time
from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column
from scipy.optimize import curve_fit
import astropy.io.fits as fits
import h5py
import numpy as n
print('CREATES FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

env =  sys.argv[1]  # 'UNIT_fA1i_DIR'
i0 = int( sys.argv[2] )
i1 = int( sys.argv[3] )
survey_list = sys.argv

if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

nl = lambda sel : len(sel.nonzero()[0])

root_dir = os.path.join(os.environ[env])

sub_survey_names = n.array([ 'BG', 'LRG', 'ELG', 'QSO', 'LyA', 'filament_GAL', 'cluster_BCG', 'cluster_redGAL', 'QHS01', 'CHANCES01'])

N_subsurvey = {'BG':1, 'filament_GAL':3, 'LRG':2, 'ELG':3, 'QSO':4, 'LyA':5, 'cluster_redGAL':2, 'cluster_BCG':1, 'QHS01':1, 'CHANCES01':1}
N_survey = {'BG':8, 'filament_GAL':5, 'LRG':8, 'ELG':8, 'QSO':8, 'LyA':8, 'cluster_redGAL':5, 'cluster_BCG':5, 'QHS01':18, 'CHANCES01':15}

#priority_values = {'BG':100, 'filament_GAL':80, 'LRG':99, 'ELG':80, 'QSO':97, 'LyA':98}

# reassigns templates correctly
z_all = n.hstack(( 0., n.arange(0.3, 3., 0.2), 3.5, 4.5, 6. ))
zmins = z_all[:-1]
zmaxs = z_all[1:]

dir_2_OUT = os.path.join(root_dir, "cat_SHAM_ALL")
dir_2_CAT = os.path.join(root_dir, "cat_QMOST_all")
HEALPIX_id = 64
def create_4most_columns(subsurvey , t_survey, HEALPIX_id=HEALPIX_id, option='K' , out_name='test.fits' ):
    N_obj = len(t_survey)
    if N_obj>0:
        print(len(t_survey), 'targets', len(t_survey)/53., 'targets/deg2')
        if option=='K':
            zmag = 8.9 - 2.5*n.log10(t_survey['flux_Zt'])
        if option=='C':
            zmag = t_survey['Z']
        t_out = Table()
        #  limit size of the string columns to the size of the longer string in the corresponding columns.
        # 'NAME':str, max 256 char
        N1 = n.arange(len(t_survey['ebv']))
        id_list = N_subsurvey[subsurvey]*1e10 + HEALPIX_id*1e6 +  N1
        NAME = n.array([ str(int(el)).zfill(11) for el in id_list ])
        t_out.add_column(Column(name='NAME', data=NAME, unit=''))
        # 'RA':n.float64, 1D
        # 'DEC':n.float64, 1D
        t_out.add_column(Column(name='RA', data=t_survey['RA'].astype('float64'), unit='deg'))
        t_out.add_column(Column(name='DEC', data=t_survey['DEC'].astype('float64'), unit='deg'))
        # 'PMRA':n.float32, 1E
        # 'PMDEC':n.float32, 1E
        # 'EPOCH':n.float32, 1E
        PMRA = n.zeros(N_obj)
        t_out.add_column(Column(name='PMRA', data=PMRA.astype('float32'), unit='mas/yr'))
        PMDEC = n.zeros(N_obj)
        t_out.add_column(Column(name='PMDEC', data=PMDEC.astype('float32'), unit='mas/yr'))
        EPOCH = n.ones(N_obj)*2015.5
        t_out.add_column(Column(name='EPOCH', data=EPOCH.astype('float32'), unit='yr'))
        # 'RESOLUTION':n.int16, 1I
        RESOLUTION = n.ones(N_obj).astype('int16')
        t_out.add_column(Column(name='RESOLUTION', data=RESOLUTION, unit=''))
        # 'SUBSURVEY':str, max 256 char
        SUBSURVEY = n.ones(N_obj).astype('str')
        SUBSURVEY[:] = subsurvey
        t_out.add_column(Column(name='SUBSURVEY', data=SUBSURVEY, unit=''))
        # EBV for templates
        ebv_1000 = (t_survey['ebv']*1000).astype('int')
        #print('EBV', n.min(ebv_1000), n.max(ebv_1000))
        ebv_1_0 = ( ebv_1000 > 1000 )
        ebv_0_5 = ( ebv_1000 > 500 ) & ( ebv_1000 <= 1000 )
        ebv_0_4 = ( ebv_1000 > 400 ) & ( ebv_1000 <= 500 )
        ebv_0_3 = ( ebv_1000 > 300 ) & ( ebv_1000 <= 400 )
        ebv_0_2 = ( ebv_1000 > 200 ) & ( ebv_1000 <= 300 )
        ebv_0_1 = ( ebv_1000 > 100 ) & ( ebv_1000 <= 200 )
        ebv_0_0 = ( ebv_1000 <= 100 )
        z_name = lambda z0, z1 : "_zmin_"+str(int(10*z0)).zfill(2)+"_zmax_"+str(int(10*z1)).zfill(2)
        # templates
        template_names = n.zeros(N_obj).astype('U100')
        ruleset_array = n.zeros(N_obj).astype('str')
        # S5
        if subsurvey == 'cluster_BCG' :
            ruleset_array[:] = "Red_SN02_Nov21"
            #ruleset_array[zmag<20.0][:] = "Red_SN03_Nov21"
            #ruleset_array[zmag<18.5][:] = "Red_SN05_Nov21"
            #ruleset_array[zmag<17.0][:] = "Red_SN10_Nov21"
            for z0,z1 in zip(zmins,zmaxs):
                zsel = (t_survey['redshift_R']>=z0) & (t_survey['redshift_R']<z1)
                if len(zsel.nonzero()[0])>0:
                    template_names[(zsel)]               = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'
                    template_names[(zsel)&(ebv_0_0)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'
                    template_names[(zsel)&(ebv_0_1)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_1.fits'
                    template_names[(zsel)&(ebv_0_2)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_2.fits'
                    template_names[(zsel)&(ebv_0_3)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_3.fits'
                    template_names[(zsel)&(ebv_0_4)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_4.fits'
                    template_names[(zsel)&(ebv_0_5)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_5.fits'
                    template_names[(zsel)&(ebv_1_0)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_1_0.fits'
        # S5
        elif subsurvey == 'cluster_redGAL' :
            ruleset_array[:] = "Red_SN02_Nov21"
            #ruleset_array[zmag<20.0][:] = "Red_SN03_Nov21"
            #ruleset_array[zmag<18.5][:] = "Red_SN05_Nov21"
            #ruleset_array[zmag<17.0][:] = "Red_SN10_Nov21"
            for z0,z1 in zip(zmins,zmaxs):
                zsel = (t_survey['redshift_R']>=z0) & (t_survey['redshift_R']<z1)
                if len(zsel.nonzero()[0])>0:
                    template_names[(zsel)]               = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'
                    template_names[(zsel)&(ebv_0_0)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'
                    template_names[(zsel)&(ebv_0_1)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_1.fits'
                    template_names[(zsel)&(ebv_0_2)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_2.fits'
                    template_names[(zsel)&(ebv_0_3)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_3.fits'
                    template_names[(zsel)&(ebv_0_4)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_4.fits'
                    template_names[(zsel)&(ebv_0_5)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_5.fits'
                    template_names[(zsel)&(ebv_1_0)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_1_0.fits'
        # S5
        elif subsurvey == 'filament_GAL' :
            ruleset_array[:] = "Red_SN02_Nov21"
            #ruleset_array[zmag<20.0][:] = "Red_SN03_Nov21"
            #ruleset_array[zmag<18.5][:] = "Red_SN05_Nov21"
            #ruleset_array[zmag<17.0][:] = "Red_SN10_Nov21"
            for z0,z1 in zip(zmins,zmaxs):
                zsel = (t_survey['redshift_R']>=z0) & (t_survey['redshift_R']<z1) & (t_survey['is_quiescent'])
                if len(zsel.nonzero()[0])>0:
                    template_names[(zsel)]               = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'
                    template_names[(zsel)&(ebv_0_0)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'
                    template_names[(zsel)&(ebv_0_1)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_1.fits'
                    template_names[(zsel)&(ebv_0_2)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_2.fits'
                    template_names[(zsel)&(ebv_0_3)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_3.fits'
                    template_names[(zsel)&(ebv_0_4)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_4.fits'
                    template_names[(zsel)&(ebv_0_5)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_5.fits'
                    template_names[(zsel)&(ebv_1_0)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_1_0.fits'
                zsel = (t_survey['redshift_R']>=z0)&(t_survey['redshift_R']<z1) & (t_survey['is_quiescent']==False)
                if len(zsel.nonzero()[0])>0:
                    template_names[(zsel)]               = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_0_01.fits'
                    template_names[(zsel)&(ebv_0_0)]     = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_0_01.fits'
                    template_names[(zsel)&(ebv_0_1)]     = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_0_1.fits'
                    template_names[(zsel)&(ebv_0_2)]     = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_0_2.fits'
                    template_names[(zsel)&(ebv_0_3)]     = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_0_3.fits'
                    template_names[(zsel)&(ebv_0_4)]     = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_0_4.fits'
                    template_names[(zsel)&(ebv_0_5)]     = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_0_5.fits'
                    template_names[(zsel)&(ebv_1_0)]     = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_1_0.fits'

        # S8 BG or LRG
        elif subsurvey == 'BG' or subsurvey == 'LRG':
            ruleset_array[:] = "COSMO_RedGAL"
            for z0,z1 in zip(zmins,zmaxs):
                zsel = (t_survey['redshift_R']>=z0) & (t_survey['redshift_R']<z1)
                if len(zsel.nonzero()[0])>0:
                    #ruleset_array[zsel] = "COSMO_RedGAL"
                    template_names[(zsel)]               = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'
                    template_names[(zsel)&(ebv_0_0)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'
                    template_names[(zsel)&(ebv_0_1)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_1.fits'
                    template_names[(zsel)&(ebv_0_2)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_2.fits'
                    template_names[(zsel)&(ebv_0_3)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_3.fits'
                    template_names[(zsel)&(ebv_0_4)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_4.fits'
                    template_names[(zsel)&(ebv_0_5)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_5.fits'
                    template_names[(zsel)&(ebv_1_0)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_1_0.fits'

        else :
            ruleset_array[:] = "rulesetName"
            for z0,z1 in zip(zmins,zmaxs):
                zsel = (t_survey['redshift_R']>=z0) & (t_survey['redshift_R']<z1) & (t_survey['is_quiescent'])
                if len(zsel.nonzero()[0])>0:
                    template_names[(zsel)]               = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'
                    template_names[(zsel)&(ebv_0_0)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'
                    template_names[(zsel)&(ebv_0_1)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_1.fits'
                    template_names[(zsel)&(ebv_0_2)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_2.fits'
                    template_names[(zsel)&(ebv_0_3)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_3.fits'
                    template_names[(zsel)&(ebv_0_4)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_4.fits'
                    template_names[(zsel)&(ebv_0_5)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_0_5.fits'
                    template_names[(zsel)&(ebv_1_0)]     = "Qmost_"+'LRG'+z_name( z0, z1)+'_EBV_1_0.fits'
                zsel = (t_survey['redshift_R']>=z0)&(t_survey['redshift_R']<z1) & (t_survey['is_quiescent']==False)
                if len(zsel.nonzero()[0])>0:
                    template_names[(zsel)]               = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_0_01.fits'
                    template_names[(zsel)&(ebv_0_0)]     = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_0_01.fits'
                    template_names[(zsel)&(ebv_0_1)]     = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_0_1.fits'
                    template_names[(zsel)&(ebv_0_2)]     = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_0_2.fits'
                    template_names[(zsel)&(ebv_0_3)]     = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_0_3.fits'
                    template_names[(zsel)&(ebv_0_4)]     = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_0_4.fits'
                    template_names[(zsel)&(ebv_0_5)]     = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_0_5.fits'
                    template_names[(zsel)&(ebv_1_0)]     = "Qmost_"+'ELG'+z_name( z0, z1)+'_EBV_1_0.fits'

        # 'TEMPLATE':str, max 256 char
        t_out.add_column(Column(name='TEMPLATE', data=template_names, unit=''))
        # 'RULESET':str, max 256 char
        t_out.add_column(Column(name='RULESET', data=ruleset_array, unit=''))
        # 'REDSHIFT_ESTIMATE':n.float32, 1E
        # 'REDSHIFT_ERROR':n.float32, 1E
        t_out.add_column(Column(name='REDSHIFT_ESTIMATE', data=t_survey['redshift_R'].astype('float32'), unit=''))
        t_out.add_column(Column(name='REDSHIFT_ERROR', data=(0.5*n.ones(N_obj)).astype('float32'), unit=''))
        # 'MAG':n.float32,
        # 'MAG_ERR':n.float32
        # 'MAG_TYPE': str max 256 char
        # Extinction on the z band
        r_v=3.1
        a_v = t_survey['ebv'] * r_v
        delta_mag = n.hstack(( n.array([ extinction.fitzpatrick99(n.array([9000.]), el, r_v=3.1, unit='aa') for el in a_v ]) ))
        #print(n.median(t_survey['ebv']), n.median(delta_mag))
        #rv = av/ebv
        #av = rv x ebv
        extincted_mag = zmag + delta_mag
        t_out.add_column(Column(name='MAG', data=extincted_mag.astype('float32'), unit='mag'))
        t_out.add_column(Column(name='MAG_ERR', data = (0.01 * n.ones(N_obj)).astype('float32'), unit='mag'))
        MAG_TYPE = n.ones(N_obj).astype('str')
        MAG_TYPE[:] = 'DECam_z_AB'
        t_out.add_column(Column(name='MAG_TYPE', data=MAG_TYPE, unit=''))
        # 'REDDENING':n.float32, 1E
        t_out.add_column(Column(name='REDDENING',data=t_survey['ebv'].astype('float32'), unit=''))
        # 'DATE_EARLIEST':n.float64, JulianDate decimal days # 01-Nov-2022
        # 'DATE_LATEST':n.float64, JulianDate decimal days # 02-Feb-2033
        t_out.add_column(Column(name='DATE_EARLIEST',data = n.zeros(N_obj).astype('float64'), unit='d'))
        t_out.add_column(Column(name='DATE_LATEST'  ,data = n.zeros(N_obj).astype('float64'), unit='d'))
        CADENCE = n.zeros(N_obj).astype('int64')
        t_out.add_column(Column(name='CADENCE', data=CADENCE, unit=''))
        # extent flags and parameters
        # 'EXTENT_FLAG': 1I
        # =1
        # 'EXTENT_PARAMETER': 1E
        # =0
        # 'EXTENT_INDEX': 1E
        # =0
        t_out.add_column(Column(name='EXTENT_FLAG'     , data=n.ones(N_obj).astype('int16') , unit=''))
        t_out.add_column(Column(name='EXTENT_PARAMETER', data=n.zeros(N_obj).astype('float32') , unit='arcsec'))
        t_out.add_column(Column(name='EXTENT_INDEX'    , data=n.zeros(N_obj).astype('float32') , unit=''))
        #print('assigns magnitudes and sersic types')
        sel_PSF = (t_survey['SERSIC'] == 0)
        t_out['EXTENT_FLAG'][sel_PSF] = 0
        t_out['EXTENT_FLAG'][sel_PSF==False] = 2
        #
        t_out['EXTENT_PARAMETER'][sel_PSF] = 0
        t_out['EXTENT_PARAMETER'][sel_PSF==False] = t_survey['SHAPE_R'][sel_PSF==False]
        #
        #print('min extent param', t_out['EXTENT_PARAMETER'][sel_PSF == False].min() )
        #print('max extent param', t_out['EXTENT_PARAMETER'][sel_PSF == False].max() )
        t_out['EXTENT_PARAMETER'] [ ( t_out['EXTENT_PARAMETER']<0.1 ) & (sel_PSF == False ) ] = 0.101
        t_out['EXTENT_PARAMETER'] [ ( t_out['EXTENT_PARAMETER']>99.9 ) & (sel_PSF == False ) ] = 99.9
        #
        t_out['EXTENT_INDEX'][sel_PSF] = 0
        t_out['EXTENT_INDEX'][sel_PSF==False] = t_survey['SERSIC'][sel_PSF==False]
        #print('min index', t_out['EXTENT_INDEX'][sel_PSF == False].min() )
        t_out.write(out_name, overwrite = True)




for HEALPIX_id in n.arange(768)[i0:i1]:
    print(HEALPIX_id, time.time()-t0)
    path_2_out_Kin = os.path.join(dir_2_OUT, 'Kmatch_'  + str(HEALPIX_id).zfill(6) + '.fit')
    if 'is_S6AGN' in fits.open(path_2_out_Kin)[1].data.columns.names :
        t_K = Table.read(path_2_out_Kin)
        not_AGN_K = ( t_K['is_S6AGN'] == False ) & ( t_K['is_S8QSO'] == False ) & (t_K['RA']<359.999) & (t_K['DEC']>-79.99) & (t_K['DEC']<19.99) & (abs(t_K['g_lat'])>5) & (t_K['ebv']<1)
        t_K = t_K[not_AGN_K]

        mass = t_K['galaxy_SMHMR_mass']
        log_sfr = t_K['galaxy_star_formation_rate']
        log_ssfr = log_sfr - mass
        t_K['is_quiescent'] = (log_ssfr < -10.3)
        t_K['is_quiescent'][t_K['is_S5BCG']][:] = True
        t_K['is_quiescent'][t_K['is_S5GAL']][:] = True

        if 'S5' in survey_list:
            eRO_footprint = ( t_K['DEC'] > -80. ) & ( t_K['DEC'] < 5. ) & ( abs( t_K['g_lon'] ) > 180. ) & ( abs( t_K['g_lat'] ) > 10. )
            # S5 BCG
            path_2_out_K = os.path.join(dir_2_CAT, 'is_S501_K_'  + str(HEALPIX_id).zfill(6) + '.fit')
            create_4most_columns(subsurvey='cluster_BCG' , t_survey = t_K[(t_K['is_S5BCG'] & eRO_footprint)], HEALPIX_id=HEALPIX_id, option='K', out_name = path_2_out_K  )
            print(path_2_out_K)

            # S5 RED GAL
            path_2_out_K = os.path.join(dir_2_CAT, 'is_S502_K_'  + str(HEALPIX_id).zfill(6) + '.fit')
            create_4most_columns(subsurvey='cluster_redGAL' , t_survey = t_K[(t_K['is_S5GAL'] & eRO_footprint )], HEALPIX_id=HEALPIX_id, option='K', out_name = path_2_out_K   )
            print(path_2_out_K)

            # S5 FILAMENT
            zmag = 8.9 - 2.5*n.log10(t_K['flux_Zt'])
            s0503 =  (zmag < 17.5)
            print(len(t_K[s0503])/53., 's0503 per deg2, z<0.5')
            path_2_out_K = os.path.join(dir_2_CAT, 'is_S503_K_'  + str(HEALPIX_id).zfill(6) + '.fit')
            create_4most_columns(subsurvey='filament_GAL' , t_survey = t_K[(s0503 & eRO_footprint )], HEALPIX_id=HEALPIX_id, option='K', out_name = path_2_out_K   )
            print(path_2_out_K)

        if 'S8' in survey_list:
            # S801 BG
            #jmag = 8.9 - 2.5*n.log10(t_K['flux_Jt'])
            #kmag = 8.9 - 2.5*n.log10(t_K['flux_Kt'])
            #w1mag = 8.9 - 2.5*n.log10(t_K['flux_W1t'])
            #x = jmag - kmag
            #y = jmag - w1mag
            #magmin,magmax = 16.3,18.14
            #s0801 = (jmag>magmin) & (jmag<magmax) & (y>1.6*x-1.6) &  (y<2.0*x-1) & (y>-2.8*x+0.45) & (y<-0.5*x+0.1)
            path_2_out_K = os.path.join(dir_2_CAT, 'is_S801_K_'  + str(HEALPIX_id).zfill(6) + '.fit')
            #create_4most_columns(subsurvey='BG' , t_survey = t_K[s0801], HEALPIX_id=HEALPIX_id, option='K', out_name = path_2_out_K    )
            #print(path_2_out_K)

            ## S802 LRG
            #jmag = 8.9 - 2.5*n.log10(t_K['flux_Jt'])
            #kmag = 8.9 - 2.5*n.log10(t_K['flux_Kt'])
            #w1mag = 8.9 - 2.5*n.log10(t_K['flux_W1t'])
            #x = jmag - kmag
            #y = jmag - w1mag
            #magmin,magmax = 18.0,19.70
            #s0801 = (jmag>magmin) & (jmag<magmax) & (y>0.5*x-0.05) & (y<4.1*x-1.95)& (y<1.5*x - 0.45) &  (y>4.1*x-2.70) & (y < 0.55)
            #path_2_out_K = os.path.join(dir_2_CAT, 'is_S802_K_'  + str(HEALPIX_id).zfill(6) + '.fit')
            #create_4most_columns(subsurvey='LRG' , t_survey = t_K[s0801], HEALPIX_id=HEALPIX_id, option='K', out_name = path_2_out_K    )
            print(path_2_out_K)

        if 'S15' in survey_list:
            # S15  CHANCES
            path_2_out_K = os.path.join(dir_2_CAT, 'is_S15_K_'  + str(HEALPIX_id).zfill(6) + '.fit')
            create_4most_columns(subsurvey='CHANCES01' , t_survey = t_K[t_K['is_CHANCES']], HEALPIX_id=HEALPIX_id, option='K', out_name = path_2_out_K    )
            print(path_2_out_K)

        if 'S18' in survey_list:
            # S18
            jmag = 8.9 - 2.5*n.log10(t_K['flux_Jt'])
            kmag = 8.9 - 2.5*n.log10(t_K['flux_Kt'])
            x = jmag - kmag
            s0801 =  (jmag<18) & (x<0.45)
            path_2_out_K = os.path.join(dir_2_CAT, 'is_S18_K_'  + str(HEALPIX_id).zfill(6) + '.fit')
            create_4most_columns(subsurvey='QHS01' , t_survey = t_K[s0801], HEALPIX_id=HEALPIX_id, option='K', out_name = path_2_out_K    )
            print(path_2_out_K)

    path_2_out_Cin = os.path.join(dir_2_OUT, 'Kmatch_'  + str(HEALPIX_id).zfill(6) + '_C.fit')
    if 'is_S6AGN' in fits.open(path_2_out_Cin)[1].data.columns.names :
        t_C = Table.read(path_2_out_Cin)
        not_AGN_C = ( t_C['is_S6AGN'] == False ) & ( t_C['is_S8QSO'] == False )& (t_C['DEC']>-79.99) & (t_C['DEC']<19.99) & (abs(t_C['g_lat'])>10) & (t_C['ebv']<1)
        t_C = t_C[not_AGN_C]

        mass = t_C['galaxy_SMHMR_mass']
        log_sfr = t_C['galaxy_star_formation_rate']
        log_ssfr = log_sfr - mass
        t_C['is_quiescent'] = (log_ssfr < -10.3)
        t_C['is_quiescent'][t_C['is_S5BCG']][:] = True
        t_C['is_quiescent'][t_C['is_S5GAL']][:] = True

        if 'S5' in survey_list:
            eRO_footprint = ( t_C['DEC'] > -80. ) & ( t_C['DEC'] < 5. ) & ( abs( t_C['g_lon'] ) > 180. ) & ( abs( t_C['g_lat'] ) > 10. )
            # S5 BCG
            path_2_out_C = os.path.join(dir_2_CAT, 'is_S501_C_'  + str(HEALPIX_id).zfill(6) + '.fit')
            create_4most_columns(subsurvey='cluster_BCG' , t_survey = t_C[(t_C['is_S5BCG'] & eRO_footprint)], HEALPIX_id=HEALPIX_id, option='C', out_name = path_2_out_C  )
            print(path_2_out_C)

            # S5 RED GAL
            path_2_out_C = os.path.join(dir_2_CAT, 'is_S502_C_'  + str(HEALPIX_id).zfill(6) + '.fit')
            create_4most_columns(subsurvey='cluster_redGAL' , t_survey = t_C[(t_C['is_S5GAL'] & eRO_footprint )], HEALPIX_id=HEALPIX_id, option='C', out_name = path_2_out_C   )
            print(path_2_out_C)

            # S5 FILAMENT
            zmag = t_C['Z']
            s0503 =  (zmag < 17.5)
            print(len(t_C[s0503])/53., 's0503 per deg2, z<0.5')
            path_2_out_C = os.path.join(dir_2_CAT, 'is_S503_C_'  + str(HEALPIX_id).zfill(6) + '.fit')
            create_4most_columns(subsurvey='filament_GAL' , t_survey = t_C[(s0503 & eRO_footprint )], HEALPIX_id=HEALPIX_id, option='C', out_name = path_2_out_C   )
            print(path_2_out_C)

        if 'S8' in survey_list:
            # S801 BG
            #jmag = t_C['J']
            #kmag = t_C['K']
            #w1mag = t_C['I1']
            #x = jmag - kmag
            #y = jmag - w1mag
            #magmin, magmax = 16.22, 18.1
            #s0801 = (jmag>magmin) & (jmag<magmax) & (y>1.6*x-1.6) &  (y<2.0*x-1) & (y>-2.8*x+0.45) & (y<-0.5*x+0.1)
            #path_2_out_C = os.path.join(dir_2_CAT, 'is_S801_C_'  + str(HEALPIX_id).zfill(6) + '.fit')
            #create_4most_columns(subsurvey='BG' , t_survey = t_C[s0801], HEALPIX_id=HEALPIX_id, option='C', out_name = path_2_out_C  )
            #print(path_2_out_C)

            # S802 LRG
            jmag = t_C['J']
            kmag = t_C['K']
            w1mag = t_C['I1']
            x = jmag - kmag
            y = jmag - w1mag
            magmin,magmax = 18.0,19.70
            s0801 = (jmag>magmin) & (jmag<magmax) & (y>0.5*x-0.05) & (y<4.1*x-1.95)& (y<1.5*x - 0.45) &  (y>4.1*x-2.70) & (y < 0.55)
            path_2_out_C = os.path.join(dir_2_CAT, 'is_S802_C_'  + str(HEALPIX_id).zfill(6) + '.fit')
            create_4most_columns(subsurvey='LRG' , t_survey = t_C[s0801], HEALPIX_id=HEALPIX_id, option='C', out_name = path_2_out_C  )
            print(path_2_out_C)

        if 'S15' in survey_list:
            # S15  CHANCES
            path_2_out_C = os.path.join(dir_2_CAT, 'is_S15_C_'  + str(HEALPIX_id).zfill(6) + '.fit')
            create_4most_columns(subsurvey='CHANCES01' , t_survey = t_C[t_C['is_CHANCES']], HEALPIX_id=HEALPIX_id, option='C', out_name = path_2_out_C  )
            print(path_2_out_C)

        if 'S18' in survey_list:
            # S18
            jmag = t_C['J']
            kmag = t_C['K']
            x = jmag - kmag
            s0801 = (jmag<18) & (x<0.45)
            path_2_out_C = os.path.join(dir_2_CAT, 'is_S18_C_'  + str(HEALPIX_id).zfill(6) + '.fit')
            create_4most_columns(subsurvey='QHS01' , t_survey = t_C[s0801], HEALPIX_id=HEALPIX_id, option='C', out_name = path_2_out_C  )
            print(path_2_out_C)
















