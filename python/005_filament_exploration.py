"""
What it does
------------

Creates a fits catalog containing cosmology tracers BGx2, LRG, ELG, QSO, LyaQSO for each healpix 768 pixel of 13 deg2 (NSIDE=8)

Command to run
--------------

python3 005_0_sham_cosmology_catalogs.py environmentVAR 

environmentVAR: environment variable linking to the directory where files are e.g. MD10
It will then work in the directory : $environmentVAR/hlists/fits/

Dependencies
------------
topcat/stilts
import time, os, sys, numpy, scipy, astropy, h5py, astropy_healpix, matplotlib

"""
import time
t0 = time.time()

from astropy_healpix import healpy
import sys, os, json

from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u

import astropy.io.fits as fits
import numpy as n
print('CREATES FITS FILES with SHAM method')
print('------------------------------------------------')
print('------------------------------------------------')
nl = lambda selection : len(selection.nonzero()[0])

env ='MD10'
hdu = fits.open("/data37s/simulation_1/MD/MD_1.0Gpc/4most_s8_kidsdr4.lsdr9fibmag_4MOST_correct_output_ETC2020-10-16_14_18_45.fits")
ra = hdu[1].data['raj2000']
dec = hdu[1].data['decj2000']
rMAG = hdu[1].data['rtot']
AREA = 400

hdu = fits.open("/data/data/VIKING/VIKING/VIKING_eFEDS_isprimary.fits")
ra = hdu[1].data['RA2000']
dec = hdu[1].data['DEC2000']
kMAG = hdu[1].data['KSAPERMAG3']
zFIB = hdu[1].data['ZAPERMAG3']
gal_type = (hdu[1].data["PGALAXY"]>0.9)&(hdu[1].data['PRIMARY_SOURCE']==1)
AREA = 67.03

hdu = fits.open("/home/comparat/data/legacysurvey/dr8/sweep-000m005-010p000.fits")
ra = hdu[1].data['RA']
dec = hdu[1].data['DEC']
rMAG = 2.5*n.log10(hdu[1].data['FLUX_R']/hdu[1].data['MW_TRANSMISSION_R']) + 22.5
rFIB = 2.5*n.log10(hdu[1].data['FIBERFLUX_R']/hdu[1].data['MW_TRANSMISSION_R']) + 22.5
psf_type = (hdu[1].data["TYPE"]=="PSF")
AREA = 50

hdu = fits.open("/data/data/GAMA/ApMatchedCat.fits")
ra = hdu[1].data['RA']
dec = hdu[1].data['DEC']
kMAG = hdu[1].data['MAG_AUTO_K']
rMAG = hdu[1].data['MAG_AUTO_r']
rFIB = 22.5 - 2.5*n.log10(hdu[1].data['FIBERFLUX_R']/hdu[1].data['MW_TRANSMISSION_R']) 

AREA = 180.

# determine K mag klimit : 17 => ~250 per deg2
for Mmax in n.arange(16.8,19.1,0.1):
	print(n.round([Mmax,nl((kMAG<Mmax))/AREA],1))

# determine R fiber mag limit : 20 => ~150 per deg2

hdu = fits.open("/data44s/eroAGN_WG_DATA/DATA/photometry/catalogs/GAMA/ApMatchedCat_LSDR8.fits")
ra = hdu[1].data['RA_1']
dec = hdu[1].data['DEC_1']
kMAG = hdu[1].data['MAG_AUTO_K']
rMAG = hdu[1].data['MAG_AUTO_r']
rFIB = 22.5 - 2.5*n.log10(hdu[1].data['FIBERFLUX_R']/hdu[1].data['MW_TRANSMISSION_R']) 
rMG = 22.5 - 2.5*n.log10(hdu[1].data['FLUX_R']/hdu[1].data['MW_TRANSMISSION_R']) 
AREA = 20.
selection = (ra>130) & (ra<140) & (dec>-2) & (dec<0) & (n.isnan(hdu[1].data['FIBERFLUX_R'])==False) & (hdu[1].data['FIBERFLUX_R']>0)
n.median(rMG[selection]-rMAG[selection])
for Mmax in n.arange(19.,23.1,0.1):
	print(n.round( [Mmax, nl((kMAG<17)&(rFIB>12)&(rFIB<Mmax)&(selection)) / nl((kMAG<17)&(selection)) ], 1) )

