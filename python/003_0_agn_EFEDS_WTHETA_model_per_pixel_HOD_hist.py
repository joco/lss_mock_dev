
import sys, os, time
import extinction
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import astropy.constants as cc
import astropy.io.fits as fits
from astropy.table import Table, Column
from scipy.special import erf
from scipy.stats import norm
from scipy.interpolate import interp2d
from scipy.interpolate import interp1d
import healpy
#import h5py
import numpy as n
print('Creates AGN mock catalogue ')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

env = sys.argv[1] # 'MD10'
f_sat_pc = int(sys.argv[2]) # 10 # percent
f_sat = f_sat_pc / 100.
scatter_0 = float(sys.argv[3]) # 1.4
closePairs = int(sys.argv[4])
#PIX_ID = str(0) # sys.argv[4]
log10_FX_lim = float(sys.argv[5])
PerCentInDenseEnv = float(sys.argv[6])
tmm_min  = float(sys.argv[7])
tmm_max  = float(sys.argv[8])
xoff_min = float(sys.argv[9])
xoff_max = float(sys.argv[10])

print(env, 'f sat', f_sat, 'scatter', scatter_0, log10_FX_lim, closePairs)
simput_dir = os.path.join(os.environ[env], 'WTH_fsat_'+str(f_sat_pc)+'_sigma_'+str(scatter_0))
if closePairs==1:
	simput_dir = os.path.join(os.environ[env], 'CP_'+str(f_sat_pc)+'_sigma_'+str(scatter_0)+'_frac_'+str(PerCentInDenseEnv)) +'_'+str(tmm_min ) + '_tmm_' + str( tmm_max  )   +'_'+ str( xoff_min ) + '_xoff_' +str( xoff_max )
if os.path.isdir(simput_dir) == False:
	os.system('mkdir -p ' + simput_dir)
hist_dir = os.path.join( simput_dir, 'HIST')
if os.path.isdir(hist_dir) == False:
	os.system('mkdir -p ' + hist_dir)

if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

LXmins = [42, 43, 44]
ZZmins = [0.2, 0.4, 1.1]
for PIX_ID in n.arange(healpy.nside2npix(8)):
	path_2_agnOut_file = os.path.join( simput_dir, str(PIX_ID).zfill(6)+'.fit')
	print('opens AGN file ', path_2_agnOut_file, time.time() - t0)
	f1 = Table.read( path_2_agnOut_file )
	zz_1 = f1['redshift_R']
	lx_1 = f1['LX_hard']
	for LXmin, ZZmin in zip(LXmins, ZZmins):
		# histogram2d of redshift and mass
		path_2_HistOut_file = os.path.join( hist_dir, 'Hist_Mvir_zz_'+str(PIX_ID).zfill(6)+'_LX_'+str(LXmin)+'_Z_'+str(ZZmin)+'.ascii')
		path_2_HistCEN_file = os.path.join( hist_dir, 'HistCen_Mvir_zz_'+str(PIX_ID).zfill(6)+'_LX_'+str(LXmin)+'_Z_'+str(ZZmin)+'.ascii')
		print(path_2_HistOut_file)
		s1 = ( lx_1 >= LXmin ) & ( zz_1 <= ZZmin )
		# full population bins=[n.arange(11.4, 15.5, 0.1), n.arange(0,6.2, 0.1)]
		H_2d = n.histogram2d( f1['CENTRAL_Mvir'][s1], f1['redshift_R'][s1], bins=[n.arange(11.4, 15.5, 0.1), n.arange(0,6.2, 0.1)])[0]
		n.savetxt(path_2_HistOut_file, H_2d)
		print(f1['CENTRAL_Mvir'][s1])
		print(H_2d)
		cen_agn=(f1['HALO_pid']==-1)&(s1)
		H_2C = n.histogram2d( f1['CENTRAL_Mvir'][cen_agn], f1['redshift_R'][cen_agn], bins=[n.arange(11.4, 15.5, 0.1), n.arange(0,6.2, 0.1)])[0]
		n.savetxt(path_2_HistCEN_file, H_2C)
		print(path_2_HistCEN_file)
		print(f1['CENTRAL_Mvir'][cen_agn])
		print(H_2C)
		os.system('chgrp erosim '+path_2_HistOut_file)
		os.system('chgrp erosim '+path_2_HistCEN_file)
