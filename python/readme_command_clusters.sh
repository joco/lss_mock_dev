#############################################
#############################################
#############################################
#
# CLUSTER model
#
#############################################
#############################################
#############################################

rm $MD04/fits/*_CLU.fits
rm $MD04/fits/*_galaxiesAroundClusters.fit
rm $MD04/MD04_eRO_CLU.fit
rm $MD04/MD04_eRO_CLU_GAL.fit
rm $MD04/cat_CLU_SIMPUT/*.fit
rm $MD04/cat_eRO_CLU/*.fit
mv $MD04/cat_CLU_SIMPUT/cluster_images $MD04/cat_CLU_SIMPUT/cluster_images_bad
mkdir $MD04/cat_CLU_SIMPUT/cluster_images
rm -rf $MD04/cat_CLU_SIMPUT/cluster_images_bad
cp -r $MD10/cat_CLU_SIMPUT/cluster_Xspectra $MD04/cat_CLU_SIMPUT/

rm $MD10/fits/*_CLU.fits
rm $MD10/fits/*_galaxiesAroundClusters.fit
rm $MD10/MD10_eRO_CLU.fit
rm $MD10/MD10_eRO_CLU_GAL.fit
rm $MD10/cat_CLU_SIMPUT/*.fit
rm $MD10/cat_eRO_CLU/*.fit
# mv $MD10/cat_CLU_SIMPUT/cluster_images $MD10/cat_CLU_SIMPUT/cluster_images_bad
# rm -rf $MD10/cat_CLU_SIMPUT/cluster_images_bad
mkdir -p $MD10/cat_CLU_SIMPUT/cluster_images
mkdir -p $MD10/cat_eRO_CLU/
mkdir -p $MD10/cat_eRO_CLU-1Count
cp -r $MD10/cat_CLU_SIMPUT_eFEDs/cluster_Xspectra $MD10/cat_CLU_SIMPUT/

rm $MD40/fits/*_CLU.fits
rm $MD40/fits/*_galaxiesAroundClusters.fit
rm $MD40/MD40_eRO_CLU.fit
rm $MD40/MD40_eRO_CLU_GAL.fit
rm $MD40/cat_CLU_SIMPUT/*.fit
rm $MD40/cat_eRO_CLU/*.fit
mv $MD40/cat_CLU_SIMPUT/cluster_images $MD40/cat_CLU_SIMPUT/cluster_images_bad
mkdir $MD40/cat_CLU_SIMPUT/cluster_images
rm -rf $MD40/cat_CLU_SIMPUT/cluster_images_bad

rm $UNIT_fA1_DIR/fits/*_CLU.fits
rm $UNIT_fA1_DIR/fits/*_galaxiesAroundClusters.fit
rm $UNIT_fA1_DIR/UNIT_fA1_DIR_eRO_CLU.fit
rm $UNIT_fA1_DIR/UNIT_fA1_DIR_eRO_CLU_GAL.fit
rm $UNIT_fA1_DIR/cat_CLU_SIMPUT/*.fit
rm $UNIT_fA1_DIR/cat_eRO_CLU/*.fit
mv $UNIT_fA1_DIR/cat_CLU_SIMPUT/cluster_images $UNIT_fA1_DIR/cat_CLU_SIMPUT/cluster_images_bad
mkdir $UNIT_fA1_DIR/cat_CLU_SIMPUT/cluster_images
rm -rf $UNIT_fA1_DIR/cat_CLU_SIMPUT/cluster_images_bad

rm $UNIT_fA2_DIR/fits/*_CLU.fits
rm $UNIT_fA2_DIR/fits/*_galaxiesAroundClusters.fit
rm $UNIT_fA2_DIR/UNIT_fA2_DIR_eRO_CLU.fit
rm $UNIT_fA2_DIR/UNIT_fA2_DIR_eRO_CLU_GAL.fit
rm $UNIT_fA2_DIR/cat_CLU_SIMPUT/*.fit
rm $UNIT_fA2_DIR/cat_eRO_CLU/*.fit
mv $UNIT_fA2_DIR/cat_CLU_SIMPUT/cluster_images $UNIT_fA2_DIR/cat_CLU_SIMPUT/cluster_images_bad
mkdir $UNIT_fA2_DIR/cat_CLU_SIMPUT/cluster_images
rm -rf $UNIT_fA2_DIR/cat_CLU_SIMPUT/cluster_images_bad

rm $UNIT_fA1i_DIR/fits/*_CLU.fits
rm $UNIT_fA1i_DIR/fits/*_galaxiesAroundClusters.fit
rm $UNIT_fA1i_DIR/UNIT_fA1i_DIR_eRO_CLU.fit
rm $UNIT_fA1i_DIR/UNIT_fA1i_DIR_eRO_CLU_GAL.fit
rm $UNIT_fA1i_DIR/cat_CLU_SIMPUT/*.fit
rm $UNIT_fA1i_DIR/cat_eRO_CLU/*.fit
mv $UNIT_fA1i_DIR/cat_CLU_SIMPUT/cluster_images $UNIT_fA1i_DIR/cat_CLU_SIMPUT/cluster_images_bad
mkdir $UNIT_fA1i_DIR/cat_CLU_SIMPUT/cluster_images
rm -rf $UNIT_fA1i_DIR/cat_CLU_SIMPUT/cluster_images_bad

rm -rf /data40s/erosim/eRASS/eRASS8_agn_MD10
rm -rf /data40s/erosim/eRASS/eRASS8_cluster_MD10

# computes cluster properties up to z=0.4 for MD04 and 1.2 (aexp=0.45) for the other simulations
# writes SIMPUT images (option yes, no)
# script: 004_0_cluster.py
# inputs: ${env}/fits/all_?.?????.fits, ${env}/fits/all_?.?????_coordinates.fits, ${env}/fits/all_?.?????_galaxy.fits
# outputs: ${env}/fits/all_?.?????_CLU.fits
cd $GIT_AGN_MOCK/python/

# clean up commands :
# mv $MD10/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0/cluster_images $MD10/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0/stuff
# mv $MD10/cat_CLU_SIMPUT_b6_CM_0_pixS_20.0/cluster_images    $MD10/cat_CLU_SIMPUT_b6_CM_0_pixS_20.0/stuff
# mv $MD10/cat_CLU_SIMPUT_b7_CM_0_pixS_2.0/cluster_images      $MD10/cat_CLU_SIMPUT_b7_CM_0_pixS_2.0/stuff
# mv $MD10/cat_CLU_SIMPUT_b7_CM_0_pixS_20.0/cluster_images    $MD10/cat_CLU_SIMPUT_b7_CM_0_pixS_20.0/stuff
# mv $MD10/cat_CLU_SIMPUT_b8_CM_0_pixS_2.0/cluster_images      $MD10/cat_CLU_SIMPUT_b8_CM_0_pixS_2.0/stuff
# mv $MD10/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/cluster_images    $MD10/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/stuff
# mv $MD10/cat_CLU_SIMPUT_b9_CM_0_pixS_2.0/cluster_images      $MD10/cat_CLU_SIMPUT_b9_CM_0_pixS_2.0/stuff
# mv $MD10/cat_CLU_SIMPUT_b9_CM_0_pixS_20.0/cluster_images    $MD10/cat_CLU_SIMPUT_b9_CM_0_pixS_20.0/stuff
# 
# rm -rf $MD10/cat_CLU_SIMPUT_b*_CM_0_pixS_*/stuff

# mv $MD40/cat_CLU_SIMPUT_b7_CM_0_pixS_2.0/cluster_images     $MD40/cat_CLU_SIMPUT_b7_CM_0_pixS_2.0/stuff
# mv $MD40/cat_CLU_SIMPUT_b7_CM_0_pixS_20.0/cluster_images    $MD40/cat_CLU_SIMPUT_b7_CM_0_pixS_20.0/stuff
# mv $MD40/cat_CLU_SIMPUT_b8_CM_0_pixS_2.0/cluster_images     $MD40/cat_CLU_SIMPUT_b8_CM_0_pixS_2.0/stuff
# mv $MD40/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/cluster_images    $MD40/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/stuff
# mv $MD40/cat_CLU_SIMPUT_b9_CM_0_pixS_2.0/cluster_images     $MD40/cat_CLU_SIMPUT_b9_CM_0_pixS_2.0/stuff
# mv $MD40/cat_CLU_SIMPUT_b9_CM_0_pixS_20.0/cluster_images    $MD40/cat_CLU_SIMPUT_b9_CM_0_pixS_20.0/stuff
# 
# rm -rf $MD40/cat_CLU_SIMPUT_b*_CM_0_pixS_*/stuff
# 
# mv $MD04/cat_CLU_SIMPUT_b7_CM_0_pixS_20.0/cluster_images    $MD04/cat_CLU_SIMPUT_b7_CM_0_pixS_20.0/stuff
# mv $MD04/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/cluster_images    $MD04/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/stuff
# mv $MD04/cat_CLU_SIMPUT_b9_CM_0_pixS_20.0/cluster_images    $MD04/cat_CLU_SIMPUT_b9_CM_0_pixS_20.0/stuff
# 
# rm -rf $MD04/cat_CLU_SIMPUT_b*_CM_0_pixS_*/stuff

# nohup python run_md_cluster.py MD10 yes 20. 1.0 0 2.0 >    logs/cluster_run_MD10_yes_20_10_0.log &                    
# nohup python run_md_cluster.py MD10 yes 20. 0.9 0 2.0 >    logs/cluster_run_MD10_yes_20_09_0.log &                    
# nohup python run_md_cluster.py MD10 yes 20. 0.8 0 2.0 >    logs/cluster_run_MD10_yes_20_08_0.log &                    
# nohup python run_md_cluster.py MD10 yes 20. 0.7 0 2.0 >    logs/cluster_run_MD10_yes_20_07_0.log &                    
# nohup python run_md_cluster.py MD10 yes 20. 0.6 0 2.0 >    logs/cluster_run_MD10_yes_20_06_0.log &                    
# 
# nohup python run_md_cluster.py UNIT_fA1_DIR  yes 20. 1.0 0 2.0 >    logs/cluster_run_UNIT_fA1_DIR_yes_20_10_0.log &                    
# nohup python run_md_cluster.py UNIT_fA1i_DIR yes 20. 1.0 0 2.0 >    logs/cluster_run_UNIT_fA1_DIR_yes_20_10_0.log &                    
# 
# nohup python run_md_cluster.py UNIT_fA1_DIR yes 20. 0.8 0 2.0 >    logs/cluster_run_UNIT_fA1_DIR_yes_20_08_0.log &                    
# nohup python run_md_cluster.py UNIT_fA1i_DIR yes 20. 0.8 0 2.0 >    logs/cluster_run_UNIT_fA1_DIR_yes_20_08_0.log &                    


# nohup python run_md_cluster.py MD10 yes 20. 0.8 m20 2.0 >  logs/cluster_run_MD10_yes_20_08_m20.log &                    
# nohup python run_md_cluster.py MD10 yes 20. 0.8 p20 2.0 >  logs/cluster_run_MD10_yes_20_08_p20.log &                    
# nohup python run_md_cluster.py MD10 yes 20. 0.8 m40 2.0 >  logs/cluster_run_MD10_yes_20_08_m40.log &                    
# nohup python run_md_cluster.py MD10 yes 20. 0.8 p40 2.0 >  logs/cluster_run_MD10_yes_20_08_p40.log &                    

# nohup python run_md_cluster.py MD10 yes 2.  0.9 0 4.0 >    logs/cluster_run_MD10_yes_2_09_0.log &                    
# nohup python run_md_cluster.py MD10 yes 2.  0.8 0 4.0 >    logs/cluster_run_MD10_yes_2_08_0.log &                    
# nohup python run_md_cluster.py MD10 yes 2.  0.7 0 4.0 >    logs/cluster_run_MD10_yes_2_07_0.log &                    
# 
# nohup python run_md_cluster.py MD40 yes 2.  0.9 0 4.0 >    logs/cluster_run_MD40_yes_2_09_0.log &                    
# nohup python run_md_cluster.py MD40 yes 2.  0.8 0 4.0 >    logs/cluster_run_MD40_yes_2_08_0.log &                    
# nohup python run_md_cluster.py MD40 yes 2.  0.7 0 4.0 >    logs/cluster_run_MD40_yes_2_07_0.log &                    
# 
# nohup python run_md_cluster.py MD40 yes 20.  0.9 0 4.0 >   logs/cluster_run_MD40_yes_20_09_0.log &                    
# nohup python run_md_cluster.py MD40 yes 20.  0.8 0 4.0 >   logs/cluster_run_MD40_yes_20_08_0.log &                    
# nohup python run_md_cluster.py MD40 yes 20.  0.7 0 4.0 >   logs/cluster_run_MD40_yes_20_07_0.log &                    
# 
# nohup python run_md_cluster.py MD04 yes 20.  0.9 0 0.4 >   logs/cluster_run_MD04_yes_2_09_0.log &                  
# nohup python run_md_cluster.py MD04 yes 20.  0.8 0 0.4 >   logs/cluster_run_MD04_yes_2_08_0.log &                  
# nohup python run_md_cluster.py MD04 yes 20.  0.7 0 0.4 >   logs/cluster_run_MD04_yes_2_07_0.log &                  

nohup python run_md_cluster.py UNIT_fA1i_DIR yes  2. 0.8 0 4.0 13.0 >    logs/cluster_run_UNIT_fA1i_DIR_yes__2_08_0_4_13.log &                    
nohup python run_md_cluster.py UNIT_fA1i_DIR yes 20. 0.8 0 1.2 13.0 >    logs/cluster_run_UNIT_fA1i_DIR_yes_20_08_0_1_13.log &                    

nohup python run_md_cluster.py UNIT_fA1i_DIR yes 20. 0.8 0 1.5 13.0 -14.5  >    logs/cluster_run_UNIT_fA1i_DIR_yes_20_08_0_1_13_15.log &
nohup python run_md_cluster.py MD40 no 20. 0.8 0 1.5 13.0 -14.5  >    logs/cluster_run_UNIT_fA1i_DIR_yes_20_08_0_1_13_15.log &


nohup python 004_0_cluster.py MD40 all_0.97810.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.97810.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.95670.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.95670.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.93570.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.93570.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.91520.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.91520.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.89510.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.89510.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.87550.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.87550.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.85640.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.85640.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.83760.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.83760.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.81920.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.81920.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.80130.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.80130.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.78370.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.78370.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.76660.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.76660.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.74980.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.74980.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.73330.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.73330.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.71730.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.71730.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.70160.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.70160.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.68620.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.68620.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.67120.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.67120.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.65650.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.65650.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.64210.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.64210.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.62800.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.62800.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.61420.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.61420.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.60080.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.60080.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.58760.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.58760.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.57470.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.57470.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.56220.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.56220.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.54980.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.54980.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.53780.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.53780.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.52600.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.52600.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.51450.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.51450.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.50320.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.50320.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.49220.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.49220.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.48140.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.48140.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.47090.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.47090.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.46050.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.46050.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.45050.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.45050.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.44060.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.44060.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.43090.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.43090.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.42150.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.42150.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.41230.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.41230.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.40320.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.40320.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.39440.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.39440.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.38570.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.38570.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.37730.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.37730.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.36900.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.36900.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.36090.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.36090.fi_no_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py MD40 all_0.35300.fi no 20. 0.8 0 13.0 -14.5 > MD40_all_0.35300.fi_no_20._0.8_0_13.0.log &


nohup python 004_0_cluster_with_profile.py MD40 all_0.97810 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.97810.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.95670 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.95670.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.93570 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.93570.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.91520 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.91520.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.89510 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.89510.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.87550 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.87550.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.85640 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.85640.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.83760 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.83760.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.81920 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.81920.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.80130 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.80130.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.78370 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.78370.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.76660 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.76660.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.74980 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.74980.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.73330 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.73330.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.71730 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.71730.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.70160 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.70160.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.68620 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.68620.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.67120 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.67120.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.65650 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.65650.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.64210 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.64210.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.62800 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.62800.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.61420 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.61420.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.60080 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.60080.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.58760 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.58760.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.57470 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.57470.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.56220 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.56220.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.54980 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.54980.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.53780 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.53780.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.52600 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.52600.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.51450 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.51450.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.50320 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.50320.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.49220 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.49220.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.48140 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.48140.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.47090 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.47090.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.46050 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.46050.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.45050 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.45050.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.44060 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.44060.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.43090 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.43090.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.42150 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.42150.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.41230 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.41230.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.40320 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.40320.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.39440 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.39440.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.38570 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.38570.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.37730 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.37730.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.36900 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.36900.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.36090 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.36090.fi_yes_10._0.8_0_13.0.log &
nohup python 004_0_cluster_with_profile.py MD40 all_0.35300 yes 10. 0.8 0 13.0 -14.5 > MD40_profiles_all_0.35300.fi_yes_10._0.8_0_13.0.log &


ls $MD40/fits/all_0.?????_CLU_b8_CM_0_pixS_10.0_M500c_13.0_FX_-14.5_image_yes_withProfiles.fits > lists/md40_profiles.list
stilts tcat in=@lists/md40_profiles.list  ifmt=fits omode=out ofmt=fits out=$MD40/MD40_eRO_CLU_b8_CM_0_pixS_10.0_M500c_13.0_FX_-14.5_25Apr2022_Profiles.fits
cp $MD40/MD40_eRO_CLU_b8_CM_0_pixS_10.0_M500c_13.0_FX_-14.5_25Apr2022_Profiles.fits /data26s/comparat/simulations/erosim/MD40_eRO_CLU_b8_CM_0_pixS_10.0_M500c_13.0_FX_-14.5_25Apr2022_Profiles.fits
chgrp erosim /data26s/comparat/simulations/erosim/MD40_eRO_CLU_b8_CM_0_pixS_10.0_M500c_13.0_FX_-14.5_25Apr2022_Profiles.fits

topcat $MD40/MD40_eRO_CLU_b8_CM_0_pixS_10.0_M500c_13.0_FX_-14.5_25Apr2022_Profiles.fits

cd $GIT_AGN_MOCK/python

python 004_0_cluster.py MD40 all_0.97810 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.95670 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.93570 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.91520 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.89510 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.87550 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.85640 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.83760 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.81920 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.80130 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.78370 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.76660 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.74980 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.73330 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.71730 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.70160 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.68620 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.67120 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.65650 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.64210 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.62800 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.61420 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.60080 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.58760 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.57470 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.56220 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.54980 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.53780 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.52600 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.51450 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.50320 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.49220 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.48140 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.47090 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.46050 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.45050 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.44060 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.43090 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.42150 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.41230 no 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py MD40 all_0.40320 no 20. 0.8 0 13.0 -14.5
#python 004_0_cluster.py MD40 all_0.39440 no 20. 0.8 0 13.0 -14.5
#python 004_0_cluster.py MD40 all_0.38570 no 20. 0.8 0 13.0 -14.5
#python 004_0_cluster.py MD40 all_0.37730 no 20. 0.8 0 13.0 -14.5
#python 004_0_cluster.py MD40 all_0.36900 no 20. 0.8 0 13.0 -14.5
#python 004_0_cluster.py MD40 all_0.36090 no 20. 0.8 0 13.0 -14.5
#python 004_0_cluster.py MD40 all_0.35300 no 20. 0.8 0 13.0 -14.5

ls $MD40/fits/all_0.?????_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_image_no.fits > lists/md40_l1_2_mgas.list
stilts tcat in=@lists/md40_l1_2_mgas.list   ifmt=fits omode=out ofmt=fits out=$MD40/MD40_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS_Sept2021.fits


#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.97810 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.97810_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.95670 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.95670_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.93570 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.93570_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.91520 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.91520_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.89510 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.89510_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.87550 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.87550_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.85640 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.85640_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.83760 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.83760_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.81920 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.81920_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.80130 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.80130_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.78370 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.78370_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.76660 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.76660_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.74980 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.74980_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.73330 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.73330_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.71730 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.71730_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.70160 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.70160_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.68620 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.68620_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.67120 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.67120_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.65650 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.65650_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.64210 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.64210_yes_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.62800 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.62800_yes_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.61420 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.61420_yes_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.60080 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.60080_yes_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.58760 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.58760_yes_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.57470 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.57470_yes_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.56220 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.56220_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.54980 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.54980_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.53780 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.53780_yes_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.52600 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.52600_yes_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.51450 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.51450_yes_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.50320 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.50320_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.49220 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.49220_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.48140 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.48140_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.47090 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.47090_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.46050 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.46050_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.45050 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.45050_yes_20._0.8_0_13.0.log &
nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.44060 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.44060_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.43090 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.43090_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.42150 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.42150_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.41230 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.41230_yes_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.40320 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.40320_yes_20._0.8_0_13.0.log &

# in a screen
python 004_0_cluster.py UNIT_fA1i_DIR all_0.44060 yes 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py UNIT_fA1i_DIR all_0.52600 yes 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py UNIT_fA1i_DIR all_0.51450 yes 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py UNIT_fA1i_DIR all_0.50320 yes 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py UNIT_fA1i_DIR all_0.62800 yes 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py UNIT_fA1i_DIR all_0.61420 yes 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py UNIT_fA1i_DIR all_0.60080 yes 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py UNIT_fA1i_DIR all_0.58760 yes 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py UNIT_fA1i_DIR all_0.57470 yes 20. 0.8 0 13.0 -14.5
python 004_0_cluster.py UNIT_fA1i_DIR all_0.56220 yes 20. 0.8 0 13.0 -14.5



# run with Mgas for Emre
#nohup python run_md_cluster.py UNIT_fA1i_DIR no  20. 0.8 0 1.2 13.0 -14.5 >    logs/cluster_run_UNIT_fA1i_DIR_yes__2_08_0_4_13.log &
#
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.65650 no 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.65650_no_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.57470 no 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.57470_no_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.54980 no 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.54980_no_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.53780 no 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.53780_no_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.50320 no 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.50320_no_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.48140 no 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.48140_no_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.47090 no 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.47090_no_20._0.8_0_13.0.log &
#nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.46050 no 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.46050_no_20._0.8_0_13.0.log &
#
#nohup python run_md_cluster.py UNIT_fA1i_DIR yes  2. 0.8 0 4.0 13.0 -14.5 >    logs/cluster_run_UNIT_fA1i_DIR_yes__2_08_0_4_13_145.log &
#nohup python run_md_cluster.py UNIT_fA1i_DIR yes  20. 0.8 0 4.0 13.0 -14.5 >    logs/cluster_run_UNIT_fA1i_DIR_yes__20_08_0_4_13_145.log &
#
#/data24s/comparat/simulation/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001/fits/all_0.52600_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5.fits
#/data24s/comparat/simulation/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001/fits/all_0.53780_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5.fits

# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.97810 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.97810_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.95670 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.95670_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.93570 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.93570_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.91520 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.91520_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.89510 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.89510_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.87550 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.87550_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.85640 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.85640_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.83760 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.83760_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.81920 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.81920_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.80130 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.80130_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.78370 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.78370_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.76660 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.76660_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.74980 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.74980_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.73330 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.73330_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.71730 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.71730_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.70160 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.70160_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.68620 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.68620_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.67120 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.67120_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.65650 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.65650_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.64210 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.64210_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.62800 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.62800_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.61420 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.61420_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.60080 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.60080_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.58760 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.58760_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.57470 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.57470_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.56220 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.56220_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.54980 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.54980_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.53780 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.53780_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.52600 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.52600_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.51450 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.51450_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.50320 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.50320_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.49220 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.49220_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.48140 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.48140_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.47090 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.47090_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.46050 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.46050_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.45050 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.45050_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.44060 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.44060_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.43090 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.43090_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.42150 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.42150_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.41230 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.41230_yes_20._0.8_0_13.0.log & 
# nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.40320 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.40320_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.39440 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.39440_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.38570 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.38570_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.37730 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.37730_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.36900 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.36900_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.36090 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.36090_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.35300 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.35300_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.34530 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.34530_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.33770 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.33770_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.33030 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.33030_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.32310 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.32310_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.31600 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.31600_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.30910 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.30910_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.30230 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.30230_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.29570 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.29570_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.28920 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.28920_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.28290 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.28290_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.27060 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.27060_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.26470 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.26470_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.25890 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.25890_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.25320 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.25320_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.24770 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.24770_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.24230 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.24230_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.23690 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.23690_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.23180 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.23180_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.22670 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.22670_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.22170 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.22170_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.21690 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.21690_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.21210 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.21210_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.20750 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.20750_yes_20._0.8_0_13.0.log & 
# # # nohup python 004_0_cluster.py UNIT_fA1i_DIR all_0.20290 yes 20. 0.8 0 13.0 -14.5 > UNIT_fA1i_DIR_all_0.20290_yes_20._0.8_0_13.0.log & 



grep rror  UNIT_fA1i_DIR_all_0.?????_yes_20._0.8_0_13.0.log



#  env + ' ' + with_image + ' ' + pixel_size_image ' ' + b_HS + ' ' + z_max

# grep -B 8 -A 2 rror  logs/cluster_run_MD40_yes_2_09_0.log    
# grep -B 8 -A 2 rror  logs/cluster_run_MD40_yes_2_08_0.log    
# grep -B 8 -A 2 rror  logs/cluster_run_MD40_yes_2_07_0.log   
# 
# grep -B 18 -A 2 rror  logs/cluster_run_MD40_yes_20_09_0.log   
# grep -B 18 -A 2 rror  logs/cluster_run_MD40_yes_20_08_0.log   
# grep -B 18 -A 2 rror  logs/cluster_run_MD40_yes_20_07_0.log   
# 
# grep -B 8 -A 2 rror  logs/cluster_run_MD04_yes_2_09_0.log    
# grep -B 8 -A 2 rror  logs/cluster_run_MD04_yes_2_08_0.log    
# grep -B 8 -A 2 rror  logs/cluster_run_MD04_yes_2_07_0.log    

# grep -B 18 -A 2 rror  logs/cluster_run_MD40_yes_20_09_0.log   
# 
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.89510 yes 20. 0.9 0 >  logs/cluster_run_MD40_0.89510_yes_20_09_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.78370 yes 20. 0.9 0 >  logs/cluster_run_MD40_0.78370_yes_20_09_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.76660 yes 20. 0.9 0 >  logs/cluster_run_MD40_0.76660_yes_20_09_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.70160 yes 20. 0.9 0 >  logs/cluster_run_MD40_0.70160_yes_20_09_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.65650 yes 20. 0.9 0 >  logs/cluster_run_MD40_0.65650_yes_20_09_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.64210 yes 20. 0.9 0 >  logs/cluster_run_MD40_0.64210_yes_20_09_0.log &
# 
# nohup python 004_0_cluster.py MD10 all_0.33770 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.33770_yes_20_10_0.log &
# nohup python 004_0_cluster.py MD10 all_0.33770 yes 20. 0.9 0 >  logs/cluster_run_MD10_0.33770_yes_20_09_0.log &
# nohup python 004_0_cluster.py MD10 all_0.33770 yes 20. 0.8 0 >  logs/cluster_run_MD10_0.33770_yes_20_08_0.log &
# nohup python 004_0_cluster.py MD10 all_0.33770 yes 20. 0.7 0 >  logs/cluster_run_MD10_0.33770_yes_20_07_0.log &
# nohup python 004_0_cluster.py MD10 all_0.33770 yes 20. 0.6 0 >  logs/cluster_run_MD10_0.33770_yes_20_06_0.log &
# 
# grep -B 18 -A 2 rror  logs/cluster_run_MD40_yes_20_08_0.log   
# 
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.89510 yes 20. 0.8 0 >  logs/cluster_run_MD40_0.89510_yes_20_08_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.83760 yes 20. 0.8 0 >  logs/cluster_run_MD40_0.83760_yes_20_08_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.78370 yes 20. 0.8 0 >  logs/cluster_run_MD40_0.78370_yes_20_08_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.76660 yes 20. 0.8 0 >  logs/cluster_run_MD40_0.76660_yes_20_08_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.70160 yes 20. 0.8 0 >  logs/cluster_run_MD40_0.70160_yes_20_08_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.65650 yes 20. 0.8 0 >  logs/cluster_run_MD40_0.65650_yes_20_08_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.64210 yes 20. 0.8 0 >  logs/cluster_run_MD40_0.64210_yes_20_08_0.log &
# 
# grep -B 18 -A 2 rror  logs/cluster_run_MD40_yes_20_07_0.log   
# 
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.89510 yes 20. 0.7 0 >  logs/cluster_run_MD40_0.89510_yes_20_07_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.76660 yes 20. 0.7 0 >  logs/cluster_run_MD40_0.76660_yes_20_07_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.70160 yes 20. 0.7 0 >  logs/cluster_run_MD40_0.70160_yes_20_07_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.65650 yes 20. 0.7 0 >  logs/cluster_run_MD40_0.65650_yes_20_07_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.64210 yes 20. 0.7 0 >  logs/cluster_run_MD40_0.64210_yes_20_07_0.log &

#####################################################
#
# File by file (parallel way to run it)
#
#####################################################

############## MD10 ################
cd $GIT_AGN_MOCK/python/

##### FIDUCIAL RUN ####
#
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.20290 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.20290_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.20750 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.20750_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.21210 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.21210_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.21690 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.21690_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.22170 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.22170_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.22670 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.22670_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.23180 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.23180_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.23690 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.23690_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.24230 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.24230_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.24770 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.24770_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.25320 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.25320_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.25890 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.25890_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.26470 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.26470_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.27060 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.27060_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.27670 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.27670_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.28290 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.28290_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.28920 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.28920_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.29570 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.29570_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.30230 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.30230_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.30910 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.30910_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.31600 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.31600_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.32310 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.32310_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.33030 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.33030_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.33770 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.33770_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.34530 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.34530_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.35300 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.35300_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.36090 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.36090_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.36900 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.36900_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.37730 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.37730_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.38570 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.38570_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.39440 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.39440_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.40320 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.40320_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.40320 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.40320_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.41230 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.41230_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.42150 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.42150_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.43090 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.43090_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.44060 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.44060_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.45050 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.45050_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.46050 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.46050_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.47090 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.47090_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.48140 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.48140_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.49220 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.49220_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.50320 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.50320_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.51450 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.51450_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.52600 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.52600_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.53780 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.53780_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.54980 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.54980_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.56220 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.56220_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.57470 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.57470_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.58760 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.58760_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.60080 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.60080_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.61420 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.61420_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.62800 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.62800_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.64210 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.64210_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.65650 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.65650_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.67120 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.67120_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.68620 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.68620_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.70160 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.70160_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.71730 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.71730_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.73330 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.73330_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.74980 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.74980_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.76660 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.76660_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.78370 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.78370_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.80130 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.80130_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.81920 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.81920_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.83760 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.83760_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.85640 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.85640_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.87550 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.87550_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.89510 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.89510_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.91520 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.91520_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.93570 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.93570_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.95670 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.95670_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_0.97810 yes 20. 1.0 0 >  logs/cluster_run_MD10_0.97810_yes_20_1_0.log &
# nohup nice -n 19 python3 004_0_cluster.py MD10 all_1.00000 yes 20. 1.0 0 >  logs/cluster_run_MD10_1.00000_yes_20_1_0.log &

############## MD40 ################

# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.35300 yes >  logs/cluster_run_MD40_0.35300_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.36090 yes >  logs/cluster_run_MD40_0.36090_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.36900 yes >  logs/cluster_run_MD40_0.36900_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.37730 yes >  logs/cluster_run_MD40_0.37730_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.38570 yes >  logs/cluster_run_MD40_0.38570_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.39440 yes >  logs/cluster_run_MD40_0.39440_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.40320 yes >  logs/cluster_run_MD40_0.40320_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.41230 yes >  logs/cluster_run_MD40_0.41230_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.42150 yes >  logs/cluster_run_MD40_0.42150_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.43090 yes >  logs/cluster_run_MD40_0.43090_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.44060 yes >  logs/cluster_run_MD40_0.44060_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.45050 yes >  logs/cluster_run_MD40_0.45050_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.46050 yes >  logs/cluster_run_MD40_0.46050_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.47090 yes >  logs/cluster_run_MD40_0.47090_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.48140 yes >  logs/cluster_run_MD40_0.48140_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.49220 yes >  logs/cluster_run_MD40_0.49220_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.50320 yes >  logs/cluster_run_MD40_0.50320_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.51450 yes >  logs/cluster_run_MD40_0.51450_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.52600 yes >  logs/cluster_run_MD40_0.52600_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.53780 yes >  logs/cluster_run_MD40_0.53780_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.54980 yes >  logs/cluster_run_MD40_0.54980_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.56220 yes >  logs/cluster_run_MD40_0.56220_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.57470 yes >  logs/cluster_run_MD40_0.57470_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.58760 yes >  logs/cluster_run_MD40_0.58760_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.60080 yes >  logs/cluster_run_MD40_0.60080_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.61420 yes >  logs/cluster_run_MD40_0.61420_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.62800 yes >  logs/cluster_run_MD40_0.62800_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.64210 yes >  logs/cluster_run_MD40_0.64210_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.65650 yes >  logs/cluster_run_MD40_0.65650_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.67120 yes >  logs/cluster_run_MD40_0.67120_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.68620 yes >  logs/cluster_run_MD40_0.68620_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.70160 yes >  logs/cluster_run_MD40_0.70160_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.71730 yes >  logs/cluster_run_MD40_0.71730_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.73330 yes >  logs/cluster_run_MD40_0.73330_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.74980 yes >  logs/cluster_run_MD40_0.74980_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.76660 yes >  logs/cluster_run_MD40_0.76660_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.78370 yes >  logs/cluster_run_MD40_0.78370_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.80130 yes >  logs/cluster_run_MD40_0.80130_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.81920 yes >  logs/cluster_run_MD40_0.81920_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.83760 yes >  logs/cluster_run_MD40_0.83760_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.85640 yes >  logs/cluster_run_MD40_0.85640_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.87550 yes >  logs/cluster_run_MD40_0.87550_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.89510 yes >  logs/cluster_run_MD40_0.89510_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.91520 yes >  logs/cluster_run_MD40_0.91520_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.93570 yes >  logs/cluster_run_MD40_0.93570_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.95670 yes >  logs/cluster_run_MD40_0.95670_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD40 all_0.97810 yes >  logs/cluster_run_MD40_0.97810_yes.log &
# 
# # ############## MD04 ################
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.70030 yes >  logs/cluster_run_MD04_0.70030_yes.log &    
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.70640 yes >  logs/cluster_run_MD04_0.70640_yes.log &     
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.71240 yes >  logs/cluster_run_MD04_0.71240_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.71830 yes >  logs/cluster_run_MD04_0.71830_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.72430 yes >  logs/cluster_run_MD04_0.72430_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.73630 yes >  logs/cluster_run_MD04_0.73630_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.74230 yes >  logs/cluster_run_MD04_0.74230_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.75440 yes >  logs/cluster_run_MD04_0.75440_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.76030 yes >  logs/cluster_run_MD04_0.76030_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.77240 yes >  logs/cluster_run_MD04_0.77240_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.77830 yes >  logs/cluster_run_MD04_0.77830_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.78730 yes >  logs/cluster_run_MD04_0.78730_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.79040 yes >  logs/cluster_run_MD04_0.79040_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.80230 yes >  logs/cluster_run_MD04_0.80230_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.80840 yes >  logs/cluster_run_MD04_0.80840_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.81440 yes >  logs/cluster_run_MD04_0.81440_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.81730 yes >  logs/cluster_run_MD04_0.81730_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.82340 yes >  logs/cluster_run_MD04_0.82340_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.82630 yes >  logs/cluster_run_MD04_0.82630_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.83240 yes >  logs/cluster_run_MD04_0.83240_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.83530 yes >  logs/cluster_run_MD04_0.83530_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.84140 yes >  logs/cluster_run_MD04_0.84140_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.84430 yes >  logs/cluster_run_MD04_0.84430_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.85040 yes >  logs/cluster_run_MD04_0.85040_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.85330 yes >  logs/cluster_run_MD04_0.85330_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.85940 yes >  logs/cluster_run_MD04_0.85940_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.86230 yes >  logs/cluster_run_MD04_0.86230_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.86840 yes >  logs/cluster_run_MD04_0.86840_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.87130 yes >  logs/cluster_run_MD04_0.87130_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.87730 yes >  logs/cluster_run_MD04_0.87730_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.88030 yes >  logs/cluster_run_MD04_0.88030_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.88630 yes >  logs/cluster_run_MD04_0.88630_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.88930 yes >  logs/cluster_run_MD04_0.88930_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.89530 yes >  logs/cluster_run_MD04_0.89530_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.89840 yes >  logs/cluster_run_MD04_0.89840_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.90430 yes >  logs/cluster_run_MD04_0.90430_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.90740 yes >  logs/cluster_run_MD04_0.90740_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.92515 yes >  logs/cluster_run_MD04_0.92515_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.95600 yes >  logs/cluster_run_MD04_0.95600_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_0.97071 yes >  logs/cluster_run_MD04_0.97071_yes.log &
# nohup nice -n 19 python3 004_0_cluster.py MD04 all_1.00000 yes >  logs/cluster_run_MD04_1.00000_yes.log &

#####################################################
#
# Merge the cluster catalogue
#
#####################################################
# $MD04: all OK
# $MD10: all OK
# $MD40, some shells computing

cd $GIT_AGN_MOCK/python/

#grep rror  UNIT_fA1i_DIR_all_0.?????_yes_2._0.8_0_13.0.log
#grep rror  UNIT_fA1i_DIR_all_0.?????_yes_20._0.8_0_13.0.log

# ls $UNIT_fA1i_DIR/fits/all_*_CLU_b8_CM_0_pixS_2.0_M500c_13.0_FX_-14.5.fits > lists/l1_2.list
# stilts tcat in=@lists/l1_2.list   ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_2.0_M500c_13.0_FX_-14.5.fits  

ls $UNIT_fA1i_DIR/fits/all_0.?????_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_image_yes.fits > lists/l1_2_mgas.list
stilts tcat in=@lists/l1_2_mgas.list   ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS_Sept2021.fits

ls $MD40/fits/all_0.?????_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_image_no.fits > lists/md40_l1_2_mgas.list
stilts tcat in=@lists/md40_l1_2_mgas.list   ifmt=fits omode=out ofmt=fits out=$MD40/MD40_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS_Sept2021.fits


MD40

#ls $UNIT_fA1i_DIR/fits/all_*_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5.fits > lists/l1_2.list
#stilts tcat in=@lists/l1_2.list   ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5.fits

#ls $UNIT_fA1i_DIR/fits/all_*_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_image_no.fits > lists/l1_2_mgas.list
#stilts tcat in=@lists/l1_2_mgas.list   ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS.fits

#cp $UNIT_fA1i_DIR/UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS.fits ~/mpecl/simulation
#topcat $UNIT_fA1i_DIR/UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS.fits

# ls $UNIT_fA1i_DIR/fits/all_*_CLU_b8_CM_0_pixS_2.0_M500c_13.0.fits > lists/l1_2.list
# stilts tcat in=@lists/l1_2.list   ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_2.0_M500c_13.0.fits

# ls $UNIT_fA1i_DIR/fits/all_*_CLU_b8_CM_0_pixS_20.0_M500c_13.0.fits > lists/l1_20.list
# stilts tcat in=@lists/l1_20.list   ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0.fits

# nohup python run_md_cluster.py UNIT_fA1_DIR  yes 20. 1.0 0 2.0 >    logs/cluster_run_UNIT_fA1_DIR_yes_20_10_0.log &                    
# nohup python run_md_cluster.py UNIT_fA1i_DIR yes 20. 1.0 0 2.0 >    logs/cluster_run_UNIT_fA1_DIR_yes_20_10_0.log &                    
# 
# nohup python run_md_cluster.py UNIT_fA1_DIR yes 20. 0.8 0 2.0 >    logs/cluster_run_UNIT_fA1_DIR_yes_20_08_0.log &                    
# nohup python run_md_cluster.py UNIT_fA1i_DIR yes 20. 0.8 0 2.0 >    logs/cluster_run_UNIT_fA1_DIR_yes_20_08_0.log &                    
# 
# ls $UNIT_fA1_DIR/fits/all_?.?????_CLU_b10_CM_0_pixS_20.0.fits  > lists/fit_list_eRO_UNIT_fA1_b10_CM_0_pixS_20.0.fits
# ls $UNIT_fA1_DIR/fits/all_?.?????_CLU_b8_CM_0_pixS_20.0.fits   > lists/fit_list_eRO_UNIT_fA1_b8_CM_0_pixS_20.0.fits
# ls $UNIT_fA1i_DIR/fits/all_?.?????_CLU_b10_CM_0_pixS_20.0.fits > lists/fit_list_eRO_UNIT_fA1i_b10_CM_0_pixS_20.0.fits
# ls $UNIT_fA1i_DIR/fits/all_?.?????_CLU_b8_CM_0_pixS_20.0.fits  > lists/fit_list_eRO_UNIT_fA1i_b8_CM_0_pixS_20.0.fits
# 
# stilts tcat in=@lists/fit_list_eRO_UNIT_fA1_b10_CM_0_pixS_20.0.fits  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1_DIR/UNIT1_CLU_b10_CM_0_pixS_20.0.fits 
# stilts tcat in=@lists/fit_list_eRO_UNIT_fA1_b8_CM_0_pixS_20.0.fits   ifmt=fits omode=out ofmt=fits out=$UNIT_fA1_DIR/UNIT1_CLU_b8_CM_0_pixS_20.0.fits  
# stilts tcat in=@lists/fit_list_eRO_UNIT_fA1i_b10_CM_0_pixS_20.0.fits ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/UNIT1i_CLU_b10_CM_0_pixS_20.0.fits
# stilts tcat in=@lists/fit_list_eRO_UNIT_fA1i_b8_CM_0_pixS_20.0.fits  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/UNIT1i_CLU_b8_CM_0_pixS_20.0.fits 
# 
# ls  /data26s/comparat/simulations/erosim/eSASS-v5-erass1_June2020_catalogues_merged/eSASS* > esass.list
# ls  /data26s/comparat/simulations/erosim/eSASS-v5-erass1_June2020_catalogues_merged/wvdet* > wvdet.list
# ls  /data26s/comparat/simulations/erosim/eSASS-v5-erass1_June2020_catalogues_merged/clusters* > cluster.list
# ls  /data26s/comparat/simulations/erosim/eSASS-v5-erass1_June2020_catalogues_merged/agn* > agn.list
# 
# stilts tcat in=@wvdet.list  ifmt=fits omode=out ofmt=fits out=wvdet_merged_wAGN_wCLU_rseppi_epoch1_field.fits
# stilts tcat in=@agn.list  ifmt=fits omode=out ofmt=fits out=agn_merged_wCLU_weSASS_rseppi_epoch1_field.fits
# stilts tcat in=@cluster.list  ifmt=fits omode=out ofmt=fits out=clusters_merged_wAGN_weSASS_rseppi_epoch1_field.fits
# stilts tcat in=@esass.list  ifmt=fits omode=out ofmt=fits out=eSASS_merged_wAGN_wCLU_rseppi_epoch1_field.fits
# 
# topcat $UNIT_fA1_DIR/UNIT1_CLU_b10_CM_0_pixS_20.0.fits $UNIT_fA1_DIR/UNIT1_CLU_b8_CM_0_pixS_20.0.fits  $UNIT_fA1i_DIR/UNIT1i_CLU_b10_CM_0_pixS_20.0.fits $UNIT_fA1i_DIR/UNIT1i_CLU_b8_CM_0_pixS_20.0.fits 
# 
# cp $UNIT_fA1_DIR/UNIT1_CLU_b10_CM_0_pixS_20.0.fits $UNIT_fA1_DIR/UNIT1_CLU_b8_CM_0_pixS_20.0.fits     ~/wwwDir/eROSITA_mock/catalogs/UNIT1/
# cp $UNIT_fA1i_DIR/UNIT1i_CLU_b10_CM_0_pixS_20.0.fits $UNIT_fA1i_DIR/UNIT1i_CLU_b8_CM_0_pixS_20.0.fits ~/wwwDir/eROSITA_mock/catalogs/UNIT1i/
# chmod ugo+rX ~/wwwDir/eROSITA_mock/catalogs/*
# chmod ugo+rX ~/wwwDir/eROSITA_mock/catalogs/*/*
# 
# 
# ls $MD04/fits/all_?.?????_CLU_b7_CM_0_pixS_20.0.fits > lists/fit_list_eRO_MD04_b7_CM_0_pixS_20.0.fits
# ls $MD04/fits/all_?.?????_CLU_b8_CM_0_pixS_20.0.fits > lists/fit_list_eRO_MD04_b8_CM_0_pixS_20.0.fits
# ls $MD04/fits/all_?.?????_CLU_b9_CM_0_pixS_20.0.fits > lists/fit_list_eRO_MD04_b9_CM_0_pixS_20.0.fits
# 
# wc -l lists/fit_list_eRO_MD04_b7_CM_0_pixS_20.0.fits
# wc -l lists/fit_list_eRO_MD04_b8_CM_0_pixS_20.0.fits
# wc -l lists/fit_list_eRO_MD04_b9_CM_0_pixS_20.0.fits
# 
# stilts tcat in=@lists/fit_list_eRO_MD04_b7_CM_0_pixS_20.0.fits ifmt=fits omode=out ofmt=fits out=$MD04/MD04_eRO_CLU_b7_CM_0_pixS_20.0.fits
# stilts tcat in=@lists/fit_list_eRO_MD04_b8_CM_0_pixS_20.0.fits ifmt=fits omode=out ofmt=fits out=$MD04/MD04_eRO_CLU_b8_CM_0_pixS_20.0.fits
# stilts tcat in=@lists/fit_list_eRO_MD04_b9_CM_0_pixS_20.0.fits ifmt=fits omode=out ofmt=fits out=$MD04/MD04_eRO_CLU_b9_CM_0_pixS_20.0.fits
# 
# cp $MD04/MD04_eRO_CLU_b7_CM_0_pixS_20.0.fits /data42s/comparat/firefly/mocks/2020-06/SMDPL/ 
# cp $MD04/MD04_eRO_CLU_b8_CM_0_pixS_20.0.fits /data42s/comparat/firefly/mocks/2020-06/SMDPL/
# cp $MD04/MD04_eRO_CLU_b9_CM_0_pixS_20.0.fits /data42s/comparat/firefly/mocks/2020-06/SMDPL/
# 
# ls $MD40/fits/all_?.?????_CLU_b7_CM_0_pixS_20.0.fits > lists/fit_list_eRO_MD40_b7_CM_0_pixS_20.0.fits 
# ls $MD40/fits/all_?.?????_CLU_b7_CM_0_pixS_2.0.fits  > lists/fit_list_eRO_MD40_b7_CM_0_pixS_2.0.fits  
# ls $MD40/fits/all_?.?????_CLU_b8_CM_0_pixS_20.0.fits > lists/fit_list_eRO_MD40_b8_CM_0_pixS_20.0.fits 
# ls $MD40/fits/all_?.?????_CLU_b8_CM_0_pixS_2.0.fits  > lists/fit_list_eRO_MD40_b8_CM_0_pixS_2.0.fits  
# ls $MD40/fits/all_?.?????_CLU_b9_CM_0_pixS_20.0.fits > lists/fit_list_eRO_MD40_b9_CM_0_pixS_20.0.fits 
# ls $MD40/fits/all_?.?????_CLU_b9_CM_0_pixS_2.0.fits  > lists/fit_list_eRO_MD40_b9_CM_0_pixS_2.0.fits  
# 
# wc -l lists/fit_list_eRO_MD40_b7_CM_0_pixS_20.0.fits 
# wc -l lists/fit_list_eRO_MD40_b7_CM_0_pixS_2.0.fits  
# wc -l lists/fit_list_eRO_MD40_b8_CM_0_pixS_20.0.fits 
# wc -l lists/fit_list_eRO_MD40_b8_CM_0_pixS_2.0.fits  
# wc -l lists/fit_list_eRO_MD40_b9_CM_0_pixS_20.0.fits 
# wc -l lists/fit_list_eRO_MD40_b9_CM_0_pixS_2.0.fits  
# 
# stilts tcat in=@lists/fit_list_eRO_MD40_b7_CM_0_pixS_20.0.fits  ifmt=fits omode=out ofmt=fits out=$MD40/MD40_eRO_CLU_b7_CM_0_pixS_20.0.fits 
# stilts tcat in=@lists/fit_list_eRO_MD40_b7_CM_0_pixS_2.0.fits   ifmt=fits omode=out ofmt=fits out=$MD40/MD40_eRO_CLU_b7_CM_0_pixS_2.0.fits  
# stilts tcat in=@lists/fit_list_eRO_MD40_b8_CM_0_pixS_20.0.fits  ifmt=fits omode=out ofmt=fits out=$MD40/MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits 
# stilts tcat in=@lists/fit_list_eRO_MD40_b8_CM_0_pixS_2.0.fits   ifmt=fits omode=out ofmt=fits out=$MD40/MD40_eRO_CLU_b8_CM_0_pixS_2.0.fits  
# stilts tcat in=@lists/fit_list_eRO_MD40_b9_CM_0_pixS_20.0.fits  ifmt=fits omode=out ofmt=fits out=$MD40/MD40_eRO_CLU_b9_CM_0_pixS_20.0.fits 
# stilts tcat in=@lists/fit_list_eRO_MD40_b9_CM_0_pixS_2.0.fits   ifmt=fits omode=out ofmt=fits out=$MD40/MD40_eRO_CLU_b9_CM_0_pixS_2.0.fits  
# 
# cp $MD40/MD40_eRO_CLU_b7_CM_0_pixS_20.0.fits /data42s/comparat/firefly/mocks/2020-06/HMD/ 
# cp $MD40/MD40_eRO_CLU_b7_CM_0_pixS_2.0.fits  /data42s/comparat/firefly/mocks/2020-06/HMD/ 
# cp $MD40/MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits /data42s/comparat/firefly/mocks/2020-06/HMD/ 
# cp $MD40/MD40_eRO_CLU_b8_CM_0_pixS_2.0.fits  /data42s/comparat/firefly/mocks/2020-06/HMD/ 
# cp $MD40/MD40_eRO_CLU_b9_CM_0_pixS_20.0.fits /data42s/comparat/firefly/mocks/2020-06/HMD/ 
# cp $MD40/MD40_eRO_CLU_b9_CM_0_pixS_2.0.fits  /data42s/comparat/firefly/mocks/2020-06/HMD/ 
# 
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b10_CM_0_pixS_20.0.fits > lists/fit_list_eRO_MD10_b10_CM_0_pixS_20.0.fits
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b6_CM_0_pixS_20.0.fits  > lists/fit_list_eRO_MD10_b6_CM_0_pixS_20.0.fits 
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b7_CM_0_pixS_20.0.fits  > lists/fit_list_eRO_MD10_b7_CM_0_pixS_20.0.fits 
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b7_CM_0_pixS_2.0.fits   > lists/fit_list_eRO_MD10_b7_CM_0_pixS_2.0.fits  
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b8_CM_0_pixS_20.0.fits  > lists/fit_list_eRO_MD10_b8_CM_0_pixS_20.0.fits 
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b8_CM_0_pixS_2.0.fits   > lists/fit_list_eRO_MD10_b8_CM_0_pixS_2.0.fits  
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b9_CM_0_pixS_20.0.fits  > lists/fit_list_eRO_MD10_b9_CM_0_pixS_20.0.fits 
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b9_CM_0_pixS_2.0.fits   > lists/fit_list_eRO_MD10_b9_CM_0_pixS_2.0.fits  
# 
# wc -l lists/fit_list_eRO_MD10_b10_CM_0_pixS_20.0.fits
# wc -l lists/fit_list_eRO_MD10_b6_CM_0_pixS_20.0.fits 
# wc -l lists/fit_list_eRO_MD10_b7_CM_0_pixS_20.0.fits 
# wc -l lists/fit_list_eRO_MD10_b8_CM_0_pixS_20.0.fits 
# wc -l lists/fit_list_eRO_MD10_b9_CM_0_pixS_20.0.fits 
# 
# wc -l lists/fit_list_eRO_MD10_b7_CM_0_pixS_2.0.fits  
# wc -l lists/fit_list_eRO_MD10_b8_CM_0_pixS_2.0.fits  
# wc -l lists/fit_list_eRO_MD10_b9_CM_0_pixS_2.0.fits  
# 
# stilts tcat in=@lists/fit_list_eRO_MD10_b10_CM_0_pixS_20.0.fits   ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b10_CM_0_pixS_20.0.fits           
# stilts tcat in=@lists/fit_list_eRO_MD10_b6_CM_0_pixS_20.0.fits    ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b6_CM_0_pixS_20.0.fits            
# stilts tcat in=@lists/fit_list_eRO_MD10_b7_CM_0_pixS_20.0.fits    ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b7_CM_0_pixS_20.0.fits            
# stilts tcat in=@lists/fit_list_eRO_MD10_b7_CM_0_pixS_2.0.fits     ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b7_CM_0_pixS_2.0.fits             
# stilts tcat in=@lists/fit_list_eRO_MD10_b8_CM_0_pixS_20.0.fits    ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b8_CM_0_pixS_20.0.fits            
# stilts tcat in=@lists/fit_list_eRO_MD10_b8_CM_0_pixS_2.0.fits     ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b8_CM_0_pixS_2.0.fits             
# stilts tcat in=@lists/fit_list_eRO_MD10_b9_CM_0_pixS_20.0.fits    ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b9_CM_0_pixS_20.0.fits            
# stilts tcat in=@lists/fit_list_eRO_MD10_b9_CM_0_pixS_2.0.fits     ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b9_CM_0_pixS_2.0.fits             
# 
# cp $MD10/MD10_eRO_CLU_b10_CM_0_pixS_20.0.fits /data42s/comparat/firefly/mocks/2020-06/MDPL2/ 
# cp $MD10/MD10_eRO_CLU_b6_CM_0_pixS_20.0.fits  /data42s/comparat/firefly/mocks/2020-06/MDPL2/ 
# cp $MD10/MD10_eRO_CLU_b7_CM_0_pixS_20.0.fits  /data42s/comparat/firefly/mocks/2020-06/MDPL2/ 
# cp $MD10/MD10_eRO_CLU_b7_CM_0_pixS_2.0.fits   /data42s/comparat/firefly/mocks/2020-06/MDPL2/ 
# cp $MD10/MD10_eRO_CLU_b8_CM_0_pixS_20.0.fits  /data42s/comparat/firefly/mocks/2020-06/MDPL2/ 
# cp $MD10/MD10_eRO_CLU_b8_CM_0_pixS_2.0.fits   /data42s/comparat/firefly/mocks/2020-06/MDPL2/ 
# cp $MD10/MD10_eRO_CLU_b9_CM_0_pixS_20.0.fits  /data42s/comparat/firefly/mocks/2020-06/MDPL2/ 
# cp $MD10/MD10_eRO_CLU_b9_CM_0_pixS_2.0.fits   /data42s/comparat/firefly/mocks/2020-06/MDPL2/ 
# 
# cd $MD10
# scp ds52:/data37s/simulation_1/MD/MD_1.0Gpc/MD10_eRO_CLU_b10_CM_0_pixS_20.0.fits .
# scp ds52:/data37s/simulation_1/MD/MD_1.0Gpc/MD10_eRO_CLU_b6_CM_0_pixS_20.0.fits  .
# scp ds52:/data37s/simulation_1/MD/MD_1.0Gpc/MD10_eRO_CLU_b7_CM_0_pixS_20.0.fits  .
# scp ds52:/data37s/simulation_1/MD/MD_1.0Gpc/MD10_eRO_CLU_b7_CM_0_pixS_2.0.fits   .
# scp ds52:/data37s/simulation_1/MD/MD_1.0Gpc/MD10_eRO_CLU_b8_CM_0_pixS_20.0.fits  .
# scp ds52:/data37s/simulation_1/MD/MD_1.0Gpc/MD10_eRO_CLU_b8_CM_0_pixS_2.0.fits   .
# scp ds52:/data37s/simulation_1/MD/MD_1.0Gpc/MD10_eRO_CLU_b9_CM_0_pixS_20.0.fits  .
# scp ds52:/data37s/simulation_1/MD/MD_1.0Gpc/MD10_eRO_CLU_b9_CM_0_pixS_2.0.fits   .
# cd $MD04
# scp ds52:/data17s/darksim/simulation_3/MD/MD_0.4Gpc/MD04_eRO_CLU_b7_CM_0_pixS_20.0.fits .
# scp ds52:/data17s/darksim/simulation_3/MD/MD_0.4Gpc/MD04_eRO_CLU_b8_CM_0_pixS_20.0.fits .
# scp ds52:/data17s/darksim/simulation_3/MD/MD_0.4Gpc/MD04_eRO_CLU_b9_CM_0_pixS_20.0.fits .
# cd $MD40
# scp ds52:/data39s/simulation_2/MD/MD_4.0Gpc/MD40_eRO_CLU_b7_CM_0_pixS_20.0.fits .
# scp ds52:/data39s/simulation_2/MD/MD_4.0Gpc/MD40_eRO_CLU_b7_CM_0_pixS_2.0.fits  .
# scp ds52:/data39s/simulation_2/MD/MD_4.0Gpc/MD40_eRO_CLU_b8_CM_0_pixS_20.0.fits .
# scp ds52:/data39s/simulation_2/MD/MD_4.0Gpc/MD40_eRO_CLU_b8_CM_0_pixS_2.0.fits  .
# scp ds52:/data39s/simulation_2/MD/MD_4.0Gpc/MD40_eRO_CLU_b9_CM_0_pixS_20.0.fits .
# scp ds52:/data39s/simulation_2/MD/MD_4.0Gpc/MD40_eRO_CLU_b9_CM_0_pixS_2.0.fits  .

# # Previously generated mocks :
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b10_CM_0.fits             > lists/fit_list_eRO_MD10_b10_CM_0.fits             
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b10_CM_0_pixS_20.0.fits   > lists/fit_list_eRO_MD10_b10_CM_0_pixS_20.0.fits   
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b10_CM_0_pixS_2.0.fits    > lists/fit_list_eRO_MD10_b10_CM_0_pixS_2.0.fits    
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b10_CM_m20.fits           > lists/fit_list_eRO_MD10_b10_CM_m20.fits           
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b10_CM_m40.fits           > lists/fit_list_eRO_MD10_b10_CM_m40.fits           
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b10_CM_p20.fits           > lists/fit_list_eRO_MD10_b10_CM_p20.fits           
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b10_CM_p40.fits           > lists/fit_list_eRO_MD10_b10_CM_p40.fits           
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b11.fits                  > lists/fit_list_eRO_MD10_b11.fits                  
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b6_CM_0_pixS_20.0.fits    > lists/fit_list_eRO_MD10_b6_CM_0_pixS_20.0.fits    
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b6.fits                   > lists/fit_list_eRO_MD10_b6.fits                   
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b7.fits                   > lists/fit_list_eRO_MD10_b7.fits                   
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b8_CM_0_pixS_20.0.fits    > lists/fit_list_eRO_MD10_b8_CM_0_pixS_20.0.fits    
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b8.fits                   > lists/fit_list_eRO_MD10_b8.fits                   
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU_b9.fits                   > lists/fit_list_eRO_MD10_b9.fits                   
# ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_?.?????_CLU.fits                      > lists/fit_list_eRO_MD10_fits                      
# 
# wc -l lists/fit_list_eRO_MD10_b10_CM_0.fits             
# wc -l lists/fit_list_eRO_MD10_b10_CM_0_pixS_20.0.fits   
# wc -l lists/fit_list_eRO_MD10_b10_CM_0_pixS_2.0.fits    
# wc -l lists/fit_list_eRO_MD10_b10_CM_m20.fits           
# wc -l lists/fit_list_eRO_MD10_b10_CM_m40.fits           
# wc -l lists/fit_list_eRO_MD10_b10_CM_p20.fits           
# wc -l lists/fit_list_eRO_MD10_b10_CM_p40.fits           
# wc -l lists/fit_list_eRO_MD10_b11.fits                  
# wc -l lists/fit_list_eRO_MD10_b6_CM_0_pixS_20.0.fits    
# wc -l lists/fit_list_eRO_MD10_b6.fits                   
# wc -l lists/fit_list_eRO_MD10_b7.fits                   
# wc -l lists/fit_list_eRO_MD10_b8_CM_0_pixS_20.0.fits    
# wc -l lists/fit_list_eRO_MD10_b8.fits                   
# wc -l lists/fit_list_eRO_MD10_b9.fits                   
# wc -l lists/fit_list_eRO_MD10_fits                      
# 
# stilts tcat in=@lists/fit_list_eRO_MD10_b10_CM_0.fits           ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b10_CM_0.fits           
# stilts tcat in=@lists/fit_list_eRO_MD10_b10_CM_0_pixS_20.0.fits ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b10_CM_0_pixS_20.0.fits 
# stilts tcat in=@lists/fit_list_eRO_MD10_b10_CM_0_pixS_2.0.fits  ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b10_CM_0_pixS_2.0.fits  
# stilts tcat in=@lists/fit_list_eRO_MD10_b10_CM_m20.fits         ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b10_CM_m20.fits         
# stilts tcat in=@lists/fit_list_eRO_MD10_b10_CM_m40.fits         ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b10_CM_m40.fits         
# stilts tcat in=@lists/fit_list_eRO_MD10_b10_CM_p20.fits         ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b10_CM_p20.fits         
# stilts tcat in=@lists/fit_list_eRO_MD10_b10_CM_p40.fits         ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b10_CM_p40.fits         
# stilts tcat in=@lists/fit_list_eRO_MD10_b11.fits                ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b11.fits                
# stilts tcat in=@lists/fit_list_eRO_MD10_b6_CM_0_pixS_20.0.fits  ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b6_CM_0_pixS_20.0.fits  
# stilts tcat in=@lists/fit_list_eRO_MD10_b6.fits                 ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b6.fits                 
# stilts tcat in=@lists/fit_list_eRO_MD10_b7.fits                 ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b7.fits                 
# stilts tcat in=@lists/fit_list_eRO_MD10_b8_CM_0_pixS_20.0.fits  ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b8_CM_0_pixS_20.0.fits  
# stilts tcat in=@lists/fit_list_eRO_MD10_b8.fits                 ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b8.fits                 
# stilts tcat in=@lists/fit_list_eRO_MD10_b9.fits                 ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b9.fits                 
# stilts tcat in=@lists/fit_list_eRO_MD10_fits                    ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_fits                    
# 
# ls $MD10/fits/*_CLU.fits > lists/fit_list_eRo_CLUMD10.list
# stilts tcat in=@lists/fit_list_eRo_CLUMD10.list ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU.fit
# 
# ls $MD10/fits/*_CLU_b6.fits > lists/fit_list_eRo_CLUMD10_b6.list
# stilts tcat in=@lists/fit_list_eRo_CLUMD10_b6.list ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b06.fit
# ls $MD10/fits/*_CLU_b7.fits > lists/fit_list_eRo_CLUMD10_b7.list
# stilts tcat in=@lists/fit_list_eRo_CLUMD10_b7.list ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b07.fit
# ls $MD10/fits/*_CLU_b8.fits > lists/fit_list_eRo_CLUMD10_b8.list
# stilts tcat in=@lists/fit_list_eRo_CLUMD10_b8.list ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b08.fit
# ls $MD10/fits/*_CLU_b9.fits > lists/fit_list_eRo_CLUMD10_b9.list
# stilts tcat in=@lists/fit_list_eRo_CLUMD10_b9.list ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b09.fit
# ls $MD10/fits/*_CLU_b11.fits > lists/fit_list_eRo_CLUMD10_b11.list
# stilts tcat in=@lists/fit_list_eRo_CLUMD10_b11.list ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b11.fit
# 
# ls $MD10/fits/*_CLU_b10_CM_m20.fits > lists/fit_list_eRo_CLUMD10_b10_m20.list
# stilts tcat in=@lists/fit_list_eRo_CLUMD10_b10_m20.list ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b10_m20.fit
# ls $MD10/fits/*_CLU_b10_CM_m40.fits > lists/fit_list_eRo_CLUMD10_b10_m40.list
# stilts tcat in=@lists/fit_list_eRo_CLUMD10_b10_m40.list ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b10_m40.fit
# ls $MD10/fits/*_CLU_b10_CM_p40.fits > lists/fit_list_eRo_CLUMD10_b10_p40.list
# stilts tcat in=@lists/fit_list_eRo_CLUMD10_b10_p40.list ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b10_p40.fit
# ls $MD10/fits/*_CLU_b10_CM_p20.fits > lists/fit_list_eRo_CLUMD10_b10_p20.list
# stilts tcat in=@lists/fit_list_eRo_CLUMD10_b10_p20.list ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b10_p20.fit
# 
# ls $MD10/fits/*_CLU_b10_CM_0.fits > lists/fit_list_eRo_CLUMD10_b10_0.list
# stilts tcat in=@lists/fit_list_eRo_CLUMD10_b10_0.list ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b10_0.fit

# gzip  -k --rsyncable $MD40/MD40_eRO_CLU.fit
# gzip  -k --rsyncable $MD04/MD04_eRO_CLU.fit
# gzip  -k --rsyncable $MD10/MD10_eRO_CLU.fit
# 
# cp $MD40/MD40_eRO_CLU.fit.gz /data42s/comparat/firefly/mocks/2020-03/HMD/  
# cp $MD04/MD04_eRO_CLU.fit.gz /data42s/comparat/firefly/mocks/2020-03/SMDPL/ 
# cp $MD10/MD10_eRO_CLU.fit.gz /data42s/comparat/firefly/mocks/2020-03/MDPL2/ 
# ls $UNIT_fA1_DIR/fits/*_CLU.fits > lists/fit_list_eRo_CLUUNIT_fA1_DIR.list
# stilts tcat in=@lists/fit_list_eRo_CLUUNIT_fA1_DIR.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1_DIR/UNIT_fA1_DIR_eRO_CLU.fit
# 
# ls $UNIT_fA1i_DIR/fits/*_CLU.fits > lists/fit_list_eRo_CLUUNIT_fA1i_DIR.list
# stilts tcat in=@lists/fit_list_eRo_CLUUNIT_fA1i_DIR.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/UNIT_fA1i_DIR_eRO_CLU.fit
# 
# ls $UNIT_fA2_DIR/fits/*_CLU.fits > lists/fit_list_eRo_CLUUNIT_fA2_DIR.list
# stilts tcat in=@lists/fit_list_eRo_CLUUNIT_fA2_DIR.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA2_DIR/UNIT_fA2_DIR_eRO_CLU.fit

# 
# topcat -stilts plot2sky \
#    xpix=1543 ypix=721 \
#    projection=aitoff viewsys=galactic crowd=7.2 gridaa=true texttype=antialias fontsize=16 fontweight=bold \
#    auxmap=accent auxfunc=log auxmin=0.001 auxmax=0.1 auxcrowd=3.5 \
#    auxvisible=true auxlabel='density of sources per arcmin**2' \
#    legend=false \
#    layer=SkyDensity \
#       in=$MD40/MD40_eRO_CLU.fit \
#       lon=RA lat=DEC datasys=equatorial \
#       level=5 combine=count-per-unit perunit=arcmin2 \
#       leglabel='clusters' \
#       omode=out out=$GIT_AGN_MOCK/figures/MD40/clusters/cluster_density.png
# 
# 
# topcat -stilts plot2sky \
#    xpix=1543 ypix=721 \
#    projection=aitoff viewsys=galactic crowd=7.2 gridaa=true texttype=antialias fontsize=16 fontweight=bold \
#    auxmap=accent auxfunc=log auxmin=0.001 auxmax=0.1 auxcrowd=3.5 \
#    auxvisible=true auxlabel='density of sources per arcmin**2' \
#    legend=false \
#    layer=SkyDensity \
#       in=$MD10/MD10_eRO_CLU.fit \
#       lon=RA lat=DEC datasys=equatorial \
#       level=5 combine=count-per-unit perunit=arcmin2 \
#       leglabel='clusters' \
#       omode=out out=$GIT_AGN_MOCK/figures/MD10/clusters/cluster_density.png
# 

###########################################
#
# FIGURES of clusters
#
###########################################


#004_5_plot_clusters_k_correction.py MD10

# plots the mass (M500c) vs redshift for the simulation and the data used by the cluster profile generator
# script: 004_5_plot_clusters_M500-z.py
# inputs: ${env}/${env}_eRO_CLU.fit
# outputs: 
#   $GIT_AGN_MOCK/figures/${env}/clusters/M500c-z-1em14.png (for a given flux limit)
#   $GIT_AGN_MOCK/figures/${env}/clusters/M500c-zpng.png (full simulation)

# Execute on the laptop, needs external data sets
#cd $GIT_AGN_MOCK/python/
#nohup nice -n 19  python 004_5_plot_clusters_M500-z.py MD10  >  logs/plot_004_5_M500z_MD10.log &
#nohup nice -n 19  python 004_5_plot_clusters_M500-z.py MD04  >  logs/plot_004_5_M500z_MD04.log &
#nohup nice -n 19  python 004_5_plot_clusters_M500-z.py MD40   >  logs/plot_004_5_M500z_MD40.log &
# nohup nice -n 19  python 004_5_plot_clusters_M500-z.py UNIT_fA1_DIR   >  logs/plot_004_5_M500z_UNIT_fA1_DIR.log &
# nohup nice -n 19  python 004_5_plot_clusters_M500-z.py UNIT_fA1i_DIR  >  logs/plot_004_5_M500z_UNIT_fA1i_DIR.log &
# nohup nice -n 19  python 004_5_plot_clusters_M500-z.py UNIT_fA2_DIR  >  logs/plot_004_5_M500z_UNIT_fA2_DIR.log &

# plots the logNlogS
# script: plot_clusters_logNlogS.py
# inputs: ${env}/${env}_eRO_CLU.fit
# outputs: 
# figures per simulation in $GIT_AGN_MOCK/figures/${env}/clusters/
#   logNlogS_2e13.png logNlogS_2e13.txt
#   logNlogS_5e13.png logNlogS_5e13.txt
#   logNlogS_1e14.png logNlogS_1e14.txt
#  summary figures (can accomodate all simulations, but currently only MD10)
#   $GIT_AGN_MOCK/figures/logNlogS_cluster_2e13.png
#   $GIT_AGN_MOCK/figures/logNlogS_cluster_5e13.png
#   $GIT_AGN_MOCK/figures/logNlogS_cluster_1e14.png
#cd $GIT_AGN_MOCK/python/figures/
#nohup nice -n 19 python plot_clusters_logNlogS.py  > logs/lognlogs.log           &
# nohup nice -n 19 python plot_clusters_logNlogS.py MD04                        > logs/lognlogs_MD04.log           &
# nohup nice -n 19 python plot_clusters_logNlogS.py MD40                        > logs/lognlogs_MD40.log           &
# nohup nice -n 19 python plot_clusters_logNlogS.py UNIT_fA1i_DIR               > logs/lognlogs_UNIT_fA1i_DIR.log  &
# nohup nice -n 19 python plot_clusters_logNlogS.py UNIT_fA1_DIR                > logs/lognlogs_UNIT_fA1_DIR.log   &
# nohup nice -n 19 python plot_clusters_logNlogS.py UNIT_fA2_DIR                > logs/lognlogs_UNIT_fA2_DIR.log   &

# plots the X-ray luminosity function
# script: plot_clusters_XLF.py
# inputs: ${env}/${env}_eRO_CLU.fit
# outputs: 
# figure in $GIT_AGN_MOCK/figures/${env}/clusters/XLF.png
#cd $GIT_AGN_MOCK/python/figures/
#nohup nice -n 19 python plot_clusters_XLF.py MD10                             > logs/xlf_MD10.log           &
# nohup nice -n 19 python plot_clusters_XLF.py MD04                             > logs/xlf_MD04.log           &
# nohup nice -n 19 python plot_clusters_XLF.py MD40                             > logs/xlf_MD40.log           &
# nohup nice -n 19 python plot_clusters_XLF.py UNIT_fA1i_DIR                    > logs/xlf_UNIT_fA1i_DIR.log  &
# nohup nice -n 19 python plot_clusters_XLF.py UNIT_fA1_DIR                     > logs/xlf_UNIT_fA1_DIR.log   &
# nohup nice -n 19 python plot_clusters_XLF.py UNIT_fA2_DIR                     > logs/xlf_UNIT_fA2_DIR.log   &

# plots the scaling relations LX-kT and M-kT
# script: plot_clusters_scaling_relations.py
# inputs: ${env}/${env}_eRO_CLU.fit
# outputs: 
# figure in $GIT_AGN_MOCK/figures/${env}/clusters/
#   LX-KT-Z.png
#   M500-KT-Z.png
#cd $GIT_AGN_MOCK/python/figures/
#
#python plot_clusters_scaling_relations_kT_LX.py
#python plot_clusters_scaling_relations_M500c_kT.py
#python plot_clusters_scaling_relations_M500c_LX.py
#
#nohup nice -n 19 python plot_clusters_scaling_relations.py MD04               > logs/scaling_relations_MD04.log           &
#nohup nice -n 19 python plot_clusters_scaling_relations.py MD10               > logs/scaling_relations_MD10.log           &
#nohup nice -n 19 python plot_clusters_scaling_relations.py MD40               > logs/scaling_relations_MD40.log           &
# nohup nice -n 19 python plot_clusters_scaling_relations.py UNIT_fA1i_DIR      > logs/scaling_relations_UNIT_fA1i_DIR.log  &
# nohup nice -n 19 python plot_clusters_scaling_relations.py UNIT_fA1_DIR       > logs/scaling_relations_UNIT_fA1_DIR.log   &
# nohup nice -n 19 python plot_clusters_scaling_relations.py UNIT_fA2_DIR       > logs/scaling_relations_UNIT_fA2_DIR.log   &

cd $GIT_AGN_MOCK/python/
# creates simput catalogues and cluster catalogues with a flux limit
# script: 004_6_clusters_simput.py
# inputs: ${env}/fits/all_?.?????_CLU.fits
# outputs: ${env}/cat_CLU_SIMPUT/c_000???_N_?.fit, ${env}/cat_eRO_CLU/000???.fit
# nohup python 004_6_clusters_simput.py MD04          > logs/run_CLU_simput_MD04.log & #                       
# nohup python 004_6_clusters_simput.py MD10          > logs/run_CLU_simput_MD10.log & #                       
# nohup python 004_6_clusters_simput.py MD40          > logs/run_CLU_simput_MD40.log & #                       
# nohup python 004_6_clusters_simput.py UNIT_fA1i_DIR > logs/run_CLU_simput_UNIT_fA1i_DIR.log & #                       
# nohup python 004_6_clusters_simput.py UNIT_fA1_DIR  > logs/run_CLU_simput_UNIT_fA1_DIR.log & #                       
# nohup python 004_6_clusters_simput.py UNIT_fA2_DIR  > logs/run_CLU_simput_UNIT_fA2_DIR.log & #                       

# nohup python run_md_cluster.py UNIT_fA1i_DIR yes  2. 0.8 0 4.0 13.0 >    logs/cluster_run_UNIT_fA1i_DIR_yes__2_08_0_4_13.log &                    
# nohup python run_md_cluster.py UNIT_fA1i_DIR yes 20. 0.8 0 1.2 13.0 >    logs/cluster_run_UNIT_fA1i_DIR_yes_20_08_0_1_13.log &                    

# nohup python 004_6_clusters_simput.py UNIT_fA1i_DIR  20. 0.8 0 13.0 >    logs/cluster_simput_UNIT_fA1_DIR_20_10_0.log &                    
# nohup python 004_6_clusters_simput.py UNIT_fA1i_DIR  2.  0.8 0 13.0 >    logs/cluster_simput_UNIT_fA1_DIR_2_10_0.log &                    

nohup python 004_6_clusters_simput.py UNIT_fA1i_DIR  20.  0.8 0 13.0 -14.5 >    logs/cluster_simput_UNIT_fA1_DIR_2_10_0145.log &
# nohup python 004_6_clusters_simput.py MD40  10.  0.8 0 13.0 -14.5 >    logs/cluster_simput_UNIT_fA1_DIR_2_10_0145.log &

# generates simput files oin the SKYMAP grid
python 004_6_clusters_simput_SKYMAP.py

# tar -czf all_0.20290.tar.gz all_0.20290
# tar -czf all_0.20750.tar.gz all_0.20750
# tar -czf all_0.21210.tar.gz all_0.21210
# tar -czf all_0.21690.tar.gz all_0.21690
# tar -czf all_0.22170.tar.gz all_0.22170
# tar -czf all_0.22670.tar.gz all_0.22670
# tar -czf all_0.23180.tar.gz all_0.23180
# tar -czf all_0.23690.tar.gz all_0.23690
# tar -czf all_0.24230.tar.gz all_0.24230
# tar -czf all_0.24770.tar.gz all_0.24770
# tar -czf all_0.25320.tar.gz all_0.25320
# tar -czf all_0.25890.tar.gz all_0.25890
# tar -czf all_0.26470.tar.gz all_0.26470
# tar -czf all_0.27060.tar.gz all_0.27060
# tar -czf all_0.28290.tar.gz all_0.28290
# tar -czf all_0.28920.tar.gz all_0.28920
# tar -czf all_0.29570.tar.gz all_0.29570
# tar -czf all_0.30230.tar.gz all_0.30230
# tar -czf all_0.30910.tar.gz all_0.30910
# tar -czf all_0.31600.tar.gz all_0.31600
# tar -czf all_0.32310.tar.gz all_0.32310
# tar -czf all_0.33030.tar.gz all_0.33030
# tar -czf all_0.33770.tar.gz all_0.33770
# tar -czf all_0.34530.tar.gz all_0.34530
# tar -czf all_0.35300.tar.gz all_0.35300
# tar -czf all_0.36090.tar.gz all_0.36090
# tar -czf all_0.36900.tar.gz all_0.36900
# tar -czf all_0.37730.tar.gz all_0.37730
# tar -czf all_0.38570.tar.gz all_0.38570
# tar -czf all_0.39440.tar.gz all_0.39440
# tar -czf all_0.40320.tar.gz all_0.40320
# tar -czf all_0.41230.tar.gz all_0.41230
# tar -czf all_0.42150.tar.gz all_0.42150
# tar -czf all_0.43090.tar.gz all_0.43090
# tar -czf all_0.44060.tar.gz all_0.44060
# tar -czf all_0.45050.tar.gz all_0.45050
# tar -czf all_0.46050.tar.gz all_0.46050
# tar -czf all_0.47090.tar.gz all_0.47090
# tar -czf all_0.48140.tar.gz all_0.48140
# tar -czf all_0.49220.tar.gz all_0.49220
# tar -czf all_0.50320.tar.gz all_0.50320
# tar -czf all_0.51450.tar.gz all_0.51450
# tar -czf all_0.52600.tar.gz all_0.52600
# tar -czf all_0.53780.tar.gz all_0.53780
# tar -czf all_0.54980.tar.gz all_0.54980
# tar -czf all_0.56220.tar.gz all_0.56220
# tar -czf all_0.57470.tar.gz all_0.57470
# tar -czf all_0.58760.tar.gz all_0.58760
# tar -czf all_0.60080.tar.gz all_0.60080
# tar -czf all_0.61420.tar.gz all_0.61420
# tar -czf all_0.62800.tar.gz all_0.62800
# tar -czf all_0.64210.tar.gz all_0.64210
# tar -czf all_0.65650.tar.gz all_0.65650
# tar -czf all_0.67120.tar.gz all_0.67120
# tar -czf all_0.68620.tar.gz all_0.68620
# tar -czf all_0.70160.tar.gz all_0.70160
# tar -czf all_0.71730.tar.gz all_0.71730
# tar -czf all_0.73330.tar.gz all_0.73330
# tar -czf all_0.74980.tar.gz all_0.74980
# tar -czf all_0.76660.tar.gz all_0.76660
# tar -czf all_0.78370.tar.gz all_0.78370
# tar -czf all_0.80130.tar.gz all_0.80130
# tar -czf all_0.81920.tar.gz all_0.81920
# tar -czf all_0.83760.tar.gz all_0.83760
# tar -czf all_0.85640.tar.gz all_0.85640
# tar -czf all_0.87550.tar.gz all_0.87550
# tar -czf all_0.89510.tar.gz all_0.89510
# tar -czf all_0.91520.tar.gz all_0.91520
# tar -czf all_0.93570.tar.gz all_0.93570
# tar -czf all_0.95670.tar.gz all_0.95670
# tar -czf all_0.97810.tar.gz all_0.97810
# 
# python 004_6_clusters_simput.py UNIT_fA1_DIR   20. 1.0 0  >    logs/cluster_simput_UNIT_fA1_DIR_20_10_0.log &                    
# nohup python 004_6_clusters_simput.py UNIT_fA1i_DIR  20. 1.0 0  >    logs/cluster_simput_UNIT_fA1_DIR_20_10_0.log &                    
# nohup python 004_6_clusters_simput.py UNIT_fA1_DIR   20. 0.8 0  >    logs/cluster_simput_UNIT_fA1_DIR_20_08_0.log &                    
# nohup python 004_6_clusters_simput.py UNIT_fA1i_DIR  20. 0.8 0  >    logs/cluster_simput_UNIT_fA1_DIR_20_08_0.log &                    
# 
# rmdir $UNIT_fA1_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0/cluster_Xspectra 
# rmdir $UNIT_fA1_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/cluster_Xspectra 
# rmdir $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0/cluster_Xspectra 
# rmdir $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/cluster_Xspectra 

cd $UNIT_fA1_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd $UNIT_fA1_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 

rmdir $UNIT_fA1_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0_galNH/cluster_Xspectra 
rmdir  $UNIT_fA1_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/cluster_Xspectra 
rmdir $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0_galNH/cluster_Xspectra 
rmdir  $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/cluster_Xspectra 

rmdir  $UNIT_fA1_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0_galNH/cluster_images 
rmdir   $UNIT_fA1_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/cluster_images 
rmdir $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0_galNH/cluster_images 
rmdir  $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/cluster_images 

cd $UNIT_fA1_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0_galNH
ln -s /data39s/simulation_2/MD/MD_4.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra .
cd $UNIT_fA1_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH
ln -s /data39s/simulation_2/MD/MD_4.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra .
cd $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0_galNH
ln -s /data39s/simulation_2/MD/MD_4.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra .
cd $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH
ln -s /data39s/simulation_2/MD/MD_4.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra .

/data24s/comparat/simulation/UNIT/ROCKSTAR_HALOS/fixedAmp_001/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/galNH_20.6_10000kT_61659_10000z_8730.fits

cd $UNIT_fA1_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0_galNH
ln -s $UNIT_fA1_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0/cluster_images .
cd $UNIT_fA1_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH
ln -s $UNIT_fA1_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/cluster_images .
cd $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0_galNH
ln -s $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0/cluster_images .
cd $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH
ln -s $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/cluster_images .
  
cd /data39s/simulation_2/MD/MD_4.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra
chgrp erosim galNH_19*
chgrp erosim galNH_20*
chgrp erosim galNH_21*
chgrp erosim galNH_22*
  
cd /data39s/simulation_2/MD/MD_4.0Gpc/cat_AGN_SIMPUT/agn_Xspectra
chgrp erosim galNH_19*
chgrp erosim galNH_20*
chgrp erosim galNH_21*
chgrp erosim galNH_22*

chgrp erosim /data24s/*
chgrp erosim /data24s/comparat/*
chgrp erosim /data24s/comparat/simulation/*
chgrp erosim /data24s/comparat/simulation/UNIT/*
chgrp erosim /data24s/comparat/simulation/UNIT/ROCKSTAR_HALOS/*
chgrp erosim /data24s/comparat/simulation/UNIT/ROCKSTAR_HALOS/fixedAmp_001/*
chgrp erosim /data24s/comparat/simulation/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001/*

chgrp erosim  $UNIT_fA1_DIR/* 
chgrp erosim  $UNIT_fA1_DIR/*
chgrp erosim $UNIT_fA1i_DIR/* 
chgrp erosim $UNIT_fA1i_DIR/*

chgrp erosim  $UNIT_fA1_DIR/cat_AGN*/*
chgrp erosim  $UNIT_fA1_DIR/cat_AGN*/*
chgrp erosim $UNIT_fA1i_DIR/cat_AGN*/*
chgrp erosim $UNIT_fA1i_DIR/cat_AGN*/*

chgrp erosim  $UNIT_fA1_DIR/cat_CLU_*/*
chgrp erosim  $UNIT_fA1_DIR/cat_CLU_*/*
chgrp erosim $UNIT_fA1i_DIR/cat_CLU_*/*
chgrp erosim $UNIT_fA1i_DIR/cat_CLU_*/*

chgrp erosim  $UNIT_fA1_DIR/cat_eRO*/*
chgrp erosim  $UNIT_fA1_DIR/cat_eRO*/*
chgrp erosim $UNIT_fA1i_DIR/cat_eRO*/*
chgrp erosim $UNIT_fA1i_DIR/cat_eRO*/*

chgrp erosim  $UNIT_fA1_DIR/cat_GALAXY_*/*
chgrp erosim  $UNIT_fA1_DIR/cat_GALAXY_*/*
chgrp erosim $UNIT_fA1i_DIR/cat_GALAXY_*/*
chgrp erosim $UNIT_fA1i_DIR/cat_GALAXY_*/*

chgrp erosim  $UNIT_fA1_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0_galNH/* 
chgrp erosim   $UNIT_fA1_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/* 
chgrp erosim $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0_galNH/* 
chgrp erosim  $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/* 

chgrp erosim  $UNIT_fA1_DIR/cat_AGN_SIMPUT/*
chgrp erosim  $UNIT_fA1_DIR/cat_AGN_SIMPUT/*
chgrp erosim $UNIT_fA1i_DIR/cat_AGN_SIMPUT/*
chgrp erosim $UNIT_fA1i_DIR/cat_AGN_SIMPUT/*

chgrp erosim  $UNIT_fA1_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0/cluster_images/* 
chgrp erosim   $UNIT_fA1_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/cluster_images/* 
chgrp erosim $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0/cluster_images/* 
chgrp erosim  $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/cluster_images/* 

cd $UNIT_fA1_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0/cluster_images
python  change_permission.py

cd $UNIT_fA1_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/cluster_images
cp /data24s/comparat/simulation/UNIT/ROCKSTAR_HALOS/fixedAmp_001/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0/cluster_images/change_permission.py .
python  change_permission.py

cd $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0/cluster_images
cp /data24s/comparat/simulation/UNIT/ROCKSTAR_HALOS/fixedAmp_001/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0/cluster_images/change_permission.py .
python  change_permission.py

cd $UNIT_fA1i_DIR/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/cluster_images
cp /data24s/comparat/simulation/UNIT/ROCKSTAR_HALOS/fixedAmp_001/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0/cluster_images/change_permission.py .
python  change_permission.py

nohup python 004_6_clusters_simput.py MD10 20. 1.0 0 >    logs/cluster_run_MD10_20_10_0.log &                    
nohup python 004_6_clusters_simput.py MD10 20. 0.9 0 >    logs/cluster_run_MD10_20_09_0.log &                    
nohup python 004_6_clusters_simput.py MD10 20. 0.8 0 >    logs/cluster_run_MD10_20_08_0.log &                    
nohup python 004_6_clusters_simput.py MD10 20. 0.7 0 >    logs/cluster_run_MD10_20_07_0.log &                    
nohup python 004_6_clusters_simput.py MD10 20. 0.6 0 >    logs/cluster_run_MD10_20_06_0.log &                    

nohup python 004_6_clusters_simput.py MD10 2.  0.9 0 >    logs/cluster_run_MD10_2_09_0.log &                    
nohup python 004_6_clusters_simput.py MD10 2.  0.8 0 >    logs/cluster_run_MD10_2_08_0.log &                    
nohup python 004_6_clusters_simput.py MD10 2.  0.7 0 >    logs/cluster_run_MD10_2_07_0.log &                    

nohup python 004_6_clusters_simput.py MD40 2.  0.9 0 >    logs/cluster_run_MD40_2_09_0.log &                    
nohup python 004_6_clusters_simput.py MD40 2.  0.8 0 >    logs/cluster_run_MD40_2_08_0.log &                    
nohup python 004_6_clusters_simput.py MD40 2.  0.7 0 >    logs/cluster_run_MD40_2_07_0.log &                    

nohup python 004_6_clusters_simput.py MD40 20.  0.9 0 >   logs/cluster_run_MD40_20_09_0.log &                    
nohup python 004_6_clusters_simput.py MD40 20.  0.8 0 >   logs/cluster_run_MD40_20_08_0.log &                    
nohup python 004_6_clusters_simput.py MD40 20.  0.7 0 >   logs/cluster_run_MD40_20_07_0.log &                    

nohup python 004_6_clusters_simput.py MD04 20.  0.9 0 >   logs/cluster_run_MD04_2_09_0.log &                  
nohup python 004_6_clusters_simput.py MD04 20.  0.8 0 >   logs/cluster_run_MD04_2_08_0.log &                  
nohup python 004_6_clusters_simput.py MD04 20.  0.7 0 >   logs/cluster_run_MD04_2_07_0.log &                  

cd $MD10/cat_CLU_SIMPUT_b10_CM_0_pixS_20.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd  $MD10/cat_CLU_SIMPUT_b6_CM_0_pixS_20.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd   $MD10/cat_CLU_SIMPUT_b7_CM_0_pixS_2.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd  $MD10/cat_CLU_SIMPUT_b7_CM_0_pixS_20.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd   $MD10/cat_CLU_SIMPUT_b8_CM_0_pixS_2.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd  $MD10/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd   $MD10/cat_CLU_SIMPUT_b9_CM_0_pixS_2.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd  $MD10/cat_CLU_SIMPUT_b9_CM_0_pixS_20.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 


cd   $MD40/cat_CLU_SIMPUT_b7_CM_0_pixS_2.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd  $MD40/cat_CLU_SIMPUT_b7_CM_0_pixS_20.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd   $MD40/cat_CLU_SIMPUT_b8_CM_0_pixS_2.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd  $MD40/cat_CLU_SIMPUT_b8_CM_0_pixS_20.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd   $MD40/cat_CLU_SIMPUT_b9_CM_0_pixS_2.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 
cd  $MD40/cat_CLU_SIMPUT_b9_CM_0_pixS_20.0
ln -s /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra 

rsync -avz $MD10/cat_CLU_SIMPUT_b8_CM_0_pixS_2.0 /data42s/comparat/firefly/mocks/2020-03/MDPL2/
cd /data42s/comparat/firefly/mocks/2020-03/MDPL2/
chmod ugo+rX *
cd /data42s/comparat/firefly/mocks/2020-03/MDPL2/cat_CLU_SIMPUT_b8_CM_0_pixS_2.0
chmod ugo+rX *

rsync -avz $MD10/cat_CLU_SIMPUT_b9_CM_0_pixS_2.0 /data42s/comparat/firefly/mocks/2020-03/MDPL2/
cd /data42s/comparat/firefly/mocks/2020-03/MDPL2/
chmod ugo+rX *
cd /data42s/comparat/firefly/mocks/2020-03/MDPL2/cat_CLU_SIMPUT_b9_CM_0_pixS_2.0
chmod ugo+rX *

rsync -avz $MD10/cat_CLU_SIMPUT_b7_CM_0_pixS_2.0 /data42s/comparat/firefly/mocks/2020-03/MDPL2/
cd /data42s/comparat/firefly/mocks/2020-03/MDPL2/
chmod ugo+rX *
cd /data42s/comparat/firefly/mocks/2020-03/MDPL2/cat_CLU_SIMPUT_b7_CM_0_pixS_2.0
chmod ugo+rX *

rsync -avz /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra /data42s/comparat/firefly/mocks/2020-03/MDPL2/
cd /data42s/comparat/firefly/mocks/2020-03/MDPL2/
chmod ugo+rX *
cd /data42s/comparat/firefly/mocks/2020-03/MDPL2/cluster_Xspectra
chmod ugo+rX *

# ONLY RUN ONCE
# computes cluster X-ray spectra
# ds54 or ds43 to have pyxspec
# script: 004_7_clusters_create_xpectra.py
# inputs: env, pyXspec
# outputs: ${env}/cat_CLU_SIMPUT/cluster_Xspectra/cluster_spectrum_10kT_????_100z_????.fits
# python 004_7_clusters_create_xpectra.py MD40
nohup nice -n 19 python 004_7_clusters_create_xpectra.py MD10 > run_CLU_7_MD10.log &
nohup nice -n 19 python 004_7_clusters_create_xpectra.py MD04 > run_CLU_7_MD04.log &
nohup nice -n 19 python 004_7_clusters_create_xpectra.py MD40 > run_CLU_7_MD40.log &
python 004_7_clusters_create_xpectra_withGALnH.py

# creates figures illustrating the spectra used 
# written in $GIT_AGN_MOCK/figures/MD10/Xspectra
python plot_Xspectra.py MD10

# Subset of spectra directly available here :
tar -czf /home/comparat/data2/firefly/mocks/2020-03/spectraAGN.tar.gz /data37s/simulation_1/MD/MD_1.0Gpc/cat_AGN_SIMPUT/agn_Xspectra/*N1024.fits 
tar -czf /home/comparat/data2/firefly/mocks/2020-03/spectraCLUSTER.tar.gz /data37s/simulation_1/MD/MD_1.0Gpc/cat_CLU_SIMPUT/cluster_Xspectra/*.fits 

cd $GIT_AGN_MOCK/python/sixte/

# simulate counts with sixte 
# script: $GIT_AGN_MOCK/python/sixte/simulate_cluster_only.py
# inputs: 
#    768 catalogues: ${env}/cat_CLU_SIMPUT/000???.fit
#    spectra: ${env}/cat_CLU_SIMPUT/cluster_Xspectra/NH??.?_Z?.?_N*.fits
#    images: ${env}/cat_CLU_SIMPUT/cluster_images/*
# outputs:
#    /data40s/erosim/eRASS/eRASS8_cluster_${env}/???/erass_ccd?_evt.fits
#    /data40s/erosim/eRASS/eRASS8_cluster_${env}/???/erass.gti
# concatenated outputs :
#    /data40s/erosim/eRASS/eRASS8_cluster_${env}/simulated_photons_ccd?.fits
#    /data40s/erosim/eRASS/eRASS8_cluster_${env}/
# public version
#    cp /data40s/erosim/eRASS/eRASS8_cluster_MD10/simulated_photons_ccd?.fits /data42s/comparat/firefly/mocks/2020-03/MDPL2/CLUSTER_counts/
#    cp /data40s/erosim/eRASS/eRASS8_cluster_MD40/simulated_photons_ccd?.fits /data42s/comparat/firefly/mocks/2020-03/HMD/CLUSTER_counts/
#    cp /data40s/erosim/eRASS/eRASS8_cluster_MD04/simulated_photons_ccd?.fits /data42s/comparat/firefly/mocks/2020-03/SMDPL/CLUSTER_counts/
#    https://firefly.mpe.mpg.de/mocks/2020-03/MDPL2/CLUSTER_counts/

# execute all commands in 
# execute all commands in this file by copy pasting them into a screen on ds43 of ds54

cd $GIT_ERASS_SIM/sixte

# ongoing ds43
nohup python all_sky_simulate_cluster.py MD10 cat_CLU_SIMPUT_b6_CM_0_pixS_20.0 > logs/sixte_MD10_cat_CLU_SIMPUT_b6_CM_0_pixS_20.0.log  &
nohup python all_sky_simulate_cluster.py MD10 cat_CLU_SIMPUT_b7_CM_0_pixS_20.0 > logs/sixte_MD10_cat_CLU_SIMPUT_b7_CM_0_pixS_20.0.log  &
nohup python all_sky_simulate_cluster.py MD10 cat_CLU_SIMPUT_b8_CM_0_pixS_20.0 > logs/sixte_MD10_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0.log  &
nohup python all_sky_simulate_cluster.py MD10 cat_CLU_SIMPUT_b9_CM_0_pixS_20.0 > logs/sixte_MD10_cat_CLU_SIMPUT_b9_CM_0_pixS_20.0.log  &
nohup python all_sky_simulate_cluster.py MD10 cat_CLU_SIMPUT_b10_CM_0_pixS_20.0 > logs/sixte_MD10_cat_CLU_SIMPUT_b10_CM_0_pixS_20.0.log  &

# ongoing ds43 
nohup python all_sky_simulate_cluster.py MD40 cat_CLU_SIMPUT_b7_CM_0_pixS_20.0 > logs/sixte_MD40_cat_CLU_SIMPUT_b7_CM_0_pixS_20.0.log  &
nohup python all_sky_simulate_cluster.py MD40 cat_CLU_SIMPUT_b8_CM_0_pixS_20.0 > logs/sixte_MD40_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0.log  &
nohup python all_sky_simulate_cluster.py MD40 cat_CLU_SIMPUT_b9_CM_0_pixS_20.0 > logs/sixte_MD40_cat_CLU_SIMPUT_b9_CM_0_pixS_20.0.log  &

# nohup nice -n 19 sh all_sky_simulate_cluster_MD40.sh > logs/sixte_MD40.log  &
# nohup nice -n 19 sh all_sky_simulate_cluster_MD04.sh > logs/sixte_MD04.log  &
# $GIT_AGN_MOCK/python/sixte/all_sky_simulate_cluster_MD04.sh

nohup sh all_sky_simulate_cluster_UNITfA1i.sh > logs/all_sky_simulate_cluster_UNITfA1i.log &
nohup sh all_sky_simulate_cluster_UNITfA1.sh  > logs/all_sky_simulate_cluster_UNITfA1.log  & 

chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1i_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/*

chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1i_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/0*/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1i_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/1*/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1i_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/2*/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1i_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/3*/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1i_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/4*/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1i_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/5*/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1i_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/6*/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1i_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/7*/*

chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/0*/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/1*/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/2*/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/3*/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/4*/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/5*/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/6*/*
chgrp erosim /data26s/comparat/simulations/erosim/eRASS8_events_cluster_UNIT_fA1_DIR_Dec2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0_galNH/7*/*

ls /data26s/comparat/simulations/erosim/eRASS8_events_cluster_MD10_June2020_cat_CLU_SIMPUT_b6_CM_0_pixS_20.0/???/erass_ccd1_evt.fits | wc -l
ls /data26s/comparat/simulations/erosim/eRASS8_events_cluster_MD10_June2020_cat_CLU_SIMPUT_b7_CM_0_pixS_20.0/???/erass_ccd1_evt.fits | wc -l
ls /data26s/comparat/simulations/erosim/eRASS8_events_cluster_MD10_June2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/???/erass_ccd1_evt.fits | wc -l
ls /data26s/comparat/simulations/erosim/eRASS8_events_cluster_MD10_June2020_cat_CLU_SIMPUT_b9_CM_0_pixS_20.0/???/erass_ccd1_evt.fits | wc -l
ls /data26s/comparat/simulations/erosim/eRASS8_events_cluster_MD10_June2020_cat_CLU_SIMPUT_b10_CM_0_pixS_20.0/???/erass_ccd1_evt.fits | wc -l
ls /data26s/comparat/simulations/erosim/eRASS8_events_cluster_MD40_June2020_cat_CLU_SIMPUT_b7_CM_0_pixS_20.0/???/erass_ccd1_evt.fits | wc -l
ls /data26s/comparat/simulations/erosim/eRASS8_events_cluster_MD40_June2020_cat_CLU_SIMPUT_b8_CM_0_pixS_20.0/???/erass_ccd1_evt.fits | wc -l
ls /data26s/comparat/simulations/erosim/eRASS8_events_cluster_MD40_June2020_cat_CLU_SIMPUT_b9_CM_0_pixS_20.0/???/erass_ccd1_evt.fits | wc -l

# need to create :
# $GIT_AGN_MOCK/python/sixte/all_sky_simulate_cluster_UNIT_fA1i_DIR.sh
# $GIT_AGN_MOCK/python/sixte/all_sky_simulate_cluster_UNIT_fA1_DIR.sh 
# $GIT_AGN_MOCK/python/sixte/all_sky_simulate_cluster_UNIT_fA2_DIR.sh 

# FIGURES 
# Makes full sky maps of the density per square arc minutes of the counts
# $GIT_AGN_MOCK/figures/${env}/sixte/cluster_eRASS?.png
# $GIT_AGN_MOCK/figures/${env}/sixte/agn_eRASS?.png
$GIT_AGN_MOCK/python/sixte/simulation_figures.sh

cd $GIT_AGN_MOCK/python
ls /data37s/simulation_1/MD/MD_1.0Gpc/cat_eRO_CLU-1Count_June2020/*.fits > lists/fit_list_eRO_MD10_b8_CM_0_pixS_20.0_w_counts 
wc -l lists/fit_list_eRO_MD10_b8_CM_0_pixS_20.0_w_counts   
stilts tcat in=@lists/fit_list_eRO_MD10_b8_CM_0_pixS_20.0_w_counts    ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b8_CM_0_pixS_20.0_wCount_June2020.fit

#####################################################
#
# Galaxies in clusters
#
#####################################################

# ONLY ON DS43 OR 54
# due to healpy dependency

# STEP 1 
# runs all the steps below in a series for each shell
# 
# script: 004_1_galaxies_around_clusters.py
# inputs: env, basename, delta_crit
#    e.g. 'MD04' "all_0.62840" '200c'
# outputs: 
#       runs the scripts below in order

# STEP 2
# identifies galaxies within a certain distance from a halo in the cluster file
# script: 004_2_cluster_galaxies.py
# inputs: ${env}/fits/all_?.?????_CLU.fits, ${env}/fits/all_?.?????.fits, ${env}/fits/all_?.?????_coordinates.fits, ${env}/fits/all_?.?????_galaxy.fits
# outputs: ${env}/fits/all_?.?????_galaxiesAroundClusters.fit

# 
python 004_1_galaxies_around_eROSITA_clusters.py MD10 MD10_eRO_CLU_b8_CM_0_pixS_20.0_wCount_June2020.fit
python 004_1_galaxies_around_eROSITA_clusters.py MD04 MD04_eRO_CLU_b8_CM_0_pixS_20.0.fits
python 004_1_galaxies_around_eROSITA_clusters.py UNIT_fA1_DIR UNIT_fA1_DIR_eRO_CLU_b8_CM_0_pixS_20.0.fits
python 004_1_galaxies_around_eROSITA_clusters.py UNIT_fA1i_DIR UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0.fits

python 004_1_galaxies_around_eROSITA_clusters.py UNIT_fA1i_DIR UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS_Sept2021.fits
python 004_1_galaxies_around_eROSITA_CHANCES_clusters.py UNIT_fA1i_DIR UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_M500c_13.0_FX_-14.5_MGAS_Sept2021.fits

nohup python 004_1_galaxies_around_eROSITA_clusters.py UNIT_fA1i_DIR UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0.fits > 004_1_galaxies_around_eROSITA_clusters_UNIT_fA1i.log &


ls $MD10/cat_eROCGAL/*.fit > lists/list_galaxiesAroundClusters.list
stilts tcat in=@lists/list_galaxiesAroundClusters.list ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_b8_CM_0_pixS_20.0_CGAL.fit

ls $MD04/cat_eROCGAL/*.fit > lists/list_galaxiesAroundClustersMD04.list
stilts tcat in=@lists/list_galaxiesAroundClustersMD04.list ifmt=fits omode=out ofmt=fits out=$MD04/MD04_eRO_CLU_b8_CM_0_pixS_20.0_CGAL.fits

ls $UNIT_fA1_DIR/cat_eROCGAL/*.fit > lists/list_galaxiesAroundClustersUNIT_fA1_DIR.list
stilts tcat in=@lists/list_galaxiesAroundClustersUNIT_fA1_DIR.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1_DIR/UNIT_fA1_DIR_eRO_CLU_b8_CM_0_pixS_20.0_CGAL.fits

ls $UNIT_fA1i_DIR/cat_eROCGAL/*.fit > lists/list_galaxiesAroundClustersUNIT_fA1i_DIR.list
stilts tcat in=@lists/list_galaxiesAroundClustersUNIT_fA1i_DIR.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/UNIT_fA1i_DIR_eRO_CLU_b8_CM_0_pixS_20.0_CGAL.fits

# STEP 3
# assign SED using observations from 4most_s8_kidsdr4.lsdr9.eromapper.fits
# NEEDS THE AGN MOCK TO BE FINISHED :
# agn = Table.read('/data42s/comparat/firefly/mocks/2021-04/QMOST/UNIT_fA1i_DIRS6_4MOST_ALL_SNR3_IR215.fit')
# qso = Table.read('/data42s/comparat/firefly/mocks/2021-04/QMOST/QSO_4MOST.fits')

# python 004_2_all_magnitudes.py MD10 
python 004_2_all_magnitudes.py MD04 
# python 004_2_all_magnitudes.py UNIT_fA1_DIR  
python 004_2_all_magnitudes.py UNIT_fA1i_DIR 
	
# nohup sh 004_2_cluster_galaxies.sh > logs/cluster_004_2_gal_MD10.log & # ongoing
# 
# nohup sh 004_3_cluster_galaxies_legacy_survey_catalogue.sh > logs/cluster_004_3_gal_MD10_LS.log &

# # # # V0 
# # # # STEP 3
# # # # identifies the quiescent galaxies around the clusters
# # # # corrects the star formation rate values
# # # # script: 004_3_cluster_red_galaxies.py
# # # # inputs: ${env}/fits/all_?.?????_galaxiesAroundClusters.fit
# # # # outputs: ${env}/fits/all_?.?????_galaxiesAroundClusters.fit (add more columns)
# # # 
# # # # STEP 4
# # # # Abundance matching to the Ricci et al. 2018 luminosity function fo obtain rest frame r-band magnitudes
# # # # redmapper red-sequence model to give colors to quiescent galaxies
# # # # estimates richness (still in dev.)
# # # # script: 004_4_red_sequence.py
# # # # inputs: ${env}/fits/all_?.?????_galaxiesAroundClusters.fit
# # # # outputs: ${env}/fits/all_?.?????_galaxiesAroundClusters.fit (add more columns)
# # # 
# # # ls $MD10/fits/*galaxiesAroundClusters_MD10_eRO_CLU_b8_CM_0_pixS_20.0.fit
# # # ls $MD10/fits/*galaxiesAroundClusters_MD10_eRO_CLU_b8_CM_0_pixS_20.0_LS.fit
# # # scp -r ds23:/data37s/simulation_1/MD/MD_1.0Gpc/fits/*galaxiesAroundClusters_MD10_eRO_CLU_b8_CM_0_pixS_20.0*.fit .
# # # 
# # # 
# # # ls $MD10/fits/all_?.?????_galaxiesAroundClusters.fit
# # # ls $MD10/fits/all_?.?????_galaxiesAroundClusters_LS.fit

#############################################
#
# FIGURES of galaxies in clusters, in shells
#
#############################################

# plots the luminosity function for each redshift shell
# script: 004_5_plot_clusters.py
# inputs: ${env}/fits/all_?.?????_galaxiesAroundClusters.fit
# outputs: $GIT_AGN_MOCK/figures/${env}/clusters/galaxies/cluster_galaxy_LF_z_*.png
cd $GIT_AGN_MOCK/python/
nohup nice -n 19  python 004_5_plot_clusters.py MD40 200c  > logs/plot_004_5_MD40.log &
nohup nice -n 19  python 004_5_plot_clusters.py MD04 200c  > logs/plot_004_5_MD04.log &
nohup nice -n 19  python 004_5_plot_clusters.py MD10 200c  > logs/plot_004_5_MD10.log &

# plots the distribution of colors: red sequence model and its realization in each redshift shell 
# script: 004_5_plot_clusters_colors.py
# inputs: ${env}/fits/all_?.?????_galaxiesAroundClusters.fit
# outputs: $GIT_AGN_MOCK/figures/${env}/clusters/galaxies/red_sequence_*.png
cd $GIT_AGN_MOCK/python/
nohup nice -n 19  python 004_5_plot_clusters_colors.py MD40 > logs/plot_004_5_color_MD40.log &
nohup nice -n 19  python 004_5_plot_clusters_colors.py MD04 > logs/plot_004_5_color_MD04.log &
nohup nice -n 19  python 004_5_plot_clusters_colors.py MD10 > logs/plot_004_5_color_MD10.log &

# plots the richness vs. mass (M200c) in each redshift shell and writes the data points for fitting
# script: 004_5_plot_clusters_colors.py
# inputs: ${env}/fits/all_?.?????_galaxiesAroundClusters.fit
# outputs: 
#   $GIT_AGN_MOCK/figures/${env}/clusters/galaxies/richness_mass_*.png
#   $GIT_AGN_MOCK/figures/${env}/clusters/galaxies/richness_mass_*.txt 
cd $GIT_AGN_MOCK/python/
nohup nice -n 19  python 004_5_plot_clusters_richness.py MD40 200c  >  logs/plot_004_5_richness_MD40.log &
nohup nice -n 19  python 004_5_plot_clusters_richness.py MD04 200c  >  logs/plot_004_5_richness_MD04.log &
nohup nice -n 19  python 004_5_plot_clusters_richness.py MD10 200c  >  logs/plot_004_5_richness_MD10.log &

# tries to fit of the richness scaling relation :
# plots the richness vs. mass (M200c) in each redshift shell and writes the data points for fitting
# script: 004_5_plot_clusters_colors.py
# inputs:  $GIT_AGN_MOCK/figures/${env}/clusters/galaxies/richness_mass_*.txt
# outputs: 
#   set of parameters
#   $GIT_AGN_MOCK/figures/${env}/clusters/galaxies/richness_mass_evolution_1e14.txt and richness_mass_evolution_1e14.png
cd $GIT_AGN_MOCK/python/
nohup nice -n 19  python 004_5_plot_clusters_richness_fits.py MD40 200c  >  logs/plot_004_5_richness_MD40_fits.log &
nohup nice -n 19  python 004_5_plot_clusters_richness_fits.py MD10 200c  >  logs/plot_004_5_richness_MD10_fits.log &
nohup nice -n 19  python 004_5_plot_clusters_richness_fits.py MD04 200c  >  logs/plot_004_5_richness_MD04_fits.log &

# In progress ...
# Tries to compares galaxy LF with SPIDERS
# script: 004_5_plot_clusters_w_SPIDERS.py
# inputs: ${env}/fits/all_?.?????_galaxiesAroundClusters.fit
# outputs: 
#   $GIT_AGN_MOCK/figures/${env}/galaxies/spiders_cluster_galaxy_LF_z_*.png 
nohup nice -n 19  python 004_5_plot_clusters_w_SPIDERS.py MD40 200c  > logs/plot_004_5_comparison_spiders_MD40.log &
nohup nice -n 19  python 004_5_plot_clusters_w_SPIDERS.py MD04 200c  > logs/plot_004_5_comparison_spiders_MD04.log &
nohup nice -n 19  python 004_5_plot_clusters_w_SPIDERS.py MD10 200c  > logs/plot_004_5_comparison_spiders_MD10.log &

#####################################################
#
# Merge the catalogue of galaxies in clusters
# r-band magnitude observed brigther than 24.5
#
#####################################################
 
# # # cd $GIT_AGN_MOCK/python/
# # # ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_*_galaxiesAroundClusters_MD10_eRO_CLU_b8_CM_0_pixS_20.0_LS.fit > lists/list_galaxiesAroundClusters_md10_b8_CM_0_pixS_20_LS.list
# # # ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_*_galaxiesAroundClusters_MD10_eRO_CLU_b8_CM_0_pixS_20.0.fit > lists/list_galaxiesAroundClusters_md10_b8_CM_0_pixS_20.list
# # # 
# # # wc -l lists/list_galaxiesAroundClusters_md10_b8_CM_0_pixS_20.list
# # # wc -l lists/list_galaxiesAroundClusters_md10_b8_CM_0_pixS_20_LS.list
# # # 
# # # stilts tcat in=@lists/list_galaxiesAroundClusters_md10_b8_CM_0_pixS_20.list  ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_galaxies_b8_CM_0_pixS_20.0.fit  
# # # stilts tcat in=@lists/list_galaxiesAroundClusters_md10_b8_CM_0_pixS_20_LS.list  ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_galaxies_b9_CM_0_pixS_20.0_LS.fit  
# # # 
# # # cd $MD10
# # # scp ds43:/data37s/simulation_1/MD/MD_1.0Gpc/MD10_eRO_CLU_galaxies_b8_CM_0_pixS_20.0.fit    .
# # # scp ds43:/data37s/simulation_1/MD/MD_1.0Gpc/MD10_eRO_CLU_galaxies_b9_CM_0_pixS_20.0_LS.fit .
# # # 
# # # 
# # # ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_*_galaxiesAroundClusters_MD10_eRO_CLU_b6_CM_0_pixS_20.0_LS.fit > lists/list_galaxiesAroundClusters_md10_b6_CM_0_pixS_20_LS.list
# # # ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_*_galaxiesAroundClusters_MD10_eRO_CLU_b7_CM_0_pixS_20.0_LS.fit > lists/list_galaxiesAroundClusters_md10_b7_CM_0_pixS_20_LS.list
# # # ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_*_galaxiesAroundClusters_MD10_eRO_CLU_b8_CM_0_pixS_20.0_LS.fit > lists/list_galaxiesAroundClusters_md10_b8_CM_0_pixS_20_LS.list
# # # ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_*_galaxiesAroundClusters_MD10_eRO_CLU_b9_CM_0_pixS_20.0_LS.fit > lists/list_galaxiesAroundClusters_md10_b9_CM_0_pixS_20_LS.list
# # # ls /data37s/simulation_1/MD/MD_1.0Gpc/fits/all_*_galaxiesAroundClusters_MD10_eRO_CLU_b10_CM_0_pixS_20.0_LS.fit > lists/list_galaxiesAroundClusters_md10_b10_CM_0_pixS_20_LS.list
# # # 
# # # wc -l lists/list_galaxiesAroundClusters_md10_b6_CM_0_pixS_20_LS.list
# # # wc -l lists/list_galaxiesAroundClusters_md10_b7_CM_0_pixS_20_LS.list
# # # wc -l lists/list_galaxiesAroundClusters_md10_b8_CM_0_pixS_20_LS.list
# # # wc -l lists/list_galaxiesAroundClusters_md10_b9_CM_0_pixS_20_LS.list
# # # wc -l lists/list_galaxiesAroundClusters_md10_b10_CM_0_pixS_20_LS.list
# # # 
# # # stilts tcat in=@lists/list_galaxiesAroundClusters_md10_b6_CM_0_pixS_20_LS.list  ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_galaxies_b6_CM_0_pixS_20.0_LS.fit 
# # # stilts tcat in=@lists/list_galaxiesAroundClusters_md10_b7_CM_0_pixS_20_LS.list  ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_galaxies_b7_CM_0_pixS_20.0_LS.fit  
# # # stilts tcat in=@lists/list_galaxiesAroundClusters_md10_b8_CM_0_pixS_20_LS.list  ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_galaxies_b8_CM_0_pixS_20.0_LS.fit  
# # # stilts tcat in=@lists/list_galaxiesAroundClusters_md10_b9_CM_0_pixS_20_LS.list  ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_galaxies_b9_CM_0_pixS_20.0_LS.fit  
# # # stilts tcat in=@lists/list_galaxiesAroundClusters_md10_b10_CM_0_pixS_20_LS.list ifmt=fits omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_galaxies_b10_CM_0_pixS_20.0_LS.fit 
# # # 
# # # ls $MD10/fits/*_galaxiesAroundClusters.fit > lists/list_galaxiesAroundClusters_md10.list
# # # ls $MD04/fits/*_galaxiesAroundClusters.fit > lists/list_galaxiesAroundClusters_md04.list
# # # ls $MD40/fits/*_galaxiesAroundClusters.fit > lists/list_galaxiesAroundClusters_md40.list
# # # 
# # # nohup stilts tcat in=@lists/list_galaxiesAroundClusters_md04.list ifmt=fits icmd='select "mag_r<24.5"' omode=out ofmt=fits out=$MD04/MD04_eRO_CLU_GAL.fit > logs/concat_galaxies_around_clusters_MD04.log &
# # # nohup stilts tcat in=@lists/list_galaxiesAroundClusters_md10.list ifmt=fits icmd='select "mag_r<24.5"' omode=out ofmt=fits out=$MD10/MD10_eRO_CLU_GAL.fit > logs/concat_galaxies_around_clusters_MD10.log &
# # # nohup stilts tcat in=@lists/list_galaxiesAroundClusters_md40.list ifmt=fits icmd='select "mag_r<24.5"' omode=out ofmt=fits out=$MD40/MD40_eRO_CLU_GAL.fit > logs/concat_galaxies_around_clusters_MD40.log &

# plots the fraction of red galaxies vs radius
# script: plot_red_fraction_vs_cluster_radius.py
# inputs: ${env}/${env}_eRO_CLU.fit,  ${env}/${env}_eRO_CLU_GAL.fit
# outputs: 
#   $GIT_AGN_MOCK/figures/${env}/frac_QU_radius.png 
cd $GIT_AGN_MOCK/python/figures/
nohup nice -n 19  python plot_red_fraction_vs_cluster_radius.py MD40 >  ../logs/plot_red_fractions_vs_radius_MD40.log &                    
nohup nice -n 19  python plot_red_fraction_vs_cluster_radius.py MD04 >  ../logs/plot_red_fractions_vs_radius_MD04.log &                    
nohup nice -n 19  python plot_red_fraction_vs_cluster_radius.py MD10 >  ../logs/plot_red_fractions_vs_radius_MD10.log &                    

# cp $MD40/MD40_eRO_CLU.fit /data42s/comparat/firefly/mocks/2020-03/HMD/ 
# cp $MD10/MD10_eRO_CLU.fit /data42s/comparat/firefly/mocks/2020-03/MDPL2/ 
# cp $MD04/MD04_eRO_CLU.fit /data42s/comparat/firefly/mocks/2020-03/SMDPL/ 
# 
# cp $MD40/MD40_eRO_CLU_GAL.fit /data42s/comparat/firefly/mocks/2020-03/HMD/ 
# cp $MD10/MD10_eRO_CLU_GAL.fit /data42s/comparat/firefly/mocks/2020-03/MDPL2/ 
# cp $MD04/MD04_eRO_CLU_GAL.fit /data42s/comparat/firefly/mocks/2020-03/SMDPL/ 


# # # # creates BCG and CGAL catalogs based on the red galaxy catalogue
# # # # writes catalogues in the 4MOST format
# # # # script: 004_9_4most_catalog.py
# # # # inputs: ${env}/${env}_eRO_CLU_GAL.fit
# # # # outputs: 
# # # #   ${env}/S5_BCG_4MOST.fit
# # # #   ${env}/S5_CGAL_4MOST.fit
# # # # nohup nice -n 19 python 004_9_4most_catalog.py MD04 > logs/004_9_4most_catalog_MD04.log & 
# # # # nohup nice -n 19 python 004_9_4most_catalog.py MD10 > logs/004_9_4most_catalog_MD10.log & 
# # # 
# # # # cp $MD10/S5_BCG_4MOST.fit /data42s/comparat/firefly/mocks/2020-03/MDPL2/
# # # # cp $MD10/S5_CGAL_4MOST.fit /data42s/comparat/firefly/mocks/2020-03/MDPL2/
# # # # 
# # # # cp $MD04/S5_BCG_4MOST.fit /data42s/comparat/firefly/mocks/2020-03/SMDPL/
# # # # cp $MD04/S5_CGAL_4MOST.fit /data42s/comparat/firefly/mocks/2020-03/SMDPL/
# # # 

# Last 4MOST STEP 
# Merges into a full S5 catalogue after the cosmology mocks are made i.e. when the Filament survey mock exists.
# script 004_9_4most_merge.py
# see below in the script
# final file will be 
# $UNIT_fA1i_DIR/S5_4MOST_Apr2021.fit 
# and copied here /data42s/comparat/firefly/mocks/2021-04/QMOST/

cd $GIT_AGN_MOCK/python
python 004_9_4most_merge.py UNIT_fA1i_DIR

cp $UNIT_fA1i_DIR/S5_4MOST_Apr2021.fit /data42s/comparat/firefly/mocks/2021-04/QMOST/ 
cd /data42s/comparat/firefly/mocks/2021-04/QMOST/ 
gzip -k --rsyncable S5_4MOST_Apr2021.fit

