"""
Creates a fits catalog containing the 4FS input columns.

Create the ID array
remove unwanted column
check rulesets and templates names are correct

"""
import glob, sys, time, os
t0 = time.time()
from astropy.table import Table, Column
import numpy as n
from sklearn.neighbors import BallTree
from astropy.coordinates import SkyCoord
import astropy.units as u
import healpy as hp

print('applies S8 footprint to the general catalogues', t0)
print('------------------------------------------------')
print('------------------------------------------------')

root_dir = '/data42s/comparat/firefly/mocks/2021-04/QMOST'
radius = 0.1 / 3600. * n.pi / 180.
deg_to_rad = n.pi/180.
# get survey footprint bitlist
def fourmost_get_survbitlist():
	mydict                = {}
	mydict['des']         = 0
	mydict['kidss']       = 1
	mydict['kidsn']       = 2
	mydict['atlassgcnotdes'] = 3
	mydict['atlasngc']    = 4
	mydict['kabs']        = 5
	mydict['vhsb10']      = 6
	mydict['vhsb15']      = 7
	mydict['vhsb20']      = 8
	mydict['vhsb20clean'] = 9
	mydict['desi']        = 10
	mydict['erosita']     = 11
	mydict['waveswide']   = 12
	mydict['euclid']      = 13
	mydict['s8elg']       = 14
	mydict['s8']          = 15
	return mydict

svbdict = fourmost_get_survbitlist()

#nl = lambda selection : len(selection.nonzero()[0])

dir_2_QSO      = os.path.join(root_dir, 'QSO_4MOST.fits'     )
dir_2_LyA      = os.path.join(root_dir, 'LyA_4MOST.fits'     )
dir_2__BG      = os.path.join(root_dir, 'BG_4MOST.fits'      )
dir_2_LRG      = os.path.join(root_dir, 'LRG_4MOST.fits'     )

dir_2_QSO_out  = os.path.join(root_dir, 'S8_QSO.fits'     )
dir_2_LyA_out  = os.path.join(root_dir, 'S8_LyA.fits'     )
dir_2__BG_out  = os.path.join(root_dir, 'S8_BG.fits'      )
dir_2_LRG_out  = os.path.join(root_dir, 'S8_LRG.fits'     )

print('open catalogs',time.time()-t0)

t_QSO  = Table.read(  dir_2_QSO  )
t_LyA  = Table.read(  dir_2_LyA  )
t__BG  = Table.read(  dir_2__BG  )
t_LRG  = Table.read(  dir_2_LRG  )

common_area = lambda ra, dec : (dec<40)

s_QSO = ( (t_QSO['MASK_BIT'] & 2**svbdict['s8']) >0 ) & common_area(t_QSO['RA'], t_QSO['DEC'])
s_LyA = ( (t_LyA['MASK_BIT'] & 2**svbdict['s8']) >0 ) & common_area(t_LyA['RA'], t_LyA['DEC'])
s__BG = ( (t__BG['MASK_BIT'] & 2**svbdict['s8']) >0 ) & common_area(t__BG['RA'], t__BG['DEC'])
s_LRG = ( (t_LRG['MASK_BIT'] & 2**svbdict['s8']) >0 ) & common_area(t_LRG['RA'], t_LRG['DEC'])

print('writes tables for S8')
t_QSO_out = Table( t_QSO[s_QSO])
t_LyA_out = Table( t_LyA[s_LyA])
t__BG_out = Table( t__BG[s__BG])
t_LRG_out = Table( t_LRG[s_LRG])
t_QSO_out.write( dir_2_QSO_out, overwrite=True)
t_LyA_out.write( dir_2_LyA_out, overwrite=True)
t__BG_out.write( dir_2__BG_out, overwrite=True)
t_LRG_out.write( dir_2_LRG_out, overwrite=True)


