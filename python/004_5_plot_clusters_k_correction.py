"""
What it does
------------

Plots the cluster model fluxes vs luminosity vs. redshift

References
----------

Command to run
--------------

python3 004_5_plot_clusters_k_correction.py environmentVAR

arguments
---------

environmentVAR: environment variable linking to the directory where files are e.g. "MD10"

Dependencies
------------

import time, os, sys, glob, numpy, astropy, scipy, matplotlib

"""
from astropy.table import Table, Column
from scipy.stats import scoreatpercentile
import glob
import sys
from astropy_healpix import healpy
import os
import time
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p

import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
from scipy.stats import norm
from scipy.interpolate import interp1d
import astropy.io.fits as fits
import numpy as n
print('Plots the k correction')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()


env = sys.argv[1]

path_2_CLU_SAT_catalog = os.path.join(os.environ['MD10'], 'MD10_eRO_CLU.fit')
clu_MD10 = fits.open(path_2_CLU_SAT_catalog)
path_2_CLU_SAT_catalog = os.path.join(os.environ['MD04'], 'MD04_eRO_CLU.fit')
clu_MD04 = fits.open(path_2_CLU_SAT_catalog)
path_2_CLU_SAT_catalog = os.path.join(os.environ['MD40'], 'MD40_eRO_CLU.fit')
clu_MD40 = fits.open(path_2_CLU_SAT_catalog)

fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters' )
if os.path.isdir(fig_dir) == False:
	os.system('mkdir -p ' + fig_dir)

# simulation setup
if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

nl = lambda sel : len(sel.nonzero()[0])

fig_out = os.path.join(fig_dir, 'z-LX-FX.png')

ok = (clu_MD10[1].data['redshift_R']<4)
y_MD10 = clu_MD10[1].data['CLUSTER_LX_soft'][ok]
z_MD10 = n.log10(clu_MD10[1].data['CLUSTER_FX_soft'][ok])
#x_MD10 = n.log10(1 + clu_MD10[1].data['redshift_R'][ok])
x_MD10 =  clu_MD10[1].data['redshift_R'][ok]

p.figure(1, (6., 5.))
p.axes([0.15, 0.15, 0.75, 0.75])
p.scatter(x_MD10, y_MD10, c=z_MD10, s=4, marker='+', rasterized=True, vmin=-15.5, vmax=-10.5, cmap='seismic')
#p.legend(frameon=False)
p.xlabel(r'$z$')
#p.xlabel(r'$\log_{10}(1+z)$')
p.ylabel(r'$\log_{10}(L_X\; [erg\; s^{-1}])$')
p.grid()
p.colorbar(label=r'$\log_{10}(F_X\; [erg\; cm^{-2}\; s^{-1}])$')
p.xlim((0.01,4.))
#p.ylim((13.5,15.5))
#p.yscale('log')
p.xscale('log')
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'z-FX-kT.png')

ok = (clu_MD10[1].data['redshift_R']<4)
z_MD10 = clu_MD10[1].data['CLUSTER_kT'][ok]
y_MD10 = n.log10(clu_MD10[1].data['CLUSTER_FX_soft'][ok])
#x_MD10 = n.log10(1 + clu_MD10[1].data['redshift_R'][ok])
x_MD10 =  clu_MD10[1].data['redshift_R'][ok]

p.figure(1, (6., 5.))
p.axes([0.15, 0.15, 0.75, 0.75])
p.scatter(x_MD10, y_MD10, c=z_MD10, s=4, marker='+', rasterized=True, vmin=0.2, vmax=6, cmap='seismic')
#p.legend(frameon=False)
p.xlabel(r'$z$')
#p.xlabel(r'$\log_{10}(1+z)$')
p.ylabel(r'$\log_{10}(F_X\; [erg\; cm^{-2}\; s^{-1}])$')
p.grid()
p.colorbar(label=r'$kT$')
p.xlim((0.01,4.))
#p.ylim((13.5,15.5))
#p.yscale('log')
p.xscale('log')
p.savefig(fig_out)
p.clf()

fig_out = os.path.join(fig_dir, 'z-M500-FX-cuts.png')

ok = (clu_MD10[1].data['redshift_R']<4)
x_MD10 =  clu_MD10[1].data['redshift_R'][ok]
y_MD10 = n.log10(clu_MD10[1].data['HALO_M500c'][ok])
FX = n.log10(clu_MD10[1].data['CLUSTER_FX_soft'][ok])
#x_MD10 = n.log10(1 + clu_MD10[1].data['redshift_R'][ok])

p.figure(1, (6., 5.))
p.axes([0.15, 0.15, 0.75, 0.75])
p.plot(x_MD10, y_MD10, 'k+', rasterized=True, label=str(len(y_MD10))+' all')#r' $M_{500c}>5\times10^{13}M_\odot$')
s1 = (FX > -14)                                                                      
p.plot(x_MD10[s1], y_MD10[s1], 'g+', rasterized=True, label=str(len(y_MD10[s1]))+r' $F_X>-14$')
s1 = (FX > -13.7)
p.plot(x_MD10[s1], y_MD10[s1], 'r+', rasterized=True, label=str(len(y_MD10[s1]))+r' $F_X>-13.7$')
s1 = (FX > -13.3)                                                                    
p.plot(x_MD10[s1], y_MD10[s1], 'b+', rasterized=True, label=str(len(y_MD10[s1]))+r' $F_X>-13.3$')
s1 = (FX > -13)                                                                      
p.plot(x_MD10[s1], y_MD10[s1], 'm+', rasterized=True, label=str(len(y_MD10[s1]))+r' $F_X>-13$')
s1 = (FX > -12.5)                                                                    
p.plot(x_MD10[s1], y_MD10[s1], 'y+', rasterized=True, label=str(len(y_MD10[s1]))+r' $F_X>-12.5$')
#  $\log_{10}(F_X\; [erg\; cm^{-2}\; s^{-1}])>-14.5$')
p.legend(loc=2, fontsize=10)
p.xlabel(r'$z$')
#p.xlabel(r'$\log_{10}(1+z)$')
p.ylabel(r'$\log_{10}(M_{500c} [M_\odot])$')
p.grid()
p.xlim((0.01,4.))
#p.ylim((13.5,15.5))
#p.yscale('log')
p.xscale('log')
p.savefig(fig_out)
p.clf()

sys.exit()

"""
topcat -stilts plot2plane \
   xpix=600 ypix=600 \
   xlog=true ylog=true xlabel=redshift ylabel='HALO M500c' \
    fontsize=14 fontweight=bold \
   xmin=0.02 xmax=1.5 ymin=4.6E13 ymax=2.0E15 \
   legend=true legpos=0.0,1.0 \
   in=/data/data/MultiDark/MD_1.0Gpc/MD10_eRO_CLU_b10_CM_0_pixS_20.0.fits \
    x=redshift_R y=HALO_M500c nlevel=4 smooth=37 \
   layer_1=Contour \
      icmd_1='select log10(CLUSTER_FX_soft)>-14' \
      color_1=blue zero_1=0.14  \
      leglabel_1='-14' \
   layer_2=Contour \
      icmd_2='select log10(CLUSTER_FX_soft)>-13.7' \
      color_2=green zero_2=0.14 \
      leglabel_2='-13.7' \
   layer_3=Contour \
      icmd_3='select log10(CLUSTER_FX_soft)>-13.3' \
      color_3=grey zero_3=0.14 \
      leglabel_3='-13.3' \
   layer_4=Contour \
      icmd_4='select log10(CLUSTER_FX_soft)>-13' \
      color_4=magenta zero_4=0.14 \
      leglabel_4='-13' \
   layer_5=Contour \
      icmd_5='select log10(CLUSTER_FX_soft)>-12.5' \
      color_5=cyan zero_5=0.14 \
      leglabel_5='-12.5' 
"""

x_MD10, y_MD10, FX_min_MD10 = get_x_y(clu_MD10, 600000)

fig_out = os.path.join(fig_dir, 'M500c-z-1em14.png')

p.figure(1, (7., 5.))
p.axes([0.2, 0.2, 0.75, 0.75])
p.plot(x_MD10, y_MD10, 'm+', label='MDPL2, 600,000 clusters with F_X>$10^{'+str(n.round(n.log10(FX_min_MD10),2))+'}$', rasterized=True, alpha=0.1)
p.plot(spt[1].data['z'], n.log10(spt[1].data['M500c']*1e14),  'kx', label='SPT', rasterized=True)
p.plot(z_HI, m_HI, 'gs', label='HIFLUGCS', rasterized=True)
p.plot(z_XC, m_XC, 'b*', label='X-COP', rasterized=True)
p.plot(z_XX, m_XX, 'ro', label='XXL', rasterized=True)
p.legend(frameon=False)
p.xlabel('redshift')
p.ylabel(r'$\log_{10}(M_{500c}/M_\odot)$')
#p.ylabel('probability distribution function')
p.grid()
p.xlim((0.01,2.1))
p.ylim((13.5,15.5))
# p.yscale('log')
p.xscale('log')
p.savefig(fig_out)
p.clf()

sys.exit()

nl = lambda sel : len(sel.nonzero()[0])
s1 = clu[1].data['CLUSTER_FX_soft']>2e-14
print(nl(s1))
x_MD10, y_MD10 = clu_MD10[1].data['redshift_R'][s1], n.log10(clu_MD10[1].data['HALO_M500c'][s1])

# opti0nal red sequence figure
fig_out = os.path.join(fig_dir, 'M500c-z-1em14.png')

p.figure(1, (6., 6.))

p.plot(x_MD10, y_MD10, 'm+', label='FX>2e-14, 6e5', rasterized=True, alpha=0.1)

p.plot(spt[1].data['z'], n.log10(spt[1].data['M500c']*1e14),  'kx', label='SPT', rasterized=True)
p.plot(z_HI, m_HI, 'gs', label='HIFLUGCS', rasterized=True)
p.plot(z_XC, m_XC, 'b*', label='X-COP', rasterized=True)
p.plot(z_XX, m_XX, 'ro', label='XXL', rasterized=True)


p.legend(frameon=False)
p.xlabel('redshift')
p.ylabel(r'$\log_{10}(M_{500c}/M_\odot)$')
# p.ylabel('probability distribution function')
p.grid()
# p.ylim((0,1.1))
# p.yscale('log')
p.xscale('log')
p.savefig(fig_out)
p.clf()

