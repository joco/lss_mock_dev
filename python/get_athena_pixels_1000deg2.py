import numpy as n
import os, sys, glob
import healpy
N_pixels = healpy.nside2npix(8)

# about 1e6 full sky randoms :
N_data = 1600 * 100 
area_ratio = 1600 * n.pi/129600.
size = int(N_data * 10 / area_ratio) # 10000 # 40000000

uu = n.random.uniform(size=size)
dec = n.arccos(1 - 2 * uu) * 180 / n.pi - 90.
ra = n.random.uniform(size=size) * 2 * n.pi * 180 / n.pi

# selection of the region
selection = lambda ra_val, dec_val :  (abs(dec_val)<10) & (ra_val > 150) & (ra_val < 200 )

in_efeds = selection(ra, dec)

HEALPIX_8 = healpy.ang2pix(8, n.pi/2. - dec[in_efeds]*n.pi/180. , ra[in_efeds]*n.pi/180. , nest=True)
DATA = n.transpose(n.unique(HEALPIX_8))
n.savetxt(os.path.join( 'list_hpx8_fields_athena_1850deg2.txt'), DATA.astype('int'), fmt='%i' )
