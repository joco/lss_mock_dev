#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 308 
python3 005_2_all_magnitudes.py MD40 309 
python3 005_2_all_magnitudes.py MD40 310 
python3 005_2_all_magnitudes.py MD40 311 
python3 005_2_all_magnitudes.py MD40 312 
python3 005_2_all_magnitudes.py MD40 313 
python3 005_2_all_magnitudes.py MD40 314 
python3 005_2_all_magnitudes.py MD40 315 
python3 005_2_all_magnitudes.py MD40 316 
python3 005_2_all_magnitudes.py MD40 317 
python3 005_2_all_magnitudes.py MD40 318 
python3 005_2_all_magnitudes.py MD40 319 
python3 005_2_all_magnitudes.py MD40 320 
python3 005_2_all_magnitudes.py MD40 321 
 
