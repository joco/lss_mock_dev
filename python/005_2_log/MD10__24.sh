#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 336 
python3 005_2_all_magnitudes.py MD10 337 
python3 005_2_all_magnitudes.py MD10 338 
python3 005_2_all_magnitudes.py MD10 339 
python3 005_2_all_magnitudes.py MD10 340 
python3 005_2_all_magnitudes.py MD10 341 
python3 005_2_all_magnitudes.py MD10 342 
python3 005_2_all_magnitudes.py MD10 343 
python3 005_2_all_magnitudes.py MD10 344 
python3 005_2_all_magnitudes.py MD10 345 
python3 005_2_all_magnitudes.py MD10 346 
python3 005_2_all_magnitudes.py MD10 347 
python3 005_2_all_magnitudes.py MD10 348 
python3 005_2_all_magnitudes.py MD10 349 
 
