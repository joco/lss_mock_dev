#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 112 
python3 005_2_all_magnitudes.py MD40 113 
python3 005_2_all_magnitudes.py MD40 114 
python3 005_2_all_magnitudes.py MD40 115 
python3 005_2_all_magnitudes.py MD40 116 
python3 005_2_all_magnitudes.py MD40 117 
python3 005_2_all_magnitudes.py MD40 118 
python3 005_2_all_magnitudes.py MD40 119 
python3 005_2_all_magnitudes.py MD40 120 
python3 005_2_all_magnitudes.py MD40 121 
python3 005_2_all_magnitudes.py MD40 122 
python3 005_2_all_magnitudes.py MD40 123 
python3 005_2_all_magnitudes.py MD40 124 
python3 005_2_all_magnitudes.py MD40 125 
 
