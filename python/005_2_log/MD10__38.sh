#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 532 
python3 005_2_all_magnitudes.py MD10 533 
python3 005_2_all_magnitudes.py MD10 534 
python3 005_2_all_magnitudes.py MD10 535 
python3 005_2_all_magnitudes.py MD10 536 
python3 005_2_all_magnitudes.py MD10 537 
python3 005_2_all_magnitudes.py MD10 538 
python3 005_2_all_magnitudes.py MD10 539 
python3 005_2_all_magnitudes.py MD10 540 
python3 005_2_all_magnitudes.py MD10 541 
python3 005_2_all_magnitudes.py MD10 542 
python3 005_2_all_magnitudes.py MD10 543 
python3 005_2_all_magnitudes.py MD10 544 
python3 005_2_all_magnitudes.py MD10 545 
 
