#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 322 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 323 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 324 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 325 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 326 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 327 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 328 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 329 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 330 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 331 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 332 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 333 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 334 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 335 
 
