#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 616 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 617 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 618 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 619 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 620 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 621 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 622 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 623 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 624 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 625 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 626 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 627 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 628 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 629 
 
