#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 294 
python3 005_2_all_magnitudes.py MD40 295 
python3 005_2_all_magnitudes.py MD40 296 
python3 005_2_all_magnitudes.py MD40 297 
python3 005_2_all_magnitudes.py MD40 298 
python3 005_2_all_magnitudes.py MD40 299 
python3 005_2_all_magnitudes.py MD40 300 
python3 005_2_all_magnitudes.py MD40 301 
python3 005_2_all_magnitudes.py MD40 302 
python3 005_2_all_magnitudes.py MD40 303 
python3 005_2_all_magnitudes.py MD40 304 
python3 005_2_all_magnitudes.py MD40 305 
python3 005_2_all_magnitudes.py MD40 306 
python3 005_2_all_magnitudes.py MD40 307 
 
