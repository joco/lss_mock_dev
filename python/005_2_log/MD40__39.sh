#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 546 
python3 005_2_all_magnitudes.py MD40 547 
python3 005_2_all_magnitudes.py MD40 548 
python3 005_2_all_magnitudes.py MD40 549 
python3 005_2_all_magnitudes.py MD40 550 
python3 005_2_all_magnitudes.py MD40 551 
python3 005_2_all_magnitudes.py MD40 552 
python3 005_2_all_magnitudes.py MD40 553 
python3 005_2_all_magnitudes.py MD40 554 
python3 005_2_all_magnitudes.py MD40 555 
python3 005_2_all_magnitudes.py MD40 556 
python3 005_2_all_magnitudes.py MD40 557 
python3 005_2_all_magnitudes.py MD40 558 
python3 005_2_all_magnitudes.py MD40 559 
 
