#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA1_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 532 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 533 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 534 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 535 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 536 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 537 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 538 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 539 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 540 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 541 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 542 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 543 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 544 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 545 
 
