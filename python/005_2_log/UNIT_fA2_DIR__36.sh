#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 504 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 505 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 506 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 507 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 508 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 509 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 510 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 511 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 512 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 513 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 514 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 515 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 516 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 517 
 
