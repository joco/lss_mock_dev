#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 280 
python3 005_2_all_magnitudes.py MD40 281 
python3 005_2_all_magnitudes.py MD40 282 
python3 005_2_all_magnitudes.py MD40 283 
python3 005_2_all_magnitudes.py MD40 284 
python3 005_2_all_magnitudes.py MD40 285 
python3 005_2_all_magnitudes.py MD40 286 
python3 005_2_all_magnitudes.py MD40 287 
python3 005_2_all_magnitudes.py MD40 288 
python3 005_2_all_magnitudes.py MD40 289 
python3 005_2_all_magnitudes.py MD40 290 
python3 005_2_all_magnitudes.py MD40 291 
python3 005_2_all_magnitudes.py MD40 292 
python3 005_2_all_magnitudes.py MD40 293 
 
