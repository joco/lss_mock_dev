#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 434 
python3 005_2_all_magnitudes.py MD40 435 
python3 005_2_all_magnitudes.py MD40 436 
python3 005_2_all_magnitudes.py MD40 437 
python3 005_2_all_magnitudes.py MD40 438 
python3 005_2_all_magnitudes.py MD40 439 
python3 005_2_all_magnitudes.py MD40 440 
python3 005_2_all_magnitudes.py MD40 441 
python3 005_2_all_magnitudes.py MD40 442 
python3 005_2_all_magnitudes.py MD40 443 
python3 005_2_all_magnitudes.py MD40 444 
python3 005_2_all_magnitudes.py MD40 445 
python3 005_2_all_magnitudes.py MD40 446 
python3 005_2_all_magnitudes.py MD40 447 
 
