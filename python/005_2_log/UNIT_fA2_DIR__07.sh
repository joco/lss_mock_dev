#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 98 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 99 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 100 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 101 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 102 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 103 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 104 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 105 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 106 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 107 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 108 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 109 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 110 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 111 
 
