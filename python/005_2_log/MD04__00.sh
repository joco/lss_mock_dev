#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 0 
python3 005_2_all_magnitudes.py MD04 1 
python3 005_2_all_magnitudes.py MD04 2 
python3 005_2_all_magnitudes.py MD04 3 
python3 005_2_all_magnitudes.py MD04 4 
python3 005_2_all_magnitudes.py MD04 5 
python3 005_2_all_magnitudes.py MD04 6 
python3 005_2_all_magnitudes.py MD04 7 
python3 005_2_all_magnitudes.py MD04 8 
python3 005_2_all_magnitudes.py MD04 9 
python3 005_2_all_magnitudes.py MD04 10 
python3 005_2_all_magnitudes.py MD04 11 
python3 005_2_all_magnitudes.py MD04 12 
python3 005_2_all_magnitudes.py MD04 13 
 
