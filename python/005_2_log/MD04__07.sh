#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 98 
python3 005_2_all_magnitudes.py MD04 99 
python3 005_2_all_magnitudes.py MD04 100 
python3 005_2_all_magnitudes.py MD04 101 
python3 005_2_all_magnitudes.py MD04 102 
python3 005_2_all_magnitudes.py MD04 103 
python3 005_2_all_magnitudes.py MD04 104 
python3 005_2_all_magnitudes.py MD04 105 
python3 005_2_all_magnitudes.py MD04 106 
python3 005_2_all_magnitudes.py MD04 107 
python3 005_2_all_magnitudes.py MD04 108 
python3 005_2_all_magnitudes.py MD04 109 
python3 005_2_all_magnitudes.py MD04 110 
python3 005_2_all_magnitudes.py MD04 111 
 
