#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 574 
python3 005_2_all_magnitudes.py MD04 575 
python3 005_2_all_magnitudes.py MD04 576 
python3 005_2_all_magnitudes.py MD04 577 
python3 005_2_all_magnitudes.py MD04 578 
python3 005_2_all_magnitudes.py MD04 579 
python3 005_2_all_magnitudes.py MD04 580 
python3 005_2_all_magnitudes.py MD04 581 
python3 005_2_all_magnitudes.py MD04 582 
python3 005_2_all_magnitudes.py MD04 583 
python3 005_2_all_magnitudes.py MD04 584 
python3 005_2_all_magnitudes.py MD04 585 
python3 005_2_all_magnitudes.py MD04 586 
python3 005_2_all_magnitudes.py MD04 587 
 
