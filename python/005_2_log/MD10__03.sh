#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 42 
python3 005_2_all_magnitudes.py MD10 43 
python3 005_2_all_magnitudes.py MD10 44 
python3 005_2_all_magnitudes.py MD10 45 
python3 005_2_all_magnitudes.py MD10 46 
python3 005_2_all_magnitudes.py MD10 47 
python3 005_2_all_magnitudes.py MD10 48 
python3 005_2_all_magnitudes.py MD10 49 
python3 005_2_all_magnitudes.py MD10 50 
python3 005_2_all_magnitudes.py MD10 51 
python3 005_2_all_magnitudes.py MD10 52 
python3 005_2_all_magnitudes.py MD10 53 
python3 005_2_all_magnitudes.py MD10 54 
python3 005_2_all_magnitudes.py MD10 55 
 
