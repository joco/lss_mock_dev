#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 490 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 491 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 492 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 493 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 494 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 495 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 496 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 497 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 498 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 499 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 500 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 501 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 502 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 503 
 
