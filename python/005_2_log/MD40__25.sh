#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 350 
python3 005_2_all_magnitudes.py MD40 351 
python3 005_2_all_magnitudes.py MD40 352 
python3 005_2_all_magnitudes.py MD40 353 
python3 005_2_all_magnitudes.py MD40 354 
python3 005_2_all_magnitudes.py MD40 355 
python3 005_2_all_magnitudes.py MD40 356 
python3 005_2_all_magnitudes.py MD40 357 
python3 005_2_all_magnitudes.py MD40 358 
python3 005_2_all_magnitudes.py MD40 359 
python3 005_2_all_magnitudes.py MD40 360 
python3 005_2_all_magnitudes.py MD40 361 
python3 005_2_all_magnitudes.py MD40 362 
python3 005_2_all_magnitudes.py MD40 363 
 
