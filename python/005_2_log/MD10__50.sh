#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 700 
python3 005_2_all_magnitudes.py MD10 701 
python3 005_2_all_magnitudes.py MD10 702 
python3 005_2_all_magnitudes.py MD10 703 
python3 005_2_all_magnitudes.py MD10 704 
python3 005_2_all_magnitudes.py MD10 705 
python3 005_2_all_magnitudes.py MD10 706 
python3 005_2_all_magnitudes.py MD10 707 
python3 005_2_all_magnitudes.py MD10 708 
python3 005_2_all_magnitudes.py MD10 709 
python3 005_2_all_magnitudes.py MD10 710 
python3 005_2_all_magnitudes.py MD10 711 
python3 005_2_all_magnitudes.py MD10 712 
python3 005_2_all_magnitudes.py MD10 713 
 
