#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA1i_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 224 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 225 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 226 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 227 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 228 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 229 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 230 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 231 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 232 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 233 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 234 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 235 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 236 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 237 
 
