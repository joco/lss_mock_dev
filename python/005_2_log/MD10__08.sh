#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 112 
python3 005_2_all_magnitudes.py MD10 113 
python3 005_2_all_magnitudes.py MD10 114 
python3 005_2_all_magnitudes.py MD10 115 
python3 005_2_all_magnitudes.py MD10 116 
python3 005_2_all_magnitudes.py MD10 117 
python3 005_2_all_magnitudes.py MD10 118 
python3 005_2_all_magnitudes.py MD10 119 
python3 005_2_all_magnitudes.py MD10 120 
python3 005_2_all_magnitudes.py MD10 121 
python3 005_2_all_magnitudes.py MD10 122 
python3 005_2_all_magnitudes.py MD10 123 
python3 005_2_all_magnitudes.py MD10 124 
python3 005_2_all_magnitudes.py MD10 125 
 
