#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 140 
python3 005_2_all_magnitudes.py MD40 141 
python3 005_2_all_magnitudes.py MD40 142 
python3 005_2_all_magnitudes.py MD40 143 
python3 005_2_all_magnitudes.py MD40 144 
python3 005_2_all_magnitudes.py MD40 145 
python3 005_2_all_magnitudes.py MD40 146 
python3 005_2_all_magnitudes.py MD40 147 
python3 005_2_all_magnitudes.py MD40 148 
python3 005_2_all_magnitudes.py MD40 149 
python3 005_2_all_magnitudes.py MD40 150 
python3 005_2_all_magnitudes.py MD40 151 
python3 005_2_all_magnitudes.py MD40 152 
python3 005_2_all_magnitudes.py MD40 153 
 
