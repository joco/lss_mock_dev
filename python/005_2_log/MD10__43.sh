#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 602 
python3 005_2_all_magnitudes.py MD10 603 
python3 005_2_all_magnitudes.py MD10 604 
python3 005_2_all_magnitudes.py MD10 605 
python3 005_2_all_magnitudes.py MD10 606 
python3 005_2_all_magnitudes.py MD10 607 
python3 005_2_all_magnitudes.py MD10 608 
python3 005_2_all_magnitudes.py MD10 609 
python3 005_2_all_magnitudes.py MD10 610 
python3 005_2_all_magnitudes.py MD10 611 
python3 005_2_all_magnitudes.py MD10 612 
python3 005_2_all_magnitudes.py MD10 613 
python3 005_2_all_magnitudes.py MD10 614 
python3 005_2_all_magnitudes.py MD10 615 
 
