#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 322 
python3 005_2_all_magnitudes.py MD40 323 
python3 005_2_all_magnitudes.py MD40 324 
python3 005_2_all_magnitudes.py MD40 325 
python3 005_2_all_magnitudes.py MD40 326 
python3 005_2_all_magnitudes.py MD40 327 
python3 005_2_all_magnitudes.py MD40 328 
python3 005_2_all_magnitudes.py MD40 329 
python3 005_2_all_magnitudes.py MD40 330 
python3 005_2_all_magnitudes.py MD40 331 
python3 005_2_all_magnitudes.py MD40 332 
python3 005_2_all_magnitudes.py MD40 333 
python3 005_2_all_magnitudes.py MD40 334 
python3 005_2_all_magnitudes.py MD40 335 
 
