#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 406 
python3 005_2_all_magnitudes.py MD10 407 
python3 005_2_all_magnitudes.py MD10 408 
python3 005_2_all_magnitudes.py MD10 409 
python3 005_2_all_magnitudes.py MD10 410 
python3 005_2_all_magnitudes.py MD10 411 
python3 005_2_all_magnitudes.py MD10 412 
python3 005_2_all_magnitudes.py MD10 413 
python3 005_2_all_magnitudes.py MD10 414 
python3 005_2_all_magnitudes.py MD10 415 
python3 005_2_all_magnitudes.py MD10 416 
python3 005_2_all_magnitudes.py MD10 417 
python3 005_2_all_magnitudes.py MD10 418 
python3 005_2_all_magnitudes.py MD10 419 
 
