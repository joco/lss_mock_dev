#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 546 
python3 005_2_all_magnitudes.py MD10 547 
python3 005_2_all_magnitudes.py MD10 548 
python3 005_2_all_magnitudes.py MD10 549 
python3 005_2_all_magnitudes.py MD10 550 
python3 005_2_all_magnitudes.py MD10 551 
python3 005_2_all_magnitudes.py MD10 552 
python3 005_2_all_magnitudes.py MD10 553 
python3 005_2_all_magnitudes.py MD10 554 
python3 005_2_all_magnitudes.py MD10 555 
python3 005_2_all_magnitudes.py MD10 556 
python3 005_2_all_magnitudes.py MD10 557 
python3 005_2_all_magnitudes.py MD10 558 
python3 005_2_all_magnitudes.py MD10 559 
 
