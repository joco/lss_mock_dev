#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 252 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 253 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 254 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 255 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 256 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 257 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 258 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 259 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 260 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 261 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 262 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 263 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 264 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 265 
 
