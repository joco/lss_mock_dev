#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 210 
python3 005_2_all_magnitudes.py MD04 211 
python3 005_2_all_magnitudes.py MD04 212 
python3 005_2_all_magnitudes.py MD04 213 
python3 005_2_all_magnitudes.py MD04 214 
python3 005_2_all_magnitudes.py MD04 215 
python3 005_2_all_magnitudes.py MD04 216 
python3 005_2_all_magnitudes.py MD04 217 
python3 005_2_all_magnitudes.py MD04 218 
python3 005_2_all_magnitudes.py MD04 219 
python3 005_2_all_magnitudes.py MD04 220 
python3 005_2_all_magnitudes.py MD04 221 
python3 005_2_all_magnitudes.py MD04 222 
python3 005_2_all_magnitudes.py MD04 223 
 
