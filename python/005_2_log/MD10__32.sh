#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 448 
python3 005_2_all_magnitudes.py MD10 449 
python3 005_2_all_magnitudes.py MD10 450 
python3 005_2_all_magnitudes.py MD10 451 
python3 005_2_all_magnitudes.py MD10 452 
python3 005_2_all_magnitudes.py MD10 453 
python3 005_2_all_magnitudes.py MD10 454 
python3 005_2_all_magnitudes.py MD10 455 
python3 005_2_all_magnitudes.py MD10 456 
python3 005_2_all_magnitudes.py MD10 457 
python3 005_2_all_magnitudes.py MD10 458 
python3 005_2_all_magnitudes.py MD10 459 
python3 005_2_all_magnitudes.py MD10 460 
python3 005_2_all_magnitudes.py MD10 461 
 
