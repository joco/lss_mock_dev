#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 756 
python3 005_2_all_magnitudes.py MD04 757 
python3 005_2_all_magnitudes.py MD04 758 
python3 005_2_all_magnitudes.py MD04 759 
python3 005_2_all_magnitudes.py MD04 760 
python3 005_2_all_magnitudes.py MD04 761 
python3 005_2_all_magnitudes.py MD04 762 
python3 005_2_all_magnitudes.py MD04 763 
python3 005_2_all_magnitudes.py MD04 764 
python3 005_2_all_magnitudes.py MD04 765 
python3 005_2_all_magnitudes.py MD04 766 
python3 005_2_all_magnitudes.py MD04 767 
 
