#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 728 
python3 005_2_all_magnitudes.py MD04 729 
python3 005_2_all_magnitudes.py MD04 730 
python3 005_2_all_magnitudes.py MD04 731 
python3 005_2_all_magnitudes.py MD04 732 
python3 005_2_all_magnitudes.py MD04 733 
python3 005_2_all_magnitudes.py MD04 734 
python3 005_2_all_magnitudes.py MD04 735 
python3 005_2_all_magnitudes.py MD04 736 
python3 005_2_all_magnitudes.py MD04 737 
python3 005_2_all_magnitudes.py MD04 738 
python3 005_2_all_magnitudes.py MD04 739 
python3 005_2_all_magnitudes.py MD04 740 
python3 005_2_all_magnitudes.py MD04 741 
 
