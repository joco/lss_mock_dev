#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 14 
python3 005_2_all_magnitudes.py MD04 15 
python3 005_2_all_magnitudes.py MD04 16 
python3 005_2_all_magnitudes.py MD04 17 
python3 005_2_all_magnitudes.py MD04 18 
python3 005_2_all_magnitudes.py MD04 19 
python3 005_2_all_magnitudes.py MD04 20 
python3 005_2_all_magnitudes.py MD04 21 
python3 005_2_all_magnitudes.py MD04 22 
python3 005_2_all_magnitudes.py MD04 23 
python3 005_2_all_magnitudes.py MD04 24 
python3 005_2_all_magnitudes.py MD04 25 
python3 005_2_all_magnitudes.py MD04 26 
python3 005_2_all_magnitudes.py MD04 27 
 
