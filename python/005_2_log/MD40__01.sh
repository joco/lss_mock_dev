#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 14 
python3 005_2_all_magnitudes.py MD40 15 
python3 005_2_all_magnitudes.py MD40 16 
python3 005_2_all_magnitudes.py MD40 17 
python3 005_2_all_magnitudes.py MD40 18 
python3 005_2_all_magnitudes.py MD40 19 
python3 005_2_all_magnitudes.py MD40 20 
python3 005_2_all_magnitudes.py MD40 21 
python3 005_2_all_magnitudes.py MD40 22 
python3 005_2_all_magnitudes.py MD40 23 
python3 005_2_all_magnitudes.py MD40 24 
python3 005_2_all_magnitudes.py MD40 25 
python3 005_2_all_magnitudes.py MD40 26 
python3 005_2_all_magnitudes.py MD40 27 
 
