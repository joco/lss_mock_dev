#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 392 
python3 005_2_all_magnitudes.py MD10 393 
python3 005_2_all_magnitudes.py MD10 394 
python3 005_2_all_magnitudes.py MD10 395 
python3 005_2_all_magnitudes.py MD10 396 
python3 005_2_all_magnitudes.py MD10 397 
python3 005_2_all_magnitudes.py MD10 398 
python3 005_2_all_magnitudes.py MD10 399 
python3 005_2_all_magnitudes.py MD10 400 
python3 005_2_all_magnitudes.py MD10 401 
python3 005_2_all_magnitudes.py MD10 402 
python3 005_2_all_magnitudes.py MD10 403 
python3 005_2_all_magnitudes.py MD10 404 
python3 005_2_all_magnitudes.py MD10 405 
 
