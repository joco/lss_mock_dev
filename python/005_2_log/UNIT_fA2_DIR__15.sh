#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 210 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 211 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 212 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 213 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 214 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 215 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 216 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 217 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 218 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 219 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 220 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 221 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 222 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 223 
 
