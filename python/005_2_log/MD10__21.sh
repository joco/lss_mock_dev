#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 294 
python3 005_2_all_magnitudes.py MD10 295 
python3 005_2_all_magnitudes.py MD10 296 
python3 005_2_all_magnitudes.py MD10 297 
python3 005_2_all_magnitudes.py MD10 298 
python3 005_2_all_magnitudes.py MD10 299 
python3 005_2_all_magnitudes.py MD10 300 
python3 005_2_all_magnitudes.py MD10 301 
python3 005_2_all_magnitudes.py MD10 302 
python3 005_2_all_magnitudes.py MD10 303 
python3 005_2_all_magnitudes.py MD10 304 
python3 005_2_all_magnitudes.py MD10 305 
python3 005_2_all_magnitudes.py MD10 306 
python3 005_2_all_magnitudes.py MD10 307 
 
