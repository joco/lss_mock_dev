#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 476 
python3 005_2_all_magnitudes.py MD10 477 
python3 005_2_all_magnitudes.py MD10 478 
python3 005_2_all_magnitudes.py MD10 479 
python3 005_2_all_magnitudes.py MD10 480 
python3 005_2_all_magnitudes.py MD10 481 
python3 005_2_all_magnitudes.py MD10 482 
python3 005_2_all_magnitudes.py MD10 483 
python3 005_2_all_magnitudes.py MD10 484 
python3 005_2_all_magnitudes.py MD10 485 
python3 005_2_all_magnitudes.py MD10 486 
python3 005_2_all_magnitudes.py MD10 487 
python3 005_2_all_magnitudes.py MD10 488 
python3 005_2_all_magnitudes.py MD10 489 
 
