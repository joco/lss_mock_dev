#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 602 
python3 005_2_all_magnitudes.py MD04 603 
python3 005_2_all_magnitudes.py MD04 604 
python3 005_2_all_magnitudes.py MD04 605 
python3 005_2_all_magnitudes.py MD04 606 
python3 005_2_all_magnitudes.py MD04 607 
python3 005_2_all_magnitudes.py MD04 608 
python3 005_2_all_magnitudes.py MD04 609 
python3 005_2_all_magnitudes.py MD04 610 
python3 005_2_all_magnitudes.py MD04 611 
python3 005_2_all_magnitudes.py MD04 612 
python3 005_2_all_magnitudes.py MD04 613 
python3 005_2_all_magnitudes.py MD04 614 
python3 005_2_all_magnitudes.py MD04 615 
 
