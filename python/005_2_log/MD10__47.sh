#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 658 
python3 005_2_all_magnitudes.py MD10 659 
python3 005_2_all_magnitudes.py MD10 660 
python3 005_2_all_magnitudes.py MD10 661 
python3 005_2_all_magnitudes.py MD10 662 
python3 005_2_all_magnitudes.py MD10 663 
python3 005_2_all_magnitudes.py MD10 664 
python3 005_2_all_magnitudes.py MD10 665 
python3 005_2_all_magnitudes.py MD10 666 
python3 005_2_all_magnitudes.py MD10 667 
python3 005_2_all_magnitudes.py MD10 668 
python3 005_2_all_magnitudes.py MD10 669 
python3 005_2_all_magnitudes.py MD10 670 
python3 005_2_all_magnitudes.py MD10 671 
 
