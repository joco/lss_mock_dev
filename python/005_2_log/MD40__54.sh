#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 756 
python3 005_2_all_magnitudes.py MD40 757 
python3 005_2_all_magnitudes.py MD40 758 
python3 005_2_all_magnitudes.py MD40 759 
python3 005_2_all_magnitudes.py MD40 760 
python3 005_2_all_magnitudes.py MD40 761 
python3 005_2_all_magnitudes.py MD40 762 
python3 005_2_all_magnitudes.py MD40 763 
python3 005_2_all_magnitudes.py MD40 764 
python3 005_2_all_magnitudes.py MD40 765 
python3 005_2_all_magnitudes.py MD40 766 
python3 005_2_all_magnitudes.py MD40 767 
 
