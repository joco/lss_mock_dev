#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 266 
python3 005_2_all_magnitudes.py MD40 267 
python3 005_2_all_magnitudes.py MD40 268 
python3 005_2_all_magnitudes.py MD40 269 
python3 005_2_all_magnitudes.py MD40 270 
python3 005_2_all_magnitudes.py MD40 271 
python3 005_2_all_magnitudes.py MD40 272 
python3 005_2_all_magnitudes.py MD40 273 
python3 005_2_all_magnitudes.py MD40 274 
python3 005_2_all_magnitudes.py MD40 275 
python3 005_2_all_magnitudes.py MD40 276 
python3 005_2_all_magnitudes.py MD40 277 
python3 005_2_all_magnitudes.py MD40 278 
python3 005_2_all_magnitudes.py MD40 279 
 
