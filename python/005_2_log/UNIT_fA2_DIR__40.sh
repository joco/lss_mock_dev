#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 560 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 561 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 562 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 563 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 564 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 565 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 566 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 567 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 568 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 569 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 570 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 571 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 572 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 573 
 
