#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 630 
python3 005_2_all_magnitudes.py MD40 631 
python3 005_2_all_magnitudes.py MD40 632 
python3 005_2_all_magnitudes.py MD40 633 
python3 005_2_all_magnitudes.py MD40 634 
python3 005_2_all_magnitudes.py MD40 635 
python3 005_2_all_magnitudes.py MD40 636 
python3 005_2_all_magnitudes.py MD40 637 
python3 005_2_all_magnitudes.py MD40 638 
python3 005_2_all_magnitudes.py MD40 639 
python3 005_2_all_magnitudes.py MD40 640 
python3 005_2_all_magnitudes.py MD40 641 
python3 005_2_all_magnitudes.py MD40 642 
python3 005_2_all_magnitudes.py MD40 643 
 
