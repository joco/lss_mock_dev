#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 168 
python3 005_2_all_magnitudes.py MD10 169 
python3 005_2_all_magnitudes.py MD10 170 
python3 005_2_all_magnitudes.py MD10 171 
python3 005_2_all_magnitudes.py MD10 172 
python3 005_2_all_magnitudes.py MD10 173 
python3 005_2_all_magnitudes.py MD10 174 
python3 005_2_all_magnitudes.py MD10 175 
python3 005_2_all_magnitudes.py MD10 176 
python3 005_2_all_magnitudes.py MD10 177 
python3 005_2_all_magnitudes.py MD10 178 
python3 005_2_all_magnitudes.py MD10 179 
python3 005_2_all_magnitudes.py MD10 180 
python3 005_2_all_magnitudes.py MD10 181 
 
