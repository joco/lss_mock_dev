#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 686 
python3 005_2_all_magnitudes.py MD10 687 
python3 005_2_all_magnitudes.py MD10 688 
python3 005_2_all_magnitudes.py MD10 689 
python3 005_2_all_magnitudes.py MD10 690 
python3 005_2_all_magnitudes.py MD10 691 
python3 005_2_all_magnitudes.py MD10 692 
python3 005_2_all_magnitudes.py MD10 693 
python3 005_2_all_magnitudes.py MD10 694 
python3 005_2_all_magnitudes.py MD10 695 
python3 005_2_all_magnitudes.py MD10 696 
python3 005_2_all_magnitudes.py MD10 697 
python3 005_2_all_magnitudes.py MD10 698 
python3 005_2_all_magnitudes.py MD10 699 
 
