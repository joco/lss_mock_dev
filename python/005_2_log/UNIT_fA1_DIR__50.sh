#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA1_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 700 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 701 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 702 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 703 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 704 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 705 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 706 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 707 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 708 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 709 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 710 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 711 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 712 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 713 
 
