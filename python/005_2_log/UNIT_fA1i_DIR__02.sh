#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA1i_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 28 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 29 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 30 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 31 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 32 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 33 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 34 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 35 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 36 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 37 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 38 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 39 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 40 
python3 005_2_all_magnitudes.py UNIT_fA1i_DIR 41 
 
