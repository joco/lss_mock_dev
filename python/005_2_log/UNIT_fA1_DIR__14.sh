#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA1_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 196 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 197 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 198 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 199 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 200 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 201 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 202 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 203 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 204 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 205 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 206 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 207 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 208 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 209 
 
