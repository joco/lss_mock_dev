#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 70 
python3 005_2_all_magnitudes.py MD10 71 
python3 005_2_all_magnitudes.py MD10 72 
python3 005_2_all_magnitudes.py MD10 73 
python3 005_2_all_magnitudes.py MD10 74 
python3 005_2_all_magnitudes.py MD10 75 
python3 005_2_all_magnitudes.py MD10 76 
python3 005_2_all_magnitudes.py MD10 77 
python3 005_2_all_magnitudes.py MD10 78 
python3 005_2_all_magnitudes.py MD10 79 
python3 005_2_all_magnitudes.py MD10 80 
python3 005_2_all_magnitudes.py MD10 81 
python3 005_2_all_magnitudes.py MD10 82 
python3 005_2_all_magnitudes.py MD10 83 
 
