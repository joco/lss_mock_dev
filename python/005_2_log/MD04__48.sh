#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 672 
python3 005_2_all_magnitudes.py MD04 673 
python3 005_2_all_magnitudes.py MD04 674 
python3 005_2_all_magnitudes.py MD04 675 
python3 005_2_all_magnitudes.py MD04 676 
python3 005_2_all_magnitudes.py MD04 677 
python3 005_2_all_magnitudes.py MD04 678 
python3 005_2_all_magnitudes.py MD04 679 
python3 005_2_all_magnitudes.py MD04 680 
python3 005_2_all_magnitudes.py MD04 681 
python3 005_2_all_magnitudes.py MD04 682 
python3 005_2_all_magnitudes.py MD04 683 
python3 005_2_all_magnitudes.py MD04 684 
python3 005_2_all_magnitudes.py MD04 685 
 
