#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 42 
python3 005_2_all_magnitudes.py MD04 43 
python3 005_2_all_magnitudes.py MD04 44 
python3 005_2_all_magnitudes.py MD04 45 
python3 005_2_all_magnitudes.py MD04 46 
python3 005_2_all_magnitudes.py MD04 47 
python3 005_2_all_magnitudes.py MD04 48 
python3 005_2_all_magnitudes.py MD04 49 
python3 005_2_all_magnitudes.py MD04 50 
python3 005_2_all_magnitudes.py MD04 51 
python3 005_2_all_magnitudes.py MD04 52 
python3 005_2_all_magnitudes.py MD04 53 
python3 005_2_all_magnitudes.py MD04 54 
python3 005_2_all_magnitudes.py MD04 55 
 
