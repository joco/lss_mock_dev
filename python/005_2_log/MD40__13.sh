#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 182 
python3 005_2_all_magnitudes.py MD40 183 
python3 005_2_all_magnitudes.py MD40 184 
python3 005_2_all_magnitudes.py MD40 185 
python3 005_2_all_magnitudes.py MD40 186 
python3 005_2_all_magnitudes.py MD40 187 
python3 005_2_all_magnitudes.py MD40 188 
python3 005_2_all_magnitudes.py MD40 189 
python3 005_2_all_magnitudes.py MD40 190 
python3 005_2_all_magnitudes.py MD40 191 
python3 005_2_all_magnitudes.py MD40 192 
python3 005_2_all_magnitudes.py MD40 193 
python3 005_2_all_magnitudes.py MD40 194 
python3 005_2_all_magnitudes.py MD40 195 
 
