#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 504 
python3 005_2_all_magnitudes.py MD40 505 
python3 005_2_all_magnitudes.py MD40 506 
python3 005_2_all_magnitudes.py MD40 507 
python3 005_2_all_magnitudes.py MD40 508 
python3 005_2_all_magnitudes.py MD40 509 
python3 005_2_all_magnitudes.py MD40 510 
python3 005_2_all_magnitudes.py MD40 511 
python3 005_2_all_magnitudes.py MD40 512 
python3 005_2_all_magnitudes.py MD40 513 
python3 005_2_all_magnitudes.py MD40 514 
python3 005_2_all_magnitudes.py MD40 515 
python3 005_2_all_magnitudes.py MD40 516 
python3 005_2_all_magnitudes.py MD40 517 
 
