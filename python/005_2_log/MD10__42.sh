#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 588 
python3 005_2_all_magnitudes.py MD10 589 
python3 005_2_all_magnitudes.py MD10 590 
python3 005_2_all_magnitudes.py MD10 591 
python3 005_2_all_magnitudes.py MD10 592 
python3 005_2_all_magnitudes.py MD10 593 
python3 005_2_all_magnitudes.py MD10 594 
python3 005_2_all_magnitudes.py MD10 595 
python3 005_2_all_magnitudes.py MD10 596 
python3 005_2_all_magnitudes.py MD10 597 
python3 005_2_all_magnitudes.py MD10 598 
python3 005_2_all_magnitudes.py MD10 599 
python3 005_2_all_magnitudes.py MD10 600 
python3 005_2_all_magnitudes.py MD10 601 
 
