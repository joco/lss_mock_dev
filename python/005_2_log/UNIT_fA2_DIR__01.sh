#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 14 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 15 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 16 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 17 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 18 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 19 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 20 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 21 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 22 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 23 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 24 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 25 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 26 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 27 
 
