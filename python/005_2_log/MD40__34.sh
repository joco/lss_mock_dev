#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 476 
python3 005_2_all_magnitudes.py MD40 477 
python3 005_2_all_magnitudes.py MD40 478 
python3 005_2_all_magnitudes.py MD40 479 
python3 005_2_all_magnitudes.py MD40 480 
python3 005_2_all_magnitudes.py MD40 481 
python3 005_2_all_magnitudes.py MD40 482 
python3 005_2_all_magnitudes.py MD40 483 
python3 005_2_all_magnitudes.py MD40 484 
python3 005_2_all_magnitudes.py MD40 485 
python3 005_2_all_magnitudes.py MD40 486 
python3 005_2_all_magnitudes.py MD40 487 
python3 005_2_all_magnitudes.py MD40 488 
python3 005_2_all_magnitudes.py MD40 489 
 
