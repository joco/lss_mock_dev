#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 630 
python3 005_2_all_magnitudes.py MD10 631 
python3 005_2_all_magnitudes.py MD10 632 
python3 005_2_all_magnitudes.py MD10 633 
python3 005_2_all_magnitudes.py MD10 634 
python3 005_2_all_magnitudes.py MD10 635 
python3 005_2_all_magnitudes.py MD10 636 
python3 005_2_all_magnitudes.py MD10 637 
python3 005_2_all_magnitudes.py MD10 638 
python3 005_2_all_magnitudes.py MD10 639 
python3 005_2_all_magnitudes.py MD10 640 
python3 005_2_all_magnitudes.py MD10 641 
python3 005_2_all_magnitudes.py MD10 642 
python3 005_2_all_magnitudes.py MD10 643 
 
