#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 140 
python3 005_2_all_magnitudes.py MD04 141 
python3 005_2_all_magnitudes.py MD04 142 
python3 005_2_all_magnitudes.py MD04 143 
python3 005_2_all_magnitudes.py MD04 144 
python3 005_2_all_magnitudes.py MD04 145 
python3 005_2_all_magnitudes.py MD04 146 
python3 005_2_all_magnitudes.py MD04 147 
python3 005_2_all_magnitudes.py MD04 148 
python3 005_2_all_magnitudes.py MD04 149 
python3 005_2_all_magnitudes.py MD04 150 
python3 005_2_all_magnitudes.py MD04 151 
python3 005_2_all_magnitudes.py MD04 152 
python3 005_2_all_magnitudes.py MD04 153 
 
