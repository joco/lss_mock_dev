#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 126 
python3 005_2_all_magnitudes.py MD04 127 
python3 005_2_all_magnitudes.py MD04 128 
python3 005_2_all_magnitudes.py MD04 129 
python3 005_2_all_magnitudes.py MD04 130 
python3 005_2_all_magnitudes.py MD04 131 
python3 005_2_all_magnitudes.py MD04 132 
python3 005_2_all_magnitudes.py MD04 133 
python3 005_2_all_magnitudes.py MD04 134 
python3 005_2_all_magnitudes.py MD04 135 
python3 005_2_all_magnitudes.py MD04 136 
python3 005_2_all_magnitudes.py MD04 137 
python3 005_2_all_magnitudes.py MD04 138 
python3 005_2_all_magnitudes.py MD04 139 
 
