#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 112 
python3 005_2_all_magnitudes.py MD04 113 
python3 005_2_all_magnitudes.py MD04 114 
python3 005_2_all_magnitudes.py MD04 115 
python3 005_2_all_magnitudes.py MD04 116 
python3 005_2_all_magnitudes.py MD04 117 
python3 005_2_all_magnitudes.py MD04 118 
python3 005_2_all_magnitudes.py MD04 119 
python3 005_2_all_magnitudes.py MD04 120 
python3 005_2_all_magnitudes.py MD04 121 
python3 005_2_all_magnitudes.py MD04 122 
python3 005_2_all_magnitudes.py MD04 123 
python3 005_2_all_magnitudes.py MD04 124 
python3 005_2_all_magnitudes.py MD04 125 
 
