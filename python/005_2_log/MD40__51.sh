#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 714 
python3 005_2_all_magnitudes.py MD40 715 
python3 005_2_all_magnitudes.py MD40 716 
python3 005_2_all_magnitudes.py MD40 717 
python3 005_2_all_magnitudes.py MD40 718 
python3 005_2_all_magnitudes.py MD40 719 
python3 005_2_all_magnitudes.py MD40 720 
python3 005_2_all_magnitudes.py MD40 721 
python3 005_2_all_magnitudes.py MD40 722 
python3 005_2_all_magnitudes.py MD40 723 
python3 005_2_all_magnitudes.py MD40 724 
python3 005_2_all_magnitudes.py MD40 725 
python3 005_2_all_magnitudes.py MD40 726 
python3 005_2_all_magnitudes.py MD40 727 
 
