#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 42 
python3 005_2_all_magnitudes.py MD40 43 
python3 005_2_all_magnitudes.py MD40 44 
python3 005_2_all_magnitudes.py MD40 45 
python3 005_2_all_magnitudes.py MD40 46 
python3 005_2_all_magnitudes.py MD40 47 
python3 005_2_all_magnitudes.py MD40 48 
python3 005_2_all_magnitudes.py MD40 49 
python3 005_2_all_magnitudes.py MD40 50 
python3 005_2_all_magnitudes.py MD40 51 
python3 005_2_all_magnitudes.py MD40 52 
python3 005_2_all_magnitudes.py MD40 53 
python3 005_2_all_magnitudes.py MD40 54 
python3 005_2_all_magnitudes.py MD40 55 
 
