#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 210 
python3 005_2_all_magnitudes.py MD40 211 
python3 005_2_all_magnitudes.py MD40 212 
python3 005_2_all_magnitudes.py MD40 213 
python3 005_2_all_magnitudes.py MD40 214 
python3 005_2_all_magnitudes.py MD40 215 
python3 005_2_all_magnitudes.py MD40 216 
python3 005_2_all_magnitudes.py MD40 217 
python3 005_2_all_magnitudes.py MD40 218 
python3 005_2_all_magnitudes.py MD40 219 
python3 005_2_all_magnitudes.py MD40 220 
python3 005_2_all_magnitudes.py MD40 221 
python3 005_2_all_magnitudes.py MD40 222 
python3 005_2_all_magnitudes.py MD40 223 
 
