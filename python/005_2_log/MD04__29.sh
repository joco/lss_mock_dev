#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 406 
python3 005_2_all_magnitudes.py MD04 407 
python3 005_2_all_magnitudes.py MD04 408 
python3 005_2_all_magnitudes.py MD04 409 
python3 005_2_all_magnitudes.py MD04 410 
python3 005_2_all_magnitudes.py MD04 411 
python3 005_2_all_magnitudes.py MD04 412 
python3 005_2_all_magnitudes.py MD04 413 
python3 005_2_all_magnitudes.py MD04 414 
python3 005_2_all_magnitudes.py MD04 415 
python3 005_2_all_magnitudes.py MD04 416 
python3 005_2_all_magnitudes.py MD04 417 
python3 005_2_all_magnitudes.py MD04 418 
python3 005_2_all_magnitudes.py MD04 419 
 
