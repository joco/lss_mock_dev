#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 56 
python3 005_2_all_magnitudes.py MD04 57 
python3 005_2_all_magnitudes.py MD04 58 
python3 005_2_all_magnitudes.py MD04 59 
python3 005_2_all_magnitudes.py MD04 60 
python3 005_2_all_magnitudes.py MD04 61 
python3 005_2_all_magnitudes.py MD04 62 
python3 005_2_all_magnitudes.py MD04 63 
python3 005_2_all_magnitudes.py MD04 64 
python3 005_2_all_magnitudes.py MD04 65 
python3 005_2_all_magnitudes.py MD04 66 
python3 005_2_all_magnitudes.py MD04 67 
python3 005_2_all_magnitudes.py MD04 68 
python3 005_2_all_magnitudes.py MD04 69 
 
