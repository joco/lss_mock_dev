#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 182 
python3 005_2_all_magnitudes.py MD10 183 
python3 005_2_all_magnitudes.py MD10 184 
python3 005_2_all_magnitudes.py MD10 185 
python3 005_2_all_magnitudes.py MD10 186 
python3 005_2_all_magnitudes.py MD10 187 
python3 005_2_all_magnitudes.py MD10 188 
python3 005_2_all_magnitudes.py MD10 189 
python3 005_2_all_magnitudes.py MD10 190 
python3 005_2_all_magnitudes.py MD10 191 
python3 005_2_all_magnitudes.py MD10 192 
python3 005_2_all_magnitudes.py MD10 193 
python3 005_2_all_magnitudes.py MD10 194 
python3 005_2_all_magnitudes.py MD10 195 
 
