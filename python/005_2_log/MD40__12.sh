#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 168 
python3 005_2_all_magnitudes.py MD40 169 
python3 005_2_all_magnitudes.py MD40 170 
python3 005_2_all_magnitudes.py MD40 171 
python3 005_2_all_magnitudes.py MD40 172 
python3 005_2_all_magnitudes.py MD40 173 
python3 005_2_all_magnitudes.py MD40 174 
python3 005_2_all_magnitudes.py MD40 175 
python3 005_2_all_magnitudes.py MD40 176 
python3 005_2_all_magnitudes.py MD40 177 
python3 005_2_all_magnitudes.py MD40 178 
python3 005_2_all_magnitudes.py MD40 179 
python3 005_2_all_magnitudes.py MD40 180 
python3 005_2_all_magnitudes.py MD40 181 
 
