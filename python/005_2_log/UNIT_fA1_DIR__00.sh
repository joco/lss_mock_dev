#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA1_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 0 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 1 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 2 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 3 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 4 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 5 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 6 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 7 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 8 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 9 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 10 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 11 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 12 
python3 005_2_all_magnitudes.py UNIT_fA1_DIR 13 
 
