#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 588 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 589 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 590 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 591 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 592 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 593 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 594 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 595 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 596 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 597 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 598 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 599 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 600 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 601 
 
