#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD10_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD10 350 
python3 005_2_all_magnitudes.py MD10 351 
python3 005_2_all_magnitudes.py MD10 352 
python3 005_2_all_magnitudes.py MD10 353 
python3 005_2_all_magnitudes.py MD10 354 
python3 005_2_all_magnitudes.py MD10 355 
python3 005_2_all_magnitudes.py MD10 356 
python3 005_2_all_magnitudes.py MD10 357 
python3 005_2_all_magnitudes.py MD10 358 
python3 005_2_all_magnitudes.py MD10 359 
python3 005_2_all_magnitudes.py MD10 360 
python3 005_2_all_magnitudes.py MD10 361 
python3 005_2_all_magnitudes.py MD10 362 
python3 005_2_all_magnitudes.py MD10 363 
 
