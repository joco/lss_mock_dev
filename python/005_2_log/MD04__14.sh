#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD04_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD04 196 
python3 005_2_all_magnitudes.py MD04 197 
python3 005_2_all_magnitudes.py MD04 198 
python3 005_2_all_magnitudes.py MD04 199 
python3 005_2_all_magnitudes.py MD04 200 
python3 005_2_all_magnitudes.py MD04 201 
python3 005_2_all_magnitudes.py MD04 202 
python3 005_2_all_magnitudes.py MD04 203 
python3 005_2_all_magnitudes.py MD04 204 
python3 005_2_all_magnitudes.py MD04 205 
python3 005_2_all_magnitudes.py MD04 206 
python3 005_2_all_magnitudes.py MD04 207 
python3 005_2_all_magnitudes.py MD04 208 
python3 005_2_all_magnitudes.py MD04 209 
 
