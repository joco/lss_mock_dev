#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 70 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 71 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 72 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 73 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 74 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 75 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 76 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 77 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 78 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 79 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 80 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 81 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 82 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 83 
 
