#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 196 
python3 005_2_all_magnitudes.py MD40 197 
python3 005_2_all_magnitudes.py MD40 198 
python3 005_2_all_magnitudes.py MD40 199 
python3 005_2_all_magnitudes.py MD40 200 
python3 005_2_all_magnitudes.py MD40 201 
python3 005_2_all_magnitudes.py MD40 202 
python3 005_2_all_magnitudes.py MD40 203 
python3 005_2_all_magnitudes.py MD40 204 
python3 005_2_all_magnitudes.py MD40 205 
python3 005_2_all_magnitudes.py MD40 206 
python3 005_2_all_magnitudes.py MD40 207 
python3 005_2_all_magnitudes.py MD40 208 
python3 005_2_all_magnitudes.py MD40 209 
 
