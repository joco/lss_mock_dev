#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=UNIT_fA2_DIR_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 280 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 281 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 282 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 283 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 284 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 285 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 286 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 287 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 288 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 289 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 290 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 291 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 292 
python3 005_2_all_magnitudes.py UNIT_fA2_DIR 293 
 
