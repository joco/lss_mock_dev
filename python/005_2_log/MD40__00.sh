#!/bin/bash 
#SBATCH --time=2000:00:00 
#SBATCH --nodes=1 
#SBATCH --ntasks=1 
#SBATCH --cpus-per-task=1 
#SBATCH --job-name=MD40_ 
 
export OMP_NUM_THREADS=1 
 
cd /home/comparat/software/linux/lss_mock_dev/python 
 
python3 005_2_all_magnitudes.py MD40 0 
python3 005_2_all_magnitudes.py MD40 1 
python3 005_2_all_magnitudes.py MD40 2 
python3 005_2_all_magnitudes.py MD40 3 
python3 005_2_all_magnitudes.py MD40 4 
python3 005_2_all_magnitudes.py MD40 5 
python3 005_2_all_magnitudes.py MD40 6 
python3 005_2_all_magnitudes.py MD40 7 
python3 005_2_all_magnitudes.py MD40 8 
python3 005_2_all_magnitudes.py MD40 9 
python3 005_2_all_magnitudes.py MD40 10 
python3 005_2_all_magnitudes.py MD40 11 
python3 005_2_all_magnitudes.py MD40 12 
python3 005_2_all_magnitudes.py MD40 13 
 
