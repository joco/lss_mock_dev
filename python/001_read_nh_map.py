"""
What it does
------------

Retrieves nH column density with 2 codes

H14PI map. http://adsabs.harvard.edu/abs/2016A\%26A...594A.116H


"""
# import python packages

import sys, os
import healpy
#from dustmaps.planck import PlanckQuery
#from dustmaps.config import config
from astropy.coordinates import SkyCoord
from astropy import wcs
#from scipy.interpolate import interp1d
#import h5py
import astropy.io.fits as fits
from astropy.table import Table, Column
import numpy as n
import time
print('Adds coordinates')
t0 = time.time()
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
from scipy.stats import scoreatpercentile
import matplotlib.pyplot as p

# reads RA, DEC
path_2_CAT = '/data37s/simulation_1/MD/MD_1.0Gpc/MD10_eRO_CLU_b8_CM_0_pixS_20.0_wCount_June2020.fit'
cat = fits.open(path_2_CAT)[1].data
ra = cat['RA']
dec = cat['DEC']
# galactic and ecliptic coordinates
coords = SkyCoord(ra, dec, unit='deg', frame='icrs')
bb_gal = coords.galactic.b.value
ll_gal = coords.galactic.l.value
print('bb_gal', bb_gal[:10], time.time() - t0, 's')
print('ll_gal', ll_gal[:10], time.time() - t0, 's')
bb_ecl = coords.barycentrictrueecliptic.lat
ll_ecl = coords.barycentrictrueecliptic.lon
print('bb_ecl', bb_ecl[:10], time.time() - t0, 's')
print('ll_ecl', ll_ecl[:10], time.time() - t0, 's')


# Data file dependencies
path_2_NH_map = '/data17s/darksim/observations/h1_maps/H14PI/asu.fit'
NH_DATA2 = fits.open(path_2_NH_map)[1].data
nh_val = NH_DATA2['NHI']
print("NH map", time.time() - t0, 's')
# H1 values
HEALPIX = healpy.ang2pix(
    1024,
    n.pi /
    2. -
    bb_gal *
    n.pi /
    180.,
    2 *
    n.pi -
    ll_gal *
    n.pi /
    180.)
NH = nh_val[HEALPIX]
print('NH', NH[:10], time.time() - t0, 's')


from astropy.io import fits
from astropy_healpix import HEALPix
from astropy.coordinates import Galactic, SkyCoord

nhf = fits.open('/data17s/darksim/observations/h1_maps/H14PI/NHI_HPX.fits', 'readonly')
nside = nhf[1].header['NSIDE']  
order = nhf[1].header['ORDERING']
hp = HEALPix(nside=nside, order=order, frame=Galactic())

nhi = nhf[1].data['NHI']
NH2 = nhi[HEALPIX]

coord = SkyCoord(ra=ra, dec=dec, frame="icrs", unit='deg')
vals = hp.interpolate_bilinear_skycoord(coord, nhi)

galactic_NH = vals
sel = (vals>1e19)
bad_NH = (sel==False)
galactic_NH[bad_NH] = 1.000001e19

logNHgrid = n.arange(19.0, 22.6, 0.1)

#nhf.close()

p.figure(0, (8., 5.5))
p.tight_layout()
p.plot(NH2, NH, 'k,', rasterized=True)
p.xscale('log')
p.yscale('log')
p.xlabel('NHI Jeremy, no interpolation')
p.ylabel('NHI Johan, no interpolation')
p.grid()
p.savefig(os.path.join(os.environ['HOME'],'Desktop/test-nh1-no-interpolation.png'))
p.clf()

p.figure(0, (8., 5.5))
p.tight_layout()
p.plot(vals, NH, 'k,', rasterized=True)
p.xscale('log')
p.yscale('log')
p.xlabel('NHI Jeremy, with interpolation')
p.ylabel('NHI Johan, no interpolation')
p.grid()
p.savefig(os.path.join(os.environ['HOME'],'Desktop/test-nh1.png'))
p.clf()

p.figure(0, (8., 5.5))
p.tight_layout()
p.plot(ra[bad_NH], dec[bad_NH], 'k+', rasterized=True)
#p.xscale('log')
#p.yscale('log')
p.xlim((-1,361))
p.ylim((-91, 91))
p.xlabel('RA')
p.ylabel('DEC')
p.grid()
p.savefig(os.path.join(os.environ['HOME'],'Desktop/test-nh1-ra-dec.png'))
p.clf()

