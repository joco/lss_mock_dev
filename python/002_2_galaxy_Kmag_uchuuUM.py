"""
What it does
------------

Add K band magnitude to the GALAXY catalogues

Command to run
--------------

python3 002_2_galaxy_Kmag_uchuuUM.py

Dependencies
------------
import time, os, sys, numpy, scipy, astropy

"""
import time
t0=time.time()
import sys, os, glob
from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column
from scipy.optimize import curve_fit
import astropy.io.fits as fits
import numpy as n
print('CREATES K-mag-abs FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')

env = 'UCHUU'
p_2_catalogues = n.array( glob.glob( os.path.join(os.environ[env], 'GPX8', 'z?p??', 'replication_*_*_*', 'glist.fits') ) )

# Now assigns K band absolute magnitudes :
z0, z1, slope, OO, zpt, scatter = n.loadtxt(os.path.join( os.environ['GIT_AGN_MOCK'], 'data', 'logMs-MK-fits', 'look-up-table-2parameters.txt'), unpack=True)
fun = lambda x, a, b : a * x + b

def add_kmag(p_2_catalogue, p_2_catalogue_out):
	# input catalogue
	t_in = fits.open(p_2_catalogue)
	z_in = t_in[1].data['redshift_R']
	M_in = n.log10(t_in[1].data['sm'])
	N_gals = len(z_in)
	# table to be output
	t_out = Table()
	t_out['K_mag_abs'] = n.zeros(N_gals)
	for zmin, zmax, slope_i, OO_i, zpt_i, scatter_i in zip(z0, z1, slope, OO, zpt, scatter):
		s_gal  = ( z_in >= zmin)  & (z_in <= zmax)
		mag = lambda x : fun( x, slope_i, OO_i)
		if len(t_out ['K_mag_abs'][s_gal])>0:
			t_out ['K_mag_abs'][s_gal]  = mag(M_in[s_gal])
			t_out ['K_mag_abs'][s_gal] += norm.rvs(loc=0, scale=scatter_i, size=len(z_in[s_gal]))
	# padding for higher redshift galaxies
	s_gal  = (z_in > zmax)
	slope_i, OO_i, scatter_i = slope[-1], OO[-1], scatter[-1]
	mag = lambda x : fun( x, slope_i, OO_i)
	if len(t_out ['K_mag_abs'][s_gal])>0:
		t_out ['K_mag_abs'][s_gal]  = mag(M_in[s_gal])
		t_out ['K_mag_abs'][s_gal] += norm.rvs(loc=0, scale=scatter_i, size=len(z_in[s_gal]))

	t_out.write  (p_2_catalogue_out   , overwrite=True)
	print(p_2_catalogue_out, 'written')

for p_2_catalogue in p_2_catalogues:
	p_2_catalogue_out = os.path.join( os.path.dirname(p_2_catalogue), 'klist.fits')
	print(p_2_catalogue, p_2_catalogue_out)
	if os.path.isfile(p_2_catalogue_out)==False:
		add_kmag(p_2_catalogue, p_2_catalogue_out)
