#############################################
#############################################
#############################################
#
#
# AGN MODEL
# 
#
#############################################
#############################################
#############################################

# Most recent update of the code march 10

# tabulate K-correction for AGN model
# machine: ds54 (pyxspec dependency)
# script: $GIT_AGN_MOCK/python/xspec/agn_tabulate_obscured_fraction.py
# inputs: parameters of the AGN X-ray spectral model
# outputs: $GIT_AGN_MOCK/data/xray_k_correction
#    v3_fraction_observed_A15_Obs_hard_Obs_soft_fscat_002.txt
#    v3_redshift0_fraction_observed_A15_RF_hard_Obs_soft_fscat_002.txt
#    v3_fraction_observed_A15_RF_hard_Obs_soft_fscat_002.txt
#    v3_fraction_observed_A15_RF_hard_Obs_hard_fscat_002.txt
# last update: March 4 2020
cd $GIT_AGN_MOCK/python/xspec/
python agn_tabulate_obscured_fraction.py

# tabulate K-correction foir AGN model
# machine: ds54 (pyxspec dependency)
# script: $GIT_AGN_MOCK/python/003_5_agn_create_xspectra.py
# inputs: parameters of the AGN X-ray spectral model
# outputs: X-ray spectra in the simput format: ${env}/cat_AGN_SIMPUT/agn_Xspectra/NH??.?_Z?.?_N*.fits
# last update: Sept 2019
cd $GIT_AGN_MOCK/python/
python 003_5_agn_create_xspectra.py


###########################################
# small sky area AGN model
# AGN clustering in eFEDS. 
###########################################
# Light cone for redshift < 6 & |dec|<10 & 130 < ra < 210 (Small area around eFEDs)
# It keeps only columns to compute the AGN model.
# script: 007_0_EFEDS_WTHETA.py
# inputs: ${env}/fits/all_?.?????.fits, ${env}/fits/all_?.?????_coordinates.fits, ${env}/fits/all_?.?????_galaxy.fits
# outputs: ${env}/fits/all_?.?????_EFEDSwtheta.fits
# nohup nice -n 19  python run_eFEDs_AGN.py MD04            >  logs/EFEDS_AGN_MD04.log &                    
# nohup nice -n 19  python run_eFEDs_AGN.py MD10            >  logs/EFEDS_AGN_MD10.log &                    
# nohup nice -n 19  python run_eFEDs_AGN.py MD40            >  logs/EFEDS_AGN_MD40.log &                    
# nohup nice -n 19  python run_eFEDs_AGN.py UNIT_fA1_DIR    >  logs/EFEDS_AGN_UNIT_fA1_DIR.log &                    
# nohup nice -n 19  python run_eFEDs_AGN.py UNIT_fA2_DIR    >  logs/EFEDS_AGN_UNIT_fA2_DIR.log &                    
# nohup nice -n 19  python run_eFEDs_AGN.py UNIT_fA1i_DIR   >  logs/EFEDS_AGN_UNIT_fA1i_DIR.log &                    

# TOPCAT command to concatenate the eFEDS light cone into a single file
# then the AGN model will be applied and clustering predicted 
# inputs: ${env}/fits/all_?.?????_EFEDSwtheta.fits
# output: ${env}/EFEDS_all.fits
# ls $MD10/fits/*_EFEDSwtheta.fits > lists/md10_efeds.lis
# stilts tcat in=@lists/md10_efeds.lis ifmt=fits omode=out ofmt=fits out=$MD10/EFEDS_all.fits
# rm md10_efeds.lis

# creates eFEDs mocks with different satellites fractions
# Applies the duty cycle
# script: 003_0_agn_EFEDS_WTHETA.py
# input: ${env}/EFEDS_all.fits
# output: ${env}/EFEDS_agn_fsat_*.fits
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA.py MD10 2   >  logs/EFEDS_AGN_MD10_fsat2_DC.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA.py MD10 4   >  logs/EFEDS_AGN_MD10_fsat4_DC.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA.py MD10 6   >  logs/EFEDS_AGN_MD10_fsat6_DC.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA.py MD10 8   >  logs/EFEDS_AGN_MD10_fsat8_DC.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA.py MD10 12  >  logs/EFEDS_AGN_MD10_fsat12_DC.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA.py MD10 14  >  logs/EFEDS_AGN_MD10_fsat14_DC.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA.py MD10 16  >  logs/EFEDS_AGN_MD10_fsat16_DC.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA.py MD10 20  >  logs/EFEDS_AGN_MD10_fsat20_DC.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA.py MD10 10  >  logs/EFEDS_AGN_MD10_fsat10_DC.log &

# Runs the AGN model
# script: 003_0_agn_EFEDS_WTHETA_model.py
# input: ${env}/EFEDS_agn_fsat_*.fits
# output: ${env}/EFEDS_agn_fsat_*.fits (updates)
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA_model.py MD10 2  >  logs/EFEDS_AGN_MD10_fsat2_MODEL.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA_model.py MD10 4  >  logs/EFEDS_AGN_MD10_fsat4_MODEL.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA_model.py MD10 6  >  logs/EFEDS_AGN_MD10_fsat6_MODEL.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA_model.py MD10 8  >  logs/EFEDS_AGN_MD10_fsat8_MODEL.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA_model.py MD10 12 >  logs/EFEDS_AGN_MD10_fsat12_MODEL.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA_model.py MD10 14 >  logs/EFEDS_AGN_MD10_fsat14_MODEL.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA_model.py MD10 16 >  logs/EFEDS_AGN_MD10_fsat16_MODEL.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA_model.py MD10 20 >  logs/EFEDS_AGN_MD10_fsat20_MODEL.log &
# nohup nice -n 19 python 003_0_agn_EFEDS_WTHETA_model.py MD10 10 >  logs/EFEDS_AGN_MD10_fsat10_MODEL.log &

# """
# quick turn around verification of the logNlogS
# git pull
# python 003_0_agn_EFEDS_WTHETA_model.py /data37s/simulation_1/MD/MD_1.0Gpc/EFEDS_agn_fsat_10.fits
# python 003_0_agn_EFEDS_WTHETA_logNlogS.py
# mv $GIT_AGN_MOCK/figures/MD10/eFEDs/logN_logS_AGN_hard.png $GIT_AGN_MOCK/figures/MD10/eFEDs/Mod1_logN_logS_AGN_hard.png
# mv $GIT_AGN_MOCK/figures/MD10/eFEDs/logN_logS_AGN_soft.png $GIT_AGN_MOCK/figures/MD10/eFEDs/Mod1_logN_logS_AGN_soft.png
# git add $GIT_AGN_MOCK/figures/MD10/eFEDs/*
# git commit -am"new logNlogS"
# git push
# """


cd $GIT_AGN_MOCK/python

# 
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.0 1 -15.0 0.01 > logs/test_01_soumya.log & 
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.0 1 -15.0 0.02 > logs/test_02_soumya.log & 
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.0 1 -15.0 0.03 > logs/test_03_soumya.log & 
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.0 1 -15.0 0.04 > logs/test_04_soumya.log & 
# 
# 
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.0 1 -15.0 0.03 1.2 1.8 0.1 0.13 > logs/test_013_soumya.log & 
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.0 1 -15.0 0.03 1.2 1.8 0.1 0.54 > logs/test_054_soumya.log & 
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.0 1 -15.0 0.03 2.2 3.2 0.08 0.09 > logs/test_05_soumya.log & 
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.0 1 -15.0 0.03 2.2 3.2 0.15 0.52 > logs/test_06_soumya.log & 
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.0 1 -15.0 0.03 3.2 4.0 0.15 0.52 > logs/test_07_soumya.log & 

# test 3D K-correction	
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 11 1.0 0 -15.0 0.03 3.7 4.4 0.00 0.03  > logs/test3d_K-correction.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  9 1.0 0 -15.0 0.03 3.7 4.4 0.00 0.03  > logs/test3d_K-correction.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  10 1.0 0 -15.0 0.03 3.7 4.4 0.00 0.03  > logs/test3d_K-correction.log & 
#
#
cd $GIT_AGN_MOCK/python
#
#
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   10 1.0 0 -17.0 0.03 3.7 4.4 0.00 0.03 0 10 4.0 > logs/AGN_UNI_fa1i_04_0.4_zlt4.log &


nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   4 0.4 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_04_0.4_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   6 0.4 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_06_0.4_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   8 0.4 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_08_0.4_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  10 0.4 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_10_0.4_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  12 0.4 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_12_0.4_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  14 0.4 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_14_0.4_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  16 0.4 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_16_0.4_zlt2.log &

nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   4 0.6 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_04_0.6_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   6 0.6 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_06_0.6_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   8 0.6 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_08_0.6_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  10 0.6 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_10_0.6_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  12 0.6 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_12_0.6_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  14 0.6 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_14_0.6_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  16 0.6 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_16_0.6_zlt2.log &

nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   4 0.8 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_04_0.8_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   6 0.8 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_06_0.8_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   8 0.8 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_08_0.8_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  10 0.8 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_10_0.8_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  12 0.8 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_12_0.8_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  14 0.8 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_14_0.8_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  16 0.8 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_16_0.8_zlt2.log &

# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   4 1.0 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_04_1.0_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   6 1.0 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_06_1.0_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   8 1.0 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_08_1.0_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  10 1.0 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_10_1.0_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  12 1.0 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_12_1.0_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  14 1.0 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_14_1.0_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  16 1.0 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_16_1.0_zlt2.log &

# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   4 1.2 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_04_1.2_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   6 1.2 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_06_1.2_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   8 1.2 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_08_1.2_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  10 1.2 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_10_1.2_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  12 1.2 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_12_1.2_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  14 1.2 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_14_1.2_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  16 1.2 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_16_1.2_zlt2.log &

nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   4 1.4 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_04_1.4_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   6 1.4 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_06_1.4_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   8 1.4 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_08_1.4_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  10 1.4 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_10_1.4_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  12 1.4 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_12_1.4_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  14 1.4 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_14_1.4_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  16 1.4 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_16_1.4_zlt2.log &

nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   4 1.6 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_04_1.6_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   6 1.6 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_06_1.6_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR   8 1.6 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_08_1.6_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  10 1.6 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_10_1.6_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  12 1.6 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_12_1.6_zlt2.log &
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  14 1.6 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_14_1.6_zlt2.log &
# nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR  16 1.6 0 -15.0 0.03 3.7 4.4 0.00 0.03 0 768 2.0 > logs/AGN_UNI_fa1i_16_1.6_zlt2.log &



=======
=======


nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 0.2 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_0.2.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 0.3 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_0.3.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 0.4 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_0.4.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 0.5 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_0.5.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 0.6 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_0.6.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 0.7 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_0.7.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 0.8 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_0.8.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 0.9 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_0.9.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 1.0 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_1.0.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 1.1 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_1.1.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 1.2 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_1.2.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 1.3 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_1.3.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 1.4 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_1.4.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 1.5 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_1.5.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 1.6 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_1.6.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 06 1.7 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_06_1.7.log & 


nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 0.2 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_0.2.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 0.3 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_0.3.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 0.4 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_0.4.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 0.5 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_0.5.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 0.6 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_0.6.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 0.7 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_0.7.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 0.8 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_0.8.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 0.9 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_0.9.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 1.0 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_1.0.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 1.1 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_1.1.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 1.2 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_1.2.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 1.3 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_1.3.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 1.4 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_1.4.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 1.5 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_1.5.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 1.6 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_1.6.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 08 1.7 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_08_1.7.log & 

nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 0.2 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_0.2.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 0.3 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_0.3.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 0.4 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_0.4.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 0.5 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_0.5.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 0.6 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_0.6.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 0.7 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_0.7.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 0.8 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_0.8.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 0.9 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_0.9.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 1.0 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_1.0.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 1.1 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_1.1.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 1.2 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_1.2.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 1.3 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_1.3.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 1.4 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_1.4.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 1.5 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_1.5.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 1.6 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_1.6.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 10 1.7 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_10_1.7.log & 

nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 0.2 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_0.2.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 0.3 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_0.3.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 0.4 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_0.4.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 0.5 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_0.5.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 0.6 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_0.6.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 0.7 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_0.7.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 0.8 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_0.8.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 0.9 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_0.9.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 1.0 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_1.0.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 1.1 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_1.1.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 1.2 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_1.2.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 1.3 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_1.3.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 1.4 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_1.4.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 1.5 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_1.5.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 1.6 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_1.6.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 12 1.7 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_12_1.7.log & 

nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 0.2 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_0.2.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 0.3 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_0.3.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 0.4 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_0.4.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 0.5 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_0.5.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 0.6 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_0.6.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 0.7 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_0.7.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 0.8 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_0.8.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 0.9 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_0.9.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 1.0 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_1.0.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 1.1 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_1.1.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 1.2 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_1.2.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 1.3 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_1.3.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 1.4 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_1.4.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 1.5 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_1.5.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 1.6 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_1.6.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py MD10 14 1.7 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_MD10_14_1.7.log & 

nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 0.2 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_0.2.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 0.3 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_0.3.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 0.4 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_0.4.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 0.5 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_0.5.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 0.6 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_0.6.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 0.7 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_0.7.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 0.8 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_0.8.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 0.9 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_0.9.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.0 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_1.0.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.1 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_1.1.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.2 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_1.2.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.3 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_1.3.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.4 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_1.4.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.5 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_1.5.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.6 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_1.6.log & 
nohup python 003_0_agn_EFEDS_WTHETA_model_per_pixel.py UNIT_fA1i_DIR 10 1.7 > logs/log_003_0_agn_EFEDS_WTHETA_model_per_pixel_U1i_10_1.7.log & 

cd $UNIT_fA1i_DIR
ls WTH_fsat_10_sigma_1.0/0000??.fit > lists0_unit_1em17.lis
stilts tcat in=@lists0_unit_1em17.lis ifmt=fits icmd='select "redshift_R>0.05 && redshift_R<0.3 && FX_soft>1e-17"' omode=out ofmt=fits out=test0.fits

cd $UNIT_fA1i_DIR
ls WTH_fsat_10_sigma_1.0/0001??.fit > lists1_unit_1em17.lis
stilts tcat in=@lists1_unit_1em17.lis ifmt=fits icmd='select "redshift_R>0.05 && redshift_R<0.3 && FX_soft>1e-17"' omode=out ofmt=fits out=test1.fits

cd $UNIT_fA1i_DIR
ls WTH_fsat_10_sigma_1.0/0002??.fit > lists2_unit_1em17.lis
stilts tcat in=@lists2_unit_1em17.lis ifmt=fits icmd='select "redshift_R>0.05 && redshift_R<0.3 && FX_soft>1e-17"' omode=out ofmt=fits out=test2.fits

cd $UNIT_fA1i_DIR
ls WTH_fsat_10_sigma_1.0/0003??.fit > lists3_unit_1em17.lis
stilts tcat in=@lists3_unit_1em17.lis ifmt=fits icmd='select "redshift_R>0.05 && redshift_R<0.3 && FX_soft>1e-17"' omode=out ofmt=fits out=test3.fits

cd $UNIT_fA1i_DIR
ls WTH_fsat_10_sigma_1.0/0004??.fit > lists4_unit_1em17.lis
stilts tcat in=@lists4_unit_1em17.lis ifmt=fits icmd='select "redshift_R>0.05 && redshift_R<0.3 && FX_soft>1e-17"' omode=out ofmt=fits out=test4.fits

cd $UNIT_fA1i_DIR
ls WTH_fsat_10_sigma_1.0/0005??.fit > lists5_unit_1em17.lis
stilts tcat in=@lists5_unit_1em17.lis ifmt=fits icmd='select "redshift_R>0.05 && redshift_R<0.3 && FX_soft>1e-17"' omode=out ofmt=fits out=test5.fits

cd $UNIT_fA1i_DIR
ls WTH_fsat_10_sigma_1.0/0006??.fit > lists6_unit_1em17.lis
stilts tcat in=@lists6_unit_1em17.lis ifmt=fits icmd='select "redshift_R>0.05 && redshift_R<0.3 && FX_soft>1e-17"' omode=out ofmt=fits out=test6.fits

cd $UNIT_fA1i_DIR
ls WTH_fsat_10_sigma_1.0/0007??.fit > lists7_unit_1em17.lis
stilts tcat in=@lists7_unit_1em17.lis ifmt=fits icmd='select "redshift_R>0.05 && redshift_R<0.3 && FX_soft>1e-17"' omode=out ofmt=fits out=test7.fits

ls test?.fits > alllist_unit_1em17.lis
stilts tcat in=@alllist_unit_1em17.lis ifmt=fits omode=out ofmt=fits out=WTH_fsat_10_sigma_1.0_005z03_fx_1em17.fits



 # THEN CONCATENATE

ls $MD10/fits/*_EFEDSwtheta.fits > lists/md10_efeds.lis
stilts tcat in=@lists/md10_efeds.lis ifmt=fits omode=out ofmt=fits out=$MD10/EFEDS_all.fits
rm md10_efeds.lis

cd $GIT_AGN_MOCK/python
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_0.2/*.fit >     lists/list_WTH_fsat_10_sigma_0.2.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_0.3/*.fit >     lists/list_WTH_fsat_10_sigma_0.3.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_0.4/*.fit >     lists/list_WTH_fsat_10_sigma_0.4.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_0.5/*.fit >     lists/list_WTH_fsat_10_sigma_0.5.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_0.6/*.fit >     lists/list_WTH_fsat_10_sigma_0.6.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_0.7/*.fit >     lists/list_WTH_fsat_10_sigma_0.7.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_0.8/*.fit >     lists/list_WTH_fsat_10_sigma_0.8.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_0.9/*.fit >     lists/list_WTH_fsat_10_sigma_0.9.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_1.0/*.fit >     lists/list_WTH_fsat_10_sigma_1.0.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_1.1/*.fit >     lists/list_WTH_fsat_10_sigma_1.1.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_1.2/*.fit >     lists/list_WTH_fsat_10_sigma_1.2.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_1.3/*.fit >     lists/list_WTH_fsat_10_sigma_1.3.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_1.4/*.fit >     lists/list_WTH_fsat_10_sigma_1.4.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_1.5/*.fit >     lists/list_WTH_fsat_10_sigma_1.5.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_1.6/*.fit >     lists/list_WTH_fsat_10_sigma_1.6.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_10_sigma_1.7/*.fit >     lists/list_WTH_fsat_10_sigma_1.7.list

cd $GIT_AGN_MOCK/python

ls $UNIT_fA1i_DIR/WTH_fsat_10_sigma_0.4_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_10_sigma_0.4_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_10_sigma_0.6_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_10_sigma_0.6_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_10_sigma_0.8_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_10_sigma_0.8_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_10_sigma_1.0_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_10_sigma_1.0_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_10_sigma_1.2_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_10_sigma_1.2_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_10_sigma_1.4_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_10_sigma_1.4_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_10_sigma_1.6_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_10_sigma_1.6_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_12_sigma_0.4_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_12_sigma_0.4_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_12_sigma_0.6_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_12_sigma_0.6_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_12_sigma_0.8_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_12_sigma_0.8_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_12_sigma_1.0_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_12_sigma_1.0_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_12_sigma_1.2_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_12_sigma_1.2_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_12_sigma_1.4_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_12_sigma_1.4_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_12_sigma_1.6_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_12_sigma_1.6_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_14_sigma_0.4_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_14_sigma_0.4_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_14_sigma_0.6_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_14_sigma_0.6_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_14_sigma_0.8_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_14_sigma_0.8_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_14_sigma_1.0_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_14_sigma_1.0_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_14_sigma_1.2_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_14_sigma_1.2_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_14_sigma_1.4_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_14_sigma_1.4_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_14_sigma_1.6_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_14_sigma_1.6_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_16_sigma_0.4_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_16_sigma_0.4_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_16_sigma_0.6_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_16_sigma_0.6_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_16_sigma_0.8_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_16_sigma_0.8_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_16_sigma_1.0_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_16_sigma_1.0_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_16_sigma_1.2_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_16_sigma_1.2_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_16_sigma_1.4_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_16_sigma_1.4_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_16_sigma_1.6_zmax_2.0_FXmin_-15.0/*.fit  > lists/list_WTH_fsat_16_sigma_1.6_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_4_sigma_0.4_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_4_sigma_0.4_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_4_sigma_0.6_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_4_sigma_0.6_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_4_sigma_0.8_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_4_sigma_0.8_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_4_sigma_1.0_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_4_sigma_1.0_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_4_sigma_1.2_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_4_sigma_1.2_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_4_sigma_1.4_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_4_sigma_1.4_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_4_sigma_1.6_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_4_sigma_1.6_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_6_sigma_0.4_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_6_sigma_0.4_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_6_sigma_0.6_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_6_sigma_0.6_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_6_sigma_0.8_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_6_sigma_0.8_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_6_sigma_1.0_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_6_sigma_1.0_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_6_sigma_1.2_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_6_sigma_1.2_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_6_sigma_1.4_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_6_sigma_1.4_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_6_sigma_1.6_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_6_sigma_1.6_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_8_sigma_0.4_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_8_sigma_0.4_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_8_sigma_0.6_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_8_sigma_0.6_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_8_sigma_0.8_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_8_sigma_0.8_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_8_sigma_1.0_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_8_sigma_1.0_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_8_sigma_1.2_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_8_sigma_1.2_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_8_sigma_1.4_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_8_sigma_1.4_zmax_2.0_FXmin_-15.0
ls $UNIT_fA1i_DIR/WTH_fsat_8_sigma_1.6_zmax_2.0_FXmin_-15.0/*.fit   > lists/list_WTH_fsat_8_sigma_1.6_zmax_2.0_FXmin_-15.0

wc -l lists/list_WTH_fsat_*_sigma_*_zmax_2.0_FXmin_-15.0

stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.4_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_0.4_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.6_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_0.6_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.8_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_0.8_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.0_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_1.0_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.2_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_1.2_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.4_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_1.4_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.6_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_1.6_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_0.4_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_12_sigma_0.4_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_0.6_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_12_sigma_0.6_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_0.8_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_12_sigma_0.8_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_1.0_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_12_sigma_1.0_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_1.2_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_12_sigma_1.2_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_1.4_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_12_sigma_1.4_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_1.6_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_12_sigma_1.6_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_0.4_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_14_sigma_0.4_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_0.6_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_14_sigma_0.6_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_0.8_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_14_sigma_0.8_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_1.0_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_14_sigma_1.0_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_1.2_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_14_sigma_1.2_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_1.4_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_14_sigma_1.4_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_1.6_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_14_sigma_1.6_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_16_sigma_0.4_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_16_sigma_0.4_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_16_sigma_0.6_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_16_sigma_0.6_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_16_sigma_0.8_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_16_sigma_0.8_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_16_sigma_1.0_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_16_sigma_1.0_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_16_sigma_1.2_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_16_sigma_1.2_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_16_sigma_1.4_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_16_sigma_1.4_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_16_sigma_1.6_zmax_2.0_FXmin_-15.0 ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_16_sigma_1.6_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_4_sigma_0.4_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_4_sigma_0.4_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_4_sigma_0.6_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_4_sigma_0.6_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_4_sigma_0.8_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_4_sigma_0.8_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_4_sigma_1.0_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_4_sigma_1.0_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_4_sigma_1.2_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_4_sigma_1.2_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_4_sigma_1.4_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_4_sigma_1.4_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_4_sigma_1.6_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_4_sigma_1.6_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_0.4_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_6_sigma_0.4_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_0.6_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_6_sigma_0.6_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_0.8_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_6_sigma_0.8_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_1.0_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_6_sigma_1.0_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_1.2_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_6_sigma_1.2_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_1.4_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_6_sigma_1.4_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_1.6_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_6_sigma_1.6_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_0.4_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_8_sigma_0.4_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_0.6_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_8_sigma_0.6_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_0.8_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_8_sigma_0.8_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_1.0_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_8_sigma_1.0_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_1.2_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_8_sigma_1.2_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_1.4_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_8_sigma_1.4_zmax_2.0_FXmin_-15.0.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_1.6_zmax_2.0_FXmin_-15.0  ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_8_sigma_1.6_zmax_2.0_FXmin_-15.0.fits



cd $GIT_AGN_MOCK/python

python 001_11_plot_hod.py 10 0.4
python 001_11_plot_hod.py 10 0.6
python 001_11_plot_hod.py 10 0.8
python 001_11_plot_hod.py 10 1.0
python 001_11_plot_hod.py 10 1.2
python 001_11_plot_hod.py 10 1.4
python 001_11_plot_hod.py 10 1.6
python 001_11_plot_hod.py 12 0.4
python 001_11_plot_hod.py 12 0.6
python 001_11_plot_hod.py 12 0.8
python 001_11_plot_hod.py 12 1.0
python 001_11_plot_hod.py 12 1.2
python 001_11_plot_hod.py 12 1.4
python 001_11_plot_hod.py 12 1.6
python 001_11_plot_hod.py 14 0.4
python 001_11_plot_hod.py 14 0.6
python 001_11_plot_hod.py 14 0.8
python 001_11_plot_hod.py 14 1.0
python 001_11_plot_hod.py 14 1.2
python 001_11_plot_hod.py 14 1.4
python 001_11_plot_hod.py 14 1.6
python 001_11_plot_hod.py 16 0.4
python 001_11_plot_hod.py 16 0.6
python 001_11_plot_hod.py 16 0.8
python 001_11_plot_hod.py 16 1.0
python 001_11_plot_hod.py 16 1.2
python 001_11_plot_hod.py 16 1.4
python 001_11_plot_hod.py 16 1.6
python 001_11_plot_hod.py  4 0.4
python 001_11_plot_hod.py  4 0.6
python 001_11_plot_hod.py  4 0.8
python 001_11_plot_hod.py  4 1.0
python 001_11_plot_hod.py  4 1.2
python 001_11_plot_hod.py  4 1.4
python 001_11_plot_hod.py  4 1.6
python 001_11_plot_hod.py  6 0.4
python 001_11_plot_hod.py  6 0.6
python 001_11_plot_hod.py  6 0.8
python 001_11_plot_hod.py  6 1.0
python 001_11_plot_hod.py  6 1.2
python 001_11_plot_hod.py  6 1.4
python 001_11_plot_hod.py  6 1.6
python 001_11_plot_hod.py  8 0.4
python 001_11_plot_hod.py  8 0.6
python 001_11_plot_hod.py  8 0.8
python 001_11_plot_hod.py  8 1.0
python 001_11_plot_hod.py  8 1.2
python 001_11_plot_hod.py  8 1.4
python 001_11_plot_hod.py  8 1.6

ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_0.2/*.fit >     lists/list_WTH_fsat_14_sigma_0.2.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_0.3/*.fit >     lists/list_WTH_fsat_14_sigma_0.3.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_0.4/*.fit >     lists/list_WTH_fsat_14_sigma_0.4.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_0.5/*.fit >     lists/list_WTH_fsat_14_sigma_0.5.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_0.6/*.fit >     lists/list_WTH_fsat_14_sigma_0.6.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_0.7/*.fit >     lists/list_WTH_fsat_14_sigma_0.7.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_0.8/*.fit >     lists/list_WTH_fsat_14_sigma_0.8.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_0.9/*.fit >     lists/list_WTH_fsat_14_sigma_0.9.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_1.0/*.fit >     lists/list_WTH_fsat_14_sigma_1.0.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_1.1/*.fit >     lists/list_WTH_fsat_14_sigma_1.1.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_1.2/*.fit >     lists/list_WTH_fsat_14_sigma_1.2.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_1.3/*.fit >     lists/list_WTH_fsat_14_sigma_1.3.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_1.4/*.fit >     lists/list_WTH_fsat_14_sigma_1.4.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_1.5/*.fit >     lists/list_WTH_fsat_14_sigma_1.5.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_1.6/*.fit >     lists/list_WTH_fsat_14_sigma_1.6.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_14_sigma_1.7/*.fit >     lists/list_WTH_fsat_14_sigma_1.7.list

ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_0.2/*.fit >      lists/list_WTH_fsat_6_sigma_0.2.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_0.3/*.fit >      lists/list_WTH_fsat_6_sigma_0.3.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_0.4/*.fit >      lists/list_WTH_fsat_6_sigma_0.4.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_0.5/*.fit >      lists/list_WTH_fsat_6_sigma_0.5.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_0.6/*.fit >      lists/list_WTH_fsat_6_sigma_0.6.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_0.7/*.fit >      lists/list_WTH_fsat_6_sigma_0.7.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_0.8/*.fit >      lists/list_WTH_fsat_6_sigma_0.8.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_0.9/*.fit >      lists/list_WTH_fsat_6_sigma_0.9.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_1.0/*.fit >      lists/list_WTH_fsat_6_sigma_1.0.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_1.1/*.fit >      lists/list_WTH_fsat_6_sigma_1.1.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_1.2/*.fit >      lists/list_WTH_fsat_6_sigma_1.2.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_1.3/*.fit >      lists/list_WTH_fsat_6_sigma_1.3.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_1.4/*.fit >      lists/list_WTH_fsat_6_sigma_1.4.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_1.5/*.fit >      lists/list_WTH_fsat_6_sigma_1.5.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_1.6/*.fit >      lists/list_WTH_fsat_6_sigma_1.6.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_6_sigma_1.7/*.fit >      lists/list_WTH_fsat_6_sigma_1.7.list

ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_0.2/*.fit >     lists/list_WTH_fsat_12_sigma_0.2.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_0.3/*.fit >     lists/list_WTH_fsat_12_sigma_0.3.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_0.4/*.fit >     lists/list_WTH_fsat_12_sigma_0.4.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_0.5/*.fit >     lists/list_WTH_fsat_12_sigma_0.5.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_0.6/*.fit >     lists/list_WTH_fsat_12_sigma_0.6.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_0.7/*.fit >     lists/list_WTH_fsat_12_sigma_0.7.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_0.8/*.fit >     lists/list_WTH_fsat_12_sigma_0.8.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_0.9/*.fit >     lists/list_WTH_fsat_12_sigma_0.9.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_1.0/*.fit >     lists/list_WTH_fsat_12_sigma_1.0.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_1.1/*.fit >     lists/list_WTH_fsat_12_sigma_1.1.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_1.2/*.fit >     lists/list_WTH_fsat_12_sigma_1.2.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_1.3/*.fit >     lists/list_WTH_fsat_12_sigma_1.3.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_1.4/*.fit >     lists/list_WTH_fsat_12_sigma_1.4.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_1.5/*.fit >     lists/list_WTH_fsat_12_sigma_1.5.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_1.6/*.fit >     lists/list_WTH_fsat_12_sigma_1.6.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_12_sigma_1.7/*.fit >     lists/list_WTH_fsat_12_sigma_1.7.list

ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_0.2/*.fit >      lists/list_WTH_fsat_8_sigma_0.2.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_0.3/*.fit >      lists/list_WTH_fsat_8_sigma_0.3.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_0.4/*.fit >      lists/list_WTH_fsat_8_sigma_0.4.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_0.5/*.fit >      lists/list_WTH_fsat_8_sigma_0.5.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_0.6/*.fit >      lists/list_WTH_fsat_8_sigma_0.6.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_0.7/*.fit >      lists/list_WTH_fsat_8_sigma_0.7.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_0.8/*.fit >      lists/list_WTH_fsat_8_sigma_0.8.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_0.9/*.fit >      lists/list_WTH_fsat_8_sigma_0.9.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_1.0/*.fit >      lists/list_WTH_fsat_8_sigma_1.0.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_1.1/*.fit >      lists/list_WTH_fsat_8_sigma_1.1.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_1.2/*.fit >      lists/list_WTH_fsat_8_sigma_1.2.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_1.3/*.fit >      lists/list_WTH_fsat_8_sigma_1.3.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_1.4/*.fit >      lists/list_WTH_fsat_8_sigma_1.4.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_1.5/*.fit >      lists/list_WTH_fsat_8_sigma_1.5.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_1.6/*.fit >      lists/list_WTH_fsat_8_sigma_1.6.list
ls  /data37s/simulation_1/MD/MD_1.0Gpc/WTH_fsat_8_sigma_1.7/*.fit >      lists/list_WTH_fsat_8_sigma_1.7.list

stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.2.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_0.2.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.3.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_0.3.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.4.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_0.4.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.5.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_0.5.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.6.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_0.6.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.7.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_0.7.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.8.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_0.8.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.9.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_0.9.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.0.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_1.0.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.1.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_1.1.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.2.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_1.2.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.3.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_1.3.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.4.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_1.4.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.5.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_1.5.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.6.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_1.6.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.7.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_10_sigma_1.7.fits

stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.2.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_0.2.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.3.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_0.3.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.4.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_0.4.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.5.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_0.5.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.6.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_0.6.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.7.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_0.7.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.8.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_0.8.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_0.9.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_0.9.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.0.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_1.0.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.1.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_1.1.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.2.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_1.2.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.3.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_1.3.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.4.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_1.4.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.5.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_1.5.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.6.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_1.6.fits
stilts tcat in=@lists/list_WTH_fsat_10_sigma_1.7.list ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/CAT_WTH_fsat_10_sigma_1.7.fits

rm WTH_fsat_10_sigma_?.?/*.fit

stilts tcat in=@lists/list_WTH_fsat_14_sigma_0.2.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_0.2.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_0.3.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_0.3.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_0.4.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_0.4.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_0.5.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_0.5.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_0.6.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_0.6.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_0.7.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_0.7.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_0.8.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_0.8.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_0.9.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_0.9.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_1.0.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_1.0.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_1.1.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_1.1.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_1.2.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_1.2.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_1.3.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_1.3.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_1.4.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_1.4.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_1.5.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_1.5.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_1.6.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_1.6.fits
stilts tcat in=@lists/list_WTH_fsat_14_sigma_1.7.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_14_sigma_1.7.fits

rm WTH_fsat_14_sigma_?.?/*.fit

stilts tcat in=@lists/list_WTH_fsat_6_sigma_0.2.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_0.2.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_0.3.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_0.3.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_0.4.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_0.4.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_0.5.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_0.5.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_0.6.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_0.6.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_0.7.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_0.7.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_0.8.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_0.8.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_0.9.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_0.9.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_1.0.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_1.0.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_1.1.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_1.1.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_1.2.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_1.2.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_1.3.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_1.3.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_1.4.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_1.4.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_1.5.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_1.5.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_1.6.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_1.6.fits
stilts tcat in=@lists/list_WTH_fsat_6_sigma_1.7.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_6_sigma_1.7.fits

rm WTH_fsat_6_sigma_?.?/*.fit

stilts tcat in=@lists/list_WTH_fsat_12_sigma_0.2.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_0.2.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_0.3.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_0.3.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_0.4.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_0.4.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_0.5.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_0.5.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_0.6.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_0.6.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_0.7.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_0.7.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_0.8.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_0.8.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_0.9.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_0.9.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_1.0.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_1.0.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_1.1.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_1.1.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_1.2.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_1.2.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_1.3.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_1.3.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_1.4.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_1.4.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_1.5.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_1.5.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_1.6.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_1.6.fits
stilts tcat in=@lists/list_WTH_fsat_12_sigma_1.7.list ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_12_sigma_1.7.fits

rm WTH_fsat_12_sigma_?.?/*.fit

stilts tcat in=@lists/list_WTH_fsat_8_sigma_0.2.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_0.2.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_0.3.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_0.3.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_0.4.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_0.4.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_0.5.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_0.5.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_0.6.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_0.6.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_0.7.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_0.7.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_0.8.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_0.8.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_0.9.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_0.9.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_1.0.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_1.0.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_1.1.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_1.1.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_1.2.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_1.2.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_1.3.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_1.3.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_1.4.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_1.4.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_1.5.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_1.5.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_1.6.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_1.6.fits
stilts tcat in=@lists/list_WTH_fsat_8_sigma_1.7.list  ifmt=fits omode=out ofmt=fits out=$MD10/CAT_WTH_fsat_8_sigma_1.7.fits

rm WTH_fsat_8_sigma_?.?/*.fit


# then go to $GIT_2PCF to compute the clustering of the models

###########################################
# full sky AGN model. 
# Full eROSITA simulations.
# script: run_md_agn.py
###########################################

# STEP 1:
# Runs the AGN model
# script: 003_0_agn.py
# inputs: ${env}/fits/all_?.?????.fits, ${env}/fits/all_?.?????_coordinates.fits, ${env}/fits/all_?.?????_galaxy.fits
# outputs: ${env}/fits/all_?.?????_agn.fits

# STEP 2:
# script: 003_1_agn_catalogs.py
# inputs: ${env}/fits/all_?.?????_agn.fits
# outputs:
# 768 files in : ${env}/fits/cat_AGN_?.?????/000???.fit 

# STEP 3: takes about 6 hours
# concatenate all pixels from shells into pixels with all the redshifts
# script: TOPCAT commands in run_md_agn.py
# inputs: 
# N_shells x 768 files in : ${env}/fits/cat_AGN_?.?????/000???.fit 
# outputs: 
# 768 files in : ${env}/cat_AGN_all/000???.fit 

# STEP 4:
# compute XLF, logNlogS, logNlogR
# script: 003_2_agn_compute_XLF_logNlogS_R.py
# inputs: 
# 768 files in : ${env}/cat_AGN_all/000???.fit 
# outputs: 
# three folders
# ${env}/cat_AGN_all/
#   logNlogR
#   logNlogS
#   XLF

# STEP 5:
# plots XLF, logNlogS, logNlogR
# script: 003_3_agn_plot_logNlogS.py
# inputs: 
# three folders
# ${env}/cat_AGN_all/
#   logNlogR
#   logNlogS
#   XLF
# outputs: 
# ${env}/cat_AGN_all/figures

# public link (300G !)
#  cp -r $MD10/cat_AGN_all /data42s/comparat/firefly/mocks/2020-03/MDPL2/
# prints out commands to be executed 
# 003_0_agn.py
# 003_1_agn_catalogs.py
# concatenation of pixels into pixels with full redshift reach.
# nohup nice -n 19 python run_md_agn.py MD04           > logs/run_agn_MD04_all.log & # DONE
# nohup nice -n 19 python run_md_agn.py MD40           > logs/run_agn_MD40_all.log & # DONE
# nohup nice -n 19 python run_md_agn.py MD10           > logs/run_agn_MD10_all.log & #              running he1srv
# nohup nice -n 19 python run_md_agn.py UNIT_fA1_DIR   > logs/run_agn_UNIT_fA1_DIR_all.log & #      running he1srv
# nohup nice -n 19 python run_md_agn.py UNIT_fA2_DIR   > logs/run_agn_UNIT_fA2_DIR_all.log & #      running he1srv
# nohup nice -n 19 python run_md_agn.py UNIT_fA1i_DIR  > logs/run_agn_UNIT_fA1i_DIR_all.log & #     

# nohup sh run_agn_unit_fa1.sh  > logs/run_agn_UNIT_fA1_DIR_all.log & # 
# nohup sh run_agn_unit_fa1i.sh > logs/run_agn_UNIT_fA1i_DIR_all.log & # 
# DONE, verify outputs

# nohup sh run_md_agn_commands.sh > logs/agn_MD10_all_concatenate.log  &

# tabulates logNlogS, logNlogR, XLF, ... and makes figures for fields 0, 100, 200, 300, 400, 500, 600, 700
#nohup nice -n 19 python3 003_2_agn_compute_XLF_logNlogS_R.py MD10 > logs/003_2_agn_MD10_all.log  &
#nohup nice -n 19 python3 003_2_agn_compute_XLF_logNlogS_R.py MD40 > logs/003_2_agn_MD40_all.log  &
#nohup nice -n 19 python3 003_2_agn_compute_XLF_logNlogS_R.py MD04 > logs/003_2_agn_MD04_all.log  &
#
#nohup nice -n 19 python3 003_2_agn_compute_XLF_logNlogS_R.py UNIT_fA1_DIR > logs/003_2_agn_UNIT_fA1_DIR_all.log  &
cd $GIT_AGN_MOCK/python
nohup nice -n 19 python3 003_2_agn_compute_XLF_logNlogS_R.py UNIT_fA1i_DIR WTH_fsat_10_sigma_1.0 > logs/003_2_agn_UNIT_fA1i_DIR_all.log  &

#nohup nice -n 19 python3 003_3_agn_plot_logNlogS.py UNIT_fA1_DIR > logs/003_3_agn_logNlogS_UNIT_fA1_DIR_all.log  &
#nohup nice -n 19 python3 003_3_agn_plot_logNlogS.py UNIT_fA1i_DIR > logs/003_3_agn_logNlogS_UNIT_fA1i_DIR_all.log  &

# routine to tabulate and plot all logNlogS together :
#nohup nice -n 19 python3 003_3_agn_plot_logNlogS.py MD10 > logs/003_3_agn_logNlogS_MD10_all.log  &
#nohup nice -n 19 python3 003_3_agn_plot_logNlogS.py UNIT_fA1_DIR > logs/003_3_agn_logNlogS_MUNIT_fA1_DIR_all.log  &
#nohup nice -n 19 python3 003_3_agn_plot_logNlogS.py UNIT_fA1i_DIR > logs/003_3_agn_logNlogS_MUNIT_fA1i_DIR_all.log  &

# VERIFY FILES                                                                             
# check the total number of files is the same (no missing files), first 4 lines
# check the date of files are in the right sequence (no outdated files), last 2 lines
# AGN files
#ls $MD04/fits/all_?.?????.fits          | wc -l
#ls $MD04/fits/all_?.?????_agn.fits   | wc -l
#ls $MD04/fits/cat_AGN_all_?.?????/000767.fit | wc -l
#ls $MD04/cat_AGN_all/000???.fit | wc -l
#ll $MD04/fits/cat_AGN_all_?.?????/000767.fit
#ll $MD04/cat_AGN_all/000767.fit
#
#ls $MD40/fits/all_?.?????.fits          | wc -l
#ls $MD40/fits/all_?.?????_agn.fits   | wc -l
#ls $MD40/fits/cat_AGN_all_?.?????/000767.fit | wc -l
#ls $MD40/cat_AGN_all/000???.fit | wc -l
#ll $MD40/fits/cat_AGN_all_?.?????/000767.fit
#ll $MD40/cat_AGN_all/000767.fit
#
#ls $MD10/fits/all_?.?????.fits          | wc -l
#ls $MD10/fits/all_?.?????_agn.fits   | wc -l
#ls $MD10/fits/cat_AGN_all_?.?????/000767.fit | wc -l
#ls $MD10/cat_AGN_all/000???.fit | wc -l
#ll $MD10/fits/cat_AGN_all_?.?????/000767.fit
#ll $MD10/cat_AGN_all/000767.fit
#
#ll $MD10/fits/all_?.?????.fits               > lists/MD10_fits_all_fits.list
#ll $MD10/fits/all_?.?????_coordinates.fits   > lists/MD10_fits_all_fits_coordinates.list
#ll $MD10/fits/all_?.?????_galaxy.fits        > lists/MD10_fits_all_fits_galaxy.list
#ll $MD10/fits/all_?.?????_agn.fits           > lists/MD10_fits_all_fits_agn.list
#
#wc -l lists/MD10_fits_all_fits.list
#wc -l lists/MD10_fits_all_fits_coordinates.list
#wc -l lists/MD10_fits_all_fits_galaxy.list
#wc -l lists/MD10_fits_all_fits_agn.list
#
#ls $UNIT_fA1_DIR/fits/all_?.?????.fits          | wc -l
#ls $UNIT_fA1_DIR/fits/all_?.?????_agn.fits   | wc -l
#ls $UNIT_fA1_DIR/fits/cat_AGN_all_?.?????/000767.fit | wc -l
#ls $UNIT_fA1_DIR/cat_AGN_all/000???.fit | wc -l
#ll $UNIT_fA1_DIR/fits/cat_AGN_all_?.?????/000767.fit
#ll $UNIT_fA1_DIR/cat_AGN_all/000767.fit
#
#ls $UNIT_fA1i_DIR/fits/all_?.?????.fits          | wc -l
#ls $UNIT_fA1i_DIR/fits/all_?.?????_agn.fits   | wc -l
#ls $UNIT_fA1i_DIR/fits/cat_AGN_all_?.?????/000767.fit | wc -l
#ls $UNIT_fA1i_DIR/cat_AGN_all/000???.fit | wc -l
#ll $UNIT_fA1i_DIR/fits/cat_AGN_all_?.?????/000767.fit
#ll $UNIT_fA1i_DIR/cat_AGN_all/000767.fit


# MD04, MD40 : DONE
# MD10, UNIT_fA1_DIR, UNIT_fA1i_DIR, UNIT_fA2_DIR : rerun periodically, when more shells are available

# still running on he1srv :

#ls $UNIT_fA1_DIR/fits/all_?.?????.fits          | wc -l
#ls $UNIT_fA1_DIR/fits/all_?.?????_agn.fits   | wc -l
#ls $UNIT_fA1_DIR/fits/cat_AGN_all_?.?????/000767.fit | wc -l
#ls $UNIT_fA1_DIR/cat_AGN_all/000???.fit | wc -l
#ll $UNIT_fA1_DIR/fits/cat_AGN_all_?.?????/000767.fit
#ll $UNIT_fA1_DIR/cat_AGN_all/000767.fit
#
#ls $UNIT_fA2_DIR/fits/all_?.?????.fits          | wc -l
#ls $UNIT_fA2_DIR/fits/all_?.?????_agn.fits   | wc -l
#ls $UNIT_fA2_DIR/fits/cat_AGN_all_?.?????/000767.fit | wc -l
#ls $UNIT_fA2_DIR/cat_AGN_all/000???.fit | wc -l
#ll $UNIT_fA1_DIR/fits/cat_AGN_all_?.?????/000767.fit
#ll $UNIT_fA1_DIR/cat_AGN_all/000767.fit
#
#ls $UNIT_fA1i_DIR/fits/all_?.?????.fits          | wc -l
#ls $UNIT_fA1i_DIR/fits/all_?.?????_agn.fits   | wc -l
#ls $UNIT_fA1i_DIR/fits/cat_AGN_all_?.?????/000767.fit | wc -l
#ls $UNIT_fA1i_DIR/cat_AGN_all/000???.fit | wc -l
#ll $UNIT_fA1_DIR/fits/cat_AGN_all_?.?????/000767.fit
#ll $UNIT_fA1_DIR/cat_AGN_all/000767.fit



#############################################
# Optical/IR properties of AGN (for a subset)
# on he1srv
#############################################

# writes slurm scripts to be executed on he1srv
# script 003_6_agn_magnitudes_WriteScripts.py
# output: cd $GIT_AGN_MOCK/python/003_6_log/*.sh
python 003_6_agn_magnitudes_WriteScripts.py

# compute magnitudes for all AGN simulated with sixte (FX_soft>2e-17)
# Execute the scripts in the directory 003_6_log
# script: 003_6_agn_magnitudes.py
# inputs: 2 x 768 files in : ${env}/cat_AGN_all/000???.fit, ${env}/cat_AGN_SIMPUT/000???.fit 
# outputs: 768 files in : ${env}/cat_AGN-MAG_all/000???.fit 
# public link (12G)
#  cp -r $MD10/cat_AGN-MAG_all /data42s/comparat/firefly/mocks/2020-03/MDPL2/

ssh he1srv
cd $GIT_AGN_MOCK/python/003_6_log/
pyCONDA
ipython

nohup python3 003_6_agn_magnitudes.py MD10 all 460 > logs/003_6_mag_460.log &  
nohup python3 003_6_agn_magnitudes.py MD10 all 236 > logs/003_6_mag_236.log & 
nohup python3 003_6_agn_magnitudes.py MD10 all 194 > logs/003_6_mag_194.log & 
nohup python3 003_6_agn_magnitudes.py MD10 all 361 > logs/003_6_mag_361.log & 
nohup python3 003_6_agn_magnitudes.py MD10 all 587 > logs/003_6_mag_587.log & 
nohup python3 003_6_agn_magnitudes.py MD10 all 124 > logs/003_6_mag_124.log & 


import os
import numpy as n
for pix in n.arange(1,768,1):
print("==================================")
cmd = "python3 003_6_agn_magnitudes.py UNIT_fA1i_DIR "+str(pix)
print(cmd)
os.system(cmd)

# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("MD04_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)
# 
# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("MD10_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)
# 
# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("MD40_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)
# 
# 
# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("UNIT_fA1i_DIR_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)
# 
# 
# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("UNIT_fA2_DIR_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)
# 
# 
# import os
# import glob
# import numpy as n
# scripts = sorted(n.array(glob.glob("UNIT_fA1_DIR_*.sh")))
# for script in scripts:
#     os.system('sbatch ' + script)

    
# cp -r $MD10/cat_AGN-MAG_all /data42s/comparat/firefly/mocks/2020-03/MDPL2/ 
# cp -r $MD40/cat_AGN-MAG_all /data42s/comparat/firefly/mocks/2020-03/HMD/ 
# cp -r $MD04/cat_AGN-MAG_all /data42s/comparat/firefly/mocks/2020-03/SMDPL/ 


# create AGN SIMPUT files for eROSITA. Takes 2.5 hours
# Considers a subset of the AGNs in the simulation: FX_soft>2e-17
# script: 003_4_agn_simput.py
# inputs: 768 files in : ${env}/cat_AGN_all/000???.fit 
# outputs: 768 files in : ${env}/cat_AGN_SIMPUT/000???.fit 
# public link (35G)
#  cp -r $MD10/cat_AGN_SIMPUT /data42s/comparat/firefly/mocks/2020-03/MDPL2/
# 
# alternative script, per pixel, more parallel: python 003_4_agn_simput_HPX.py MD10 767
# Running ds43 :
# nohup nice -n 19 python 003_4_agn_simput.py MD10  1e-17  > logs/run_simput_MD10_1e17.log & # 
# nohup nice -n 19 python 003_4_agn_simput.py MD10  1e-18  > logs/run_simput_MD10_1e18.log & # 
# # quite incomplete below flux 1e-17 :
# nohup nice -n 19 python 003_4_agn_simput.py MD10  2e-17  > logs/run_simput_MD10_2e17.log & # 
# nohup nice -n 19 python 003_4_agn_simput.py MD10  2e-20  > logs/run_simput_MD10_2e20.log & # 
# nohup nice -n 19 python 003_4_agn_simput.py MD04  2e-20   > logs/run_simput_MD04.log & # 
# 
# nohup nice -n 19 python 003_4_agn_simput.py MD40          > logs/run_simput_MD40.log & # 
# nohup nice -n 19 python 003_4_agn_simput.py UNIT_fA1_DIR  1e-17 > logs/run_simput_UNIT_fA1_DIR.log &
# # nohup nice -n 19 python 003_4_agn_simput.py UNIT_fA2_DIR  > logs/run_simput_UNIT_fA2_DIR.log &
# nohup nice -n 19 python 003_4_agn_simput.py	 UNIT_fA1i_DIR 1e-17 > logs/run_simput_UNIT_fA1i_DIR.log &
# 
# nohup nice -n 19 python 003_4_agn_simput.py UNIT_fA1i_DIR 1e-19 > logs/run_simput_UNIT_fA1i_DIR_deep.log &

nohup nice -n 19 python 003_4_agn_simput.py UNIT_fA1i_DIR 1e-17 WTH_fsat_10_sigma_1.0 SIMPUT_WTH_fsat_10_sigma_1.0 > logs/run_simput_UNIT_fA1i_DIR_erowide.log &

cd $UNIT_fA1i_DIR/SIMPUT_WTH_fsat_10_sigma_1.0
ln -s /data39s/simulation_2/MD/MD_4.0Gpc/cat_AGN_SIMPUT/agn_Xspectra .

#python get_athena_pixels_1000deg2.py
#
#ll $MD10/cat_AGN_SIMPUT/SIMPUT_000767_1024.fit
#ll $MD40/cat_AGN_SIMPUT/SIMPUT_000767_1024.fit
#ll $MD04/cat_AGN_SIMPUT/SIMPUT_000767_1024.fit
#
#cp -r $MD10/cat_AGN_SIMPUT /data42s/comparat/firefly/mocks/2020-03/MDPL2/
#cp -r $MD40/cat_AGN_SIMPUT /data42s/comparat/firefly/mocks/2020-03/HMD/
#cp -r $MD04/cat_AGN_SIMPUT /data42s/comparat/firefly/mocks/2020-03/SMDPL/
#
#cd $UNIT_fA1i_DIR/cat_AGN_SIMPUT_galNH
#ln -s /data39s/simulation_2/MD/MD_4.0Gpc/cat_AGN_SIMPUT/agn_Xspectra .
#cd $UNIT_fA1_DIR/cat_AGN_SIMPUT_galNH
#ln -s /data39s/simulation_2/MD/MD_4.0Gpc/cat_AGN_SIMPUT/agn_Xspectra .
#
#cd /data39s/simulation_2/MD/MD_4.0Gpc/cat_AGN_SIMPUT/agn_Xspectra
#
#chgrp erosim galNH19*_N1024.fits
#chgrp erosim galNH19*_N256.fits
#chgrp erosim galNH19*_N512.fits
#chgrp erosim galNH20*_N1024.fits
#chgrp erosim galNH20*_N256.fits
#chgrp erosim galNH20*_N512.fits
#chgrp erosim galNH21*_N1024.fits
#chgrp erosim galNH21*_N256.fits
#chgrp erosim galNH21*_N512.fits
#chgrp erosim galNH22*_N1024.fits
#chgrp erosim galNH22*_N256.fits
#chgrp erosim galNH22*_N512.fits
#chgrp erosim galNH23*_N1024.fits
#chgrp erosim galNH23*_N256.fits
#chgrp erosim galNH23*_N512.fits
#chgrp erosim galNH24*_N1024.fits
#chgrp erosim galNH24*_N256.fits
#chgrp erosim galNH24*_N512.fits

#/data42s/comparat/firefly/mocks/2020-06/MDPL2

# simulate counts with sixte 
# script: $GIT_AGN_MOCK/python/sixte/simulate_agn_only.py
# inputs: 
#    768 catalogues: ${env}/cat_AGN_SIMPUT/000???.fit
#    spectra: ${env}/cat_AGN_SIMPUT/agn_Xspectra/NH??.?_Z?.?_N*.fits
# outputs:
#    /data40s/erosim/eRASS/eRASS8_agn_MD10/???/erass_ccd?_evt.fits
#    /data40s/erosim/eRASS/eRASS8_agn_MD10/???/erass.gti
# concatenated outputs :
#    /data40s/erosim/eRASS/eRASS8_agn_MD10/simulated_photons_ccd?.fits
#    /data40s/erosim/eRASS/eRASS8_agn_MD10/
# public version
#    cp /data40s/erosim/eRASS/eRASS8_agn_MD10/simulated_photons_ccd?.fits /data42s/comparat/firefly/mocks/2020-03/MDPL2/AGN_counts/
#    https://firefly.mpe.mpg.de/mocks/2020-03/MDPL2/AGN_counts/

cd $GIT_ERASS_SIM/sixte
# script for the other simulations are to be created
# execute all commands in this file by copy pasting them into a screen on ds43 of ds54

#nohup sh all_sky_simulate_agn_UNITfA1.sh  > logs/all_sky_simulate_agn_UNITfA1.log  &

#nohup sh all_sky_simulate_agn_UNITfA1i.sh > logs/all_sky_simulate_agn_UNITfA1i.log &
nohup sh all_sky_simulate_agn_UNITfA1i_100.sh > logs/agn_unitfa1i_sixte.log &
nohup sh all_sky_simulate_agn_UNITfA1i_200.sh > logs/agn_unitfa1i_sixte.log &
nohup sh all_sky_simulate_agn_UNITfA1i_300.sh > logs/agn_unitfa1i_sixte.log &
nohup sh all_sky_simulate_agn_UNITfA1i_400.sh > logs/agn_unitfa1i_sixte.log &
nohup sh all_sky_simulate_agn_UNITfA1i_500.sh > logs/agn_unitfa1i_sixte.log &
nohup sh all_sky_simulate_agn_UNITfA1i_600.sh > logs/agn_unitfa1i_sixte.log &
nohup sh all_sky_simulate_agn_UNITfA1i_700.sh > logs/agn_unitfa1i_sixte.log &

# Then the figures and concatenate photons and copy the files 
$GIT_AGN_MOCK/python/sixte/simulation_figures.sh

# ALL COPIED !

# compute SDSS-5 / 4MOST AGN / QSO catalogues for BHM and 4MOST consortium surveys S6 and S8
# script: 003_7_4most_catalog.py
# inputs: 768 files in : ${env}/cat_AGN-MAG_all/000???.fit,
# outputs: 768 files in : ${env}/cat_AGN_4MOST/*_000???.fit 
#   global selection :    |g_lat|>10
#      AGN_IR_000???.fit     ( AGN_type=22 | AGN_type=21 ) < 19
#      AGN_DEEP_000???.fit   g_lon>180 & |ecl_lat|>80 & FX > flux_limit_SNR3 - 0.1
#      AGN_WIDE_000???.fit   g_lon>180 & |ecl_lat|<80 & FX > flux_limit_SNR3 - 0.1
#      LyA_000???.fit        AGN_type==11 & AGN_SDSS_r_magnitude < 22.6 & z > 2.2
#      QSO_000???.fit        AGN_type==11 & AGN_SDSS_r_magnitude < 23.2 & 0.9 < z < 2.2
# 
# public link (12G)
#  cp -r $MD10/cat_AGN-MAG_all /data42s/comparat/firefly/mocks/2020-03/MDPL2/
cd $GIT_AGN_MOCK/python/
nohup nice -n 19 python 003_7_4most_catalog.py MD04          > logs/agn_4most_MD04.log &  
nohup nice -n 19 python 003_7_4most_catalog.py MD10          > logs/agn_4most_MD10.log &  
nohup nice -n 19 python 003_7_4most_catalog.py MD40          > logs/agn_4most_MD40.log &  
nohup nice -n 19 python 003_7_4most_catalog.py UNIT_fA1_DIR  > logs/agn_4most_UNIT_fA1_DIR.log &  
nohup nice -n 19 python 003_7_4most_catalog.py UNIT_fA2_DIR  > logs/agn_4most_UNIT_fA2_DIR.log &  
nohup nice -n 19 python 003_7_4most_catalog.py UNIT_fA1i_DIR > logs/agn_4most_UNIT_fA1i_DIR.log &  

# Concatenate catalogs 
# inputs: 768 files in : ${env}/cat_AGN_4MOST/*_000???.fit 
# outputs: 
#     cat_AGN-MAG_all ==>> ${env}/AGN_all.fits
#     cat_AGN_4MOST/AGN_DEEP* ==>> ${env}/AGN_DEEP_4MOST.fits
#     cat_AGN_4MOST/AGN_WIDE* ==>> ${env}/AGN_WIDE_4MOST.fits
#     cat_AGN_4MOST/AGN_IR* ==>> ${env}/AGN_IR_4MOST.fits
#     cat_AGN_4MOST/QSO* ==>> ${env}/QSO_4MOST.fits
#     cat_AGN_4MOST/LyA* ==>> ${env}/LyA_4MOST.fits
#   public links
#     /data42s/comparat/firefly/mocks/2020-03/QMOST/S6_4MOST_R-228-234.fit
#     /data42s/comparat/firefly/mocks/2020-03/QMOST/4MOST_S8_AGN.fits
#     /data42s/comparat/firefly/mocks/2020-03/MDPL2/MD10_AGN_all.fits
#     /data42s/comparat/firefly/mocks/2020-03/SMDPL/MD04_AGN_all.fits

cd $GIT_AGN_MOCK/python/
# # MD04  
# ls $MD04/cat_AGN-MAG_all/*.fit > lists/md04_4most_agn_all.lis
# nohup stilts tcat in=@lists/md04_4most_agn_all.lis ifmt=fits omode=out ofmt=fits out=$MD04/AGN_all.fits         > logs/md04_4most_agn_all_CONCAT.log &
# 
# ls $MD04/cat_AGN_4MOST/AGN_DEEP_*.fit > lists/md04_4most_agn_deep.lis
# nohup stilts tcat in=@lists/md04_4most_agn_deep.lis ifmt=fits omode=out ofmt=fits out=$MD04/AGN_DEEP_4MOST.fits > logs/md04_4most_agn_deep_CONCAT.log &
# 
# ls $MD04/cat_AGN_4MOST/AGN_WIDE_*.fit > lists/md04_4most_agn_wide.lis
# nohup stilts tcat in=@lists/md04_4most_agn_wide.lis ifmt=fits omode=out ofmt=fits out=$MD04/AGN_WIDE_4MOST.fits > logs/md04_4most_agn_wide_CONCAT.log &
# 
# ls $MD04/cat_AGN_4MOST/AGN_IR_*.fit > lists/md04_4most_agn_ir.lis
# nohup stilts tcat in=@lists/md04_4most_agn_ir.lis ifmt=fits omode=out ofmt=fits out=$MD04/AGN_IR_4MOST.fits     > logs/md04_4most_agn_ir_CONCAT.log &
# 
# ls $MD04/cat_AGN_4MOST/QSO_*.fit > lists/md04_4most_qso.lis
# nohup stilts tcat in=@lists/md04_4most_qso.lis ifmt=fits omode=out ofmt=fits out=$MD04/QSO_4MOST.fits           > logs/md04_4most_qso_CONCAT.log &
# 
# ls $MD04/cat_AGN_4MOST/LyA_*.fit > lists/md04_4most_lya.lis
# nohup stilts tcat in=@lists/md04_4most_lya.lis ifmt=fits omode=out ofmt=fits out=$MD04/LyA_4MOST.fits               > logs/md04_4most_lya_CONCAT.log &
# 
# # MD40  
# ls $MD40/cat_AGN-MAG_all/*.fit > lists/md40_4most_agn_all.lis
# nohup stilts tcat in=@lists/md40_4most_agn_all.lis ifmt=fits omode=out ofmt=fits out=$MD40/AGN_all.fits          > logs/md40_4most_agn_all_CONCAT.log &
# 
# ls $MD40/cat_AGN_4MOST/AGN_DEEP_*.fit > lists/md40_4most_agn_deep.lis
# nohup stilts tcat in=@lists/md40_4most_agn_deep.lis ifmt=fits omode=out ofmt=fits out=$MD40/AGN_DEEP_4MOST.fits  > logs/md40_4most_agn_deep_CONCAT.log &
# 
# ls $MD40/cat_AGN_4MOST/AGN_WIDE_*.fit > lists/md40_4most_agn_wide.lis                                                     
# nohup stilts tcat in=@lists/md40_4most_agn_wide.lis ifmt=fits omode=out ofmt=fits out=$MD40/AGN_WIDE_4MOST.fits  > logs/md40_4most_agn_wide_CONCAT.log &
# 
# ls $MD40/cat_AGN_4MOST/AGN_IR_*.fit > lists/md40_4most_agn_ir.lis                                                         
# nohup stilts tcat in=@lists/md40_4most_agn_ir.lis ifmt=fits omode=out ofmt=fits out=$MD40/AGN_IR_4MOST.fits      > logs/md40_4most_agn_ir_CONCAT.log &
# 
# ls $MD40/cat_AGN_4MOST/QSO_*.fit > lists/md40_4most_qso.lis                                                               
# nohup stilts tcat in=@lists/md40_4most_qso.lis ifmt=fits omode=out ofmt=fits out=$MD40/QSO_4MOST.fits            > logs/md40_4most_qso_CONCAT.log &
# 
# ls $MD40/cat_AGN_4MOST/LyA_*.fit > lists/md40_4most_lya.lis                                                               
# nohup stilts tcat in=@lists/md40_4most_lya.lis ifmt=fits omode=out ofmt=fits out=$MD40/LyA_4MOST.fits            > logs/md40_4most_lya_CONCAT.log &
# 
# #MD10
# ls $MD10/cat_AGN-MAG_all/*.fit > lists/md10_4most_agn_all.lis
# nohup stilts tcat in=@lists/md10_4most_agn_all.lis ifmt=fits omode=out ofmt=fits out=$MD10/AGN_all.fits          > logs/md10_4most_agn_all_CONCAT.log &
# 
# python 003_7_4most_catalog.py UNIT_fA1_DIR
# 
# ls $UNIT_fA1_DIR/cat_AGN_4MOST/AGN_DEEP_*.fit > lists/md10_4most_agn_deep.lis
# stilts tcat in=@lists/md10_4most_agn_deep.lis ifmt=fits omode=out ofmt=fits out=$UNIT_fA1_DIR/AGN_DEEP_4MOST.fits  #> logs/md10_4most_agn_deep_CONCAT.log &
# 
# ls $UNIT_fA1_DIR/cat_AGN_4MOST/AGN_WIDE_*.fit > lists/md10_4most_agn_wide.lis
# stilts tcat in=@lists/md10_4most_agn_wide.lis ifmt=fits omode=out ofmt=fits out=$UNIT_fA1_DIR/AGN_WIDE_4MOST.fits  #> logs/md10_4most_agn_wide_CONCAT.log &
# 
# ls $UNIT_fA1_DIR/cat_AGN_4MOST/AGN_IR_*.fit > lists/md10_4most_agn_ir.lis
# stilts tcat in=@lists/md10_4most_agn_ir.lis ifmt=fits omode=out ofmt=fits out=$UNIT_fA1_DIR/AGN_IR_4MOST.fits    #  > logs/md10_4most_agn_ir_CONCAT.log &
# 
# cd $GIT_AGN_MOCK/python
# python 003_7_merge.py UNIT_fA1_DIR

cd $GIT_AGN_MOCK/python
python 003_7_4most_catalog.py UNIT_fA1i_DIR

cd $GIT_AGN_MOCK/python
ls $UNIT_fA1i_DIR/cat_AGN_4MOST/AGN_DEEP_*.fit > lists/md10_4most_agn_deep.lis
stilts tcat in=@lists/md10_4most_agn_deep.lis ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/AGN_DEEP_4MOST.fits  #> logs/md10_4most_agn_deep_CONCAT.log &

ls $UNIT_fA1i_DIR/cat_AGN_4MOST/AGN_WIDE_*.fit > lists/md10_4most_agn_wide.lis
stilts tcat in=@lists/md10_4most_agn_wide.lis ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/AGN_WIDE_4MOST.fits  #> logs/md10_4most_agn_wide_CONCAT.log &

ls $UNIT_fA1i_DIR/cat_AGN_4MOST/AGN_IR_*.fit > lists/md10_4most_agn_ir.lis
stilts tcat in=@lists/md10_4most_agn_ir.lis ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/AGN_IR_4MOST.fits    #  > logs/md10_4most_agn_ir_CONCAT.log &

ls $UNIT_fA1i_DIR/cat_AGN_4MOST/QSO_*.fit > lists/md40_4most_qso.lis                                                               
nohup stilts tcat in=@lists/md40_4most_qso.lis ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/QSO_4MOST.fits            > logs/md40_4most_qso_CONCAT.log &

ls $UNIT_fA1i_DIR/cat_AGN_4MOST/LyA_*.fit > lists/md40_4most_lya.lis                                                               
nohup stilts tcat in=@lists/md40_4most_lya.lis ifmt=fits omode=out ofmt=fits out=$UNIT_fA1i_DIR/LyA_4MOST.fits            > logs/md40_4most_lya_CONCAT.log &

python 005_4_add_footprint.py /data24s/comparat/simulation/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001/LyA_4MOST.fits
python 005_4_add_footprint.py /data24s/comparat/simulation/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001/QSO_4MOST.fits

python 003_7_merge.py UNIT_fA1i_DIR

cp $UNIT_fA1i_DIR/QSO_4MOST.fits /data42s/comparat/firefly/mocks/2021-04/QMOST/
cp $UNIT_fA1i_DIR/LyA_4MOST.fits /data42s/comparat/firefly/mocks/2021-04/QMOST/

# python 003_7_4most_catalog.py MD10
# 
# ls $MD10/cat_AGN_4MOST/AGN_DEEP_*.fit > lists/md10_4most_agn_deep.lis
# stilts tcat in=@lists/md10_4most_agn_deep.lis ifmt=fits omode=out ofmt=fits out=$MD10/AGN_DEEP_4MOST.fits  #> logs/md10_4most_agn_deep_CONCAT.log &
# 
# ls $MD10/cat_AGN_4MOST/AGN_WIDE_*.fit > lists/md10_4most_agn_wide.lis
# stilts tcat in=@lists/md10_4most_agn_wide.lis ifmt=fits omode=out ofmt=fits out=$MD10/AGN_WIDE_4MOST.fits  #> logs/md10_4most_agn_wide_CONCAT.log &
# 
# ls $MD10/cat_AGN_4MOST/AGN_IR_*.fit > lists/md10_4most_agn_ir.lis
# stilts tcat in=@lists/md10_4most_agn_ir.lis ifmt=fits omode=out ofmt=fits out=$MD10/AGN_IR_4MOST.fits    #  > logs/md10_4most_agn_ir_CONCAT.log &
# 
# python 003_7_merge.py MD10


# ls $MD10/cat_AGN_4MOST/QSO_*.fit > lists/md10_4most_qso.lis
# nohup stilts tcat in=@lists/md10_4most_qso.lis ifmt=fits omode=out ofmt=fits out=$MD10/QSO_4MOST.fits            > logs/md10_4most_qso_CONCAT.log &
# 
# ls $MD10/cat_AGN_4MOST/LyA_*.fit > lists/md10_4most_lya.lis
# nohup stilts tcat in=@lists/md10_4most_lya.lis ifmt=fits omode=out ofmt=fits out=$MD10/LyA_4MOST.fits            > logs/md10_4most_lya_CONCAT.log &

# Verifications on AGN catalogs 
# Computes the number of AGN in each catalogue and present in other catalogues (overlap matrix)
# script: 003_7_shared_agn.py
# inputs: 
#     ${env}/AGN_all.fits
#     ${env}/AGN_DEEP_4MOST.fits
#     ${env}/AGN_WIDE_4MOST.fits
#     ${env}/AGN_IR_4MOST.fits
#     ${env}/QSO_4MOST.fits
#     ${env}/LyA_4MOST.fits
# output: latex tables, https://www.overleaf.com/project/5dd687852c646e0001c1aa58
# nohup nice -n 19 python 003_7_shared_agn.py MD10 > logs/agn_overlap_matrix_MD10.log & 
# nohup nice -n 19 python 003_7_shared_agn.py MD04 > logs/agn_overlap_matrix_MD04.log & 

# Merge the AGN catalogues from MD04 and MD10 into a single 4MOST S6 catalogue
# script: 003_7_merge.py
# inputs: from MD04 and MD10 
#     ${env}/AGN_DEEP_4MOST.fits
#     ${env}/AGN_WIDE_4MOST.fits
#     ${env}/AGN_IR_4MOST.fits
# output: /data42s/comparat/firefly/mocks/2020-03/QMOST/S6_4MOST_R-228-234.fit 
nohup nice -n 19 python 003_7_merge.py > logs/agn_merge_4most_catalogue.log &
python 003_7_merge.py UNIT_fA1i_DIR
python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2021-04/QMOST/S6_4MOST_ALL_SNR3_IR215.fit
# Add footprint bits
# script: 005_4_add_footprint.py
# inputs: catalogue file (any)    
# outputs: updates the files with the footprint bit
# nohup python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2021-04/QMOST/S6_4MOST_ALL_SNR3_IR215.fit > logs/4most_footprint_S6_4MOST_R-228-234.log &
nohup python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2021-04/QMOST/QSO_4MOST.fits > logs/4most_footprint_s8_qso.log &
nohup python 005_4_add_footprint.py /data42s/comparat/firefly/mocks/2021-04/QMOST/LyA_4MOST.fits > logs/4most_footprint_s8_lya.log &

# public links 
cd /data42s/comparat/firefly/mocks/2021-04/QMOST/
gzip -k --rsyncable UNIT_fA1i_DIRS6_4MOST_ALL_SNR3_IR215.fit 
gzip -k --rsyncable LyA_4MOST.fits 
gzip -k --rsyncable QSO_4MOST.fits 


# cd $UNIT_fA1i_DIR/cat_AGN-MAG_all
# tar -czf ~/data2/firefly/mocks/2020-12/UNIT1i/000000.fit.tar.gz  000000.fit
# 
# cd $UNIT_fA1i_DIR/cat_AGN_all
# tar -czf ~/data2/firefly/mocks/2020-12/UNIT1i/agn000000.fit.tar.gz  000000.fit
# 
# cd $UNIT_fA1i_DIR/cat_GALAXY_all
# tar -czf ~/data2/firefly/mocks/2020-12/UNIT1i/gal000000.fit.tar.gz  000000.fit
# 
# 
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_agn_catalogue/000001.fit.tar.gz  000001.fits
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_agn_catalogue/000002.fit.tar.gz  000002.fits
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_agn_catalogue/000003.fit.tar.gz  000003.fits
# tar -czf ~/wwwDir/eROSITA_mock/catalogs/MDPL2/single_small_agn_catalogue/000004.fit.tar.gz  000004.fits

