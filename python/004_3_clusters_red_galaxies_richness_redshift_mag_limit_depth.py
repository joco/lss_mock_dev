"""
What it does
------------

Computes the richness limit / mass limit as a function of redshift and survey depth

"""

from scipy.optimize import newton
import sys, os, time
from scipy.stats import norm
from scipy.interpolate import interp1d 
#import astropy.io.fits as fits
import numpy as n
from scipy.special import erf
from astropy.table import Table, Column
from scipy.integrate import quad  
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p

env = 'MD10'
mag_lim = 21.5

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
cosmoMD = FlatLambdaCDM(H0=67.77 * u.km / u.s / u.Mpc, Om0=0.307115) 
h = 0.6777
L_box = 1000.0 / h
cosmo = cosmoMD

hdu_clu = Table.read(os.path.join(os.environ[env], 'MD10_eRO_CLU_b10_CM_0_pixS_20.0.fits'))


# Possibly implement the double schechter from Popesso 2005 ?
logPhi_z, alpha_z, M_s_z = n.loadtxt(os.path.join(
    os.environ['GIT_AGN_MOCK'], 'data', 'cluster_galaxies', 'ricci_2018_table3.ascii'))


# schechter function, Eq. 8,9.
def Schechter_L(L, phi_s, L_s, alpha):
	"""
	Eq. 8 Ricci 2018
	"""
	return phi_s * (L / L_s)**alpha * n.e**(-L / L_s) / L_s

def Schechter_M(M, phi_s, M_s, alpha): 
	"""
	Eq. 9 Ricci 2018
	"""
	return 0.4 * n.log(10.) * phi_s * 10**(0.4 * (M_s - M) * (alpha + 1)) * n.e**(-10**(0.4 * (M_s - M)))

# redshift evolution function for the parameters o fthe Schecter function, Eq. 10
def function_eq10(redshift, richness, a, b, c): 
	"""
	Eq. 10 for a median redshift and a median richness
	"""
	return a * n.log10(1 + redshift) + b * n.log10(richness) + c

def logPhi_evol( redshift, richness):
	"""
	Redshift evolution of Phi star
	Combination of the values in Table 3 and Eq. 10 of Ricci 2018
	"""
	return function_eq10( redshift, richness, logPhi_z[0], logPhi_z[1], logPhi_z[2] )

def alpha_evol( redshift, richness): 
	"""
	Redshift evolution of alpha
	Combination of the values in Table 3 and Eq. 10 of Ricci 2018
	"""
	return function_eq10( redshift, richness, alpha_z[0], alpha_z[1], alpha_z[2] )

def M_s_evol(redshift, richness): 
	"""
	Redshift evolution of M star
	Combination of the values in Table 3 and Eq. 10 of Ricci 2018
	"""
	return function_eq10( redshift, richness, M_s_z[0], M_s_z[1], M_s_z[2] )


def Schechter_M_z(M, redshift, richness): 
	"""
	Schechter function as a function of absolute magnitude for a median redshift and a median richness
	Combination of the values in Table 3 and Eq. 8, 9, 10 of Ricci 2018
	"""
	return 0.4 * n.log(10.) * 10**logPhi_evol(redshift, richness) * 10**(0.4 * (M_s_evol(redshift, richness) - M) * (alpha_evol(redshift, richness) + 1)) * n.e**( -10** ( 0.4 * (M_s_evol(redshift,richness) - M)))


def mass_2_richness(M200c, redshift): 
	"""
	M200c to richness conversion using the scaling relation 
	Table 2, second line of Capasso et al. 2019 (1812.0609
	"""
	return 2 * 39.8 * (M200c/3e14)**(0.98) * ((1+redshift)/(1+0.18))**(-1.08)

def Schechter_M_z_M200c(M, redshift, M200c): 
	"""
	Schechter function as a function of absolute magnitude for a median redshift and a median M200c
	Combination of the values in Table 3 and Eq. 8, 9, 10 of Ricci 2018
	"""
	return 0.4 * n.log(10.) * 10**logPhi_evol(redshift, mass_2_richness(M200c, redshift)) * 10**(0.4 * (M_s_evol(redshift, mass_2_richness(M200c, redshift)) - M) * (alpha_evol(redshift, mass_2_richness(M200c, redshift)) + 1)) * n.e**( -10** ( 0.4 * (M_s_evol(redshift,mass_2_richness(M200c, redshift)) - M)))

omega = lambda zz: cosmo.Om0*(1+zz)**3. / cosmo.efunc(zz)**2
DeltaVir_bn98 = lambda zz : (18.*n.pi**2. + 82.*(omega(zz)-1)- 39.*(omega(zz)-1)**2.)/omega(zz)

# modle of the quiescent fraction vs redshift and radius
def frac_old(x, z_cluster): return (erf((-n.log10(x) + 0.1)/0.6)+0.9)*0.38 * (1+z_cluster)**(-0.65)+0.22 


HALO_Mvir = hdu_clu['HALO_Mvir'] 
HALO_Rvir = hdu_clu['HALO_Rvir']
HALO_M500c = hdu_clu['HALO_M500c'] 
HALO_R500c = (DeltaVir_bn98(hdu_clu['redshift_R'])/500. * HALO_M500c / HALO_Mvir)**(1./3.)*HALO_Rvir
HALO_M200c = hdu_clu['HALO_M200c'] 
HALO_R200c = (DeltaVir_bn98(hdu_clu['redshift_R'])/200. * HALO_M200c / HALO_Mvir)**(1./3.)*HALO_Rvir


rds = n.random.random(len(hdu_clu['redshift_R']))
selection = (hdu_clu['redshift_R']<1.0)&(hdu_clu['redshift_R']>0.01)&(HALO_M200c>1e13) & (rds<0.001) 
z_cluster = hdu_clu['redshift_R'][selection]
M200c = HALO_M200c[selection]
R200c = HALO_R200c[selection]
Mstar = M_s_evol(z_cluster, mass_2_richness(M200c, z_cluster))
dist_mod = cosmo.distmod(z_cluster).value

mag_lim = 21.5
magAbs_mag_lim = mag_lim - dist_mod
obse = (Mstar + 0.35 < magAbs_mag_lim ) 

zs = n.arange(0., 2, 0.01)
Ms = M_s_evol(zs, mass_2_richness(2e14, zs))
Ms1e13 = M_s_evol(zs, mass_2_richness(5e13, zs))

p.figure(0, (6,6))
#p.plot(zs, Ms+cosmo.distmod(zs).value, 'k.-', label='Mstar 1e14')
#p.plot(zs, Ms1e13+cosmo.distmod(zs).value, 'r.-', label='Mstar 1e13')
#p.plot(zs, 2+Ms+cosmo.distmod(zs).value, 'k--', label='Mstar+2 1e14')
#p.plot(zs, 2+Ms1e13+cosmo.distmod(zs).value, 'r--', label='Mstar+2 1e13')
#p.plot(zs, 21.5*n.ones_like(zs), 'b+', label='mag lim='+str(mag_lim))
p.plot(zs, 22.7*n.ones_like(zs), label='SDSS 22.7')
p.plot(zs, 23.4*n.ones_like(zs), label='LS DR8/DES DR1 23.4')
p.plot(zs, 26.0*n.ones_like(zs), label='HSC 26.4')
p.fill_between(zs, y1=Ms+cosmo.distmod(zs).value, y2 = 1.7+Ms+cosmo.distmod(zs).value,  alpha=0.2,label=r'$[0.2,1]L^*, M_{200c}=2x10^{14}M_\odot$') 
p.fill_between(zs, y1=Ms1e13+cosmo.distmod(zs).value, y2 = 1.7+Ms1e13+cosmo.distmod(zs).value,  alpha=0.2,label=r'$[0.2,1]L^*, M_{200c}=5x10^{13}M_\odot$') 
#p.yscale('log')
p.ylabel('r mag')
p.xlabel('redshift')
p.xlim((0.1,1.4))
p.ylim((17,26.5))
p.grid()
p.legend(loc=4)
p.title('photometry')
p.savefig(os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters', 'MS_OBS_z-M-'+str(mag_lim)+'.png'))
p.clf()

p.figure(0, (6,6))
#p.plot(zs, Ms+cosmo.distmod(zs).value, 'k.-', label='Mstar 1e14')
#p.plot(zs, Ms1e13+cosmo.distmod(zs).value, 'r.-', label='Mstar 1e13')
#p.plot(zs, 2+Ms+cosmo.distmod(zs).value, 'k--', label='Mstar+2 1e14')
#p.plot(zs, 2+Ms1e13+cosmo.distmod(zs).value, 'r--', label='Mstar+2 1e13')
#p.plot(zs, 21.5*n.ones_like(zs), 'b+', label='mag lim='+str(mag_lim))
p.plot(zs, 21.7*n.ones_like(zs), label='SDSS 21.7')
p.plot(zs, 22.5*n.ones_like(zs), label='4MOST 22.5')
p.plot(zs, 25.0*n.ones_like(zs), label='MSE 25.0')
p.fill_between(zs, y1=Ms+cosmo.distmod(zs).value, y2 = 1.7+Ms+cosmo.distmod(zs).value,  alpha=0.2,label=r'$[0.2,1]L^*, M_{200c}=2x10^{14}M_\odot$') 
p.fill_between(zs, y1=Ms1e13+cosmo.distmod(zs).value, y2 = 1.7+Ms1e13+cosmo.distmod(zs).value,  alpha=0.2,label=r'$[0.2,1]L^*, M_{200c}=5x10^{13}M_\odot$') 
#p.yscale('log')
p.ylabel('r mag')
p.xlabel('redshift')
p.xlim((0.1,1.4))
p.ylim((17,26.5))
p.grid()
p.legend(loc=4)
p.title('spectroscopy')
p.savefig(os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters', 'MS_OBS_z-SPEC-M-'+str(mag_lim)+'.png'))
p.clf()

p.figure(0, (6,6))
p.plot(zs, Ms, 'k+', label='Mstar 1e14')
p.plot(zs, Ms1e13, 'r+', label='Mstar 1e13')
p.plot(zs, mag_lim-cosmo.distmod(zs).value, 'b+', label='mag lim='+str(mag_lim))
#p.yscale('log')
p.legend()
p.savefig(os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters', 'MS_z-M-'+str(mag_lim)+'.png'))
p.clf()


p.figure(0, (6,6))
p.plot(z_cluster, M200c, 'k+')
p.plot(z_cluster[obse], M200c[obse], 'b+')
p.yscale('log')
p.savefig(os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters', 'z-M-'+str(mag_lim)+'.png'))
p.clf()


#idx = 14404872
#z_cluster = hdu_clu['redshift_R'][idx]
#M200c = HALO_M200c[idx]
#R200c = HALO_R200c[idx]
def get_observability(z_clu, m200, r200, mag_lim ):
	richness = mass_2_richness(m200, z_clu)
	Mstar = M_s_evol(z_clu, richness)
	MstarM2 = Mstar + 0.35 
	dist_mod = cosmo.distmod(z_clu).value
	#print(n.log10(m200), richness)
	magObs_faintest = dist_mod + MstarM2  
	magAbs_mag_lim = mag_lim - dist_mod
	fun = lambda x : frac_old(x, z_clu)
	red_fraction = quad(fun, 0., 1)[0]
	#print('z_clu', z_clu)
	area_Mpc2 = n.pi * (r200 / 1000)**2
	#print('area_Mpc2', area_Mpc2)
	delta_mag = 0.01
	mrange = n.arange(-30, -10, delta_mag)
	diff_number = Schechter_M_z_M200c(mrange, z_clu, m200) * area_Mpc2 * delta_mag * n.log(10)
	total_number = n.cumsum(Schechter_M_z_M200c( mrange, z_clu, m200) * area_Mpc2 * delta_mag * n.log(10))
	#print('total_number', total_number, diff_number)
	x_itp = n.hstack((mrange[total_number > 0.1], mrange[total_number > 0.1][-1]))
	y_itp = n.hstack((total_number[total_number > 0.1], total_number[total_number > 0.1][-1] * 10))
	itp_clu = interp1d(x_itp, y_itp)
	N_gal_MstarM2_observable = itp_clu(magAbs_mag_lim)
	N_gal_MstarM2_observable_red = red_fraction * N_gal_MstarM2_observable
	N_gal_MstarM2 = itp_clu(MstarM2)
	N_gal_MstarM2_red = red_fraction * N_gal_MstarM2
	#print('N gal', N_gal_MstarM2, N_gal_MstarM2_observable)
	#print('N gal red', N_gal_MstarM2_red, N_gal_MstarM2_observable_red)
	observable = N_gal_MstarM2_red < N_gal_MstarM2_observable_red
	return observable.astype('int'), richness, N_gal_MstarM2_red, N_gal_MstarM2_observable_red

mag_lim = 21.5

obs = (n.zeros_like(z_cluster)==1)
richness = n.zeros_like(z_cluster)
N_gal_MstarM2_red, N_gal_MstarM2_observable_red = n.zeros_like(z_cluster), n.zeros_like(z_cluster)
for jj, (aa, bb, cc) in enumerate(zip(z_cluster, M200c, R200c)):
	try :
		obs[jj], richness[jj], N_gal_MstarM2_red[jj], N_gal_MstarM2_observable_red[jj]  = get_observability(aa, bb, cc, mag_lim)
	except(ValueError):
		print('err', jj, (aa, bb, cc))

print(n.mean(richness/N_gal_MstarM2_red))
print(n.std(richness/N_gal_MstarM2_red))


p.figure(0, (6,6))
p.plot(z_cluster, M200c, 'k+')
p.plot(z_cluster[obs], M200c[obs], 'b+')
p.yscale('log')
p.savefig(os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters', 'z-M-'+str(mag_lim)+'.png'))
p.clf()


mag_lim = 22.5

obs = (n.zeros_like(z_cluster)==1)
richness = n.zeros_like(z_cluster)
for jj, (aa, bb, cc) in enumerate(zip(z_cluster, M200c, R200c)):
	try :
		obs[jj], richness[jj] = get_observability(aa, bb, cc, mag_lim)
	except(ValueError):
		print('err', jj, (aa, bb, cc))

p.figure(0, (6,6))
p.plot(z_cluster, M200c, 'k+')
p.plot(z_cluster[obs], M200c[obs], 'b+')
p.yscale('log')
p.savefig(os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters', 'z-M-'+str(mag_lim)+'.png'))
p.clf()

mag_lim = 23.5

obs = (n.zeros_like(z_cluster)==1)
richness = n.zeros_like(z_cluster)
for jj, (aa, bb, cc) in enumerate(zip(z_cluster, M200c, R200c)):
	try :
		obs[jj], richness[jj] = get_observability(aa, bb, cc, mag_lim)
	except(ValueError):
		print('err', jj, (aa, bb, cc))

p.figure(0, (6,6))
p.plot(z_cluster, M200c, 'k+')
p.plot(z_cluster[obs], M200c[obs], 'b+')
p.yscale('log')
p.savefig(os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters', 'z-M-'+str(mag_lim)+'.png'))
p.clf()

mag_lim = 24.5

obs = (n.zeros_like(z_cluster)==1)
richness = n.zeros_like(z_cluster)
for jj, (aa, bb, cc) in enumerate(zip(z_cluster, M200c, R200c)):
	try :
		obs[jj], richness[jj] = get_observability(aa, bb, cc, mag_lim)
	except(ValueError):
		print('err', jj, (aa, bb, cc))

p.figure(0, (6,6))
p.plot(z_cluster, M200c, 'k+')
p.plot(z_cluster[obs], M200c[obs], 'b+')
p.yscale('log')
p.savefig(os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters', 'z-M-'+str(mag_lim)+'.png'))
p.clf()

mag_lim = 25.5

obs = (n.zeros_like(z_cluster)==1)
richness = n.zeros_like(z_cluster)
for jj, (aa, bb, cc) in enumerate(zip(z_cluster, M200c, R200c)):
	try :
		obs[jj], richness[jj] = get_observability(aa, bb, cc, mag_lim)
	except(ValueError):
		print('err', jj, (aa, bb, cc))

p.figure(0, (6,6))
p.plot(z_cluster, M200c, 'k+')
p.plot(z_cluster[obs], M200c[obs], 'b+')
p.yscale('log')
p.savefig(os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters', 'z-M-'+str(mag_lim)+'.png'))
p.clf()
