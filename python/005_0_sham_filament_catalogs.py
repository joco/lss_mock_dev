"""
What it does
------------

Creates a fits catalog containing cosmology tracers BGx2, LRG, ELG, QSO, LyaQSO for each healpix 768 pixel of 13 deg2 (NSIDE=8)

Command to run
--------------

python3 005_0_sham_cosmology_catalogs.py environmentVAR 

environmentVAR: environment variable linking to the directory where files are e.g. MD10
It will then work in the directory : $environmentVAR/hlists/fits/

Dependencies
------------
topcat/stilts
import time, os, sys, numpy, scipy, astropy, h5py, astropy_healpix, matplotlib

"""
import time
t0 = time.time()

from astropy_healpix import healpy
import sys, os, json

from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u

import astropy.io.fits as fits
import numpy as n
print('CREATES FITS FILES with SHAM method')
print('------------------------------------------------')
print('------------------------------------------------')

env = sys.argv[1]  # 'MD04'

if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT


nl = lambda selection : len(selection.nonzero()[0])

area = healpy.nside2pixarea(8, degrees=True)

root_dir = os.path.join(os.environ[env])

dir_2_gal_all = os.path.join(root_dir, "cat_GALAXY_all")

dir_2_OUT = os.path.join(root_dir, "cat_SHAM_COSMO")

if os.path.isdir(dir_2_OUT) == False:
	os.system('mkdir -p ' + dir_2_OUT)

# dz = 0.005 is a finer binning than any snapshot steps. The smallet step is 0.011 in dz.
#dz = 0.05
dz = 0.01

def json_read(fn):
    with open(fn,'r') as f:
        fixed_json = ''.join(line for line in f if not line.startswith('#'))
        mydict     = json.loads(fixed_json)
    f.close()
    return mydict

def get_nz(fn,sel,ntot,dz):
    # ntot = density * area (total number of targets)
    mydict  = json_read(fn)
    z          = n.arange(mydict['zmin'],mydict['zmax']+dz,dz)
    ps,mus,sds = mydict[sel]['p'],mydict[sel]['mu'],mydict[sel]['sd']
    n1          = n.array([p*norm.pdf(z,mu,sd) for p,mu,sd in zip(ps,mus,sds)])
    n1          = n.sum(n1,axis=0)
    n1         *= ntot
    return n.floor(n1+3).astype('int')

fn = os.path.join( os.environ['GIT_AGN_MOCK'], 'data', 'cosmo-4most', '4most_s8_kidsdr4.nzgmm.json')
mydict  = json_read(fn)

# redshift array
all_z = n.arange(mydict['zmin'],mydict['zmax']+dz, dz)
zmin_all, zmax_all = all_z, all_z+dz
zmid_all = (zmax_all + zmin_all)/2.

# density of tracers per deg-2
#BG_density     = 250. # 185138
#LRG_density    = 400. # 253764
#ELG_density    = 1200.
#QSO_density    = 190.
#QSOLya_density = 50.
#BG_S5_density = 400. # 547756
#BG_S5_density_unique = 200. # 268067

#s5_fn = os.path.join( os.environ['GIT_AGN_MOCK'], 'data', 'cosmo-4most', 'filamentSurvey.nzgmm.json')
#s5_mydict  = json_read(s5_fn)
## redshift array
#s5_all_z = n.arange(s5_mydict['zmin'], s5_mydict['zmax']+dz, dz)
#s5_zmin_all, s5_zmax_all = s5_all_z, s5_all_z+dz
#s5_zmid_all = (s5_zmax_all + s5_zmin_all)/2.
#NN_s5bg  = get_nz(s5_fn, 'R195' , BG_S5_density_unique * 1.22 * area * dz, dz)
#selection_s5bg = (NN_s5bg>8) & (s5_zmid_all>0.05) & (s5_zmid_all<0.51) 
#print('NN_s5bg N/deg2 = ', n.sum(NN_s5bg[selection_s5bg])/area )

N_pixels = healpy.nside2npix(8)
def run_mock(HEALPIX_id):
	#HEALPIX_id=359
	# galaxy catalogs
	path_2_gal_all_catalog = os.path.join(dir_2_gal_all, str(HEALPIX_id).zfill(6) + '.fit')
	# output catalog
	path_2_OUT_catalog_BG_S5 = os.path.join(dir_2_OUT, 'FHSK19_'  + str(HEALPIX_id).zfill(6) + '.fit')

	print('=================================================================')
	print(path_2_gal_all_catalog)
	print( path_2_OUT_catalog_BG_S5  )

	hd_all = fits.open(path_2_gal_all_catalog)[1].data
	N_GAL_all = len(hd_all['ra'])

	####################################
	####################################
	# GALAXIES
	####################################
	####################################

	ra_all  =  hd_all['RA']           
	dec_all =  hd_all['DEC']          
	ebv_all =  hd_all['ebv'] 

	all_mvir = n.log10( hd_all['HALO_Mvir'] )                 
	logm     = hd_all['galaxy_SMHMR_mass']        
	sfr      = hd_all['galaxy_star_formation_rate'] 
	allz     = hd_all['redshift_R']
	distance_modulus = cosmo.distmod(allz)   
	all_kmag = hd_all['K_mag_abs'] + distance_modulus.value
	rds      = norm.rvs(loc=0, scale=0.15, size=len(logm))
	all_vmax = logm + rds
	N_halos = len(all_vmax)
	
	print("======================================")
	# S5 BG case
	#s5_selection = (all_kmag < 17.3) & (logm > 0)
	s5_selection = (all_kmag < 19.0) & (logm > 0)
	print("Filament S5, time=", time.time()-t0)
	#for zmin, zmax, N_s5lrg1 in zip(s5_zmin_all[selection_s5bg], s5_zmax_all[selection_s5bg], NN_s5bg[selection_s5bg]):
		#z_sel = (allz>=zmin)&(allz<zmax)
		#if len(allz[z_sel]) > N_s5lrg1 :
			#print( zmin, zmax, N_s5lrg1, len(allz[z_sel]) )
			#all_vmax_sort_id = n.argsort(all_vmax[z_sel])
			#min_mass = all_vmax[z_sel][all_vmax_sort_id[-N_s5lrg1-1]]
			#mass_selection = (all_vmax>min_mass)&(z_sel)
			#s5_selection = (mass_selection) | (s5_selection)

	print('N FILAMENT S5 selected', len(s5_selection.nonzero()[0]), ', density=', len(s5_selection.nonzero()[0])/area )
	# write a BG catalogue
	t = Table(hd_all[s5_selection])
	#t['RA'] = Column(ra_all[s5_selection], unit='degree', dtype=n.float64)
	#t['DEC'] = Column(dec_all[s5_selection], unit='degree', dtype=n.float64)
	#t['Z'] = Column(allz[s5_selection], unit='', dtype=n.float32)
	#t['Mstar'] = Column(logm[s5_selection], unit='log10(Mass/[Msun])', dtype=n.float32)
	#t['SFR'] = Column(sfr[s5_selection], unit='log10(SFR/[Msun/yr])', dtype=n.float32)
	#t['EBV'] = Column(ebv_all[s5_selection], unit='mag', dtype=n.float32)
	#t['K_mag_abs'] = Column(hd_all['K_mag_abs'][s5_selection], unit='mag', dtype=n.float32)
	t.write(path_2_OUT_catalog_BG_S5, overwrite=True)#
	print(path_2_OUT_catalog_BG_S5, 'written', time.time() - t0)

	# LRG1 CASE
	# SHAM with scatter until NZ is filled for cen and sat
	# scatter 0.15
	#lrg1_selection = (n.ones(N_halos)==0)
	#print("BG S8, time=", time.time()-t0)
	#for zmin, zmax, N_lrg1 in zip(zmin_all[(selection_s8bg)], zmax_all[(selection_s8bg)], NN_s8bg[(selection_s8bg)]):
		#z_sel = (allz>=zmin)&(allz<zmax) # &(s5_selection==False)
		#print(zmin, zmax, N_lrg1, len(allz[z_sel]))
		#if len(allz[z_sel]) > N_lrg1 :
			#all_vmax_sort_id = n.argsort(all_vmax[z_sel])
			#min_mass = all_vmax[z_sel][all_vmax_sort_id[-N_lrg1-1]]
			#mass_selection = (all_vmax>min_mass) & (z_sel) & (logm > 0)
			#lrg1_selection = (mass_selection) | (lrg1_selection)

	#print('N BG S8 selected', len(lrg1_selection.nonzero()[0]),', density=', len(lrg1_selection.nonzero()[0])/area )
	#t = Table(hd_all[lrg1_selection])
	#t.write(path_2_OUT_catalog_BG, overwrite=True)#
	#print(path_2_OUT_catalog_BG, 'written', time.time() - t0)



for HEALPIX_id in n.arange(N_pixels):
	run_mock(HEALPIX_id)

#for HEALPIX_id in n.arange(N_pixels):
	#print("""nohup nice -n 19  python 005_0_sham_cosmology_catalogs.py MD10 """+str(HEALPIX_id).zfill(3)+""" >  cosmo_4most_run_MD10_"""+str(HEALPIX_id).zfill(3)+""".log &  """   )               
