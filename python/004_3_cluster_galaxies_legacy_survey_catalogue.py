"""
What it does
------------

Matches to the closest legacy survey entry

Parse this catalogue in redshift bins by 0.05 

stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_00_01.fits cmd='select "photo_z<=0.1 && photo_z>=0.0 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_01_02.fits cmd='select "photo_z<=0.2 && photo_z>=0.1 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_02_03.fits cmd='select "photo_z<=0.3 && photo_z>=0.2 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_03_04.fits cmd='select "photo_z<=0.4 && photo_z>=0.3 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_04_05.fits cmd='select "photo_z<=0.5 && photo_z>=0.4 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_05_06.fits cmd='select "photo_z<=0.6 && photo_z>=0.5 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_06_07.fits cmd='select "photo_z<=0.7 && photo_z>=0.6 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_07_08.fits cmd='select "photo_z<=0.8 && photo_z>=0.7 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_08_09.fits cmd='select "photo_z<=0.9 && photo_z>=0.8 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_09_10.fits cmd='select "photo_z<=1.0 && photo_z>=0.9 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_10_11.fits cmd='select "photo_z<=1.1 && photo_z>=1.0 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_11_12.fits cmd='select "photo_z<=1.2 && photo_z>=1.1 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_12_13.fits cmd='select "photo_z<=1.3 && photo_z>=1.2 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_13_14.fits cmd='select "photo_z<=1.4 && photo_z>=1.3 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_14_15.fits cmd='select "photo_z<=1.5 && photo_z>=1.4 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_15_16.fits cmd='select "photo_z<=1.6 && photo_z>=1.5 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_16_17.fits cmd='select "photo_z<=1.7 && photo_z>=1.6 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_17_18.fits cmd='select "photo_z<=1.8 && photo_z>=1.7 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_18_19.fits cmd='select "photo_z<=1.9 && photo_z>=1.8 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_19_20.fits cmd='select "photo_z<=2.0 && photo_z>=1.9 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_20_21.fits cmd='select "photo_z<=2.1 && photo_z>=2.0 "'
stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_21_22.fits cmd='select "photo_z<=2.2 && photo_z>=2.1 "'


"""

#cmd = lambda zmin, zmax, zstr : """stilts tpipe in=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat.fits ifmt=fits omode=out ofmt=fits out=$DARKSIM_DIR/observations/legacysurvey/dr6dr7_photoz_csp_totcat_"""+zstr+""".fits cmd='select "photo_z<="""+str(zmax)+""" && photo_z>="""+str(zmin)+""" "'"""

#for jj in n.arange(0., 2.2, 0.1):
	#zstr = str(int(jj*10)).zfill(2) + "_" + str(int((jj+0.1)*10)).zfill(2)
	#print(cmd(n.round(jj,1), n.round(jj+0.1,1), zstr))

import glob
import sys
from astropy_healpix import healpy
import os
import time
t0 = time.time()

import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
from sklearn.neighbors import BallTree
from scipy.interpolate import interp1d
import astropy.io.fits as fits
import h5py
import numpy as n
from astropy.table import Table, Column
	
print('CREATES FITS FILE with galaxies around clusters')
print('------------------------------------------------')
print('------------------------------------------------')

env = sys.argv[1]
baseName = sys.argv[2]
delta_crit = sys.argv[3]
CLU_catalog_name = sys.argv[4]
#env="MD10" 
#baseName="all_0.89510"
#delta_crit = '200c'
#delta_crit = '500c'
#delta_crit = 'vir'
#delta_crit = '2rvir'

z_snap = 1./float(baseName.split('_')[1])-1.
aexp_str = str(int(float(baseName.split('_')[1])*1e5)).zfill(6)
print(env, baseName, delta_crit, CLU_catalog_name)

# simulation setup
if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

path_2_CLU_SAT_catalog = os.path.join(os.environ[env], 'fits', baseName + '_galaxiesAroundClusters_' + CLU_catalog_name[:-5] +'.fit')
path_2_out_catalog = os.path.join(os.environ[env], 'fits', baseName + '_galaxiesAroundClusters_' + CLU_catalog_name[:-5] +'_LS.fit')

# simulation table
hdu_clu = Table.read(path_2_CLU_SAT_catalog)

ra          = hdu_clu['RA']
dec         = hdu_clu['DEC']
halo_id     = hdu_clu['id']
line_number = n.arange(len(ra))

zzr = hdu_clu['redshift_R']
shell_z_min = n.min(zzr)
shell_z_max = n.max(zzr)

log_mass = hdu_clu['SMHMR_mass']
log_mass[log_mass<0] = 3
shell_mass_min = n.min(log_mass)
shell_mass_max = n.max(log_mass)

#log_sfr = hdu_clu['star_formation_rate']
#log_sfr[log_sfr<-50] = -20
#shell_sfr_min = n.min(log_sfr)
#shell_sfr_max = n.max(log_sfr)

# defined quenched, not quenched, defined in script 004_3_cluster_red_galaxies.py
#log_ssfr = log_sfr - log_mass
#is_QU = (log_ssfr < -10.3)
is_quiescent = hdu_clu['is_quiescent']
is_SF = (hdu_clu['is_quiescent'] == False)
#is_quiescent
#star_formation_rate-SMHMR_mass

SIMULATION_QU = n.transpose([zzr, log_mass])[is_quiescent]
SIMULATION_SF = n.transpose([zzr, log_mass])[is_SF]

# snapshots are close enough that there is enough with two redshift shells in the real data
zstrs = n.array([
	str(int((shell_z_min)*10)).zfill(2) + "_" + str(int((shell_z_min+0.1)*10)).zfill(2),
	str(int((shell_z_min+0.1)*10)).zfill(2) + "_" + str(int((shell_z_min+0.2)*10)).zfill(2) 
	])
p_2_cat = lambda zstr : os.path.join( os.environ['DARKSIM_DIR'], 'observations/legacysurvey/dr6dr7_photoz_csp_totcat_'+zstr+'.fits' ) 
for zstr in zstrs:
	print(p_2_cat(zstr))
print('loading catalogues', time.time()-t0)
cat_data = n.array([Table.read(p_2_cat(zstr)) for zstr in zstrs])
t_LS = n.hstack((cat_data))
# divied the Table into SF and quenched
# data_zzr      = t_LS['photo_z']
# data_log_mass = t_LS['MASS_BEST']
data_log_ssfr  = t_LS['SSFR_BEST'] 
data_quenched = (data_log_ssfr<-10)
data_SF = (data_log_ssfr>=-10)

t_LS_QU = t_LS[data_quenched]
t_LS_SF = t_LS[data_SF]

print('creates data tree', time.time()-t0)
Tree_LS_QU = BallTree( n.transpose([t_LS_QU['photo_z'], t_LS_QU['MASS_BEST']]) ) 
Tree_LS_SF = BallTree( n.transpose([t_LS_SF['photo_z'], t_LS_SF['MASS_BEST']]) ) 

LS_ID_QU = n.arange( len( t_LS_QU['photo_z'] ) )
LS_ID_SF = n.arange( len( t_LS_SF['photo_z'] ) )


print('matches data - simulation', time.time()-t0)
ids_out_QU = Tree_LS_QU.query(SIMULATION_QU, k=1, return_distance = False)
ids_QU = n.hstack((ids_out_QU))
id_to_map_QU = LS_ID_QU[ids_QU]

ids_out_SF = Tree_LS_SF.query(SIMULATION_SF, k=1, return_distance = False)
ids_SF = n.hstack((ids_out_SF))
id_to_map_SF = LS_ID_SF[ids_SF]

t_out_QU = Table( t_LS_QU[id_to_map_QU] )
t_out_SF = Table( t_LS_SF[id_to_map_SF] )


t_out_QU['RA']  = ra  [is_quiescent]
t_out_QU['DEC'] = dec [is_quiescent]
t_out_QU.add_column(Column(name='simulation_id' , data=  halo_id  [is_quiescent] ) ) 
t_out_QU.add_column(Column(name='line_number' , data= line_number [is_quiescent]  ) ) 

t_out_SF['RA']  = ra  [is_SF]
t_out_SF['DEC'] = dec [is_SF]
t_out_SF.add_column(Column(name='simulation_id' , data=  halo_id  [is_SF] ) ) 
t_out_SF.add_column(Column(name='line_number' , data= line_number [is_SF]  ) ) 

t_out = Table( n.hstack((t_out_QU, t_out_SF)) )

print('creates output table', time.time()-t0)
#t_out = Table(t_LS[id_to_map])
#t_out['RA']  = ra
#t_out['DEC'] = dec
#t_out.add_column(Column(name='simulation_id' , data= halo_id ) ) 

t_out.write(path_2_out_catalog, overwrite=True)
print('written to ',path_2_out_catalog, 'after ', time.time()-t0, ' seconds')
