"""
Author
------

mainly: T. Liu

What it does
------------

python2.7 on ds43 !!

- 1. Create the AGN X-ray spectra

References
----------

Command to run
--------------

python3 003_5_agn_create_spectra.py

arguments
---------


Dependencies
------------

import xspec

import time, os, sys, glob, numpy, astropy, scipy, matplotlib

"""
import sys
import os
import time
import xspec as XS
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
from scipy.interpolate import interp1d
import astropy.io.fits as fits
import numpy as n
print('CREATES SIMPUT AGN SPECTRA')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

env = 'MD40' # sys.argv[1]  # 'MD04'

# simulation setup
if env[:2] == "MD" :
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env[:4] == "UNIT" :
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

root_dir = os.path.join(os.environ[env])
dir_2_SMPT = os.path.join(root_dir, "cat_CLU_SIMPUT")
dir_2_eRO_all = os.path.join(dir_2_SMPT, "cluster_Xspectra")

if os.path.isdir(dir_2_SMPT) == False:
    os.system('mkdir -p ' + dir_2_SMPT)
if os.path.isdir(dir_2_eRO_all) == False:
    os.system('mkdir -p ' + dir_2_eRO_all)

XS.Xset.cosmo = str(cosmo.H0.value) + " " + str(cosmo.Ode0)


def saveintofile(FileName, N=0):
    if N > 0:
        XS.AllData.dummyrsp(lowE=0.1, highE=50, nBins=N)
    XS.Plot('model')
    Col_E = fits.column.Column(array=n.array(
        [XS.Plot.x(), ], dtype=n.object), name='ENERGY', format='PE()', unit='keV')
    Col_F = fits.column.Column(
        array=n.array(
            [
                XS.Plot.model(),
            ],
            dtype=n.object),
        name='FLUXDENSITY',
        format='PE()',
        unit='photon/s/cm**2/keV')
    HDU1 = fits.BinTableHDU.from_columns([Col_E, Col_F])
    HDU1.header['EXTNAME'] = 'SPECTRUM'
    HDU1.header['HDUCLAS1'] = 'SPECTRUM'
    HDU1.writeto(os.path.join(dir_2_eRO_all, FileName), overwrite=True)

XS.Model('TBabs(apec)')
XS.AllModels(1).TBabs.nH = 10**(20 - 22)
XS.AllModels(1).apec.kT = 2.0
XS.AllModels(1).apec.Abundanc = 0.3
XS.AllModels(1).apec.Redshift = 0.0
XS.AllModels(1).apec.norm = 1.0

kt_arr = 10**n.arange(-1,1.3,0.05)
z_arr = n.hstack((n.array([0., 0.01]), 10**n.arange(n.log10(0.02), n.log10(4.), 0.05)))
galNH = n.arange(19.0, 22.6, 0.1)

kt_arrays, z_arrays, nh_arrays = n.meshgrid(kt_arr, z_arr, galNH)

kt_array = n.hstack(( n.hstack((kt_arrays)) ))
z_array  = n.hstack(( n.hstack((z_arrays))  ))
nh_array = n.hstack(( n.hstack((nh_arrays)) ))

for temperature, redshift, nh_val in zip(kt_array, z_array, nh_array):
	XS.AllModels(1).TBabs.nH = 10**(nh_val - 22)
	XS.AllModels(1).apec.kT = temperature
	XS.AllModels(1).apec.Redshift = redshift
	filename = 'galNH_' + str(n.round(nh_val, 3)) +'_10000kT_' + str(int(10000*temperature)) + '_10000z_' + str(int(10000*redshift)) + '.fits'
	print(filename)
	saveintofile(filename, 1024)
 
 
