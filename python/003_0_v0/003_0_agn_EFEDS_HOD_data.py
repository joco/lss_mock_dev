"""
What it does
------------

Computes the AGN model from Comparat et al. 2019.

cd $GIT_AGN_MOCK/python

python 003_0_agn_EFEDS_HOD_data.py MD10 10 0.2 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 0.3 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 0.4 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 0.5 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 0.6 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 0.7 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 0.8 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 0.9 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 1.0 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 1.1 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 1.2 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 1.3 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 1.4 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 1.5 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 1.6 
python 003_0_agn_EFEDS_HOD_data.py MD10 10 1.7 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 0.2 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 0.3 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 0.4 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 0.5 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 0.6 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 0.7 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 0.8 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 0.9 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 1.0 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 1.1 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 1.2 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 1.3 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 1.4 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 1.5 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 1.6 
python 003_0_agn_EFEDS_HOD_data.py MD10 06 1.7 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 0.2 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 0.3 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 0.4 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 0.5 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 0.6 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 0.7 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 0.8 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 0.9 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 1.0 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 1.1 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 1.2 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 1.3 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 1.4 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 1.5 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 1.6 
python 003_0_agn_EFEDS_HOD_data.py MD10 14 1.7 


"""
import sys
import os
import time
import extinction
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import astropy.constants as cc
import astropy.io.fits as fits
from astropy.table import Table, Column
from scipy.special import erf
from scipy.stats import norm
from scipy.interpolate import interp2d
from scipy.interpolate import interp1d
import healpy
#import h5py
import numpy as n
print('Creates AGN mock catalogue ')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

env = sys.argv[1] # 'MD10'
f_sat_pc = int(sys.argv[2]) # 10 # percent
f_sat = f_sat_pc / 100.
scatter_0 = float(sys.argv[3]) # 1.4

print(env, 'f sat', f_sat, 'scatter', scatter_0)
simput_dir = os.path.join(os.environ[env], 'WTH_fsat_'+str(f_sat_pc)+'_sigma_'+str(scatter_0))
if os.path.isdir(simput_dir) == False:
	os.system('mkdir -p ' + simput_dir)
hist_dir = os.path.join( simput_dir, 'HIST')
if os.path.isdir(hist_dir) == False:
	os.system('mkdir -p ' + hist_dir)
	
gal_dir = os.path.join(os.environ[env], 'cat_GALAXY_all/HIST')

out_dir =  os.path.join(os.environ['GIT_AGN_MOCK'], 'data/HOD_AGN')
gal_out_dir =  os.path.join(os.environ['GIT_AGN_MOCK'], 'data/HOD_GAL')

m_bins, z_bins = n.arange(11.4, 15.5, 0.1), n.arange(0,6.2, 0.1)
H_2ds, H_2Cs, H_c, H_s = [], [], [], []
for PIX_ID in n.arange(healpy.nside2npix(8)):
	path_2_HistOut_file = os.path.join( hist_dir, 'Hist_Mvir_zz_'+str(PIX_ID).zfill(6)+'.ascii')
	path_2_HistCEN_file = os.path.join( hist_dir, 'HistCen_Mvir_zz_'+str(PIX_ID).zfill(6)+'.ascii')
	H_2ds.append(n.loadtxt(path_2_HistOut_file))
	H_2Cs.append(n.loadtxt(path_2_HistCEN_file))
	#path_2_HistOut_GAL = os.path.join( gal_dir, 'HistSat_Mvir_zz_'+str(PIX_ID).zfill(6)+'.ascii')  
	#path_2_HistCEN_GAL = os.path.join( gal_dir, 'Hist_Mvir_zz_'+str(PIX_ID).zfill(6)+'.ascii')
	#H_s.append(n.loadtxt(path_2_HistOut_GAL))
	#H_c.append(n.loadtxt(path_2_HistCEN_GAL))

#H_GAL_C = n.sum(H_c, axis=0)
#H_GAL_S = n.sum(H_s, axis=0)
H_AGN_C = n.sum(H_2Cs, axis=0)
H_AGN_S = n.sum(H_2ds, axis=0)

n.savetxt(os.path.join(out_dir, 'fsat_'+str(f_sat_pc)+'_sigma_'+str(scatter_0)+'_CEN.ascii' ), H_AGN_C)
n.savetxt(os.path.join(out_dir, 'fsat_'+str(f_sat_pc)+'_sigma_'+str(scatter_0)+'_SAT.ascii' ), H_AGN_S)

#n.savetxt(os.path.join(gal_out_dir, 'CEN.ascii' ), H_GAL_C)
#n.savetxt(os.path.join(gal_out_dir, 'SAT.ascii' ), H_GAL_S)
