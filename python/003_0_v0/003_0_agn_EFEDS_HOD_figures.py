"""
What it does
------------

Figures of the obtained HOD
"""
import sys, os, glob, healpy
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import astropy.constants as cc
import astropy.io.fits as fits
from astropy.table import Table, Column
from scipy.special import erf
from scipy.stats import norm
from scipy.interpolate import interp2d
from scipy.interpolate import interp1d
import numpy as n

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p

print('Creates HOD figures')
print('------------------------------------------------')
print('------------------------------------------------')

out_dir =  os.path.join(os.environ['GIT_AGN_MOCK'], 'data/HOD_AGN')
gal_out_dir =  os.path.join(os.environ['GIT_AGN_MOCK'], 'data/HOD_GAL')

list_sats = n.array(glob.glob(os.path.join(out_dir, 'fsat_*_sigma_*_SAT.ascii' )))
list_cens = n.array(glob.glob(os.path.join(out_dir, 'fsat_*_sigma_*_CEN.ascii' )))
list_sats.sort()
list_cens.sort()

SAT_f_sat = n.array([ float(os.path.basename(el).split('_')[1]) for el in list_sats ])
SAT_sigma = n.array([ float(os.path.basename(el).split('_')[3]) for el in list_sats ])
CEN_f_sat = n.array([ float(os.path.basename(el).split('_')[1]) for el in list_cens ])
CEN_sigma = n.array([ float(os.path.basename(el).split('_')[3]) for el in list_cens ])

m_bins, z_bins = n.arange(11.4, 15.5, 0.1), n.arange(0,6.2, 0.1)
z_mid = ( z_bins[1:] + z_bins[:-1] ) / 2.
m_mid = ( m_bins[1:] + m_bins[:-1] ) / 2.

H_AGN_S = n.array([ n.loadtxt(el) for el in list_sats ])
H_AGN_C = n.array([ n.loadtxt(el) for el in list_cens ])

H_GAL_C = n.loadtxt(os.path.join(gal_out_dir, 'CEN.ascii' ))
H_GAL_S = n.loadtxt(os.path.join(gal_out_dir, 'SAT.ascii' ))

#idx = n.searchsorted(z_mid, 1.3)
for fsat_val in [6, 10, 14]:
	for idx in n.arange(len(z_mid)):
		H_AGN_S_Mhist = n.array([ el.T[idx] for el in H_AGN_S ])
		H_AGN_C_Mhist = n.array([ el.T[idx] for el in H_AGN_C ])

		H_GAL_C_Mhist = H_GAL_C.T[idx] 
		H_GAL_S_Mhist = H_GAL_S.T[idx] 

		# an array of parameters, each of our curves depend on a specific
		# value of parameters
		parameters = n.unique(SAT_sigma)

		# norm is a class which, when called, can normalize data into the
		# [0.0, 1.0] interval.
		norm = matplotlib.colors.Normalize(
			vmin=n.min(parameters),
			vmax=n.max(parameters))

		# choose a colormap
		c_m = matplotlib.cm.coolwarm

		# create a ScalarMappable and initialize a data structure
		s_m = matplotlib.cm.ScalarMappable(cmap=c_m, norm=norm)
		s_m.set_array([])

		p_2_figure = os.path.join(out_dir, 'hod_z='+str(n.round(z_mid[idx],2))+ '_fsat='+str(n.round(fsat_val,0)) + '.png' )
		print(p_2_figure)
		p.figure(3, (6.5,5.5))
		p.axes([0.15,0.15,0.8,0.8])
		for agn_s, agn_c, fsat, sigma in zip(H_AGN_S_Mhist, H_AGN_C_Mhist,  SAT_f_sat, SAT_sigma):
			if fsat==fsat_val:
				p.plot(m_mid, (agn_s + agn_c)/H_GAL_C_Mhist, color=s_m.to_rgba(sigma))
		p.xlabel(r'$\log_{10}(M_{vir}/M_\odot)$')
		p.ylabel(r'$\langle N(M) \rangle$')
		p.yscale('log')
		p.xlim((10.0, 15.5))
		p.ylim((1e-6, 1))
		p.colorbar(s_m, label=r'$\sigma$')
		p.grid()
		p.title('z='+str(n.round(z_mid[idx],2))+ ', fsat='+str(n.round(fsat_val,0)))
		p.savefig(p_2_figure)
		p.clf()

		p_2_figure = os.path.join(out_dir, 'hod_z='+str(n.round(z_mid[idx],2))+ '_fsat='+str(n.round(fsat_val,0))  + '_sat.png' )
		p.figure(3, (6.5,5.5))
		p.axes([0.15,0.15,0.8,0.8])
		for agn_s, agn_c, fsat, sigma in zip(H_AGN_S_Mhist, H_AGN_C_Mhist,  SAT_f_sat, SAT_sigma):
			if fsat==fsat_val:
				p.plot(m_mid, (agn_s)/H_GAL_C_Mhist, color=s_m.to_rgba(sigma))
		p.xlabel(r'$\log_{10}(M_{vir}/M_\odot)$')
		p.ylabel(r'$\langle N_{sat}(M) \rangle$')
		p.yscale('log')
		p.xlim((10.0, 15.5))
		p.ylim((1e-6, 1))
		p.colorbar(s_m, label=r'$\sigma$')
		p.grid()
		p.title('z='+str(n.round(z_mid[idx],2))+ ', fsat='+str(n.round(fsat_val,0)))
		p.savefig(p_2_figure)
		p.clf()


		p_2_figure = os.path.join(out_dir, 'hod_z='+str(n.round(z_mid[idx],2))+ '_fsat='+str(n.round(fsat_val,0))  + '_cen.png' )
		p.figure(3, (6.5,5.5))
		p.axes([0.15,0.15,0.8,0.8])
		for agn_s, agn_c, fsat, sigma in zip(H_AGN_S_Mhist, H_AGN_C_Mhist,  SAT_f_sat, SAT_sigma):
			if fsat==fsat_val:
				p.plot(m_mid, (agn_c)/H_GAL_C_Mhist, color=s_m.to_rgba(sigma))
		p.xlabel(r'$\log_{10}(M_{vir}/M_\odot)$')
		p.ylabel(r'$\langle N_{cen}(M) \rangle$')
		p.yscale('log')
		p.xlim((10.0, 15.5))
		p.ylim((1e-6, 1))
		p.colorbar(s_m, label=r'$\sigma$')
		p.grid()
		p.title('z='+str(n.round(z_mid[idx],2))+ ', fsat='+str(n.round(fsat_val,0)))
		p.savefig(p_2_figure)
		p.clf()






