import numpy as n
import os
import sys
from astropy.table import Table, Column
# select random 50/deg2 in the QSO mock to get all the data
# replace RA, DEC by random positions

root_dir = '/data42s/comparat/firefly/mocks/2021-04/QMOST'
dir_2_QSO  = os.path.join(root_dir, 'S8_QSO.fits.gz'     )
dir_2_RDS      = os.path.join(root_dir, 'QMOST_random_ra-dec-50perdeg2.fits.gz'     )
dir_2_OUT      = os.path.join(root_dir, 'S8_QSO_w_contamination.fits.gz'     )

t_qso = Table.read(dir_2_QSO)
# downsamples to the right total amount
s190 = (n.random.random(len(t_qso)) < (7500*190.+1)/len(t_qso))
t_qso = t_qso[s190]

t_rds = Table.read(dir_2_RDS)

#common_area = lambda ra, dec : (dec<40)
s_RDS =  ((t_rds['MASK_BIT'] & 2**15) >0 ) 
t_RADEC = t_rds[s_RDS]

# fraction selected in the original file
qso_density = len(t_qso)/7500.
print(qso_density)
f_select = 49./qso_density
s1 = (n.random.random(len(t_qso)) < f_select )

N_new = len(s1.nonzero()[0])
t_qso['RA'][s1] = t_RADEC['RA'][:N_new].astype('float32')
t_qso['DEC'][s1] = t_RADEC['DEC'][:N_new].astype('float32')

N1 = n.arange(N_new)
id_list = 6*1e10 + N1
new_NAME = n.array([ str(int(el)).zfill(11) for el in id_list ])
t_qso['NAME'][s1] = new_NAME

t_qso.write(dir_2_OUT, overwrite=True)



