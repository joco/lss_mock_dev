"""
What it does
------------

Plots luminosity functions

"""

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p
from sklearn.neighbors import BallTree

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import extinction
import healpy
#from astropy_healpix import healpy
import sys
import os
import time
from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column
from scipy.optimize import curve_fit
import astropy.io.fits as fits
import h5py
import numpy as n
print('Plots LF of all bands')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

env = 'UNIT_fA1i_DIR'
#HEALPIX_id = int(sys.argv[2])
#print(env, HEALPIX_id)

if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

nl = lambda sel : len(sel.nonzero()[0])
zs = n.arange(0.0000001, 7.1, 0.001)
dm_itp = interp1d(zs, cosmo.distmod(zs).value)

root_dir = os.path.join(os.environ[env])
dir_2_OUT = os.path.join(root_dir, "cat_SHAM_ALL")
dir_2_IN = os.path.join(root_dir, "cat_GALAXY_all")
fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'galaxies')
if os.path.isdir(fig_dir) == False:
    os.system('mkdir -p ' + fig_dir)

Mk_m_5logh_z0, log10_phi_h3_Mpcm3_magm1 = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data/LF_SMF', 'driver-2012-K-LF.txt'), unpack = True )
Msun_h70m2,  phi_All, phi_All_err = n.loadtxt( os.path.join(os.environ['GIT_AGN_MOCK'], 'data/LF_SMF', 'driver-2022-SMF.txt'), unpack = True )

N_pixels = healpy.nside2npix(8)
area_pixel = healpy.nside2pixarea(8, degrees=True)
#for HEALPIX_id in n.arange(N_pixels)[::-1][:10]: #n.arange(N_pixels)[::-1]:
HEALPIX_id = 767
print(HEALPIX_id, time.time()-t0)
path_2_out_K = os.path.join(dir_2_OUT, 'Kmatch_'  + str(HEALPIX_id).zfill(6) + '.fit')
path_2_out_I = os.path.join(dir_2_OUT, 'Imatch_'  + str(HEALPIX_id).zfill(6) + '.fit')
path_2_out_K_C = os.path.join(dir_2_OUT, 'Kmatch_'  + str(HEALPIX_id).zfill(6) + '_C.fit')
path_2_out_I_C = os.path.join(dir_2_OUT, 'Imatch_'  + str(HEALPIX_id).zfill(6) + '_C.fit')

t_k = Table.read(path_2_out_K)
t_k_C = Table.read(path_2_out_K_C)

path_2_COSMOS = os.path.join(os.environ['HOME'],'data1/COSMOS/catalogs/photoz-2.0/photoz_vers2.0_010312_LSDR9only.fits')
t = Table.read(path_2_COSMOS)
good = (t['Separation']<0.4) & (t['photoz']>0. )&( t['photoz']< 2. ) & ( t['Z']>0 )&( t['R']>0 )& ( t['I']>0 )& ( t['J']>0 )& ( t['K']>0 )& ( t['MK']<0 )&( t['MK']>-40 )&( t['mass_med']<14 )&( t['mass_med']>8 )
COSMOS = t[good]

path_2_kids = os.path.join(os.environ['UNIT_fA1i_DIR'], 'G09.GAMADR4+LegacyDR9.galreference+RM.fits')
#path_2_kids = os.path.join(os.environ['HOME'], 'data', 'UNIT_fA1i', 'G09.GAMADR4+LegacyDR9.galreference+RM.fits')
t_kids = Table.read(path_2_kids)
kmag = 8.9 - 2.5*n.log10(t_kids['flux_Kt'])
zmag = 8.9 - 2.5*n.log10(t_kids['flux_Zt'])
rmag = 8.9 - 2.5*n.log10(t_kids['flux_rt'])
imag = 8.9 - 2.5*n.log10(t_kids['flux_it'])
keep_kids = (kmag>0) & (t_kids['z_peak']>0.01) & (t_kids['z_peak']<1.99) & (zmag>0) & (rmag>0)  & (imag>0)
KIDS = t_kids[keep_kids]
##
kmag = 8.9 - 2.5*n.log10(KIDS['flux_Kt'])
KCORR_DATA = n.loadtxt( os.path.join( os.environ['GIT_AGN_MOCK'], 'data/K-corr/VISTA_Ks_kcorrections.txt'), unpack = True)
kcorr_itp = interp1d(KCORR_DATA[0], KCORR_DATA[3])
dm_values = dm_itp(KIDS['z_peak'].data.data)
kmag_abs = kmag - ( dm_values + kcorr_itp(KIDS['z_peak'].data.data) )
KIDS['Kmag_abs'] = kmag_abs


ll_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'data/LF_SMF')

L15_ngal,   L15_M,     L15_phi,    L15_Err = n.loadtxt(os.path.join(ll_dir, 'loveday_2015/smfs.txt'), unpack=True)

smf_ilbert13 = lambda M, M_star, phi_1s, alpha_1s, phi_2s, alpha_2s : ( phi_1s * (M/M_star) ** alpha_1s + phi_2s * (M/M_star) ** alpha_2s ) * n.e ** (-M/M_star) * (M/ M_star)
path_ilbert13_SMF = os.path.join(ll_dir, "ilbert_2013_mass_function_params.txt")
zmin, zmax, N, M_comp, M_star, phi_1s, alpha_1s, phi_2s, alpha_2s, log_rho_s = n.loadtxt(path_ilbert13_SMF, unpack=True)

smf_ilbert_fun = n.array([
lambda mass : smf_ilbert13( mass , 10**M_star[0], phi_1s[0]*10**(-3), alpha_1s[0], phi_2s[0]*10**(-3), alpha_2s[0] )
, lambda mass : smf_ilbert13( mass , 10**M_star[1], phi_1s[1]*10**(-3), alpha_1s[1], phi_2s[1]*10**(-3), alpha_2s[1] )
, lambda mass : smf_ilbert13( mass , 10**M_star[2], phi_1s[2]*10**(-3), alpha_1s[2], phi_2s[2]*10**(-3), alpha_2s[2] )
, lambda mass : smf_ilbert13( mass , 10**M_star[3], phi_1s[3]*10**(-3), alpha_1s[3], phi_2s[3]*10**(-3), alpha_2s[3] )
, lambda mass : smf_ilbert13( mass , 10**M_star[4], phi_1s[4]*10**(-3), alpha_1s[4], phi_2s[4]*10**(-3), alpha_2s[4] )
, lambda mass : smf_ilbert13( mass , 10**M_star[5], phi_1s[5]*10**(-3), alpha_1s[5], phi_2s[5]*10**(-3), alpha_2s[5] )
, lambda mass : smf_ilbert13( mass , 10**M_star[6], phi_1s[6]*10**(-3), alpha_1s[6], phi_2s[6]*10**(-3), alpha_2s[6] )
, lambda mass : smf_ilbert13( mass , 10**M_star[7], phi_1s[7]*10**(-3), alpha_1s[7], phi_2s[7]*10**(-3), alpha_2s[7] )
])


smf_ilbert_zmin = n.array([
0.2
, 0.5
, 0.8
, 1.1
, 1.5
, 2.0
, 2.5
, 3.0 ])

smf_ilbert_zmax = n.array([
0.5
, 0.8
, 1.1
, 1.5
, 2.0
, 2.5
, 3.0
, 4.0 ])

smf_ilbert_name = n.array([ "Ilbert 2013 "+str(zmin)+"<z<"+str(zmax) for zmin, zmax in zip(smf_ilbert_zmin,smf_ilbert_zmax) ])
mdex = 0.1
mbins = n.arange(8, 12.1, mdex)

z_mins = n.arange(0.1, 1.4, 0.1)
z_maxs = n.arange(0.1, 1.4, 0.1) + 0.1
for z_min, z_max in zip( z_mins, z_maxs ):
    fig_out = os.path.join(fig_dir, 'stellarMass_galaxy_F_' + str(HEALPIX_id).zfill(6) +'_'+str(n.round(z_min,1)) +'.png')
    volume = (( cosmo.comoving_volume(z_max) - cosmo.comoving_volume(z_min) ) * area_pixel * n.pi / 129600.).value
    z_sel = ( t_k['redshift_R'] > z_min ) & ( t_k['redshift_R'] < z_max )
    z_sel_C = ( t_k_C['redshift_R'] > z_min ) & ( t_k_C['redshift_R'] < z_max )
    z_mean = n.mean(t_k['redshift_R'][z_sel])

    z_cosmos = (COSMOS['photoz']>z_min) & (COSMOS['photoz']<z_max)
    volume_cosmos =  (( cosmo.comoving_volume(z_max) - cosmo.comoving_volume(z_min) ) * 2 * n.pi / 129600.).value
    MK_arr_cosmos = COSMOS['mass_med'][z_cosmos]

    p.figure(1, (6., 5.5))
    p.axes([0.15, 0.15, 0.8, 0.8])
    p.plot(Msun_h70m2,  10**phi_All, lw=3 , label='Driver 2021, z=0') # +5*n.log10(h)
    #
    #out = p.hist(MK_arr_cosmos, lw=2, weights=n.ones_like(MK_arr_cosmos) / mdex / volume_cosmos , bins = mbins, histtype = 'step', rasterized = True, label = 'COSMOS data')
    #
    #out = p.hist(t_k['galaxy_SMHMR_mass'][z_sel], lw=4, weights=n.ones_like(t_k['galaxy_SMHMR_mass'][z_sel]) / mdex / volume , bins = mbins, histtype = 'step', rasterized = True, label = 'mock', color='black')
    out = p.hist(t_k_C['galaxy_SMHMR_mass'][z_sel_C], lw=2, weights=n.ones_like(t_k_C['galaxy_SMHMR_mass'][z_sel_C]) / mdex / volume , bins = mbins, histtype = 'step', rasterized = True, label = 'MOCK', color='grey')
    #
    ilbert_sel = (z_mean>=smf_ilbert_zmin) & (z_mean<smf_ilbert_zmax)
    if len(ilbert_sel.nonzero()[0])>0:
        fun, name = smf_ilbert_fun[ilbert_sel][0], smf_ilbert_name[ilbert_sel][0]
        p.plot(mbins, fun(10**mbins)/(0.7**3), label=name, ls='dashed', lw=2)

    #p.errorbar( L15_M, y =  L15_phi*0.7**3, yerr=   L15_Err*0.7**3, label='Loveday 15, z<0.65')
    #p.errorbar( L15_M+2*n.log10(0.7),   y =  L15_phi*0.7**(3), yerr=   L15_Err*0.7**(3) , label='Loveday 15, z<0.65')

    p.title(r'$\bar{z}$=' + str(n.round(z_mean, 3)))
    p.ylabel(r'$\Phi/\; dex^{-1}\; Mpc^{-3}$')
    p.xlabel('stellar mass')
    p.yscale('log')
    p.xlim(( 8, 12))
    p.ylim((1e-6, 0.1))
    p.legend(loc=3, frameon=False)
    p.grid()
    p.savefig(fig_out)
    p.clf()

mdex = 0.1
mbins = n.arange(-30, -10, mdex)
z_mins = n.arange(0.1, 0.7, 0.1)
z_maxs = n.arange(0.1, 0.7, 0.1) + 0.1
for z_min, z_max in zip( z_mins, z_maxs ):
    fig_out = os.path.join(fig_dir, 'K_galaxy_LF_' + str(HEALPIX_id).zfill(6) +'_'+str(n.round(z_min,1)) +'.png')
    volume = (( cosmo.comoving_volume(z_max) - cosmo.comoving_volume(z_min) ) * area_pixel * n.pi / 129600.).value
    z_sel = ( t_k['redshift_R'] > z_min ) & ( t_k['redshift_R'] < z_max )
    z_sel_C = ( t_k_C['redshift_R'] > z_min ) & ( t_k_C['redshift_R'] < z_max )
    z_mean = n.mean(t_k['redshift_R'][z_sel])

    z_cosmos = (COSMOS['photoz']>z_min) & (COSMOS['photoz']<z_max)
    volume_cosmos =  (( cosmo.comoving_volume(z_max) - cosmo.comoving_volume(z_min) ) * 2 * n.pi / 129600.).value
    MK_arr_cosmos = COSMOS['MK'][z_cosmos]

    z_kids = (KIDS['z_peak']>z_min) & (KIDS['z_peak']<z_max)
    volume_kids =  (( cosmo.comoving_volume(z_max) - cosmo.comoving_volume(z_min) ) * 180 * n.pi / 129600.).value
    MK_arr_kids = KIDS['Kmag_abs'][z_kids]

    p.figure(1, (6., 5.5))
    p.axes([0.15, 0.15, 0.8, 0.8])
    p.plot(Mk_m_5logh_z0 - 5 * n.log10(h), 10**log10_phi_h3_Mpcm3_magm1 * 0.7**3, lw=3 , label='Driver 2012, z=0') # +5*n.log10(h)
    #
    out = p.hist(MK_arr_cosmos, lw=2, weights=n.ones_like(MK_arr_cosmos) / mdex / volume_cosmos , bins = mbins, histtype = 'step', rasterized = True, label = 'COSMOS data', color='blue')
    out = p.hist(MK_arr_kids, lw=2, weights=n.ones_like(MK_arr_kids) / mdex / volume_kids , bins = mbins, histtype = 'step', rasterized = True, label = 'KIDS data', color='red')
    #
    out = p.hist(t_k['K_mag_abs'][z_sel], lw=2, weights=n.ones_like(t_k['K_mag_abs'][z_sel]) / mdex / volume , bins = mbins, histtype = 'step', rasterized = True, label = 'GAMA_KIDS', color='black')
    out = p.hist(t_k_C['K_mag_abs'][z_sel_C], lw=2, weights=n.ones_like(t_k_C['K_mag_abs'][z_sel_C]) / mdex / volume , bins = mbins, histtype = 'step', rasterized = True, label = 'COSMOS_LSDR9', color='grey')
    #
    p.title(r'$\bar{z}$=' + str(n.round(z_mean, 3)))
    p.ylabel(r'$\Phi/\; mag^{-1}\; Mpc^{-3}$')
    p.xlabel('Absolute magnitude K')
    p.yscale('log')
    p.xlim(( -19, -26 ))
    p.ylim((1e-6, 0.005))
    p.legend(loc=3, frameon=False)
    p.grid()
    p.savefig(fig_out)
    p.clf()

sys.exit()

fig_out = os.path.join(fig_dir, 'I_galaxy_LF_' + str(HEALPIX_id).zfill(6) +'.png')

t_k = Table.read(path_2_out_K)
t_k_C = Table.read(path_2_out_K_C)

z_min = 0.05
z_max = 0.1
volume = (( cosmo.comoving_volume(z_max) - cosmo.comoving_volume(z_min) ) * area_pixel * n.pi / 129600.).value
z_sel = ( t_k['redshift_R'] > z_min ) & ( t_k['redshift_R'] < z_max )
z_sel_C = ( t_k_C['redshift_R'] > z_min ) & ( t_k_C['redshift_R'] < z_max )
z_mean = n.mean(t_k['redshift_R'][z_sel])

mdex = 0.1
mbins = n.arange(-30, -10, mdex)
p.figure(1, (6., 5.5))
p.axes([0.15, 0.15, 0.8, 0.8])

out = p.hist(t_k['I_mag_abs'][z_sel], lw=2, weights=n.ones_like(t_k['I_mag_abs'][z_sel]) / mdex / volume , bins = mbins, histtype = 'step', rasterized = True, label = 'GAMA_KIDS', color='black')
out = p.hist(t_k_C['I_mag_abs'][z_sel_C], lw=2, weights=n.ones_like(t_k_C['I_mag_abs'][z_sel_C]) / mdex / volume , bins = mbins, histtype = 'step', rasterized = True, label = 'COSMOS_LSDR9', color='grey')
p.plot(Mk_m_5logh_z0, 10**log10_phi_h3_Mpcm3_magm1 * h**3 , label='Driver 12') # +5*n.log10(h)

p.title(r'$\bar{z}$=' + str(n.round(z_mean, 3)))
p.ylabel(r'$\Phi=N_{gal} mag^{-1} Mpc^{-3}$')
p.xlabel('Absolute magnitude I')
p.yscale('log')
p.xlim(( -19, -26 ))
p.ylim((1e-6, 0.20))
p.legend(loc=4, frameon=False)
p.grid()
p.savefig(fig_out)
p.clf()







KCORR_DATA = n.loadtxt( os.path.join( os.environ['GIT_AGN_MOCK'], 'data/K-corr/VISTA_Ks_kcorrections.txt'), unpack = True)
# redshift distmod bandpass kcorr_median kcorr_16pc kcorr_84pc
#bandpass_itp = interp1d(KCORR_DATA[0], KCORR_DATA[2])
kcorr_itp = interp1d(KCORR_DATA[0], KCORR_DATA[3])
#kmag = absmag + distmod + bandpass + kcorrection
# absmag = appmag - distmod - kcorr  
# is properly zero-centred; or at least zero-ish; the median +/- NMAD is 0.05 +/- 0.12.

dm_values = dm_itp(t_kids['z_peak'].data.data)
#kmag_abs = kmag - ( dm_values + bandpass_itp(t_kids['z_peak']) + kcorr_itp(t_kids['z_peak']) )
kmag_abs = kmag - ( dm_values + kcorr_itp(t_kids['z_peak'].data.data) )

#kmag_abs = 8.7 - 2.5*n.log10(t_kids['flux_Kt']) - dm_values
t_kids['Kmag_abs'] = kmag_abs
t_kids['dist_mod'] = dm_values




KCORR_DATA = n.loadtxt( os.path.join( os.environ['GIT_AGN_MOCK'], 'data/K-corr/KiDS_i_kcorrections.txt'), unpack = True)
# redshift distmod bandpass kcorr_median kcorr_16pc kcorr_84pc
#bandpass_itp = interp1d(KCORR_DATA[0], KCORR_DATA[2])
kcorr_itp_i = interp1d(KCORR_DATA[0], KCORR_DATA[3])
#kmag = absmag + distmod + bandpass + kcorrection
# absmag = appmag - distmod - kcorr
# is properly zero-centred; or at least zero-ish; the median +/- NMAD is 0.05 +/- 0.12.
imag_abs = imag - ( dm_values + kcorr_itp_i(t_kids['z_peak'].data.data) )

t_kids['Imag_abs'] = imag_abs

#t_kids.write(os.path.join(os.environ['HOME'], 'data', 'UNIT_fA1i', 'G09.GAMADR4+LegacyDR9.galreference+RM_Kabs_Iabs.fits'), overwrite = True)
#t_kids.write(os.path.join(os.environ['UNIT_fA1i_DIR'], 'G09.GAMADR4+LegacyDR9.galreference+RM_Kabs_Iabs.fits'), overwrite = True)
t_kids = t_kids[ ( t_kids['Kmag_abs'] < -20 ) ]


# rescale variables
min_Z = 1.5 # n.min(t_kids['z_peak'])
max_Z = 0.0 # n.max(t_kids['z_peak'])
min_K = -26. #n.min(t_kids['Kmag_abs'])
max_K = -20. #n.max(t_kids['Kmag_abs'])

z_01 = (t_kids['z_peak'] - min_Z ) / ( max_Z - min_Z )

k_01 = (t_kids['Kmag_abs'] - min_K ) / ( max_K - min_K )
Tree_kids = BallTree(n.transpose([z_01, k_01]))

i_01 = (t_kids['Imag_abs'] - min_K ) / ( max_K - min_K )
Tree_kids_I = BallTree(n.transpose([z_01, i_01]))
print(len(t_kids),'lines in KIDS catalogue')

sub_survey_names = n.array([ 'BG', 'LRG', 'ELG', 'QSO', 'LyA', 'filament_GAL'])

N_subsurvey = {'BG':1, 'filament_GAL':3, 'LRG':2, 'ELG':3, 'QSO':4, 'LyA':5}
priority_values = {'BG':100, 'filament_GAL':80, 'LRG':99, 'ELG':80, 'QSO':97, 'LyA':98}

# reassigns templates correctly
z_all = n.hstack(( 0., n.arange(0.3, 3., 0.2), 3.5, 4.5, 6. ))
zmins = z_all[:-1]
zmaxs = z_all[1:]



def add_kmag(t_gal):
	#HEALPIX_id=359
	# catalogs
	z0, z1, slope, OO, zpt, scatter = n.loadtxt(os.path.join( os.environ['GIT_AGN_MOCK'], 'data', 'logMs-MK-fits', 'look-up-table-2parameters.txt'), unpack=True)
	fun = lambda x, a, b : a * x + b
	for zmin, zmax, slope_i, OO_i, zpt_i, scatter_i in zip(z0, z1, slope, OO, zpt, scatter):
		#print(zmin, zmax, p_b)
		s_gal  = (t_gal['redshift_R']>=zmin)  & (t_gal['redshift_R']<=zmax) 
		mag = lambda x : fun( x, slope_i, OO_i)
		if len(t_gal ['K_mag_abs'][s_gal])>0:
			t_gal ['K_mag_abs'][s_gal]  = mag(t_gal ['galaxy_SMHMR_mass'][s_gal])
			t_gal ['K_mag_abs'][s_gal]+=norm.rvs(loc=0, scale=scatter_i, size=len(t_gal['redshift_R'][s_gal]))
	return t_gal 

def add_imag(t_gal):
	#HEALPIX_id=359
	# catalogs
	z0, z1, slope, OO, zpt, scatter = n.loadtxt(os.path.join( os.environ['GIT_AGN_MOCK'], 'data', 'logMs-MI-fits', 'look-up-table-2parameters.txt'), unpack=True)
	fun = lambda x, a, b : a * x + b
	for zmin, zmax, slope_i, OO_i, zpt_i, scatter_i in zip(z0, z1, slope, OO, zpt, scatter):
		#print(zmin, zmax, p_b)
		s_gal  = (t_gal['redshift_R']>=zmin)  & (t_gal['redshift_R']<=zmax) 
		mag = lambda x : fun( x, slope_i, OO_i)
		if len(t_gal ['I_mag_abs'][s_gal])>0:
			t_gal ['I_mag_abs'][s_gal]  = mag(t_gal ['galaxy_SMHMR_mass'][s_gal])
			t_gal ['I_mag_abs'][s_gal]+=norm.rvs(loc=0, scale=scatter_i, size=len(t_gal['redshift_R'][s_gal]))
	return t_gal 

def add_magnitudes_direct_match_K(t_survey, t_kids2=t_kids, Tree_kids=Tree_kids):
	# simulated quantities
	sim_redshift = t_survey['redshift_R']
	sim_k_mag    = t_survey['K_mag_abs'] # +dm_itp(t_survey['redshift_R'])
	# rescale variables
	SIMz_01      = ( sim_redshift - min_Z ) / ( max_Z - min_Z )
	SIMk_01      = ( sim_k_mag - min_K ) / ( max_K - min_K )
	DATA = n.transpose([SIMz_01, SIMk_01])
	# search nearest neighbour in tree
	dist_out, ids_out = Tree_kids.query(DATA, k=1, return_distance = True)
	kids_ID = n.arange(len(t_kids2))
	ids = n.hstack((ids_out))
	dist_map = n.hstack(( dist_out ))
	id_to_map = kids_ID[ids]
	columns_to_add = n.array(['CATAID', 'z_peak', 'Kmag_abs', 'Imag_abs', 'dist_mod', 'flux_FUVt',
								#'flux_err_FUVt',
								'flux_NUVt',
								#'flux_err_NUVt',
								'flux_ut',
								#'flux_err_ut',
								'flux_gt',
								#'flux_err_gt',
								'flux_rt',
								#'flux_err_rt',
								'flux_it',
								#'flux_err_it',
								'flux_Zt',
								#'flux_err_Zt',
								'flux_Yt',
								#'flux_err_Yt',
								'flux_Jt',
								#'flux_err_Jt',
								'flux_Ht',
								#'flux_err_Ht',
								'flux_Kt',
								#'flux_err_Kt',
								'flux_W1t',
								#'flux_err_W1t',
								'flux_W2t',
								#'flux_err_W2t'
								 'FIBERFLUX_R'
								, 'SHAPE_R'
								, 'SHAPE_E1'
								, 'SHAPE_E2'
								, 'SERSIC'
								, 'MASKBITS'])
								#, 'RM_p_mem', 'RM_cg'])
	print('outputing results')
	for el in columns_to_add :
		t_survey[el] = t_kids2[el][id_to_map]
	t_survey['distance_match'] = dist_map
	return t_survey #.write(path_2_out)


def add_magnitudes_direct_match_I(t_survey, t_kids2=t_kids, Tree_kids=Tree_kids_I):
	# simulated quantities
	sim_redshift = t_survey['redshift_R']
	sim_k_mag    = t_survey['I_mag_abs'] # +dm_itp(t_survey['redshift_R'])
	# rescale variables
	SIMz_01      = ( sim_redshift - min_Z ) / ( max_Z - min_Z )
	SIMk_01      = ( sim_k_mag - min_K ) / ( max_K - min_K )
	DATA = n.transpose([SIMz_01, SIMk_01])
	# search nearest neighbour in tree
	dist_out, ids_out = Tree_kids.query(DATA, k=1, return_distance = True)
	kids_ID = n.arange(len(t_kids2))
	ids = n.hstack((ids_out))
	dist_map = n.hstack(( dist_out ))
	id_to_map = kids_ID[ids]
	columns_to_add = n.array(['CATAID', 'z_peak', 'Kmag_abs', 'Imag_abs', 'dist_mod', 'flux_FUVt',
								#'flux_err_FUVt',
								'flux_NUVt',
								#'flux_err_NUVt',
								'flux_ut',
								#'flux_err_ut',
								'flux_gt',
								#'flux_err_gt',
								'flux_rt',
								#'flux_err_rt',
								'flux_it',
								#'flux_err_it',
								'flux_Zt',
								#'flux_err_Zt',
								'flux_Yt',
								#'flux_err_Yt',
								'flux_Jt',
								#'flux_err_Jt',
								'flux_Ht',
								#'flux_err_Ht',
								'flux_Kt',
								#'flux_err_Kt',
								'flux_W1t',
								#'flux_err_W1t',
								'flux_W2t',
								#'flux_err_W2t'
								 'FIBERFLUX_R'
								, 'SHAPE_R'
								, 'SHAPE_E1'
								, 'SHAPE_E2'
								, 'SERSIC'
								, 'MASKBITS'])
								#, 'RM_p_mem', 'RM_cg'])
	print('outputing results')
	for el in columns_to_add :
		t_survey[el] = t_kids2[el][id_to_map]
	t_survey['distance_match'] = dist_map
	return t_survey #.write(path_2_out)

t0 = time.time()
N_pixels = healpy.nside2npix(8)
for HEALPIX_id in n.arange(N_pixels)[::-1][:10]: #n.arange(N_pixels)[::-1]:
    print(HEALPIX_id, time.time()-t0)
    path_2_out_K = os.path.join(dir_2_OUT, 'Kmatch_'  + str(HEALPIX_id).zfill(6) + '.fit')
    path_2_out_I = os.path.join(dir_2_OUT, 'Imatch_'  + str(HEALPIX_id).zfill(6) + '.fit')
    path_2_in = os.path.join(dir_2_IN, str(HEALPIX_id).zfill(6) + '.fit')
    print(path_2_in)
    t_survey = Table.read(path_2_in)
    initial_ids = n.arange(len(t_survey))
    t_survey['line_id_cat_GALAXY_all'] = initial_ids
    t_in = t_survey[(t_survey['redshift_R']<1.5) & (t_survey['redshift_R']>0.01) & (t_survey['galaxy_SMHMR_mass'] > 5)]
    print('adding k mag abs')
    t_in = add_kmag(t_in)
    print('adding i mag abs')
    t_in['I_mag_abs'] = n.zeros(len(t_in['K_mag_abs']))
    t_in = add_imag(t_in)
    print(len(t_in))
    t_out = add_magnitudes_direct_match_K(t_survey = t_in, t_kids2 = t_kids, Tree_kids=Tree_kids)
    t_out.write(path_2_out_K, overwrite = True)
    t_out = add_magnitudes_direct_match_I(t_survey = t_in, t_kids2 = t_kids, Tree_kids=Tree_kids_I)
    t_out.write(path_2_out_I, overwrite = True)
    print(path_2_out_K, path_2_out_I, 'written')


