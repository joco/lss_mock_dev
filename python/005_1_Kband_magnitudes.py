"""
What it does
------------

Add K band luminosity to BG, LRG and ELG

Command to run
--------------

python3 005_1_Kband_magnitudes.py environmentVAR 

environmentVAR: environment variable linking to the directory where files are e.g. MD10
It will then work in the directory : $environmentVAR/hlists/fits/

Dependencies
------------
topcat/stilts
import time, os, sys, numpy, scipy, astropy, h5py, astropy_healpix, matplotlib

"""

import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p

from astropy_healpix import healpy
import sys
import os
import time
from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column
from scipy.optimize import curve_fit
#import linmix
import astropy.io.fits as fits
import h5py
import numpy as n
print('CREATES FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()
#import astropy.io.fits as fits
# import all pathes


from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import extinction
cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
h = 0.6774
L_box = 1000.0 / h
cosmo = cosmoUNIT

nl = lambda sel : len(sel.nonzero()[0])
zs = n.arange(0.0000001, 7.1, 0.001)
dm_itp = interp1d(zs, cosmo.distmod(zs).value)

#env = sys.argv[1]  # 'MD04'
#print(env)

#root_dir = os.path.join(os.environ[env])
plotDir = os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'logMs-MK-fits')
#dir_2_OUT = os.path.join(root_dir, "cat_SHAM_COSMO")

z_str = 'z0p05'

path_2_AGN_catalog = os.path.join( os.environ['UCHUU'], 'GPX8', 'agn_uchuu_'+z_str+'.fits' )
path_2_GAL_catalog = os.path.join( os.environ['UCHUU'], 'GPX8', 'uchuu_'+z_str+'.fits' )
path_2_MAG_catalog = os.path.join( os.environ['UCHUU'], 'GPX8', 'gal_mag_uchuu_'+z_str+'.fits' )

print(path_2_AGN_catalog, path_2_GAL_catalog, path_2_MAG_catalog, time.time()-t0)
hd = fits.open(path_2_AGN_catalog)
hg = fits.open(path_2_GAL_catalog)
hm = fits.open(path_2_MAG_catalog)
uchuu_logm = n.log10(hg[1].data['sm'])
uchuu_kmagABS = hm[1].data['Kmag_abs']

#def add_kmag(t_gal):
	#t_gal ['K_mag_abs'] = -99.
	#z0, z1, slope, OO, zpt, scatter = n.loadtxt(os.path.join( os.environ['GIT_AGN_MOCK'], 'data', 'logMs-MK-fits', 'look-up-table-2parameters.txt'), unpack=True)
	#fun = lambda x, a, b : a * x + b
	#for zmin, zmax, slope_i, OO_i, zpt_i, scatter_i in zip(z0, z1, slope, OO, zpt, scatter):
		##print(zmin, zmax, p_b)
		#scatter_i = 0.15
		#s_gal  = (t_gal['redshift_R']>=zmin)  & (t_gal['redshift_R']<=zmax)
		#mag = lambda x : fun( x, slope_i, OO_i)
		#if len(t_gal ['K_mag_abs'][s_gal])>0:
			#t_gal ['K_mag_abs'][s_gal]  = mag(n.log10(t_gal ['sm'][s_gal]))
			#t_gal ['K_mag_abs'][s_gal]+=norm.rvs(loc=0, scale=scatter_i, size=len(t_gal['redshift_R'][s_gal]))
	#return t_gal

#t_survey = Table.read(path_2_GAL_catalog)
#t_in = add_kmag(t_survey)
#uchuu_kmagABS = t_in['K_mag_abs']

# first, fits the K-band stellr mass relation as a function of redshift
path_2_COSMOS = os.path.join(os.environ['HOME'],'sf_Shared/data/COSMOS','photoz_vers2.0_010312.fits')
t = Table.read(path_2_COSMOS)
good = (t['photoz']>0. )&( t['photoz']< 2. ) & ( t['MK']<0 )&( t['MK']>-40 )&( t['mass_med']<14 )&( t['mass_med']>6 )

path_2_kids = os.path.join(os.environ['HOME'], 'sf_Shared/data/GAMA/', 'forJohan.fits')
t_kids = Table.read(path_2_kids)
kmag = 8.9 - 2.5*n.log10(t_kids['flux_Kt'])
zmag = 8.9 - 2.5*n.log10(t_kids['flux_Zt'])
rmag = 8.9 - 2.5*n.log10(t_kids['flux_rt'])
imag = 8.9 - 2.5*n.log10(t_kids['flux_it'])
keep_kids = (kmag>0) & (t_kids['Z']>0.01) & (t_kids['Z']<1.99) & (zmag>0) & (rmag>0)  & (imag>0)
t_kids = Table(t_kids[keep_kids])
kmag = 8.9 - 2.5*n.log10(t_kids['flux_Kt'])
zmag = 8.9 - 2.5*n.log10(t_kids['flux_Zt'])
rmag = 8.9 - 2.5*n.log10(t_kids['flux_rt'])
imag = 8.9 - 2.5*n.log10(t_kids['flux_it'])

KCORR_DATA = n.loadtxt( os.path.join( os.environ['GIT_AGN_MOCK'], 'data/K-corr/VISTA_Ks_kcorrections.txt'), unpack = True)
# redshift distmod bandpass kcorr_median kcorr_16pc kcorr_84pc
#bandpass_itp = interp1d(KCORR_DATA[0], KCORR_DATA[2])
kcorr_itp = interp1d(KCORR_DATA[0], KCORR_DATA[3])
#kmag = absmag + distmod + bandpass + kcorrection
# absmag = appmag - distmod - kcorr  
# is properly zero-centred; or at least zero-ish; the median +/- NMAD is 0.05 +/- 0.12.

dm_values = dm_itp(t_kids['Z'].data.data)
#kmag_abs = kmag - ( dm_values + bandpass_itp(t_kids['z_peak']) + kcorr_itp(t_kids['z_peak']) )
kmag_abs = kmag - ( dm_values + kcorr_itp(t_kids['Z'].data.data) )

#kmag_abs = 8.7 - 2.5*n.log10(t_kids['flux_Kt']) - dm_values
t_kids['Kmag_abs'] = n.array(kmag_abs)
t_kids['dist_mod'] = dm_values
t_kids['ms'] = n.array(n.log10( t_kids['StellarMass_50'] ))



dz = 0.1
zall = n.arange(0., 2.+dz, dz)
params, covar = [], []
params2, covar2 = [], []
params_lm = []
resid, resid2 = [], []
for zmin, zmax in zip(zall[:-1], zall[1:]):
	s00el = (good) & (t['photoz']>zmin) & (t['photoz']<=zmax)
	s00el2 = (t_kids['Z']>zmin) & (t_kids['Z']<=zmax)
	sel = (good) & (t['photoz']>zmin) & (t['photoz']<=zmax) &( t['mass_med']>9.5)#8+zmin/2 )
	sel2 = (t_kids['Z']>zmin) & (t_kids['Z']<=zmax) &( t_kids['ms']>9.5)#8+zmin/2 )
	fun2 = lambda x, a, b : a*x+b
	# fixing the slope to avoid jumps with redshift correlated to the second parameters
	fun = lambda x, b : -2.15 * x + b
	x_data = n.hstack(( t['mass_med'][sel], t_kids['ms'][sel2]))
	y_data = n.hstack(( t['MK'][sel]      , t_kids['Kmag_abs'][sel2]))
	popt, pcov= curve_fit(fun, x_data, y_data, p0=(0.))#, sigma=y_err)
	popt2, pcov2= curve_fit(fun2, x_data, y_data, p0=(-2.15, 0.))#, sigma=y_err)
	params.append(popt)
	covar.append(pcov)
	params2.append(popt2)
	covar2.append(pcov2)
	#n.random.seed(2)
	#lm = linmix.LinMix(x_data, y_data, K=2)
	#lm.run_mcmc(miniter=200, maxiter=600)
	#params_lm.append([n.median(lm.chain['beta']), n.median(lm.chain['alpha'])])
	#print(n.round(zmin,1),n.round(zmax,1),'curve_fit',n.round(popt,2), n.round(pcov[0][0],4), n.round(pcov[1][1],4))#,'linmix',params_lm[-1])# = 0.05
	print(n.round(zmin,1),n.round(zmax,1),'curve_fit',n.round(popt,2), n.round(pcov[0][0]**0.5,4))
	print(n.round(zmin,1),n.round(zmax,1),'curve_fit',n.round(popt2[1],2),n.round(popt2[0],2), n.round(pcov2[0][0]**0.5,4), n.round(pcov2[1][1]**0.5,4))
	# figure
	xs = n.arange(n.min(x_data), n.max(x_data), (n.max(x_data)-n.min(x_data))/20.)
	p.figure(figsize=(6,6))
	p.plot(uchuu_logm, uchuu_kmagABS, 'r,', rasterized = True)

	p.plot(t['mass_med'][s00el], t['MK'][s00el], 'k,')#, alpha=0.5)#, label='COSMOS, N='+str(len(t['mass_med'][sel])))
	p.plot(t_kids['ms'][s00el2], t_kids['Kmag_abs'][s00el2], 'g,')#, label='GAMA, N='+str(len(t_kids['ms'][sel2])))
	#
	p.plot(t['mass_med'][sel], t['MK'][sel], 'k+', alpha=0.5, label='COSMOS, N='+str(len(t['mass_med'][sel])))
	p.plot(t_kids['ms'][sel2], t_kids['Kmag_abs'][sel2], 'gx', alpha=0.5, label='GAMA, N='+str(len(t_kids['ms'][sel2])))
	#p.plot(x_data, y_data, 'g+', alpha=0.5, label='COSMOS+GAMA, N='+str(len(x_data)))
	#p.errorbar(x_data, y_data, xerr=x_err, yerr=y_err, ls=' ', alpha=0.5)
	#for i in range(0, len(lm.chain), 25):
		#
		#ys = lm.chain[i]['alpha'] + xs * lm.chain[i]['beta']
		#p.plot(xs, ys, color='r', alpha=0.02)
	#ys = n.median(lm.chain['alpha']) + xs * n.median(lm.chain['beta'])
	#p.plot(xs, ys, color='k', label=r'$MK=\log_{10}(M_s)\times$'+str(n.round(n.median(lm.chain['beta']),2)) +'+'+str(n.round(n.median(lm.chain['alpha']),2)))
	#p.plot(xs, fun(xs, popt[0], popt[1]), 'b--', label=r'$MK=\log_{10}(M_s)\times$'+str(n.round(popt[0],2)) +'+'+str(n.round(popt[1],2)))
	xs2 = n.arange(7.5, 12.5, 0.01)
	p.plot(xs2, fun(xs2, popt[0]), 'b--', lw=3, label=r'$MK=-2.15\log_{10}(M_s)$'+'+'+str(n.round(popt[0],2)))
	p.plot(xs2, fun2(xs2, popt2[0], popt2[1]), 'r--', lw=2, label=r'$MK=$'+str(n.round(popt2[0],2))+r'$\log_{10}(M_s)$'+'+'+str(n.round(popt2[1],2)))
	p.xlabel(r'$\log_{10}(M_s/M_\odot)$')
	p.ylabel(r'MK')
	p.legend(frameon=False, loc=3)
	p.ylim((-26,-15))
	p.xlim((7.5, 12.5))
	p.grid()
	p.title(str(n.round(zmin,2))+r'$<z<$'+str(n.round(zmax,2)) )#+ r", $F_X>1\times10^{-17}$ ")
	p.savefig(os.path.join( plotDir, "bayesian_mass_Kmag_"+str(n.round(zmin,2))+".png") )
	p.clf()
	# TREATMENT OF THE RESIDUALS
	# data
	dy_B = y_data - fun(x_data, popt[0])
	dy_C = y_data - fun2(x_data, popt2[0], popt2[1])
	#y_model_B = n.median(lm.chain['alpha']) + x_data * n.median(lm.chain['beta'])
	#dy_B = y_data-y_model_B
	#fit
	funN = lambda x, loc, scale : norm.cdf(x, loc=loc, scale=scale) 
	# figure
	bins=n.arange(-1.5,1.5,0.05)
	xbins=bins[1:]-0.025
	p.figure(2, (6,6))
	n_t2 = p.hist(dy_B , bins=bins, density=True, cumulative=True, histtype='step', label='residuals 1p' )[0]
	pt, ct = curve_fit(funN, xbins, n_t2, p0=(0,0.4))
	p.plot(xbins, norm.cdf(xbins, loc=pt[0], scale=pt[1]), label='N1p('+str(n.round(pt[0],2))+', '+str(n.round(pt[1],2))+')', ls='dashed')
	# model 2 params
	n_t2 = p.hist(dy_C , bins=bins, density=True, cumulative=True, histtype='step', label='residuals 2p' )[0]
	pt2, ct2 = curve_fit(funN, xbins, n_t2, p0=(0,0.4))
	p.plot(xbins, norm.cdf(xbins, loc=pt2[0], scale=pt2[1]), label='N2p('+str(n.round(pt2[0],2))+', '+str(n.round(pt2[1],2))+')', ls='dashed')
	p.ylabel('counts')
	p.xlabel(r'delta mag')
	p.grid()
	p.legend(frameon=False, loc=0)
	p.title(str(n.round(zmin,2))+r'$<z<$'+str(n.round(zmax,2)))
	p.savefig(os.path.join( plotDir, "hist_residual_mass_Kmag_"+str(n.round(zmin,2))+".png"))
	p.clf()
	print('residuals', pt, pt2)
	resid.append( pt)# ,ct] )
	resid2.append( pt2)# ,ct2] )
	#return popt, pcov, n.median(lm.chain['beta']), n.median(lm.chain['alpha']), n.std(lm.chain['beta']), n.std(lm.chain['alpha']), residuals

#scatter = 0.5
#p_a, p_b = n.transpose(params)
#OUT = n.transpose([zall[:-1], zall[1:],  p_a, p_b])
p_a = n.transpose(params)
p_b = n.transpose(resid)
OUT = n.transpose([ zall[:-1], zall[1:],  p_a[0], p_b[0], p_b[1] ])
n.savetxt(os.path.join( plotDir, 'look-up-table_2023Apr6.txt'), OUT)

p_a = n.transpose(params2)
p_b = n.transpose(resid2)
OUT = n.transpose([zall[:-1], zall[1:],  p_a[0], p_a[1], p_b[0], p_b[1] ])
n.savetxt(os.path.join( plotDir, 'look-up-table-2parameters_2023Apr6.txt'), OUT)
sys.exit()

# Now assigns K band absolute magnitudes :
#z0,z1,a0,b0 = n.loadtxt(os.path.join( plotDir, 'look-up-table.txt'), unpack=True)
#fun = lambda x, a, b : a*x+b
# 1 param to have a smoother evolution with redshift
#z0,z1,b0 = n.loadtxt(os.path.join( plotDir, 'look-up-table_2023Apr6.txt'), unpack=True)
#fun = lambda x, b : -2.15*x+b

sys.exit()
#N_pixels = healpy.nside2npix(8)
#for HEALPIX_id in n.arange(N_pixels)[::-1][:340]:
def add_kmag(HEALPIX_id):
	#HEALPIX_id=359
	# catalogs
	path_2_BG    = os.path.join(dir_2_OUT, 'BG_'  + str(HEALPIX_id).zfill(6) + '.fit')
	path_2_BG_S5 = os.path.join(dir_2_OUT, 'S5GAL_'  + str(HEALPIX_id).zfill(6) + '.fit')
	path_2_LRG   = os.path.join(dir_2_OUT, 'LRG_' + str(HEALPIX_id).zfill(6) + '.fit')
	path_2_ELG   = os.path.join(dir_2_OUT, 'ELG_' + str(HEALPIX_id).zfill(6) + '.fit')

	t_bg = Table.read(path_2_BG)
	if 'K_mag_abs' not in t_bg.columns:
		t_bg.add_column(Column(name='K_mag_abs', data=n.zeros_like(t_bg['Mstar'].data), unit='mag'))

	t_bgS5 = Table.read(path_2_BG_S5)
	if 'K_mag_abs' not in t_bgS5.columns:
		t_bgS5.add_column(Column(name='K_mag_abs', data=n.zeros_like(t_bgS5['Mstar'].data), unit='mag'))

	t_lrg = Table.read(path_2_LRG)
	if 'K_mag_abs' not in t_lrg.columns:
		t_lrg.add_column(Column(name='K_mag_abs', data=n.zeros_like(t_lrg['Mstar'].data), unit='mag'))

	t_elg = Table.read(path_2_ELG)
	if 'K_mag_abs' not in t_elg.columns:
		t_elg.add_column(Column(name='K_mag_abs', data=n.zeros_like(t_elg['Mstar'].data), unit='mag'))

	#for zmin, zmax, p_a, p_b in zip(z0, z1, a0, b0):
	for zmin, zmax, p_b in zip(z0, z1, b0):
		#print(zmin, zmax, p_a, p_b)
		s_bg  = (t_bg['Z']>=zmin)  & (t_bg['Z']<=zmax)
		s_bgS5  = (t_bgS5['Z']>=zmin)  & (t_bgS5['Z']<=zmax)
		s_lrg = (t_lrg['Z']>=zmin) & (t_lrg['Z']<=zmax)
		s_elg = (t_elg['Z']>=zmin) & (t_elg['Z']<=zmax)
		#mag = lambda x : fun( x, p_a, p_b)
		mag = lambda x : fun( x, p_b)
		t_bg ['K_mag_abs'][s_bg]  = mag(t_bg ['Mstar'][s_bg])
		t_bgS5 ['K_mag_abs'][s_bgS5]  = mag(t_bgS5 ['Mstar'][s_bgS5])
		t_lrg['K_mag_abs'][s_lrg] = mag(t_lrg['Mstar'][s_lrg])
		t_elg['K_mag_abs'][s_elg] = mag(t_elg['Mstar'][s_elg])

	t_bg ['K_mag_abs']+=norm.rvs(loc=0, scale=0.15, size=len(t_bg['Z']))
	t_bgS5['K_mag_abs']+=norm.rvs(loc=0, scale=0.15, size=len(t_bgS5['Z']))
	t_lrg['K_mag_abs']+=norm.rvs(loc=0, scale=0.15, size=len(t_lrg['Z']))
	t_elg['K_mag_abs']+=norm.rvs(loc=0, scale=0.15, size=len(t_elg['Z']))

	t_bg.write  (path_2_BG   , overwrite=True)
	t_bgS5.write(path_2_BG_S5, overwrite=True)
	t_lrg.write (path_2_LRG  , overwrite=True)
	t_elg.write (path_2_ELG  , overwrite=True)


#N_pixels = healpy.nside2npix(8)
#for HEALPIX_id in n.arange(N_pixels)[::-1][:340]:
def add_kmag_bgs5_only(HEALPIX_id):
	#HEALPIX_id=359
	# catalogs
	path_2_BG_S5 = os.path.join(dir_2_OUT, 'S5GAL_'  + str(HEALPIX_id).zfill(6) + '.fit')

	t_bgS5 = Table.read(path_2_BG_S5)
	if 'K_mag_abs' not in t_bgS5.columns:
		t_bgS5.add_column(Column(name='K_mag_abs', data=n.zeros_like(t_bgS5['Mstar'].data), unit='mag'))

	#for zmin, zmax, p_a, p_b in zip(z0, z1, a0, b0):
	for zmin, zmax, p_b in zip(z0, z1, b0):
		#print(zmin, zmax, p_a, p_b)
		s_bgS5  = (t_bgS5['Z']>=zmin)  & (t_bgS5['Z']<=zmax)
		#mag = lambda x : fun( x, p_a, p_b)
		mag = lambda x : fun( x, p_b)
		t_bgS5 ['K_mag_abs'][s_bgS5]  = mag(t_bgS5 ['Mstar'][s_bgS5])

	t_bgS5['K_mag_abs']+=norm.rvs(loc=0, scale=0.15, size=len(t_bgS5['Z']))

	t_bgS5.write(path_2_BG_S5, overwrite=True)

def add_kmag_only_elg(HEALPIX_id):
	path_2_ELG   = os.path.join(dir_2_OUT, 'ELG_' + str(HEALPIX_id).zfill(6) + '.fit')
	t_elg = Table.read(path_2_ELG)
	if 'K_mag_abs' not in t_elg.columns:
		t_elg.add_column(Column(name='K_mag_abs', data=n.zeros_like(t_elg['Mstar'].data), unit='mag'))

	for zmin, zmax, p_b in zip(z0, z1, b0):
		s_elg = (t_elg['Z']>=zmin) & (t_elg['Z']<=zmax)
		mag = lambda x : fun( x, p_b)
		t_elg['K_mag_abs'][s_elg] = mag(t_elg['Mstar'][s_elg])

	t_elg['K_mag_abs']+=norm.rvs(loc=0, scale=0.15, size=len(t_elg['Z']))
	t_elg.write (path_2_ELG  , overwrite=True)

N_pixels = healpy.nside2npix(8)
for HEALPIX_id in n.arange(N_pixels):
	print(HEALPIX_id)
	try :
		add_kmag(HEALPIX_id)
		#add_kmag_only_elg(HEALPIX_id)
		#add_kmag_bgs5_only(HEALPIX_id)
	except(ValueError):
		print('already computed')
