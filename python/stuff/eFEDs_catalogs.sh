#!/bin/bash


stilts tpipe ifmt=fits in=/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_all_eRO_AGN.fit ofmt=fits out=/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_all_eRO_AGN_eFEDs.fit cmd='select "ra>125 && ra < 145 && dec>-3 && dec<6"' 

stilts tpipe ifmt=fits in=/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_sat_eRO_AGN.fit ofmt=fits out=/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_sat_eRO_AGN_eFEDs.fit cmd='select "ra>125 && ra < 145 && dec>-3 && dec<6"' 

stilts tpipe ifmt=fits in=/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001//hlists/fits/UNIT_fA1i_DIR_all_eRO_AGN.fit ofmt=fits out=/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001//hlists/fits/UNIT_fA1i_DIR_all_eRO_AGN_eFEDs.fit cmd='select "ra>125 && ra < 145 && dec>-3 && dec<6"' 

stilts tpipe ifmt=fits in=/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001//hlists/fits/UNIT_fA1i_DIR_sat_eRO_AGN.fit ofmt=fits out=/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_InvPhase_001//hlists/fits/UNIT_fA1i_DIR_sat_eRO_AGN_eFEDs.fit cmd='select "ra>125 && ra < 145 && dec>-3 && dec<6"' 

stilts tpipe ifmt=fits in=/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_002//hlists/fits/UNIT_fA2_DIR_all_eRO_AGN.fit ofmt=fits out=/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_002//hlists/fits/UNIT_fA2_DIR_all_eRO_AGN_eFEDs.fit cmd='select "ra>125 && ra < 145 && dec>-3 && dec<6"' 

stilts tpipe ifmt=fits in=/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_002//hlists/fits/UNIT_fA2_DIR_sat_eRO_AGN.fit ofmt=fits out=/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_002//hlists/fits/UNIT_fA2_DIR_sat_eRO_AGN_eFEDs.fit cmd='select "ra>125 && ra < 145 && dec>-3 && dec<6"' 


stilts tpipe ifmt=fits in=/data17s/darksim/MD/MD_1.0Gpc/hlists/fits/MD10_all_eRO_AGN.fit ofmt=fits out=/data17s/darksim/MD/MD_1.0Gpc/hlists/fits/MD10_all_eRO_AGN_eFEDs.fit cmd='select "ra>125 && ra < 145 && dec>-3 && dec<6"'

stilts tpipe ifmt=fits in=/data17s/darksim/MD/MD_1.0Gpc/hlists/fits/MD10_sat_eRO_AGN.fit ofmt=fits out=/data17s/darksim/MD/MD_1.0Gpc/hlists/fits/MD10_sat_eRO_AGN_eFEDs.fit cmd='select "ra>125 && ra < 145 && dec>-3 && dec<6"'

stilts tpipe ifmt=fits in=/data17s/darksim/MD/MD_0.4Gpc/hlists/fits/MD04_all_eRO_AGN.fit ofmt=fits out=/data17s/darksim/MD/MD_0.4Gpc/hlists/fits/MD04_all_eRO_AGN_eFEDs.fit cmd='select "ra>125 && ra < 145 && dec>-3 && dec<6"'

stilts tpipe ifmt=fits in=/data17s/darksim/MD/MD_0.4Gpc/hlists/fits/MD04_sat_eRO_AGN.fit ofmt=fits out=/data17s/darksim/MD/MD_0.4Gpc/hlists/fits/MD04_sat_eRO_AGN_eFEDs.fit cmd='select "ra>125 && ra < 145 && dec>-3 && dec<6"'

ll /data17s/darksim/MD/MD_1.0Gpc/hlists/fits/MD10_all_eRO_AGN_eFEDs.fit
ll /data17s/darksim/MD/MD_1.0Gpc/hlists/fits/MD10_sat_eRO_AGN_eFEDs.fit
ll /data17s/darksim/MD/MD_0.4Gpc/hlists/fits/MD04_all_eRO_AGN_eFEDs.fit
ll /data17s/darksim/MD/MD_0.4Gpc/hlists/fits/MD04_sat_eRO_AGN_eFEDs.fit
ll /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_all_eRO_AGN_eFEDs.fit 
ll /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_sat_eRO_AGN_eFEDs.fit 
ll /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_all_eRO_AGN_eFEDs.fit 
ll /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_sat_eRO_AGN_eFEDs.fit 
ll /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_002//hlists/fits/UNIT_fA2_DIR_all_eRO_AGN_eFEDs.fit
ll /data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_002//hlists/fits/UNIT_fA2_DIR_sat_eRO_AGN_eFEDs.fit

tar -czf ~/data2/firefly/new_mocks/eFEDs_AGN.tar.gz /data17s/darksim/MD/MD_1.0Gpc/hlists/fits/MD10_all_eRO_AGN_eFEDs.fit \
/data17s/darksim/MD/MD_1.0Gpc/hlists/fits/MD10_sat_eRO_AGN_eFEDs.fit \
/data17s/darksim/MD/MD_0.4Gpc/hlists/fits/MD04_all_eRO_AGN_eFEDs.fit \
/data17s/darksim/MD/MD_0.4Gpc/hlists/fits/MD04_sat_eRO_AGN_eFEDs.fit \
/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_all_eRO_AGN_eFEDs.fit \
/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_sat_eRO_AGN_eFEDs.fit \
/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_all_eRO_AGN_eFEDs.fit \
/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_sat_eRO_AGN_eFEDs.fit \
/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_002//hlists/fits/UNIT_fA2_DIR_all_eRO_AGN_eFEDs.fit \
/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_002//hlists/fits/UNIT_fA2_DIR_sat_eRO_AGN_eFEDs.fit

scp -r ds52:/data17s/darksim/MD/MD_1.0Gpc/hlists/fits/MD10_all_eRO_AGN_eFEDs.fit .
scp -r ds52:/data17s/darksim/MD/MD_1.0Gpc/hlists/fits/MD10_sat_eRO_AGN_eFEDs.fit .
scp -r ds52:/data17s/darksim/MD/MD_0.4Gpc/hlists/fits/MD04_all_eRO_AGN_eFEDs.fit .
scp -r ds52:/data17s/darksim/MD/MD_0.4Gpc/hlists/fits/MD04_sat_eRO_AGN_eFEDs.fit .
scp -r ds52:/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_all_eRO_AGN_eFEDs.fit .
scp -r ds52:/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_sat_eRO_AGN_eFEDs.fit .
scp -r ds52:/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_all_eRO_AGN_eFEDs.fit .
scp -r ds52:/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_001//hlists/fits/UNIT_fA1_DIR_sat_eRO_AGN_eFEDs.fit .
scp -r ds52:/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_002//hlists/fits/UNIT_fA2_DIR_all_eRO_AGN_eFEDs.fit .
scp -r ds52:/data47s/comparat/simulations/UNIT/ROCKSTAR_HALOS/fixedAmp_002//hlists/fits/UNIT_fA2_DIR_sat_eRO_AGN_eFEDs.fit .




