"""
Creates an observed cluster fits file

CLU_FX_soft_attenuated

clu = ( M500c > 5e13 )


"""
from astropy_healpix import healpy
import sys
import os
import time
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
from scipy.interpolate import interp1d
import astropy.io.fits as fits
import h5py
import numpy as n
print('CREATES CLUSTER FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()
#import astropy.io.fits as fits
# import all pathes

env = sys.argv[1]  # 'MD04'
baseName = sys.argv[2]  # "sat_0.62840"
print(env, baseName)

test_dir = os.path.join(os.environ[env], 'hlists', 'fits')

path_2_light_cone = os.path.join(test_dir, baseName + '.fits')
path_2_coordinate_file = os.path.join(test_dir, baseName + '_coordinates.h5')
path_2_galaxy_file = os.path.join(test_dir, baseName + '_galaxy.h5')
path_2_agn_file = os.path.join(test_dir, baseName + '_agn.h5')
path_2_CLU_catalog = os.path.join(test_dir, baseName + '_eRO_CLU.fit')
path_2_M500c_catalog = os.path.join(test_dir, baseName + '_m500c.fit')

path_2_XPAINTING_sbhalo = os.path.join(
    test_dir, baseName + "_m500c_sbhalo.txt")
path_2_XPAINTING_sbprof = os.path.join(
    test_dir, baseName + "_m500c_sbprof.txt")

# x ray extinction for clusters, from A. Finoguenov
path_2_NH_law = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    "data",
    "xray_k_correction",
    'nh_flux.tbl')
NH_DATA = n.loadtxt(path_2_NH_law, unpack=True)
nh_law = interp1d(
    n.hstack(
        (-1.e30,
         1.0 * 10**10,
         NH_DATA[0],
         1.0 * 10**30)),
    n.hstack(
            (1.,
             1.,
             NH_DATA[1][0] / NH_DATA[1],
             7.03612982e-09)))

# eRosita flux limits for point sources
path_2_flux_limits = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    "data",
    "erosita",
    "flux_limits.fits")

# simulation setup
if env == "MD10" or env == "MD04":
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

f3 = h5py.File(path_2_agn_file, 'r')
FX_hard = f3["AGN/FX_hard"].value
FX_soft = f3["AGN/FX_soft"].value
FX_soft_attenuated = f3["AGN/FX_soft_attenuated"].value
LX_hard = f3["AGN/LX_hard"].value
LX_soft = f3["AGN/LX_soft"].value
SDSS_r_AB = f3["AGN/SDSS_r_AB"].value
SDSS_r_AB_attenuated = f3["AGN/SDSS_r_AB_attenuated"].value
agn_type = f3["AGN/agn_type"].value
ids_active = f3["AGN/ids_active"].value
logNH = f3["AGN/logNH"].value
random = f3["AGN/random"].value
f3.close()
print('agn file opened', time.time() - t0)

M500c_cat = fits.open(path_2_M500c_catalog)[1].data
print('M500c file opened', time.time() - t0)


f1 = fits.open(path_2_light_cone)
if f1[1].columns.names[0] == 'id':
    Mvir = f1[1].data['Mvir'] / h
    M500c = f1[1].data['M500c'] / h
    halo_id = f1[1].data['id']
    if os.path.basename(path_2_light_cone)[:3] == 'sat':
        halo_host_id = f1[1].data['pid']
    else:
        halo_host_id = -n.ones_like(halo_id).astype('int')

else:
    if os.path.basename(path_2_light_cone)[:3] == 'sat':
        hh = 'id pid Mvir Rvir rs scale_of_last_MM vmax x y z vx vy vz M200c M500c b_to_a_500c c_to_a_500c Acc_Rate_1_Tdyn'
        header = hh.split()
        dc_col = {}
        for ii in range(len(header)):
            dc_col[header[ii]] = 'col' + str(ii + 1)

    if os.path.basename(path_2_light_cone)[:3] == 'all':
        hh = 'id Mvir Rvir rs scale_of_last_MM vmax x y z vx vy vz M200c M500c b_to_a_500c c_to_a_500c Acc_Rate_1_Tdyn'
        header = hh.split()
        dc_col = {}
        for ii in range(len(header)):
            dc_col[header[ii]] = 'col' + str(ii + 1)

    Mvir = f1[1].data[dc_col['Mvir']] / h
    M500c = f1[1].data[dc_col['M500c']] / h
    halo_id = f1[1].data[dc_col['id']]
    if os.path.basename(path_2_light_cone)[:3] == 'sat':
        halo_host_id = f1[1].data[dc_col['pid']]
    else:
        halo_host_id = -n.ones_like(halo_id).astype('int')
N_galaxies = len(Mvir)
f1.close()

clu = (M500c > 5e13)
ids_clu = n.arange(N_galaxies)[clu]
# now retrieve the AGNs in the clusters

ids_active_in_clu = n.intersect1d(ids_clu, ids_active)
sel_AGN = n.in1d(ids_active, ids_active_in_clu)

arr_0 = n.zeros_like(M500c)

all_ids = n.arange(len(arr_0))
sel_AGN_left = n.in1d(all_ids, ids_active_in_clu)


inter_FX_hard = n.zeros_like(M500c)
inter_FX_soft = n.zeros_like(M500c)
inter_FX_soft_attenuated = n.zeros_like(M500c)
inter_LX_hard = n.zeros_like(M500c)
inter_LX_soft = n.zeros_like(M500c)
inter_SDSS_r_AB = n.zeros_like(M500c)
inter_SDSS_r_AB_attenuated = n.zeros_like(M500c)
inter_agn_type = n.zeros_like(M500c)
inter_ids_active = n.zeros_like(M500c)
inter_logNH = n.zeros_like(M500c)
inter_random = n.zeros_like(M500c)

inter_2_FX_hard = FX_hard[sel_AGN]
inter_2_FX_soft = FX_soft[sel_AGN]
inter_2_FX_soft_attenuated = FX_soft_attenuated[sel_AGN]
inter_2_LX_hard = LX_hard[sel_AGN]
inter_2_LX_soft = LX_soft[sel_AGN]
inter_2_SDSS_r_AB = SDSS_r_AB[sel_AGN]
inter_2_SDSS_r_AB_attenuated = SDSS_r_AB_attenuated[sel_AGN]
inter_2_agn_type = agn_type[sel_AGN]
inter_2_ids_active = ids_active[sel_AGN]
inter_2_logNH = logNH[sel_AGN]
inter_2_random = random[sel_AGN]

n.putmask(inter_FX_hard, sel_AGN_left, inter_2_FX_hard)
n.putmask(inter_FX_soft, sel_AGN_left, inter_2_FX_soft)
n.putmask(inter_FX_soft_attenuated, sel_AGN_left, inter_2_FX_soft_attenuated)
n.putmask(inter_LX_hard, sel_AGN_left, inter_2_LX_hard)
n.putmask(inter_LX_soft, sel_AGN_left, inter_2_LX_soft)
n.putmask(inter_SDSS_r_AB, sel_AGN_left, inter_2_SDSS_r_AB)
n.putmask(
    inter_SDSS_r_AB_attenuated,
    sel_AGN_left,
    inter_2_SDSS_r_AB_attenuated)
n.putmask(inter_agn_type, sel_AGN_left, inter_2_agn_type)
n.putmask(inter_ids_active, sel_AGN_left, inter_2_ids_active)
n.putmask(inter_logNH, sel_AGN_left, inter_2_logNH)
n.putmask(inter_random, sel_AGN_left, inter_2_random)

agn_in_clu_FX_hard = inter_FX_hard[clu]
agn_in_clu_FX_soft = inter_FX_soft[clu]
agn_in_clu_FX_soft_attenuated = inter_FX_soft_attenuated[clu]
agn_in_clu_LX_hard = inter_LX_hard[clu]
agn_in_clu_LX_soft = inter_LX_soft[clu]
agn_in_clu_SDSS_r_AB = inter_SDSS_r_AB[clu]
agn_in_clu_SDSS_r_AB_attenuated = inter_SDSS_r_AB_attenuated[clu]
agn_in_clu_agn_type = inter_agn_type[clu]
agn_in_clu_ids_active = inter_ids_active[clu]
agn_in_clu_logNH = inter_logNH[clu]
agn_in_clu_random = inter_random[clu]

print('halo file opened', time.time() - t0)

f1 = h5py.File(path_2_galaxy_file, 'r')
galaxy_stellar_mass = f1['galaxy/SMHMR_mass'].value[clu]
galaxy_star_formation_rate = f1['galaxy/star_formation_rate'].value[clu]
galaxy_LX_hard = f1['galaxy/LX_hard'].value[clu]
galaxy_mag_r = f1['galaxy/mag_r'].value[clu]
galaxy_mag_abs_r = f1['galaxy/mag_abs_r'].value[clu]
f1.close()

print('galaxy file opened', time.time() - t0)

f2 = h5py.File(path_2_coordinate_file, 'r')
ra = f2['/coordinates/ra'].value[clu]
dec = f2['/coordinates/dec'].value[clu]
zzr = f2['/coordinates/redshift_R'].value[clu]
zzs = f2['/coordinates/redshift_S'].value[clu]
dL_cm = f2['/coordinates/dL'].value[clu]
galactic_NH = f2['/coordinates/NH'].value[clu]
galactic_ebv = f2['/coordinates/ebv'].value[clu]
g_lat = f2['/coordinates/g_lat'].value[clu]
g_lon = f2['/coordinates/g_lon'].value[clu]
ecl_lat = f2['/coordinates/ecl_lat'].value[clu]
ecl_lon = f2['/coordinates/ecl_lon'].value[clu]
N_galaxies = len(zzr)
f2.close()

print('coordinate file opened', time.time() - t0)

pix_ids = healpy.ang2pix(
    512,
    n.pi /
    2. -
    g_lat *
    n.pi /
    180.,
    g_lon *
    n.pi /
    180.,
    nest=True)
flux_lim_data = fits.open(path_2_flux_limits)
#flux_limit_eRASS3, flux_limit_eRASS8, flux_limit_SNR3
flux_limit = flux_lim_data[1].data['flux_limit_eRASS8'][pix_ids]
print('flux limit file opened', time.time() - t0)

# matches to the output X-ray luminosity (before milky way)
CLU_LX_soft = n.loadtxt(path_2_XPAINTING_sbhalo, unpack=True, usecols=(11))
attenuation = nh_law(galactic_NH)
CLU_FX_soft = CLU_LX_soft / (4 * n.pi * dL_cm**2.)
CLU_FX_soft_attenuated = attenuation * CLU_FX_soft

sf = (CLU_FX_soft_attenuated + agn_in_clu_FX_soft_attenuated > 10**(flux_limit))

hdu_cols = fits.ColDefs([
    ##
    # Coordinates
    ##
    fits.Column(
        name="ra", format='D', unit='degree', array=ra[sf]), fits.Column(
        name="dec", format='D', unit='degree', array=dec[sf]), fits.Column(
            name="g_lat", format='D', unit='degree', array=g_lat[sf]), fits.Column(
                name="g_lon", format='D', unit='degree', array=g_lon[sf]), fits.Column(
                    name="ecl_lat", format='D', unit='degree', array=ecl_lat[sf]), fits.Column(
                        name="ecl_lon", format='D', unit='degree', array=ecl_lon[sf])    # distances
    # extinction maps
    # Galaxy properties
    , fits.Column(name="redshift_R", format='D', unit='real space', array=zzr[sf]), fits.Column(name="redshift_S", format='D', unit='redshift space', array=zzs[sf]), fits.Column(name="dL_cm", format='D', unit='cm', array=dL_cm[sf]), fits.Column(name="galactic_NH", format='D', unit='cm', array=galactic_NH[sf]), fits.Column(name="galactic_ebv", format='D', unit='cm', array=galactic_ebv[sf]), fits.Column(name="galaxy_stellar_mass", format='D', unit='M_sun', array=galaxy_stellar_mass[sf]), fits.Column(name="galaxy_star_formation_rate", format='D', unit='M_sun/yr', array=galaxy_star_formation_rate[sf]), fits.Column(name="galaxy_LX_hard", format='D', unit='erg/s', array=galaxy_LX_hard[sf]), fits.Column(name="galaxy_mag_r", format='D', unit='', array=galaxy_mag_r[sf]), fits.Column(name="galaxy_mag_abs_r", format='D', unit='', array=galaxy_mag_abs_r[sf])    # AGN properties
    ##
    , fits.Column(name="AGN_LX_soft", format='D', unit='Luminosity/[erg/s] 0.5-2 keV', array=agn_in_clu_LX_soft[sf]), fits.Column(name="AGN_FX_soft", format='D', unit='Flux/[erg/cm2/s] 0.5-2 keV', array=agn_in_clu_FX_soft_attenuated[sf]), fits.Column(name="AGN_LX_hard", format='D', unit='Luminosity/[erg/s] 2-10 keV', array=agn_in_clu_LX_hard[sf]), fits.Column(name="AGN_FX_hard", format='D', unit='Flux/[erg/cm2/s] 2-10 keV', array=agn_in_clu_FX_hard[sf]), fits.Column(name="AGN_SDSS_r_magnitude", format='D', unit='mag', array=agn_in_clu_SDSS_r_AB_attenuated[sf]), fits.Column(name="AGN_Nh", format='D', unit='log10(Nh/[cm-2])', array=agn_in_clu_logNH[sf]), fits.Column(name="AGN_random_number", format='D', unit='', array=agn_in_clu_random[sf]), fits.Column(name="AGN_type", format='D', unit='X/opt type: 11, 12, 21, 22', array=agn_in_clu_agn_type[sf])    # Dark matter halo
    ##
    , fits.Column(name="HALO_M200c", format='D', unit='log10(M_sun)', array=M500c_cat["M200c"][sf]), fits.Column(name="HALO_M500c", format='D', unit='log10(M_sun)', array=M500c_cat["M500c"][sf]), fits.Column(name="HALO_Mvir", format='D', unit='log10(M_sun)', array=M500c_cat["Mvir"][sf]), fits.Column(name="HALO_Acc_Rate_1Tdyn", format='D', unit='Msun/yr', array=M500c_cat["Acc_Rate_1Tdyn"][sf]), fits.Column(name="HALO_NFWconcentration", format='D', unit='', array=M500c_cat["NFWconcentration"][sf]), fits.Column(name="HALO_b_to_a_500c", format='D', unit='', array=M500c_cat["b_to_a_500c"][sf]), fits.Column(name="HALO_c_to_a_500c", format='D', unit='', array=M500c_cat["c_to_a_500c"][sf]), fits.Column(name="HALO_rs", format='D', unit='kpc', array=M500c_cat["rs"][sf]), fits.Column(name="HALO_rvir", format='D', unit='kpc', array=M500c_cat["rvir"][sf]), fits.Column(name="HALO_vmax", format='D', unit='km/s', array=M500c_cat["vmax"][sf])    #
    # cluster properties
    , fits.Column(name="halo_id", format='K', unit='', array=halo_id[clu][sf]), fits.Column(name="pid", format='K', unit='', array=halo_host_id[clu][sf]), fits.Column(name='CLU_FX_soft', unit='erg/cm2/s', format='D', array=CLU_FX_soft[sf]), fits.Column(name='CLU_FX_soft_attenuated', unit='erg/cm2/s', format='D', array=CLU_FX_soft_attenuated[sf]), fits.Column(name='CLU_LX_soft', unit='erg/s', format='D', array=CLU_LX_soft[sf])

])


tb_hdu = fits.BinTableHDU.from_columns(hdu_cols)
prihdr = fits.Header()
prihdr['author'] = 'JC'
prihdu = fits.PrimaryHDU(header=prihdr)
thdulist = fits.HDUList([prihdu, tb_hdu])
if os.path.isfile(path_2_CLU_catalog):
    os.system("rm " + path_2_CLU_catalog)
thdulist.writeto(path_2_CLU_catalog)
