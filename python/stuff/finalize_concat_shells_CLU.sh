#!/bin/bash

ls $MD04/hlists/fits/*_eRO_CLU.fit > fit_list_md04.list

java -jar ~/software/stilts.jar tcat in=@fit_list_md04.list ifmt=fits omode=out ofmt=fits out=/home/comparat/data/MultiDark/MD_0.4Gpc/hlists/fits/MD04_eRO_CLU.fit

ls $MD10/hlist/fits/*_eRO_CLU.fit > fit_list_md01.list

java -jar ~/software/stilts.jar tcat in=@fit_list_md01.list ifmt=fits omode=out ofmt=fits out=/home/comparat/data/MultiDark/MD_1.0Gpc/hlist/fits/MD10_eRO_CLU.fit

rm fit_list_*.list