"""
export GIT_AGN_MOCK='/home/comparat/software/lss_mock_dev/'
export GIT_XPAINT='/home/comparat/software/XSB_Painting'


"""
import glob
import sys
import os
import numpy as n
import time
t0 = time.time()

env = sys.argv[1]  # 'MD10'
ftyp = sys.argv[2]  # 'all' or 'sat'

test_dir = os.path.join(os.environ[env], 'hlists', 'fits')
if env == 'MD04':
    z_max = 0.45
else:
    z_max = 1.5

lss_git_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python')

baseNames_all = sorted(
    n.array(
        [
            os.path.basename(el)[
                :-
                5] for el in n.array(
                    glob.glob(
                        os.path.join(
                            test_dir,
                            ftyp +
                            '_?.?????.fits')))]))

z_array = n.array([1. / float(eel.split('_')[1]) - 1 for eel in baseNames_all])
z_sel = (z_array < z_max)
baseNames = baseNames_all[z_sel]
print(baseNames)


def run_only_gal(env, baseName):
    print('coordinates + galaxies', env, baseName, time.time() - t0)
    os.chdir(lss_git_dir)
    # pathes to general output files :
    path_2_coordinate_file = os.path.join(
        test_dir, baseName + '_coordinates.h5')
    path_2_galaxy_file = os.path.join(test_dir, baseName + '_galaxy.h5')
    if os.path.isfile(path_2_coordinate_file):
        # Adds galaxy properties
        if os.path.isfile(path_2_galaxy_file) == False:
            os.system("python3 002_0_galaxy.py " + env + ' ' + baseName)
        if os.path.isfile(path_2_galaxy_file):
            print("gal done")
            os.remove(path_2_galaxy_file)
            os.system("python3 002_0_galaxy.py " + env + ' ' + baseName)


for bn in baseNames[::-1]:
    print(env, bn)
    run_only_gal(env, bn)
