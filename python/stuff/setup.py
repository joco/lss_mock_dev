import dustmaps
import healpy
import h5py
import astropy
import scipy
import numpy
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
import os

# Data file dependencies
path_2_NH_map = '/data17s/darksim/observations/h1_maps/H14PI/asu.fit'

path_2_planck_EBV_map = '/data17s/darksim/observations/dust_maps/'
# this directory must contain this file a directory planck that contains :
# HFI_CompMap_ThermalDustModel_2048_R1.20.fits

test_dir = os.path.join(os.environ['MD04'], 'hlists', 'fits')


#baseName = "sat_0.62840"
#baseName = "sat_0.64030"
#baseName = "sat_0.82630"
#baseName = "sat_0.86230"
#baseName = "sat_0.90740"
## baseName = "sat_0.95600"
## baseName = "sat_1.00000"
#baseName = "all_0.64030"
#baseName = "all_0.82630"
#baseName = "all_0.86230"
baseName = "all_0.90740"
## baseName = "all_0.95600"
## baseName = "all_1.00000"

path_2_light_cone = os.path.join(test_dir, baseName + '.fits')
path_2_coordinate_file = os.path.join(test_dir, baseName + '_coordinates.h5')
path_2_galaxy_file = os.path.join(test_dir, baseName + '_galaxy.h5')
path_2_agn_file = os.path.join(test_dir, baseName + '_agn.h5')
path_2_eRO_catalog = os.path.join(test_dir, baseName + '_eRO_AGN.fit')

# X-ray K-correction and attenuation curves
path_2_hard_RF_obs_soft = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    "data",
    "xray_k_correction",
    "fraction_observed_A15_RF_hard_Obs_soft_fscat_002.txt")
path_2_RF_obs_hard = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    "data",
    "xray_k_correction",
    "fraction_observed_A15_RF_hard_Obs_hard_fscat_002.txt")
path_2_NH_attenuation = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    "data",
    "xray_k_correction",
    'gal_nh_ratio_relation_newg16.dat')

# eRosita flux limits
path_2_flux_limits = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    "data",
    "erosita",
    "flux_limits.fits")


# simulation setup

cosmoMD = FlatLambdaCDM(
    H0=67.77 *
    u.km /
    u.s /
    u.Mpc,
    Om0=0.307115,
    Ob0=0.048206)
h = 0.6777
L_box = 1000.0 / h


sudo apt install virtualenv
python3 - m pip install - -user virtualenv

virtualenv pyEnv_lss_mock

source pyEnv_lss_mock / bin / activate

python3 - m pip install - -upgrade pip

python3 - m pip install - -user numpy scipy astropy h5py healpy dustmaps
