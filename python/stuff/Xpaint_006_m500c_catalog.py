"""
Creates a fits catalog to input XSB painting

clu = ( M500c > 5e13 )

"""
from astropy_healpix import healpy
import sys
import os
import time
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
import astropy.io.fits as fits
import h5py
import numpy as n
print('CREATES FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()
#import astropy.io.fits as fits

# import all pathes

env = sys.argv[1]  # 'MD04'
baseName = sys.argv[2]  # "sat_0.62840"
print(env, baseName)

test_dir = os.path.join(os.environ[env], 'hlists', 'fits')

path_2_light_cone = os.path.join(test_dir, baseName + '.fits')
path_2_coordinate_file = os.path.join(test_dir, baseName + '_coordinates.h5')
path_2_galaxy_file = os.path.join(test_dir, baseName + '_galaxy.h5')
path_2_M500c_catalog = os.path.join(test_dir, baseName + '_m500c.fit')

# simulation setup
if env == "MD10" or env == "MD04":
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT


f1 = fits.open(path_2_light_cone)
if f1[1].columns.names[0] == 'id':
    Mvir = f1[1].data['Mvir'] / h
    rvir = f1[1].data['Rvir']
    rs = f1[1].data['rs']
    scale_of_last_MM = f1[1].data['scale_of_last_MM']
    vmax = f1[1].data['vmax']
    M200c = f1[1].data['M200c'] / h
    M500c = f1[1].data['M500c'] / h
    b_to_a_500c = f1[1].data['b_to_a_500c']
    c_to_a_500c = f1[1].data['c_to_a_500c']
    Acc_Rate_1_Tdyn = f1[1].data['Acc_Rate_1_Tdyn']
    halo_id = f1[1].data['id']
    if os.path.basename(path_2_light_cone)[:3] == 'sat':
        halo_host_id = f1[1].data['pid']
    else:
        halo_host_id = -n.ones_like(halo_id).astype('int')

else:
    if os.path.basename(path_2_light_cone)[:3] == 'sat':
        hh = 'id pid Mvir Rvir rs scale_of_last_MM vmax x y z vx vy vz M200c M500c b_to_a_500c c_to_a_500c Acc_Rate_1_Tdyn'
        header = hh.split()
        dc_col = {}
        for ii in range(len(header)):
            dc_col[header[ii]] = 'col' + str(ii + 1)

    if os.path.basename(path_2_light_cone)[:3] == 'all':
        hh = 'id Mvir Rvir rs scale_of_last_MM vmax x y z vx vy vz M200c M500c b_to_a_500c c_to_a_500c Acc_Rate_1_Tdyn'
        header = hh.split()
        dc_col = {}
        for ii in range(len(header)):
            dc_col[header[ii]] = 'col' + str(ii + 1)

    Mvir = f1[1].data[dc_col['Mvir']] / h
    rvir = f1[1].data[dc_col['Rvir']]
    rs = f1[1].data[dc_col['rs']]
    scale_of_last_MM = f1[1].data[dc_col['scale_of_last_MM']]
    vmax = f1[1].data[dc_col['vmax']]
    M200c = f1[1].data[dc_col['M200c']] / h
    M500c = f1[1].data[dc_col['M500c']] / h
    b_to_a_500c = f1[1].data[dc_col['b_to_a_500c']]
    c_to_a_500c = f1[1].data[dc_col['c_to_a_500c']]
    Acc_Rate_1_Tdyn = f1[1].data[dc_col['Acc_Rate_1_Tdyn']]
    halo_id = f1[1].data[dc_col['id']]

    if os.path.basename(path_2_light_cone)[:3] == 'sat':
        halo_host_id = f1[1].data[dc_col['pid']]
    else:
        halo_host_id = -n.ones_like(halo_id).astype('int')
f1.close()

print('halo file opened', time.time() - t0)

f2 = h5py.File(path_2_coordinate_file, 'r')
ra = f2['/coordinates/ra'].value
dec = f2['/coordinates/dec'].value
zzr = f2['/coordinates/redshift_R'].value
zzs = f2['/coordinates/redshift_S'].value
N_galaxies = len(zzr)
f2.close()

print('coordinate file opened', time.time() - t0)


f1 = h5py.File(path_2_galaxy_file, 'r')
galaxy_stellar_mass = f1['galaxy/SMHMR_mass'].value
galaxy_star_formation_rate = f1['galaxy/star_formation_rate'].value
galaxy_LX_hard = f1['galaxy/LX_hard'].value
f1.close()

print('galaxy file opened', time.time() - t0)

# selection function here :
sf = (M500c > 5e13)

print('selection function applied', time.time() - t0)
arr_0 = n.zeros(len(M500c[sf]))

hdu_cols = fits.ColDefs([
    ##
    # Coordinates
    ##
    fits.Column(name="ra", format='D', unit='degree', array=ra[sf]), fits.Column(
        name="dec", format='D', unit='degree', array=dec[sf])    # distances
    # Galaxy properties
    # ,fits.Column(name= "galaxy_star_formation_rate"  , format='D', unit='M_sun', array = galaxy_star_formation_rate [sf] )
    , fits.Column(name="redshift_R", format='D', unit='real space', array=zzr[sf]), fits.Column(name="redshift_S", format='D', unit='redshift space', array=zzs[sf]), fits.Column(name="galaxy_stellar_mass", format='D', unit='M_sun', array=galaxy_stellar_mass[sf])    # ,fits.Column(name= "galaxy_LX_hard"  , format='D', unit='M_sun', array = galaxy_LX_hard [sf] )
    ##
    # Dark matter halo
    ##
    , fits.Column(name="M200c", format='D', unit='log10(M_sun)', array=n.log10(M200c[sf])), fits.Column(name="M500c", format='D', unit='log10(M_sun)', array=n.log10(M500c[sf])), fits.Column(name="M2500c", format='K', unit='log10(M_sun)', array=arr_0), fits.Column(name="Mvir", format='D', unit='log10(M_sun)', array=n.log10(Mvir[sf])), fits.Column(name="Mpeak", format='K', unit='log10(M_sun)', array=arr_0)    #
    , fits.Column(name="Acc_Rate_100Myr", format='D', unit='Msun/yr', array=arr_0), fits.Column(name="Acc_Rate_1Tdyn", format='D', unit='Msun/yr', array=Acc_Rate_1_Tdyn[sf]), fits.Column(name="Acc_Rate_2Tdyn", format='D', unit='Msun/yr', array=arr_0)    #
    , fits.Column(name="NFWconcentration", format='D', unit='', array=rvir[sf] / rs[sf])    #
    , fits.Column(name="b_to_a", format='D', unit='', array=arr_0), fits.Column(name="b_to_a_500c", format='D', unit='', array=b_to_a_500c[sf]), fits.Column(name="c_to_a", format='D', unit='', array=arr_0), fits.Column(name="c_to_a_500c", format='D', unit='', array=c_to_a_500c[sf]), fits.Column(name="rs", format='D', unit='kpc', array=rs[sf]), fits.Column(name="rvir", format='D', unit='kpc', array=rvir[sf]), fits.Column(name="vmax", format='D', unit='km/s', array=vmax[sf])
    # ,fits.Column(name=  "halo_id"             , format='K', unit='', array = halo_id[sf]               )
    # ,fits.Column(name=  "pid"             , format='K', unit='', array = halo_host_id[sf] )
])

print('fits columnes created', time.time() - t0)

tb_hdu = fits.BinTableHDU.from_columns(hdu_cols)
prihdr = fits.Header()
prihdr['author'] = 'JC'
prihdu = fits.PrimaryHDU(header=prihdr)
thdulist = fits.HDUList([prihdu, tb_hdu])

if os.path.isfile(path_2_M500c_catalog):
    os.system("rm " + path_2_M500c_catalog)
thdulist.writeto(path_2_M500c_catalog)
print('written', path_2_M500c_catalog, time.time() - t0)
