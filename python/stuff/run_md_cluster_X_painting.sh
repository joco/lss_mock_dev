"""
export GIT_AGN_MOCK='/home/comparat/software/lss_mock_dev/'
export GIT_XPAINT='/home/comparat/software/XSB_Painting'


"""
import time
t0=time.time()
import os, sys, glob
import numpy as n

env = sys.argv[1] # 'MD10'
ftyp = sys.argv[2] # 'all' or 'sat'

laptop = False
#laptop = True
if laptop:
	stilts_cmd = 'java -jar /home/comparat/software/stilts.jar'
else:
	stilts_cmd = 'stilts'

test_dir = os.path.join( os.environ[env], 'hlists', 'fits' )

x_paint_git_dir = os.path.join( os.environ['GIT_XPAINT'])
lss_git_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'python')

baseNames_all = n.array([ os.path.basename(el)[:-5] for el in n.array(glob.glob(os.path.join(test_dir, ftyp+'_?.?????.fits'))) ])
baseNames_all.sort()

if env=='MD04':
	z_max = 0.45 
else:
	z_max = 1.5

z_array = n.array([ 1./float(eel.split('_')[1])-1 for eel in baseNames_all ])
z_sel = (z_array<z_max)
baseNames = baseNames_all[z_sel]
baseNames.sort()
print(baseNames)

def concatenate_agn(env):
	print('concatenate all AGN catalogs', env, time.time()-t0)
	path_2_agn_summary_file = os.path.join(test_dir, env+"_"+ftyp+"_eRO_AGN.fit")
	#if os.pah.isfile(path_2_agn_summary_file)==False:
	os.chdir(lss_git_dir)
	# concatenates shells into a single fits catalog 
	c1='ls '+test_dir+'/'+ftyp+'_*_eRO_AGN.fit > fit_list_'+env+"_"+ftyp+'_AGN.list'
	print(c1)
	os.system(c1)
	c2=stilts_cmd+""" tcat in=@fit_list_""" + env+"_"+ftyp+ """_AGN.list ifmt=fits omode=out ofmt=fits out="""+path_2_agn_summary_file
	print(c2)
	os.system(c2)
	os.system('rm fit_list_'+env+'_AGN.list')
	# simput format table + images
	# revise code to create spectra 
	os.system("python3 simput_AGN.py "+env+" "+ftyp)
	
	
def run_all_clusters(env, baseName):
	print('cluster',env, baseName, time.time()-t0)
	os.chdir(lss_git_dir)
	##
	##
	# Clusters
	##
	##
	# cluster related quantities
	if ftyp=='all':
		# pathes to cluster output files :
		# creates input for X-ray painting code
		path_2_CLU_file = os.path.join(test_dir, baseName+'_CLU.h5')
		path_2_CLU_catalog = os.path.join(test_dir, baseName+'_eRO_CLU.fit')
		print(path_2_CLU_file)
		# parent cluster catalog M500c>1e13
		print('mCLU file', time.time()-t0)
		if os.path.isfile(path_2_CLU_file)==False:
			os.system("python3 005_cluster_Xray.py "+env + ' ' + baseName)
		else:
			#print("eroCAT done")
			os.remove(path_2_CLU_file)
			os.system("python3 005_cluster_Xray.py "+env + ' ' + baseName)
			
def run_all_clusters_XPainting(env, baseName):
	print('cluster',env, baseName, time.time()-t0)
	os.chdir(lss_git_dir)
	##
	##
	# Clusters
	##
	##
	# cluster related quantities
	if ftyp=='all':
		# pathes to cluster output files :
		# creates input for X-ray painting code
		path_2_M500c_catalog = os.path.join(test_dir, baseName+'_m500c.fit')
		path_2_XPAINTING_sbhalo = os.path.join(test_dir, baseName+"_m500c_sbhalo.txt" )
		path_2_XPAINTING_sbprof = os.path.join(test_dir, baseName+"_m500c_sbprof.txt" ) 
		path_2_CLU_catalog = os.path.join(test_dir, baseName+'_eRO_CLU.fit')
		print(path_2_M500c_catalog)
		print(path_2_XPAINTING_sbhalo)
		print(path_2_XPAINTING_sbprof)
		print(path_2_CLU_catalog)
		# parent cluster catalog M500c>1e13
		print('m500c file', time.time()-t0)
		if os.path.isfile(path_2_M500c_catalog)==False:
			os.system("python3 006_m500c_catalog.py "+env + ' ' + baseName)
		else:
			#print("eroCAT done")
			os.remove(path_2_M500c_catalog)
			os.system("python3 006_m500c_catalog.py "+env + ' ' + baseName)

		# X-ray painting from E. Lau, D. Nagai
		print('X painting code', time.time()-t0)
		if os.path.isfile(path_2_XPAINTING_sbhalo)==False:
			# running the X-ray painting code 
			os.chdir(x_paint_git_dir)
			os.system("./sbprof example.cfg "+env+" "+baseName+"_m500c.fit ")
			os.system("mv sbhalo.txt "+path_2_XPAINTING_sbhalo)
			os.system("mv sbprof.txt "+path_2_XPAINTING_sbprof)
		else:
			os.remove(path_2_XPAINTING_sbhalo)
			os.chdir(x_paint_git_dir)
			os.system("./sbprof example.cfg "+env+" "+baseName+"_m500c.fit ")
			os.system("mv sbhalo.txt "+path_2_XPAINTING_sbhalo)
			os.system("mv sbprof.txt "+path_2_XPAINTING_sbprof)
			
		# eRASS8 cluster catalog, flux limit (point-like sources)
		print('merge outputs', time.time()-t0)
		if os.path.isfile(path_2_CLU_catalog)==False:
			#gather into a single catalog 
			os.chdir(lss_git_dir)
			os.system("python3 007_merge_XBS_painting_outputs.py "+env + ' ' + baseName)
		else:
			os.remove(path_2_CLU_catalog)
			os.chdir(lss_git_dir)
			os.system("python3 007_merge_XBS_painting_outputs.py "+env + ' ' + baseName)
			

def concatenate_clusters(env):
	if ftyp=='all':
		os.chdir(lss_git_dir)
		path_2_cluster_summary_file = os.path.join(test_dir, env+"_"+ftyp+"_eRO_CLU.fit")
		# concatenates shells into a single fits catalog for the clusters, for each simulation
		c1='ls '+test_dir+'/'+ftyp+'_*_eRO_CLU.fit > fit_list_'+env+"_"+ftyp+'_CLU.list'
		print(c1)
		os.system(c1)
		c2 = stilts_cmd+""" tcat in=@fit_list_""" + env+"_"+ftyp + """_CLU.list ifmt=fits omode=out ofmt=fits out="""+path_2_cluster_summary_file
		print(c2)
		os.system(c2)
		# simput format table + images
		# create a basis of shells in arcsecond space : pixels of 1 arcsecond
		# all pixels which center is within the following 1200 shells 10", 20, 30, 60, 120,  1200, 12000 by 10 of (0,0)
		# only create shells with total flux > flux_limit/100
		# create a set of spectra varying redshift (light cone shell redshifts) for each kT existing
		# replicate each cluster entry many times and create images with circular shells that 
		# match the spectrum + kT + norm 


##
##
# eRosita Cluster galaxies
##
##
# galaxies around clusters within some X-ray SB radius  at all redshifts 
def run_galaxies_around_clusters(env, bn):
	os.chdir(lss_git_dir)
	path_2_galaxyAroundClusters_catalog = os.path.join(test_dir, bn+'_galaxiesAroundClusters.fit')
	if os.path.isfile(path_2_galaxyAroundClusters_catalog)==False:
		os.system("python3 008_galaxiesAroundClusters.py "+env + ' ' + bn)
	else:
		os.remove(path_2_galaxyAroundClusters_catalog)
		os.system("python3 008_galaxiesAroundClusters.py "+env + ' ' + bn)

def concatenate_cluster_galaxies(env):
	os.chdir(lss_git_dir)
	path_2_galaxiesAroundClusters_summary_file = os.path.join(test_dir, env+"_"+ftyp+"_galaxiesAroundClusters.fit")
	# concatenates shells into a single fits catalog for the clusters, for each simulation
	c1='ls '+test_dir+'/'+ftyp+'_*_galaxiesAroundClusters.fit > fit_list_'+env+"_"+ftyp+'_galaxiesAroundClusters.list'
	print(c1)
	os.system(c1)
	c2 = stilts_cmd+""" tcat in=@fit_list_""" + env+"_"+ftyp + """_galaxiesAroundClusters.list ifmt=fits omode=out ofmt=fits out="""+path_2_galaxiesAroundClusters_summary_file
	print(c2)
	os.system(c2)
	# simput format table + images
	# create a basis of shells in arcsecond space : pixels of 1 arcsecond
	# all pixels which center is within the following 1200 shells 10", 20, 30, 60, 120,  1200, 12000 by 10 of (0,0)
	# only create shells with total flux > flux_limit/100
	# create a set of spectra varying redshift (light cone shell redshifts) for each kT existing
	# replicate each cluster entry many times and create images with circular shells that 
	# match the spectrum + kT + norm 

#correlates : ra, dec, r_vir_in_asrcseconds
#MD10_all_eRO_CLU.fit
#to positions in 
#all_0.97810_coordinates.h5  sat_0.91520_coordinates.h5 

#save data within 1 rvir with >0 distance of the cluster with the haversine tree (as in 4FS)
#save data within 1 rvir < r < 2 rvir distance
# up to redshift 2 only

##
##
# 4MOST Cosmology
##
##
# LSS traced by galaxies, BG, LRG, ELG, optical QSO

##
##
# 4MOST WAVES
##
##
# full catalog over specific areas of the sky


# compute clustering in all shells
# stellar mass selected samples, dlogM = 0.5
# halo mass Mvir selected samples, dlogM = 0.5
# V max selected samples
# particular tracers: agn, cluster, cluster galaxies, cosmo ...



#for bn in baseNames[::-1]:
	#print(env, bn)
	#run_all_gal(env, bn)
	#run_all_agn(env, bn)

#concatenate_agn(env)

for bn in baseNames[::-1]:
	print(env, bn)
	run_all_clusters(env, bn)

concatenate_clusters(env)

for bn in baseNames[::-1]:
	print(env, bn)
	run_galaxies_around_clusters(env, bn)

concatenate_cluster_galaxies(env)

#for bn in baseNames_1[::-1]:
	#run_all(env, bn)



# rm /home/comparat/data/MultiDark/MD_0.4Gpc/hlists/fits/*.h5
# rm /home/comparat/data/MultiDark/MD_0.4Gpc/hlists/fits/*.fit
# rm /home/comparat/data/MultiDark/MD_0.4Gpc/hlists/fits/*.txt

# rm /home/comparat/data/MultiDark/MD_1.0Gpc/hlist/fits/*.h5
# rm /home/comparat/data/MultiDark/MD_1.0Gpc/hlist/fits/*.fit
# rm /home/comparat/data/MultiDark/MD_1.0Gpc/hlist/fits/*.txt
