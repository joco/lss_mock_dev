"""
What it does
------------

Plots the cluster model

References
----------

Command to run
--------------

conda activate eroconda
python3 004_5_plot_clusters_colors.py MD10

arguments
---------

environmentVAR: environment variable linking to the directory where files are e.g. "MD10"
It will then	 work in the directory : $environmentVAR/hlists/fits/

Dependencies
------------

import time, os, sys, glob, numpy, astropy, scipy, matplotlib

"""
import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning) 
from astropy.table import Table, Column
from scipy.stats import scoreatpercentile
import glob
import sys
from astropy_healpix import healpy
import os
import time
import matplotlib
matplotlib.use('Agg')
matplotlib.rcParams.update({'font.size': 14})
import matplotlib.pyplot as p

from redmapper.redsequence import RedSequenceColorPar
import astropy.units as u
from astropy.cosmology import FlatLambdaCDM
from scipy.stats import norm
from scipy.interpolate import interp1d
import astropy.io.fits as fits
import numpy as n
print('Create file with galaxies around clusters')
print('=> Abundance matching for magnitudes')
print('=> Red sequence colors')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()


env =  sys.argv[1]
#delta_crit = sys.argv[2]
#print(env, delta_crit)
test_dir = os.path.join(os.environ[env])
all_catalogs = n.array(glob.glob(os.path.join(test_dir, 'fits', 'all_*_galaxiesAroundClusters_*_LS.fit')))
all_aexp = n.array([ os.path.basename(el).split('_')[1] for el in all_catalogs ])
all_aexp.sort()

fig_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'figures', env, 'clusters', 'galaxies')
if os.path.isdir(fig_dir) == False:
	os.system('mkdir -p ' + fig_dir)

path_2_spiders_host_file = os.path.join(
	os.environ['HOME'],
	'hegcl/SPIDERS',
	'mastercatalogue_FINAL_CODEXID.fits')
spiders_host = fits.open(path_2_spiders_host_file)[1].data

path_2_spiders_file = os.path.join(
	os.environ['HOME'],
	'hegcl/SPIDERS',
	'mastercatalogue_FINAL_CODEXID-flat_DR16_firefly.fits')
spiders_gal = fits.open(path_2_spiders_file)[1].data

magi_low, magi_high, frac_obs = n.loadtxt(os.path.join(os.environ['GIT_AGN_MOCK'], 'data/spiders/fraction-observed-cmodemag-i.txt'), unpack=True)
frac_obs_itp = interp1d (
	n.hstack(( magi_low[0]-10,(magi_low+ magi_high)/2., magi_high[-1]+10 )) , 
	n.hstack((1., frac_obs, 1. )) ) 

sdss_redmapper_members = '/home/comparat/data/redmapper/Archive/redmapper_dr8_public_v6.3_members.fits.gz'
sdss_RM_mem = Table.read(sdss_redmapper_members)

gr_sdss_data = sdss_RM_mem['MODEL_MAG_G'] - sdss_RM_mem['MODEL_MAG_R']
rz_sdss_data = sdss_RM_mem['MODEL_MAG_R'] - sdss_RM_mem['MODEL_MAG_Z']
zz_sdss_data = sdss_RM_mem['Z_SPEC']

# red sequence DATA
red_seq_dir = os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'redSequence')
path_2_f1 = os.path.join(red_seq_dir, 'sdss_dr8_cal_zspec_redgals_model.fit')
path_2_f2 = os.path.join(red_seq_dir, 'sdss_dr8_cal_iter3_pars.fit')

f1 = fits.open(path_2_f1)
f2 = fits.open(path_2_f2)

redshift_RS = f2[1].data['nodes'][0]
ug_RS = f2[1].data['meancol'].T[0].T[0]
gr_RS = f2[1].data['meancol'].T[1].T[0]
ri_RS = f2[1].data['meancol'].T[2].T[0]
iz_RS = f2[1].data['meancol'].T[3].T[0]

ug_sigma_RS = f2[1].data['meancol_scatter'].T[0].T[0]
gr_sigma_RS = f2[1].data['meancol_scatter'].T[1].T[0]
ri_sigma_RS = f2[1].data['meancol_scatter'].T[2].T[0]
iz_sigma_RS = f2[1].data['meancol_scatter'].T[3].T[0]

ug_RS_itp = interp1d(n.hstack((0., redshift_RS, 2.0)),
					n.hstack((ug_RS[0], ug_RS, ug_RS[-1])))
gr_RS_itp = interp1d(n.hstack((0., redshift_RS, 2.0)),
					n.hstack((gr_RS[0], gr_RS, gr_RS[-1])))
ri_RS_itp = interp1d(n.hstack((0., redshift_RS, 2.0)),
					n.hstack((ri_RS[0], ri_RS, ri_RS[-1])))
iz_RS_itp = interp1d(n.hstack((0., redshift_RS, 2.0)),
					n.hstack((iz_RS[0], iz_RS, iz_RS[-1])))

ug_sigma_RS_itp = interp1d(n.hstack((0., redshift_RS, 2.0)), n.hstack(
	(ug_sigma_RS[0], ug_sigma_RS, ug_sigma_RS[-1])))
gr_sigma_RS_itp = interp1d(n.hstack((0., redshift_RS, 2.0)), n.hstack(
	(gr_sigma_RS[0], gr_sigma_RS, gr_sigma_RS[-1])))
ri_sigma_RS_itp = interp1d(n.hstack((0., redshift_RS, 2.0)), n.hstack(
	(ri_sigma_RS[0], ri_sigma_RS, ri_sigma_RS[-1])))
iz_sigma_RS_itp = interp1d(n.hstack((0., redshift_RS, 2.0)), n.hstack(
	(iz_sigma_RS[0], iz_sigma_RS, iz_sigma_RS[-1])))

# red sequence DATA legacy survey
LS_path_2_f1 = os.path.join(red_seq_dir, 'decals_dr8_cal_zspec_redgals_model.fit')
LS_path_2_f2 = os.path.join(red_seq_dir, 'decals_dr8_cal_iter3_pars.fit')

LS_f1 = fits.open(LS_path_2_f1)
LS_f2 = fits.open(LS_path_2_f2)

LS_redshift_RS = LS_f1[1].data['nodes'][0]
LS_gr_RS = LS_f2[1].data['meancol'].T[0].T[0]
LS_rz_RS = LS_f2[1].data['meancol'].T[1].T[0]

LS_gr_sigma_RS = LS_f2[1].data['meancol_scatter'].T[0].T[0]
LS_rz_sigma_RS = LS_f2[1].data['meancol_scatter'].T[1].T[0]

LS_gr_RS_itp = interp1d(n.hstack((0., LS_redshift_RS, 2.0)),n.hstack(( LS_gr_RS[0], LS_gr_RS, LS_gr_RS[-1])))
LS_rz_RS_itp = interp1d(n.hstack((0., LS_redshift_RS, 2.0)),n.hstack(( LS_rz_RS[0], LS_rz_RS, LS_rz_RS[-1])))

LS_gr_sigma_RS_itp = interp1d(n.hstack((0., LS_redshift_RS, 2.0)), n.hstack(( LS_gr_sigma_RS[0], LS_gr_sigma_RS, LS_gr_sigma_RS[-1])))
LS_rz_sigma_RS_itp = interp1d(n.hstack((0., LS_redshift_RS, 2.0)), n.hstack(( LS_rz_sigma_RS[0], LS_rz_sigma_RS, LS_rz_sigma_RS[-1])))

rm_dir = os.path.join(os.environ['HOME'],'data/erosita/eRASS/redmapper/legacy_dr8_0.05_0.72_grz_v1.0.0/cats/ext_gt_0/')
rm_cat = os.path.join( rm_dir, 'all_e1_200614_poscorr_mpe_clean_eromapper_mlt40_lgt20_vl50_catalog_members.fit' )
RM = Table.read(rm_cat)
RM_redshift = RM['desi_photo_z']
RM_gr       = RM['mag'].T[0] - RM['mag'].T[1]    
RM_rz       = RM['mag'].T[1] - RM['mag'].T[2]    

# simulation setup
if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    L_box = 1000.0 / h
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    from astropy.cosmology import FlatLambdaCDM
    import astropy.units as u
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

# opti0nal red sequence figure
fig_out = os.path.join(fig_dir, 'red_sequence.png')
z_all = n.arange(0., 1.2, 0.05)
p.figure(1, (6., 6.))

p.plot(z_all, gr_RS_itp(z_all), label='SDSS gr model', color='blue', ls='solid')
p.plot(z_all, gr_RS_itp(z_all)+gr_sigma_RS_itp(z_all), color='blue', ls='dashed')
p.plot(z_all, gr_RS_itp(z_all)-gr_sigma_RS_itp(z_all), color='blue', ls='dashed')

p.plot(z_all, ri_RS_itp(z_all), label='SDSS ri model', color='green', ls='solid')
p.plot(z_all, ri_RS_itp(z_all)+ri_sigma_RS_itp(z_all), color='green', ls='dashed')
p.plot(z_all, ri_RS_itp(z_all)-ri_sigma_RS_itp(z_all), color='green', ls='dashed')

p.plot(z_all, iz_RS_itp(z_all), label='SDSS iz model', color='red', ls='solid')
p.plot(z_all, iz_RS_itp(z_all)+iz_sigma_RS_itp(z_all), color='red', ls='dashed')
p.plot(z_all, iz_RS_itp(z_all)-iz_sigma_RS_itp(z_all), color='red', ls='dashed')

p.plot(z_all, LS_gr_RS_itp(z_all), label='LS gr model', color='m', ls='solid')
p.plot(z_all, LS_gr_RS_itp(z_all)+LS_gr_sigma_RS_itp(z_all), color='m', ls='dashed')
p.plot(z_all, LS_gr_RS_itp(z_all)-LS_gr_sigma_RS_itp(z_all), color='m', ls='dashed')

p.plot(z_all, LS_rz_RS_itp(z_all), label='LS rz model', color='k', ls='solid')
p.plot(z_all, LS_rz_RS_itp(z_all)+LS_rz_sigma_RS_itp(z_all), color='k', ls='dashed')
p.plot(z_all, LS_rz_RS_itp(z_all)-LS_rz_sigma_RS_itp(z_all), color='k', ls='dashed')

p.legend(frameon=False)
p.xlabel('redshift')
p.ylabel('color')
#p.ylabel('probability distribution function')
p.grid()
# p.ylim((0,1.1))
# p.yscale('log')
# p.xscale('log')
p.savefig(fig_out)
p.clf()

color_bins = n.arange(-0.05, 3.5,0.01)


for aexp_str in all_aexp[::-1][2:-5]:
	baseName = 'all_'+aexp_str # sys.argv[2]  # "all_0.62840"
	z_snap = 1./float(baseName.split('_')[1])-1.
	aexp_str = str(int(float(baseName.split('_')[1])*1e5)).zfill(6)
	print(env, baseName)
	test_dir = os.path.join(os.environ[env])

	path_2_CLU_SAT_catalog = os.path.join(test_dir, 'fits', baseName + '_galaxiesAroundClusters_MD10_eRO_CLU_b8_CM_0_pixS_20.0.fit')
	path_2_CLU_MAG_catalog = os.path.join(test_dir, 'fits', baseName + '_galaxiesAroundClusters_MD10_eRO_CLU_b8_CM_0_pixS_20.0_LS.fit')
	# galaxies

	hdu_clu = Table.read(path_2_CLU_SAT_catalog)  # , mode='update')
	hdu_mag = Table.read(path_2_CLU_MAG_catalog)  # , mode='update')
	LS_gr = hdu_mag['MAG_G']-hdu_mag['MAG_R']
	LS_rz = hdu_mag['MAG_R']-hdu_mag['MAG_Z']
	is_quiescent = hdu_clu['is_quiescent']
	zr_gal = hdu_clu['redshift_R']
	mean_z = n.mean(zr_gal)
	mag_available = (hdu_clu['sdss_r_err'] > 0 ) & ( is_quiescent )
	sdss_gr_mock = hdu_clu['sdss_g'][mag_available] # - hdu_clu['sdss_r'][mag_available]
	sdss_ri_mock = hdu_clu['sdss_r'][mag_available] # - hdu_clu['sdss_i'][mag_available]
	sdss_iz_mock = hdu_clu['sdss_i'][mag_available] # - hdu_clu['sdss_z'][mag_available]
	sdss_rz_mock = hdu_clu['sdss_r'][mag_available] # - hdu_clu['sdss_z'][mag_available]

	mag_available_LS = (hdu_clu['LS_z_err'] > 0 ) & ( is_quiescent ) & (hdu_clu['LS_g']>0) & (hdu_clu['LS_r']>0) & (hdu_clu['LS_z']>0) & (hdu_clu['LS_g']<50) & (hdu_clu['LS_r']<50) & (hdu_clu['LS_z']<50)
	LS_gr_mock = hdu_clu['galaxy_gr_LS'][mag_available_LS] #- hdu_clu['LS_r'][mag_available_LS]
	LS_rz_mock = hdu_clu['galaxy_rz_LS'][mag_available_LS] #- hdu_clu['LS_z'][mag_available_LS]

	zmin = n.min(zr_gal) 
	zmax = n.max(zr_gal) 
	print(zmin, '<z<', zmax)

	# g-r color
	# opti0nal red sequence figure
	fig_out = os.path.join(fig_dir, 'red_sequence_GR_'+aexp_str+'.png')
	p.figure(1, (6., 6.))
	# cumulative, density hist
	p.hist(sdss_gr_mock, bins=color_bins, histtype = 'step', rasterized = True,label='SDSS gr mock', color='blue' , cumulative=True, density=True, lw=2)
	p.hist(LS_gr_mock, bins=color_bins, histtype = 'step', rasterized = True,label='LS gr mock', color='cyan' , cumulative=True, density=True, lw=2)
	# density hist
	p.hist(sdss_gr_mock, bins=color_bins, histtype = 'step', rasterized = True, color='blue', density=True, lw=2)
	out = p.hist(LS_gr_mock, bins=color_bins, histtype = 'step', rasterized = True, color='cyan', density=True, lw=2)

	RM_selection = (RM_redshift>zmin)&(RM_redshift<zmax)
	if len(RM_gr[RM_selection])>0:
		p.hist(RM_gr[RM_selection], bins=color_bins, histtype = 'step', rasterized = True,label='LS gr data', color='r', cumulative=True, density=True, lw=2)
		p.hist(RM_gr[RM_selection], bins=color_bins, histtype = 'step', rasterized = True, color='r', density=True, lw=2)

	RM_selection_2 = (zz_sdss_data>zmin)&(zz_sdss_data<zmax)
	if len(gr_sdss_data[RM_selection_2])>0:
		p.hist(gr_sdss_data[RM_selection_2], bins=color_bins, histtype = 'step', rasterized = True,label='SDSS gr data', color='g', cumulative=True, density=True, lw=2)
		p.hist(gr_sdss_data[RM_selection_2], bins=color_bins, histtype = 'step', rasterized = True, color='g', density=True, lw=2)

	#p.axvline(gr_RS_itp(mean_z), label='gr model', color='blue', ls='dotted')
	#p.axvline(gr_RS_itp(mean_z) + gr_sigma_RS_itp(mean_z), color='blue', ls='dashed')
	#p.axvline(gr_RS_itp(mean_z) - gr_sigma_RS_itp(mean_z), color='blue', ls='dashed')

	p.axvline(LS_gr_RS_itp(mean_z), label='LS gr model', color='m', ls='dotted')
	p.axvline(LS_gr_RS_itp(mean_z) + LS_gr_sigma_RS_itp(mean_z), color='m', ls='dashed')
	p.axvline(LS_gr_RS_itp(mean_z) - LS_gr_sigma_RS_itp(mean_z), color='m', ls='dashed')

	p.xlim((n.min(out[1][1:][out[0] > 0]) - 0.05, n.max(out[1][1:][out[0] > 0]) + 0.05))
	p.legend(frameon=False)
	p.xlabel('color g-r')
	p.ylabel('probability distribution function')
	p.grid()
	p.title('z='+str(n.round(z_snap,2)))
	p.savefig(fig_out)
	p.clf()

	# r-z color
	# opti0nal red sequence figure
	fig_out = os.path.join(fig_dir, 'red_sequence_RZ_'+aexp_str+'.png')
	p.figure(1, (6., 6.))
	# SDSS 
	p.hist(sdss_rz_mock, bins=color_bins, histtype = 'step', rasterized = True,label='SDSS rz mock', color='forestgreen'  , cumulative=True, density=True, lw=2)
	p.hist(sdss_rz_mock, bins=color_bins, histtype = 'step', rasterized = True, color='forestgreen', density=True, lw=2)
	# Legacy Survey
	p.hist(LS_rz_mock, bins=color_bins, histtype = 'step', rasterized = True,label='LS rz mock', color='y', cumulative=True, density=True, lw=2)
	out = p.hist(LS_rz_mock, bins=color_bins, histtype = 'step', rasterized = True, color='y', density=True, lw=2)

	RM_selection = (RM_redshift>zmin)&(RM_redshift<zmax)
	if len(RM_gr[RM_selection])>0:
		p.hist(RM_rz[RM_selection], bins=color_bins, histtype = 'step', rasterized = True,label='LS rz data', color='k', cumulative=True, density=True, lw=2)
		p.hist(RM_rz[RM_selection], bins=color_bins, histtype = 'step', rasterized = True,color='k',  density=True, lw=2)

	RM_selection_2 = (zz_sdss_data>zmin)&(zz_sdss_data<zmax)
	if len(gr_sdss_data[RM_selection_2])>0:
		p.hist(rz_sdss_data[RM_selection_2], bins=color_bins, histtype = 'step', rasterized = True,label='SDSS rz data', color='b', cumulative=True, density=True, lw=2)
		p.hist(rz_sdss_data[RM_selection_2], bins=color_bins, histtype = 'step', rasterized = True, color='b',  density=True, lw=2)

	#p.axvline(ri_RS_itp(mean_z), label='ri model', color='green', ls='dotted')
	#p.axvline(ri_RS_itp(mean_z) + ri_sigma_RS_itp(mean_z), color='green', ls='dashed')
	#p.axvline(ri_RS_itp(mean_z) - ri_sigma_RS_itp(mean_z), color='green', ls='dashed')

	#p.axvline(iz_RS_itp(mean_z), label='iz model', color='red', ls='dotted')
	#p.axvline(iz_RS_itp(mean_z) + iz_sigma_RS_itp(mean_z), color='m', ls='dashed')
	#p.axvline(iz_RS_itp(mean_z) - iz_sigma_RS_itp(mean_z), color='m', ls='dashed')

	p.axvline(LS_rz_RS_itp(mean_z), label='LS rz model', color='m', ls='dotted')
	p.axvline(LS_rz_RS_itp(mean_z) + LS_rz_sigma_RS_itp(mean_z), color='m', ls='dashed')
	p.axvline(LS_rz_RS_itp(mean_z) - LS_rz_sigma_RS_itp(mean_z), color='m', ls='dashed')
	
	p.xlim((n.min(out[1][1:][out[0] > 0]) - 0.05, n.max(out[1][1:][out[0] > 0]) + 0.05))
	p.legend(frameon=False)
	p.xlabel('color r-z')
	p.ylabel('probability distribution function')
	p.grid()
	p.title('z='+str(n.round(z_snap,2)))
	p.savefig(fig_out)
	p.clf()

	# opti0nal red sequence figure
	fig_out = os.path.join(fig_dir, 'color_color_gr_rz_'+aexp_str+'.png')
	p.figure(2, (6., 6.))
	fig, ax = p.subplots()
	#ax.plot(sdss_gr_mock,    sdss_rz_mock,                                'k+', rasterized = True, label='SDSS mock')
	ax.plot(RM_gr[RM_selection], RM_rz[RM_selection], 'r+', alpha = 0.5, rasterized = True, label='LS data')
	#ax.plot(gr_sdss_data[RM_selection_2], rz_sdss_data[RM_selection_2], 'g+', alpha = 0.5, rasterized = True, label='SDSS data')
	ax.plot(LS_gr_mock, LS_rz_mock,                             'k+', alpha = 0.1, rasterized = True, label='LS mock')

	p.axhline(LS_rz_RS_itp(mean_z), label='LS rz model', color='b', ls='dotted')
	p.axhline(LS_rz_RS_itp(mean_z) + LS_rz_sigma_RS_itp(mean_z), color='b', ls='dashed')
	p.axhline(LS_rz_RS_itp(mean_z) - LS_rz_sigma_RS_itp(mean_z), color='b', ls='dashed')

	p.axvline(LS_gr_RS_itp(mean_z), label='LS gr model', color='m', ls='dotted')
	p.axvline(LS_gr_RS_itp(mean_z) + LS_gr_sigma_RS_itp(mean_z), color='m', ls='dashed')
	p.axvline(LS_gr_RS_itp(mean_z) - LS_gr_sigma_RS_itp(mean_z), color='m', ls='dashed')
	
	#p.axvline(gr_RS_itp(mean_z), label='gr model', color='blue', ls='dotted')
	#p.axvline(gr_RS_itp(mean_z) + gr_sigma_RS_itp(mean_z), color='blue', ls='dashed')
	#p.axvline(gr_RS_itp(mean_z) - gr_sigma_RS_itp(mean_z), color='blue', ls='dashed')

	#p.axvline(ri_RS_itp(mean_z), label='ri model', color='green', ls='dotted')
	#p.axvline(ri_RS_itp(mean_z) + ri_sigma_RS_itp(mean_z), color='green', ls='dashed')
	#p.axvline(ri_RS_itp(mean_z) - ri_sigma_RS_itp(mean_z), color='green', ls='dashed')

	#p.axvline(iz_RS_itp(mean_z), label='iz model', color='red', ls='dotted')
	#p.axvline(iz_RS_itp(mean_z) + iz_sigma_RS_itp(mean_z), color='red', ls='dashed')
	#p.axvline(iz_RS_itp(mean_z) - iz_sigma_RS_itp(mean_z), color='red', ls='dashed')
	
	#p.errorbar(redshift_RS, gr_RS, xerr=0.05, yerr=gr_sigma_RS, label='g-r')
	#p.errorbar(redshift_RS, ri_RS, xerr=0.05, yerr=ri_sigma_RS, label='r-i')
	#p.errorbar(redshift_RS, iz_RS, xerr=0.05, yerr=iz_sigma_RS, label='i-z')
	ax.legend(frameon=True)
	ax.set_title('z='+str(n.round(z_snap,2)))
	ax.set_xlabel('g-r')
	ax.set_ylabel('r-z')
	ax.grid()
	xmin, xmax = n.min(LS_gr_mock) - 0.1, n.max(LS_gr_mock) + 0.1
	ymin, ymax = n.min(LS_rz_mock) - 0.1, n.max(LS_rz_mock) + 0.1
	print(xmin, xmax)
	print(ymin, ymax)
	ax.set_xlim(( xmin, xmax ))
	ax.set_ylim(( ymin, ymax ))
	# p.yscale('log')
	# p.xscale('log')
	p.savefig(fig_out)
	p.clf()

