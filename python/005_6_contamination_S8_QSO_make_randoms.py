import numpy as n
import os
import sys
from astropy.table import Table, Column

root_dir  = '/data42s/comparat/firefly/mocks/2021-04/QMOST'
dir_2_RDS = os.path.join(root_dir, 'QMOST_random_ra-dec-50perdeg2.fits' )

density = 50 # deg-2
area = 129600/n.pi
size = int(density*area)
uu = n.random.uniform(size=size)
dec = n.arccos(1 - 2 * uu) * 180 / n.pi - 90.
ra = n.random.uniform(size=size) * 2 * n.pi * 180 / n.pi

t = Table()
t['RA'] = Column(ra, unit='degree', dtype=n.float64)
t['DEC'] = Column(dec, unit='degree', dtype=n.float64)
t.write(dir_2_RDS, overwrite=True)

command = 'python 005_4_add_footprint.py '+dir_2_RDS
os.system(command)

# select random 50/deg2 in the QSO mock to get all the data
# replace RA, DEC by random positions

