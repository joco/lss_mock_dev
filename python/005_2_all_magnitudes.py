"""
What it does
------------

Add realistic magnitudes to BG, LRG and FIL (ELG deprecated)

Command to run
--------------

python3 005_2_all_magnitudes.py environmentVAR 

"""

#import matplotlib
#matplotlib.use('Agg')
#matplotlib.rcParams.update({'font.size': 14})
#import matplotlib.pyplot as p
from sklearn.neighbors import BallTree

from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
import extinction

from astropy_healpix import healpy
import sys
import os
import time
from scipy.interpolate import interp1d
from scipy.stats import norm
from astropy.table import Table, Column
from scipy.optimize import curve_fit
import astropy.io.fits as fits
import h5py
import numpy as n
print('CREATES FITS FILES')
print('------------------------------------------------')
print('------------------------------------------------')
t0 = time.time()

doBG = True
doLRG = True
doFILAMENT = True
doELG = True
doFHS = True

doBG = False
doLRG = False
doFILAMENT = False
doELG = False
#doFHS = False

env = sys.argv[1]  # 'UNIT_fA1i_DIR'
#HEALPIX_id = int(sys.argv[2])
#print(env, HEALPIX_id)

if env[:2] == "MD" : # env == "MD04" or env == "MD40" or env == "MD10" or env == "MD25"
    cosmoMD = FlatLambdaCDM(
        H0=67.77 * u.km / u.s / u.Mpc,
        Om0=0.307115)  # , Ob0=0.048206)
    h = 0.6777
    cosmo = cosmoMD
if env[:4] == "UNIT" : # == "UNIT_fA1_DIR" or env == "UNIT_fA1i_DIR" or env == "UNIT_fA2_DIR":
    cosmoUNIT = FlatLambdaCDM(H0=67.74 * u.km / u.s / u.Mpc, Om0=0.308900)
    h = 0.6774
    L_box = 1000.0 / h
    cosmo = cosmoUNIT

nl = lambda sel : len(sel.nonzero()[0])
zs = n.arange(0.0000001, 7.1, 0.001)
dm_itp = interp1d(zs, cosmo.distmod(zs).value)

root_dir = os.path.join(os.environ[env])

max_distance = 0.1 / 3600. * n.pi / 180.
deg_to_rad = n.pi/180.

# Match to BCG and GAL catalogues 
print('load and match to BCG and GAL catalogues')
path_2_out_GAL = os.path.join(root_dir, env + '_eRO_CLU_b8_CM_0_pixS_20.0_4MOST_GAL.fits')
path_2_out_BCG = os.path.join(root_dir, env + '_eRO_CLU_b8_CM_0_pixS_20.0_4MOST_BCG.fits')
t_GAL = Table.read(path_2_out_GAL)
t_BCG = Table.read(path_2_out_BCG)
coord_GAL = deg_to_rad * n.transpose([t_GAL['DEC'], t_GAL['RA'] ])
coord_BCG = deg_to_rad * n.transpose([t_BCG['DEC'], t_BCG['RA'] ])

Tree_GAL = BallTree(coord_GAL, metric='haversine')
Tree_BCG = BallTree(coord_BCG, metric='haversine')

# Match to AGN catalogues 
print('load and match to AGN catalogues')
agn = Table.read('/data42s/comparat/firefly/mocks/2021-04/QMOST/S6_4MOST_ALL_SNR3_IR215.fit.gz')
qso = Table.read('/data42s/comparat/firefly/mocks/2021-04/QMOST/QSO_4MOST.fits.gz')

coord_AGN = deg_to_rad * n.transpose([agn['DEC'], agn['RA'] ])
coord_QSO = deg_to_rad * n.transpose([qso['DEC'], qso['RA'] ])

Tree_AGN = BallTree(coord_AGN, metric='haversine')
Tree_QSO = BallTree(coord_QSO, metric='haversine')


path_2_kids = os.path.join(os.environ['MD10'], '4most_s8_kidsdr4.lsdr9.fits') 
t_kids = Table.read(path_2_kids)
dup_type = (t_kids['lsdr9_TYPE']=="DUP" )
dash_type = (t_kids['lsdr9_TYPE']=="-" ) | (t_kids['lsdr9_TYPE']=='-  ')
good_phot = ( t_kids['rtot']>0 ) & (t_kids['gr'] > -10 ) & (t_kids['ri'] > -10 )  & (t_kids['iz'] > -10 )  & (t_kids['zy'] > -10 )  & (t_kids['yj'] > -10 )  & (t_kids['jh'] > -10 )  & (t_kids['hks'] > -10 ) & (t_kids['zphot']>0)
keep_kids = (dup_type==False) & (dash_type==False) & (good_phot)
t_kids = t_kids[keep_kids]
print(len(t_kids),'lines in KIDS catalogue')
print('unique (lsdr9_TYPE) = ',n.unique(t_kids['lsdr9_TYPE']))

s_kids_fil = (t_kids['zphot']<=0.9) & (t_kids['zphot']>=0.) 
t_kids_fil = t_kids[s_kids_fil]
kids_k_mag = t_kids_fil['rtot']-(t_kids_fil['ri']+t_kids_fil['iz']+t_kids_fil['zy']+t_kids_fil['yj']+t_kids_fil['jh']+t_kids_fil['hks'])
kids_photoz = t_kids_fil['zphot']
Tree_kids_fil = BallTree(n.transpose([kids_photoz, kids_k_mag]))

s_kids_bg = (t_kids['iss8bg']) & (t_kids['zphot']<=0.9) & (t_kids['zphot']>=0.) 
t_kids_bg = t_kids[s_kids_bg]
kids_k_mag = t_kids_bg['rtot']-(t_kids_bg['ri']+t_kids_bg['iz']+t_kids_bg['zy']+t_kids_bg['yj']+t_kids_bg['jh']+t_kids_bg['hks'])
kids_photoz = t_kids_bg['zphot']
Tree_kids_bg = BallTree(n.transpose([kids_photoz, kids_k_mag]))

s_kids_lrg = (t_kids['iss8lrg']) & (t_kids['zphot']<=1.2) & (t_kids['zphot']>=0.) 
t_kids_lrg = t_kids[s_kids_lrg]
kids_k_mag = t_kids_lrg['rtot']-(t_kids_lrg['ri']+t_kids_lrg['iz']+t_kids_lrg['zy']+t_kids_lrg['yj']+t_kids_lrg['jh']+t_kids_lrg['hks'])
kids_photoz = t_kids_lrg['zphot']
Tree_kids_lrg = BallTree(n.transpose([kids_photoz, kids_k_mag]))

sub_survey_names = n.array([ 'BG', 'LRG', 'ELG', 'QSO', 'LyA', 'filament_GAL'])

N_subsurvey = {'BG':1, 'filament_GAL':3, 'LRG':2, 'ELG':3, 'QSO':4, 'LyA':5}
priority_values = {'BG':100, 'filament_GAL':80, 'LRG':99, 'ELG':80, 'QSO':97, 'LyA':98}

# reassigns templates correctly
z_all = n.hstack(( 0., n.arange(0.3, 3., 0.2), 3.5, 4.5, 6. ))
zmins = z_all[:-1]
zmaxs = z_all[1:]

plotDir = os.path.join(os.environ['GIT_AGN_MOCK'], 'data', 'logMs-MK-fits')
dir_2_OUT = os.path.join(root_dir, "cat_SHAM_COSMO")

def add_all_MAG(HEALPIX_id):
	#HEALPIX_id = 365 
	def add_magnitudes_direct_match(t_survey, flag = '', t_kids2=t_kids_lrg, Tree_kids=Tree_kids_lrg):
		# simulated quantities
		sim_redshift = t_survey['redshift_R']
		sim_k_mag = t_survey['K_mag_abs']+dm_itp(t_survey['redshift_R'])
		DATA = n.transpose([sim_redshift, sim_k_mag])
		# search nearest neighbour in tree
		ids_out = Tree_kids.query(DATA, k=1, return_distance = False)
		kids_ID = n.arange(len(t_kids2['rtot']))
		ids = n.hstack((ids_out))
		id_to_map = kids_ID[ids]
		columns_to_add = n.array(['rtot'          
									, 'rfib'          
									, 'ug'            
									, 'gr'            
									, 'ri'            
									, 'iz'            
									, 'zy'            
									, 'yj'            
									, 'jh'            
									, 'hks'           
									, 'lsdr9_TYPE'    
									, 'lsdr9_RA'      
									, 'lsdr9_DEC'     
									, 'lsdr9_SHAPE_R' 
									, 'lsdr9_SHAPE_E1'
									, 'lsdr9_SHAPE_E2'
									, 'lsdr9_SERSIC'  
									, 'lsdr9_MASKBITS'])
		print('outputing results')
		for el in columns_to_add :
			if el in t_survey.columns :
				t_survey[el] = t_kids2[el][id_to_map]
			else:
				t_survey.add_column(Column(name=el, data=t_kids2[el][id_to_map]))
		return t_survey #.write(path_2_out)
	
	def add_4most_columns(subsurvey , t_survey  ):
		#subsurvey = 'BG'
		#t_survey = t_bg 
		#subsurvey = 'LRG'
		#t_survey = t_lrg 
		#subsurvey = 'ELG'
		#t_survey = t_elg 
		#subsurvey = 'QSO'
		#t_survey = t_qso 
		#subsurvey = 'Lya'
		#t_survey = t_lya 
		#subsurvey = 'filament_GAL'
		#t_survey = t_bgS5 
		N_obj = len(t_survey)
		#  limit size of the string columns to the size of the longer string in the corresponding columns. 
		# 'NAME':str, max 256 char
		N1 = n.arange(len(t_survey['ebv']))
		id_list = N_subsurvey[subsurvey]*1e10 + HEALPIX_id*1e6 +  N1
		NAME = n.array([ str(int(el)).zfill(11) for el in id_list ])
		t_survey.add_column(Column(name='NAME', data=NAME, unit=''))
		# 'RA':n.float64, 1D
		# 'DEC':n.float64, 1D
		# 'PMRA':n.float32, 1E
		# 'PMDEC':n.float32, 1E
		# 'EPOCH':n.float32, 1E
		PMRA = n.zeros(N_obj)
		t_survey.add_column(Column(name='PMRA', data=PMRA, unit='mas/yr'))
		PMDEC = n.zeros(N_obj)
		t_survey.add_column(Column(name='PMDEC', data=PMDEC, unit='mas/yr'))
		EPOCH = n.ones(N_obj)*2000.
		t_survey.add_column(Column(name='EPOCH', data=EPOCH, unit='yr'))
		# 'RESOLUTION':n.int16, 1I
		RESOLUTION = n.ones(N_obj).astype('int')
		t_survey.add_column(Column(name='RESOLUTION', data=RESOLUTION, unit=''))
		# 'SUBSURVEY':str, max 256 char
		SUBSURVEY = n.ones(N_obj).astype('str')
		SUBSURVEY[:] = subsurvey
		t_survey.add_column(Column(name='SUBSURVEY', data=SUBSURVEY, unit=''))
		# 'PRIORITY':n.int16, 1I
		PRIORITY = n.zeros(N_obj).astype('int') + priority_values[subsurvey]
		t_survey.add_column(Column(name='PRIORITY', data=PRIORITY, unit=''))
		# EBV for templates
		ebv_1000 = (t_survey['ebv']*1000).astype('int')
		#print('EBV', n.min(ebv_1000), n.max(ebv_1000))
		ebv_1_0 = ( ebv_1000 > 1000 ) 
		ebv_0_5 = ( ebv_1000 > 500 ) & ( ebv_1000 <= 1000 ) 
		ebv_0_4 = ( ebv_1000 > 400 ) & ( ebv_1000 <= 500 ) 
		ebv_0_3 = ( ebv_1000 > 300 ) & ( ebv_1000 <= 400 ) 
		ebv_0_2 = ( ebv_1000 > 200 ) & ( ebv_1000 <= 300 ) 
		ebv_0_1 = ( ebv_1000 > 100 ) & ( ebv_1000 <= 200 ) 
		ebv_0_0 = ( ebv_1000 <= 100 ) 
		z_name = lambda z0, z1 : "_zmin_"+str(int(10*z0)).zfill(2)+"_zmax_"+str(int(10*z1)).zfill(2)
		# templates
		template_names = n.zeros(N_obj).astype('U100')
		ruleset_array = n.zeros(N_obj).astype('str')
		# S8 BG or LRG
		if subsurvey == 'BG' or subsurvey == 'LRG':
			ruleset_array[:] = "COSMO_RedGAL"
			for z0,z1 in zip(zmins,zmaxs):
				zsel = (t_survey['redshift_R']>=z0) & (t_survey['redshift_R']<z1)
				if len(zsel.nonzero()[0])>0:
					#ruleset_array[zsel] = "COSMO_RedGAL"
					template_names[(zsel)]               = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
					template_names[(zsel)&(ebv_0_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
					template_names[(zsel)&(ebv_0_1)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_1.fits'   
					template_names[(zsel)&(ebv_0_2)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_2.fits'   
					template_names[(zsel)&(ebv_0_3)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_3.fits'   
					template_names[(zsel)&(ebv_0_4)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_4.fits'   
					template_names[(zsel)&(ebv_0_5)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_5.fits'   
					template_names[(zsel)&(ebv_1_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_1_0.fits'   
		# S5 filament gal
		if subsurvey == 'filament_GAL' :
			ruleset_array[:] = "RedGAL"
			for z0,z1 in zip(zmins,zmaxs):
				zsel = (t_survey['redshift_R']>=z0) & (t_survey['redshift_R']<z1)
				if len(zsel.nonzero()[0])>0:
					#ruleset_array[zsel] = "RedGAL"
					template_names[(zsel)]               = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
					template_names[(zsel)&(ebv_0_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
					template_names[(zsel)&(ebv_0_1)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_1.fits'   
					template_names[(zsel)&(ebv_0_2)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_2.fits'   
					template_names[(zsel)&(ebv_0_3)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_3.fits'   
					template_names[(zsel)&(ebv_0_4)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_4.fits'   
					template_names[(zsel)&(ebv_0_5)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_5.fits'   
					template_names[(zsel)&(ebv_1_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_1_0.fits'   
		# S8 ELG
		if subsurvey == 'ELG' :
			ruleset_array[:] = "ELG"
			for z0,z1 in zip(zmins,zmaxs):
				zsel = (t_survey['redshift_R']>=z0)&(t_survey['redshift_R']<z1)
				if len(zsel.nonzero()[0])>0:
					#ruleset_array[zsel] = "ELG"
					template_names[(zsel)]               = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_01.fits' 
					template_names[(zsel)&(ebv_0_0)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_01.fits' 
					template_names[(zsel)&(ebv_0_1)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_1.fits'  
					template_names[(zsel)&(ebv_0_2)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_2.fits'  
					template_names[(zsel)&(ebv_0_3)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_3.fits'  
					template_names[(zsel)&(ebv_0_4)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_4.fits'  
					template_names[(zsel)&(ebv_0_5)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_5.fits'  
					template_names[(zsel)&(ebv_1_0)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_1_0.fits'  

		# 'TEMPLATE':str, max 256 char
		t_survey.add_column(Column(name='TEMPLATE', data=template_names, unit=''))
		# 'RULESET':str, max 256 char
		t_survey.add_column(Column(name='RULESET', data=ruleset_array, unit=''))
		# 'REDSHIFT_ESTIMATE':n.float32, 1E
		# 'REDSHIFT_ERROR':n.float32, 1E
		t_survey.add_column(Column(name='REDSHIFT_ESTIMATE', data=t_survey['redshift_R'], unit=''))
		t_survey.add_column(Column(name='REDSHIFT_ERROR', data=n.ones(N_obj), unit=''))
		# 'MAG':n.float32,
		# 'MAG_ERR':n.float32
		# 'MAG_TYPE': str max 256 char
		r_v=3.1
		a_v = t_survey['ebv'] * r_v
		delta_mag = n.hstack(( n.array([ extinction.fitzpatrick99(n.array([9000.]), el, r_v=3.1, unit='aa') for el in a_v ]) ))
		print(n.median(t_survey['ebv']), n.median(delta_mag))
		#rv = av/ebv
		#av = rv x ebv
		extincted_mag = t_survey['rtot']-t_survey['ri']-t_survey['iz'] + delta_mag
		t_survey.add_column(Column(name='MAG', data=extincted_mag, unit='mag'))
		t_survey.add_column(Column(name='MAG_ERR', data=0.01 * n.ones(N_obj), unit='mag'))
		MAG_TYPE = n.ones(N_obj).astype('str')
		MAG_TYPE[:] = 'DECam_z_AB'
		t_survey.add_column(Column(name='MAG_TYPE', data=MAG_TYPE, unit=''))
		# 'REDDENING':n.float32, 1E
		t_survey.add_column(Column(name='REDDENING',data=t_survey['ebv'], unit='mag'))
		# 'DATE_EARLIEST':n.float64, JulianDate decimal days # 01-Nov-2022
		# 'DATE_LATEST':n.float64, JulianDate decimal days # 02-Feb-2033
		t_survey.add_column(Column(name='DATE_EARLIEST',data=2459945.5 * n.ones(N_obj), unit='d'))
		t_survey.add_column(Column(name='DATE_LATEST'  ,data=2462501.5 * n.ones(N_obj), unit='d'))
		CADENCE = n.zeros(N_obj).astype('int')
		t_survey.add_column(Column(name='CADENCE', data=CADENCE, unit=''))
		# extent flags and parameters
		# 'EXTENT_FLAG': 1I
		# =1
		# 'EXTENT_PARAMETER': 1E
		# =0
		# 'EXTENT_INDEX': 1E
		# =0
		t_survey.add_column(Column(name='EXTENT_FLAG'     , data=n.ones(N_obj).astype('int') , unit=''))
		t_survey.add_column(Column(name='EXTENT_PARAMETER', data=n.zeros(N_obj), unit=''))
		t_survey.add_column(Column(name='EXTENT_INDEX'    , data=n.zeros(N_obj), unit=''))
		print('assigns magnitudes and sersic types')
		phot_types = n.unique(t_survey['lsdr9_TYPE'])  
		print('LS types', phot_types)
		sel_DEV = (t_survey['lsdr9_TYPE'] == "DEV")
		sel_EXP = (t_survey['lsdr9_TYPE'] == "EXP")
		sel_PSF = (t_survey['lsdr9_TYPE'] == "PSF")
		sel_REX = (t_survey['lsdr9_TYPE'] == "REX")
		sel_SER = (t_survey['lsdr9_TYPE'] == "SER")
		t_survey['EXTENT_FLAG'][sel_PSF] = 0
		t_survey['EXTENT_FLAG'][sel_DEV] = 2
		t_survey['EXTENT_FLAG'][sel_EXP] = 2
		t_survey['EXTENT_FLAG'][sel_REX] = 2
		t_survey['EXTENT_FLAG'][sel_SER] = 2
		#
		t_survey['EXTENT_PARAMETER'][sel_PSF] = 0
		t_survey['EXTENT_PARAMETER'][sel_DEV] = t_survey['lsdr9_SHAPE_R'][sel_DEV]
		t_survey['EXTENT_PARAMETER'][sel_EXP] = t_survey['lsdr9_SHAPE_R'][sel_EXP]
		t_survey['EXTENT_PARAMETER'][sel_REX] = t_survey['lsdr9_SHAPE_R'][sel_REX]
		t_survey['EXTENT_PARAMETER'][sel_SER] = t_survey['lsdr9_SHAPE_R'][sel_SER]
		#
		print('min extent param', t_survey['EXTENT_PARAMETER'][sel_PSF == False].min() )
		t_survey['EXTENT_PARAMETER'] [ ( t_survey['EXTENT_PARAMETER']<0.1 ) & (sel_PSF == False ) ] = 0.101
		#
		t_survey['EXTENT_INDEX'][sel_PSF] = 0
		t_survey['EXTENT_INDEX'][sel_DEV] = 4
		t_survey['EXTENT_INDEX'][sel_EXP] = 1
		t_survey['EXTENT_INDEX'][sel_REX] = 1
		t_survey['EXTENT_INDEX'][sel_SER] = t_survey['lsdr9_SERSIC'][sel_SER]
		print('min index', t_survey['EXTENT_INDEX'][sel_PSF == False].min() )
		return t_survey


	def update_4most_columns(subsurvey , t_survey  ):
		#subsurvey = 'BG'
		#t_survey = t_bg 
		#subsurvey = 'LRG'
		#t_survey = t_lrg 
		#subsurvey = 'ELG'
		#t_survey = t_elg             
		#subsurvey = 'QSO'
		#t_survey = t_qso 
		#subsurvey = 'Lya'
		#t_survey = t_lya 
		#subsurvey = 'filament_GAL'
		#t_survey = t_bgS5 
		N_obj = len(t_survey)
		#  limit size of the string columns to the size of the longer string in the corresponding columns. 
		# 'NAME':str, max 256 char
		N1 = n.arange(len(t_survey['ebv']))
		id_list = N_subsurvey[subsurvey]*1e10 + HEALPIX_id*1e6 +  N1
		NAME = n.array([ str(int(el)).zfill(11) for el in id_list ])
		t_survey['NAME'] = NAME
		PMRA = n.zeros(N_obj)
		t_survey['PMRA'] = PMRA
		PMDEC = n.zeros(N_obj)
		t_survey['PMDEC'] = PMDEC
		EPOCH = n.ones(N_obj)*2000.
		t_survey['EPOCH'] = EPOCH
		# 'RESOLUTION':n.int16, 1I
		RESOLUTION = n.ones(N_obj).astype('int')
		t_survey['RESOLUTION'] = RESOLUTION
		# 'SUBSURVEY':str, max 256 char
		SUBSURVEY = n.ones(N_obj).astype('str')
		SUBSURVEY[:] = subsurvey
		t_survey['SUBSURVEY'] = SUBSURVEY
		# 'PRIORITY':n.int16, 1I
		PRIORITY = n.zeros(N_obj).astype('int') + priority_values[subsurvey]
		t_survey['PRIORITY'] = PRIORITY
		# EBV for templates
		ebv_1000 = (t_survey['ebv']*1000).astype('int')
		#print('EBV', n.min(ebv_1000), n.max(ebv_1000))
		ebv_1_0 = ( ebv_1000 > 1000 ) 
		ebv_0_5 = ( ebv_1000 > 500 ) & ( ebv_1000 <= 1000 ) 
		ebv_0_4 = ( ebv_1000 > 400 ) & ( ebv_1000 <= 500 ) 
		ebv_0_3 = ( ebv_1000 > 300 ) & ( ebv_1000 <= 400 ) 
		ebv_0_2 = ( ebv_1000 > 200 ) & ( ebv_1000 <= 300 ) 
		ebv_0_1 = ( ebv_1000 > 100 ) & ( ebv_1000 <= 200 ) 
		ebv_0_0 = ( ebv_1000 <= 100 ) 
		z_name = lambda z0, z1 : "_zmin_"+str(int(10*z0)).zfill(2)+"_zmax_"+str(int(10*z1)).zfill(2)
		# templates
		template_names = n.zeros(N_obj).astype('U100')
		ruleset_array = n.zeros(N_obj).astype('str')
		# S8 BG or LRG
		if subsurvey == 'BG' or subsurvey == 'LRG':
			ruleset_array[:] = "COSMO_RedGAL"
			for z0,z1 in zip(zmins,zmaxs):
				zsel = (t_survey['redshift_R']>=z0) & (t_survey['redshift_R']<z1)
				if len(zsel.nonzero()[0])>0:
					#ruleset_array[zsel] = "COSMO_RedGAL"
					template_names[(zsel)]               = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
					template_names[(zsel)&(ebv_0_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
					template_names[(zsel)&(ebv_0_1)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_1.fits'   
					template_names[(zsel)&(ebv_0_2)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_2.fits'   
					template_names[(zsel)&(ebv_0_3)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_3.fits'   
					template_names[(zsel)&(ebv_0_4)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_4.fits'   
					template_names[(zsel)&(ebv_0_5)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_5.fits'   
					template_names[(zsel)&(ebv_1_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_1_0.fits'   
		# S5 filament gal
		if subsurvey == 'filament_GAL' :
			ruleset_array[:] = "RedGAL"
			for z0,z1 in zip(zmins,zmaxs):
				zsel = (t_survey['redshift_R']>=z0) & (t_survey['redshift_R']<z1)
				if len(zsel.nonzero()[0])>0:
					#ruleset_array[zsel] = "RedGAL"
					template_names[(zsel)]               = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
					template_names[(zsel)&(ebv_0_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_01.fits'  
					template_names[(zsel)&(ebv_0_1)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_1.fits'   
					template_names[(zsel)&(ebv_0_2)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_2.fits'   
					template_names[(zsel)&(ebv_0_3)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_3.fits'   
					template_names[(zsel)&(ebv_0_4)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_4.fits'   
					template_names[(zsel)&(ebv_0_5)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_0_5.fits'   
					template_names[(zsel)&(ebv_1_0)]     = "4most_"+'LRG'+z_name( z0, z1)+'_EBV_1_0.fits'   
		# S8 ELG
		if subsurvey == 'ELG' :
			ruleset_array[:] = "ELG"
			for z0,z1 in zip(zmins,zmaxs):
				zsel = (t_survey['redshift_R']>=z0)&(t_survey['redshift_R']<z1)
				if len(zsel.nonzero()[0])>0:
					#ruleset_array[zsel] = "ELG"
					template_names[(zsel)]               = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_01.fits' 
					template_names[(zsel)&(ebv_0_0)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_01.fits' 
					template_names[(zsel)&(ebv_0_1)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_1.fits'  
					template_names[(zsel)&(ebv_0_2)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_2.fits'  
					template_names[(zsel)&(ebv_0_3)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_3.fits'  
					template_names[(zsel)&(ebv_0_4)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_4.fits'  
					template_names[(zsel)&(ebv_0_5)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_0_5.fits'  
					template_names[(zsel)&(ebv_1_0)]     = "4most_"+'ELG'+z_name( z0, z1)+'_EBV_1_0.fits'  

		# 'TEMPLATE':str, max 256 char
		t_survey['TEMPLATE'] = template_names
		# 'RULESET':str, max 256 char
		t_survey['RULESET'] = ruleset_array
		# 'REDSHIFT_ESTIMATE':n.float32, 1E
		# 'REDSHIFT_ERROR':n.float32, 1E
		t_survey['REDSHIFT_ESTIMATE'] = t_survey['redshift_R']
		t_survey['REDSHIFT_ERROR'] = n.ones(N_obj)
		# 'MAG':n.float32,
		# 'MAG_ERR':n.float32
		# 'MAG_TYPE': str max 256 char
		r_v=3.1
		a_v = t_survey['ebv'] * r_v
		delta_mag = n.hstack(( n.array([ extinction.fitzpatrick99(n.array([9000.]), el, r_v=3.1, unit='aa') for el in a_v ]) ))
		print(n.median(t_survey['ebv']), n.median(delta_mag))
		#rv = av/ebv
		#av = rv x ebv
		extincted_mag = t_survey['rtot']-t_survey['ri']-t_survey['iz'] + delta_mag
		t_survey['MAG'] = extincted_mag
		t_survey['MAG_ERR'] = 0.01 * n.ones(N_obj)
		MAG_TYPE = n.ones(N_obj).astype('str')
		MAG_TYPE[:] = 'DECam_z_AB'
		t_survey['MAG_TYPE'] = MAG_TYPE
		# 'REDDENING':n.float32, 1E
		t_survey['REDDENING'] = t_survey['ebv']
		# 'DATE_EARLIEST':n.float64, JulianDate decimal days # 01-Nov-2022
		# 'DATE_LATEST':n.float64, JulianDate decimal days # 02-Feb-2033
		t_survey['DATE_EARLIEST'] = 2459945.5 * n.ones(N_obj)
		t_survey['DATE_LATEST'  ] = 2462501.5 * n.ones(N_obj)
		t_survey['CADENCE'  ] = n.zeros(N_obj).astype('int')
		# 'EXTENT_FLAG': 1I
		# =1
		# 'EXTENT_PARAMETER': 1E
		# =0
		# 'EXTENT_INDEX': 1E
		# =0
		t_survey['EXTENT_FLAG'     ] = n.zeros(N_obj).astype('int') 
		t_survey['EXTENT_PARAMETER'] = n.zeros(N_obj)
		t_survey['EXTENT_INDEX'    ] = n.zeros(N_obj)
		print('assigns magnitudes and sersic types')
		phot_types = n.unique(t_survey['lsdr9_TYPE'])  
		print('LS types', phot_types)
		sel_DEV = (t_survey['lsdr9_TYPE'] == "DEV")
		sel_EXP = (t_survey['lsdr9_TYPE'] == "EXP")
		sel_PSF = (t_survey['lsdr9_TYPE'] == "PSF")
		sel_REX = (t_survey['lsdr9_TYPE'] == "REX")
		sel_SER = (t_survey['lsdr9_TYPE'] == "SER")
		# extent flags and parameters
		t_survey['EXTENT_FLAG'][sel_PSF] = 0
		t_survey['EXTENT_FLAG'][sel_DEV] = 2
		t_survey['EXTENT_FLAG'][sel_EXP] = 2
		t_survey['EXTENT_FLAG'][sel_REX] = 2
		t_survey['EXTENT_FLAG'][sel_SER] = 2
		#
		t_survey['EXTENT_PARAMETER'][sel_PSF] = 0
		t_survey['EXTENT_PARAMETER'][sel_DEV] = t_survey['lsdr9_SHAPE_R'][sel_DEV]
		t_survey['EXTENT_PARAMETER'][sel_EXP] = t_survey['lsdr9_SHAPE_R'][sel_EXP]
		t_survey['EXTENT_PARAMETER'][sel_REX] = t_survey['lsdr9_SHAPE_R'][sel_REX]
		t_survey['EXTENT_PARAMETER'][sel_SER] = t_survey['lsdr9_SHAPE_R'][sel_SER]
		#
		print('min extent param', t_survey['EXTENT_PARAMETER'][sel_PSF == False].min() )
		t_survey['EXTENT_PARAMETER'] [ ( t_survey['EXTENT_PARAMETER']<0.1 ) & (sel_PSF == False ) ] = 0.101
		#
		t_survey['EXTENT_INDEX'][sel_PSF] = 0
		t_survey['EXTENT_INDEX'][sel_DEV] = 4
		t_survey['EXTENT_INDEX'][sel_EXP] = 1
		t_survey['EXTENT_INDEX'][sel_REX] = 1
		t_survey['EXTENT_INDEX'][sel_SER] = t_survey['lsdr9_SERSIC'][sel_SER]
		print('min index', t_survey['EXTENT_INDEX'][sel_PSF == False].min() )
		return t_survey

	#def add_magnitudes(t_survey, flag = 'iselg', t_kids_i=t_kids):
		## normalize simulated arrays to 0-1
		## need to renormalize SFR values with inf or nan
		#sim_redshift = t_survey['redshift_R']
		#sfr_value = t_survey['galaxy_star_formation_rate']
		#sfr_0 = (sfr_value==0)
		#min_sfr = n.min(sfr_value[sfr_0 == False])
		#sfr_value[sfr_0] = min_sfr
		#sim_sfr = (sfr_value - sfr_value.min() ) / ( sfr_value.max() - sfr_value.min() )
		#NN, bb = n.histogram(sim_sfr, bins=1000)
		#cdf_itp = interp1d(bb, n.hstack((0,n.cumsum(NN)))/n.sum(NN) )
		#sim_sfr_0_1 = cdf_itp(sim_sfr)

		#sim_k_mag_i = t_survey['K_mag_abs']+dm_itp(t_survey['redshift_R'])
		#sim_k_mag = (sim_k_mag_i - sim_k_mag_i.min()) / (sim_k_mag_i.max() - sim_k_mag_i.min())
		#NN, bb = n.histogram(sim_k_mag, bins=1000)
		#cdf_itpK = interp1d(bb, n.hstack((0,n.cumsum(NN)))/n.sum(NN) )
		#sim_k_mag_0_1 = cdf_itpK(sim_k_mag)
		## normalize data arrays to the min max of the simulation
		#if flag =="":
			#s_kids = (t_kids_i['zphot']<=n.max(sim_redshift)) & (t_kids_i['zphot']>=n.min(sim_redshift)) 
		#else:
			#s_kids = (t_kids_i[flag]) & (t_kids_i['zphot']<=n.max(sim_redshift)) & (t_kids_i['zphot']>=n.min(sim_redshift)) 
		#t_kids2 = t_kids_i[s_kids]
		## K magnitude
		#kids_k_mag_i = t_kids2['rtot']-(t_kids2['ri']+t_kids2['iz']+t_kids2['zy']+t_kids2['yj']+t_kids2['jh']+t_kids2['hks'])
		#kids_k_mag = (kids_k_mag_i - kids_k_mag_i.min()) / (kids_k_mag_i.max() - kids_k_mag_i.min())
		#NN, bb = n.histogram(kids_k_mag, bins=1000)
		#cdf_itpINV_kids_KMAG = interp1d(n.hstack((0,n.cumsum(NN)))/n.sum(NN), bb)
		## REDSHIFT
		#kids_photoz = t_kids2['zphot']
		## SFR / COLOR
		#kids_sfr =  (t_kids2['gr'] - t_kids2['gr'].min() ) / ( t_kids2['gr'].max() - t_kids2['gr'].min() )
		#NN, bb = n.histogram(kids_sfr, bins=1000)
		#cdf_itpINV_kids_SFR = interp1d(n.hstack((0,n.cumsum(NN)))/n.sum(NN), bb)
		## input kids into a tree
		#Tree_kids = BallTree(n.transpose([kids_photoz, kids_k_mag, kids_sfr]))
		#sim_k_mag_NORM = cdf_itpINV_kids_KMAG(sim_k_mag_0_1)
		#sim_sfr_NORM = cdf_itpINV_kids_SFR(sim_sfr_0_1)
		#DATA = n.transpose([sim_redshift, sim_k_mag_NORM, sim_sfr_NORM])
		#kids_ID = n.arange(len(t_kids2['rtot']))
		#"""
		#p.figure(2, (6,6))
		#p.hist(sim_redshift , bins=100, normed=True, cumulative=True, histtype='step', label='sim' )
		#p.hist(kids_photoz , bins=100, normed=True, cumulative=True, histtype='step', label='data' )
		#p.ylabel('cdf')
		#p.xlabel(r'redshift')
		#p.grid()
		#p.legend(frameon=False, loc=0)
		#p.savefig(os.path.join( plotDir, "3d_hist_REDSHIFT.png"))
		#p.clf()

		#p.figure(2, (6,6))
		#p.hist(sim_sfr_NORM , bins=100, normed=True, cumulative=True, histtype='step', label='sim' )
		#p.hist(kids_sfr , bins=100, normed=True, cumulative=True, histtype='step', label='data' )
		#p.ylabel('cdf')
		#p.xlabel(r'SFR proxy')
		#p.grid()
		#p.legend(frameon=False, loc=0)
		#p.savefig(os.path.join( plotDir, "3d_hist_SFR.png"))
		#p.clf()

		#p.figure(2, (6,6))
		#p.hist(sim_k_mag_NORM , bins=100, normed=True, cumulative=True, histtype='step', label='sim' )
		#p.hist(kids_k_mag , bins=100, normed=True, cumulative=True, histtype='step', label='data' )
		#p.ylabel('cdf')
		#p.xlabel(r'kmag')
		#p.grid()
		#p.legend(frameon=False, loc=0)
		#p.savefig(os.path.join( plotDir, "3d_hist_Kmag.png"))
		#p.clf()
		#"""
		##Tree_sim = BallTree(DATA)
		##test = Tree_kids.query(DATA, k=1, return_distance = True)
		#ids_out = Tree_kids.query(DATA, k=1, return_distance = False)
		#ids = n.hstack((ids_out))
		#id_to_map = kids_ID[ids]
		#columns_to_add = n.array(['rtot'          
									#, 'rfib'          
									#, 'ug'            
									#, 'gr'            
									#, 'ri'            
									#, 'iz'            
									#, 'zy'            
									#, 'yj'            
									#, 'jh'            
									#, 'hks'           
									#, 'lsdr9_TYPE'    
									#, 'lsdr9_RA'      
									#, 'lsdr9_DEC'     
									#, 'lsdr9_SHAPE_R' 
									#, 'lsdr9_SHAPE_E1'
									#, 'lsdr9_SHAPE_E2'
									#, 'lsdr9_SERSIC'  
									#, 'lsdr9_MASKBITS'])
		#print('outputing results')
		#for el in columns_to_add :
			#if el in t_survey.columns :
				#t_survey[el] = t_kids2[el][id_to_map]
			#else:
				#t_survey.add_column(Column(name=el, data=t_kids2[el][id_to_map]))
		#return t_survey 

	if doFILAMENT:
		path_2_BG_S5 = os.path.join(dir_2_OUT, 'S5GAL_'  + str(HEALPIX_id).zfill(6) + '.fit')
		print(path_2_BG_S5)
		if os.path.isfile(path_2_BG_S5):
			t_survey = Table.read(path_2_BG_S5)
			t_survey = add_magnitudes_direct_match(t_survey = t_survey, t_kids2 = t_kids_fil, Tree_kids=Tree_kids_fil)
			#print(t_survey.columns)
			if 'PMRA' in t_survey.columns :
				t_survey = update_4most_columns('filament_GAL' , t_survey  )
			else:
				t_survey = add_4most_columns('filament_GAL' , t_survey  )

			coord_SV = deg_to_rad * n.transpose([t_survey['DEC'], t_survey['RA'] ])
			# First deal with AGN and QSO
			dist_agn, idx_agn = Tree_AGN.query(coord_SV, return_distance=True)
			dist_qso, idx_qso = Tree_QSO.query(coord_SV, return_distance=True)
			match_agn = idx_agn[ ( dist_agn < max_distance ) ]
			match_qso = idx_qso[ ( dist_qso < max_distance ) ]
			match_agn_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_agn )) < max_distance ) ]
			match_qso_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_qso )) < max_distance ) ]
			print(len(match_agn_d1_position), 'AGN matches')
			print(len(match_qso_d1_position), 'QSO matches')
			# If type 21, then leave them in
			# else removes the lines
			# QSO can be directly removes (only type 1)
			matched_types = agn['agn_type'][match_agn]
			# to remove
			blu_agn = (matched_types ==11)| (matched_types==12)
			red_agn = (blu_agn == False)
			idx_remove = n.hstack(( match_agn_d1_position[blu_agn], match_qso_d1_position ))
			# the ones to keep
			idx_red_agn = match_agn_d1_position[red_agn]
			# get the ones that are exactly referring to the same halo :
			same_halo = (t_survey[idx_red_agn]['HALO_Mvir']/agn[match_agn[red_agn]]['HALO_Mvir']==1)
			print(nl(same_halo), 'red AGN are kept')
			# these will see their values changed to the AGN value
			# t_survey[idx_red_agn[same_halo]] <<== agn[match_agn[red_agn][same_halo]]
			if len(same_halo)>0:
				t_survey['MAG'][idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG']
				t_survey['MAG_ERR']          [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG_ERR']
				t_survey['EXTENT_FLAG']      [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_FLAG']
				t_survey['EXTENT_PARAMETER'] [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_PARAMETER']
				t_survey['EXTENT_INDEX']     [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_INDEX']
				t_survey['MAG_TYPE']         [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG_TYPE']
				t_survey['rtot']             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']
				t_survey['rfib']             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']          
				t_survey['ug'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['GALEX-NUV']-agn[match_agn[red_agn][same_halo]]['HSC-g']          
				t_survey['gr'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-g']-agn[match_agn[red_agn][same_halo]]['HSC-r']          
				t_survey['ri']               [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']-agn[match_agn[red_agn][same_halo]]['HSC-i']
				t_survey['iz']               [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-i']-agn[match_agn[red_agn][same_halo]]['HSC-z']
				t_survey['zy'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-z']-agn[match_agn[red_agn][same_halo]]['HSC-y']          
				t_survey['yj'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-y']-agn[match_agn[red_agn][same_halo]]['VISTA-J']                   
				t_survey['jh'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['VISTA-J']-agn[match_agn[red_agn][same_halo]]['VISTA-H']                             
				t_survey['hks' ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['VISTA-H']-agn[match_agn[red_agn][same_halo]]['VISTA-K']                   
			# removing QSOs
			if len(idx_remove)>0:
				print(len(idx_remove), 'QSO removed')
				t_survey[idx_remove] = -999
				t_survey = t_survey[t_survey['RA']>-990]

			# Second with BCG and GAL
			# match get their values replaced by the BCG and GAL model
			coord_SV = deg_to_rad * n.transpose([t_survey['DEC'], t_survey['RA'] ])
			# 
			dist_bcg, idx_bcg = Tree_BCG.query(coord_SV, return_distance=True)
			dist_gal, idx_gal = Tree_GAL.query(coord_SV, return_distance=True)
			match_bcg = idx_bcg[ ( dist_bcg < max_distance ) ]
			match_gal = idx_gal[ ( dist_gal < max_distance ) ]
			match_bcg_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_bcg )) < max_distance ) ]
			match_gal_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_gal )) < max_distance ) ]
			print(len(match_bcg_d1_position), 'BCG matches out of', len(coord_SV))
			print(len(match_gal_d1_position), 'GAL matches out of', len(coord_SV))
			# replace the values as follows
			# t_survey[match_bcg_d1_position] <<== t_BCG[match_bcg]
			# t_survey[match_gal_d1_position] <<== t_GAL[match_gal]
			if len(match_gal)>0:
				t_survey['MAG']              [match_gal_d1_position] = t_GAL[match_gal]['MAG']              
				t_survey['MAG_ERR']          [match_gal_d1_position] = t_GAL[match_gal]['MAG_ERR']          
				t_survey['EXTENT_FLAG']      [match_gal_d1_position] = t_GAL[match_gal]['EXTENT_FLAG']      
				t_survey['EXTENT_PARAMETER'] [match_gal_d1_position] = t_GAL[match_gal]['EXTENT_PARAMETER'] 
				t_survey['EXTENT_INDEX']     [match_gal_d1_position] = t_GAL[match_gal]['EXTENT_INDEX']     
				t_survey['MAG_TYPE']         [match_gal_d1_position] = t_GAL[match_gal]['MAG_TYPE']         
				t_survey['rtot']             [match_gal_d1_position] = t_GAL[match_gal]['rtot']             
				t_survey['rfib']             [match_gal_d1_position] = t_GAL[match_gal]['rfib']             
				t_survey['ug'  ]             [match_gal_d1_position] = t_GAL[match_gal]['ug'  ]                      
				t_survey['gr'  ]             [match_gal_d1_position] = t_GAL[match_gal]['gr'  ]                  
				t_survey['ri']               [match_gal_d1_position] = t_GAL[match_gal]['ri']               
				t_survey['iz']               [match_gal_d1_position] = t_GAL[match_gal]['iz']               
				t_survey['zy'  ]             [match_gal_d1_position] = t_GAL[match_gal]['zy'  ]                  
				t_survey['yj'  ]             [match_gal_d1_position] = t_GAL[match_gal]['yj'  ]                             
				t_survey['jh'  ]             [match_gal_d1_position] = t_GAL[match_gal]['jh'  ]                                         
				t_survey['hks' ]             [match_gal_d1_position] = t_GAL[match_gal]['hks' ]                               
			if len(match_bcg)>0:
				t_survey['MAG']              [match_bcg_d1_position] = t_BCG[match_bcg]['MAG']              
				t_survey['MAG_ERR']          [match_bcg_d1_position] = t_BCG[match_bcg]['MAG_ERR']          
				t_survey['EXTENT_FLAG']      [match_bcg_d1_position] = t_BCG[match_bcg]['EXTENT_FLAG']      
				t_survey['EXTENT_PARAMETER'] [match_bcg_d1_position] = t_BCG[match_bcg]['EXTENT_PARAMETER'] 
				t_survey['EXTENT_INDEX']     [match_bcg_d1_position] = t_BCG[match_bcg]['EXTENT_INDEX']     
				t_survey['MAG_TYPE']         [match_bcg_d1_position] = t_BCG[match_bcg]['MAG_TYPE']         
				t_survey['rtot']             [match_bcg_d1_position] = t_BCG[match_bcg]['rtot']             
				t_survey['rfib']             [match_bcg_d1_position] = t_BCG[match_bcg]['rfib']             
				t_survey['ug'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['ug'  ]                      
				t_survey['gr'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['gr'  ]                  
				t_survey['ri']               [match_bcg_d1_position] = t_BCG[match_bcg]['ri']               
				t_survey['iz']               [match_bcg_d1_position] = t_BCG[match_bcg]['iz']               
				t_survey['zy'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['zy'  ]                  
				t_survey['yj'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['yj'  ]                             
				t_survey['jh'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['jh'  ]                                         
				t_survey['hks' ]             [match_bcg_d1_position] = t_BCG[match_bcg]['hks' ]                               
			t_survey.write (path_2_BG_S5  , overwrite=True)
			print(path_2_BG_S5, 'written')
			

	if doFHS:
		path_2_BG_S5 = os.path.join(dir_2_OUT, 'FHSK19_'  + str(HEALPIX_id).zfill(6) + '.fit')
		print(path_2_BG_S5)
		if os.path.isfile(path_2_BG_S5):
			t_survey = Table.read(path_2_BG_S5)
			t_survey = add_magnitudes_direct_match(t_survey = t_survey, t_kids2 = t_kids_fil, Tree_kids=Tree_kids_fil)
			#print(t_survey.columns)
			if 'PMRA' in t_survey.columns :
				t_survey = update_4most_columns('filament_GAL' , t_survey  )
			else:
				t_survey = add_4most_columns('filament_GAL' , t_survey  )

			coord_SV = deg_to_rad * n.transpose([t_survey['DEC'], t_survey['RA'] ])
			# First deal with AGN and QSO
			dist_agn, idx_agn = Tree_AGN.query(coord_SV, return_distance=True)
			dist_qso, idx_qso = Tree_QSO.query(coord_SV, return_distance=True)
			match_agn = idx_agn[ ( dist_agn < max_distance ) ]
			match_qso = idx_qso[ ( dist_qso < max_distance ) ]
			match_agn_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_agn )) < max_distance ) ]
			match_qso_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_qso )) < max_distance ) ]
			print(len(match_agn_d1_position), 'AGN matches')
			print(len(match_qso_d1_position), 'QSO matches')
			# If type 21, then leave them in
			# else removes the lines
			# QSO can be directly removes (only type 1)
			matched_types = agn['agn_type'][match_agn]
			# to remove
			blu_agn = (matched_types ==11)| (matched_types==12)
			red_agn = (blu_agn == False)
			idx_remove = n.hstack(( match_agn_d1_position[blu_agn], match_qso_d1_position ))
			# the ones to keep
			idx_red_agn = match_agn_d1_position[red_agn]
			# get the ones that are exactly referring to the same halo :
			same_halo = (t_survey[idx_red_agn]['HALO_Mvir']/agn[match_agn[red_agn]]['HALO_Mvir']==1)
			print(nl(same_halo), 'red AGN are kept')
			# these will see their values changed to the AGN value
			# t_survey[idx_red_agn[same_halo]] <<== agn[match_agn[red_agn][same_halo]]
			if len(same_halo)>0:
				t_survey['MAG'][idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG']
				t_survey['MAG_ERR']          [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG_ERR']
				t_survey['EXTENT_FLAG']      [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_FLAG']
				t_survey['EXTENT_PARAMETER'] [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_PARAMETER']
				t_survey['EXTENT_INDEX']     [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_INDEX']
				t_survey['MAG_TYPE']         [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG_TYPE']
				t_survey['rtot']             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']
				t_survey['rfib']             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']          
				t_survey['ug'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['GALEX-NUV']-agn[match_agn[red_agn][same_halo]]['HSC-g']          
				t_survey['gr'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-g']-agn[match_agn[red_agn][same_halo]]['HSC-r']          
				t_survey['ri']               [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']-agn[match_agn[red_agn][same_halo]]['HSC-i']
				t_survey['iz']               [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-i']-agn[match_agn[red_agn][same_halo]]['HSC-z']
				t_survey['zy'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-z']-agn[match_agn[red_agn][same_halo]]['HSC-y']          
				t_survey['yj'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-y']-agn[match_agn[red_agn][same_halo]]['VISTA-J']                   
				t_survey['jh'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['VISTA-J']-agn[match_agn[red_agn][same_halo]]['VISTA-H']                             
				t_survey['hks' ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['VISTA-H']-agn[match_agn[red_agn][same_halo]]['VISTA-K']                   
			# removing QSOs
			if len(idx_remove)>0:
				print(len(idx_remove), 'QSO removed')
				t_survey[idx_remove] = -999
				t_survey = t_survey[t_survey['RA']>-990]

			# Second with BCG and GAL
			# match get their values replaced by the BCG and GAL model
			coord_SV = deg_to_rad * n.transpose([t_survey['DEC'], t_survey['RA'] ])
			# 
			dist_bcg, idx_bcg = Tree_BCG.query(coord_SV, return_distance=True)
			dist_gal, idx_gal = Tree_GAL.query(coord_SV, return_distance=True)
			match_bcg = idx_bcg[ ( dist_bcg < max_distance ) ]
			match_gal = idx_gal[ ( dist_gal < max_distance ) ]
			match_bcg_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_bcg )) < max_distance ) ]
			match_gal_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_gal )) < max_distance ) ]
			print(len(match_bcg_d1_position), 'BCG matches out of', len(coord_SV))
			print(len(match_gal_d1_position), 'GAL matches out of', len(coord_SV))
			# replace the values as follows
			# t_survey[match_bcg_d1_position] <<== t_BCG[match_bcg]
			# t_survey[match_gal_d1_position] <<== t_GAL[match_gal]
			if len(match_gal)>0:
				t_survey['MAG']              [match_gal_d1_position] = t_GAL[match_gal]['MAG']              
				t_survey['MAG_ERR']          [match_gal_d1_position] = t_GAL[match_gal]['MAG_ERR']          
				t_survey['EXTENT_FLAG']      [match_gal_d1_position] = t_GAL[match_gal]['EXTENT_FLAG']      
				t_survey['EXTENT_PARAMETER'] [match_gal_d1_position] = t_GAL[match_gal]['EXTENT_PARAMETER'] 
				t_survey['EXTENT_INDEX']     [match_gal_d1_position] = t_GAL[match_gal]['EXTENT_INDEX']     
				t_survey['MAG_TYPE']         [match_gal_d1_position] = t_GAL[match_gal]['MAG_TYPE']         
				t_survey['rtot']             [match_gal_d1_position] = t_GAL[match_gal]['rtot']             
				t_survey['rfib']             [match_gal_d1_position] = t_GAL[match_gal]['rfib']             
				t_survey['ug'  ]             [match_gal_d1_position] = t_GAL[match_gal]['ug'  ]                      
				t_survey['gr'  ]             [match_gal_d1_position] = t_GAL[match_gal]['gr'  ]                  
				t_survey['ri']               [match_gal_d1_position] = t_GAL[match_gal]['ri']               
				t_survey['iz']               [match_gal_d1_position] = t_GAL[match_gal]['iz']               
				t_survey['zy'  ]             [match_gal_d1_position] = t_GAL[match_gal]['zy'  ]                  
				t_survey['yj'  ]             [match_gal_d1_position] = t_GAL[match_gal]['yj'  ]                             
				t_survey['jh'  ]             [match_gal_d1_position] = t_GAL[match_gal]['jh'  ]                                         
				t_survey['hks' ]             [match_gal_d1_position] = t_GAL[match_gal]['hks' ]                               
			if len(match_bcg)>0:
				t_survey['MAG']              [match_bcg_d1_position] = t_BCG[match_bcg]['MAG']              
				t_survey['MAG_ERR']          [match_bcg_d1_position] = t_BCG[match_bcg]['MAG_ERR']          
				t_survey['EXTENT_FLAG']      [match_bcg_d1_position] = t_BCG[match_bcg]['EXTENT_FLAG']      
				t_survey['EXTENT_PARAMETER'] [match_bcg_d1_position] = t_BCG[match_bcg]['EXTENT_PARAMETER'] 
				t_survey['EXTENT_INDEX']     [match_bcg_d1_position] = t_BCG[match_bcg]['EXTENT_INDEX']     
				t_survey['MAG_TYPE']         [match_bcg_d1_position] = t_BCG[match_bcg]['MAG_TYPE']         
				t_survey['rtot']             [match_bcg_d1_position] = t_BCG[match_bcg]['rtot']             
				t_survey['rfib']             [match_bcg_d1_position] = t_BCG[match_bcg]['rfib']             
				t_survey['ug'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['ug'  ]                      
				t_survey['gr'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['gr'  ]                  
				t_survey['ri']               [match_bcg_d1_position] = t_BCG[match_bcg]['ri']               
				t_survey['iz']               [match_bcg_d1_position] = t_BCG[match_bcg]['iz']               
				t_survey['zy'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['zy'  ]                  
				t_survey['yj'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['yj'  ]                             
				t_survey['jh'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['jh'  ]                                         
				t_survey['hks' ]             [match_bcg_d1_position] = t_BCG[match_bcg]['hks' ]                               
			t_survey.write (path_2_BG_S5  , overwrite=True)
			print(path_2_BG_S5, 'written')
			
	if doLRG:
		path_2_LRG   = os.path.join(dir_2_OUT, 'LRG_' + str(HEALPIX_id).zfill(6) + '.fit')
		print(path_2_LRG)
		if os.path.isfile(path_2_LRG):
			t_survey = Table.read(path_2_LRG)
			t_survey = add_magnitudes_direct_match(t_survey = t_survey, t_kids2 = t_kids_lrg, Tree_kids=Tree_kids_lrg)
			if 'PMRA' in t_survey.columns :
				t_survey = update_4most_columns('LRG' , t_survey  )
			else:
				t_survey = add_4most_columns('LRG' , t_survey  )
			coord_SV = deg_to_rad * n.transpose([t_survey['DEC'], t_survey['RA'] ])
			# First deal with AGN and QSO
			dist_agn, idx_agn = Tree_AGN.query(coord_SV, return_distance=True)
			dist_qso, idx_qso = Tree_QSO.query(coord_SV, return_distance=True)
			match_agn = idx_agn[ ( dist_agn < max_distance ) ]
			match_qso = idx_qso[ ( dist_qso < max_distance ) ]
			match_agn_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_agn )) < max_distance ) ]
			match_qso_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_qso )) < max_distance ) ]
			print(len(match_agn_d1_position), 'AGN matches')
			print(len(match_qso_d1_position), 'QSO matches')
			# If type 21, then leave them in
			# else removes the lines
			# QSO can be directly removes (only type 1)
			matched_types = agn['agn_type'][match_agn]
			# to remove
			blu_agn = (matched_types ==11)| (matched_types==12)
			red_agn = (blu_agn == False)
			idx_remove = n.hstack(( match_agn_d1_position[blu_agn], match_qso_d1_position ))
			# the ones to keep
			idx_red_agn = match_agn_d1_position[red_agn]
			# get the ones that are exactly referring to the same halo :
			same_halo = (t_survey[idx_red_agn]['HALO_Mvir']/agn[match_agn[red_agn]]['HALO_Mvir']==1)
			print(nl(same_halo), 'red AGN are kept')
			# these will see their values changed to the AGN value
			# t_survey[idx_red_agn[same_halo]] <<== agn[match_agn[red_agn][same_halo]]
			if len(same_halo)>0:
				t_survey['MAG'][idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG']
				t_survey['MAG_ERR']          [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG_ERR']
				t_survey['EXTENT_FLAG']      [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_FLAG']
				t_survey['EXTENT_PARAMETER'] [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_PARAMETER']
				t_survey['EXTENT_INDEX']     [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_INDEX']
				t_survey['MAG_TYPE']         [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG_TYPE']
				t_survey['rtot']             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']
				t_survey['rfib']             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']          
				t_survey['ug'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['GALEX-NUV']-agn[match_agn[red_agn][same_halo]]['HSC-g']          
				t_survey['gr'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-g']-agn[match_agn[red_agn][same_halo]]['HSC-r']          
				t_survey['ri']               [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']-agn[match_agn[red_agn][same_halo]]['HSC-i']
				t_survey['iz']               [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-i']-agn[match_agn[red_agn][same_halo]]['HSC-z']
				t_survey['zy'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-z']-agn[match_agn[red_agn][same_halo]]['HSC-y']          
				t_survey['yj'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-y']-agn[match_agn[red_agn][same_halo]]['VISTA-J']                   
				t_survey['jh'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['VISTA-J']-agn[match_agn[red_agn][same_halo]]['VISTA-H']                             
				t_survey['hks' ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['VISTA-H']-agn[match_agn[red_agn][same_halo]]['VISTA-K']                   
			# removing QSOs
			if len(idx_remove)>0:
				print(len(idx_remove), 'QSO removed')
				t_survey[idx_remove] = -999
				t_survey = t_survey[t_survey['RA']>-990]

			# Second with BCG and GAL
			# match get their values replaced by the BCG and GAL model
			coord_SV = deg_to_rad * n.transpose([t_survey['DEC'], t_survey['RA'] ])
			# 
			dist_bcg, idx_bcg = Tree_BCG.query(coord_SV, return_distance=True)
			dist_gal, idx_gal = Tree_GAL.query(coord_SV, return_distance=True)
			match_bcg = idx_bcg[ ( dist_bcg < max_distance ) ]
			match_gal = idx_gal[ ( dist_gal < max_distance ) ]
			match_bcg_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_bcg )) < max_distance ) ]
			match_gal_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_gal )) < max_distance ) ]
			print(len(match_bcg_d1_position), 'BCG matches out of', len(coord_SV))
			print(len(match_gal_d1_position), 'GAL matches out of', len(coord_SV))
			# replace the values as follows
			# t_survey[match_bcg_d1_position] <<== t_BCG[match_bcg]
			# t_survey[match_gal_d1_position] <<== t_GAL[match_gal]
			if len(match_gal)>0:
				t_survey['MAG']              [match_gal_d1_position] = t_GAL[match_gal]['MAG']              
				t_survey['MAG_ERR']          [match_gal_d1_position] = t_GAL[match_gal]['MAG_ERR']          
				t_survey['EXTENT_FLAG']      [match_gal_d1_position] = t_GAL[match_gal]['EXTENT_FLAG']      
				t_survey['EXTENT_PARAMETER'] [match_gal_d1_position] = t_GAL[match_gal]['EXTENT_PARAMETER'] 
				t_survey['EXTENT_INDEX']     [match_gal_d1_position] = t_GAL[match_gal]['EXTENT_INDEX']     
				t_survey['MAG_TYPE']         [match_gal_d1_position] = t_GAL[match_gal]['MAG_TYPE']         
				t_survey['rtot']             [match_gal_d1_position] = t_GAL[match_gal]['rtot']             
				t_survey['rfib']             [match_gal_d1_position] = t_GAL[match_gal]['rfib']             
				t_survey['ug'  ]             [match_gal_d1_position] = t_GAL[match_gal]['ug'  ]                      
				t_survey['gr'  ]             [match_gal_d1_position] = t_GAL[match_gal]['gr'  ]                  
				t_survey['ri']               [match_gal_d1_position] = t_GAL[match_gal]['ri']               
				t_survey['iz']               [match_gal_d1_position] = t_GAL[match_gal]['iz']               
				t_survey['zy'  ]             [match_gal_d1_position] = t_GAL[match_gal]['zy'  ]                  
				t_survey['yj'  ]             [match_gal_d1_position] = t_GAL[match_gal]['yj'  ]                             
				t_survey['jh'  ]             [match_gal_d1_position] = t_GAL[match_gal]['jh'  ]                                         
				t_survey['hks' ]             [match_gal_d1_position] = t_GAL[match_gal]['hks' ]                               
			if len(match_bcg)>0:
				t_survey['MAG']              [match_bcg_d1_position] = t_BCG[match_bcg]['MAG']              
				t_survey['MAG_ERR']          [match_bcg_d1_position] = t_BCG[match_bcg]['MAG_ERR']          
				t_survey['EXTENT_FLAG']      [match_bcg_d1_position] = t_BCG[match_bcg]['EXTENT_FLAG']      
				t_survey['EXTENT_PARAMETER'] [match_bcg_d1_position] = t_BCG[match_bcg]['EXTENT_PARAMETER'] 
				t_survey['EXTENT_INDEX']     [match_bcg_d1_position] = t_BCG[match_bcg]['EXTENT_INDEX']     
				t_survey['MAG_TYPE']         [match_bcg_d1_position] = t_BCG[match_bcg]['MAG_TYPE']         
				t_survey['rtot']             [match_bcg_d1_position] = t_BCG[match_bcg]['rtot']             
				t_survey['rfib']             [match_bcg_d1_position] = t_BCG[match_bcg]['rfib']             
				t_survey['ug'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['ug'  ]                      
				t_survey['gr'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['gr'  ]                  
				t_survey['ri']               [match_bcg_d1_position] = t_BCG[match_bcg]['ri']               
				t_survey['iz']               [match_bcg_d1_position] = t_BCG[match_bcg]['iz']               
				t_survey['zy'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['zy'  ]                  
				t_survey['yj'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['yj'  ]                             
				t_survey['jh'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['jh'  ]                                         
				t_survey['hks' ]             [match_bcg_d1_position] = t_BCG[match_bcg]['hks' ]                               
			t_survey.write (path_2_LRG  , overwrite=True)
			print(path_2_LRG, 'written')

	if doBG:
		path_2_BG    = os.path.join(dir_2_OUT, 'BG_'  + str(HEALPIX_id).zfill(6) + '.fit')
		print(path_2_BG)
		if os.path.isfile(path_2_BG):
			t_survey = Table.read(path_2_BG)
			t_survey = add_magnitudes_direct_match(t_survey = t_survey, t_kids2 = t_kids_bg, Tree_kids=Tree_kids_bg)
			if 'PMRA' in t_survey.columns :
				t_survey = update_4most_columns('BG' , t_survey  )
			else:
				t_survey = add_4most_columns('BG' , t_survey  )
			coord_SV = deg_to_rad * n.transpose([t_survey['DEC'], t_survey['RA'] ])
			# First deal with AGN and QSO
			dist_agn, idx_agn = Tree_AGN.query(coord_SV, return_distance=True)
			dist_qso, idx_qso = Tree_QSO.query(coord_SV, return_distance=True)
			match_agn = idx_agn[ ( dist_agn < max_distance ) ]
			match_qso = idx_qso[ ( dist_qso < max_distance ) ]
			match_agn_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_agn )) < max_distance ) ]
			match_qso_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_qso )) < max_distance ) ]
			print(len(match_agn_d1_position), 'AGN matches')
			print(len(match_qso_d1_position), 'QSO matches')
			# If type 21, then leave them in
			# else removes the lines
			# QSO can be directly removes (only type 1)
			matched_types = agn['agn_type'][match_agn]
			# to remove
			blu_agn = (matched_types ==11)| (matched_types==12)
			red_agn = (blu_agn == False)
			idx_remove = n.hstack(( match_agn_d1_position[blu_agn], match_qso_d1_position ))
			# the ones to keep
			idx_red_agn = match_agn_d1_position[red_agn]
			# get the ones that are exactly referring to the same halo :
			same_halo = (t_survey[idx_red_agn]['HALO_Mvir']/agn[match_agn[red_agn]]['HALO_Mvir']==1)
			print(nl(same_halo), 'red AGN are kept')
			# these will see their values changed to the AGN value
			# t_survey[idx_red_agn[same_halo]] <<== agn[match_agn[red_agn][same_halo]]
			if len(same_halo)>0:
				t_survey['MAG'][idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG']
				t_survey['MAG_ERR']          [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG_ERR']
				t_survey['EXTENT_FLAG']      [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_FLAG']
				t_survey['EXTENT_PARAMETER'] [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_PARAMETER']
				t_survey['EXTENT_INDEX']     [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['EXTENT_INDEX']
				t_survey['MAG_TYPE']         [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['MAG_TYPE']
				t_survey['rtot']             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']
				t_survey['rfib']             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']          
				t_survey['ug'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['GALEX-NUV']-agn[match_agn[red_agn][same_halo]]['HSC-g']          
				t_survey['gr'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-g']-agn[match_agn[red_agn][same_halo]]['HSC-r']          
				t_survey['ri']               [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-r']-agn[match_agn[red_agn][same_halo]]['HSC-i']
				t_survey['iz']               [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-i']-agn[match_agn[red_agn][same_halo]]['HSC-z']
				t_survey['zy'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-z']-agn[match_agn[red_agn][same_halo]]['HSC-y']          
				t_survey['yj'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['HSC-y']-agn[match_agn[red_agn][same_halo]]['VISTA-J']                   
				t_survey['jh'  ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['VISTA-J']-agn[match_agn[red_agn][same_halo]]['VISTA-H']                             
				t_survey['hks' ]             [idx_red_agn[same_halo]] = agn[match_agn[red_agn][same_halo]]['VISTA-H']-agn[match_agn[red_agn][same_halo]]['VISTA-K']                   
			# removing QSOs
			if len(idx_remove)>0:
				print(len(idx_remove), 'QSO removed')
				t_survey[idx_remove] = -999
				t_survey = t_survey[t_survey['RA']>-990]

			# Second with BCG and GAL
			# match get their values replaced by the BCG and GAL model
			coord_SV = deg_to_rad * n.transpose([t_survey['DEC'], t_survey['RA'] ])
			# 
			dist_bcg, idx_bcg = Tree_BCG.query(coord_SV, return_distance=True)
			dist_gal, idx_gal = Tree_GAL.query(coord_SV, return_distance=True)
			match_bcg = idx_bcg[ ( dist_bcg < max_distance ) ]
			match_gal = idx_gal[ ( dist_gal < max_distance ) ]
			match_bcg_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_bcg )) < max_distance ) ]
			match_gal_d1_position = n.arange(len(t_survey))[ ( n.hstack(( dist_gal )) < max_distance ) ]
			print(len(match_bcg_d1_position), 'BCG matches out of', len(coord_SV))
			print(len(match_gal_d1_position), 'GAL matches out of', len(coord_SV))
			# replace the values as follows
			# t_survey[match_bcg_d1_position] <<== t_BCG[match_bcg]
			# t_survey[match_gal_d1_position] <<== t_GAL[match_gal]
			if len(match_gal)>0:
				t_survey['MAG']              [match_gal_d1_position] = t_GAL[match_gal]['MAG']              
				t_survey['MAG_ERR']          [match_gal_d1_position] = t_GAL[match_gal]['MAG_ERR']          
				t_survey['EXTENT_FLAG']      [match_gal_d1_position] = t_GAL[match_gal]['EXTENT_FLAG']      
				t_survey['EXTENT_PARAMETER'] [match_gal_d1_position] = t_GAL[match_gal]['EXTENT_PARAMETER'] 
				t_survey['EXTENT_INDEX']     [match_gal_d1_position] = t_GAL[match_gal]['EXTENT_INDEX']     
				t_survey['MAG_TYPE']         [match_gal_d1_position] = t_GAL[match_gal]['MAG_TYPE']         
				t_survey['rtot']             [match_gal_d1_position] = t_GAL[match_gal]['rtot']             
				t_survey['rfib']             [match_gal_d1_position] = t_GAL[match_gal]['rfib']             
				t_survey['ug'  ]             [match_gal_d1_position] = t_GAL[match_gal]['ug'  ]                      
				t_survey['gr'  ]             [match_gal_d1_position] = t_GAL[match_gal]['gr'  ]                  
				t_survey['ri']               [match_gal_d1_position] = t_GAL[match_gal]['ri']               
				t_survey['iz']               [match_gal_d1_position] = t_GAL[match_gal]['iz']               
				t_survey['zy'  ]             [match_gal_d1_position] = t_GAL[match_gal]['zy'  ]                  
				t_survey['yj'  ]             [match_gal_d1_position] = t_GAL[match_gal]['yj'  ]                             
				t_survey['jh'  ]             [match_gal_d1_position] = t_GAL[match_gal]['jh'  ]                                         
				t_survey['hks' ]             [match_gal_d1_position] = t_GAL[match_gal]['hks' ]                               
			if len(match_bcg)>0:
				t_survey['MAG']              [match_bcg_d1_position] = t_BCG[match_bcg]['MAG']              
				t_survey['MAG_ERR']          [match_bcg_d1_position] = t_BCG[match_bcg]['MAG_ERR']          
				t_survey['EXTENT_FLAG']      [match_bcg_d1_position] = t_BCG[match_bcg]['EXTENT_FLAG']      
				t_survey['EXTENT_PARAMETER'] [match_bcg_d1_position] = t_BCG[match_bcg]['EXTENT_PARAMETER'] 
				t_survey['EXTENT_INDEX']     [match_bcg_d1_position] = t_BCG[match_bcg]['EXTENT_INDEX']     
				t_survey['MAG_TYPE']         [match_bcg_d1_position] = t_BCG[match_bcg]['MAG_TYPE']         
				t_survey['rtot']             [match_bcg_d1_position] = t_BCG[match_bcg]['rtot']             
				t_survey['rfib']             [match_bcg_d1_position] = t_BCG[match_bcg]['rfib']             
				t_survey['ug'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['ug'  ]                      
				t_survey['gr'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['gr'  ]                  
				t_survey['ri']               [match_bcg_d1_position] = t_BCG[match_bcg]['ri']               
				t_survey['iz']               [match_bcg_d1_position] = t_BCG[match_bcg]['iz']               
				t_survey['zy'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['zy'  ]                  
				t_survey['yj'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['yj'  ]                             
				t_survey['jh'  ]             [match_bcg_d1_position] = t_BCG[match_bcg]['jh'  ]                                         
				t_survey['hks' ]             [match_bcg_d1_position] = t_BCG[match_bcg]['hks' ]                               
			t_survey.write (path_2_BG  , overwrite=True)
			print(path_2_BG, 'written')
	#if doELG:
		#if env=="MD04" or env=="MD40":
			#print('pass ELG')
		#else:
			#path_2_ELG   = os.path.join(dir_2_OUT, 'ELG_' + str(HEALPIX_id).zfill(6) + '.fit')
			#t_elg = Table.read(path_2_ELG)
			#t_survey = add_magnitudes_direct_match(t_survey = t_elg, flag = 'iss8elg', t_kids_i = t_kids)
			#t_survey.write (path_2_ELG  , overwrite=True)


#add_all_MAG(HEALPIX_id)
t0 = time.time()
N_pixels = healpy.nside2npix(8)
for HEALPIX_id in n.arange(N_pixels): #n.arange(N_pixels)[::-1]:
	print(HEALPIX_id, time.time()-t0)
	add_all_MAG(HEALPIX_id)

