"""
Example script

"""

#

#
import numpy as n
import os
import sys
import subprocess
import glob

# FIRST THE GEOMETRY
# ls $TEST_DIR
# hlists: directory
# snap_list.txt: list of snapshots

path_2_script = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    'python',
    'DM_LC',
    '000_geometry.py')
env = 'TEST_DIR'
L_box = '1000.0'
h = '0.6777'
py_command = ['python', path_2_script, L_box, h, env]
geometry_sub = subprocess.Popen(py_command)
# runs the geometry of the light cone to determine the boundaries of the
# shells in comoving Mpc

# ls $TEST_DIR
# snap_list_with_border.txt should have appeared
# ls $TEST_DIR/hlists
# full list of snapshots

# SECOND THE REPLICATION

path_2_script = os.path.join(
    os.environ['GIT_AGN_MOCK'],
    'python',
    'DM_LC',
    '001_process_hlists.py')
N_skip = '64'
Mmin_str = '9.0e9'  # MD04
Mmin_str = '1.0e11'  # MD10
Mmin_str = '1.0e9'  # MD10
# pathes to header files
HDR_CEN = "/home/comparat/software/lss_mock_dev/data/test_simulation/header"
HDR_SAT = "/home/comparat/software/lss_mock_dev/data/test_simulation/header_sat"

PATH_SNAPS = sorted(
    n.array(
        glob.glob(
            os.path.join(
                os.environ['TEST_DIR'],
                "hlists",
                "hlist_?.?????.list"))))

# central halos
for PATH_SNAP in PATH_SNAPS[::-1]:
    py_command = [
        'python',
        path_2_script,
        L_box,
        h,
        N_skip,
        env,
        HDR_CEN,
        PATH_SNAP,
        Mmin_str]
    replication_cen = subprocess.Popen(py_command)


# satellite halos
for PATH_SNAP in PATH_SNAPS[::-1]:
    py_command = [
        'python',
        path_2_script,
        L_box,
        h,
        N_skip,
        env,
        HDR_SAT,
        PATH_SNAP,
        Mmin_str]
    replication_sat = subprocess.Popen(py_command)
