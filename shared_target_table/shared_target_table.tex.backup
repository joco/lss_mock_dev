\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=0.5cm]{geometry}
\usepackage{hyperref}
\title{4MOST mocks Cluster, AGN and Cosmology surveys (S5, 6, 8) \\ Shared targets}
% \subtitle{Documentation}
\author{Johan Comparat\footnote{\url{comparat@mpe.mpg.de}}, MPE, Germany}
\date{March 2020}

\usepackage{natbib}
\usepackage{graphicx}

\begin{document}

\maketitle

\section{Mock catalogues description}

Catalogues are here: \url{https://firefly.mpe.mpg.de/mocks/2020-03/QMOST/}.

\noindent Code is here:  \url{https://gitlab.mpcdf.mpg.de/joco/lss_mock_dev/}.

\noindent This document is here:  \url{https://gitlab.mpcdf.mpg.de/joco/lss_mock_dev/tree/master/shared_target_table}.

\subsection{Methods}

\begin{itemize}
\item cluster + group model $M_{500c}>2\times10^{13}M_\odot$, galaxies around within 200c. Article in preparation.

\item agn model \url{https://ui.adsabs.harvard.edu/abs/2019MNRAS.487.2005C/abstract}

\item galaxy model: SHAM from 
\begin{itemize}
 \item ELG: \url{https://ui.adsabs.harvard.edu/abs/2016MNRAS.461.3421F/abstract}
 \item BG, LRG: \url{https://ui.adsabs.harvard.edu/abs/2016MNRAS.460.1173R/abstract}
\end{itemize}

\end{itemize}

\noindent Current maximum redshift of the light cones $z\sim4.2$.

The SDSS Fiber mag is computed in the r-band. 

\subsection{Areas} 
Definitions

\begin{itemize}
 \item FS: full sky == 41,273 deg$^2$
 \item b10: $|g_{lat}|>10$, == 34,089 deg$^2$ 
 \item eROSITA: $|g_{lat}|>10$, $g_{lon}>180$, == 17,036 deg$^2$ 
 \item eROSITA, Dec$<$20: $|g_{lat}|>10$, $Dec.<20$, $g_{lon}>180$, == 15,542 deg$^2$ 
 \item eROSITA DEEP: $|g_{lat}|>10$, $Dec.<20$, $g_{lon}>180$, $|b_{ecl}|>80$, == 313 deg$^2$ 
 \item eROSITA WIDE: $|g_{lat}|>10$, $Dec.<20$, $g_{lon}>180$, $|b_{ecl}|<80$, == 15,228 deg$^2$ 
 \item eROSITA, Dec$<$5: $|g_{lat}|>10$, $Dec.<5$, $g_{lon}>180$, == 13,674 deg$^2$ 
 \item Small area (region in common between sub surveys where the density of common targets is estimated): $160<R.A.<200$, $-20<Dec.<-10$, == 385 deg$^2$
\end{itemize}

\subsection{Catalogues}
% /data37s/simulation_1/MD/MD_1.0Gpc/4most_s58_kidsdr4.fits 10112143
% /data37s/simulation_1/MD/MD_1.0Gpc/AGN_DEEP_4MOST.fits 109588
% /data37s/simulation_1/MD/MD_1.0Gpc/AGN_IR_4MOST.fits 737565
% /data37s/simulation_1/MD/MD_1.0Gpc/AGN_WIDE_4MOST.fits 2293010
% /data37s/simulation_1/MD/MD_1.0Gpc/AGN_all.fits 256040154
% /data37s/simulation_1/MD/MD_1.0Gpc/AGN_eFEDS.fits 3016846
% /data37s/simulation_1/MD/MD_1.0Gpc/BG_4MOST.fits 10326528
% /data37s/simulation_1/MD/MD_1.0Gpc/EFEDS_agn_fsat_10.fits 55330447
% /data37s/simulation_1/MD/MD_1.0Gpc/EFEDS_agn_fsat_12.fits 48493140
% /data37s/simulation_1/MD/MD_1.0Gpc/EFEDS_agn_fsat_14.fits 42285125
% /data37s/simulation_1/MD/MD_1.0Gpc/EFEDS_agn_fsat_16.fits 37634232
% /data37s/simulation_1/MD/MD_1.0Gpc/EFEDS_agn_fsat_2.fits 51408398
% /data37s/simulation_1/MD/MD_1.0Gpc/EFEDS_agn_fsat_20.fits 31118120
% /data37s/simulation_1/MD/MD_1.0Gpc/EFEDS_agn_fsat_4.fits 52387856
% /data37s/simulation_1/MD/MD_1.0Gpc/EFEDS_agn_fsat_6.fits 53379104
% /data37s/simulation_1/MD/MD_1.0Gpc/EFEDS_agn_fsat_8.fits 54345733
% /data37s/simulation_1/MD/MD_1.0Gpc/EFEDS_all.fits 203585375
% /data37s/simulation_1/MD/MD_1.0Gpc/ELG_4MOST.fits 49128564
% /data37s/simulation_1/MD/MD_1.0Gpc/FILAMENTS5_4MOST.fits 9841895
% /data37s/simulation_1/MD/MD_1.0Gpc/LRG_4MOST.fits 16500480
% /data37s/simulation_1/MD/MD_1.0Gpc/LyA_4MOST.fits 2114525
% /data37s/simulation_1/MD/MD_1.0Gpc/MD10_LC.fit 28579470
% /data37s/simulation_1/MD/MD_1.0Gpc/MD10_eRO_CLU.fit 10687090
% /data37s/simulation_1/MD/MD_1.0Gpc/MD10_eRO_CLU_GAL.fit 37913951
% /data37s/simulation_1/MD/MD_1.0Gpc/QSO_4MOST.fits 4804985
% /data37s/simulation_1/MD/MD_1.0Gpc/S5_4MOST.fit 5103993
% /data37s/simulation_1/MD/MD_1.0Gpc/S5_BCG_4MOST.fit 450520
% /data37s/simulation_1/MD/MD_1.0Gpc/S5_CGAL_4MOST.fit 1938309
% /data37s/simulation_1/MD/MD_1.0Gpc/S6_AGN.fits 6668943
% /data37s/simulation_1/MD/MD_1.0Gpc/S6_AGN_hig_z.fit 6578745
% /data37s/simulation_1/MD/MD_1.0Gpc/S8_AGN_hig_z.fit 12979319
% /data37s/simulation_1/MD/MD_1.0Gpc/SPIDERS_AGN_all.fit 5712409
% /data37s/simulation_1/MD/MD_1.0Gpc/hlists_MD_1.0Gpc.fits 94
% /data37s/simulation_1/MD/MD_1.0Gpc/output_MD_1.0Gpc.fits 46
% /data37s/simulation_1/MD/MD_1.0Gpc/output_MD_1.0Gpc_all.fits 55
% /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S5_CLUSTER.fits 5103993
% /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_BG.fits 10326528
% /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_ELG.fits 49128564
% /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_LRG.fits 16500480
% /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_LyA.fits 2114525
% /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8_QSO.fits 4804985
% /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8footprint_BG.fits 1886551
% /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8footprint_ELG.fits 1336103
% /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8footprint_LRG.fits 3015543
% /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8footprint_LyA.fits 513828
% /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8footprint_QSO.fits 1139832
% /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_S8footprint_QSO_w_contamination.fits 1507481
% /data42s/comparat/firefly/mocks/2020-03/QMOST/QMOST_random_ra-dec-50perdeg2.fits 2062648
% /data42s/comparat/firefly/mocks/2020-03/QMOST/S6_4MOST_R-228-234.fit 2286159
% 


Surveys 5, 6, 8 have the following sub surveys that contain the following density (per deg2) of targets. 
\begin{itemize}
 \item S5: \texttt{QMOST\_S5\_CLUSTER.fits } contains 3 sub surveys
   \begin{itemize}
   \item SUBSURVEY = 'cluster\_BCG'   , eROSITA, Dec$<$20, N/area =   411,316/ 15542.3 =  26.4 /deg2
   \item SUBSURVEY = 'cluster\_redGAL', eROSITA, Dec$<$20, N/area = 1,685,593/ 15542.3 = 108.4 /deg2
   \item SUBSURVEY = 'filament\_GAL'  , eROSITA, Dec$<$20, N/area = 3,007,084/ 15542.3 = 193.4 /deg2
   \end{itemize}

 \item S6: \texttt{S6\_4MOST\_R-228-234.fit} contains 3 sub surveys    
   \begin{itemize}
   \item SUBSURVEY = 'AGN\_WIDE' , eROSITA WIDE, N/area = 1,852,429/ 15228.6 = 121.6 /deg2
   \item SUBSURVEY = 'AGN\_DEEP' , eROSITA DEEP, N/area = 100,727/ 313.7 = 321.1 /deg2
   \item SUBSURVEY = 'AGN\_IR'   , eROSITA, Dec$<$20,  N/area = 333,003/ 15542.3 = 21.4 /deg2
   \end{itemize}

 \item S8: 5 sub surveys, each has a catalogue         
  \begin{itemize}
  \item BG:  \texttt{QMOST\_S8\_BG.fits},  FS,         N/area = 10,326,528/41,273 = 250.2  /deg2
  \item LRG: \texttt{QMOST\_S8\_LRG.fits}, FS,         N/area = 16,500,480/41,273 = 399.79  /deg2
  \item ELG: \texttt{QMOST\_S8\_ELG.fits}, FS,         N/area = 49,128,564/41,273 = 1,190.33  /deg2
  \item QSO: \texttt{QMOST\_S8\_QSO.fits}, b10,        N/area =  4,804,985/34,089  = 140.95  /deg2
  \item Ly$\alpha$: \texttt{QMOST\_S8\_LyA.fits}, b10, N/area =  2,114,525/34,089  = 62.03  /deg2
  \end{itemize}

 \item S8 catalogues trimmed to the exact footprint are :  
  \begin{itemize}
  \item BG:  \texttt{QMOST\_S8footprint\_BG.fits},         N = 1,886,551 / 7,500 = 251.54 /deg2
  \item LRG: \texttt{QMOST\_S8footprint\_LRG.fits},        N = 3,015,543 / 7,500 = 402.07 /deg2
  \item ELG: \texttt{QMOST\_S8footprint\_ELG.fits},        N = 1,336,103 / 1,100 = 1214.64 /deg2
  \item QSO: \texttt{QMOST\_S8footprint\_QSO.fits},        N = 1,139,832 / 7,500 = 151.98 /deg2
  \item Ly$\alpha$: \texttt{QMOST\_S8footprint\_LyA.fits}, N =  513,828 / 7,500 = 68.51 /deg2
  \end{itemize}                                                                  

  \item QSO: \texttt{QMOST\_S8footprint\_QSO\_w\_contamination.fits},  N = 1,507,481 / 7,500 =  200.1 /deg2   (Not used in the overlap matrix as contamination targets are random)
\end{itemize}


\subsection{Download}

To download the files you need to use the option 'no check certificate' in wget as follows:
\begin{verbatim}
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/QMOST/QMOST_S8footprint_BG.fits.gz .
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/QMOST/QMOST_S8footprint_LRG.fits.gz .
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/QMOST/QMOST_S8footprint_ELG.fits.gz .
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/QMOST/QMOST_S8footprint_QSO_w_contamination.fits.gz .
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/QMOST/QMOST_S8footprint_LyA.fits.gz .
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/QMOST/QMOST_S5_CLUSTER.fits.gz
wget --no-check-certificate https://firefly.mpe.mpg.de/mocks/2020-03/QMOST/S6_4MOST_R-228-234.fit.gz
\end{verbatim}


\subsection{Selections}

\begin{itemize}

 \item S6: \texttt{S6\_4MOST\_R-228-234.fit} contains 3 sub surveys
   \begin{itemize}
   \item SUBSURVEY = 'AGN\_WIDE', 
   \begin{verbatim}SNR 3 eRASS8 X-ray detections on the wide area. SDSS fiber mag $r<22.8$      \end{verbatim}
   \item SUBSURVEY = 'AGN\_DEEP', 
   \begin{verbatim}SNR 3 eRASS8 X-ray detections at the ecliptic pole. SDSS fiber mag $r<23.4$      \end{verbatim}
   \item SUBSURVEY = 'AGN\_IR', 
   \begin{verbatim}(AGN\_type=22 | AGN\_type=21)  & mag WISE W1<18 & SDSS fiber mag r <22.8   \end{verbatim}
   \item 'NAME' in the 4MOST catalogue is defined as follows : 
   \begin{verbatim}
  import numpy as n
  import healpy
  N_subsurvey = {'AGN_WIDE':1, 'AGN_DEEP':2, 'AGN_IR':3}
  # healpy NSIDE 8 where the target is  in 0-767
  # 53 deg2 area
  HEALPIX_id = healpy.ang2pix(8, n.pi/2. - DEC*n.pi/180. , RA*n.pi/180. , nest=True)
  # line of the target in the catalogue
  # takes values in 0, ... 60,000. 
  N1 = n.arange(N_targets) 
  # construct the ids : 
  # first digit sub survey
  id_list = N_subsurvey*1e10 + HEALPIX_id*1e6 +  N1
  NAME = n.array([ str(int(el)).zfill(11) for el in id_list ])
   \end{verbatim}
   
   \end{itemize}

 \item S8: 5 sub surveys, each has a catalogue, 
  \begin{itemize}
  \item BG: \texttt{QMOST\_S8\_BG.fits},   
   \begin{verbatim} SHAM(galaxy_stellar_mass + N(0,0.15)), with BG NZ and BG total number density \end{verbatim}
  \item LRG: \texttt{QMOST\_S8\_LRG.fits}, 
   \begin{verbatim} SHAM(galaxy_stellar_mass + N(0,0.15)), with LRG NZ and LRG total number density & not BG\end{verbatim}
  \item ELG: \texttt{QMOST\_S8\_ELG.fits}, 
   \begin{verbatim} Mvir in Gaussian(12.1, 0.3), random sub sampling, with ELG NZ and ELG total number density \end{verbatim}
  \item QSO: \texttt{QMOST\_S8\_QSO.fits}, 
   \begin{verbatim} 0.9<z<2.2 AGN\_type=11 & SDSS fiber mag r < 22.5 \end{verbatim}
  \item Ly$\alpha$: \texttt{QMOST\_S8\_LyA.fits}, 
   \begin{verbatim} z>2.2 AGN\_type=11 & SDSS fiber mag r < 22.5 \end{verbatim}
  \item Contamination added by taking 50 QSO per deg2 from the catalogue and randomizing their ra, dec.
  \item 'NAME' in the 4MOST catalogue is defined as follows : 
  \begin{verbatim}
  import numpy as n
  import healpy
  N_subsurvey = {'BG':1, 'LRG':2, 'ELG':3, 'QSO':4, 'LyA':5, 'contamination_qso': 6}
  # healpy NSIDE 8 where the target is  in 0-767
  # 53 deg2 area
  HEALPIX_id = healpy.ang2pix(8, n.pi/2. - DEC*n.pi/180. , RA*n.pi/180. , nest=True)
  # line of the target in the catalogue
  # takes values in 0, ... 60,000. 
  N1 = n.arange(N_targets) 
  # construct the ids : 
  # first digit sub survey
  id_list = N_subsurvey*1e10 + HEALPIX_id*1e6 +  N1
  NAME = n.array([ str(int(el)).zfill(11) for el in id_list ])
  \end{verbatim}
  \end{itemize}
  
\textbf{Within a survey NAMES are unique. Across different surveys, note that NAME can be the same for different targets, it is not a unique identifier across surveys. }

\end{itemize}

\subsection{Sky areas}

\begin{figure}
\centering
\includegraphics[width=.32\columnwidth]{figures/S5_BCG.png}
\includegraphics[width=.32\columnwidth]{figures/S5_GAL.png}
\includegraphics[width=.32\columnwidth]{figures/S5_FIL.png}
\includegraphics[width=.32\columnwidth]{figures/S6_Awi.png}
\includegraphics[width=.32\columnwidth]{figures/S6_Ade.png}
\includegraphics[width=.32\columnwidth]{figures/S6_Air.png}
\includegraphics[width=.32\columnwidth]{figures/S8__BG.png}
\includegraphics[width=.32\columnwidth]{figures/S8_LRG.png}
\includegraphics[width=.32\columnwidth]{figures/S8_ELG.png}
\includegraphics[width=.32\columnwidth]{figures/S8_QSO.png}
\includegraphics[width=.32\columnwidth]{figures/S8_LyA.png}
\caption{\label{fig:footprints}Footprints of the sub surveys considered. First line: S5: BCG, cluster galaxies, Filament. Second line: S6: eROSITA WIDE, eROSITA DEEP, AGN IR. Third line: S8: BG, LRG, ELG. Fourth line: S8: QSO, Lyman $\alpha$}
\end{figure}


\begin{figure}
\centering
\includegraphics[width=0.8\columnwidth]{/home/comparat/software/lss_mock_dev/python/qmost/S568_NZ.png}
\includegraphics[width=0.8\columnwidth]{/home/comparat/software/lss_mock_dev/python/qmost/S568_NZ-xlog.png}
\caption{\label{fig:NZ:all_subsurveys}Redshift distribution of all sub surveys. }
\end{figure}



\clearpage
\subsection{Overlap}
We compute the number of targets within 0.1 arc seconds between full catalogues. They are considered as common targets. 


Over the area (385.87 deg2) : \begin{verbatim}(ra>160)&(ra<200)&(dec>-20)&(dec<-10)\end{verbatim}, all sub surveys (expect for AGN DEEP and COSMO ELG) are present. Cosmo ELG has almost no correlation with other subsurveys. AGN DEEP is a very small and specific area: no overlap with COSMO.

\noindent There we compute overlap in number density. 
Table \ref{tab:AGN:QSO:model} gives the number density (per square degree) of common targets. 
Fig. \ref{fig:Venn:AGN:QSO} shows the equivalent numbers estimated with real data. 
% Some discrepancies are to be noted and need an explanation. 
% \begin{itemize}
%  \item number density of S8 QSO and Lya ni the simulation are twice higher than in the Venn diagram.
% \end{itemize}

\noindent We compute the common targets across the full footprints to have total numbers and fractions: Table \ref{tab:total:numbers} gives the total numbers and Table \ref{tab:fraction:numbers} gives the fractions. 

\begin{table}
    \centering
    \begin{tabular}{|c|ccc|cc|cccc|}
    \hline 
%  & S6 AGN\_WIDE & S6 AGN\_IR & S8 QSO & S8 LyA \\
% \hline
% S6 AGN\_WIDE  &  116.4 & 6.3 & 76.9 & 12.6  \\
% S6 AGN\_IR    &  6.3   & 25.3 & 0.0 & 0.0  \\
% S8 QSO       &  76.9   & 0.0 & 296.5 & 0.0  \\
% S8 LyA       &  12.6   & 0.0 & 0.0 & 127.8  \\
% 
\hline
survey    & 
\multicolumn{3}{|c|}{S5 CLUSTER} &   
\multicolumn{2}{|c|}{S6 AGN} &   
\multicolumn{4}{|c|}{S8 COSMO} \\   
sub & BCG & GAL & FIL & WIDE & IR & QSO & LyA & BG & LRG \\
N & 7859 & 34300 & 76923 & 39510 & 9121 & 56933 & 25655 & 97220 & 155445 \\
\hline \hline 
BCG &   20.4 & 19.6 & 8.4 & 0.2 & 0.0 & 0.0 & 0.0 & 12.0 & 5.7  \\
GAL &   19.6 & 92.8 & 13.9 & 0.4 & 0.0 & 0.0 & 0.0 & 17.3 & 9.0  \\
FIL &   8.4 & 13.9 & 199.4 & 0.9 & 0.0 & 0.0 & 0.0 & 96.9 & 6.3  \\       \hline
WIDE&   0.2 & 0.4 & 0.9 & 102.4 & 5.7 & 50.2 & 10.1 & 1.1 & 2.7  \\
IR  &   0.0 & 0.0 & 0.0 & 5.7 & 23.6 & 0.0 & 0.0 & 0.0 & 0.1  \\   \hline 
QSO &   0.0 & 0.0 & 0.0 & 50.2 & 0.0 & 147.5 & 0.0 & 0.0 & 0.2  \\
LyA &   0.0 & 0.0 & 0.0 & 10.1 & 0.0 & 0.0 & 66.5 & 0.0 & 0.0  \\
 BG &   12.0 & 17.3 & 96.9 & 1.1 & 0.0 & 0.0 & 0.0 & 252.0 & 0.0  \\
LRG &   5.7 & 9.0 & 6.3 & 2.7 & 0.1 & 0.2 & 0.0 & 0.0 & 402.8  \\

\hline
    \end{tabular}
    \caption{On a small 385 deg2 region in common between the S5, S6 and S8 components, is estimated the number density of targets per deg2 in common. }
    \label{tab:AGN:QSO:model}
\end{table}

\begin{table}
    \centering
    \begin{tabular}{|c|ccc|cc|cccc|}
    \hline 
%  & S6 AGN\_WIDE & S6 AGN\_IR & S8 QSO & S8 LyA \\
% \hline
% S6 AGN\_WIDE  &  116.4 & 6.3 & 76.9 & 12.6  \\
% S6 AGN\_IR    &  6.3   & 25.3 & 0.0 & 0.0  \\
% S8 QSO       &  76.9   & 0.0 & 296.5 & 0.0  \\
% S8 LyA       &  12.6   & 0.0 & 0.0 & 127.8  \\
% 
survey    & 
\multicolumn{3}{|c|}{S5 CLUSTER} &   
\multicolumn{2}{|c|}{S6 AGN} &   
\multicolumn{4}{|c|}{S8 COSMO} \\   
sub & BCG & GAL & FIL & WIDE & IR & QSO & LyA & BG & LRG \\
N & 7859 & 34300 & 76923 & 39510 & 9121 & 56933 & 25655 & 97220 & 155445 \\ \hline 

BCG &  100.0 & 22.0 & 4.2 & 0.2 & 0.0 & 0.0 & 0.0 & 4.8 & 1.4  \\        
GAL &  96.1 & 104.4 & 7.0 & 0.4 & 0.1 & 0.0 & 0.0 & 6.8 & 2.2  \\        
FIL &  41.1 & 15.7 & 100.0 & 0.9 & 0.1 & 0.0 & 0.0 & 38.5 & 1.6  \\          \hline
WIDE&  0.9 & 0.4 & 0.5 & 100.0 & 23.9 & 34.0 & 15.2 & 0.4 & 0.7  \\           
IR  &  0.1 & 0.0 & 0.0 & 5.5 & 100.0 & 0.0 & 0.0 & 0.0 & 0.0  \\     \hline
QSO &  0.1 & 0.0 & 0.0 & 49.0 & 0.0 & 100.0 & 0.0 & 0.0 & 0.0  \\        
LyA &  0.0 & 0.0 & 0.0 & 9.9 & 0.0 & 0.0 & 100.0 & 0.0 & 0.0  \\        
 BG &  58.9 & 19.4 & 48.6 & 1.1 & 0.1 & 0.0 & 0.0 & 100.0 & 0.0  \\        
LRG &  28.0 & 10.1 & 3.2 & 2.6 & 0.6 & 0.1 & 0.0 & 0.0 & 100.0  \\         

\hline
    \end{tabular}
    \caption{On a small 385 deg2 region in common between the S5, S6 and S8 components, I estimate the percentages in common. The denominator is the value at the top of the column, the matrix is thus not symmetric and gives the share w.r.t. a sub survey or the other. }
    \label{tab:AGN:QSO:model:pc}
\end{table}


\begin{table}
    \centering
    \begin{tabular}{| c | ccc | ccc | ccccc | cccccc}
    \hline  
survey    & 
\multicolumn{3}{|c|}{S5 CLUSTER} &   
\multicolumn{3}{|c|}{S6 AGN} &   
\multicolumn{5}{|c|}{S8 COSMO} \\   
sub & CLUSTER & CLUSTER & LSS & AGN & AGN & AGN & QSO & LyA & ELG & BG & LRG \\
      & BCG & GAL & FILAMENT   & WIDE & DEEP & IR & & & & &  \\
N & 411316 & 1685593 & 3007084 & 1852429 & 100727 & 333003 & 1139832 & 513828 & 1336103 & 1886551 & 3015543 \\

 \hline

BCG &   411344 & 392771 & 139602 & 4188 & 345 & 172 & 220 & 0 & 1 & 92387 & 55189  \\
GAL &   392771 & 1757521 & 224548 & 7817 & 611 & 289 & 363 & 0 & 2317 & 131481 & 80415  \\
FIL &   139602 & 224548 & 3007088 & 16376 & 589 & 395 & 1 & 0 & 7708 & 632789 & 40524  \\          \hline
WIDE &  4188 & 7817 & 16376 & 1852429 & 0 & 95754 & 399360 & 95711 & 2242 & 8605 & 20627  \\
DEEP &  345 & 611 & 589 & 0 & 100727 & 3711 & 8658 & 3212 & 0 & 209 & 453  \\
IR &    172 & 289 & 395 & 95754 & 3711 & 333003 & 0 & 1 & 115 & 206 & 1063  \\          \hline
QSO &   220 & 363 & 1 & 399360 & 8658 & 0 & 1139832 & 0 & 1020 & 2 & 1150  \\
LyA &   0 & 0 & 0 & 95711 & 3212 & 1 & 0 & 513828 & 0 & 0 & 0  \\
ELG &   1 & 2317 & 7708 & 2242 & 0 & 115 & 1020 & 0 & 1336105 & 6171 & 6802  \\
 BG &   92387 & 131481 & 632789 & 8605 & 209 & 206 & 2 & 0 & 6171 & 1886551 & 1  \\
LRG &   55189 & 80415 & 40524 & 20627 & 453 & 1063 & 1150 & 0 & 6802 & 1 & 3015557  \\

\hline
    \end{tabular}
    \caption{Overlap matrix wthin 0.1 arc seconds. Full footprints.}
    \label{tab:total:numbers}
\end{table}



\begin{table}
    \centering
    \begin{tabular}{| c | ccc | ccc | ccccc | cccccc}
    \hline  
survey    & 
\multicolumn{3}{|c|}{S5 CLUSTER} &   
\multicolumn{3}{|c|}{S6 AGN} &   
\multicolumn{5}{|c|}{S8 COSMO} \\   
sub & CLUSTER & CLUSTER & LSS & AGN & AGN & AGN & QSO & LyA & ELG & BG & LRG \\
& BCG & GAL & FILAMENT & WIDE & DEEP & IR & & & & &  \\
N & 411316 & 1685593 & 3007084 & 1852429 & 100727 & 333003 & 1139832 & 513828 & 1336103 & 1886551 & 3015543  \\
\hline
BCG &  100.0 & 23.3 & 4.6 & 0.2 & 0.3 & 0.1 & 0.0 & 0.0 & 0.0 & 4.9 & 1.8  \\
GAL &  95.5 & 104.3 & 7.5 & 0.4 & 0.6 & 0.1 & 0.0 & 0.0 & 0.2 & 7.0 & 2.7  \\
FIL &  33.9 & 13.3 & 100.0 & 0.9 & 0.6 & 0.1 & 0.0 & 0.0 & 0.6 & 33.5 & 1.3  \\      \hline
WIDE & 1.0 & 0.5 & 0.5 & 100.0 & 0.0 & 28.8 & 35.0 & 18.6 & 0.2 & 0.5 & 0.7  \\
DEEP & 0.1 & 0.0 & 0.0 & 0.0 & 100.0 & 1.1 & 0.8 & 0.6 & 0.0 & 0.0 & 0.0  \\
IR &   0.0 & 0.0 & 0.0 & 5.2 & 3.7 & 100.0 & 0.0 & 0.0 & 0.0 & 0.0 & 0.0  \\      \hline
QSO &  0.1 & 0.0 & 0.0 & 21.6 & 8.6 & 0.0 & 100.0 & 0.0 & 0.1 & 0.0 & 0.0  \\
LyA &  0.0 & 0.0 & 0.0 & 5.2 & 3.2 & 0.0 & 0.0 & 100.0 & 0.0 & 0.0 & 0.0  \\
ELG &  0.0 & 0.1 & 0.3 & 0.1 & 0.0 & 0.0 & 0.1 & 0.0 & 100.0 & 0.3 & 0.2  \\
 BG &  22.5 & 7.8 & 21.0 & 0.5 & 0.2 & 0.1 & 0.0 & 0.0 & 0.5 & 100.0 & 0.0  \\
LRG &  13.4 & 4.8 & 1.3 & 1.1 & 0.4 & 0.3 & 0.1 & 0.0 & 0.5 & 0.0 & 100.0  \\
\hline
    \end{tabular}
    \caption{Overlap matrix wthin 0.1 arc seconds. Full footprints, percentage relative to the All value given in the third line. CLUSTER GAL subsurvey is very dense so there are some pairs closer than 0.1 arcseconds, thus the 103\%.}
    \label{tab:fraction:numbers}
\end{table}

\begin{figure}
\centering
\includegraphics[width=0.8\columnwidth]{figures/Venn-diagramm-AGN-QSO.png}
\caption{\label{fig:Venn:AGN:QSO}Correlation between sub surveys as measured in the data. Computed by the AGN and Cosmo surveys. }
\end{figure}



\clearpage
\section{Exposure time calculation}

\subsection{Inputs}

Templates are constructed by stacking observed spectra from SDSS, see Comparat et al. 2019 for further description. 
Rules and rulesets are in \url{https://gitlab.mpcdf.mpg.de/joco/lss_mock_dev/tree/master/python/qmost/S*/*.csv}. We report the lines of interest below

S5:
\begin{verbatim}
RULE_NAME;VARIABLE;METRIC;OPERATOR;VALUE;L_MIN;L_MAX;L_UNIT;DELTA_L;DELTA_L_UNIT
Elliptical;SNR;MEDIAN;DIV;1.0;7200.0;9000.0;AA;1.0;AA

RULESET_NAME;REQ_VALUE;EXPRESSION
RedGAL;1.0;(Elliptical >= 5.)
ClusBCG;1.0;(Elliptical >= 20.) 
\end{verbatim}

S6:
\begin{verbatim}
RULE_NAME;VARIABLE;METRIC;OPERATOR;VALUE;L_MIN;L_MAX;L_UNIT;DELTA_L;DELTA_L_UNIT
SNR_RULEB;SNR;MEDIAN;DIV;1.0;4200.0;5000.0;AA;1.0;AA
SNR_RULEG;SNR;MEDIAN;DIV;1.0;5500.0;6700.0;AA;1.0;AA
SNR_RULER;SNR;MEDIAN;DIV;1.0;7200.0;9000.0;AA;1.0;AA

RULESET_NAME;REQ_VALUE;EXPRESSION
AGN_ALL_3PC;1.0;(SNR_RULEB>=2.1)||(SNR_RULEG>=2.4)||(SNR_RULER>=2.8)
AGN_ALL_5PC;1.0;(SNR_RULEB>1.1)||(SNR_RULEG>1.3)||(SNR_RULER>1.5)
\end{verbatim}

S8:
\begin{verbatim}
RULE_NAME;VARIABLE;METRIC;OPERATOR;VALUE;L_MIN;L_MAX;L_UNIT;DELTA_L;DELTA_L_UNIT
SNR_RULEB;SNR;MEDIAN;DIV;1.0;4200.0;5000.0;AA;1.0;AA
SNR_RULEG;SNR;MEDIAN;DIV;1.0;5500.0;6700.0;AA;1.0;AA
SNR_RULER;SNR;MEDIAN;DIV;1.0;7200.0;9000.0;AA;1.0;AA

RULESET_NAME;REQ_VALUE;EXPRESSION
COSMO_RedGAL;1.0;(SNR_RULER>=2.)
COSMO_AGN;1.0;(SNR_RULEB>1.0)||(SNR_RULEG>1.1)||(SNR_RULER>1.4)
COSMO_LYA;1.0;(SNR_RULEB>=2.0)||(SNR_RULEG>=2.2)||(SNR_RULER>=2.4)
ELG;1.0;(SNR_RULER>5) 
\end{verbatim}


\subsection{Results}

The S5,6,8 catalogues were run through the 4FS ETC, they are available here: 
\begin{itemize}
 \item \url{http://4most.mpe.mpg.de/ETCrunWI_output/S5/S5_2020_04_09_JC/QMOST_S5_CLUSTER_09April2020_correct_output_ETC09_04_20_15-39-10.fits} 
 \item \url{http://4most.mpe.mpg.de/ETCrunWI_output/S6/S6_2020_04_09_JC/S6_4MOST_R-228-234_09April2020_correct_output_ETC09_04_20_15-39-31.fits}
 \item \url{http://4most.mpe.mpg.de/ETCrunWI_output/S8/S8_2020_04_09_JC/QMOST_S8_09April2020_correct_output_ETC09_04_20_15-40-00.fits} 
\end{itemize}

Because shared targets have different S/N requirement, they do not require the same exposure time. 
One figure per template per ruleset is available here 
\begin{itemize}
 \item \url{https://gitlab.mpcdf.mpg.de/joco/lss_mock_dev/tree/master/python/qmost/S5/etc_plots}
 \item \url{https://gitlab.mpcdf.mpg.de/joco/lss_mock_dev/tree/master/python/qmost/S6/etc_plots}
 \item \url{https://gitlab.mpcdf.mpg.de/joco/lss_mock_dev/tree/master/python/qmost/S8/etc_plots}
\end{itemize}
A few examples are shown below. 

\begin{figure}
\centering
\includegraphics[width=0.8\columnwidth]{/home/comparat/Desktop/lss_mock_dev/python/qmost/S5/etc_plots/ClusBCG_4most_LRG_zmin_00_zmax_03_EBV_0_1.png}
\caption{\label{fig:etc:template}Example of ETC output for an S5 BCG .}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\columnwidth]{/home/comparat/Desktop/lss_mock_dev/python/qmost/S6/etc_plots/AGN_ALL_3PC_4most_AGN_type2_zmin_07_zmax_09_EBV_0_1.png}
\caption{\label{fig:etc:template}Example of ETC output for an S6 type 2 AGN.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\columnwidth]{/home/comparat/Desktop/lss_mock_dev/python/qmost/S6/etc_plots/AGN_ALL_3PC_4most_qso_BL_zmin_07_zmax_09_EBV_0_1.png}
\caption{\label{fig:etc:template}Example of ETC output for an S6 type 1 AGN.}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\columnwidth]{/home/comparat/Desktop/lss_mock_dev/python/qmost/S8/etc_plots/COSMO_RedGAL_4most_LRG_zmin_07_zmax_09_EBV_0_01.png}
\caption{\label{fig:etc:template}Example of ETC output for an S8 LRG.}
\end{figure}

% \begin{figure}
% \centering
% \includegraphics[width=0.8\columnwidth]{/home/comparat/Desktop/lss_mock_dev/python/qmost/S8/etc_plots/ELG_4most_ELG_zmin_05_zmax_07_EBV_0_01.png}
% \caption{\label{fig:etc:template}Example of ETC output for an S8 ELG.}
% \end{figure}


\section{Survey scope}


The S5,6,8 catalogues were run through the 4FS SOS survey scop, they are available here: 
\begin{itemize}
 \item \url{https://4most.mpe.mpg.de/ETCrunWI_output/S5/S5_2020_04_09_JC/survey_scope_FOMID67/html/survey_scope.html}
 \item \url{https://4most.mpe.mpg.de/ETCrunWI_output/S6/S6_2020_04_09_JC/survey_scope_FOMID26/html/survey_scope.html}
 \item \url{https://4most.mpe.mpg.de/ETCrunWI_output/S8/S8_2020_04_09_JC/survey_scope_FOMID51/html/survey_scope.html}
\end{itemize}

They are reproduced below. The adjustment in survey scope due to common targets is related to the fraction of total targets in Table \ref{tab:fraction:numbers}. 
For example S8 QSO has 24\% of all.

\begin{itemize}
 \item S5 dark conditions. The sum of fiber hour request: BCG + galaxies in clusters + filament galaxies reads $12.6+22.1×0.4+42.5 = 63.94 \times 10^4$ fiber hours. 
\begin{itemize}
 \item  The factor 0.4 reflects the fact that we cannot get to the completeness required (90\%) but in practice, due to fiber collision, we only manage to observer 30\%-40\%. 
 \item The total 63.9 is a 28\% above the allocation $\sim 50$. 
 \item 22\% of the filament galaxies are in common with S8 BG or LRG i.e. 9.3$\times 10^4$ fiber hours are shared. 
 \item 12\% of cluster galaxies are shared with S8 BG, LRG i.e. 2.6$\times 10^4$ fiber hours are shared. 
 \item All BCG are included in the cluster galaxies, but with a higher SNR request, so it is not double counted.
 \item Diminishing the required area from 13,500 to 10,000 square degrees would diminish the request from 35\%. 
\end{itemize}

 \item S6 dark conditions. The sum of fiber hour request: WIDE + DEEP + IR reads $54.4+9.5+3 = 66.9 \times 10^4$ fiber hours. 
\begin{itemize}
 \item The total 66.9 is a 33\% above the allocation $\sim 50$. 
  \item 26\% of the AGN WIDE are in common with S8 QSO or LyA i.e. 14$\times 10^4$ fiber hours are shared. 
  \item 11\% of the AGN DEEP are in common with S8 QSO or LyA i.e. 1$\times 10^4$ fiber hours are shared. 
  \item 28\% of the AGN IR are in common with AGN WIDE i.e. 0.84$\times 10^4$ fiber hours are shared (within this survey). 
 \item Diminishing the signal to noise request at the faint end to focus on obtaining redshifts can alleviate this (SSc have been tested in the past to do so). 
\end{itemize}
 
 \item S8 dark conditions. The sum of fiber hour request: BG + ELG + LRG + LyA + QSO reads $31.7+52.4+137+12.2+24.5 = 257.8 \times 10^4$ fiber hours. It is 28\% above the allocation $\sim 200$. 
\end{itemize}



  \begin{table*}
  \caption{Input parameters}
  \begin{center}
  \begin{tabular}{ l l rrrr rrr rrr}
  \hline \hline 
  S  & sub & res & $t_{\rm max}$ & sky & N$_{\rm targets}$ & \multicolumn{2}{c}{fraction} & req area & mock area & f$_{\rm area}$ & NSIDE \\
  &     &     &               &     &               & t=$t_{\rm max}$ & t=$t_{\rm min}$ & deg$^2$ & deg$^2$  & \% \\ 
  \hline 
  
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S5/survey_scope/survey_scope_cluster_BCG_t1.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S5/survey_scope/survey_scope_cluster_redGAL_t1.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S5/survey_scope/survey_scope_filament_GAL_t1.tex} 
\hline  \hline
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S6/survey_scope/survey_scope_AGN_DEEP_t1.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S6/survey_scope/survey_scope_AGN_IR_t1.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S6/survey_scope/survey_scope_AGN_WIDE_t1.tex} 
\hline       \hline
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S8/survey_scope/survey_scope_BG_t1.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S8/survey_scope/survey_scope_ELG_t1.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S8/survey_scope/survey_scope_LRG_t1.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S8/survey_scope/survey_scope_LyA_t1.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S8/survey_scope/survey_scope_QSO_t1.tex}

  \hline
  \end{tabular}
  \end{center}
  \end{table*}
  

  \begin{table*}
  \caption{Survey scope}
  \begin{center}
  \begin{tabular}{l l r r rr rr r}
  \hline \hline 
  S & sub & res & $t_{\rm max}$ &  sky & \multicolumn{2}{c}{N$_{\rm fh}10^{4}$} &  \multicolumn{2}{c}{speed (Avg $Fh/{Deg}^2)$} \\
  &     &     &   & &    SSM=0.5 & SSM=0.9          &    SSM=0.5 & SSM=0.9      \\
  \hline
  
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S5/survey_scope/survey_scope_cluster_BCG_t2.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S5/survey_scope/survey_scope_cluster_redGAL_t2.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S5/survey_scope/survey_scope_filament_GAL_t2.tex} 
S5 & sum & & D & 77.2 \\
\hline  \hline
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S6/survey_scope/survey_scope_AGN_DEEP_t2.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S6/survey_scope/survey_scope_AGN_IR_t2.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S6/survey_scope/survey_scope_AGN_WIDE_t2.tex} 
S6 & sum & & D & 77.2 \\
\hline     \hline
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S8/survey_scope/survey_scope_BG_t2.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S8/survey_scope/survey_scope_ELG_t2.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S8/survey_scope/survey_scope_LRG_t2.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S8/survey_scope/survey_scope_LyA_t2.tex}
\input{/home/comparat/Desktop/lss_mock_dev/python/qmost/S8/survey_scope/survey_scope_QSO_t2.tex}
S8 & sum & & D & 77.2 \\

  \hline
  \end{tabular}
  \end{center}
  \end{table*}
  


%\bibliographystyle{plain}
%\bibliography{references}
\end{document}

