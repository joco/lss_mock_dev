# LSS_mock

Code repository that generates mock catalogs for the SRG / eRosita 
Active Galactic Nuclei sample

# Reference

Comparat et al. 2019. https://arxiv.org/abs/1901.10866

# Dependencies 

python3 

and the following packages all installable with pip

numpy, scipy, astropy
h5py
healpy
speclite
extinction
dustmaps

# Files 

Together with the dustmap package, you need the E(B-V) Planck map:
https://irsa.ipac.caltech.edu/data/Planck/release_1/all-sky-maps/previews/HFI_CompMap_ThermalDustModel_2048_R1.20/HFI_CompMap_ThermalDustModel_2048_R1.20_EBV.png

The H1 map of our galaxy 
http://cdsarc.u-strasbg.fr/ftp/J/A+A/594/A116/NHI_HPX.fits.gz

# Environment variable

$GIT_AGN_MOCK
export GIT_AGN_MOCK='/path/2/software/lss_mock_dev/'
export TEST_DIR='/path/2/software/lss_mock_dev/data/test_simulation/'
# SETUP

fill the python/setup.py file with all pathes corresponding to your file system

# test 



cd python/DM_LC

readme_command_light_cone.sh

cd python

how to create coordinate and galaxy files: readme_command_galaxies.sh  
how to create AGN mock: readme_command_agn.sh  
how to create cluster mock: readme_command_clusters.sh  
how to create LSS tracer mock: readme_command_lss_tracers.sh



<!-- import numpy, scipy, astropy, h5py, healpy, dustmaps  -->
python3 -m pip install --user virtualenv
virtualenv pyEnv_lss_mock

source pyEnv_lss_mock/bin/activate

conda xxx virtualenv

python3 -m pip install --upgrade python3

python3 -m pip install numpy scipy astropy h5py healpy  dustmaps
python3 -m pip install speclite extinction

source deactivate astroconda

# python2 version 
conda create --name py2CONDA python=2.7
conda activate py2CONDA
python2 -m pip install --upgrade python2
python2 -m pip install numpy scipy astropy h5py healpy  dustmaps

# creates a problem with healpy :
# python2 -m pip install speclite extinction

python2 -m pip install ipython

source deactivate py2CONDA

Preparation of the test data
============================

cd $TEST_DIR/hlists
scp -r ds52:/data17s/darksim/MD/MD_1.0Gpc/hlists_DATA/hlist_0.06*.list.bz2 .
scp -r ds52:/data17s/darksim/MD/MD_1.0Gpc/hlists_DATA/hlist_0.07*.list.bz2 .
bunzip2 *.bz2
mv hlist_0.06270.list hlist_0.10000.list
mv hlist_0.06410.list hlist_0.20000.list
mv hlist_0.06560.list hlist_0.30000.list
mv hlist_0.06700.list hlist_0.40000.list
mv hlist_0.06850.list hlist_0.50000.list
mv hlist_0.07010.list hlist_0.60000.list
mv hlist_0.07160.list hlist_0.70000.list
mv hlist_0.07320.list hlist_0.80000.list
mv hlist_0.07490.list hlist_0.90000.list
mv hlist_0.07660.list hlist_0.95000.list
mv hlist_0.07830.list hlist_1.00000.list
